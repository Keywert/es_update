package es;

import es.job.ESBulkInsert;

import es.module.datafarm.Queue2Data;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class ApplicationV2 {
    public static void main(String[] args) throws IOException {
        Logger logger = Logger.getLogger("Log");
        FileHandler fileHandler;
        Set<String> docIds = new HashSet<>();


        // check argument
        if (args.length < 2) {
            System.out.println("Usage : java -jar esUpdate.jar [Update Type] [Param] .... ");
            System.out.println("[Update Type] ");
            System.out.println("[insertJson] [index] [filepath]");
            System.out.println("[dartsip] [index]");
            return ;
        }

        /**
         *  검색엔진 json bulkInsert :  jsonInsert
         *
         *
         *  reindex
         */
        if (args[0].matches("insertJson")) {
            System.out.println(args[0] + " : " + args[1] + " : " + args[2]);
            ESBulkInsert esBulkInsert = new ESBulkInsert(args[1], args[2]);
            docIds = esBulkInsert.jobMain();

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } else if (args[0].matches("reindex")){
                System.out.println(args[0] + " : " + args[1] );
        }
        else {
            System.out.println(args[0] + " is not matched!");
        }
    }
}
