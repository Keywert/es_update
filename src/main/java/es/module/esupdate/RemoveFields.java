package es.module.esupdate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.ESConfig;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RemoveFields {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();

        esConfigs.add(new ESConfig("es51"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es189"));
//        esConfigs.add(new ESConfig("es218"));
        esConfigs.add(new ESConfig("es18"));


        if (esConfigs.isEmpty())
            return;

        String index = "docdb";
        String type = "patent";

        String startDate = "19740101";
        String endDate = "19741017";

        String[] lss = {"ap","id","iw","em","ia"};

        String[] fields = {"docdbFamilyId","familyId"};

        String[] apNums = {"1019960002448","1019980001354","1019980019608","1019990016746","1019990019082","1020000018272","1020000021348","1020000026270","1020000029178","1020000031702","1020000036223","1020010004027","1020010005056","1020010005056","1020010005703","1020010006549","1020010007539","1020010011612","1020010013905","1020010015266","1020010017202","1020010040967","1020020000544","1020020001512","1020020003174","1020020003175","1020020004114","1020020004412","1020020004955","1020020006101","1020020006684","1020020018948","1020020022669","1020020028032","1020030004122","1020030009864","1020030019200","1020030034369","1020030039729","1020050000805","1020050003314","1020050031334","1020060016653","1020070006049","1020070015279","1020090003015","1020100008093","1020100008094","1020110000787","1020110004513","1020130002827","1020130008528","1020160003407","1020190002073","1020200001088"};
        String[] regNums = {"0144364","0181358","0211337","0167657","0173669","0224121","0210265","0213296","0222354","0225387","0243159","0230555","0231986","0231986","0233512","0233364","0236267","0237287","0243853","0243471","0247006","0270437","0272023","0272444","0275695","0275696","0283769","0280677","0276496","0277081","0277283","0290351","0295323","0298723","0311648","0319522","0327457","0340437","0347652","0381920","0383769","0408305","0425407","0441567","0439226","0445586","0461668","0460573","0456424","0460793","0476168","0477255","0482390","0493111","0494576"};
        String[] docType = {"A","B"};

        String[] familyIds = {"42247670","43357110","43384580","44146997","44203106","44279798","44460244","44478127","44543192","44653282","44720187","44735944","44736089","44741723","44901129","44983699","44992058","45329815","45402388","45476615","45722680","45874158","45876164","45927242","45938799","45953192","45973667","45995494","46024921","46198063","46206523","46314460","46314917","46420336","46456937","46548385","46579157","46601822","46724621","46724640","46724652","46754409","46763184","46924528","46934664","46970397","47008631","47018863","47042036","47045023","47090199","47217560","47297386","47501550","47558091","47603623","47668797","47715096","47715312","47743430","47744453","47744964","47747641","47749755","47753387","47757118","47829393","47830007","47831855","47914883","47996287","48095992","48167002","48191606","48428945","48797755","48799803","48875015","49237520","49237522","49623313","49623323","49757810","49841845","49881803","49917609","50027394","50159582","50223597","50288076","50340845","50387262","50473752","50477290","50488201","50488495","50626881","50978491","51020447","51221451","51261685","51351309","51353666","51353668","51353672","51490491","51536550","51624353","51897937","52116317","52431298","52483752","52586414","52586488","52596608","52628698","52665641","52742483","53029520","53478028","53681130","53681132","54479826","54833625","55269236","55399872","55630308","56108703","56285908","60325875","61301488","62195571","41219880","44991384","44991437","41719281","43067235","42035805","44166671","43910771","44834137","45066873","43429180","44195876","42539658","43429203","40229773","42106865","41010577","43971143","42110503","41051126","45772516","34945999","37888733","38218244","38080985","35520072","37942418","35786547","33448527","37054034","37053679","15418425","41610497","42115430","42242456","40247453","38512810","41706933","40823903","34381130","29266487","34594848","9955613","33418315","36908146","34062932","34062619","34116205","34434950","34437319","32849510","32712278","34879443","35786949","34625210","34559707","31493025","32771849","8565819","36406213","34918338","38122597","38122598","34917987","8852717","26007358","26246810","32012234","29268874","31497208","31184656","29553509","34055993","40626849","43297509","41731539","43356202","40527415","40011356","39367272","42110999","41162319","42170748","41415355","43456236","27351212","23297428","16575503","16575566","15914502","15914522","14283315","27341710","23526279","27307478","26745148","3817077","8018264","7783101","16613600","24930796","26309710","10795638","14326379","13857949","13858009","13858039","14147541","13857978","14147516","8225945","18358291","20408173","8233351","7798347","43162907","23330788","22980025","19896111","22476431","21693712","25164336","11610698","6858619","22978424","25438854"};
        String[] faIds = {"6308995","72945419"};

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.boolQuery()
                        .should(QueryBuilders.termsQuery("familyId",faIds))
                        .should(QueryBuilders.termsQuery("docdbFamilyId",faIds)))
                .filter(QueryBuilders.existsQuery("sepInfo"))
//                .filter(QueryBuilders.termsQuery("patentType",docType))
//                .filter(QueryBuilders.boolQuery()
//                        .should(QueryBuilders.termsQuery("originalApplicationNumber",apNums)))
//                .filter(QueryBuilders.matchQuery("mainIpc","none"))
//                .filter(QueryBuilders.termsQuery("legalStatus",lss))
//                .must(
//                        QueryBuilders.boolQuery()
//                                .should(QueryBuilders.matchQuery("inpadocFamilyId","none"))
//                                .should(QueryBuilders.matchQuery("extendedFamilyId","none")))
//                .filter(QueryBuilders.existsQuery("registerDate"))
//                .must(QueryBuilders.existsQuery("dartipInfo"))
//                .must(QueryBuilders.idsQuery().addIds("us05515568a"))
//                .must(QueryBuilders.multiMatchQuery("37087211",fields))
//                .filter(QueryBuilders.existsQuery("sepInfo"))
//                .must(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate));
//                        QueryBuilders.boolQuery()
//                                .should(QueryBuilders.rangeQuery("publicationDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("registerDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("openDate").from(startDate).to(endDate)));
        ;

        String[] ids = {"us05515568a"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.existsQuery("referenceData"))
                .must(QueryBuilders.existsQuery("summary"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(20000101).to(20061231));

        String[] indexies = {"kipo", "docdb", "jpo", "uspto", "sipo", "pct", "epo", "dpma", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo", "uspto_past", "jpo_past"};


        IdsQueryBuilder idsQb = QueryBuilders.idsQuery().addIds(ids);

        SearchResponse scrollResp = esConfigs.get(0).client
                .prepareSearch(indexies)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setSize(2)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            int count = 0;
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 1000 == 0)
                        System.out.println(count);

                    System.out.println(hit.getId());

//                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    JsonNode legalInfo = jsonNode.get("legalInfo");
//                    if (legalInfo == null)
//                        continue;
//
//                    ((ObjectNode) jsonNode).remove("legalInfo");

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
                    JsonNode sepInfo = jsonNode.get("sepInfo");
                    if (sepInfo == null)
                        continue;
                    ((ObjectNode) jsonNode).remove("sepInfo");

                    JsonNode sepUpdateDate = jsonNode.get("sepUpdateDate");
                    if (sepUpdateDate == null)
                        continue;
                    ((ObjectNode) jsonNode).remove("sepUpdateDate");

                    JsonNode sepOriginFlag = jsonNode.get("sepOriginFlag");
                    if (sepOriginFlag == null)
                        continue;
                    ((ObjectNode) jsonNode).remove("sepOriginFlag");



//                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    JsonNode dartipInfo = jsonNode.get("dartipInfo");
//                    if (dartipInfo == null)
//                        continue;
//
//                    ((ObjectNode) jsonNode).remove("dartipInfo");

//                    JsonNode mainIpc = jsonNode.get("mainIpc");
//                    if (mainIpc == null)
//                        continue;
//
//                    ((ObjectNode) jsonNode).remove("mainIpc");
//                    ((ObjectNode) jsonNode).remove("techIpc");
//                    ((ObjectNode) jsonNode).remove("mainIpcInfo");



//                    System.out.println(hit.getSourceAsString());

//                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    JsonNode summary = jsonNode.get("summary");
//                    if (summary == null)
//                        continue;
//
//                    String text = summary.toString();
//
//                    if (text.contains("abstract"))
//                        continue;
//
//                    if (text.contains("SDOAB"))
//                        continue;
//
//                    if (text.contains("p"))
//                        continue;
//
//                    if (text.contains("a"))
//                        continue;
//
//                    System.out.println(hit.getId() + " : " + text);
//                    ((ObjectNode) jsonNode).remove("summary");

//                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    JsonNode sepInfo = jsonNode.get("dartipInfo");
//                    if (sepInfo == null)
//                        continue;
//
//                    ((ObjectNode) jsonNode).remove("dartipInfo");

//                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    JsonNode sepInfo = jsonNode.get("sepInfo");
//                    if (sepInfo == null)
//                        continue;
//
//                    ((ObjectNode) jsonNode).remove("sepInfo");

//                    JsonNode priorityClaims = jsonNode.get("priorityClaims");
//                    if (priorityClaims != null)
//                        ((ObjectNode) jsonNode).remove("priorityClaims");
//
//                    JsonNode internationalApplication = jsonNode.get("internationalApplication");
//                    if (internationalApplication != null)
//                        ((ObjectNode) jsonNode).remove("internationalApplication");
//
//                    JsonNode internationalPublicationNumber = jsonNode.get("internationalPublicationNumber");
//                    if (internationalPublicationNumber != null)
//                        ((ObjectNode) jsonNode).remove("internationalPublicationNumber");
//
//                    JsonNode internationalPublicationDate = jsonNode.get("internationalPublicationDate");
//                    if (internationalPublicationDate != null)
//                        ((ObjectNode) jsonNode).remove("internationalPublicationDate");
//
//                    JsonNode internationalApplicationNumber = jsonNode.get("internationalApplicationNumber");
//                    if (internationalApplicationNumber != null)
//                        ((ObjectNode) jsonNode).remove("internationalApplicationNumber");
//
//                    JsonNode internationalApplicationDate = jsonNode.get("internationalApplicationDate");
//                    if (internationalApplicationDate != null)
//                        ((ObjectNode) jsonNode).remove("internationalApplicationDate");


//                    JsonNode docdbFamilyId = jsonNode.get("docdbFamilyId");
//                    if (docdbFamilyId != null || "none".equals(docdbFamilyId.asText()))
//                        ((ObjectNode) jsonNode).remove("docdbFamilyId");

//                    JsonNode examinationRequestDate_origin = jsonNode.get("examinationRequestDate_origin");
//                    if (examinationRequestDate_origin != null) {
//                        System.out.println(examinationRequestDate_origin);
//                        ((ObjectNode) jsonNode).remove("examinationRequestDate_origin");
//                    }

//                    JsonNode extendedFamilyId = jsonNode.get("extendedFamilyId");
//                    if (inpadocFamilyId != null && "none".equals(extendedFamilyId.asText()))
//                        ((ObjectNode) jsonNode).remove("extendedFamilyId");
//
//                    JsonNode inpadocFamilyId = jsonNode.get("inpadocFamilyId");
//                    if (inpadocFamilyId != null && "none".equals(inpadocFamilyId.asText()))
//                        ((ObjectNode) jsonNode).remove("inpadocFamilyId");
//
//                    JsonNode extendedFamilyId = jsonNode.get("extendedFamilyId");
//                    if (inpadocFamilyId != null && "none".equals(extendedFamilyId.asText()))
//                        ((ObjectNode) jsonNode).remove("extendedFamilyId");

//                    JsonNode dartipInfo = jsonNode.get("dartipInfo");
//                    if (dartipInfo != null)
//                        ((ObjectNode) jsonNode).remove("dartipInfo");
//                    else
//                        continue;

//                    JsonNode publicationDate = jsonNode.get("publicationDate");
//                    JsonNode registerDate = jsonNode.get("registerDate");
//                    if (publicationDate != null && registerDate != null){
//                        if (publicationDate.asText().equals(registerDate.asText()))
//                            ((ObjectNode) jsonNode).remove("publicationDate");
//                        else
//                            continue;
//
//                    } else
//                        continue;

//                    ((ObjectNode) jsonNode).remove("docdbFamilyId");
//                    ((ObjectNode) jsonNode).remove("docdbFamilyCount");
//                    ((ObjectNode) jsonNode).remove("docdbFamilyCitationCount");
//                    ((ObjectNode) jsonNode).remove("docdbFamilyCount");
//                    ((ObjectNode) jsonNode).remove("inpadocFamilyId");
//

//                    System.out.println(hit.getIndex() + " : " + hit.getId() + " - " + objectMapper.writeValueAsString(jsonNode));
//
                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new IndexRequest(hit.getIndex(), type, hit.getId())
                                .source(objectMapper.writeValueAsString(jsonNode)));

                }
                scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            System.out.println("Finish Count : " + count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
