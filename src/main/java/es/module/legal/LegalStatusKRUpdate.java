package es.module.legal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.utility.ESDocIdCheck;
import es.module.utility.MakeDocID;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.sql.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LegalStatusKRUpdate {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();

//        esConfigs.add(new ESConfig("es51"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es13"));

//        esConfigs.add(new ESConfig("es218"));


        if (esConfigs.isEmpty())
            return;

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String tableName = "patent.KIPO_ADMIN_BIBLIOGRAPHIC";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now().minus(Period.ofDays(300));

//        String query = "select * from " + tableName + " where upd_date > '" + localDate + "'" +
//                " and status is null and registration <> '' limit 10000";

        String query = "select * from " + tableName + " where " +
//                " registration <> '' and upd_date > '20210716' " +
                " (status is null ) and registration <> '' " +
////                " and applicationNumber regexp '^(10|20)(19|20)[0-9]{2}07[0-9]{5}$'" +
//                                " and registration in ('공고')"+
//                                " and registration not in ('등록','공개')"+
//                " and upd_date > '" + localDate + "'" +
                " limit 15000";

        System.out.println(query);

        String index = "kipo";
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.existsQuery("documentId"));

        String[] fields = {"applicationNumber"};

        Statement st = null;
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String,String> map = new HashMap<>();
        List<String> appNums = new ArrayList<>();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (++count % 100 == 0) {
                    System.out.println(count);
                }

                String applicationNumber = rs.getString("applicationNumber");
                String registration = rs.getString("registration");
//                System.out.println(applicationNumber);
//                System.out.println(registration);

                if (registration == null || "".equals(registration))
                    continue;

                List<String> docIds = MakeDocID.getDocIds(index,applicationNumber,"P");
                if (docIds == null)
                    continue;

                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();
                patentFamilyVO.setLegalStatus(registration);

                String json = objectMapper.writeValueAsString(patentFamilyVO);
                appNums.add( applicationNumber);

                for (String docId : docIds)
                    map.put(docId,json);

                if (map.size() > 100) {
                    List<String> hitDocIds = ESDocIdCheck.getHitDocIds(esConfigs.get(0),index,map);

                    updateES(esConfigs, index,map, hitDocIds);

                    Thread.sleep(1000);

                    updateDB(conn,appNums);

                    map.clear();
                    appNums.clear();
                }

                docIds.clear();
            }

            if(!map.isEmpty()) {
                List<String> hitDocIds = ESDocIdCheck.getHitDocIds(esConfigs.get(0),index,map);

                updateES(esConfigs, index,map, hitDocIds);
//                Thread.sleep(1000);
                updateDB(conn,appNums);
            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            System.out.println("Finished Count : " + count);


        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static void updateDB(Connection conn, List<String> appNums) {
        String query = "UPDATE patent.KIPO_ADMIN_BIBLIOGRAPHIC SET status = 2 " +
                " WHERE applicationNumber = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String apnum : appNums) {
//                System.out.println(apnum);

                preparedStatement.setString(1, apnum);
//                preparedStatement.setString(2, apnum.substring(1));
                preparedStatement.addBatch();

            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void updateES(List<ESConfig> esConfigs, String index, Map<String, String> map, List<String> docIds) {
        if (map == null)
            return;

        for (String docId : docIds ) {
            String json = map.get(docId);

            if (json == null)
                continue;

//            System.out.println(docId + " : " + json);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", docId)
                        .doc(json.getBytes())
                        .upsert(json.getBytes()));


        }
    }
}
