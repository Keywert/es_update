package es.module.file;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileProc {
    public static List getFilepathList(String filepath) {
        List arrayList = new ArrayList();

        File dir = new File(filepath);

        if (dir.exists() == false) {
            System.out.println("Not exist File Path.");
            return null;
        }

        visitDir(arrayList, dir);

        return arrayList;
    }

    public static void visitDir(List arrayList, File file) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles)
                visitDir(arrayList, f);

        } else {
//            if( file.getName().matches("^kr(\\d){11}(\\w)(\\d)?\\.json$"))
//                arrayList.add(file);
            if (file.getName().matches(".+\\.json$"))
                arrayList.add(file);
//             jpo
//            if (file.getName().matches(".+[^5-9]\\.json$"))
//                arrayList.add(file);
        }
    }


    public static ArrayList getFilePathList(String inputPath, String type) {
        ArrayList arrayList = new ArrayList();

        File dir = new File(inputPath);

        if (dir.exists() == false) {
            System.out.println("경로가 존재하지 않음.");
            return null;
        }

        visitDir(arrayList, dir, type);

        return arrayList;
    }

    public static void visitDir(ArrayList arrayList, File file, String type) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles) {
                visitDir(arrayList, f, type);
            }
        } else {
//            if (type.matches("(sipo_n|sipo_cn2|pct_n|dpma_n)")) { // xml
            if (type.matches("[A-z]{3,5}_[n|d]")) { // xml
                ////////// epo xml
                if (file.getName().matches("[0-9|A-z|-]*[NEW]*\\.[xX][mM][lL]")) {
                    arrayList.add(file.getAbsolutePath());
                }

            } else if (type.matches("jpo_t1|jpo_t2|uspto_t1|uspto_t2")) {  // txt
                // jpo txt
                if (file.getName().matches("[0-9|A-z|-]*\\.[Tt][Xx][Tt]")) {
                    arrayList.add(file.getAbsolutePath());
                }

            } else if (type.matches("docdb")){
                if (file.getName().matches("^DOCDB-20[1-2][0-9][0-9|A-z|-]*\\.[xX][mM][lL]")) {
                    arrayList.add(file.getAbsolutePath());
                }

            }




//            if (file.getName().matches("[0-9|A-z|-]*\\.json")) {
//                arrayList.add(file.getAbsolutePath());
//            }

//            /// kipo
//            if (file.getName().matches("[0-9]*\\.[xX][mM][lL]")) {
//                arrayList.add(file.getAbsolutePath());
//            }

            //// kipo, sipoen
//            if (file.getName().matches("[0-9|A-z|-]*[NEW]*\\.[xX][mM][lL]")) {
//                arrayList.add(file.getAbsolutePath());
//            }

            //// sipo
//            if (file.getName().matches("[0-9|A-z]*\\.[xX][mM][lL]")) {
//                arrayList.add(file.getAbsolutePath());
//            }


            //// uspto txt, xml, sgml
//            if (file.getName().matches("[0-9|A-z|-]*\\.[Ss][Gg][Mm][Ll]?")) { //sgm
////            if (file.getName().matches("^[a-z][0-9|A-z|-]*\\.[Xx][Mm][Ll]$")){
//                arrayList.add(file.getAbsolutePath());
//            }

            /// epo xml
//            if (file.getName().matches("^DOC[0-9|A-z|-]*[NEW]*\\.[xX][mM][lL]")){
//            if (file.getName().matches("[0-9|A-z|-]*\\.[xX][mM][lL]")) {
//            if (file.getName().matches("[0-9|A-z|-]*\\.[SsNn][GgRr][Mm]")){
//            if (file.getName().matches("[0-9|A-z|-]*\\.[Ss][Gg][Mm]")){
//                arrayList.add(file.getAbsolutePath());
//            }

//            /// paj
//            if (file.getName().matches("[0-9|A-z|-]*[NEW]*\\.[xXsSN][mMgGR][lLmM]")) {
//                arrayList.add(file.getAbsolutePath());
//            }

//            /// pct
//            if (file.getName().matches("wo[0-9|A-z|-]*[NEW]*\\.[xXsS][mMgG][lLmM]")){
//                arrayList.add(file.getAbsolutePath());
//            }
//            if (file.getName().matches("wo[0-9|A-z|-]*[NEW]*\\.[xXsS][mMgG][lLmM]")){
//                arrayList.add(file.getAbsolutePath());
//            }
//            if (file.getName().matches("[0-9|A-z|-]*[NEW]*\\.[xX][mM][lL]")) {
//                arrayList.add(file.getAbsolutePath());
//            }


//            // jpo 5-9 xx
//            if (file.getName().matches("[0-9|A-z|-]*[NEW]*[^5-9]\\.[xX][mM][lL]")){
//                arrayList.add(file.getAbsolutePath());
//            }


//            if (file.getName().matches("wo[0-9|A-z|-]*\\.[Nn][Rr][Mm]")){a
//                arrayList.add(file.getAbsolutePath());
//            }

//            if(file.getAbsolutePath().contains("B012")) {
//                if (file.getName().matches("[0-9]*\\.[xXs][mMg][lLm]")) {
//                    arrayList.add(file);
//                }
//            }
        }
    }

    public int makeFile(String outFile, Map<String, Object> jsonMap, String documentId, String type) {
        ObjectMapper objectMapper = new ObjectMapper();

//        System.out.println(outFile + " : " + documentId);
        String outPath = "";

        try {
//            if (jsonMap.size() > 1)
//                System.out.println(objectMapper.writeValueAsString(jsonMap));
//            System.out.println(documentId);

            String fileName = documentId;


            Matcher matcher = Pattern.compile("^[A-z]{2}(\\d){6,8}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 5), fileName.substring(5, 8), fileName.substring(2));

            }

            matcher = Pattern.compile("^[A-z]{2}(\\d){9}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 5), fileName.substring(5, 8), fileName.substring(8, 11), fileName.substring(2));

            }

            matcher = Pattern.compile("^[A-z]{2}(\\d){10,18}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 6), fileName.substring(6, 9), fileName.substring(9, 12), fileName.substring(2));

            }

            matcher = Pattern.compile("^[A-z]{2}[A-z|0-9]{10,18}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 6), fileName.substring(6, 9), fileName.substring(9, 12), fileName.substring(2));

            }

            matcher = Pattern.compile("^de[A-z|0-9]{8}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 5), fileName.substring(5, 8), fileName.substring(2));

            }

            matcher = Pattern.compile("^tw[A-z]{1}[0-9]{6}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 5), fileName.substring(5, 8), fileName.substring(2));

            }

            matcher = Pattern.compile("^(au|fr|ru|in|gb|ca|it)[0-9]{2,5}[A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 5), fileName.substring(2));
            }

            matcher = Pattern.compile("^(au|fr|ru|in|gb|ca|it)[0-9][A-z].*$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 3), fileName.substring(2));
            }

            matcher = Pattern.compile("^(in|it|au)[0-9|A-z]{7,11}$").matcher(fileName);
            if (matcher.find()) {
                outPath = String.format(outFile + "/json/%s/%s/%s/", fileName.substring(0, 2),
                        fileName.substring(2, 5), fileName.substring(2));
            }


            if (outPath.isEmpty()) {
                return -1;
            }


            fileName += ".json";

            File dest = new File(outPath);
            if (!dest.exists())
                dest.mkdirs();
            outPath += fileName;

            objectMapper.writeValue(new File(outPath), jsonMap);


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return 0;
    }

    public void errorFile(File file, String outFilePath) {
        String outPath = "";
        String fileName = file.getName();


        outPath = String.format(outFilePath + "/error/%s/%s/%s/", fileName.substring(0, 2),
                fileName.substring(2, 5), fileName.replaceAll("\\..+$", ""));


        File dest = new File(outPath);
        if (!dest.exists())
            dest.mkdirs();
        outPath += fileName;

        fileCopy(file.getAbsolutePath(), outPath);


    }

    public void fileCopy(String orgFilePath, String newFilePath) {
        try {
            FileInputStream inputStream = new FileInputStream(orgFilePath);
            FileOutputStream outputStream = new FileOutputStream(newFilePath);
            FileChannel fcin = inputStream.getChannel();
            FileChannel fcout = outputStream.getChannel();

            long size = fcin.size();
            fcin.transferTo(0, size, fcout);

            fcout.close();
            fcin.close();
            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
        }
    }
}
