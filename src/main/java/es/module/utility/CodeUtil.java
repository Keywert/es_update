package es.module.utility;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by coolhoony on 2016-02-02.
 */
public class CodeUtil {
//    private static Matcher matcher;


    public static String getIpc_jp(String ipc, Logger logger){
        String [] list = ipc.split("\\s+");

        String input;
        if (list.length < 2)
            input = ipc;
        else
            input = list[0]+list[1];

        return getIpc(input,logger);
    }
    public static String getIpc(String ipc){
        ipc = ipc.replaceAll("\\s+","").replaceAll("^[0-9]","").replaceAll("[A-z|-]$","").trim();
        String code="";

        try{

            if (!ipc.contains("/")){
                if (ipc.matches("^[A-z](\\d){2}[A-z](\\d){5,6}$")) {
                    code = ipc.substring(0, 4) + "-" + ipc.substring(4, 7) + "/" + ipc.substring(7, ipc.length());
//                    System.out.println(code);
                    return code;
                }
            }

            if(ipc.length() < 4)
                return null;

//            System.out.println(ipc);
            String[] group = ipc.substring(4, ipc.length()).split("/");

            if(group[0].length() ==0) {
                code = ipc;
                return code;
            }else if(group[0].matches("[A-z|\\.]+[0-9]*")){
                code = ipc;
                return code;
            } else {
                if (group.length == 2) {
                    String str = group[0].replaceAll("[^0-9]","");
                    String mainGroup = String.format("%03d", Integer.parseInt(str));
                    code = ipc.substring(0, 4) + "-" + mainGroup + "/" + group[1];
                } else {
                    code = ipc;
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }
        return code;
    }

    public static String getIpc(String ipc,Logger logger){
        ipc = ipc.replaceAll("\\s+","").replaceAll("^[0-9]","").replaceAll("[A-z|-]$","").trim();
        String code="";

        try{

            if (!ipc.contains("/")){
                if (ipc.matches("^[A-z](\\d){2}[A-z](\\d){5,6}$")) {
                    code = ipc.substring(0, 4) + "-" + ipc.substring(4, 7) + "/" + ipc.substring(7, ipc.length());
//                    System.out.println(code);
                    return code;
                }
            }

            if(ipc.length() < 4)
                return null;

//            System.out.println(ipc);
            String[] group = ipc.substring(4, ipc.length()).split("/");

            if(group[0].length() ==0) {
                code = ipc;
                return code;
            }else if(group[0].matches("[A-z|\\.]+[0-9]*")){
                code = ipc;
                return code;
            } else {
                if (group.length == 2) {
                    String mainGroup = String.format("%03d", Integer.parseInt(group[0]));
                    code = ipc.substring(0, 4) + "-" + mainGroup + "/" + group[1];
                } else {
                    code = ipc;
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(ipc);
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(ipc);
            return null;
        }
        return code;
    }

    public static String getCpc(String ipc,Logger logger){
        ipc = ipc.replaceAll("\\s+","");
        String code="";

        try{

            if (!ipc.contains("/")){
                if (ipc.matches("^[A-z](\\d){2}[A-z](\\d){5,6}$")) {
                    code = ipc.substring(0, 4) + "-" + ipc.substring(4, 7) + "/" + ipc.substring(7, ipc.length());
//                    System.out.println(code);
                    return code;
                }
            }

            if(ipc.length() < 4)
                return null;

//            System.out.println(ipc);
            String[] group = ipc.substring(4, ipc.length()).split("/");

            if(group[0].length() ==0) {
                code = ipc;
                return code;
            }else if(group[0].matches("[A-z|\\.]+[0-9]*")){
                code = ipc;
                return code;
            } else {
                if (group.length == 2) {
                    String mainGroup = String.format("%04d", Integer.parseInt(group[0]));
                    String subGroup = group[1];
                    code = ipc.substring(0, 4) + "-" + mainGroup + "/" + subGroup;
                } else {
                    code = ipc;
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(ipc);
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(ipc);
            return null;
        }
        return code;
    }

    public static String getCpc(String ipc){
        ipc = ipc.replaceAll("\\s+","");
        String code="";

        try{

            if (!ipc.contains("/")){
                if (ipc.matches("^[A-z](\\d){2}[A-z](\\d){5,6}$")) {
                    code = ipc.substring(0, 4) + "-" + ipc.substring(4, 7) + "/" + ipc.substring(7, ipc.length());
//                    System.out.println(code);
                    return code;
                }
            }

            if(ipc.length() < 4)
                return null;

//            System.out.println(ipc);
            String[] group = ipc.substring(4, ipc.length()).split("/");

            if(group[0].length() ==0) {
                code = ipc;
                return code;
            }else if(group[0].matches("[A-z|\\.]+[0-9]*")){
                code = ipc;
                return code;
            } else {
                if (group.length == 2) {
                    String mainGroup = String.format("%04d", Integer.parseInt(group[0]));
                    String subGroup = group[1];
                    code = ipc.substring(0, 4) + "-" + mainGroup + "/" + subGroup;
                } else {
                    code = ipc;
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }
        return code;
    }

    public static void insertMainIpcInfo(Map<String, Object> jsonMap, String ipc) {
        HashMap<String,String> hashMap = new HashMap<String, String>();
//        System.out.println(ipc);

        ipc = ipc.replaceAll("-","");

        try {
            if (ipc.length() < 7) {
                hashMap.put("level1", ipc.substring(0, 1));
                hashMap.put("level2", ipc.substring(0, 3));
                hashMap.put("level3", ipc.substring(0, 4));
            } else {
                String[] group = ipc.substring(4, ipc.length()).split("/");

                if(group.length==2){
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                    hashMap.put("level4", ipc.substring(0, 4) + group[0]);
                    hashMap.put("level5", ipc.substring(0, 4) + group[0] + group[1]);
                } else {
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                }

            }
            jsonMap.put("mainIpcInfo", hashMap);
        }catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }catch (NumberFormatException e){
            e.printStackTrace();
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }


    public static void insertMainIpcInfo(Map<String, Object> jsonMap, String ipc, Logger logger) {
        HashMap<String,String> hashMap = new HashMap<String, String>();
//        System.out.println(ipc);

        ipc = ipc.replaceAll("-","");

        try {
            if (ipc.length() < 7) {
                hashMap.put("level1", ipc.substring(0, 1));
                hashMap.put("level2", ipc.substring(0, 3));
                hashMap.put("level3", ipc.substring(0, 4));
            } else {
                String[] group = ipc.substring(4, ipc.length()).split("/");

                if(group.length==2){
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                    hashMap.put("level4", ipc.substring(0, 4) + group[0]);
                    hashMap.put("level5", ipc.substring(0, 4) + group[0] + group[1]);
                } else {
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                }

            }
            jsonMap.put("mainIpcInfo", hashMap);
        }catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(ipc);

        }catch (NumberFormatException e){
            e.printStackTrace();
            logger.info(ipc);
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
            logger.info(ipc);
        }
    }

    public static void insertMainCpcInfo(Map<String, Object> jsonMap, String ipc, Logger logger) {
        HashMap<String,String> hashMap = new HashMap<String, String>();
//        System.out.println(ipc);

        ipc = ipc.replaceAll("-","");

        try {
            if (ipc.length() < 7) {
                hashMap.put("level1", ipc.substring(0, 1));
                hashMap.put("level2", ipc.substring(0, 3));
                hashMap.put("level3", ipc.substring(0, 4));
            } else {
                String[] group = ipc.substring(4, ipc.length()).split("/");

                if(group.length==2){
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                    hashMap.put("level4", ipc.substring(0, 4) + group[0]);
                    hashMap.put("level5", ipc.substring(0, 4) + group[0] + group[1]);
                } else {
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                }

            }
            jsonMap.put("mainCpcInfo", hashMap);
        }catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(ipc);

        }catch (NumberFormatException e){
            e.printStackTrace();
            logger.info(ipc);
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
            logger.info(ipc);
        }
    }

    public static void insertMainCpcInfo(Map<String, Object> jsonMap, String ipc) {
        HashMap<String,String> hashMap = new HashMap<String, String>();
//        System.out.println(ipc);

        ipc = ipc.replaceAll("-","");

        try {
            if (ipc.length() < 7) {
                hashMap.put("level1", ipc.substring(0, 1));
                hashMap.put("level2", ipc.substring(0, 3));
                hashMap.put("level3", ipc.substring(0, 4));
            } else {
                String[] group = ipc.substring(4, ipc.length()).split("/");

                if(group.length==2){
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                    hashMap.put("level4", ipc.substring(0, 4) + group[0]);
                    hashMap.put("level5", ipc.substring(0, 4) + group[0] + group[1]);
                } else {
                    hashMap.put("level1", ipc.substring(0, 1));
                    hashMap.put("level2", ipc.substring(0, 3));
                    hashMap.put("level3", ipc.substring(0, 4));
                }

            }
            jsonMap.put("mainCpcInfo", hashMap);
        }catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }catch (NumberFormatException e){
            e.printStackTrace();
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }

    public static void insertMainFiInfo(Map<String, Object> jsonMap, String fi, Logger logger) {
        HashMap<String,String> hashMap = new HashMap<String, String>();
//        System.out.println(ipc);

        fi = fi.replaceAll("-","");

        try {
            if (fi.length() < 7) {
                hashMap.put("level1", fi.substring(0, 1));
                hashMap.put("level2", fi.substring(0, 3));
                hashMap.put("level3", fi.substring(0, 4));
            } else {
                String[] group = fi.substring(4, fi.length()).split("/");

                if(group.length==2){
                    hashMap.put("level1", fi.substring(0, 1));
                    hashMap.put("level2", fi.substring(0, 3));
                    hashMap.put("level3", fi.substring(0, 4));
                    hashMap.put("level4", fi.substring(0, 4) + group[0]);
                    hashMap.put("level5", fi.substring(0, 4) + group[0] + group[1]);
                } else {
                    hashMap.put("level1", fi.substring(0, 1));
                    hashMap.put("level2", fi.substring(0, 3));
                    hashMap.put("level3", fi.substring(0, 4));
                }

            }
            jsonMap.put("mainFiInfo", hashMap);
        }catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            logger.info(fi);

        }catch (NumberFormatException e){
            e.printStackTrace();
            logger.info(fi);
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
            logger.info(fi);
        }
    }

    public String getUpc(String upc, Logger logger) {
        String code = upc.replaceAll("\\s","0");

//        System.out.println(upc + " : " + code);
        String strs ="";

        if (upc.contains("R"))
            strs = ".00R";

//        PLT45
        if (upc.matches("PLT.+"))
            return "PLT/"+ String.format("%03d",Integer.parseInt(code.replaceAll("[A-z]","")));


        if (code.length() > 6){
            String lastString = code.substring(6);
            int length = lastString.length();
            for (int i = length; i < 3; i++)
                lastString += "0";
            code = String.format("%s/%s.%s", code.substring(0, 3), code.substring(3, 6), lastString);
        } else if (code.length() == 6) {
            code = String.format("%s/%s", code.substring(0, 3), code.substring(3));
        } else {
            if (code.matches("^D(\\d)+")){
                code = "D"+ String.format("%05d",Integer.parseInt(code.replaceAll("[A-z]","")));
            } else {
                code = String.format("%06d",Integer.parseInt(code.replaceAll("[A-z]","")));
            }

            code = String.format("%s/%s", code.substring(0, 3), code.substring(3));
        }

        if (!strs.isEmpty())
            code+=strs;

//        System.out.println(upc + " : " + code);
        return code;
    }

//    public String getUpc(String upc,Logger logger) {
//        upc = upc.replaceAll("\\s+", "")
//                .replaceFirst("/FOR", "/");
//
//        if (upc.length() < 4)
//            return upc;
//
//        String code = "";
//        try {
//            List<String> group = Splitter.on("/").splitToList(upc);
//            String lv1 = "";
//            String lv2 = "";
//            String lv3 = "";
//
//            if (group.size() == 2) {
//                if (group.get(0).length() < 3) {
//                    lv1 = String.format("%03d", Integer.parseInt(group.get(0)));
//                } else {
//                    lv1 = group.get(0);
//                }
//
//                String[] subGroup = group.get(1).split("\\.");
//
//                lv2 = subGroup[0];
//
//                if (lv2.length() != 3) {
//
//                    if (lv2.contains("E")) {
//                        lv2 = "E" + String.format("%02d", Integer.parseInt(lv2.replace("E", "")));
//                    } else if (lv2.length() == 0) {
//                        lv2 = "000";
//                    } else {
//                        lv2 = String.format("%03d", Integer.parseInt(lv2));
//                    }
//
//                    if (subGroup.length == 2) {
//                        matcher = Pattern.compile("[A-z]+").matcher(subGroup[1]);
//                        if (matcher.find()) {
//                            String a = matcher.group();
//                            String t = subGroup[1].replaceAll(a, "");
//
//                            lv3 = String.format("%0" + (3 - a.length()) + "d", Integer.parseInt(t)) + a;
//                        } else {
//                            lv3 = String.format("%03d", Integer.parseInt(subGroup[1]));
//                        }
//
//                    }
//                }
//
//            } else {
//                code = upc;
//            }
//
//            if (lv3.length() != 0)
//                code = lv1 + "/" + lv2 + "." + lv3;
//            else
//                code = lv1 + "/" + lv2;
//
//        } catch (StringIndexOutOfBoundsException e) {
//            e.printStackTrace();
//            logger.info(upc);
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//            logger.info(upc);
//        }
//        return code;
//    }


    public void insertMainUpcInfo(Map<String, Object> jsonMap, String upc, Logger logger) {
        HashMap<String,String> hashMap = new HashMap<String, String>();

        String [] upcArr = upc.split("/");

        if(upcArr.length==2){
            hashMap.put("level1", upcArr[0]);
            hashMap.put("level2", upcArr[0]+upcArr[1]);
        }else
            hashMap.put("level1",upcArr[0]);


        jsonMap.put("mainUpcInfo",hashMap);

    }


}

