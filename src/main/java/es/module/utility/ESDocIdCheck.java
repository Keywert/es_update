package es.module.utility;

import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;

import java.util.*;

public class ESDocIdCheck {
    public static Map<String, String> removePatentMap(ESConfig esConfig, String index, Map<String, String> map) {
        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(map.keySet());

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        List<String> hitIds = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                hitIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);


        return null;
    }

    public static List<String> getHitDocIds(ESConfig esConfig, String index, Map<String, String> map) {
//        String[] exceptLegalStatus = {"it","iw","ia","ag"};
        String[] exceptLegalStatus = {"it"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.idsQuery().addIds(map.keySet()))
                .mustNot(QueryBuilders.termsQuery("legalStatus",exceptLegalStatus))
                ;
        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(map.keySet());

//        System.out.println(bqb);

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        List<String> hitIds = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                hitIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);


        return hitIds;
    }
    public static List<String> getHitDocIds2(ESConfig esConfig, String index, Map<String, String> map) {
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.idsQuery().addIds(map.keySet()))
                .mustNot(QueryBuilders.existsQuery("originalApplicationNumber"));
        ;
//        System.out.println(bqb);

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        List<String> hitIds = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                hitIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);


        return hitIds;
    }


    public static List<String> getJPHitDocIds(ESConfig esConfig, String index, Map<String, PatentFamilyVO> map) {
//        String[] exceptLegalStatus = {"it","iw","ia","ag"};
        String[] exceptLegalStatus = {"it"};
        if (map.isEmpty())
            return null;

        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()){
            String id = iterator.next();
            PatentFamilyVO patentFamilyVO = map.get(id);

            BoolQueryBuilder subbq = QueryBuilders.boolQuery()
                    .must(QueryBuilders.idsQuery().addIds(id))
                    .mustNot(QueryBuilders.matchQuery("legalStatus",patentFamilyVO.getLegalStatus()));

            bqb.should(subbq);
        }

//        bqb.mustNot(QueryBuilders.termsQuery("legalStatus",exceptLegalStatus));

//                .must(QueryBuilders.idsQuery().addIds(map.keySet()))
//                .mustNot(QueryBuilders.termsQuery("legalStatus",exceptLegalStatus))
//                ;
        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(map.keySet());

//        System.out.println(bqb);

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        List<String> hitIds = new ArrayList<>();

//        System.out.println(scrollResp.getHits().getTotalHits());

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                hitIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);


        return hitIds;
    }

    public static Map<String, String> getUSdocIds(ESConfig esConfig, String index, Map<String, String> map) {
        TermsQueryBuilder tqb = QueryBuilders.termsQuery("applicationNumber",map.keySet());

//        String[] exceptLegalStatus = {"it","iw","ia","ag"};
        String[] exceptLegalStatus = {"it"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("applicationNumber",map.keySet()))
                .mustNot(QueryBuilders.termsQuery("legalStatus",exceptLegalStatus))
                ;


//        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(map.keySet());

        String[] fields = {"documentId","applicationNumber"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        Map<String, String> docIdsMap = new HashMap<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object apNum = hit.getSource().get("applicationNumber");
                if (apNum == null)
                    continue;

                String json = map.get(apNum);

                if (json == null)
                    continue;

                docIdsMap.put(hit.getId(),json);

            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);


        if (!docIdsMap.isEmpty())
            return docIdsMap;

        return null;
    }

    public static Map<String, Set<String>> getESdocIds(ESConfig esConfig, String index, ArrayList<String> appNums) {
        TermsQueryBuilder tqb = QueryBuilders.termsQuery("applicationNumber",appNums);

        String[] fields = {"documentId","applicationNumber"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(tqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        Map<String, Set<String>> docIdsMap = new HashMap<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object apNum = hit.getSource().get("applicationNumber");
                if (apNum == null)
                    continue;

                Set<String> docIds = docIdsMap.get(apNum.toString());

                if (docIds == null){
                    docIds = new HashSet<>();
                    docIds.add(hit.getId());
                }else {
                    docIds.add(hit.getId());
                }

                docIdsMap.put(apNum.toString(),docIds);
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (docIdsMap.isEmpty())
            return null;

        return docIdsMap;
    }

    public static List<String> getHitDocId(ESConfig esConfig, String index, ArrayList<String> docIds) {
        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(docIds);

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        List<String> hitIds = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                hitIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        return hitIds;
    }

    public static Set<String> getJPHitdocs(ESConfig esConfig, String index, Set<String> numbers) {
        String[] fields = {"applicationNumber"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.matchQuery("legalStatus","em"))
//                .must(QueryBuilders.termsQuery("patentType","B"))
                .must(QueryBuilders.termsQuery("applicationNumber",numbers));

        System.out.println(bqb);

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        Set<String> apSet = new HashSet<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                apSet.add(hit.getId());
                System.out.println(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        return apSet;
    }

    public static Set<String> getKRRegDocIds(ESConfig esConfig, String index, String regNum) {
        if (!regNum.matches("^(10|20)[0-9]{7}0000$")){
            System.out.println(regNum + " does not match the format~!");
            return null;
        }

        String registerNumber = regNum.substring(2,9);

        String patentType = "B";
        if (regNum.matches("^20[0-9]{7}0000$"))
            patentType = "Y";

        String[] fields = {"patentId"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("patentType",patentType))
                .must(QueryBuilders.termQuery("registerNumber",registerNumber));

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        Set<String> docIds = new HashSet<>();
        Set<String> patentIds = new HashSet<>();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            docIds.add(hit.getId());

            Object patentId = hit.getSource().get("patentId");
            if (patentId != null)
                patentIds.add(patentId.toString());
        }

        if (docIds.isEmpty())
            return null;

        if (!patentIds.isEmpty()) {
            Set<String> pairSet = getDocIdsPatentId(esConfig,index,patentIds);
            if (pairSet != null)
                docIds.addAll(pairSet);
        }

        return docIds;
    }

    private static Set<String> getDocIdsPatentId(ESConfig esConfig,String index, Set<String> patentIds) {
        String[] fields = {"documentId"};
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("patentId",patentIds));

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        Set<String> docIds = new HashSet<>();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            docIds.add(hit.getId());
        }

        if (docIds.isEmpty())
            return null;

        return docIds;
    }
}
