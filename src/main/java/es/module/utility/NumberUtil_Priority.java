package es.module.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtil_Priority {
    //    private static Matcher matcher;

    private static String[] eraName = {
            "1988",
            "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", // 1~10
            "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", // 11~20
            "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", // 21~30
            "2019", "2020", "2021", "1959", "1960", "1961", "1962", "1963", "1964", "1965", // 31~40
            "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", // 41~50
            "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", // 51~60
            "1986", "1987", "1988", "1989"};

    // 일본연호 1925, 평성 + 1988

    public static String getNumber(String country, String number, String type) {
        if (country == null)
            return number;

        if (number == null)
            return number;

        Matcher matcher = Pattern.compile("^([A-z]{2})?PCT.+$").matcher(number);
        if (matcher.find())
            return getNumberPCT("WO",number,type);

        matcher = Pattern.compile("^PCT.+").matcher(number);
        if (matcher.find())
            return getNumberPCT("WO",number,type);

        matcher = Pattern.compile("^WO(19|20)[0-9]{2}[A-z]{2}[0-9]{4,6}$").matcher(number);
        if (matcher.find())
            return getNumberPCT("WO",number,type);

        if (country.matches("KR"))
            return getNumberKR(number, type);
        else if (country.matches("JP"))
            return getNumberJP(country, number, type);
        else if (country.matches("WO"))
            return getNumberPCT(country, number, type);
        else if (country.matches("CN"))
            return getNumberCN(country, number, type);
        else if (country.matches("EP"))
            return getNumberEPO(country, number, type);
        else if (country.matches("US"))
            return getNumberUS(country, number, type);

        return number;
    }

    private static String getNumberUS(String country, String number, String type) {
        Matcher matcher;
        String num = number.replaceAll("\\..?$", "")
                .replaceAll("[\\.|,]", "");

        if (type.matches("AN")) {
            // 200480031274
            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "/" + num.substring(4);

//            61/648,189
            matcher = Pattern.compile("^(\\d){2}/(\\d){6}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^(\\d){1}/(\\d){6}$").matcher(num);
            if (matcher.find())
                return "0" + num;


            // 10094301
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 2) + "/" + num.substring(2);

            // US18370000345
//            System.out.println(num);
//            System.out.println(getUSSerialCode(num));
            matcher = Pattern.compile("^US(\\d){11}$").matcher(num);
            if (matcher.find())
                return getUSSerialCode(num);

//            US200813128443
            matcher = Pattern.compile("^US(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(6, 8) + "-" + num.substring(8);


        } else if (type.matches("PN|RN")) {

            // 275367
            matcher = Pattern.compile("^(\\d){6}$").matcher(num);
            if (matcher.find())
                return "0" + num;

            // 51130891
            matcher = Pattern.compile("^(\\d){7}$").matcher(num);
            if (matcher.find())
                return num;


            // 051130891
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return num.substring(1);


            // 2004/0153747
            matcher = Pattern.compile("^(\\d){4}\\/(\\d){7}$").matcher(num);
            if (matcher.find())
                return num.replaceFirst("\\/", "-");


            // D85019
            matcher = Pattern.compile("^D(\\d){5}$").matcher(num);
            if (matcher.find())
                return "D" + String.format("%06d", Integer.parseInt(num.substring(1)));


        }


//        System.out.println("getNumberCN : " +num);

        return number;
    }


    public static String getUSSerialCode(String applicationNumber) {
        if (applicationNumber == null)
            return null;

        String tmp = applicationNumber.replaceAll("(us|US)", "");

        String years = tmp.substring(0, 4);
        String remain = tmp.substring(5);

        if (applicationNumber.contains("a") || applicationNumber.contains("b")) {
            int year = Integer.parseInt(years);
            return getYearToSeries(year) + remain;
        } else if (applicationNumber.contains("u")) {
            return "28" + remain;
        } else if (applicationNumber.contains("s")) {
            return "29" + remain;
        } else if (applicationNumber.contains("e")) {
            return "90" + remain;
        } else {
            int year = Integer.parseInt(years);
            return getYearToSeries(year) + "-" + remain;
        }
    }

    private static String getYearToSeries(int year) {
        if (year < 1948) {
            return "02";
        } else if (year < 1960) {
            return "03";
        } else if (year < 1969) {
            return "04";
        } else if (year < 1979) {
            return "05";
        } else if (year < 1987) {
            return "06";
        } else if (year < 1993) {
            return "07";
        } else if (year < 1998) {
            return "08";
        } else if (year < 2002) {
            return "09";
        } else if (year < 2005) {
            return "10";
        } else if (year < 2008) {
            return "11";
        } else
            return "12";
    }

    private static String getNumberEPO(String country, String number, String type) {
        Matcher matcher;

        String num = number.replaceAll("[^A-z|0-9|\\.|/]", "");

        num = num.replaceAll("EP", "")
                .replaceAll("\\..?$", "")
                .replaceAll("\\.", "")
                .replaceAll("-", "");


        if (type.matches("AN")) {
            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4);


            matcher = Pattern.compile("^(\\d){2}\\.?(\\d){6}$").matcher(num);
            if (matcher.find()) {

                int tmp = Integer.parseInt(num.substring(0, 2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + num.substring(0, 2);
                else
                    year = "20" + num.substring(0, 2);

                return year + "-" + num.substring(2);
            }

//            EP20020080342
            matcher = Pattern.compile("^EP(\\d){11}$").matcher(num);
            if (matcher.find())
                return num.substring(2, 6) + "-" + num.substring(7);

            //            EP19860103508D
            matcher = Pattern.compile("^EP(\\d){11}D$").matcher(num);
            if (matcher.find()) {
                num = num.replaceAll("D", "");
                return num.substring(2, 6) + "-" + num.substring(7);
            }
////            WO2007EP64547
//            matcher = Pattern.compile("^WO(\\d){11}$").matcher(num);
//            if (matcher.find())
//                return num.substring(2, 6) + "-" + num.substring(7);

            //PCT/US92/04438
            num = number.replaceAll("PCT", "")
                    .replaceAll("/", "");

            matcher = Pattern.compile("^[A-z]{2}(\\d){7,8}$").matcher(num);
            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(2, 4)) > 50) ? "19" + num.substring(2, 4) : "20" + num.substring(2, 4);
                return "PCT/" + num.substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(num.substring(4)));
            }

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){6}$").matcher(number);
            if (matcher.find())
                return number;

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){5}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 10) + "/" + String.format("%06d", Integer.parseInt(num.substring(10)));


        } else if (type.matches("PN")) {

            num = number.replaceAll("[\\s|-]", "");

            matcher = Pattern.compile("^(\\d){5,8}$").matcher(num);

            if (matcher.find())
                return String.format("%07d", Integer.parseInt(num));


        }

//        System.out.println("getNumberEPO : " + number + " type : " + type + " country : " + country);
        return number;
    }

    private static String getNumberCN(String country, String number, String type) {
        Matcher matcher;
        String num = number.replaceAll("\\..+$", "");

        if (type == "AN") {
            // 200480031274
            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4);


            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find()) {

                int tmp = Integer.parseInt(num.substring(0, 2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + num.substring(0, 2);
                else
                    year = "20" + num.substring(0, 2);

                return year + "-" + num.substring(2, 3) + String.format("%07d", Integer.parseInt(num.substring(3)));
            }
        } else if (type == "RN") {
            // 0031274
            matcher = Pattern.compile("^(\\d){7}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^(\\d){9}$").matcher(num);
            if (matcher.find())
                return num;

            // 10, 201212122
            matcher = Pattern.compile("^(10|20)(\\d){7}$").matcher(num);
            if (matcher.find())
                return num.substring(2);

        }


//        System.out.println("getNumberCN : " +num);

        return number;
    }

    public static String getNumber(String country, String number, String type, String date) {
        if (country.matches("KR"))
            return getNumberKR(number, type);
        else if (country.matches("JP"))
            return getNumberJP(country, number, type);
        else if (country.matches("WO"))
            return getNumberPCT(country, number, type);
        else if (country.matches("EP"))
            return getNumberEP(country, number, type, date);
        else if (country.matches("CN"))
            return getNumberCN(number, type);

        return number;
    }

    private static String getNumberEP(String country, String number, String type, String date) {
        Matcher matcher;

        number = number.replaceAll("\\s", "").replaceFirst("EP", "");

        // 09356058
        matcher = Pattern.compile("^(\\d){8}$").matcher(number);
        if (matcher.find())
            return date.substring(0, 4) + "-" + matcher.group().substring(2);

//        09176918.2
        matcher = Pattern.compile("^(\\d){8}\\.(\\d)$").matcher(number);
        if (matcher.find())
            return date.substring(0, 4) + "-" + matcher.group().substring(2, 8);

//        System.out.println(number +" : "+ date);

        return number;
    }

    private static String getNumberPCT(String country, String number, String type) {
        Matcher matcher;
        String num = number.replaceAll("[^A-z|0-9]|\\/", "").trim();

        if (type.matches("PN")) {
            num = num.replaceAll("WO", "").trim();

            matcher = Pattern.compile("^(\\d){2}\\/(\\d){6}$").matcher(num);

            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(0, 2)) > 50) ? "19" + num.substring(0, 2) : "20" + num.substring(0, 2);
                return "WO" + year + "/" + String.format("%06d", Integer.parseInt(num.substring(2, num.length())));
            }

            matcher = Pattern.compile("^(\\d){4}\\/(\\d){6}$").matcher(num);

            if (matcher.find())
                return num;


            // 199834593
            matcher = Pattern.compile("^(\\d){4}(\\d){5}$").matcher(num);
            if (matcher.find())
                return "WO" + num.substring(0, 4) + "/" + String.format("%06d", Integer.parseInt(num.substring(4)));

            matcher = Pattern.compile("^(\\d){4}(\\d){6}$").matcher(num);
            if (matcher.find())
                return "WO" + num.substring(0, 4) + "/" + num.substring(4);

            // 20040024537
            matcher = Pattern.compile("^(\\d){4}(\\d){7}$").matcher(num);
            if (matcher.find())
                return "WO" + num.substring(0, 4) + "/" + num.substring(5);

            matcher = Pattern.compile("^(\\d){6,8}$").matcher(num);

            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(0, 2)) > 50) ? "19" + num.substring(0, 2) : "20" + num.substring(0, 2);
                return "WO" + year + "/" + String.format("%06d", Integer.parseInt(num.substring(2)));
            }


        } else if (type.matches("AN")) {
            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){6}$").matcher(number);
            if (matcher.find())
                return number;

            //PCT/US92/04438
            num = num.replaceAll("PCT", "");

            matcher = Pattern.compile("^[A-z]{2}(\\d){7,8}$").matcher(num);
            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(2, 4)) > 50) ? "19" + num.substring(2, 4) : "20" + num.substring(2, 4);
                return "PCT/" + num.substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(num.substring(4)));
            }


            matcher = Pattern.compile("^[A-z]{2}(\\d){10}$").matcher(num);
            if (matcher.find()) {
                return num.substring(0, 6) + "/" + String.format("%06d", Integer.parseInt(num.substring(6)));
            }

            //WO2007AT00486 WO2007AT004865    -> PCT/AT2007/000486
            matcher = Pattern.compile("^WO(\\d){4}[A-z]{2}(\\d){5,6}$").matcher(num);
            if (matcher.find()) {
                return "PCT/" + num.substring(6, 8) + num.substring(2, 6) + "/" + String.format("%06d", Integer.parseInt(num.substring(8)));
            }

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){5}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 10) + "/" + String.format("%06d", Integer.parseInt(num.substring(10)));

            // WO2018CN119519
            matcher = Pattern.compile("^WO(19|20)[0-9]{2}[A-z]{2}[0-9]{4,6}$").matcher(number);
            if (matcher.find()) {
                return "PCT/" + number.substring(6, 8) + number.substring(2, 6) + "/" + String.format("%06d", Integer.parseInt(number.substring(8)));
            }

        }

        System.out.println("getNumberPCT : " + number + " type : " + type + " country : " + country);

        return number;
    }

    private static String getNumberJP(String country, String number, String type) {
        Matcher matcher;
        if (type.matches("U|B[0-9]?"))
            return number;

        //2006-072868
        matcher = Pattern.compile("^(\\d){4}-(\\d){6}$").matcher(number);
        if (matcher.find())
            return matcher.group();

        //2006-72868
        matcher = Pattern.compile("^(\\d){4}-(\\d){3,5}$").matcher(number);
        if (matcher.find())
            return matcher.group().substring(0, 5) + String.format("%06d", Integer.parseInt(matcher.group().substring(5, matcher.group().length())));

        // 10-023061
        matcher = Pattern.compile("^(\\d){1,2}[-|/](\\d){4,6}$").matcher(number);
        if (matcher.find()) {
            String[] results = matcher.group().split("[-|/]");
            if (Integer.parseInt(results[0]) >= eraName.length) {
                int tmp = Integer.parseInt(number.substring(0, 2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + number.substring(0, 2);
                else
                    year = "20" + number.substring(0, 2);

                return year + "-" + String.format("%06d", Integer.parseInt(number.substring(4)));
            }
            return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
        }

        if (type.matches("RN")) {
            matcher = Pattern.compile("(\\d){7}").matcher(number);
            if (matcher.find())
                return matcher.group();

        } else if (type.matches("AN|PN|ON")) {

//            제41057/1989 -  제41057/1989
            String num = number.replaceAll("[^0-9|/|-]", "");
            matcher = Pattern.compile("^(\\d){5,6}/(\\d){4}$").matcher(num);
            if (matcher.find()) {
                String[] results = num.split("/");
                return results[1] + "-" + String.format("%06d", Integer.parseInt(results[0]));
            }

            matcher = Pattern.compile("^(\\d){5,6}[/](\\d){2}$").matcher(num);
            if (matcher.find()) {
                String[] results = num.split("/");
                int tmp = Integer.parseInt(results[1]);
                String year = "";
                if (tmp >= 50)
                    year = "19" + results[1];
                else
                    year = "20" + results[1];

                return year + "-" + String.format("%06d", Integer.parseInt(results[0]));
            }

            // 10-023061
            matcher = Pattern.compile("^(\\d){1,2}[-|/](\\d){4,6}$").matcher(num);
            if (matcher.find()) {
                String[] results = matcher.group().split("[-|/]");
                if (Integer.parseInt(results[0]) >= eraName.length) {
                    int tmp = Integer.parseInt(num.substring(0, 2));
                    String year = "";
                    if (tmp >= 50)
                        year = "19" + num.substring(0, 2);
                    else
                        year = "20" + num.substring(0, 2);

                    return year + "-" + String.format("%06d", Integer.parseInt(num.substring(4)));
                }
                return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
            }

            //JP-P-2008-242147
            matcher = Pattern.compile("JP-[P|U]-(\\d){4}-(\\d){6}$").matcher(number);
            if (matcher.find())
                return number.replaceAll("[^0-9]", "");

            // 연호 미포함
            matcher = Pattern.compile("^(\\d){10}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4, number.length());

            // 연호 포함
            matcher = Pattern.compile("^(\\d){8}$").matcher(number);
            if (matcher.find()) {
                if (Integer.parseInt(number.substring(0, 2)) >= eraName.length)
                    return number;

                return eraName[Integer.parseInt(number.substring(0, 2))] + "-" + number.substring(2);
            }

//            JP19710000001U ,  JP19720000001
            matcher = Pattern.compile("^JP(\\d){11}U?").matcher(number);
            if (matcher.find()) {
                return number.substring(2, 6) + "-" + number.substring(7, 13);
            }

        }


//        System.out.println(number);
        return number;
    }


    // AN, PN, RN, ON
    private static String getNumberKR(String number, String type) {
        String num = number.replaceAll("[^0-9|\\-]", "");
        Matcher matcher;

        if (type.matches("ON|AN")) {
//            1019980065165
            matcher = Pattern.compile("^(10|20|30)[0-9]{11}").matcher(number);
            if (matcher.find())
                return number;

            matcher = Pattern.compile("^(10|20|30)-(19|20)(\\d){2}-(\\d){7}$").matcher(number);
            if (matcher.find())
                return number;

            matcher = Pattern.compile("^KR(\\d){11}$").matcher(number);
            if (matcher.find())
                return "10" + number.substring(2, 6) + number.substring(6);

            //KR1997 10 0000983
            matcher = Pattern.compile("^KR(\\d){4}10(\\d){7}$").matcher(number);
            if (matcher.find())
                return "10" + number.substring(2, 6) + number.substring(8);


        } else if (type.matches("PN")) {
//            1004730350000
            matcher = Pattern.compile("(10|20|30)[0-9]{7}0{4}$").matcher(number);
            if (matcher.find())
                return number.substring(2, 9);

//                        1019980065165
            matcher = Pattern.compile("(10|20|30)[0-9]{11}").matcher(number);
            if (matcher.find())
                return number.substring(2, 6) + "-" + number.substring(6);

        } else if (type.matches("RN")) {
//            10-0377477
            matcher = Pattern.compile("^(10|20|30)-[0-9]{7}$").matcher(number);
            if (matcher.find())
                return number.substring(3, 10);

            // 260300
            matcher = Pattern.compile("^[0-9]{6}$").matcher(number);
            if (matcher.find())
                return String.format("%07d", Integer.parseInt(number));

            matcher = Pattern.compile("^(10|20|30)-[0-9]{6}$").matcher(number);
            if (matcher.find())
                return String.format("%07d", Integer.parseInt(number.substring(3, 9)));


            // 10-01002192
            matcher = Pattern.compile("^(10|20|30)-[0-9]{8}$").matcher(number);
            if (matcher.find())
                return number.substring(4, 11);


            // 10-01002192
            matcher = Pattern.compile("^(10|20|30)-[0-9]{7}-0000$").matcher(number);
            if (matcher.find())
                return number.substring(3, 10);


        }

        // 연호 미포함
//        matcher = Pattern.compile("^(JP)?(\\d){4}(-)?(\\d){6}((A|B|U|S|Y)(\\d)?)?$|^(JP)?((\\d){7})((A|B|U|S|Y)(\\d)?)?$").matcher(number);
//        if (matcher.find()) {
//            return matcher.group().replace("JP", "");
//        }


        System.out.println("getNumberKR : "+ number + " type " + type);
        return number;
    }


    public static String getNumberJP(String number, String type) {
        Matcher matcher;
        matcher = Pattern.compile("^JP(\\d){10}$").matcher(number);
        if (matcher.find())
            return "PCT/" + number.substring(0, 6) + "/" + number.substring(6);

        matcher = Pattern.compile("^WO(\\d){10}$").matcher(number);
        if (matcher.find())
            return number.substring(0, 6) + "/" + number.substring(6);


        String num = number.replaceAll("[^0-9]", "");

        if (type.matches("RN")) {
            matcher = Pattern.compile("(\\d){7}").matcher(num);
            if (matcher.find())
                return matcher.group();

        } else if (type.matches("AN|PN|ON")) {
            // 연호 미포함
            matcher = Pattern.compile("^(\\d){10}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4, number.length());

            // 연호 포함
            matcher = Pattern.compile("^(\\d){8}$").matcher(number);
            if (matcher.find())
                return eraName[Integer.parseInt(matcher.group().substring(0, 2))] + "-" + matcher.group().substring(2, matcher.group().length());
        }
        return number;
    }

    public static String getTxtNumberJP(String number, String type) {
        Matcher matcher;
//        System.out.println(number);


        if (type.matches("IAN")) {
//            PCT/JP94/01409
            matcher = Pattern.compile("^PCT/(\\w){2}(\\d){2,4}/(\\d){5,6}").matcher(number);
            if (matcher.find()) {
                String[] arrs = number.split("/");

                int tmp = Integer.parseInt(arrs[1].substring(2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + arrs[1].substring(2);
                else
                    year = "20" + arrs[1].substring(2);

                return arrs[0] + "/" + arrs[1].substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(arrs[2]));
            }
            System.out.println("getTxtNumberJP : " + number);
        } else if (type.matches("IPN")) {
//ＷＯ９５／０６０３２
            matcher = Pattern.compile("^WO(\\d){2,4}/(\\d){5,6}").matcher(number);
            if (matcher.find()) {
                String[] arrs = number.split("/");
                int tmp = Integer.parseInt(arrs[0].substring(2));
                String year = String.format("%02d", tmp);

                if (tmp >= 50)
                    year = "19" + year;
                else
                    year = "20" + year;

                return arrs[0].substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(arrs[1]));
            }
            System.out.println("getTxtNumberJP : " + number);

        }

        String num = number.replaceAll("\\(.*\\)", "");

        num = num.replaceAll("[^0-9|−|-]", "")
                .replaceAll("−", "-");

        if (type.matches("RN")) {
            matcher = Pattern.compile("(\\d){7}").matcher(number);
            if (matcher.find())
                return matcher.group();

            matcher = Pattern.compile("(\\d){7}").matcher(num);
            if (matcher.find())
                return matcher.group();


            // 10-023061, 1-254521
            matcher = Pattern.compile("^(\\d){1,2}[-|−](\\d)+$").matcher(num);
            if (matcher.find()) {
                String[] results = matcher.group().split("[-|−]");
                return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
            }


        } else if (type.matches("AN|PN|ON")) {
            //特開２００３－２６０３４５（Ｐ２００３－２６０３４５Ａ）
            //2006-072868
            matcher = Pattern.compile("^(\\d){4}[-|−](\\d){6}$").matcher(num);
            if (matcher.find())
                return matcher.group();

            //2006-72868
            matcher = Pattern.compile("^(\\d){4}[-|−](\\d)+$").matcher(num);
            if (matcher.find())
                return matcher.group().substring(0, 5) + String.format("%06d", Integer.parseInt(matcher.group().substring(5, matcher.group().length())));

            // 10-023061, 1-254521
            matcher = Pattern.compile("^(\\d){1,2}[-|−](\\d)+$").matcher(num);
            if (matcher.find()) {
                String[] results = matcher.group().split("[-|−]");
                return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
            }

            // 연호 미포함
            matcher = Pattern.compile("^(\\d){10}$").matcher(num);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4, number.length());

            // 연호 포함
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return eraName[Integer.parseInt(matcher.group().substring(0, 2))] + "-" + matcher.group().substring(2, matcher.group().length());
        }

        return number;
    }


    public static String getNumberCN(String number, String type) {
//        System.out.println(number);

//        2015 3 0028611
//        2012 1 0225317
//        2009 1 0055719
//        2007 2 0195884 U

        Matcher matcher;
        //00100023
        String num = number.replaceAll("[^0-9]", "");
        String year = "";

        if (type.matches("AN")) {
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find()) {
                if (Integer.parseInt(num.substring(0, 2)) < 50)
                    year = "20" + num.substring(0, 2);
                else
                    year = "19" + num.substring(0, 2);

                return year + "-" + num.charAt(2) + String.format("%07d", Integer.parseInt(num.substring(3, num.length())));
            }

            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4, num.length());

            matcher = Pattern.compile("^(\\d){15}$").matcher(num);
            if (matcher.find())
                return num.substring(2, 6) + "-" + num.substring(0, 1) + num.substring(8);

        } else if (type.matches("RN")) {
            matcher = Pattern.compile("^(\\d){9}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^(\\d){7}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 1) + String.format("%08d", Integer.parseInt(num.substring(2, num.length())));

        }

        return number;
    }

}
