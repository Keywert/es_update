package es.module.utility;

import org.elasticsearch.common.Strings;

public class PatentID {

    public static String getPatentId(String documentId, String applicationNumber) {
        String country = documentId.substring(0,2);
        String type = "P";

        if (documentId.matches(".+[u|y][0-9]?$"))
            type = "U";
        else if (documentId.matches(".+[s][0-9]?$"))
            type = "S";
        else if (documentId.matches(".+[e][0-9]?$"))
            type = "E";
        else if (documentId.matches(".+[H][0-9]?$"))
            type = "H";
        else if (documentId.matches(".+[I][0-9]?$"))
            type = "I";
        else if (documentId.matches(".+[P][0-9]?$"))
            type = "P";


        if (country.matches("wo"))
            country="";
        else if (country.matches("kr")){
            if (type.equals("P"))
                applicationNumber = "10"+applicationNumber;
            else
                applicationNumber = "20"+applicationNumber;
        }

        String id  = country + Strings.padStart(applicationNumber.replaceAll("[-|/]",""),13,'0') + type ;
        id = id.toUpperCase();

//        System.out.println(id);

        return id;
    }

    public static String getKeywertId(String docid) {
        String keywertId = docid.toUpperCase();

        if (docid.matches("^(JPPS|USPS).+$"))
            return keywertId.substring(0,4) + lpad(keywertId.substring(4),13,"0");
        else if (docid.matches("^kr.+$")){
            if (docid.matches(".+[u|y][0-9]?$"))
                return "KR" + lpad("20"+keywertId.substring(2),13,"0");
            else
                return "KR" + lpad("10"+keywertId.substring(2),13,"0");
        } else if (docid.matches("^[A-z]{2}[0-9].+$")){
            return keywertId.substring(0,2) + lpad(keywertId.substring(2),15,"0");
        }

        return null;
    }

    private static String lpad(String str, int l, String s) {
        String result = str;

        for (int i=0,len=l-result.length(); i<len; i++)
            result = s + result;

        return result;
    }
}
