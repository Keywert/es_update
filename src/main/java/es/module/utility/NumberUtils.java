package es.module.utility;

import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtils {
//    private static Matcher matcher;

    private static String[] eraName = {
            "1988",
            "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", // 1~10
            "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", // 11~20
            "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", // 21~30
            "2019", "2020", "2021", "1959", "1960", "1961", "1962", "1963", "1964", "1965", // 31~40
            "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", // 41~50
            "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", // 51~60
            "1986", "1987", "1988", "1989"};

    // 일본연호 1925, 평성 + 1988

    public static String getNumber(String country, String number, String type) {
        if (number == null)
            return null;
        number = number.trim();

        Matcher matcher = Pattern.compile("^([A-z]{2})?PCT.+$").matcher(number);
        if (matcher.find())
            return getNumberPCT("WO",number,type);

        matcher = Pattern.compile("^PCT.+").matcher(number);
        if (matcher.find())
            return getNumberPCT("WO",number,type);

        matcher = Pattern.compile("^WO(19|20)[0-9]{2}[A-z]{2}[0-9]{4,6}$").matcher(number);
        if (matcher.find())
            return getNumberPCT("WO",number,type);

        if (country.matches("KR"))
            return getNumberKR(number, type);
        else if (country.matches("JP"))
            return getNumberJP(country, number, type);
        else if (country.matches("(WO|PCT)"))
            return getNumberPCT(country, number, type);
        else if (country.matches("CN"))
            return getNumberCN(number, type);
        else if (country.matches("EP"))
            return getNumberEPO(country, number, type);
        else if (country.matches("US"))
            return getNumberUS(country, number, type);
        else if (country.matches("DE"))
            return getNumberDE(number, type);
        else if (country.matches("CA"))
            return getNumberCA(number, type);

        return number;
    }

    private static String getNumberCA(String number, String type) {
        Matcher matcher;
        String num = number.toUpperCase();

        if (type.matches("AN")) {
            //  188984, 2936983
            matcher = Pattern.compile("^(\\d){6,7}$").matcher(num);
            if (matcher.find())
                return num;

            // CA 3065462, CA 194240
            matcher = Pattern.compile("^CA(\\s)*(\\d){6,7}$").matcher(num);
            if (matcher.find())
                return num.replaceAll("^CA(\\s)*","");

//            2000-314,723
            matcher = Pattern.compile("^(19|20)[0-9]{2}-(\\d){3},(\\d){3}$").matcher(num);
            if (matcher.find())
                return num.replaceAll(",","");

            // 2,665,035 , 2 982 652
            matcher = Pattern.compile("^[0-9]([,|\\.| ]|, )(\\d){3}([,|\\.| ]|, )(\\d){3}$").matcher(num);
            if (matcher.find())
                return num.replaceAll("[^0-9]","");


            // 665,035 , 982 652
            matcher = Pattern.compile("^(\\d){3}([,|\\.| ]|, )(\\d){3}$").matcher(num);
            if (matcher.find())
                return num.replaceAll("[^0-9]","");

        }

        System.out.println("getNumberCA : " + number + " type : " + type);
        return number;
    }

    private static String getNumberDE(String number, String type) {
        Matcher matcher;
        String num = number.replaceAll("\\..+$", "").toUpperCase();

        if (type.matches("AN")) {
//            10 2016 010 511
            num = num.replaceAll(" ", "");

            matcher = Pattern.compile("^(\\d){2}-(19|20)(\\d){2}-(\\d){6}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 2) + "-" + num.substring(2, 6) + "-" + num.substring(6);

            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 2) + "-" + num.substring(2, 6) + "-" + num.substring(6);

            matcher = Pattern.compile("^(\\d){1,8}$").matcher(num);
            if (matcher.find())
                return String.format("%08d", Integer.parseInt(num));

            matcher = Pattern.compile("^(\\d){1,8}[A-z]$").matcher(num);
            if (matcher.find())
                return String.format("%07d", Integer.parseInt(num.substring(0, num.length() - 1))) + num.substring(num.length() - 1);

            // 191100568X
//            matcher = Pattern.compile("^19(\\d){7}X$").matcher(num);
//            if (matcher.find()) {
//                return num.substring(0,2) + '-' + num.substring(2).replaceAll("X","");
//            }

            //2007002219
            matcher = Pattern.compile("^(\\d){10}$").matcher(num);
            if (matcher.find()) {
                return num.substring(0, 4) + '-' + num.substring(4);
            }

            // Sch63215
            matcher = Pattern.compile("^SCH(\\d){5}$").matcher(num);
            if (matcher.find()) {
                return num;
            }


            /// 1988 88309606 ,83  83112147
            matcher = Pattern.compile("^(\\d){2,4}\\s+(\\d){7,8}$").matcher(number);
            if (matcher.find()) {
                String[] numbers = number.split("\\s+");
                String year = (Integer.parseInt(numbers[1].substring(0, 2)) > 50) ? "19" + numbers[1].substring(0, 2) : "20" + numbers[1].substring(0, 2);
                return year + "-" + String.format("%06d", Integer.parseInt(numbers[1].substring(2)));
            }

            // St026177
            matcher = Pattern.compile("^[A-z]{1,2}(\\d)+$").matcher(number);
            if (matcher.find()) {
                return String.format("%1$8s", number).replace(' ', '0').toUpperCase();
            }

            // 68R48028,68R 48028
            matcher = Pattern.compile("^(\\d){2}[A-z]\\s*(\\d)+$").matcher(number);
            if (matcher.find()) {
                return String.format("%1$8s", number.replaceAll("\\s", "")).replace(' ', '0').toUpperCase();
            }


        } else if (type.matches("PN")) {
            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 2) + "-" + num.substring(2, 6) + "-" + num.substring(6);

            matcher = Pattern.compile("^(\\d){2}20(\\d){7}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 2) + "-" + num.substring(2, 6) + "-" + String.format("%06d", Integer.parseInt(num.substring(6)));

            matcher = Pattern.compile("^(\\d){1,8}$").matcher(num);
            if (matcher.find())
                return String.format("%08d", Integer.parseInt(num));

            // St026177
            matcher = Pattern.compile("^[A-z]{1,2}(\\d)+$").matcher(number);
            if (matcher.find()) {
                return String.format("%1$8s", number).replace(' ', '0').toUpperCase();
            }

        }

        System.out.println("getNumberDE : " + number + " type : " + type);
        return number;
    }

    private static String getNumberUS(String country, String number, String type) {
        Matcher matcher;

        if (number == null)
            return number;

        String num = number.replaceAll("\\..+$", "")
                .replaceAll("^US\\s?", "")
                .replaceAll(",", "");


        if (type.matches("AN")) {
//            // 200480031274
//            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
//            if (matcher.find())
//                return num.substring(0, 4) + "-" + num.substring(4);

            matcher = Pattern.compile("^(\\d){2}-(\\d){6}$").matcher(num);
            if (matcher.find())
                return num;


            //11/865003
            matcher = Pattern.compile("^(\\d){2}\\/(\\d){6}$").matcher(num);
            if (matcher.find())
                return num.replaceAll("\\/", "-");


            // 10094301
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 2) + "-" + num.substring(2);

            // US18370000345
//            System.out.println(num);
//            System.out.println(getUSSerialCode(num));
            matcher = Pattern.compile("^US(\\d){11}$").matcher(num);
            if (matcher.find())
                return getUSSerialCode(num);

//            US200813128443
            matcher = Pattern.compile("^US(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(6, 8) + "-" + num.substring(8);


        } else if (type.matches("PN|RN")) {

            // 275367
            matcher = Pattern.compile("^(\\d){6}$").matcher(num);
            if (matcher.find())
                return "0" + num;

            // 51130891
            matcher = Pattern.compile("^(\\d){7}$").matcher(num);
            if (matcher.find())
                return num;


            // 151130891
            matcher = Pattern.compile("^1(\\d){7}$").matcher(num);
            if (matcher.find())
                return num;

            // 051130891
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return num.substring(1);

            // 20160034248
            matcher = Pattern.compile("^(19|20)(\\d){2}0(\\d){6}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4);


            // 2004/0153747
            matcher = Pattern.compile("^(\\d){4}/(\\d){7}$").matcher(num);
            if (matcher.find()) {
                return num.replaceFirst("\\/", "-");
            }

            // 2004-153747
            matcher = Pattern.compile("^(\\d){4}-(\\d){6}$").matcher(num);
            if (matcher.find()) {
                return num.substring(0, 5) + String.format("%07d", Integer.parseInt(num.substring(5)));
            }

            // D85019
            matcher = Pattern.compile("^D(\\d){5}$").matcher(num);
            if (matcher.find())
                return "D" + String.format("%06d", Integer.parseInt(num.substring(1)));


        }


        System.out.println("getNumberUS : " + num + " type " + type);

        return number;
    }


    public static String getUSSerialCode(String applicationNumber) {
        if (applicationNumber == null)
            return null;

        String tmp = applicationNumber.replaceAll("(us|US)", "");

        String years = tmp.substring(0, 4);
        String remain = tmp.substring(5);

        if (applicationNumber.contains("a") || applicationNumber.contains("b")) {
            int year = Integer.parseInt(years);
            return getYearToSeries(year) + remain;
        } else if (applicationNumber.contains("u")) {
            return "28" + remain;
        } else if (applicationNumber.contains("s")) {
            return "29" + remain;
        } else if (applicationNumber.contains("e")) {
            return "90" + remain;
        } else {
            int year = Integer.parseInt(years);
            return getYearToSeries(year) + "-" + remain;
        }
    }

    private static String getYearToSeries(int year) {
        if (year < 1948) {
            return "02";
        } else if (year < 1960) {
            return "03";
        } else if (year < 1969) {
            return "04";
        } else if (year < 1979) {
            return "05";
        } else if (year < 1987) {
            return "06";
        } else if (year < 1993) {
            return "07";
        } else if (year < 1998) {
            return "08";
        } else if (year < 2002) {
            return "09";
        } else if (year < 2005) {
            return "10";
        } else if (year < 2008) {
            return "11";
        } else
            return "12";
    }

    private static String getNumberEPO(String country, String number, String type) {
        Matcher matcher;

        if (number.matches("^[0-9]{8}\\.[0-9]$")) {
            type = "AN";
        }

        String num = number.replaceAll("\\.[0-9]$", "")
                .replaceAll("^EP", "")
                .replaceAll("\\s", "");
//        System.out.println(num);


        if (type.matches("AN")) {
            matcher = Pattern.compile("^(19|20)(\\d){2}-(\\d){6}$").matcher(num);
            if (matcher.find())
                return num;

            // 2000-113661.3
            matcher = Pattern.compile("^(19|20)(\\d){2}-(\\d){6}\\.[0-9]$").matcher(num);
            if (matcher.find())
                return num.replaceFirst("\\.[0-9]$", "");

            matcher = Pattern.compile("^(\\d){10}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4);


            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find()) {

                int tmp = Integer.parseInt(num.substring(0, 2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + num.substring(0, 2);
                else
                    year = "20" + num.substring(0, 2);

                return year + "-" + num.substring(2);
            }

//            EP20020080342
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find()) {

                int tmp = Integer.parseInt(num.substring(0, 2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + num.substring(0, 2);
                else
                    year = "20" + num.substring(0, 2);

                return year + "-" + num.substring(2);
            }

            matcher = Pattern.compile("^EP(\\d){11}$").matcher(num);
            if (matcher.find())
                return num.substring(2, 6) + "-" + num.substring(7);


            //            EP19860103508D
            matcher = Pattern.compile("^EP(\\d){11}D$").matcher(num);
            if (matcher.find()) {
                num = num.replaceAll("D", "");
                return num.substring(2, 6) + "-" + num.substring(7);
            }
////            WO2007EP64547
//            matcher = Pattern.compile("^WO(\\d){11}$").matcher(num);
//            if (matcher.find())
//                return num.substring(2, 6) + "-" + num.substring(7);


        } else if (type.matches("PN")) {

            num = number.replaceAll("[\\s|-]", "");

            matcher = Pattern.compile("^(\\d){5,8}$").matcher(num);

            if (matcher.find())
                return String.format("%07d", Integer.parseInt(num));


        }


//        System.out.println("getNumberEPO - number : " + number + ", num :"+  num + " type : " + type + " country : " + country);
        return number;
    }

    private static String getNumberCN(String country, String number, String type) {
        Matcher matcher;
        String num = number.replaceAll("\\..+$", "")
                .replaceAll("[A-z]", "");

        if (type.matches("AN")) {
            // 200480031274
            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4);


            matcher = Pattern.compile("^(\\d){13}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4, 12);


            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find()) {

                int tmp = Integer.parseInt(num.substring(0, 2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + num.substring(0, 2);
                else
                    year = "20" + num.substring(0, 2);

                return year + "-" + num.substring(2, 3) + String.format("%07d", Integer.parseInt(num.substring(3)));
            }
        } else if (type.matches("RN")) {
            // 0031274
            matcher = Pattern.compile("^(\\d){7}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^(\\d){9}$").matcher(num);
            if (matcher.find())
                return num;

            // 10, 201212122
            matcher = Pattern.compile("^(10|20)(\\d){7}$").matcher(num);
            if (matcher.find())
                return num.substring(2);

        }


//        System.out.println("getNumberCN : " +num);

        return number;
    }

    public static String getNumber(String country, String number, String type, String date) {
        if (country.matches("KR"))
            return getNumberKR(number, type);
        else if (country.matches("JP"))
            return getNumberJP(country, number, type, date);
        else if (country.matches("WO"))
            return getNumberPCT(country, number, type);
        else if (country.matches("EP"))
            return getNumberEP(country, number, type, date);
        else if (country.matches("CN"))
            return getNumberCN(number, type);

        return number;
    }

    private static String getNumberJP(String country, String number, String type, String date) {
        Matcher matcher;

        String t_year = "";
        if (date.matches("^(19|20)[0-9]{6}$"))
            t_year = date.substring(0, 4);

        String year = "";
        // 연호 포함
        matcher = Pattern.compile("^(\\d){4,7}$").matcher(number);
        if (matcher.find()) {
            if (Integer.parseInt(number.substring(0, 2)) < eraName.length) {
                year = eraName[Integer.parseInt(number.substring(0, 2))];

                if (year.matches(t_year))
                    return year + "-" + String.format("%06d", Integer.parseInt(number.substring(2)));
            } else {
                if (Integer.parseInt(number.substring(0, 1)) < eraName.length) {
                    year = eraName[Integer.parseInt(number.substring(0, 1))];
                    if (year.matches(t_year))
                        return year + "-" + String.format("%06d", Integer.parseInt(number.substring(1)));
                } else
                    return number;

            }

        }
//        System.out.println("getNumberJP :  " + number + " - " +date);


        return number;

    }

    private static String getNumberEP(String country, String number, String type, String date) {
        Matcher matcher;

        number = number.replaceAll("\\s", "").replaceFirst("EP", "");

        // 09356058
        matcher = Pattern.compile("^(\\d){8}$").matcher(number);
        if (matcher.find())
            return date.substring(0, 4) + "-" + matcher.group().substring(2);

//        09176918.2
        matcher = Pattern.compile("^(\\d){8}\\.(\\d)$").matcher(number);
        if (matcher.find())
            return date.substring(0, 4) + "-" + matcher.group().substring(2, 8);

//        System.out.println(number +" : "+ date);

        return number;
    }

    private static String getNumberPCT(String country, String number, String type) {
        Matcher matcher;

        number = number.replaceAll("WO PCT", "PCT")
                .replaceAll("PCT\\s+/?", "PCT/")
                .replaceAll("-", "/")
                .replaceAll("//", "/")
                .replaceAll("^(PGT|PTC|PUT)", "PCT")
                .replaceAll("^W0", "WO")
                .replaceAll("[^A-z|\\s|/|0-9]", "");

        if (number.matches("^PCT.+"))
            type = "AN";

        String num = number.replaceAll("[^A-z|0-9]|/", "").trim();


        if (type.matches("PN")) {
            num = num.replaceAll("WO", "").trim();

            matcher = Pattern.compile("^(\\d){2}/(\\d){6}$").matcher(num);

            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(0, 2)) > 50) ? "19" + num.substring(0, 2) : "20" + num.substring(0, 2);
                return "WO" + year + "/" + String.format("%06d", Integer.parseInt(num.substring(2, num.length())));
            }

            matcher = Pattern.compile("^(\\d){4}/(\\d){6}$").matcher(num);

            if (matcher.find())
                return "WO" + num;

            // 199834593
            matcher = Pattern.compile("^(\\d){9}$").matcher(num);
            if (matcher.find())
                return "WO" + num.substring(0, 4) + "/" + String.format("%06d", Integer.parseInt(num.substring(4)));

            matcher = Pattern.compile("^(\\d){10}$").matcher(num);
            if (matcher.find())
                return "WO" + num.substring(0, 4) + "/" + num.substring(4);

            // 20040024537
            matcher = Pattern.compile("^(\\d){11}$").matcher(num);
            if (matcher.find())
                return "WO" + num.substring(0, 4) + "/" + num.substring(5);

            matcher = Pattern.compile("^(\\d){6,8}$").matcher(num);

            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(0, 2)) > 50) ? "19" + num.substring(0, 2) : "20" + num.substring(0, 2);
                return "WO" + year + "/" + String.format("%06d", Integer.parseInt(num.substring(2)));
            }


        } else if (type.matches("AN")) {
//            System.out.println(number);

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){6}$").matcher(number);
            if (matcher.find()) {
                return number;
            }

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}(\\d){6}$").matcher(number);
            if (matcher.find()) {
                return number.substring(0, 10) + "/" + String.format("%06d", Integer.parseInt(number.substring(10)));
            }

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){4,5}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 11) + String.format("%06d", Integer.parseInt(number.substring(11)));

            matcher = Pattern.compile("^PCT/[A-z]{2}\\s+(\\d){2}/(\\d){3,6}$").matcher(number);
            if (matcher.find()) {
                String[] temps = number.split("\\s+");
                String year = (Integer.parseInt(temps[1].substring(0, 2)) > 50) ? "19" + temps[1].substring(0, 2) : "20" + temps[1].substring(0, 2);
                return temps[0] + year + "/" + String.format("%06d", Integer.parseInt(temps[1].substring(3)));
            }

//            PCTGB01/02616
            matcher = Pattern.compile("^PCT[A-z]{2}(\\d){2}/(\\d){5,6}$").matcher(number);
            if (matcher.find()) {
                String temp = number.replaceAll("^PCT", "PCT/");
                String year = (Integer.parseInt(temp.substring(6, 8)) > 50) ? "19" + temp.substring(6, 8) : "20" + temp.substring(6, 8);
                return temp.substring(0, 6) + year + "/" + String.format("%06d", Integer.parseInt(temp.substring(9)));
            }

            //PCTEP2009064811
            matcher = Pattern.compile("^PCT[A-z]{2}(\\d){4}(\\d){6}$").matcher(number);
            if (matcher.find()) {
                return number.substring(0, 3) + "/" + number.substring(3, 9) + "/" + number.substring(9);
            }

            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){4}/(\\d){6}$").matcher(number);
            if (matcher.find())
                return number;

//            PCT/US07/000072
            matcher = Pattern.compile("^PCT/[A-z]{2}(\\d){2}/(\\d){5,6}$").matcher(number);
            if (matcher.find()) {
                String year = (Integer.parseInt(number.substring(6, 8)) > 50) ? "19" + number.substring(6, 8) : "20" + number.substring(6, 8);
                return number.substring(0, 6) + year + "/" + String.format("%06d", Integer.parseInt(number.substring(9)));
            }

            matcher = Pattern.compile("^[A-z]{2}(\\d){7,8}$").matcher(num);
            if (matcher.find()) {
                String year = (Integer.parseInt(num.substring(2, 4)) > 50) ? "19" + num.substring(2, 4) : "20" + num.substring(2, 4);
                return "PCT/" + num.substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(num.substring(4)));
            }

            matcher = Pattern.compile("^[A-z]{2}(\\d){10}$").matcher(num);
            if (matcher.find()) {
                return "PCT/" + num.substring(0, 6) + "/" + String.format("%06d", Integer.parseInt(num.substring(6)));
            }

            //WO2007AT00486 WO2007AT004865    -> PCT/AT2007/000486
            matcher = Pattern.compile("^WO(\\d){4}[A-z]{2}(\\d){5,6}$").matcher(num);
            if (matcher.find()) {
                return "PCT/" + num.substring(6, 8) + num.substring(2, 6) + "/" + String.format("%06d", Integer.parseInt(num.substring(8)));
            }

            // WO2018CN119519
            matcher = Pattern.compile("^WO(19|20)[0-9]{2}[A-z]{2}[0-9]{4,6}$").matcher(number);
            if (matcher.find()) {
                return "PCT/" + number.substring(6, 8) + number.substring(2, 6) + "/" + String.format("%06d", Integer.parseInt(number.substring(8)));
            }
        }

//        System.out.println("getNumberPCT - number : " + number + ", num :"+  num + " type : " + type + " country : " + country);


        return number;
    }

    private static String getNumberJP(String country, String number, String type) {
        String num = number
                .replaceAll("JP-P-", "")
                .replaceAll("JP", "")
                .replaceAll("/", "-")
                .replaceAll("A", "");

        Matcher matcher;
//        if (type.matches("U|B[0-9]?"))
//            return number;

//        System.out.println("getNumberJP :" + number );

        //6168330
        matcher = Pattern.compile("^(\\d){7}$").matcher(number);
        if (matcher.find())
            return matcher.group();

        //2006-072868
        matcher = Pattern.compile("^(\\d){4}-(\\d){6}$").matcher(number);
        if (matcher.find())
            return matcher.group();

        //2006-72868
        matcher = Pattern.compile("^(\\d){4}-(\\d){2,5}$").matcher(number);
        if (matcher.find())
            return matcher.group().substring(0, 5) + String.format("%06d", Integer.parseInt(matcher.group().substring(5, matcher.group().length())));

        // 10-023061
        matcher = Pattern.compile("^(\\d){1,2}[-| ](\\d)+$").matcher(num);
        if (matcher.find()) {
            String[] results = matcher.group().split("(-| )");
            if (Integer.parseInt(results[0]) >= eraName.length)
                return number;
            return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
        }

        if (type.matches("RN")) {
            matcher = Pattern.compile("(\\d){7}").matcher(number);
            if (matcher.find())
                return matcher.group();

            matcher = Pattern.compile("(\\d){3,6}").matcher(number);
            if (matcher.find())
                return String.format("%07d", Integer.parseInt(number));

        } else if (type.matches("AN|PN|ON")) {

            // 연호 미포함
            matcher = Pattern.compile("^(19|20)(\\d){2}-(\\d){6}$").matcher(num);
            if (matcher.find())
                return num;

            // 연호 미포함
            matcher = Pattern.compile("^(19|20)(\\d){8}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4);

            // 연호 미포함
            matcher = Pattern.compile("^(19|20)(\\d){8}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4);

            // 연호 포함
            matcher = Pattern.compile("^(\\d){8}$").matcher(number);
            if (matcher.find()) {
                if (Integer.parseInt(number.substring(0, 2)) >= eraName.length) {

                    return number;
                }

                return eraName[Integer.parseInt(number.substring(0, 2))] + "-" + number.substring(2);
            }

            // 연호 포함
            matcher = Pattern.compile("^0(\\d){6}$").matcher(number);
            if (matcher.find()) {
                if (Integer.parseInt(number.substring(0, 2)) >= eraName.length)
                    return number;

                return eraName[Integer.parseInt(number.substring(0, 2))] + "-" + String.format("%06d", Integer.parseInt(number.substring(2)));
            }

            // 연호 포함
            matcher = Pattern.compile("^(\\d){7}$").matcher(number);
            if (matcher.find()) {
                number = "0" + number;

                if (Integer.parseInt(number.substring(0, 2)) >= eraName.length) {
                    return number.substring(1);
                }

                return eraName[Integer.parseInt(number.substring(0, 2))] + "-" + number.substring(2);
            }

//            JP19710000001U ,  JP19720000001
            matcher = Pattern.compile("^JP(\\d){11}U?").matcher(number);
            if (matcher.find()) {
                return number.substring(2, 6) + "-" + number.substring(7, 13);
            }

            // S
            matcher = Pattern.compile("^[H|S](\\d){3,8}$").matcher(number);
            if (matcher.find()) {
                if (Integer.parseInt(number.substring(1, 3)) >= eraName.length)
                    return number;

                return eraName[Integer.parseInt(number.substring(1, 3))] + "-" + String.format("%06d", Integer.parseInt(number.substring(3)));
            }

            //P2011-218677
            matcher = Pattern.compile("^P(19|20)[0-9]{2}-[0-9]{6}$").matcher(number);
            if (matcher.find())
                return number.substring(1);

            // P 2001-042115, P.2001-034187
            matcher = Pattern.compile("^P( |\\.)(19|20)[0-9]{2}-[0-9]{6}$").matcher(number);
            if (matcher.find())
                return number.substring(2);

            // JP 2007-026728
            matcher = Pattern.compile("^JP( |\\.)(19|20)[0-9]{2}-[0-9]{6}$").matcher(number);
            if (matcher.find())
                return number.replaceAll("^JP ","");

            // 2007 026728
            matcher = Pattern.compile("^(19|20)[0-9]{2} [0-9]{6}$").matcher(number);
            if (matcher.find())
                return number.replaceAll(" ","-");


            // JP-P-2004-00269319
            matcher = Pattern.compile("^JP-P-(19|20)[0-9]{2}-00[0-9]{6}$").matcher(number);
            if (matcher.find())
                return number.substring(5, 9) + "-" + number.substring(12);


        }


        System.out.println(" getNumberJP  -  " + number);
        return number;
    }


    // AN, PN, RN, ON
    private static String getNumberKR(String number, String type) {
        number = number.replaceAll("[^0-9|-|\\-]", "").trim();
        Matcher matcher;

        if (type.matches("ON|AN")) {
//            1019980065165
            matcher = Pattern.compile("^(10|20|30)(19|20)[0-9]{9}").matcher(number);
            if (matcher.find())
                return number.substring(0,2) + "-" + number.substring(2, 6) + "-" + number.substring(6);

            matcher = Pattern.compile("^(10|20|30)-(19|20)(\\d){2}-(\\d){7}$").matcher(number);
            if (matcher.find())
                return number;

            matcher = Pattern.compile("^KR(\\d){11}A?$").matcher(number);
            if (matcher.find())
                return number.substring(2, 6) + "-" + number.substring(6);

            //KR1997 10 0000983
            matcher = Pattern.compile("^KR(\\d){4}10(\\d){7}A?$").matcher(number);
            if (matcher.find())
                return number.substring(2, 6) + "-" + number.substring(8);

//            20140178539
            matcher = Pattern.compile("^(\\d){11}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4);

            //2015-050260,2015-0005621
            matcher = Pattern.compile("^(\\d){4}-(\\d){6,7}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + String.format("%07d", Integer.parseInt(number.substring(5)));


        } else if (type.matches("PN")) {
//            1004730350000
            matcher = Pattern.compile("^(10|20|30)[0-9]{7}0{4}$").matcher(number);
            if (matcher.find())
                return number.substring(2, 9);

//                        1019980065165
            matcher = Pattern.compile("^(10|20|30)(19|20)[0-9]{8,9}$").matcher(number);
            if (matcher.find())
                return number.substring(2, 6) + "-" + String.format("%07d", Integer.parseInt(number.substring(6)));

            // 20040037369, 2004009097
            matcher = Pattern.compile("^[0-9]{10,11}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + String.format("%07d", Integer.parseInt(number.substring(4)));

//            000100831877
            matcher = Pattern.compile("^000(10|20|30)[0-9]{7}$").matcher(number);
            if (matcher.find())
                return number.substring(5);

//            900006010
            matcher = Pattern.compile("^[8-9](\\d){8}$").matcher(number);
            if (matcher.find())
                return "19" + number.substring(0, 2) + "-" + number.substring(2);

//            102015044590
            matcher = Pattern.compile("^[8-9](\\d){8}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 2) + "-" + number.substring(2);


        } else if (type.matches("RN")) {
//            10-0377477
            matcher = Pattern.compile("^(10|20|30)-[0-9]{7}$").matcher(number);
            if (matcher.find())
                return number.substring(3, 10);

            // 260300
            matcher = Pattern.compile("^[0-9]{6}$").matcher(number);
            if (matcher.find())
                return String.format("%07d", Integer.parseInt(number));

            matcher = Pattern.compile("^(10|20|30)-[0-9]{6}$").matcher(number);
            if (matcher.find())
                return String.format("%07d", Integer.parseInt(number.substring(3, 9)));


            // 10-01002192
            matcher = Pattern.compile("^(10|20|30)-[0-9]{8}$").matcher(number);
            if (matcher.find())
                return number.substring(4, 11);


            // 10-01002192
            matcher = Pattern.compile("^(10|20|30)-[0-9]{7}-0000$").matcher(number);
            if (matcher.find())
                return number.substring(3, 10);

            // 10010021920000
            matcher = Pattern.compile("^(10|20|30)[0-9]{7}0000$").matcher(number);
            if (matcher.find())
                return number.substring(2, 9);

            matcher = Pattern.compile("^(\\d){7}$").matcher(number);
            if (matcher.find())
                return number;
        }

        // 연호 미포함
//        matcher = Pattern.compile("^(JP)?(\\d){4}(-)?(\\d){6}((A|B|U|S|Y)(\\d)?)?$|^(JP)?((\\d){7})((A|B|U|S|Y)(\\d)?)?$").matcher(number);
//        if (matcher.find()) {
//            return matcher.group().replace("JP", "");
//        }


        System.out.println("getNumberKR : " + number + " type : " + type);
        return number;
    }


    public static String getNumberJP(String number, String type) {
        Matcher matcher;
        matcher = Pattern.compile("^JP(\\d){10}$").matcher(number);
        if (matcher.find())
            return "PCT/" + number.substring(0, 6) + "/" + number.substring(6);

        matcher = Pattern.compile("^WO(\\d){10}$").matcher(number);
        if (matcher.find())
            return number.substring(0, 6) + "/" + number.substring(6);


        String num = number.replaceAll("[^0-9]", "");

        if (type.matches("RN")) {
            matcher = Pattern.compile("(\\d){7}").matcher(num);
            if (matcher.find())
                return matcher.group();

        } else if (type.matches("AN|PN|ON")) {
            // 연호 미포함
            matcher = Pattern.compile("^(\\d){10}$").matcher(number);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4, number.length());

            // 연호 포함
            matcher = Pattern.compile("^(\\d){8}$").matcher(number);
            if (matcher.find())
                return eraName[Integer.parseInt(matcher.group().substring(0, 2))] + "-" + matcher.group().substring(2, matcher.group().length());
        }
        return number;
    }

    public static String getTxtNumberJP(String number, String type) {
        Matcher matcher;
//        System.out.println(number);


        if (type.matches("IAN")) {
//            PCT/JP94/01409
            matcher = Pattern.compile("^PCT/(\\w){2}(\\d){2,4}/(\\d){5,6}").matcher(number);
            if (matcher.find()) {
                String[] arrs = number.split("/");

                int tmp = Integer.parseInt(arrs[1].substring(2));
                String year = "";
                if (tmp >= 50)
                    year = "19" + arrs[1].substring(2);
                else
                    year = "20" + arrs[1].substring(2);

                return arrs[0] + "/" + arrs[1].substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(arrs[2]));
            }
            System.out.println("getTxtNumberJP : " + number);
        } else if (type.matches("IPN")) {
//ＷＯ９５／０６０３２
            matcher = Pattern.compile("^WO(\\d){2,4}/(\\d){5,6}").matcher(number);
            if (matcher.find()) {
                String[] arrs = number.split("/");
                int tmp = Integer.parseInt(arrs[0].substring(2));
                String year = String.format("%02d", tmp);

                if (tmp >= 50)
                    year = "19" + year;
                else
                    year = "20" + year;

                return arrs[0].substring(0, 2) + year + "/" + String.format("%06d", Integer.parseInt(arrs[1]));
            }
            System.out.println("getTxtNumberJP : " + number);

        }

        String num = number.replaceAll("\\(.*\\)", "");

        num = num.replaceAll("[^0-9|−|-]", "")
                .replaceAll("−", "-");

        if (type.matches("RN")) {
            matcher = Pattern.compile("(\\d){7}").matcher(number);
            if (matcher.find())
                return matcher.group();

            matcher = Pattern.compile("(\\d){7}").matcher(num);
            if (matcher.find())
                return matcher.group();


            // 10-023061, 1-254521
            matcher = Pattern.compile("^(\\d){1,2}[-|−](\\d)+$").matcher(num);
            if (matcher.find()) {
                String[] results = matcher.group().split("[-|−]");
                return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
            }


        } else if (type.matches("AN|PN|ON")) {
            //特開２００３－２６０３４５（Ｐ２００３－２６０３４５Ａ）
            //2006-072868
            matcher = Pattern.compile("^(\\d){4}[-|−](\\d){6}$").matcher(num);
            if (matcher.find())
                return matcher.group();

            //2006-72868
            matcher = Pattern.compile("^(\\d){4}[-|−](\\d)+$").matcher(num);
            if (matcher.find())
                return matcher.group().substring(0, 5) + String.format("%06d", Integer.parseInt(matcher.group().substring(5, matcher.group().length())));

            // 10-023061, 1-254521
            matcher = Pattern.compile("^(\\d){1,2}[-|−](\\d)+$").matcher(num);
            if (matcher.find()) {
                String[] results = matcher.group().split("[-|−]");
                return eraName[Integer.parseInt(results[0])] + "-" + String.format("%06d", Integer.parseInt(results[1]));
            }

            // 연호 미포함
            matcher = Pattern.compile("^(\\d){10}$").matcher(num);
            if (matcher.find())
                return number.substring(0, 4) + "-" + number.substring(4, number.length());

            // 연호 포함
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return eraName[Integer.parseInt(matcher.group().substring(0, 2))] + "-" + matcher.group().substring(2, matcher.group().length());
        }

        return number;
    }


    public static String getNumberCN(String number, String type) {
//        System.out.println(number);
//        2015 3 0028611
//        2012 1 0225317
//        2009 1 0055719
//        2007 2 0195884 U

        Matcher matcher;
        //00100023
        String num = number.replaceAll("\\.[0-9]$", "")
                .replaceAll("[^0-9]", "");
        String year = "";

        if (type.matches("AN")) {
            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find()) {
                if (Integer.parseInt(num.substring(0, 2)) < 50)
                    year = "20" + num.substring(0, 2);
                else
                    year = "19" + num.substring(0, 2);

                return year + "-" + num.charAt(2) + String.format("%07d", Integer.parseInt(num.substring(3, num.length())));
            }

            // 201520167566.3
            matcher = Pattern.compile("^(\\d){12}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 4) + "-" + num.substring(4, num.length());


            //202015000167566
            matcher = Pattern.compile("^(\\d){15}$").matcher(num);
            if (matcher.find())
                return num.substring(2, 6) + "-" + num.substring(0, 1) + num.substring(8);

            //WO2017JP30541
            matcher = Pattern.compile("^(\\d){15}$").matcher(num);
            if (matcher.find())
                return num.substring(2, 6) + "-" + num.substring(0, 1) + num.substring(8);


        } else if (type.matches("(RN|PN)")) {
            matcher = Pattern.compile("^(\\d){9}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^[1-2](\\d){6}$").matcher(num);
            if (matcher.find())
                return num;

            matcher = Pattern.compile("^(\\d){8}$").matcher(num);
            if (matcher.find())
                return num.substring(0, 1) + String.format("%08d", Integer.parseInt(num.substring(2)));

        }

        return number;
    }

    public static String getDocdbApplicationNumber(String countryCode, String applicationNumber, String applicationDate, String type) {
        String tail = "";
        if (countryCode.matches("US")) {
            if (type.matches("^S[0-9]?$"))
                tail = "F";
            return countryCode + applicationDate.substring(0, 4) + applicationNumber.replaceAll("-", "") + tail;
        } else if (countryCode.matches("KR")) {
            if (type.matches("^[U|Y][0-9]?$"))
                tail = "U";

            return countryCode + applicationNumber.replaceAll("-", "") + tail;
        } else if (countryCode.matches("(JP|EP)")) {
            if (type.matches("^[U|Y][0-9]?$"))
                tail = "U";

            String[] parts = applicationNumber.split("-");
            if (parts.length == 2)
                return countryCode + parts[0] +"0"+ parts[1] + tail;
            else
                return countryCode + applicationNumber.replaceAll("-", "") + tail;

        } else if (countryCode.matches("(WO)")) {
            if (applicationNumber.matches("^PCT/[A-z]{2}(19|20)[0-9]{2}/0[0-9]{5}$"))
                return countryCode + applicationNumber.substring(6,10) + applicationNumber.substring(4,6) + applicationNumber.substring(12);
            else if (applicationNumber.matches("^PCT/[A-z]{2}(19|20)[0-9]{2}/[1-9][0-9]{5}$"))
                return countryCode + applicationNumber.substring(6,10) + applicationNumber.substring(4,6) + applicationNumber.substring(11);

        } else if (countryCode.matches("(DE)")) {
            if (type.matches("^[U|Y][0-9]?$"))
                tail = "U";

            String number = applicationNumber.replaceAll("-","");

            if (number.matches(""))
                return countryCode + applicationNumber.substring(6,10) + applicationNumber.substring(4,6) + applicationNumber.substring(12) +tail;

        }

        if (countryCode.matches("(EP|US|JP|KR|DE|WO|CN)"))
            System.out.println("getDocdbApplicationNumber : " + countryCode + "  " + applicationNumber);

        return null;
    }

    public static Set<String> checkNumber(String country, Set<String> appNums, String type) {
        if (country.matches("US"))
            return checkNumberUS(appNums, type);

        return appNums;
    }

    private static Set<String> checkNumberUS(Set<String> appNums, String type) {
        if (appNums.isEmpty())
            return appNums;

        Matcher matcher;

        if (type.matches("AN")) {
            Set<String> numSet = new HashSet<>();
            for (String number : appNums){
                matcher = Pattern.compile("^(\\d){8}$").matcher(number);
                if (matcher.find())
                    numSet.add(number);

                matcher = Pattern.compile("^(\\d){2}-(\\d){6}$").matcher(number);
                if (matcher.find()) {
                    numSet.add(number);
                    numSet.add(number.replaceAll("-",""));
                }
            }

            if (!numSet.isEmpty())
                return numSet;
        }

        return appNums;
    }
}