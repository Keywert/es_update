package es.module.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PriorityUtil {

    public static String countryNormalize(String str) {
        if (str == null)
            return null;

        /// 대한민국(KR)
         Matcher matcher = Pattern.compile("\\([A-z]{2}\\)").matcher(str);
         if(matcher.find())
             return matcher.group().replaceAll("\\(|\\)","");


        return str;
    }
}
