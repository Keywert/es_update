package es.module.utility;

import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MakeDocID {


    public static List<String> getDocIds(String index, String applicationNumber, String documentType) {

        if (index == null || "".equals(index))
            return null;


        if (index.matches("jpo"))
            return jpoDocIds(applicationNumber,documentType);

        if (index.matches("epo"))
            return epoDocIds(applicationNumber);

        if (index.matches("kipo"))
            return kipoDocIds(applicationNumber);


        return null;

    }

    private static List<String> kipoDocIds(String applicationNumber) {
        List<String> docIds = new ArrayList<>();

        if (applicationNumber.matches("^(10|20)[0-9]{4}07[0-9]{5}$"))
            applicationNumber = applicationNumber.substring(0, 6) + "70" + applicationNumber.substring(8);

        if (applicationNumber.matches("^10.+")) {
            docIds.add("kr" + applicationNumber.substring(2) + "a");
            docIds.add("kr" + applicationNumber.substring(2) + "b1");
        } else if (applicationNumber.matches("^20.+")) {
            docIds.add("kr" + applicationNumber.substring(2) + "u");
            docIds.add("kr" + applicationNumber.substring(2) + "y1");
        } else
            return null;


        return docIds;
    }

    private static List<String> epoDocIds(String applicationNumber) {
        String[] docTypes = { "A1", "B1", "A2", "A3", "B2", "B8", "B9", "A8", "A9", "B3"};
        List<String> docIds = new ArrayList<>();

        for (String docType : docTypes) {
            String docId = "EP" + applicationNumber + docType;

            docId = docId.replaceAll("-","").toLowerCase();

            docIds.add(docId);
        }

        return docIds;
    }

    private static List<String> jpoDocIds(String applicationNumber, String documentType) {
        List<String> docTypes = new ArrayList<>();

        String[] pType = {"A","A1","B","B1","B2"};
        String[] uType = {"U","Y2","Y1"};

        if (documentType.matches("^(P|A|B)[0-9]?$"))
            docTypes.addAll(Arrays.asList(pType));
        else if (documentType.matches("^(U|Y)[0-9]?$"))
            docTypes.addAll(Arrays.asList(uType));
        else
            return null;


        List<String> docIds = new ArrayList<>();

        for (String docType : docTypes) {
            String docId = "JP" + applicationNumber + docType;

            docId = docId.replaceAll("-","").toLowerCase();

            docIds.add(docId);
        }

        return docIds;
    }

    public static List<String> getDocumentIds(ESConfig esConfig, String index, String appNum) {
        if (appNum == null)
            return null;

        List<String> docIds = getDocIds(index,appNum,"Type");
        if (docIds.isEmpty())
            return null;

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(docIds);

        String[] fields = {"documentId"};

        SearchResponse response = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        List<String> documentIds = new ArrayList<>();
        for (SearchHit hit : response.getHits().getHits())
            documentIds.add(hit.getId());

        if (!documentIds.isEmpty())
            return documentIds;

        return null;
    }
}
