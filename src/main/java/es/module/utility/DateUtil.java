package es.module.utility;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    static SimpleDateFormat basicFormat = new SimpleDateFormat("yyyyMMdd");
    static SimpleDateFormat commaFormat = new SimpleDateFormat("yyyy.MM.dd");

    public static String replaceDashToComma(String date) {
        if (StringUtils.isEmpty(date))
            return date;

        return date.replaceAll("(-|\\.)", "");
    }

    public static String getCurrentStringTime() {
        return basicFormat.format(new Date());
    }

    public static Integer getCurrentIntegerTime() {
        return Integer.valueOf(basicFormat.format(new Date()));
    }


    public static Timestamp convertTimestampByString(String strDate, String format) {
        try {
            Date date = new SimpleDateFormat(format).parse(strDate);
            return new Timestamp(date.getTime());
        } catch (Exception var5) {
            return null;
        }
    }


    private Date convertTimestampToDate(long timestamp) {
        Timestamp timeStamp = new Timestamp(timestamp);
        java.sql.Date date = new java.sql.Date(timeStamp.getTime());
        return date;
    }

    public static Integer convertDateToInt(String date) {
        if(StringUtils.isEmpty(date))
            return null;


        return Integer.parseInt(replaceDashToComma(date));
    }

    public static String convertDateToString(String value) {
        if (StringUtils.isEmpty(value))
            return null;

        try {
            Date date = basicFormat.parse(replaceDashToComma(value));
            return commaFormat.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static String convertDateToStringBasicFormat(Date date) {

        try {
            return commaFormat.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean checkCompareDate(String applicationDate, int compareDate) {
        if(StringUtils.isEmpty(applicationDate) || compareDate == 0)
            return false;

        return Integer.parseInt(replaceDashToComma(applicationDate)) < compareDate;
    }

    public static boolean validateDateCheck(String date) {
        if(StringUtils.isEmpty(date))
            return false;

        date = replaceDashToComma(date);

        if(date.length() >= 8)
            return false;

        return true;

    }

    public static DateTime convertDateTimeByString(String date) {
        if(StringUtils.isEmpty(date))
            return null;

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        return formatter.parseDateTime(replaceDashToComma(date));
    }
}
