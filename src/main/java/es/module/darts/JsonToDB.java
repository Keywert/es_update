package es.module.darts;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.module.darts.model.DartIpVO;
import es.module.darts.model.DocumentVO;
import es.module.darts.model.Patent;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonToDB {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage : java -jar json.jar [Input Paths]");
            return;
        }

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        ArrayList<String> files = getFilepathList(args[0]);
        ObjectMapper objectMapper = new ObjectMapper();

        String tableName = "dartip.litigation";

        try {
            for (String filePath : files) {
                System.out.println(filePath);

                DartIpVO dartIpVO = objectMapper.readValue(new File(filePath), DartIpVO.class);

                if (dartIpVO != null)
                    insertDB(conn, tableName, dartIpVO);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void insertDB(Connection conn, String tableName, DartIpVO dartIpVO) {
//        String insertQuery = "INSERT ignore into  " + tableName +
        String insertQuery = "INSERT into  " + tableName +
                "(country_code ,application_number ,publication_number ,documentType ,docdbFamilyId,inpadocFamily ,hasLitigation ,dartsUrl ,firstActionTypes) " +
                " values(?,?,?,?,?,?,?,?,?) " +
                " on duplicate key update " +
                " firstActionTypes = values(firstActionTypes)";

        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            List<Patent> patentList = dartIpVO.getPatents();
            Matcher matcher;

            int count = 0;
            System.out.println(patentList.size());

            for (Patent patent : patentList) {
//                System.out.println(new ObjectMapper().writeValueAsString(patent));

                DocumentVO application = patent.getApplication();
                if (application == null)
                    continue;

                List<DocumentVO> publications = patent.getPublications();
                if (publications == null)
                    continue;

                for (DocumentVO publication : publications) {
                    String dartUrl = patent.getDartsUrl();
                    String docdbFamilyId = "";
                    if (dartUrl != null && !"".equals(dartUrl) ) {
//                        System.out.println(dartUrl);
                        matcher = Pattern.compile("family=(\\d)+").matcher(dartUrl);
                        if (matcher.find()) {
//                            System.out.println(matcher.group());
                            docdbFamilyId = matcher.group().split("=")[1];
//                            System.out.println(docdbFamilyId);

                        }
                    }

                    statement.setString(1, (application.getAuthority() == null) ? null : application.getAuthority());
                    statement.setString(2, (application.getNumber() == null) ? null : application.getNumber());
                    statement.setString(3, (publication.getNumber() == null) ? null : publication.getNumber());
                    statement.setString(4, (publication.getKind() == null) ? null : publication.getKind());
                    statement.setString(5, docdbFamilyId);
                    statement.setString(6, (patent.getInpadocFamily() == null) ? null : patent.getInpadocFamily());
                    statement.setBoolean(7, true);
                    statement.setString(8, (patent.getDartsUrl() == null) ? null : patent.getDartsUrl());
                    statement.setString(9, (patent.getFirstActionTypes() == null) ? null : String.join("|",patent.getFirstActionTypes()));

                    statement.addBatch();

                    if (++count % 5000 == 0) {
                        statement.executeBatch();
                        statement.clearBatch();
                        System.out.println(count);
                    }

                }


            }

            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList getFilepathList(String filepath) {
        ArrayList arrayList = new ArrayList();

        File dir = new File(filepath);

        if (dir.exists() == false) {
            System.out.println("Not exist File Path.");
            return null;
        }

        visitDir(arrayList, dir);

        return arrayList;
    }
    public static void visitDir(ArrayList arrayList, File file) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles) {
                //    System.out.println("f :" + f.toString());
                visitDir(arrayList, f);
            }
        } else {
            if (file.getName().matches(".+\\.(txt|json|js)")) {
                arrayList.add(file.getAbsolutePath());
            }
        }
    }
}
