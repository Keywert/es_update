package es.module.darts.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ObjectDeseralizer  extends JsonDeserializer {
    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode node = p.readValueAsTree();
        List<String> lists = new ArrayList<>();
        if (node instanceof ArrayNode) {
            String[] objects = new ObjectMapper().readValue(node.toString(), String[].class);
            lists.addAll(Arrays.asList(objects));
        } else if (node instanceof JsonNode) {
            String fterm = new ObjectMapper().readValue(node.toString(), String.class);
            lists.add(fterm);
        }

        if (lists.isEmpty())
            return null;

        return lists;
    }
}
