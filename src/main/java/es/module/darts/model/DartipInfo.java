package es.module.darts.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DartipInfo {
    public boolean hasLitigation;

    public String dartsUrl;

    public List<String> getFirstActionTypes() {
        if (firstActionTypes == null) {
            List<String> list = new ArrayList<>();
            list.add("UN");
        }
        return firstActionTypes;
    }

    @JsonDeserialize(using = ObjectDeseralizer.class)
    public List<String> firstActionTypes;

    public String docdbFamilyId;
    public String inpadocFamilyId;
}
