package es.module.darts.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Patent {
    public DocumentVO application;
    public List<DocumentVO> publications;
    public String inpadocFamily;
    public boolean hasLitigation;
    public String dartsUrl;
    public String docdbFamilyId;
    public List<String> firstActionTypes;

    public boolean getHasLitigation() {
        return hasLitigation;
    }
}
