//package es.module.darts;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import es.config.DBHelper;
//import es.config.ESConfig;
//import es.model.v1.CountryEnum;
//import es.module.darts.model.DartipInfo;
//import es.module.datafarm.Queue2Data;
//import es.module.utility.NumberUtils;
//import org.elasticsearch.action.update.UpdateRequest;
//
//import java.sql.*;
//import java.util.*;
//import java.util.concurrent.TimeUnit;
//
//public class DartsIpUpdate {
//    public List<Queue2Data> updateES(String esIndex){
//        DBHelper dbHelper = new DBHelper();
//        Connection conn = dbHelper.getConn_dartsip();
//        String tableName = "litigation_base";
//        String esName = "es206";
//        String searchEsName = "esTest";
//        ESConfig esConfig = new ESConfig(esName);
//        ESConfig searchEsConfig = new ESConfig(searchEsName);
//        String task_country_code = CountryEnum.getEnumByIndex(esIndex).getDesc();
//
//        //String query = "select * from " + tableName + " where status is null";
//        String query = "select * from " + tableName + " where status is null and country_code = '" + task_country_code + "'";
//        //String query = "select * from " + tableName + " where status = 4 and task_country_code = '" + countryCode + "'";
//
//
//        System.out.println(query);
//
//        Statement st = null;
//        List<Queue2Data> queuing_docid = new ArrayList<>();
//
//        try {
//            st = conn.createStatement();
//            st.setFetchSize(100);
//            ResultSet rs = st.executeQuery(query);
//
//            ObjectMapper objectMapper = new ObjectMapper();
//
//            int count = 0;
//            while (rs.next()) {
//                if (++count % 1000 == 0)
//                    System.out.println(count);
//
//                Map<String, String> map = new HashMap<>();
//
//                String country_code = rs.getString("country_code");
//                if (country_code == null)
//                    continue;
//
//
//                String index = CountryEnum.valueOf(country_code).getIndex();
//                if (index == null && "docdb".equals(index))
//                    continue;
//
//                map.put("country_code", country_code);
//
//                String mappingId = rs.getString("mappingId");
//                if (mappingId != null)
//                    map.put("mappingId", mappingId);
//
//                String documentType = rs.getString("documentType");
//                if (documentType != null)
//                    map.put("documentType", documentType);
//
//                String application_number = rs.getString("application_number");
//                if (application_number != null)
//                    map.put("application_number", application_number);
//
////                String application_number_norm = rs.getString("application_number_norm");
////                if (application_number_norm != null)
////                    map.put("application_number_norm", application_number_norm);
//
//                String publication_number = rs.getString("publication_number");
//                if (publication_number != null)
//                    map.put("publication_number", publication_number);
//
////                String publication_number_norm = rs.getString("publication_number_norm");
////                if (publication_number_norm != null)
////                    map.put("publication_number_norm", publication_number_norm);
//
//
//                // get documentId
//                List<String> docIds = NumberUtils.getDocIds(searchEsConfig, index, map);
//                if (docIds == null) {
//                    updateDB(conn, tableName, country_code, application_number, publication_number, 4);
//                    System.out.println(country_code);
//                    continue;
//                }
//
//                DartipInfo dartipInfo = new DartipInfo();
//                String firstActionTypes = rs.getString("firstActionTypes");
//                if (firstActionTypes != null) {
//                    String[] firstTypes = firstActionTypes.split("\\|");
//                    if (firstTypes.length > 0)
//                        dartipInfo.setFirstActionTypes(Arrays.asList(firstTypes));
//                }
//
//                dartipInfo.setDartsUrl(rs.getString("dartsUrl"));
//                dartipInfo.setDocdbFamilyId(rs.getString("docdbFamilyId"));
//                dartipInfo.setInpadocFamilyId(rs.getString("inpadocFamily"));
//                dartipInfo.setHasLitigation(true);
//
//                Map<String, DartipInfo> dartipMap = new HashMap<>();
//
//                dartipMap.put("dartipInfo", dartipInfo);
//
//                String json = objectMapper.writeValueAsString(dartipMap);
//
//                for (String docId : docIds) {
//                    String destIndex = index;
//                    if (docId.matches("^[A-Z]{2}.+"))
//                        destIndex = "docdb";
//                    else if (docId.matches("^usps.+") && index == "uspto")
//                        destIndex = "uspto_past";
//                    else if (docId.matches("^jpps.+") && index == "jpo")
//                        destIndex = "jpo_past";
//                    queuing_docid.add(new Queue2Data(docId,5,destIndex));
//
//                    System.out.println(destIndex + " - " + docId + " : " + json);
//
//                    esConfig.bulkProcessor.add(new UpdateRequest(destIndex, "patent", docId)
//                            .doc(json.getBytes())
//                            .upsert(json.getBytes()));
//                    insertDocID(conn, "litigation_base_docid",mappingId, destIndex, docId);
//                }
//                updateDB(conn, tableName, country_code, application_number, publication_number, 2);
//            }
//
//            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        return queuing_docid;
//    }
//    private static void updateDB(Connection conn, String tableName, String country_code, String application_number, String publication_number, int i) {
//        String query = "UPDATE " + tableName + " SET status = " + i +
//                " WHERE country_code = '" + country_code + "'" +
//                " and application_number = '" + application_number + "'" +
//                " and publication_number = '" + publication_number + "'";
//
//        System.out.println(query);
//        PreparedStatement preparedStatement = null;
//
//        try {
//            preparedStatement = conn.prepareStatement(query);
//            preparedStatement.clearParameters();
//
//
//            preparedStatement.executeUpdate();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//    private static void insertDocID(Connection conn, String tableName, String mappingId, String index_name, String docId) {
//        String query = "INSERT INTO " + tableName + "(mappingId,index_name,documentId)" +
//                " VALUES ('" + mappingId + "','" +  index_name + "','" + docId + "')";
//        System.out.println(query);
//        PreparedStatement preparedStatement = null;
//
//        try {
//            preparedStatement = conn.prepareStatement(query);
//            preparedStatement.clearParameters();
//
//
//            preparedStatement.executeUpdate();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//}
