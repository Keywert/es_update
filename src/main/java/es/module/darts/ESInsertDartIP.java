package es.module.darts;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.v1.CountryEnum;
import es.module.darts.model.DartipInfo;
import es.module.utility.NumberUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ESInsertDartIP {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        List<ESConfig> esConfigs = new ArrayList<>();

        esConfigs.add(new ESConfig("es51"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es189"));
        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es218"));
        esConfigs.add(new ESConfig("es18"));

        if (esConfigs.isEmpty())
            return;

        String countryCode = "US";

        String tableName = "dartip.litigation_copy";
        //String query = "select * from " + tableName + " where status is null and country_code not in ('JP') and publication_number regexp '^(19|20)[0-9]{2}[0-9]{6}$' limit 1000";
        String query = "select * from " + tableName + " where status is null and country_code = '" + countryCode + "'";

//        String query = "select * from " + tableName + " where status is null and country_code not in ('JP') and application_number regexp  '^(20)[0-2][0-9]{9}$' limit 1000";
//        String query = "select * from " + tableName + " where status is null and country_code = '" + country + "' and application_number = '2006US31653'  limit 100";

//        String query = "select * from " + tableName + " where application_number = '200913127062'";
//        String query = "select * from " + tableName + " where country_code in ('WO') and status is null and application_number regexp '^(19|20)[0-9]{2}[0-9]{8}$' limit 1051";


        System.out.println(query);

        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            ObjectMapper objectMapper = new ObjectMapper();

            int count = 0;
            while (rs.next()) {
                if (++count % 1000 == 0)
                    System.out.println(count);

                Map<String, String> map = new HashMap<>();

                String country_code = rs.getString("country_code");
                if (country_code == null)
                    continue;


                String index = CountryEnum.valueOf(country_code).getIndex();
                if (index == null && "docdb".equals(index))
                    continue;

                map.put("country_code", country_code);

                String documentType = rs.getString("documentType");
                if (documentType != null)
                    map.put("documentType", documentType);

                String application_number = rs.getString("application_number");
                if (application_number != null)
                    map.put("application_number", application_number);

                String application_number_norm = rs.getString("application_number_norm");
                if (application_number_norm != null)
                    map.put("application_number_norm", application_number_norm);

                String publication_number = rs.getString("publication_number");
                if (publication_number != null)
                    map.put("publication_number", publication_number);

                String publication_number_norm = rs.getString("publication_number_norm");
                if (publication_number_norm != null)
                    map.put("publication_number_norm", publication_number_norm);


                // get documentId
                List<String> docIds = getDocIds(esConfigs.get(0), index, map);
                if (docIds == null) {
//                    updateDB(conn, tableName, country_code, application_number, publication_number, 3);
                    System.out.println(country_code);
                    continue;
                }

                DartipInfo dartipInfo = new DartipInfo();
                String firstActionTypes = rs.getString("firstActionTypes");
                if (firstActionTypes != null) {
                    String[] firstTypes = firstActionTypes.split("\\|");
                    if (firstTypes.length > 0)
                        dartipInfo.setFirstActionTypes(Arrays.asList(firstTypes));
                }

                dartipInfo.setDartsUrl(rs.getString("dartsUrl"));
                dartipInfo.setDocdbFamilyId(rs.getString("docdbFamilyId"));
                dartipInfo.setInpadocFamilyId(rs.getString("inpadocFamily"));
                dartipInfo.setHasLitigation(true);

                Map<String, DartipInfo> dartipMap = new HashMap<>();

                dartipMap.put("dartipInfo", dartipInfo);

                String json = objectMapper.writeValueAsString(dartipMap);

                for (String docId : docIds) {
                    String destIndex = index;
                    if (docId.matches("^[A-Z]{2}.+"))
                        destIndex = "docdb";
                    else if (docId.matches("^usps.+"))
                        destIndex = "uspto_past";
                    else if (docId.matches("^jpps.+"))
                        destIndex = "jpo_past";

                    System.out.println(destIndex + " - " + docId + " : " + json);

//                    for (ESConfig esConfig : esConfigs)
//                        esConfig.bulkProcessor.add(new UpdateRequest(destIndex, "patent", docId)
//                                .doc(json.getBytes())
//                                .upsert(json.getBytes()));

                }
//                updateDB(conn, tableName, country_code, application_number, publication_number, 1);
            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private static List<String> getDocIds(ESConfig esConfig, String index, Map<String, String> map) {
        String application_number = map.get("application_number");
        String publication_number = map.get("publication_number");
        String application_number_norm = map.get("application_number_norm");
        String publication_number_norm = map.get("publication_number_norm");
        String documentType = map.get("documentType");
        String country_code = map.get("country_code");

        String application_number_n = "";
        if (index.matches("uspto")) {
            if (application_number.matches("^(19|20)[0-9]{2}[0-9]{8}$"))
                application_number_n = NumberUtils.getNumber(country_code, application_number, "AN");
        } else
            application_number_n = NumberUtils.getNumber(country_code, application_number, "AN");

        System.out.println("application_number : " + application_number + " " +"application_number_n : " + application_number_n );

        String publication_number_n = NumberUtils.getNumber(country_code, publication_number, "PN");

        Set<String> appNums = new HashSet<>();
        if (application_number != null && !"".equals(application_number))
            appNums.add(application_number);

        if (application_number_norm != null && !"".equals(application_number_norm))
            appNums.add(application_number_norm);

        if (application_number_n != null && !"".equals(application_number_n))
            appNums.add(application_number_n);

        Set<String> pubNums = new HashSet<>();
        if (publication_number != null && !"".equals(publication_number))
            pubNums.add(publication_number);

        if (publication_number_norm != null && !"".equals(publication_number_norm))
            pubNums.add(publication_number_norm);

        if (publication_number_n != null && !"".equals(publication_number_n))
            pubNums.add(publication_number_n);

        System.out.println(country_code + " " + documentType + " : " + application_number + " : " + publication_number);
        System.out.println(country_code + " " + documentType + " : " + application_number_n + " : " + publication_number_n);
        System.out.println(country_code + " " + documentType + " : " + application_number_norm + " : " + publication_number_norm);

        Set<String> indexSet = new HashSet<>();
        indexSet.add("docdb");
        indexSet.add(index);
        if (index.matches("uspto"))
            indexSet.add("uspto_past");
        else if (index.matches("jpo"))
            indexSet.add("jpo_past");

        String[] countryFields = {"countryCode", "publishingORG"};

        Set<String> patentTypeSet = new HashSet<>();
        if (documentType == null || "".equals(documentType)) {
            patentTypeSet.add("A");
            patentTypeSet.add("B");
        } else {
            if (documentType.matches("^(U|Y|W)[0-9]?$")) {
                patentTypeSet.add("U");
                patentTypeSet.add("Y");
            } else if (documentType.matches("^(S)[0-9]?$")) {
                patentTypeSet.add("S");
            } else if (documentType.matches("^(E)[0-9]?$")) {
                patentTypeSet.add("E");
            } else {
                patentTypeSet.add("A");
                patentTypeSet.add("B");
            }
        }

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.multiMatchQuery(country_code, countryFields))
                .must(QueryBuilders.termsQuery("patentType", patentTypeSet))
                .must(QueryBuilders.boolQuery()
                        .should(QueryBuilders.termsQuery("applicationNumber", appNums))
                        .should(QueryBuilders.termsQuery("patentNumber", pubNums)))
                        .should(
                                QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("_index","docdb"))
                                .must(QueryBuilders.termsQuery("originalApplicationNumber", appNums))
                        )
                .mustNot(QueryBuilders.existsQuery("dartipInfo"));

//        System.out.println(bqb);

        String[] includes = {
                "documentId"
        };

        SearchResponse scrollResp = esConfig.client.prepareSearch(indexSet.stream().toArray(n -> new String[n]))
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(includes, null)
                .setSize(10)
                .execute()
                .actionGet();

//        System.out.println(esConfig.client.prepareSearch(indexSet.stream().toArray(n -> new String[n]))
//                .setTypes("patent")
//                .setSearchType(SearchType.SCAN)
//                .setScroll(new TimeValue(200000))
//                .setQuery(bqb)
//                .setFetchSource(includes, null)
//                .setSize(10));

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        List<String> docIds = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                docIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!docIds.isEmpty())
            return docIds;

        return null;
    }

    private static void updateDB(Connection conn, String tableName, String country_code, String application_number, String publication_number, int i) {
        String query = "UPDATE dartip.litigation SET status = " + i +
                " WHERE country_code = '" + country_code + "'" +
                " and application_number = '" + application_number + "'" +
                " and publication_number = '" + publication_number + "'";

//        System.out.println(query);
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            int r = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void updateDB(Connection conn, String country, List<String> list) {
        String query = "UPDATE dartip.litigation SET status = 1" +
                " WHERE application_number_norm = ? and country_code = '" + country + "'";
//        " WHERE docdbFamilyId regexp '^[U|Y][0-9]?$' and application_number_norm = ? and country_code = '" + country + "'";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String num : list) {
                preparedStatement.setString(1, num);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
