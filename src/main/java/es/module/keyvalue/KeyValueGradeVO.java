package es.module.keyvalue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KeyValueGradeVO {
    public String appno;
    public String patentId;

    public String grade;
    public String gradeT;
    public String gradeR;
    public String gradeU;

    public String smart3Grade;
    public String smart3GradeT;
    public String smart3GradeR;
    public String smart3GradeU;

    public Double citation_weight;
    public Double family_weight;
    public Double annuity_weight;

    public Double gradeUpperFence;
    public Double gradeTUpperFence;
    public Double gradeRUpperFence;
    public Double gradeUUpperFence;

    public Double gradeTotalScore;
    public Double gradeTTotalScore;
    public Double gradeRTotalScore;
    public Double gradeUTotalScore;

    public Double getGrade_score() {
        return (citation_weight*2)+(family_weight*2)+(annuity_weight);
    }

    public Double getGradeT_score() {
        return citation_weight+family_weight;
    }

    public Double getGradeR_score() {
        return citation_weight+family_weight;
    }

    public Double getGradeU_score() {
        return annuity_weight+family_weight;
    }

    public Double grade_score;
    public Double gradeT_score;
    public Double gradeR_score;
    public Double gradeU_score;

    public Integer getGrade() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(grade);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }

    public Integer getGradeT() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(gradeT);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }
    public Integer getGradeR() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(gradeR);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }
    public Integer getGradeU() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(gradeU);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }
    public Integer getSmart3Grade() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3Grade);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }
    public Integer getSmart3GradeT() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3GradeT);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }
    public Integer getSmart3GradeR() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3GradeR);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }
    public Integer getSmart3GradeU() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3GradeU);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.score;
    }



}
