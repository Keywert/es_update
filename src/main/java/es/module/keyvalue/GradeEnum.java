package es.module.keyvalue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum GradeEnum {
    A("1등급",9,Arrays.asList("A","AAA","9")),
    B("2등급",8,Arrays.asList("B","AA","8")),
    C("3등급",7,Arrays.asList("C","A","7")),
    D("4등급",6,Arrays.asList("D","BBB","6")),
    E("5등급",5,Arrays.asList("E","BB","5")),
    F("6등급",4,Arrays.asList("F","B","4")),
    G("7등급",3,Arrays.asList("G","CCC","3")),
    H("8등급",2,Arrays.asList("H","CC","2")),
    I("9등급",1,Arrays.asList("I","C","1")),
    EMPTY("없음",0,Collections.EMPTY_LIST);

    public String grade;
    public Integer score;
    private List<String> resultList;


    GradeEnum(String grade, Integer score, List resultList) {
        this.grade = grade;
        this.score = score;
        this.resultList = resultList;
    }

    public static GradeEnum findByGradeEnum(String grade) {
        return Arrays.stream(GradeEnum.values())
                .filter(out -> out.hasResult(grade))
                .findAny()
                .orElse(EMPTY);
    }

    public boolean hasResult(String result) {
        return resultList.stream()
                .anyMatch(out -> out.equals(result));
    }

}
