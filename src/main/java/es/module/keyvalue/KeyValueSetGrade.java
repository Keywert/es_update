package es.module.keyvalue;

import es.config.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class KeyValueSetGrade {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_local();

//        String baseTable = "iprating.kipo_rating_202203";
//        String weightTable = "iprating.kipo_rating_data";

//        String baseTable = "iprating.epo_rating_202203";
//        String weightTable = "iprating.epo_rating_data";

        String baseTable = "iprating.uspto_rating_202203";
        String weightTable = "iprating.uspto_rating_data";

//        String baseTable = "iprating.kipo_rating_202111";
//        String weightTable = "iprating.kipo_rating_data";



        String query = "select * from " + baseTable + " A inner join  "
                + weightTable + " B on A.APPLNO = B.APPNO";

//        String query = "select * from " + baseTable + " A left outer join  "
//                + weightTable + " B on A.APPLNO = B.APPNO ";

        System.out.println(query);

        Statement st = null;
        int count = 0;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<KeyValueGradeVO> gradeVOS = new ArrayList<>();

            List<Double> gradeScoreList = new ArrayList<>();
            List<Double> gradeTScoreList = new ArrayList<>();
            List<Double> gradeRScoreList = new ArrayList<>();
            List<Double> gradeUScoreList = new ArrayList<>();

            while (rs.next()) {
                KeyValueGradeVO keyValueGradeVO = new KeyValueGradeVO();
                String appno = rs.getString("APPNO");
                keyValueGradeVO.setAppno(appno);

                String patentId = rs.getString("B.patentId");
                keyValueGradeVO.setPatentId(patentId);

                String smart3grade = rs.getString("A.GRADE");
                keyValueGradeVO.setSmart3Grade(smart3grade);

                String smart3gradeT = rs.getString("A.T_GRADE");
                keyValueGradeVO.setSmart3GradeT(smart3gradeT);

                String smart3gradeR = rs.getString("A.R_GRADE");
                keyValueGradeVO.setSmart3GradeR(smart3gradeR);

                String smart3gradeU = rs.getString("A.U_GRADE");
                keyValueGradeVO.setSmart3GradeU(smart3gradeU);

                double citation_weight = rs.getDouble("B.citation_weight");
                keyValueGradeVO.setCitation_weight(citation_weight);

                double family_weight = rs.getDouble("B.family_weight");
                keyValueGradeVO.setFamily_weight(family_weight);

                double annuity_weight = rs.getDouble("B.annuity_weight");
                keyValueGradeVO.setAnnuity_weight(annuity_weight);

//                System.out.println(keyValueGradeVO.getGrade_score());
                gradeScoreList.add(keyValueGradeVO.getGrade_score());
                gradeTScoreList.add(keyValueGradeVO.getGradeT_score());
                gradeRScoreList.add(keyValueGradeVO.getGradeR_score());
                gradeUScoreList.add(keyValueGradeVO.getGradeU_score());

                gradeVOS.add(keyValueGradeVO);

            }

            Double gradeUpperFence = calculateGradeData(gradeScoreList);
            Double gradeTUpperFence = calculateGradeData(gradeTScoreList);
            Double gradeRUpperFence = calculateGradeData(gradeRScoreList);
            Double gradeUUpperFence = calculateGradeData(gradeUScoreList);

            System.out.println(gradeVOS.size());

            int totalCount = gradeVOS.size();
            long start = System.currentTimeMillis();
            for (KeyValueGradeVO keyValueGradeVO : gradeVOS) {
                double totalScore = getTotalScore(keyValueGradeVO.getSmart3Grade(), keyValueGradeVO.getGrade_score(), gradeUpperFence);
//                System.out.println(keyValueGradeVO.getSmart3Grade() + " : " + keyValueGradeVO.getGrade_score() + " : " + gradeUpperFence + " = " + totalScore);
                keyValueGradeVO.setGradeTotalScore(totalScore);

                double totalTScore = getTotalScore(keyValueGradeVO.getSmart3GradeT(), keyValueGradeVO.getGradeT_score(), gradeTUpperFence);
                keyValueGradeVO.setGradeTTotalScore(totalTScore);
//                System.out.println(keyValueGradeVO.getSmart3GradeT() + " : " + keyValueGradeVO.getGradeT_score() + " : " + gradeTUpperFence + " = " + totalTScore);

                double totalRScore = getTotalScore(keyValueGradeVO.getSmart3GradeR(), keyValueGradeVO.getGradeR_score(), gradeRUpperFence);
                keyValueGradeVO.setGradeRTotalScore(totalRScore);

                double totalUScore = getTotalScore(keyValueGradeVO.getSmart3GradeU(), keyValueGradeVO.getGradeU_score(), gradeUUpperFence);
//                System.out.println(keyValueGradeVO.getSmart3GradeU() + " : " + keyValueGradeVO.getGradeU_score() + " : " + gradeUUpperFence + " = " + totalUScore);

                keyValueGradeVO.setGradeUTotalScore(totalUScore);

//                System.out.println(new ObjectMapper().writeValueAsString(keyValueGradeVO));
            }
            System.out.println("getTotalScore : " + (System.currentTimeMillis() - start) / 1000.0);

            start = System.currentTimeMillis();
            // Grade
            Collections.sort(gradeVOS, new Comparator<KeyValueGradeVO>() {
                @Override
                public int compare(KeyValueGradeVO s1, KeyValueGradeVO s2) {
                    if (s1.getGradeTotalScore() > s2.getGradeTotalScore()) {
                        return -1;
                    } else if (s1.getGradeTotalScore() < s2.getGradeTotalScore()) {
                        return 1;
                    }
                    return 0;
                }
            });

            double previousScore = 0.0;
            String previousStanineScore = "9";
            for (int i = 0, len = gradeVOS.size(); i < len; i++) {
                String stanineScore = getStanineScore(len, i+1);
                double score = gradeVOS.get(i).getGradeTotalScore();

                if (!previousStanineScore.matches(stanineScore) && previousScore == score)
                    gradeVOS.get(i).setGrade(previousStanineScore);
                else {
                    gradeVOS.get(i).setGrade(stanineScore);
                    previousStanineScore = stanineScore;
                    previousScore = score;
                }
            }

            System.out.println("Grade : " + (System.currentTimeMillis() - start) / 1000.0);

            start = System.currentTimeMillis();

            // T Grade
            Collections.sort(gradeVOS, new Comparator<KeyValueGradeVO>() {
                @Override
                public int compare(KeyValueGradeVO s1, KeyValueGradeVO s2) {
                    if (s1.getGradeTTotalScore() > s2.getGradeTTotalScore()) {
                        return -1;
                    } else if (s1.getGradeTTotalScore() < s2.getGradeTTotalScore()) {
                        return 1;
                    }
                    return 0;
                }
            });

            previousScore = 0.0;
            previousStanineScore = "9";
            for (int i = 0, len = gradeVOS.size(); i < len; i++) {
                String stanineScore = getStanineScore(len, i+1);
                double score = gradeVOS.get(i).getGradeTTotalScore();

                if (!previousStanineScore.matches(stanineScore) && previousScore == score)
                    gradeVOS.get(i).setGradeT(previousStanineScore);
                else {
                    gradeVOS.get(i).setGradeT(stanineScore);
                    previousStanineScore = stanineScore;
                    previousScore = score;
                }
            }

            System.out.println("T Grade : " + (System.currentTimeMillis() - start) / 1000.0);
            start = System.currentTimeMillis();

            // R Grade
            Collections.sort(gradeVOS, new Comparator<KeyValueGradeVO>() {
                @Override
                public int compare(KeyValueGradeVO s1, KeyValueGradeVO s2) {
                    if (s1.getGradeRTotalScore() > s2.getGradeRTotalScore()) {
                        return -1;
                    } else if (s1.getGradeRTotalScore() < s2.getGradeRTotalScore()) {
                        return 1;
                    }
                    return 0;
                }
            });

            previousScore = 0.0;
            previousStanineScore = "9";
            for (int i = 0, len = gradeVOS.size(); i < len; i++) {
                String stanineScore = getStanineScore(len, i+1);
                double score = gradeVOS.get(i).getGradeRTotalScore();

                if (!previousStanineScore.matches(stanineScore) && previousScore == score)
                    gradeVOS.get(i).setGradeR(previousStanineScore);
                else {
                    gradeVOS.get(i).setGradeR(stanineScore);
                    previousStanineScore = stanineScore;
                    previousScore = score;
                }
            }

            System.out.println("R Grade : " + (System.currentTimeMillis() - start) / 1000.0);
            start = System.currentTimeMillis();

            // U Grade
            Collections.sort(gradeVOS, new Comparator<KeyValueGradeVO>() {
                @Override
                public int compare(KeyValueGradeVO s1, KeyValueGradeVO s2) {
                    if (s1.getGradeUTotalScore() > s2.getGradeUTotalScore()) {
                        return -1;
                    } else if (s1.getGradeUTotalScore() < s2.getGradeUTotalScore()) {
                        return 1;
                    }
                    return 0;
                }
            });

            previousScore = 0.0;
            previousStanineScore = "9";
            for (int i = 0, len = gradeVOS.size(); i < len; i++) {
                String stanineScore = getStanineScore(len, i+1);
                double score = gradeVOS.get(i).getGradeUTotalScore();

                if (!previousStanineScore.matches(stanineScore) && previousScore == score)
                    gradeVOS.get(i).setGradeU(previousStanineScore);
                else {
                    gradeVOS.get(i).setGradeU(stanineScore);
                    previousStanineScore = stanineScore;
                    previousScore = score;
                }
            }

            System.out.println("U Grade : " + (System.currentTimeMillis() - start) / 1000.0);

//            updateDB(conn, baseTable, gradeVOS);
            insertDB(conn, baseTable, gradeVOS);

//
//            for (KeyValueGradeVO keyValueGradeVO : gradeVOS) {
////                System.out.println(keyValueGradeVO.getGradeUTotalScore());
//                System.out.println(new ObjectMapper().writeValueAsString(keyValueGradeVO));
//            }

        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
//        catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
    }

    private static void insertDB(Connection conn, String baseTable, List<KeyValueGradeVO> gradeVOS) {
        String query = "INSERT into " + baseTable +
                " (" +
                " APPLNO, WERT_GRADE, WERT_R_GRADE , WERT_T_GRADE , WERT_U_GRADE ,patentId,status " +
                ") " +
                "values(?,?,?,?,?,?,1) " +
                "on duplicate key update " +
                " WERT_GRADE = values (WERT_GRADE)," +
                " WERT_R_GRADE = values (WERT_R_GRADE), "+
                " WERT_T_GRADE = values (WERT_T_GRADE), "+
                " WERT_U_GRADE = values (WERT_U_GRADE), "+
                " patentId = values (patentId), "+
                " status = 1 "
                ;

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            int count = 0;
            for (KeyValueGradeVO keyValueGradeVO : gradeVOS) {
                preparedStatement.setString(1, keyValueGradeVO.getAppno());
                preparedStatement.setString(2, keyValueGradeVO.getGrade().toString());
                preparedStatement.setString(3, keyValueGradeVO.getGradeR().toString());
                preparedStatement.setString(4, keyValueGradeVO.getGradeT().toString());
                preparedStatement.setString(5, keyValueGradeVO.getGradeU().toString());
                preparedStatement.setString(6, keyValueGradeVO.getPatentId());

                preparedStatement.addBatch();

                if (++count % 1000 == 0) {
                    long start = System.currentTimeMillis();

                    preparedStatement.executeBatch();

                    preparedStatement.clearBatch();

                    System.out.println("updateDB : " + (System.currentTimeMillis() - start) / 1000.0);
//                    conn.commit();
                }
            }

            preparedStatement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateDB(Connection conn, String tableName, List<KeyValueGradeVO> gradeVOS) {
        String query = "UPDATE " + tableName + " SET " +
                " WERT_GRADE = ?, WERT_R_GRADE = ?, WERT_T_GRADE = ?, WERT_U_GRADE = ?  ,patentId = ?" +
                ", status = 1 " +
                " WHERE APPLNO = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            int count = 0;
            for (KeyValueGradeVO keyValueGradeVO : gradeVOS) {
                preparedStatement.setString(1, keyValueGradeVO.getGrade().toString());
                preparedStatement.setString(2, keyValueGradeVO.getGradeR().toString());
                preparedStatement.setString(3, keyValueGradeVO.getGradeT().toString());
                preparedStatement.setString(4, keyValueGradeVO.getGradeU().toString());
                preparedStatement.setString(5, keyValueGradeVO.getPatentId());
                preparedStatement.setString(6, keyValueGradeVO.getAppno());


                preparedStatement.addBatch();

                if (++count % 1000 == 0) {
                    long start = System.currentTimeMillis();

                    preparedStatement.executeBatch();

                    preparedStatement.clearBatch();

                    System.out.println("updateDB : " + (System.currentTimeMillis() - start) / 1000.0);
//                    conn.commit();
                }
            }

            preparedStatement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    private static String getStanineScore(int max, int rank) {
        List<Integer> list = new ArrayList<>();
        list.add((int) (max * 0.04));
        list.add((int) (max * 0.11));
        list.add((int) (max * 0.23));
        list.add((int) (max * 0.40));
        list.add((int) (max * 0.60));
        list.add((int) (max * 0.77));
        list.add((int) (max * 0.89));
        list.add((int) (max * 0.96));
        list.add((int) (max * 1.0));

//        System.out.println(list);

        int stanineScore = 9;
        for (int i = 0, len = list.size(); i < len; i++) {
            if (list.get(i) >= rank)
                return String.valueOf(stanineScore - i);
        }

        return null;
    }

    private static double getTotalScore(Integer smart3Grade, Double grade_score, Double gradeUpperFence) {
        double smartRatio = 0.7;
        double keywertRatio = 0.3;

        if (gradeUpperFence != null && grade_score > gradeUpperFence)
            grade_score = gradeUpperFence;


        return ((((double)smart3Grade - 1) / 8) * 100 * smartRatio) + ((grade_score / gradeUpperFence) * 100 * keywertRatio);
    }

    private static Double calculateGradeData(List<Double> input) {
        Collections.sort(input);
        List<Double> output = new ArrayList<Double>();
        List<Double> data1 = new ArrayList<Double>();
        List<Double> data2 = new ArrayList<Double>();

        if (input.size() % 2 == 0) {
            data1 = input.subList(0, input.size() / 2);
            data2 = input.subList(input.size() / 2, input.size());
        } else {
            data1 = input.subList(0, input.size() / 2);
            data2 = input.subList(input.size() / 2 + 1, input.size());
        }

        double q1 = getMedian(data1);
        double q3 = getMedian(data2);
        double iqr = q3 - q1;
        double lowerFence = q1 - 1.5 * iqr;
        double upperFence = q3 + 1.5 * iqr;

//        System.out.println("lowerFence : " + lowerFence);
//        System.out.println("upperFence : " + upperFence);

        return upperFence + (upperFence/10);
    }

    private static double getMedian(List<Double> data) {
        if (data.size() % 2 == 0)
            return (data.get(data.size() / 2) + data.get(data.size() / 2 - 1)) / 2;
        else
            return data.get(data.size() / 2);
    }

}
