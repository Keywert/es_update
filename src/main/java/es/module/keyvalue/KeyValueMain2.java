package es.module.keyvalue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.utility.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.queryparser.xml.builders.FilteredQueryBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class KeyValueMain2 {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        Connection conn_aurora2 = dbHelper.getConn_aurora2();

        ESConfig esConfig = new ESConfig("es205");
        ESConfig usESConfig = new ESConfig("es51");


//        String tableName = "iprating.uspto_rating_data";
//        String tableName = "iprating.kipo_rating_data";
        String tableName = "iprating.epo_rating_data";

        long start = System.currentTimeMillis();
        int limitCount = 3000000;

        String query = "select * from " + tableName
//                + " where APPNO in('US06836048') " ;
//                 + " where APPNO regexp '^(10|20)(19|20)[0-9]{2}07[0-9]{5}$' " ;

//                " status = 11 " +
//                " and documentId = 'kr20197021833a'" +
                + " where status is null limit " + limitCount;

        System.out.println(query);

        String index = null;
        if (tableName.contains("kipo"))
            index = "kipo";
        else if (tableName.contains("epo"))
            index = "epo";
        else if (tableName.contains("uspto"))
            index = "uspto";
        else {
            System.out.println(tableName);
            return;
        }


        Statement st = null;

        int count = 0;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, KeyValueDataVO> keyValueDataVOMap = new HashMap<>();

            while (rs.next()) {
                if (++count % 1000 == 0) {
                    System.out.println(count);
                }

                String appno = rs.getString("APPNO");
                String rgstno = rs.getString("RGSTNO");

                KeyValueDataVO keyValueDataVO = new KeyValueDataVO();
                keyValueDataVO.setAppno(appno);
                keyValueDataVO.setRgstno(rgstno);
                keyValueDataVO.setIndex(index);

                keyValueDataVOMap.put(appno, keyValueDataVO);

                if (keyValueDataVOMap.size() > 100) {
                    KeyValueDataProc(esConfig, usESConfig, conn_aurora2, index, keyValueDataVOMap);
                    updateDB(conn, tableName, keyValueDataVOMap);
                    keyValueDataVOMap.clear();
                }
            }

            if (!keyValueDataVOMap.isEmpty()) {
                KeyValueDataProc(esConfig, usESConfig, conn_aurora2, index, keyValueDataVOMap);
                updateDB(conn, tableName, keyValueDataVOMap);
            }


            System.out.println(limitCount + " 평균 : " + ((System.currentTimeMillis() - start) / 1000.0) / count);

        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

    }

    private static void KeyValueDataProc(ESConfig esConfig, ESConfig usESConfig, Connection conn_aurora2, String index, Map<String, KeyValueDataVO> keyValueDataVOMap) {
//        long start = System.currentTimeMillis();
        getPatentDatas(esConfig, index, keyValueDataVOMap);
//        System.out.println("getPatentDatas : " + (System.currentTimeMillis() - start) / 1000.0);

//        start = System.currentTimeMillis();
        getCitationWeightData(conn_aurora2, esConfig, usESConfig, index, keyValueDataVOMap);
//        System.out.println("getCitationWeightData : " + (System.currentTimeMillis() - start) / 1000.0);

//        start = System.currentTimeMillis();
        getFamilyWeightData(esConfig, keyValueDataVOMap);
//        System.out.println("getFamilyWeightData : " +(System.currentTimeMillis() - start) / 1000.0);


//        for (KeyValueDataVO keyValueDataVO : keyValueDataVOMap.values()) {
//            try {
//                System.out.println(new ObjectMapper().writeValueAsString(keyValueDataVO));
//            } catch (JsonProcessingException e) {
//                e.printStackTrace();
//            }
//        }

        return;
    }

    private static void getFamilyWeightData(ESConfig esConfig, Map<String, KeyValueDataVO> keyValueDataVOMap) {
        String index = "docdb";
        String type = "patent";

        Set<String> familyIds = new HashSet<>();

        for (KeyValueDataVO keyValueDataVO : keyValueDataVOMap.values()) {
            String familyId = keyValueDataVO.getDocdbFamilyId();
            if (familyId != null)
                familyIds.add(familyId);
        }

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("familyId", familyIds));

        String[] includes = {
                "epoPublicationNumber", "applicationNumber", "countryCode", "extendedFamilyId", "familyId", "epoApplicationNumber", "documentType",
                "originalApplicationNumber"
        };

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(includes, null)
                .setSize(50)
                .execute()
                .actionGet();

        Map<String, Set<String>> familyCountrySetMap = new HashMap<>();
        Map<String, Map<String, String>> familyMap = new HashMap<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object familyId = hit.getSource().get("familyId");
                if (familyId == null)
                    continue;

                Set<String> countrySet = familyCountrySetMap.get(familyId.toString());
                if (countrySet == null)
                    countrySet = new HashSet<>();
                countrySet.add(hit.getId().substring(0, 2));
                familyCountrySetMap.put(familyId.toString(), countrySet);

                Map<String, String> map = familyMap.get(familyId.toString());
                if (map == null)
                    map = new HashMap<>();

                Object epoApplicationNumber = hit.getSource().get("epoApplicationNumber");
                if (epoApplicationNumber == null)
                    map.put(hit.getId(), hit.getId().substring(0, 2));
                else
                    map.put(epoApplicationNumber.toString(), hit.getId().substring(0, 2));
                familyMap.put(familyId.toString(), map);

            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        Iterator<String> iterator = keyValueDataVOMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            KeyValueDataVO keyValueDataVO = keyValueDataVOMap.get(key);
            if (keyValueDataVO == null)
                continue;

            String docdbFamilyId = keyValueDataVO.getDocdbFamilyId();
            if (docdbFamilyId == null)
                continue;

            double basicWeight = 1.0;
            double weight = 1.5;
            double familyWeight = 0.0;

            // 6.0
            String[] us_jp_ep = {"US", "JP", "EP", "DE"};
            String[] us_jp_de = {"US", "JP", "DE"};
            // 4.0
            String[] us_jp = {"US", "JP"};
            String[] us_ep = {"US", "EP"};
            String[] us_de = {"US", "DE"};

            Set<String> countrySet = familyCountrySetMap.get(docdbFamilyId);
            if (countrySet == null)
                continue;

            if (countrySet.containsAll(Arrays.asList(us_jp_ep)))
                weight = 6.0;
            else if (countrySet.containsAll(Arrays.asList(us_jp_de)))
                weight = 4.0;
            else if (countrySet.containsAll(Arrays.asList(us_jp)) || countrySet.containsAll(Arrays.asList(us_ep)) || countrySet.containsAll(Arrays.asList(us_de)))
                weight = 2.0;

            Map<String, String> map = familyMap.get(docdbFamilyId);
            for (String country : map.values()) {
                if (country.matches("(US|EP|DE|JP)"))
                    familyWeight += weight;
                else
                    familyWeight += basicWeight;
            }

            // 자기자신 제외
            double base = basicWeight;
            if (keyValueDataVO.getIndex().matches("(uspto|epo)"))
                base = weight;

            keyValueDataVO.setFamily_weight(familyWeight - base);
            keyValueDataVOMap.put(key, keyValueDataVO);
        }
    }

    private static void getCitationWeightData(Connection conn_aurora2, ESConfig esConfig, ESConfig usESConfig, String index, Map<String, KeyValueDataVO> keyValueDataVOMap) {
        String tableName = getCitationTableName(index);
        if (tableName == null)
            return;

        List<String> patentIds = new ArrayList<>();
        Map<String, String> keyMap = new HashMap<>();
        for (KeyValueDataVO keyValueDataVO : keyValueDataVOMap.values()) {
            String patentId = keyValueDataVO.getPatentId();
            if (patentId != null) {
                patentIds.add(patentId);
                keyMap.put(patentId, keyValueDataVO.getAppno());
            }
        }

        String str = StringUtils.join(patentIds, "','");

        String query = "SELECT * FROM " + tableName + " where  cited_patentId in('" + str + "')";

        Statement st = null;
        try {
            Map<String, List<String>> citationMap = new HashMap<>();
            Set<String> patentIdSet = new HashSet<>();

            if (index.matches("uspto")) {
//                long start = System.currentTimeMillis();
                getUSCitationInfo(usESConfig, patentIds, citationMap, patentIdSet);
//                System.out.println("getUSCitationInfo : " + (System.currentTimeMillis() - start) / 1000.0);

            } else {
                st = conn_aurora2.createStatement();
                st.setFetchSize(100);
                ResultSet rs = st.executeQuery(query);

                while (rs.next()) {
                    String cited_patentId = rs.getString("cited_patentId");
                    String citing_patentId = rs.getString("citing_patentId");

                    if (cited_patentId.equals(citing_patentId))
                        continue;

                    patentIdSet.add(citing_patentId);

                    List<String> citing_patentId_list = citationMap.get(cited_patentId);
                    if (citing_patentId_list == null)
                        citing_patentId_list = new ArrayList<>();

                    citing_patentId_list.add(citing_patentId);

                    citationMap.put(cited_patentId, citing_patentId_list);
                }
            }

            if (!patentIdSet.isEmpty()) {
                long start = System.currentTimeMillis();
                Map<String, KeyValueDataVO> patentMap = getPatents(esConfig, patentIdSet);
                if (patentMap == null)
                    return;


                for (String citedPatentId : keyMap.keySet()) {
                    KeyValueDataVO citedData = keyValueDataVOMap.get(keyMap.get(citedPatentId));
                    if (citedData == null)
                        continue;

                    List<String> citing_patentId_list = citationMap.get(citedPatentId);
                    if (citing_patentId_list == null)
                        continue;

                    double citationWeight = 0.0;
                    Set<String> citedNames = citedData.getNameSet();
                    if (citedNames == null) {
                        citationWeight = citing_patentId_list.size() * 3.0;
                    } else {
                        for (String citing_patentId : citing_patentId_list) {
                            double weight = 3.0;
                            KeyValueDataVO citingData = patentMap.get(citing_patentId);
                            if (citingData != null) {
                                Set<String> citingNames = citingData.getNameSet();
                                if (citingNames != null)
                                    for (String name : citingNames) {
                                        if (citedNames.contains(name)) {
                                            weight = 1.0;
                                            break;
                                        }
                                    }
                            }
                            citationWeight += weight;
                        }
                    }

                    citedData.setCitation_weight(citationWeight);
                    keyValueDataVOMap.put(keyMap.get(citedPatentId), citedData);
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    private static void getUSCitationInfo(ESConfig usESConfig, List<String> patentIds, Map<String, List<String>> citationMap, Set<String> patentIdSet) {
        String index = "uspto";
        String type = "patent";

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termsQuery("citedPatentId", patentIds));

        SearchResponse scrollResp = usESConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
//                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object citingPatentId = hit.getSource().get("citingPatentId");
                if (citingPatentId == null || "".equals(citingPatentId))
                    continue;

                Object citedPatentId = hit.getSource().get("citedPatentId");
                if (citedPatentId == null || "".equals(citedPatentId))
                    continue;

                patentIdSet.add(citingPatentId.toString());

                List<String> citing_patentId_list = citationMap.get(citedPatentId.toString());
                if (citing_patentId_list == null)
                    citing_patentId_list = new ArrayList<>();

                citing_patentId_list.add(citingPatentId.toString());
                citationMap.put(citedPatentId.toString(), citing_patentId_list);
            }
            scrollResp = usESConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);


    }

    private static Map<String, KeyValueDataVO> getPatents(ESConfig esConfig, Set<String> patentIdSet) {
        String[] indexies = {"kipo", "jpo", "uspto", "sipo", "pct", "epo", "dpma"};
        String type = "patent";
        String[] fields = {
                "docdbFamilyId", "extendedFamilyId", "applicantInfo", "currentApplicantInfo", "representApplicantInfo", "rightHolderInfo",
                "currentRightHolderInfo", "assigneeInfo", "patentId", "registerDate"
        };

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termsQuery("patentId", patentIdSet));

//        long start = System.currentTimeMillis();

        SearchResponse scrollResp = esConfig.client.prepareSearch(indexies)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        ObjectMapper mapper = new ObjectMapper();


//        System.out.println(patentIdSet.size() + " - getUSCitationInfo01 : " + (System.currentTimeMillis() - start) / 1000.0);


        Map<String, KeyValueDataVO> keyValueDataVOMap = new HashMap<>();

        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    KeyValuePatentVO patent = mapper.readValue(hit.getSourceAsString(), KeyValuePatentVO.class);
                    if (patent == null)
                        continue;

                    String patentId = patent.getPatentId();
                    KeyValueDataVO keyValueData = keyValueDataVOMap.get(patentId);
                    if (keyValueData == null)
                        keyValueData = new KeyValueDataVO();

                    keyValueData.setPatentId(patentId);

                    Set<String> names = patent.getNames();
                    if (names != null) {
                        Set<String> nameSet = keyValueData.getNameSet();
                        if (nameSet == null)
                            nameSet = new HashSet<>();

                        nameSet.addAll(names);
                        keyValueData.setNameSet(nameSet);
                    }

                    String registerDate = patent.getRegisterDate();
                    if (registerDate != null)
                        keyValueData.setRegisterDate(registerDate);

                    String docdbFamilyId = patent.getDocdbFamilyId();
                    if (docdbFamilyId != null)
                        keyValueData.setDocdbFamilyId(docdbFamilyId);

                    String extendedFamilyId = patent.getExtendedFamilyId();
                    if (extendedFamilyId != null)
                        keyValueData.setExtendedFamilyId(extendedFamilyId);


                    keyValueDataVOMap.put(patentId, keyValueData);


                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!keyValueDataVOMap.isEmpty())
                return keyValueDataVOMap;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getCitationTableName(String index) {

        if (index == null)
            return null;
        else if (index.matches("kipo"))
            return "citation.forward_citation_kipo";
        else if (index.matches("epo"))
            return "citation.forward_citation_epo";
        else if (index.matches("uspto"))
            return "citation.forward_citation_uspto";

        return null;
    }

    private static void getPatentDatas(ESConfig esConfig, String index, Map<String, KeyValueDataVO> keyValueDataVOMap) {
        String type = "patent";
        String[] fields = {
                "docdbFamilyId", "extendedFamilyId", "applicantInfo", "currentApplicantInfo", "representApplicantInfo", "rightHolderInfo",
                "currentRightHolderInfo", "assigneeInfo", "patentId", "registerDate", "applicationNumber"
        };

        List<String> appNums = new ArrayList<>();
        List<String> regNums = new ArrayList<>();

        for (KeyValueDataVO keyValueDataVO : keyValueDataVOMap.values()) {
            appNums.add(NumberUtils.getNumber(keyValueDataVO.getIndex(), keyValueDataVO.getApplicationNumber(), "AN"));
            regNums.add(NumberUtils.getNumber(keyValueDataVO.getIndex(), keyValueDataVO.getRgstno(), "RN"));
        }

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.boolQuery().should(QueryBuilders.termsQuery("applicationNumber", appNums))
                        .should(QueryBuilders.termsQuery("registerNumber", regNums)))
                .filter(QueryBuilders.regexpQuery("documentType", "[A|B|E|F|C][0-9]?"));

        //        System.out.println("Total scroll Hits : " + scrollResp.getHits().getTotalHits());

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

//        System.out.println("Total scroll Hits : " + scrollResp.getHits().getTotalHits() + "  " + scrollResp.getTook());


        ObjectMapper mapper = new ObjectMapper();

        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    KeyValuePatentVO patent = mapper.readValue(hit.getSourceAsString(), KeyValuePatentVO.class);
                    if (patent == null)
                        continue;

                    String applicationNumber = patent.getApplicationNumber();
                    KeyValueDataVO keyValueData = keyValueDataVOMap.get(applicationNumber);
                    if (keyValueData == null && applicationNumber.matches("^(10|20)(19|20)[0-9]{2}70[0-9]{5}$")) {
                        String apn = applicationNumber.substring(0, 6) + "07" + applicationNumber.substring(8);
                        keyValueData = keyValueDataVOMap.get(apn);
                        applicationNumber = apn;
                        if (keyValueData == null) {
                            System.out.println(applicationNumber + " not Found~");
                            continue;
                        }
                    }

                    if (keyValueData == null)
                        continue;

                    Set<String> names = patent.getNames();

                    if (names != null) {
                        Set<String> nameSet = keyValueData.getNameSet();
                        if (nameSet == null)
                            nameSet = new HashSet<>();

                        nameSet.addAll(names);
                        keyValueData.setNameSet(nameSet);
                    }

                    String registerDate = patent.getRegisterDate();
                    if (registerDate != null)
                        keyValueData.setRegisterDate(registerDate);

                    String docdbFamilyId = patent.getDocdbFamilyId();
                    if (docdbFamilyId != null)
                        keyValueData.setDocdbFamilyId(docdbFamilyId);

                    String extendedFamilyId = patent.getExtendedFamilyId();
                    if (extendedFamilyId != null)
                        keyValueData.setExtendedFamilyId(extendedFamilyId);

                    String patentId = patent.getPatentId();
                    if (patentId != null)
                        keyValueData.setPatentId(patentId);

//                    System.out.println(applicationNumber);
                    keyValueDataVOMap.put(applicationNumber, keyValueData);

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String tableName, Map<String, KeyValueDataVO> keyValueDataVOS) {
        String query = "UPDATE " + tableName + " SET status = 1 , citation_weight = ?, family_weight = ?, annuity_weight = ? " +
                ", docdbFamilyId =?,extendedFamilyId =?,patentId = ?, registerDate =?,applicantNames=? , updateDate = current_timestamp() " +
                " WHERE appno = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();
            for (KeyValueDataVO keyValueDataVO : keyValueDataVOS.values()) {

                preparedStatement.setDouble(1, keyValueDataVO.getCitation_weight());
                preparedStatement.setDouble(2, keyValueDataVO.getFamily_weight());
                preparedStatement.setDouble(3, keyValueDataVO.getAnnuity_weight());
                preparedStatement.setString(4, keyValueDataVO.getDocdbFamilyId());
                preparedStatement.setString(5, keyValueDataVO.getExtendedFamilyId());
                preparedStatement.setString(6, keyValueDataVO.getPatentId());
                preparedStatement.setString(7, keyValueDataVO.getRegisterDate());
                preparedStatement.setString(8, keyValueDataVO.getApplicantNames());
                preparedStatement.setString(9, keyValueDataVO.getAppno());

                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
