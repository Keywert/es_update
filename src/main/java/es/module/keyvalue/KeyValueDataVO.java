package es.module.keyvalue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KeyValueDataVO {
    public String index;

    public String getApplicationNumber() {
        if (appno.matches("^(10|20)(19|20)[0-9]{2}07[0-9]{5}$")) {
            String apn = appno.substring(0, 6) + "70" + appno.substring(8);
            return apn;
        }

        return appno;
    }

    public String appno;
    public String rgstno;
    public String registerDate;

    public String patentId;
    public String docdbFamilyId;
    public String extendedFamilyId;

    public Double getCitation_weight() {
        if (citation_weight == null)
            return 0.0;

        return citation_weight;
    }

    public Double citation_weight;

    public Double getFamily_weight() {
        if (family_weight == null)
            return 0.0;

        return family_weight;
    }

    public Double family_weight;

    public Set<String> nameSet;

    public Double getAnnuity_weight() {
        if (registerDate == null)
            return 1.5;

        try {
        SimpleDateFormat todaySdf = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
        //한국기준 날짜
        Calendar calendar = Calendar.getInstance();
        Date date = new Date(calendar.getTimeInMillis());
        todaySdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        String todayDate = todaySdf.format(date);
        //오늘 타임스탬프(데이트포맷으로 저장했다고 치고 그걸 타임스탬프로 바꿔보는 작업)
        long todayTimestamp = todaySdf.parse(todayDate).getTime();
        long nextdayTimestamp = todaySdf.parse(registerDate).getTime();

        long difference = (todayTimestamp - nextdayTimestamp) / (24 * 60 * 60 * 1000 );

        int year = (int) (difference / 365 + 1);

        double weight = 0.0;
        if ( year > 13 )
            weight = (year -3) * 1.2;
        else
            weight = (year - 1 ) * 1.5;

            annuity_weight = weight;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return annuity_weight;
    }

    public Double annuity_weight;

    public String getApplicantNames() {
        if (nameSet != null)
            return StringUtils.join(nameSet, "||");

        return applicantNames;
    }

    public String applicantNames;

    public String getCitationTableName() {
        if (index == null)
            return null;
        else if (index.matches("kipo"))
            return "citation.forward_citation_kipo";
        else if (index.matches("epo"))
            return "citation.forward_citation_epo";
        else if (index.matches("uspto"))
            return "citation.forward_citation_uspto";

        return null;
    }
}
