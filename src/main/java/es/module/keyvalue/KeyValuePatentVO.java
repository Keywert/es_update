package es.module.keyvalue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.model.PersonVO;
import es.module.utility.NameNormalizer;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KeyValuePatentVO {
    public String docdbFamilyId;
    public String extendedFamilyId;
    public String patentId;
    public String registerDate;
    public String applicationDate;

    public KeyValuePatentVO() {
        this.normalizer = new NameNormalizer();
    }

    public String getApplicationNumber() {
        if (patentId.matches("^KR.+"))
            return "10" + applicationNumber.replaceAll("-","");
        else if (patentId.matches("^EP.+"))
            return "EP" + applicationNumber.replaceAll("-","").substring(2);
        else if (patentId.matches("^US.+"))
            return "US" + applicationNumber.replaceAll("-","");

        return applicationNumber;
    }

    public String applicationNumber;


    public List<PersonVO> applicantInfo;          // 출원인
    public List<PersonVO> currentApplicantInfo;          // 출원인
    public List<PersonVO> representApplicantInfo;          // 출원인
    public List<PersonVO> rightHolderInfo;          // 출원인
    public List<PersonVO> currentRightHolderInfo;          // 출원인
    public List<PersonVO> assigneeInfo;          // 출원인

    NameNormalizer normalizer;

    public Set<String> getNames() {
        Set<String> names = new HashSet<>();

        List<PersonVO> personVOList = new ArrayList<>();

        if (this.getApplicantInfo() != null)
            personVOList.addAll(this.getApplicantInfo());

        if (this.getAssigneeInfo() != null)
            personVOList.addAll(this.getAssigneeInfo());

        if (this.getCurrentApplicantInfo() != null)
            personVOList.addAll(this.getCurrentApplicantInfo());

        if (this.getCurrentRightHolderInfo() != null)
            personVOList.addAll(this.getCurrentRightHolderInfo());

        if (this.getRightHolderInfo() != null)
            personVOList.addAll(this.getRightHolderInfo());

        if (this.getRepresentApplicantInfo() != null)
            personVOList.addAll(this.getRepresentApplicantInfo());

        for (PersonVO personVO : personVOList) {
            String name = personVO.getOrgName();
            if (name == null) {
                name = personVO.getName();
                if (name == null) {
                    name = personVO.getOrgnameStandardized();
                    if (name != null)
                        names.add(normalizer.nameNorm(name));
                } else
                    names.add(normalizer.nameNorm(name));
            } else
                names.add(normalizer.nameNorm(name));

            name = personVO.getNameKR();
            if (name != null)
                names.add(normalizer.nameNorm(name));

            name = personVO.getNameEN();
            if (name != null)
                names.add(normalizer.nameNorm(name));

            name = personVO.getEnName();
            if (name != null)
                names.add(normalizer.nameNorm(name));

            name = personVO.getApplicantCodeKR();
            if (name != null)
                names.add(normalizer.nameNorm(name));

            name = personVO.getCode();
            if (name != null ) {
                if (!name.matches("^[0-9]{2}$"))
                    names.add(normalizer.nameNorm(name));
            }
        }

//        System.out.println(StringUtils.join(names, "||"));

        if (!names.isEmpty())
            return names;

        return null;
    }
}
