package es.module.kipris;

import es.model.Applicant;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.util.*;

public class KiprisAPI {
    public static Map<String, Object> xmlpath = new HashMap<String, Object>();
    public final static String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";

    public static List<Applicant> patentApplicantInfo(String applicationNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("applicantInfo", "//body/items/patentApplicantInfo");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/patUtiModInfoSearchSevice/patentApplicantInfo";
//        String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        String xmlResult = null;

        try {
            apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
//            System.out.println("RESULT => " + xmlResult);

//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpathMap.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpathMap.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    Applicant applicantVO = new Applicant();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("ApplicantNumber")) {
                            applicantVO.setCode(value);
                        } else if (name.matches("ApplicantName")) {
                            applicantVO.setName(value);
                        } else if (name.matches("ApplicantEnglishsentenceName")) {
                            applicantVO.setEnName(value);
                        } else if (name.matches("ApplicantAddress")) {
                            applicantVO.setAddress(value);
                        } else if (name.matches("ApplicantCountryName")) {
                            applicantVO.setCountry(value);
                        }
                    }

                    applicantVOS.add(applicantVO);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }

    public static List<Applicant> rpstApplicantInfoV2(String applicationNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("applicantInfo", "//body/items/rpstApplicantV2Info");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RpstApplicantService/rpstApplicantInfoV2";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        Applicant applicantVO = new Applicant();
        String xmlResult = null;

        try {
            apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
//            System.out.println("RESULT => " + xmlResult);

//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpathMap.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpathMap.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("rpstApplicantNumber")) {
                            applicantVO.setCode(value);
                        } else if (name.matches("rpstApplicantName")) {
                            applicantVO.setName(value);
                        } else if (name.matches("rpstApplicantNameEng")) {
                            applicantVO.setEnName(value);
                        }
                    }
                }
            }

            String applicantCode = applicantVO.getCode();

            if (applicantCode == null)
                return null;

            if (!applicantCode.contains("|")) {
                applicantVO.setSequence("1");
                applicantVOS.add(applicantVO);
            } else {
                String [] codes = applicantCode.split("\\|");
                String [] names_ko = applicantVO.getEnName().split("\\|");

                String [] names_en = null;
                if (applicantVO.getEnName() != null)
                    names_en = applicantVO.getEnName().split("\\|");

                for (int i=0,len=codes.length; i<len; i++) {
                    Applicant applicant = applicantVO.clone();
                    applicant.setSequence(String.valueOf(i));
                    applicant.setCode(codes[i]);
                    applicant.setName(names_ko[i]);

                    if (names_en != null)
                        if (i < names_en.length)
                            applicant.setEnName(names_en[i]);
                    applicantVOS.add(applicant);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }

    public List<Applicant> registrationLastRightHolderInfo(String registerNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("rightholderInfo", "//body/items/registrationLastRightHolderInfo");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationLastRightHolderInfo";
//        String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        String xmlResult = null;

        try {
            apiUrl += "?registrationNumber=" + registerNumber + "0000" + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
//            System.out.println("RESULT => " + xmlResult);

//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpathMap.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpathMap.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    Applicant applicantVO = new Applicant();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("lastRightHolderNumber")) {
                            applicantVO.setCode(value);
                        } else if (name.matches("lastRightHolderName")) {
                            applicantVO.setName(value);
                        } else if (name.matches("lastRightHolderAddress")) {
                            applicantVO.setAddress(value);
                        } else if (name.matches("lastRightHolderCountry")) {
                            applicantVO.setCountry(value);
                        }
                    }

                    applicantVOS.add(applicantVO);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }

    public List<Applicant> registrationRightHolderInfo(String registerNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("registrationRightHolderInfo", "//body/items/registrationLastRightHolderInfo");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationRightHolderInfo";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        String xmlResult = null;

        try {
            apiUrl += "?registrationNumber=" + registerNumber + "0000" + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
            System.out.println("RESULT => " + xmlResult);




        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }
}
