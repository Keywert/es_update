package es.module.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ESDataUpdate {
    public static void main(String[] args) {
        ESConfig esConfig = new ESConfig("es188");
        String index = "kipo";
        String type = "patent";

        String start = "20000101";
        String end = "20041231";

        String[] docIds = {"kr20247004345a"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(docIds))
//                .mustNot(QueryBuilders.existsQuery("claims.independentClaimText"))
                .filter(QueryBuilders.existsQuery("patentNumber"))
//                .filter(QueryBuilders.rangeQuery("patentDate").from(start).to(end))
                ;

        String[] fields = {
                "patentNumber"
        };

        DataFarmDB dataFarmDB = new DataFarmDB();

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
        ObjectMapper objectMapper = new ObjectMapper();
        List<QueueVO> queueVOList = new ArrayList<>();

        int count = 0;
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
                    JsonNode node = jsonNode.get("patentNumber");
                    if (node == null) {
                        System.out.println(hit.getId());
                        continue;
                    }

                    // 데이터 수정
                    String patentNumber = node.asText();

                    Map<String,Object> jsonMap = new HashMap<>();
                    jsonMap.put("patentNumber",  patentNumber);

                    String json = objectMapper.writeValueAsString(jsonMap);

                    System.out.println(hit.getId() + " : " + json);

//                    esConfig.bulkProcessor.add((new UpdateRequest(index, type, hit.getId())
//                            .doc(json)
//                            .upsert(json)));


//                    QueueVO queueVO = new QueueVO();
//                    queueVO.setEsindex(index);
//                    queueVO.setDocumentId(hit.getId());
//                    queueVO.setLevel(0);
//
//                    queueVOList.add(queueVO);
//                    if (queueVOList.size() > 10) {
//                        dataFarmDB.insertESQueue(queueVOList);
//                        queueVOList.clear();
//                    }

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

//            if (!queueVOList.isEmpty())
//                dataFarmDB.insertESQueue(queueVOList);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
