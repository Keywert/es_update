package es.module.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class BulkReindex_v2 {
    public static void main(String[] args) throws IOException {
        Logger logger = Logger.getLogger("Log");
        FileHandler fileHandler;


        if (args.length == 0) {
            System.out.println("Usage : java -jar reindex.jar [indexName] [startDate] [endDate]");
            return;
        }

        ESConfig es_src = new ESConfig("es189");
        ESConfig es_dest = new ESConfig("es209");

        String index = args[0];
        String type = "patent";

        String startDate = args[1];
        String endDate = args[2];

        System.out.println(index + " : " + startDate + " ~ " + endDate);

        String [] docIds = {"cn111732655b"};

        QueryBuilder qb = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.existsQuery("patentDate"))
//                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
                ;

        String[] fields = {"documentId"};//"description"};

        ArrayList<String> documentIds = new ArrayList<>();

        SearchResponse scrollResp = es_dest.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(3000)
                .execute()
                .actionGet();

        System.out.println("Destination Hits : " + scrollResp.getHits().getTotalHits());

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                documentIds.add(hit.getId());
            }
            scrollResp = es_dest.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .mustNot(QueryBuilders.existsQuery("patentDate"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
                .mustNot(QueryBuilders.idsQuery().addIds(documentIds))
                ;

        scrollResp = es_src.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(1)
//                .setFetchSource(includes,excludes)
                .execute()
                .actionGet();

        System.out.println("Start Reindex Count : " + scrollResp.getHits().getTotalHits());
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                if (++count % 100 == 0) {
                    System.out.println(count);
//                    try {
//                        Thread.sleep(15000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }

                JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());

                String json = objectMapper.writeValueAsString(jsonNode);
//                System.out.println(documentIds + " : " + json);
                es_dest.bulkProcessor.add(new IndexRequest(index, type, hit.getId()).source(json));

            }
            scrollResp = es_src.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        System.out.println("Finish Count : " + count);

        try {
            es_dest.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
