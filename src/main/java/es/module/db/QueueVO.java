package es.module.db;

import lombok.Data;

@Data
public class QueueVO {
    public String documentId;
    public String esindex;
    public Integer level;
}
