package es.module.db;

import es.config.DBHelper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class DataFarmDB {
    private DBHelper dbHelper;

    public DataFarmDB() {
        this.dbHelper = new DBHelper();
    }

    public void insertESQueue(List<QueueVO> queueList) {
        String table = "datafarm.ES_UPDATE_QUEUE";

        String insertQuery = "INSERT into " + table +
                " (" +
                "document_id, es_index, level" +
                ") " +
                " values(?,?,?) " +
                " on duplicate key update " +
                "    level = CASE WHEN(level > values(level)) THEN values(level) ELSE level END, " +
                "    update_date =  CURRENT_TIMESTAMP() ";

        PreparedStatement statement = null;
        try {
            statement = this.dbHelper.getConn_aurora2().prepareStatement(insertQuery);
            statement.clearParameters();

            for (QueueVO queue : queueList) {
                statement.setString(1, queue.getDocumentId());
                statement.setString(2, queue.getEsindex());
                statement.setInt(3, queue.getLevel());
                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Map<String, List<String>> selectQueue(int level, int queue_size) {
        String query = "select A.es_index,group_concat(A.document_id separator ' ## ') docIdStr from datafarm.ES_UPDATE_QUEUE " +
                " A inner join (select document_id, es_index from datafarm.ES_UPDATE_QUEUE where level = " + level + " order by level,es_index limit " + queue_size + ") B " +
                " on A.document_id = B.document_id and A.es_index = B.es_index " +
                " group by A.es_index ";

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, List<String>> map = new HashMap<>();
            while (rs.next()) {
                String es_index = rs.getString("es_index");
                if (es_index == null)
                    continue;

                String docIdStr = rs.getString("docIdStr");
                if (docIdStr == null)
                    continue;
                String[] docIds = docIdStr.split(" ## ");
                if (docIds.length != 0)
                    map.put(es_index, Arrays.asList(docIds));
            }

            if (!map.isEmpty())
                return map;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String selectClusterInfo(String index, int type) {
        String table = "datafarm.ES_CONFIG";
        String query = "select es_cluster from " + table + " where es_index = '" + index + "' and  es_type = " + type;

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, List<String>> map = new HashMap<>();
            while (rs.next()) {
                String es_cluster = rs.getString("es_cluster");
                if (es_cluster == null)
                    continue;

                return es_cluster;
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateDB(Map<String, List<String>> map) {
        String table = "datafarm.ES_UPDATE_QUEUE";

        String query = "UPDATE " + table + " SET level = 99 " +
                " WHERE document_id = ? and es_index = ? ";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = this.dbHelper.getConn_aurora2().prepareStatement(query);
            preparedStatement.clearParameters();

            for (String index : map.keySet()) {
                for (String docId : map.get(index)) {
                    preparedStatement.setString(1, docId);
                    preparedStatement.setString(2, index);

                    preparedStatement.addBatch();
                }
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public List<String> selectTaskList(int task_type) {
        String table = "datafarm.ES_TASK";
        String query = "select task from " + table + " where status is null and task_type = " + task_type + " order by level ";

        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<String> list = new ArrayList<>();


            while (rs.next()) {
                String task = rs.getString("task");
                if (task == null)
                    continue;

                list.add(task);
            }

            if (!list.isEmpty())
                return list;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void updateTask(String taskName, Integer status) {
        String tableName = "datafarm.ES_TASK";
        String query = "UPDATE " + tableName + " SET status = ?" +
                " WHERE task = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = dbHelper.getConn_aurora2().prepareStatement(query);
            preparedStatement.clearParameters();

            preparedStatement.setInt(1, status);
            preparedStatement.setString(2, taskName);

            preparedStatement.addBatch();


            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertESQueue(String index, Integer level, Set<String> keySet) {
        String table = "datafarm.ES_UPDATE_QUEUE";

        String insertQuery = "INSERT into " + table +
                " (" +
                "document_id, es_index, level" +
                ") " +
                " values(?,?,?) " +
                " on duplicate key update " +
                "    level = CASE WHEN(level > values(level)) THEN values(level) ELSE level END, " +
                "    update_date =  CURRENT_TIMESTAMP() ";

        PreparedStatement statement = null;
        try {
            statement = this.dbHelper.getConn_aurora2().prepareStatement(insertQuery);
            statement.clearParameters();

            for (String docId : keySet) {
                statement.setString(1, docId);
                statement.setString(2, index);
                statement.setInt(3, level);
                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<String> selectQueueByIndex(String index, int level) {
        String query = "select document_id from datafarm.ES_UPDATE_QUEUE where es_index = '" + index + "' and level = " + level + "  ";

        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> docIdSet = new HashSet<>();
            while (rs.next()) {
                String document_id = rs.getString("document_id");
                if (document_id == null)
                    continue;

                docIdSet.add(document_id);
            }

            if (!docIdSet.isEmpty())
                return docIdSet;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Set<String> selectExceptFamily() {
        String query = "select extendFamilyId from family.opo_extended_error ";

        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> idSet = new HashSet<>();
            while (rs.next()) {
                String extendFamilyId = rs.getString("extendFamilyId");
                if (extendFamilyId == null)
                    continue;

                idSet.add(extendFamilyId);
            }

            if (!idSet.isEmpty())
                return idSet;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<String> selectOSQueue(int queue_size) {
        String query = "select document_id,es_index from datafarm.ES_OS_UPDATE_QUEUE where level=1 limit " + queue_size;

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<String> list = new ArrayList<>();
            while (rs.next()) {
                String documentId = rs.getString("document_id");
                if (documentId == null)
                    continue;

                list.add(documentId);
            }

            if (!list.isEmpty())
                return list;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }


    public List<String> selectClassQueue(int queue_size) {
        String query = "select document_id,es_index from datafarm.ES_CLASS_UPDATE_QUEUE where level=1 limit " + queue_size;

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<String> list = new ArrayList<>();
            while (rs.next()) {
                String documentId = rs.getString("document_id");
                if (documentId == null)
                    continue;

                list.add(documentId);
            }

            if (!list.isEmpty())
                return list;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<String> selectSeqQueue(int queue_size) {
        String query = "select document_id,es_index from datafarm.ES_SEQUENCE_UPDATE where level=0 limit " + queue_size;

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<String> list = new ArrayList<>();
            while (rs.next()) {
                String documentId = rs.getString("document_id");
                if (documentId == null)
                    continue;

                list.add(documentId);
            }

            if (!list.isEmpty())
                return list;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void seqUpdateDB(List<String> docIds) {
        String table = "datafarm.ES_SEQUENCE_UPDATE";

        String query = "UPDATE " + table + " SET level = 99 " +
                " WHERE document_id = ? ";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = this.dbHelper.getConn_aurora2().prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : docIds) {
                preparedStatement.setString(1, docId);
                preparedStatement.addBatch();
            }


            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void classUpdateDB(String table, List<String> docIds) {
        String query = "UPDATE " + table + " SET level = 99 " +
                " WHERE document_id = ? ";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = this.dbHelper.getConn_aurora2().prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : docIds) {
                preparedStatement.setString(1, docId);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertESClassQueue(String table, List<QueueVO> queueList) {
        String insertQuery = "INSERT into " + table +
                " (" +
                "document_id, es_index, level" +
                ") " +
                " values(?,?,?) " +
                " on duplicate key update " +
                "    level = CASE WHEN(level > values(level)) THEN values(level) ELSE level END, " +
                "    update_date =  CURRENT_TIMESTAMP() ";

        PreparedStatement statement = null;
        try {
            statement = this.dbHelper.getConn_aurora2().prepareStatement(insertQuery);
            statement.clearParameters();

            for (QueueVO queue : queueList) {
                statement.setString(1, queue.getDocumentId());
                statement.setString(2, queue.getEsindex());
                statement.setInt(3, queue.getLevel());
                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<String> selectCitationQueue(int queue_size) {
        String query = "select citation_id,es_index from datafarm.ES_CITATION_UPDATE " +
                " where level=0  limit " + queue_size;

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<String> list = new ArrayList<>();
            while (rs.next()) {
                String citation_id = rs.getString("citation_id");
                if (citation_id == null)
                    continue;

                list.add(citation_id);
            }

            if (!list.isEmpty())
                return list;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void insertESCitationQueue(String index, Integer level, Set<String> keySet) {
        String table = "datafarm.ES_CITATION_UPDATE";

        String insertQuery = "INSERT into " + table +
                " (" +
                "citation_id, es_index, level" +
                ") " +
                " values(?,?,?) " +
                " on duplicate key update " +
                "    level = CASE WHEN(level > values(level)) THEN values(level) ELSE level END, " +
                "    update_date =  CURRENT_TIMESTAMP() ";

        PreparedStatement statement = null;
        try {
            statement = this.dbHelper.getConn_aurora2().prepareStatement(insertQuery);
            statement.clearParameters();

            for (String docId : keySet) {
//                System.out.println(docId);
                statement.setString(1, docId);
                statement.setString(2, index);
                statement.setInt(3, level);
                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void citationUpdateDB(List<String> docIds) {
        String table = "datafarm.ES_CITATION_UPDATE";

        String query = "UPDATE " + table + " SET level = 98 " +
                " WHERE citation_id = ? ";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = this.dbHelper.getConn_aurora2().prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : docIds) {
                preparedStatement.setString(1, docId);
                preparedStatement.addBatch();
            }


            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<String> selectTotalDocId(String index) {
        String query = "select document_id from datafarm.ES_UPDATE_QUEUE where document_id regexp '^cn1[1-9][7-9].+$' and es_index='" + index
                + "' and level !=99 and level = 9";

//        System.out.println(query);
        Statement st = null;

        try {
            st = this.dbHelper.getConn_aurora2().createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> set = new HashSet<>();
            while (rs.next()) {
                String documentId = rs.getString("document_id");
                if (documentId == null)
                    continue;

                set.add(documentId);
            }

            if (!set.isEmpty())
                return set;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String getORG(String documentId) {
        if (documentId.matches("^kr.+$"))
            return "kipo";
        else if (documentId.matches("^usps.+$"))
            return "uspto_past";
        else if (documentId.matches("^jpps.+$"))
            return "jpo_past";
        else if (documentId.matches("^us.+$"))
            return "uspto";
        else if (documentId.matches("^jp.+$"))
            return "jpo";
        else if (documentId.matches("^paj.+$"))
            return "paj";
        else if (documentId.matches("^wo.+$"))
            return "pct";
        else if (documentId.matches("^ep.+$"))
            return "epo";
        else if (documentId.matches("^cn.+$"))
            return "sipo";
        else if (documentId.matches("^de.+$"))
            return "dpma";
        else if (documentId.matches("^au.+$"))
            return "aupo";
        else if (documentId.matches("^ca.+$"))
            return "capo";
        else if (documentId.matches("^fr.+$"))
            return "frpo";
        else if (documentId.matches("^gb.+$"))
            return "gbpo";
        else if (documentId.matches("^in.+$"))
            return "inpo";
        else if (documentId.matches("^it.+$"))
            return "itpo";
        else if (documentId.matches("^ru.+$"))
            return "rupo";
        else if (documentId.matches("^tw.+$"))
            return "tipo";
        else if (documentId.matches("^[A-Z]{2}.+$"))
            return "docdb";

        return null;
    }

    public String getClusterNameByDocId(String documentId) {
        String index = getORG(documentId);
        return this.selectClusterInfo(index, 1);
    }

}