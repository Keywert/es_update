package es.module.datafarm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import es.model.bulk.Patent;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ESData {

    public List<Patent> getPatent(ESConfig esConfig, String index, String appNum) {
        if (appNum == null)
            return null;

        List<String> docIds = kipoDocIds(appNum);
        if (docIds == null)
            return null;

        String type = "patent";
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .mustNot(QueryBuilders.existsQuery("personInfoUpdateDate"))
                .filter(QueryBuilders.idsQuery().addIds(docIds));

        String[] fields = {"currentApplicantInfo", "applicantInfo", "documentId", "applicationNumber", "registerNumber"};
        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        ObjectMapper objectMapper = new ObjectMapper();

        List<Patent> list = new ArrayList<>();

        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    Patent patent = objectMapper.readValue(hit.getSourceAsString(), Patent.class);
                    if (patent == null)
                        continue;

                    list.add(patent);
                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!list.isEmpty())
                return list;


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }


    private static List<String> kipoDocIds(String applicationNumber) {
        List<String> docIds = new ArrayList<>();

        if (applicationNumber.matches("^(10|20)[0-9]{4}07[0-9]{5}$"))
            applicationNumber = applicationNumber.substring(0, 6) + "70" + applicationNumber.substring(8);

        if (applicationNumber.matches("^10.+")) {
            docIds.add("kr" + applicationNumber.substring(2) + "a");
            docIds.add("kr" + applicationNumber.substring(2) + "b1");
        } else if (applicationNumber.matches("^20.+")) {
            docIds.add("kr" + applicationNumber.substring(2) + "u");
            docIds.add("kr" + applicationNumber.substring(2) + "y1");
        } else
            return null;


        return docIds;
    }

}
