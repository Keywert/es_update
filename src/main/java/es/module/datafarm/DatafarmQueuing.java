//package es.module.datafarm;
//
//import es.config.DBHelper;
//import es.config.ESConfig;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.action.search.SearchType;
//import org.elasticsearch.common.unit.TimeValue;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.search.SearchHit;
//
//import java.awt.*;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//public class DatafarmQueuing {
//    public  List<Queue2Data> priorityFind(String index, Set<String> docIds) {
//
//        ESConfig esConfig = new ESConfig("es206");
//        List<Queue2Data> queuing_docid = new ArrayList<>();
//
//        if (docIds.isEmpty())
//            return null;
//
//        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.idsQuery().addIds(docIds));
//
//        System.out.println(bqb);
//
//        String[] fields = {"documentId"};//"description"};
//        Set<String> updateDocIds = new HashSet<>();
//
//        String type = "patent";
//
//        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
//                .setTypes(type)
//                .setSearchType(SearchType.SCAN)
//                .setScroll(new TimeValue(200000))
//                .setQuery(bqb)
//                .setFetchSource(fields, null)
//                .setSize(1000)
//                .execute()
//                .actionGet();
//
//        System.out.println("Destination Hits : " + scrollResp.getHits().getTotalHits());
//
//        do {
//            for (SearchHit hit : scrollResp.getHits().getHits()) {
//                updateDocIds.add(hit.getId());
//                queuing_docid.add(new Queue2Data(hit.getId(),2,index));
//            }
//            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
//        } while (scrollResp.getHits().getHits().length != 0);
//
//        docIds.removeAll(updateDocIds);
//        for (String docId : docIds) {
//            queuing_docid.add(new Queue2Data(docId,1,index));
//        }
//
//
//        return queuing_docid;
//
////        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
////                .filter(QueryBuilders.idsQuery().addIds(ids));
////
////
////        SearchResponse response = esConfig.client.prepareSearch(indexies)
////                .setTypes("patent")
////                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
////                .setQuery(bqb)
////                .setFetchSource(fields, null)
////                .setFrom(200)
////                .setSize(200)
////                .execute()
////                .actionGet();
////
////        for (SearchHit hit : response.getHits().getHits()) {
//
////            System.out.println(hit.getId() + " : " + hit.getSourceAsString());
////        }
//
//
//    }
//
//
//    public void queueByDocid(List<Queue2Data> queue2Data) {
//        DBHelper dbHelper = new DBHelper();
//        Connection conn = dbHelper.getConn_datafarm();
//
//
//        Statement statement = null;
//
//        try {
//            int count = 0;
//
//            statement = conn.createStatement();
//
//            for(Queue2Data data : queue2Data)  {
//
//                String query = "INSERT into queue2_" + data.index + " " +
//                        "(document_id ,priority ,insert_time ,state) " +
//                        " values(" + "'" + data.docid + "'" + "," + data.priority + "," + "now()" + ",'wait') " +
//                        " on duplicate key update " +
//                        " insert_time = IF( state='finish', values(insert_time),insert_time)," +
//                        " priority = IF(state='finish',values(priority), IF(priority>values(priority),values(priority),priority))," +
//                        " state = values(state);";
//
//                statement.addBatch(query);
//
//                if (++count % 5000 == 0) {
//                    statement.executeBatch();
//                    statement.clearBatch();
//                    System.out.println(count);
//                }
//            }
//            statement.executeBatch();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
