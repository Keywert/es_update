package es.module.dataproc.norm;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class StdNameNorm {
    public static Set<String> countrySet;

    public StdNameNorm() {
        this.countrySet = CountryData.countrySet;
    }

    /**
     * 1. 소문자 -> 대문자
     * 2. 국가명/도시명 삭
     * 3. 특수문자제거
     * 4. 문자통일
     * 5.
     */

    public String stdNameProc (String name){
        if (name != null && !"".equals(name)) {
            if (name.matches(".*[A-z].*"))
                return enNameSTD(name);
            else {
                String etcName = koNameNorm(name);
                return enNameSTD(etcName);
            }
        }

        return name;
    }

    public String enNameSTD(String name_orign) {
        if (name_orign == null)
            return null;

//        System.out.println("0 : " + name_orign.trim());

        // 1. 대소문자 및 띄어쓰기 갯수 맞춤
        String name = name_orign
                .replaceAll("\\s+", " ")
                .replaceAll("(\\.){2,3}", ".")
                .replaceAll(" \\. ", " ")
                .trim()
                .toUpperCase();

//        System.out.println("1 : " + name);


        // 2. 국가/도시 이름 ,, 콤마사이 데이터 처리
        if (name.matches("^(\\(|（).+(\\)|）)$"))
            name = name.replaceAll("[\\(|（|\\)|）]", " ")
                    .replaceAll("\\s+", " ")
                    .trim();


        name = name
//                .replaceAll("^[0-9|\\-]+\\s", "")
                .replaceAll("(\\(|（).+(\\)|）)", "")
//                .replaceAll("(,|;)(\\s)?[\\s|0-9|A-z|\\-]+(\\s)?(,)", ",")
//                .replaceAll("(,|;)(\\s)?[\\s|0-9|A-z|\\-]+(\\s)?(,)", ",")
//                .replaceAll("(,|;)(\\s)?[\\s|0-9|A-z|\\-]+(\\s)?(,)", ",")
//                .replaceAll("(,)?AM\\s[A-z]+$","")
        ;

        name = name
                .replaceAll("(,|;)\\s(SWEDEN|JAN)$", "")
        ;

//        System.out.println("2 : " + name);


        // 3. 특수문자 전처리 및 제거
        name = name
                .replaceAll("(è|é|É)", "E")
                .replaceAll("(à|ä|À|Ä|Å|Ã)", "A")
                .replaceAll("(ö|Ô|Ö)", "O")
                .replaceAll("(ü|Ú|Ü)", "U")
                .replaceAll("(î|Í)", "I")
                .replaceAll("(Ɫ)","L")
                .replaceAll("(,)", " ")
                .replaceAll("\\s+", " ")
                .replaceAll("\\sRESEARCH", " RES")
                .replaceAll("\\sINSTITUTE", " INS")
                .replaceAll("\\s(LLC)$","")
                .replaceAll("\\s&\\sCO"," &CO")
                .trim();

//        System.out.println("3 : " + name);

        List<String> nameList = new ArrayList<>();
        nameList.add("REPUBLIC OF KOREA");
//        nameList.add("FOCKE & CO.");
//        nameList.add("GOLDMAN SACHS & CO.");


        if (nameList.contains(name)) {
//            System.out.println();
            return name;
        }

        // 3. 중간 불용어 제거

//        name = name.replaceAll("GMBH\\s.+$", "");

        // 중간단어 제거
        name = name.replaceAll("\\s(CORP|INC|LLC|CO|LTD|KG|AG|PTE|A/S|IND|ITD|LIMITED|PTY|TECH|MFG" +
                        "|UNLIMITED|U\\.S\\.A|SRL|SPA|SE AND CO\\. KG|SAS|SARL|SA DE CV" +
                        "|SA|S\\.R\\.L\\.|S\\.R\\.L|S\\.P\\.A|S\\.L|S\\.A\\.U\\.|S\\.A\\.S\\.|S\\.A\\.S|S\\.A\\.R\\.L|S\\.A\\. DE C\\.V" +
                        "|S\\.A\\.|S\\.A R\\.L|S\\.A|S A|RANDAMP|PTY|PTE|OY AB|OY|NV|NATIONAL UNIVERSITY CORPORATION|N\\.V\\.|N\\.V" +
                        "|N V|MBH AND CO\\. KG|MBH|LTDA|LTD\\.STI|LTD|LT|LP|LLC|LIMITED|L\\.P\\.|L\\.P|L\\.L\\.C|KK|KGAA|KABUSHIKI KAISHA|KABUSHIKI KAISAH" +
                        "|K\\.K\\.|K\\.K|K K|INCORPORATED|INC\\.|INC|GMBH AND CO\\. KG|GMBH AND CO|GMBH|GESELLSCHAFT|G\\.M\\.B\\.H" +
                        "|E\\.V|DUESSELDORF|DEUT|DBA|D\\.B\\.A\\.|CORPORATION|CORPORATE|CORP|COR|COMPANY|COMP|CO\\.|CO UNITED INC" +
                        "|CO LP|CO KG|CO INC|CO|C\\.V|BY|B\\.Y\\.|B\\.V\\.|B\\.V|B\\.B|B Y|AS|AND코카케|AND코카게|ANDLT;|ANDGT;|ANDAMP;" +
                        "|AND COMPANY|AND CO|AND C\\.|AKTIENGESELLSCHAFT|AKTIENGESELLSCHAF|AKTIEBOLAG|AKT|AK|AG CO OHG|AG AND CO" +
                        "|AG|AB\\.|AB|A/S|:KK" +
                        ")(\\.)?\\s"
                , " ")
                .replaceAll("\\s+", " ").trim()
        ;

        // 종결단어 제
        name = name.replaceAll("\\s(CO\\. LTD\\.|CO\\. LTD|CORP|INC|LLC|CO|LTD|KG|AG|PTE|A/S|IND|ITD|LIMITED|PTY|TECH|MFG" +
                        "|UNLIMITED|U\\.S\\.A|SRL|SPA|SE AND CO\\. KG|SAS|SARL|SA DE CV" +
                        "|SA|S\\.R\\.L\\.|S\\.R\\.L|S\\.P\\.A|S\\.L|S\\.A\\.U\\.|S\\.A\\.S\\.|S\\.A\\.S|S\\.A\\.R\\.L|S\\.A\\. DE C\\.V" +
                        "|S\\.A\\.|S\\.A R\\.L|S\\.A|S A|RANDAMP|PTY|PTE|OY AB|OY|NV|NATIONAL UNIVERSITY CORPORATION|N\\.V\\.|N\\.V" +
                        "|N V|MBH AND CO\\. KG|MBH|LTDA|LTD\\.STI|LTD|LT|LP|LLC|LIMITED|L\\.P\\.|L\\.P|L\\.L\\.C|KK|KGAA|KABUSHIKI KAISHA" +
                        "|K\\.K\\.|K\\.K|K K|INCORPORATED|INC\\.|INC|GMBH AND CO\\. KG|GMBH AND CO|GMBH|GESELLSCHAFT|G\\.M\\.B\\.H" +
                        "|E\\.V|DUESSELDORF|DEUT|DBA|D\\.B\\.A\\.|CORPORATION|CORPORATE|CORP|COR|COMPANY|COMP|CO\\.|CO UNITED INC" +
                        "|CO LP|CO KG|CO INC|CO|C\\.V|BY|B\\.Y\\.|B\\.V\\.|B\\.V|B\\.B|B Y|AS|AND코카케|AND코카게|ANDLT;|ANDGT;|ANDAMP;" +
                        "|AND COMPANY|AND CO|AND C\\.|AKTIENGESELLSCHAFT|AKTIENGESELLSCHAF|AKTIEBOLAG|AKT|AK|AG CO OHG|AG AND CO" +
                        "|AG|AB\\.|AB|A/S|:KK" +
                        ")(\\.)?$"
                , "");

//        System.out.println("4 : " + name);

        if (name.matches("^[A-z]{1,3}\\s&\\s[A-z]{1,4}$"))
            return name;

//        System.out.println(name);
        // 예외 단어 제거
        name = name.replaceAll("KABUSHIKI (KAISHA|KAIHSA|KAISA)", "")
//                .replaceAll("\\sA\\s(OF )?[A-z|\\.]+$","")
                .replaceAll("\\sA\\s(CORPOF )?(GERMANY)$", "")
                .replaceAll("\\s(A OF)$", "")
                .replaceAll("\\s(A)$", "")
                .trim()
        ;


//        System.out.println("5 : " + name);

        if (name.contains("-")) {
            List<String> list = new ArrayList<>();
            String[] arr = name.split("\\s");

            list.add(arr[0]);

            if (arr.length > 1)
                for (int i = 1, len = arr.length; i < len; i++)
                    list.add(arr[i].replaceAll("-", " "));

            name = String.join(" ", list);
        }

        name = name
//                   .replaceAll("(-)"," ")
                .replaceAll("[(|)|!|#|%|:|@]", " ")
                .replaceAll("[\"]", " ")
//                .replaceAll("[\"|\']", " ")
                .replaceAll("\\s+", " ")
                .replaceAll("\\s(&|BV|SR|SP|S\\.|S\\. A\\.)$", "")
                .replaceAll("\\s&CO"," & CO")
                .replaceAll("\\sCO\\.$", " CO")
                .trim()
        ;


        // 예외를 제외한 국가 이름 제거
        if (!name.matches(".*(BANK|NATIONAL|AIR).*")){
            if (!countrySet.isEmpty()) {
                String countryPattern = "\\s(A )?(OF|IN)?\\s?(" + StringUtils.join(countrySet, "|") + ")$";
//                System.out.println(countryPattern);
                name = name.replaceAll(countryPattern, "");
            }

//            if (!citySet.isEmpty()) {
//                String ctyPattern = "\\s(OF|IN)?\\s?(" + StringUtils.join(citySet, "|") + ")$";
//                System.out.println(ctyPattern);
//                name = name.replaceAll(ctyPattern, "");
//            }
        }

//        System.out.println("6 : " + name);

        // 특이 케이 변환
        name = name
                .replaceAll("D/B/A", "DBA")
                .replaceAll(" DBA(\\s|$)"," ")
                .replaceAll("^INC\\. ","")
                .replaceAll("\\.$","")
                .replaceAll("^CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE CNRS$", "CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE")
                .trim()
        ;

        if (name.matches("^(AT & T|AT T)$"))
            name = "AT&T";


//        System.out.println("F : " + name);

//        if (name.isEmpty())
//            return name_orign.replaceAll("\\s+"," ").toUpperCase().trim();

        return name;
    }

    public String enNameNorm(String name_orign) {
        if (name_orign == null)
            return null;

        String name = name_orign;

/**
 *  1. 소문자 -> 대문자
 *  2. 띄어쓰기 1칸으로 변경
 *  3. 특수문자제거
 *  4. 문자통일
 *  5.
 */
        name = name.toUpperCase()
                .replaceAll("\\s+", " ")
                .replaceAll(",\\.", ".,")
                .replaceAll(",$", "")
                .replaceAll(",\\s?ET AL\\.?$", "")
                .replaceAll(",? KG\\. ", " KG ")
                .replaceAll(",? KG\\.$", " KG")
                .replaceAll(",? AG\\. ", " AG ")
                .replaceAll(",? AG\\.$", " AG")
                .replaceAll(",? PTE\\. ", " PTE ")
                .replaceAll(",? PTE\\.$", " PTE")
                .replaceAll(",? LLC\\. ", " LLC ")
                .replaceAll(",? LLC\\.?$", " LLC")
                .replaceAll(",? LNC\\. ", " LNC ")
                .replaceAll(",? LNC\\.$", " LNC")
                .replaceAll(",? A/S ", " AS ")
                .replaceAll(",? A/S$", " AS")
                .replaceAll(",? IND\\. ", " IND ")
                .replaceAll(",? IND\\.$", " IND")
                .replaceAll(",? ITD\\. ", " ITD ")
                .replaceAll(",? ITD\\.?$", " ITD")
                .replaceAll(",? LTD\\. ", " LTD ")
                .replaceAll(",? LTD\\.?$", " LTD")
                .replaceAll(",LTD\\.?$", " LTD")
                .replaceAll(",? LIMITED$", " LIMITED")
                .replaceAll(",? INC\\. ", " INC ")
                .replaceAll(",? INC\\.?$", " INC")
                .replaceAll(",INC\\.?$", " INC")
                .replaceAll(",? INT\\. ", " INT ")
                .replaceAll(",? INT\\.$", " INT")
                .replaceAll(",? PTY\\. ", " PTY ")
                .replaceAll(",? PTY\\.$", " PTY")
                .replaceAll(",? CO\\. ", " CO ")
                .replaceAll(",? CO\\.$", " CO")
                .replaceAll(",? CORP\\. ", " CORP ")
                .replaceAll(",? CORP\\.$", " CORP")
                .replaceAll(",? TECH\\. ", " TECH ")
                .replaceAll(",? TECH\\.$", " TECH")
                .replaceAll(",? MFG\\. ", " MFG ")
                .replaceAll(",? MFG\\.$", " MFG")
                .replaceAll(",? S\\.A\\.R\\.L\\. ", " SARL ")
                .replaceAll(",? S\\.A\\.R\\.L\\.?$", " SARL")
                .replaceAll(",? G\\.M\\.B\\.H\\. ", " GMBH ")
                .replaceAll(",? G\\.M\\.B\\.H\\.?$", " GMBH")
                .replaceAll(",? S\\.A\\.S\\. ", " SAS ")
                .replaceAll(",? S\\.A\\.S\\.?$", " SAS")
                .replaceAll(",? S\\.R\\.L\\. ", " SRL ")
                .replaceAll(",? S\\.R\\.L\\.?$", " SRL")
                .replaceAll(",? L\\.L\\.C\\. ", " LLC ")
                .replaceAll(",? L\\.L\\.C\\.$", " LLC")
                .replaceAll(",? S\\.P\\.A\\. ", " SPA ")
                .replaceAll(",? S\\.P\\.A\\.?$", " SPA")
                .replaceAll(",? P\\.L\\.C\\. ", " PLC ")
                .replaceAll(",? P\\.L\\.C\\.?$", " PLC")
                .replaceAll(",? L\\.?\\s?P\\. ", " LP ")
                .replaceAll(",? L\\.?\\s?P\\.$", " LP")
                .replaceAll(",? N\\.?\\s?V\\. ", " NV ")
                .replaceAll(",? N\\.?\\s?V\\.$", " NV")
                .replaceAll(",? B\\.?\\s?V\\. ", " BV ")
                .replaceAll(",? B\\.?\\s?V\\.$", " BV")
                .replaceAll(",? E\\.?\\s?V\\. ", " EV ")
                .replaceAll(",? E\\.?\\s?V\\.$", " EV")
                .replaceAll(",? C\\.?\\s?V\\. ", " CV ")
                .replaceAll(",? C\\.?\\s?V\\.$", " CV")
                .replaceAll(",? K\\.?\\s?K\\. ", " KK ")
                .replaceAll(",? K\\.?\\s?K\\.$", " KK")
                .replaceAll(",? E\\.?\\s?R\\. ", " ER ")
                .replaceAll(",? E\\.?\\s?R\\.$", " ER")
                .replaceAll(",? S\\.?\\s?A\\. ", " SA ")
                .replaceAll(",? S\\.?\\s?A\\.$", " SA")
                .replaceAll(",? S\\.?\\s?L\\. ", " SL ")
                .replaceAll(",? S\\.?\\s?L\\.$", " SL")
                .replaceAll("\\sLIMITED\\s", " LTD ")
                .replaceAll("\\sLIMITED\\.?$", " LTD")
                .replaceAll("\\sINCORPORATED\\s", " INC ")
                .replaceAll("\\sINCORPORATED", " INC")
                .replaceAll("\\s(CORPORATION|CORP)\\s", " CO ")
                .replaceAll("\\s(CORPORATION|CORP)\\.?$", " CO")
                .replaceAll("\\sINDUSTRIES LTD\\s", " IND LTD ")
                .replaceAll("\\sINDUSTRIES LTD$", " IND LTD")
                .replaceAll("(KABUSHIKI\\s?KAISHA|AKTIENGESELLSCHAFT)", "")
                .replaceAll("AKTIENGESELLSCHAFT", "AG")
                .replaceAll("(\\sDENKI|\\sDENKO)", " ELECTRIC")
                .replaceAll("\\sCOMPANY\\s(LTD|LP|INC|LLC|GMBH|SA|BV|NV)\\.?\\s", " COMPANY ")
                .replaceAll("\\sCOMPANY\\s(LTD|LP|INC|LLC|GMBH|SA|BV|NV)\\.$", " COMPANY")
                .replaceAll("\\sLTD\\sCOMPANY\\s", " COMPANY ")
                .replaceAll("\\sLTD\\sCOMPANY\\s$", " COMPANY")
                .replaceAll("\\s?-\\s?", " ")
                .replaceAll("\\(PUBL\\)", "")
                .replaceAll("\\sL\\sM\\s", " LM ")
                .replaceAll("\\s\\.\\s", " ")
//                .replaceAll("\\s?&\\s?","&")
                .replaceAll("\\s+", " ")
                .trim()
        ;


//        System.out.println(name);

        return name;
    }

    public String koNameNorm(String name_orign) {
        String name = name_orign;

/**
 *  1. 소문자 -> 대문자
 *  2. 띄어쓰기 1칸으로 변경
 *  3. 특수문자제거
 *  4. 문자통일
 *  5.
 */
        name = name.toUpperCase()
                .replaceAll("\\s+", " ")
                .replaceAll("-", " ")
                .replaceAll("．", ".")
                .replaceAll(",?\\s?에스\\.\\s?에이\\.\\s?알\\.\\s?엘\\.?$", "")
                .replaceAll(",?\\s?에스\\.\\s?에이\\.\\s?에스\\.?", "")
                .replaceAll(",?\\s?에스\\.아\\.에스\\.", "")
                .replaceAll(",?\\s?에스\\.피\\.에이\\.", "")
                .replaceAll(",?\\s?에스\\.에이\\.유\\.", "")
                .replaceAll("에스이 운트 코\\. 케이지\\.", "")
                .replaceAll("게엠베하 운트 코\\.\\s?카게[,|\\.]?", "")
                .replaceAll("게엠베하 운트 코퍼레이션\\.", "")
                .replaceAll("게엠베하 앤드 코\\. 카게", "")
                .replaceAll("게엠베하 엔에프게\\.카게", "")
                .replaceAll("게엠베하 운트 체오\\. 카케", "")
                .replaceAll("게엠베하운트콤파니카게", "")
                .replaceAll("게엠베하 운트 콤파니\\.\\s?카게", "")
                .replaceAll("게엠베하 운트 체오\\.\\s?카게[,|\\.]?", "")
                .replaceAll("게엠베하 & 코\\.\\s?카게[,|\\.]?", "")
                .replaceAll("게엠베하 ＆ 컴퍼니 카게[,|\\.]?", "")
                .replaceAll("게엠베하 운트 코\\.", "")
                .replaceAll("게엠베하 운트 (컴파니|콤파니) 카게[,|\\.]?", "")
                .replaceAll("악티엔게젤샤프트 운트 콤파니 카케[,|\\.]?", "")
                .replaceAll("가부시끼가이샤\\.?", "")
                .replaceAll(",?\\s?(가부시키 가이샤|가부시키가이샤|엘엘씨|인코포레이티드|리미티드|코포레이션|소시에테 아노님|악티엔게젤샤프트|인코포레이티드|아크티엔게젤샤프트|악티에볼라그|게엠베하)[,|\\.]?", "")
                .replaceAll(",?\\s?엘\\.피\\.", "")
                .replaceAll(",?\\s?인크\\.", "")
                .replaceAll(",?\\s?비\\.\\s?부이\\.", "")
                .replaceAll(",?\\s?비\\.\\s?브이\\.", "")
                .replaceAll(",?\\s?베\\.\\s?(뷔|붸)\\.", "")
                .replaceAll(",?\\s?에스\\.\\s?에이\\.", "")
                .replaceAll(",?\\s?씨\\.\\s?브이\\.", "")
                .replaceAll(",?\\s?에스\\.페\\.아\\.", "")
                .replaceAll(",?\\s?에스\\.페\\.아\\.", "")
                .replaceAll(",?\\s?에스\\.\\s?피\\.\\s?에이\\.", "")
                .replaceAll(",?\\s?케이\\.\\s?케이\\.", "")
                .replaceAll("에이\\/에스", "")
                .replaceAll("에이\\/에스", "")
                .replaceAll("에이\\/에스", "")
                .replaceAll(",?\\s?에스\\.에이\\.에스\\.", "")
                .replaceAll("잉크\\.$", "")
                .replaceAll("운트 코\\. 카게아아", "")
                .replaceAll(",?\\s?엘\\.엘\\.씨\\.?$", "")
                .replaceAll(",?\\s?엘\\.티\\.디\\.?$", "")
                .replaceAll(",?\\s?에스\\.\\s?알\\.\\s?엘\\.?$", "")
                .replaceAll(",?\\s?엘\\.\\s?엘\\.\\s?시\\.?$", "")
                .replaceAll(",?\\s?이\\.\\s?피\\.\\s?에이\\.?$", "")
                .replaceAll(",?\\s?에스\\.\\s?에이\\.?$", "")
                .replaceAll(",?\\s?에스\\.\\s?엘\\.?$", "")
                .replaceAll(",?\\s?에스\\.\\s?아\\.?\\s", "")
                .replaceAll(",?\\s?엘\\.\\s?피\\.?\\s", "")
                .replaceAll(",?\\s?(아이엔씨|엘티디|컴퍼니|캄파니|씨오|피티이|코프|피티와이)(^\\s)", "")
                .replaceAll(",?\\s?(아이엔씨|엘티디|컴퍼니|캄파니|컴패니|씨오|피티이|코|코프|피티와이)\\.$", "")
                .replaceAll(",?\\s?(에스|에세)\\.\\s?(에이|아)\\.?$", "")
                .replaceAll(",?\\s?(에스|에쎄)\\.\\s?(삐|페|피)\\.\\s?(아|에이)\\.?$", "")
                .replaceAll(",?\\s?엘\\.\\s?피\\.?$", "")
                .replaceAll(",?\\s?에이\\.\\s?에스\\.?$", "")
                .replaceAll(",?\\s?엠\\.\\s?베\\.하\\.?$", "")
                .replaceAll(",?\\s?(비|비이)\\.\\s?브이\\.?$", "")
                .replaceAll("\\s아게$", "")
                .replaceAll("\\([A-z|\\s|\\.|,]+\\)", "")
                .trim()
        ;

        if (name.contains("주식회사")) {
            name = name.replaceAll("주식회사", "");
            name += " 주식회사";
        }

        if (name.contains("(주)")) {
            name = name.replaceAll("\\(주\\)", "");
            name += " 주식회사";
        }

        if (name.contains("주)")) {
            name = name.replaceAll("^주\\)", "");
            name += " 주식회사";
        }


        name = name.replaceAll("\\s+", " ")
                .trim();

        return name;
    }
}
