package es.module.dataproc.norm;

import es.model.Applicant;
import es.model.bulk.Patent;
import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class RepresentApn {

    public List<Applicant> representApplicantInfo(Connection conn, Patent patent) {
        List<Applicant> applicantInfo = patent.getCurrentApplicantInfo();
        if (applicantInfo == null)
            applicantInfo = patent.getApplicantInfo();

        if (applicantInfo == null)
            return null;

        String docId = patent.getDocumentId();
//        System.out.println(docId);

        List<String> codes = new ArrayList<>();

        for (Applicant applicant : applicantInfo) {
            String code = applicant.getApplicantCodeNumber();
            if (code == null || "".equals(code))
                continue;

            codes.add(code);
        }

        if (codes.isEmpty())
            return null;

        String documentId = patent.getDocumentId();


        List<Applicant> applicants = getRepresentAP(conn, documentId, codes);
        if (applicants != null) {
//            try {
//                System.out.println(new ObjectMapper().writeValueAsString(applicants));
//            } catch (JsonProcessingException e) {
//                e.printStackTrace();
//            }
            return applicants;
        }
        return null;
    }

    private List<Applicant> getRepresentAP(Connection conn, String documentId, List<String> codes) {
        if (codes == null || codes.isEmpty())
            return null;

        String code_str = StringUtils.join(codes, "','");
        String cc = documentId.substring(0,2).toLowerCase();


        String query = "SELECT * FROM patent.KEYWERT_REPRESENT_APPLICANT where applicant_id_"+ cc +" in('" + code_str + "')";
//        System.out.println(query);

        int count = 0;
        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<Applicant> applicants = new ArrayList<>();

            while (rs.next()) {
                if (++count % 100 == 0)
                    System.out.println(count);

                Applicant applicant = new Applicant();

                applicant.setApplicantCodeKR(rs.getString("applicant_id_kr"));
                applicant.setApplicantCodeJP(rs.getString("applicant_id_jp"));
                applicant.setKeywertCodeKR(rs.getString("keywert_code_kr"));
                applicant.setKeywertCodeEN(rs.getString("keywert_code_en"));
                applicant.setKeywertCodeTL(rs.getString("keywert_code_total"));
                applicant.setNameKR(rs.getString("represent_applicant_name_kr"));
                applicant.setNameEN(rs.getString("represent_applicant_name_en"));
                applicant.setNameJP(rs.getString("represent_applicant_name_jp"));
                applicant.setCountry(rs.getString("country_code"));
                applicant.setAddress((rs.getString("address")));
                applicant.setBusinessNumber(rs.getString("applicant_id_jp"));
                applicant.setType(rs.getString("applicant_type"));
//                applicant.setSequence(rs.getInt("seq"));

                applicants.add(applicant);

            }

            if (!applicants.isEmpty())
                return applicants;


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }




    public List<Applicant> getJPApplicantInfo( Connection conn, List<Applicant> applicantInfo) {
        if (applicantInfo == null)
            return null;

        List<String> codes = new ArrayList<>();

        for (Applicant applicant : applicantInfo) {
            String enName = applicant.getEnName();
            if (enName != null)
                return null;

            String code = applicant.getApplicantCodeNumber();
            if (code == null || "".equals(code))
                continue;

            codes.add(code);
        }

        Map<String,String> enNameMap = getJPenName(conn,codes);
        if (enNameMap == null)
            return null;

        List<Applicant> applicants = new ArrayList<>();
        for (Applicant applicant : applicantInfo) {
            String code = applicant.getApplicantCodeNumber();
            if (code != null && !"".equals(code)) {
                String enName = enNameMap.get(code);
                if (enName != null && !"".equals(enName)){
                    applicant.setEnName(enName);
                }
            }

            applicants.add(applicant);
        }

        if (!applicants.isEmpty())
            return applicants;

        return null;
    }

    private Map<String, String> getJPenName(Connection conn, List<String> codes) {
        if (codes == null)
            return null;
        if (codes.isEmpty())
            return null;

        String code_str = StringUtils.join(codes, "','");

        String query = "select * from patent.JPO_APPLICANT_REGISTERINFO where enName is not null and registeredNumber in('" + code_str + "')";
//        System.out.println(query);

        int count = 0;
        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String,String> map = new HashMap<>();

            while (rs.next()) {
                if (++count % 100 == 0)
                    System.out.println(count);

                String enName = rs.getString("enName");
                if (enName == null || "".equals(enName))
                    continue;

                String registeredNumber = rs.getString("registeredNumber");
                if (registeredNumber == null || "".equals(registeredNumber))
                    continue;

                map.put(registeredNumber,enName);
            }

            if (!map.isEmpty())
                return map;


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    public List<Applicant> getCNApplicantInfo(Connection conn, List<Applicant> applicantInfo) {
        if (applicantInfo == null)
            return null;

        Set<String> nameCNSet = new HashSet<>();

        for (Applicant applicant : applicantInfo) {
            String eName = applicant.geteName();
            if (eName != null)
                continue;

            String nameCN = applicant.getOrgNameCN();
            if (nameCN != null)
                nameCNSet.add(nameCN);

        }

        Map<String,String> enNameMap = getCNenName(conn,nameCNSet);
        if (enNameMap == null)
            return null;

        System.out.println(enNameMap);

        return null;
    }

    private Map<String, String> getCNenName(Connection conn, Set<String> nameCNSet) {
        if (nameCNSet.isEmpty())
            return null;

        String cnName_str = StringUtils.join(nameCNSet, "','");
        String query = "select * from person_info.sipo_names where orgNameCN in('" + cnName_str + "')";
        System.out.println(query);

        int count = 0;
        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String,String> map = new HashMap<>();

            while (rs.next()) {
                if (++count % 100 == 0)
                    System.out.println(count);

                String name = rs.getString("name");
                if (name == null || "".equals(name))
                    continue;


                map.put("name",name);
            }

            if (!map.isEmpty())
                return map;


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }


        return null;
    }
}
