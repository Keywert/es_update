package es.module.dataproc;

import es.config.DBHelper;
import es.model.bulk.CitationDoc;
import es.model.bulk.Patent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InsertPatentToDB {
    public InsertPatentToDB() {
    }

    private static String getBiblioTable(String org) {
        if (org == null)
            return null;

        return "citation.bibliographic_" + org;
    }

    private static String getCitationTable(String org) {
        if (org == null)
            return null;


        return "citation.citation_" + org;
    }

    public static void insertCitation(Connection conn, ArrayList<Patent> patents) {
        String org = patents.get(0).getOrg();
        String table = getCitationTable(org);
        if (table == null)
            return;

        String insertQuery = "INSERT IGNORE into " + table +
                " (" +
                "documentId, sequence ,patentId, citedType, examinerCitationState, citedPhase, country,documentNumber," +
                "documentType, date, name, text" +
                ") " +
                " values(?,?,?,?,?,?,?,?,?,?,?,?) " ;
//        +
//                " ON DUPLICATE KEY UPDATE " +
//                " name = name";

        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                List<CitationDoc> citationDocs = patent.getCitationDoc();

                if (citationDocs == null)
                    continue;


                int seq = 0;
                for (CitationDoc citation : citationDocs) {
                    if ("U".equals(citation.getCitedType()))
                        continue;

                    ++seq;

                    statement.setString(1, patent.getDocumentId());
                    statement.setInt(2, (citation.getSequence() == null) ? seq : citation.getSequence());
                    statement.setString(3, (patent.getPatentId() == null) ? null : patent.getPatentId());
                    statement.setString(4, (citation.getCitedType() == null) ? null : citation.getCitedType());
                    statement.setString(5, (citation.getExaminerCitationState() == null) ? null : citation.getExaminerCitationState());
                    statement.setString(6, (citation.getCitedPhase() == null) ? null : citation.getCitedPhase());
                    statement.setString(7, (citation.getCountry() == null) ? null : citation.getCountry());
                    statement.setString(8, (citation.getDocumentNumber() == null) ? null : citation.getDocumentNumber());
                    statement.setString(9, (citation.getKind() == null) ? null : citation.getKind());
                    statement.setString(10, (citation.getDate() == null) ? null : citation.getDate());
                    statement.setString(11, (citation.getName() == null) ? null : citation.getName());

                    String text = citation.getText();
                    if (text != null) {
                        if (text.length() > 1000)
                            text = text.substring(0,1000);
                    }

                    statement.setString(12, (text == null) ? null : text);
                    statement.addBatch();
                }
            }

            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void insertBiblio(Connection conn, ArrayList<Patent> patents) {
        String org = patents.get(0).getOrg();
        String table = getBiblioTable(org);

        String insertQuery = "INSERT ignore into " + table +
                " (" +
                "documentId,patentId , documentType, publicationNumber, publicationDate, registerNumber, registerDate,openNumber," +
                "openDate, applicationNumber, applicationDate" +
                ") " +
                " values(?,?,?,?,?,?,?,?,?,?,?) ";
//        +
//                " ON DUPLICATE KEY UPDATE " +
////                " publicationNumber = publicationNumber ";
//                " openDate = openDate";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                statement.setString(1, patent.getDocumentId());
                statement.setString(2, (patent.getPatentId() == null) ? null : patent.getPatentId());
                statement.setString(3, (patent.getDocumentType() == null) ? null : patent.getDocumentType());
                statement.setString(4, (patent.getPublicationNumber() == null) ? null : patent.getPublicationNumber());
                statement.setString(5, (patent.getPublicationDate() == null) ? null : patent.getPublicationDate());
                statement.setString(6, (patent.getRegisterNumber() == null) ? null : patent.getRegisterNumber());
                statement.setString(7, (patent.getRegisterDate() == null) ? null : patent.getRegisterDate());
                statement.setString(8, (patent.getOpenNumber() == null) ? null : patent.getOpenNumber());
                statement.setString(9, (patent.getOpenDate() == null) ? null : patent.getOpenDate());
                statement.setString(10, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                statement.setString(11, (patent.getApplicationDate() == null) ? null : patent.getApplicationDate());
//                statement.setString(12, (patent.getUpdate_date() == null) ? null : patent.getUpdate_date());

                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void upsertLegalStatus(DBHelper dbHelper, ArrayList<Patent> patents) {
        String org = patents.get(0).getOrg();
//        System.out.println(org);

        if (org.matches("epo")) {
            upsertLegalStatusEP(dbHelper,patents);
        } else if (org.matches("jpo")) {
            upsertLegalStatusJP(dbHelper,patents);
        } else if (org.matches("kipo")) {
            upsertLegalStatusKR(dbHelper,patents);
        } else if (org.matches("uspto")) {
            upsertLegalStatusUS(dbHelper,patents);
        } else
            return;
    }

    private static void upsertLegalStatusUS(DBHelper dbHelper, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA " +
                " (" +
                " application_number, filing_date,patent_number, patent_issue_date, appl_status_code " +
                ") " +
                " values(?,?,?,?,?) " +
                " on duplicate key update " +
                " appl_status_code = values(appl_status_code)," +
                " status = null" +
                " " ;

//        System.out.println(insertQuery);

        PreparedStatement statement = null;
        try {
            Connection conn = dbHelper.getConn_aurora();
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String docType = patent.getDocumentType();

//                System.out.println(patent.getApplicationNumber() +"  "+ docType);

                statement.setString(1, patent.getApplicationNumber().replaceAll("-",""));
                statement.setString(2, patent.getApplicationDate());
                statement.setString(3, patent.getRegisterNumber());
                statement.setString(4, patent.getRegisterDate());

                statement.setString(5, (docType.matches("^[A][0-9]?$")) ? "1" : "150");

                statement.addBatch();
            }

            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void upsertLegalStatusKR(DBHelper dbHelper, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into patent.KIPO_ADMIN_BIBLIOGRAPHIC " +
                " (" +
                " applicationNumber, registration " +
                ") " +
                " values(?,?) " +
                " on duplicate key update " +
                " registration = values (registration)" +
                " ,status = null" +
                " ,upd_date = current_timestamp() ";

        PreparedStatement statement = null;
        try {
            Connection conn = dbHelper.getConn_aurora2();
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String docType = patent.getDocumentType();

//                System.out.println(patent.getApplicationNumber() +"  "+ docType);

                statement.setString(1, patent.getApplicationNumber());
                statement.setString(2, (docType.matches("^[B|Y][0-9]?$")) ? "등록" : "공개");

                statement.addBatch();
            }

            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void upsertLegalStatusJP(DBHelper dbHelper, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into JPO.APPLICATION " +
                " (" +
                "TYPE, APPLICATION_NUMBER, STATUS_CODE, ES_APPLY" +
                ") " +
                " values(?,?,?,?) " +
                " on duplicate key update " +
                " ES_APPLY = 0";

        String insertQueryP = "INSERT into JPO.APPLICATION " +
                " (" +
                "TYPE, APPLICATION_NUMBER, STATUS_CODE, ES_APPLY" +
                ") " +
                " values(?,?,?,?) " +
                " on duplicate key update " +
                " ES_APPLY = 0";

        PreparedStatement statement = null;
        PreparedStatement statementP = null;

        try {
            Connection conn = dbHelper.getConn_aurora();
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            statementP = conn.prepareStatement(insertQueryP);
            statementP.clearParameters();

            for (Patent patent : patents) {
                String docType = patent.getDocumentType();

//                System.out.println(docType);

                if (docType.matches("^[B|Y][0-9]?$")) {
                    statement.setString(1, (docType.matches("^[A|B][0-9]?$")) ? "P" : "U");
                    statement.setString(2, patent.getApplicationNumber());
                    statement.setString(3, "000002" );
                    statement.setInt(4, 9);

                    statement.addBatch();
                } else {
                    statementP.setString(1, (docType.matches("^[A|B][0-9]?$")) ? "P" : "U");
                    statementP.setString(2, patent.getApplicationNumber());
                    statementP.setString(3, "000001");
                    statementP.setInt(4, 9);

                    statementP.addBatch();
                }
            }

            int[] updateCounts = statement.executeBatch();
            int[] updateCountsP = statementP.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void upsertLegalStatusEP(DBHelper dbHelper, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into EPO.APPLICATION " +
                " (" +
                "APPLICATION_NUMBER, STATUS_CODE" +
                ") " +
                " values(?,?) " +
                " on duplicate key update " +
                " STATUS_CODE = values (STATUS_CODE)" +
                " ,ES_APPLY = 0";

        String insertQueryP = "INSERT into EPO.APPLICATION " +
                " (" +
                "APPLICATION_NUMBER, STATUS_CODE" +
                ") " +
                " values(?,?) " +
                " on duplicate key update " +
                " ES_APPLY = 0";

        PreparedStatement statement = null;
        PreparedStatement statementP = null;

        try {
            Connection conn = dbHelper.getConn_aurora();
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            statementP = conn.prepareStatement(insertQueryP);
            statementP.clearParameters();

            for (Patent patent : patents) {
                String docType = patent.getDocumentType();

                if (docType.matches("^[B|Y][0-9]?$")) {
                    statement.setString(1, patent.getApplicationNumber());
                    statement.setInt(2, 255);

                    statement.addBatch();
                } else {
                    statementP.setString(1, patent.getApplicationNumber());
                    statementP.setInt(2, 17);

                    statementP.addBatch();
                }
            }

            int[] updateCounts = statement.executeBatch();
            int[] updateCountsP = statementP.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateRightholder(DBHelper dbHelper, ArrayList<Patent> patents) {
        String org = patents.get(0).getOrg();
//        System.out.println(org);

        if (org.matches("uspto")) {
            updateRightholderUS(dbHelper,patents);
//        } else if (org.matches("jpo")) {
//            upsertLegalStatusJP(dbHelper,patents);
//        } else if (org.matches("kipo")) {
//            upsertLegalStatusKR(dbHelper,patents);
        } else
            return;

    }

    private static void updateRightholderUS(DBHelper dbHelper, ArrayList<Patent> patents) {
        String query = "UPDATE USPTO.APPLICATION_ASSIGNMENT SET status=null " +
                " WHERE application_number = ?";

        PreparedStatement preparedStatement = null;

        try {
            Connection conn = dbHelper.getConn_aurora();
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (Patent patent : patents) {
                preparedStatement.setString(1, patent.getApplicationNumber().replaceAll("-",""));

                preparedStatement.addBatch();
            }

            int[] updateCounts = preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertUSRelatedDoc(DBHelper dbHelper, ArrayList<Patent> patents) {
        
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof InsertPatentToDB)) return false;
        final InsertPatentToDB other = (InsertPatentToDB) o;
        if (!other.canEqual((Object) this)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof InsertPatentToDB;
    }

    public int hashCode() {
        int result = 1;
        return result;
    }

    public static void insertESQueue(Connection conn, ArrayList<Patent> patents) {
        String org = patents.get(0).getOrg();
        String table = "datafarm.ES_UPDATE_QUEUE";

        String insertQuery = "INSERT into " + table +
                " (" +
                "document_id, es_index, level" +
                ") " +
                " values(?,?,?) " +
                " on duplicate key update " +
                "    level = CASE WHEN(level > values(level)) THEN values(level) ELSE level END, " +
                "    update_date =  CURRENT_TIMESTAMP() "
                ;

        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                statement.setString(1, patent.getDocumentId());
                statement.setString(2, patent.getOrg());
                statement.setInt(3, patent.getUpdateLevel());
                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }




    }

    public static void insertKRDocid(Connection conn, ArrayList<Patent> patents) {
        String org = patents.get(0).getOrg();
        String table = "KIPRIS.DOCUMENT_ID_LIST";

        String insertQuery = "INSERT ignore into " + table +
                " (" +
                "DOCUMENT_ID, APPLICATION_NUMBER, REGISTRATION_NUMBER" +
                ") " +
                " values(?,?,?) "
                ;

        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                statement.setString(1, patent.getDocumentId());
                statement.setString(2, patent.getApplicationNumber());
                statement.setString(3, patent.getRegisterNumber()==null ? null : patent.getRegisterNumber() + "0000" );
                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return "InsertPatentToDB()";
    }
}
