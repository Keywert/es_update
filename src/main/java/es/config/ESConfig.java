package es.config;

import lombok.Data;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Data
public class ESConfig {

    public Settings settings = null;
    public TransportClient client = null;
    public BulkProcessor bulkProcessor = null;

    public ESConfig(String clusterName) {
        try {
            if (clusterName.matches("es18")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-production").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.18"), 49300));
            } else if (clusterName.matches("es206")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "test-cluster-01").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.206"), 49300));
            } else if (clusterName.matches("es188")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es-worker-01").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.53"), 49300));
            } else if (clusterName.matches("es189")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es-worker-02").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.58"), 49300));
            } else if (clusterName.matches("es28")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es-datafarm-05").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.28"), 49300));
            } else if (clusterName.matches("es29")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es-datafarm-06").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.29"), 49300));
            } else if (clusterName.matches("es218")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es1-data-14").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.218"), 49300));

            } else if (clusterName.matches("es167")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es-citation-node-01").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.23"), 49300));

            } else if (clusterName.matches("es168")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("node.name", "es-citation-node-02").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.49"), 49300));

            } else if (clusterName.matches("es209")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-alert-cluster01").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.209"), 49300))
                ;
            } else if (clusterName.matches("es205")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster01").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.205"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.204"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.198"), 49300))
                ;

            } else if (clusterName.matches("es51")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster02").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.51"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.52"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.53"), 49300));

            } else if (clusterName.matches("es227")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster09").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.227"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.228"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.248"), 49300));
            } else if (clusterName.matches("es232")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster10").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.232"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.233"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.234"), 49300));

            } else if (clusterName.matches("es13")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster03").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.13"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.14"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("211.62.58.3"), 49300))
                ;

            } else if (clusterName.matches("es43")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster04").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.211"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.210"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.209"), 49300))
                ;

            } else if (clusterName.matches("es225")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster05").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.221"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.223"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.225"), 49300))
                ;

            } else if (clusterName.matches("es226")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "keywert-cluster06").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.222"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.224"), 49300))
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("43.246.155.226"), 49300))
                ;

            } else if (clusterName.matches("esLog")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "elasticsearch").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.16"), 59300));



            } else if (clusterName.matches("esTest")) {
                settings = Settings.settingsBuilder()
                        .put("client.transport.sniff", true)
                        .put("client.transport.ping_timeout", "60s")
                        .put("cluster.name", "test-cluster-02").build();
                client = TransportClient.builder().settings(settings).build()
                        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("112.175.148.8"), 49300));

            }

            bulkProcessor = BulkProcessor.builder(
                            client,
                            new BulkProcessor.Listener() {
                                public void beforeBulk(long l, BulkRequest bulkRequest) {

                                }

                                public void afterBulk(long l, BulkRequest bulkRequest, BulkResponse bulkResponse) {

                                }

                                public void afterBulk(long l, BulkRequest bulkRequest, Throwable throwable) {
                                    throw new RuntimeException(String.format("Execution with id %s failed", l), throwable);
                                }
                            })
                    .setBulkActions(5)
                    .setBulkSize(new ByteSizeValue(3, ByteSizeUnit.MB))
                    .setFlushInterval(TimeValue.timeValueSeconds(5))
                    .setConcurrentRequests(1)
                    .build();

            System.out.println(clusterName);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
