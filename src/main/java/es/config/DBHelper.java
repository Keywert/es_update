/**
 * SSU, Mobile Database lab. ,
 * DBHelper.java    Help the connecting and processing thd DB,
 * <p>
 * 2013-02-26,
 * for further information, contact SHIN, JUNGHOON (coolhoony81 AT gmail DOT com)
 */
package es.config;

import lombok.Data;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

@Data
public class DBHelper {

    public Connection conn = null;
    public Connection conn_local = null;
    public Connection conn_local_36 = null;


    public Connection conn_local_166 = null;
    public Connection conn_local_167 = null;
    public Connection conn_local_183 = null;
    public Connection conn_local_157 = null;
    public Connection conn_local_175 = null;
    public Connection conn_local_184 = null;
    public Connection conn_local_158 = null;
    public Connection conn_local_205 = null;


    public Connection conn_real = null;
    public Connection conn_real02 = null;
    public Connection conn_41 = null;



    public Connection conn_real_kwork = null;
    public Connection conn_real_ex = null;

    public Connection conn_aurora = null;
    public Connection conn_aurora2 = null;
    public Connection conn_axis = null;



    public DBHelper() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
//			conn = DriverManager.getConnection("Jdbc:mysql://localhost:3306/patent","hoon","dsldsl");
//            conn = DriverManager
//                    .getConnection("jdbc:mysql://keywertawsdb2.cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306/patent?useUnicode=true&characterEncoding=utf8&"
//                            + "user=kiri&password=keywert321!");

//            conn = DriverManager
//                    .getConnection("jdbc:mysql://localhost:3306/patent?useUnicode=true&characterEncoding=utf8&"
//                            + "user=root&password=dsldsl");
//
//            conn_real02 = DriverManager
////                    .getConnection("jdbc:mysql://112.175.148.15:6303/patent?"
//                    .getConnection("jdbc:mysql://112.175.148.15:6303/patent?"
//                            + "user=dblab&password=dsldsl");

//            conn_axis = DriverManager
//                    .getConnection("jdbc:mysql://112.175.148.12:53306/kppa?"
//                            + "user=wertkey&password=wertkey321!");

//            jdbc:log4jdbc:mysql://112.175.148.12:53306/kppa?allowMultiQueries=true&autoReconnect=true&useUnicode=true&characterEncoding=utf8&mysqlEncoding=utf8;

//            conn_real = DriverManager
////                    .getConnection("jdbc:mysql://112.175.148.15:6303/patent?"
//                    .getConnection("jdbc:mysql://211.62.58.210:6303/patent?"
//                            + "user=dblab&password=dsldsl&useSSL=false");

            conn_aurora = DriverManager
                    .getConnection("jdbc:mysql://keywertawsdb.cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306/patent?useUnicode=true&characterEncoding=utf8&"
                            + "user=kiri&password=keywert321!&useSSL=false");

            conn_aurora2 = DriverManager
                    .getConnection("jdbc:mysql://keywertawsdb2.cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306/patent?useUnicode=true&characterEncoding=utf8&"
                            + "user=kiri&password=keywert321!&useSSL=false");

//            conn_41 = DriverManager
//                    .getConnection("jdbc:mysql://112.175.148.41:56303/patent?"
//                            + "user=wert&password=dsldsl");

            //            conn_41 = DriverManager
//                    .getConnection("jdbc:mysql://112.175.148.41:56303/patent?"
//                            + "user=wert&password=dsldsl");

//            conn = DriverManager
//                    .getConnection("jdbc:mysql://112.175.148.15:6303/business?"
//                            + "user=dblab&password=dsldsl");


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void DBConnection() {

    }

//        public Connection conn_local_166 = null;
//    public Connection conn_local_167 = null;
//    public Connection conn_local_183 = null;
//    public Connection conn_local_157 = null;

//    public Connection getConn_axis() {
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//
//            conn_axis = DriverManager
//                    .getConnection("jdbc:mysql://112.175.148.12:53306/kppa?"
//                            + "user=wertkey&password=wertkey321!");
//
//
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return conn_axis;
//    }

    public Connection getConn_real_kwork() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

//            conn_real_kwork = DriverManager
//                    .getConnection("jdbc:mysql:aws://database-1.cluster-cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306/useUnicode=true&characterEncoding=utf8&"
//                                    + "user=admin&password=keywert1!&useSSL=false");

            conn_real_kwork = DriverManager
                    .getConnection("jdbc:mysql:aws://database-1.cluster-cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306","admin","keywert1!");

//            jdbc:mysql:aws://database-1.cluster-cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306

//            conn_real_kwork = DriverManager
//                    .getConnection("jdbc:mysql://database-1.cluster-ro-cgsra6bgmcxd.ap-northeast-2.rds.amazonaws.com:53306/useUnicode=true&characterEncoding=utf8&"
//                            + "user=admin&password=keywert1!&useSSL=false");

//            conn_real_kwork = DriverManager
//                    .getConnection("jdbc:mysql://43.246.155.230:13306?useSSL=false&serverTimezone=Asia/Seoul&autoReconnection=true&useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&"
//                            + "user=dblab&password=dsldsl");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_real_kwork;
    }

    public Connection getConn_real_ex() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_real_ex = DriverManager
                    .getConnection("jdbc:mysql://43.246.155.231:53306?useSSL=false&serverTimezone=Asia/Seoul&autoReconnection=true&useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&"
                            + "user=dblab&password=dsldsl");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_real_ex;
    }


    public Connection getConn_local_166() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_166 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.166:3306/KIPO_DRAWING_INFO_DB?useUnicode=true&characterEncoding=utf8&"
                            + "user=wert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_166;
    }

    public Connection getConn_local_167() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_167 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.167:3306/KIPO_DRAWING_INFO_DB?useUnicode=true&characterEncoding=utf8&"
//                            + "user=root&password=keywert1!");
                            + "user=wert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_167;
    }

    public Connection getConn_local_183() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_183 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.183:3306/KIPO_DRAWING_INFO_DB?useUnicode=true&characterEncoding=utf8&"
                            + "user=wert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_183;
    }

    public Connection getConn_local_158() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_158 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.158:3306/code?useUnicode=true&characterEncoding=utf8&"
                            + "user=root&password=keywert1!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_158;
    }

    public Connection getConn_local_205() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_205 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.205:3306/code?useUnicode=true&characterEncoding=utf8&"
                            + "user=keywert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_205;
    }

    public Connection getConn_local_184() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_183 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.184:3306/JPO_DRAWING_INFO_DB?useUnicode=true&characterEncoding=utf8&"
                            + "user=wert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_183;
    }

    public Connection getConn_local_157() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_157 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.157:3306/KIPO_DRAWING_INFO_DB?useUnicode=true&characterEncoding=utf8&"
                            + "user=wert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_157;
    }

    public Connection getConn_local_175() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local_175 = DriverManager
                    .getConnection("jdbc:mysql://192.168.0.175:3306/KIPO_DRAWING_INFO_DB?useUnicode=true&characterEncoding=utf8&"
                            + "user=wert&password=Keywert321!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local_175;
    }

    public Connection getConn_local() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/patent?useSSL=false&useUnicode=true&characterEncoding=utf8&"
                            + "user=root&password=keywert1!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local;
    }

    public Connection getConn_local_36() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn_local = DriverManager
                    .getConnection("jdbc:mysql://175.209.232.194:53306/patent?useSSL=false&useUnicode=true&characterEncoding=utf8&"
                            + "user=root&password=asdfmoon003!");


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn_local;
    }

    public Connection getConn() {
        return conn;
    }

    public Connection getConn_real02() {
        return conn_real02;
    }

    public Connection getConn_real() {
        return conn_real;
    }

    public ResultSet queryExcute(String query) {
        ResultSet rs = null;
        java.sql.Statement st = null;

        try {
            st = conn.createStatement();
            if (st.execute(query)) {
                rs = st.getResultSet();
                return rs;
            }


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;

    }

    public void insertQueryExcute(String query) {
        //ResultSet rs = null;
        PreparedStatement st = null;

        try {
            st = conn.prepareStatement(query);
            st.executeUpdate();


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //return null;
    }

    public void insertQuery(String _query, String[] _value) throws SQLException, ClassNotFoundException {
        PreparedStatement statement = conn.prepareStatement(_query);
        statement.clearParameters();

        for (int i = 0; i < _value.length; i++) {
            statement.setString(i + 1, _value[i]);
        }
    }

    public void insertCitationQuery(String _query, ArrayList<Map<String, Object>> mapArrayList) throws SQLException, ClassNotFoundException {

        PreparedStatement statement = conn.prepareStatement(_query);
        statement.clearParameters();

        for (Map<String, Object> valueMap : mapArrayList) {
//            statement.setString(1, valueMap.get("citationId").toString());
//            statement.setString(2, valueMap.get("startDocId").toString());
//            statement.setNull(3, Types.VARCHAR);
//            statement.setString(4, (valueMap.get("citedType") == null) ? null : valueMap.get("citedType").toString().replaceAll("\"", ""));
//            statement.setString(5, (valueMap.get("examinerCitationState") == null) ? null : valueMap.get("examinerCitationState").toString().replaceAll("\"", ""));
//            statement.setString(6, (valueMap.get("country") == null) ? null : getStr(valueMap.get("country").toString().replaceAll("\"", ""),25));
//            statement.setString(7, (valueMap.get("documentNumber") == null) ? null : getStr(valueMap.get("documentNumber").toString().replaceAll("\"", "").trim(),25));
//            statement.setString(8, (valueMap.get("kind") == null) ? null : getStr(valueMap.get("kind").toString().replaceAll("\"", "").trim(),25));
//            statement.setString(9, (valueMap.get("text") == null) ? null : getStr(valueMap.get("text").toString().replaceAll("\"", "").trim(), 1024));

            statement.setString(1, valueMap.get("citationId").toString());
            statement.setString(2, (valueMap.get("applicationNumber") == null) ? null : valueMap.get("applicationNumber").toString().replaceAll("\"", ""));
            statement.setString(3, (valueMap.get("registerNumber") == null) ? null : valueMap.get("registerNumber").toString().replaceAll("\"", ""));
            statement.setString(4, (valueMap.get("publicationNumber") == null) ? null : valueMap.get("publicationNumber").toString().replaceAll("\"", ""));
            statement.setString(5, valueMap.get("startDocId").toString());
            statement.setNull(6, Types.VARCHAR);
            statement.setString(7, (valueMap.get("citedType") == null) ? null : valueMap.get("citedType").toString().replaceAll("\"", ""));
            statement.setString(8, (valueMap.get("examinerCitationState") == null) ? null : valueMap.get("examinerCitationState").toString().replaceAll("\"", ""));
            statement.setString(9, (valueMap.get("country") == null) ? null : getStr(valueMap.get("country").toString().replaceAll("\"", ""), 25));
            statement.setString(10, (valueMap.get("documentNumber") == null) ? null : getStr(valueMap.get("documentNumber").toString().replaceAll("\"", "").trim(), 25));
            statement.setString(11, (valueMap.get("kind") == null) ? null : getStr(valueMap.get("kind").toString().replaceAll("\"", "").trim(), 25));
            statement.setString(12, (valueMap.get("text") == null) ? null : getStr(valueMap.get("text").toString().replaceAll("\"", "").trim(), 1024));
            statement.setString(13, (valueMap.get("patentNumber") == null) ? null : getStr(valueMap.get("patentNumber").toString().replaceAll("\"", "").trim(), 25));

            statement.addBatch();

//            String  str = valueMap.get("text").toString().replaceAll("\"", "").trim();
//
//            if (str.length() > 200)
//                System.out.println(str);
        }

        // execute the batch
        int[] updateCounts = statement.executeBatch();


//		checkUpdateCounts(updateCounts);

        // since there were no errors, commit
//		conn.commit();

    }

    public void insertCitationQuery41(String _query, ArrayList<Map<String, Object>> mapArrayList) throws SQLException, ClassNotFoundException {

        PreparedStatement statement = conn_41.prepareStatement(_query);
        statement.clearParameters();

        for (Map<String, Object> valueMap : mapArrayList) {
//            statement.setString(1, valueMap.get("citationId").toString());
//            statement.setString(2, valueMap.get("startDocId").toString());
//            statement.setNull(3, Types.VARCHAR);
//            statement.setString(4, (valueMap.get("citedType") == null) ? null : valueMap.get("citedType").toString().replaceAll("\"", ""));
//            statement.setString(5, (valueMap.get("examinerCitationState") == null) ? null : valueMap.get("examinerCitationState").toString().replaceAll("\"", ""));
//            statement.setString(6, (valueMap.get("country") == null) ? null : getStr(valueMap.get("country").toString().replaceAll("\"", ""),25));
//            statement.setString(7, (valueMap.get("documentNumber") == null) ? null : getStr(valueMap.get("documentNumber").toString().replaceAll("\"", "").trim(),25));
//            statement.setString(8, (valueMap.get("kind") == null) ? null : getStr(valueMap.get("kind").toString().replaceAll("\"", "").trim(),25));
//            statement.setString(9, (valueMap.get("text") == null) ? null : getStr(valueMap.get("text").toString().replaceAll("\"", "").trim(), 1024));

            statement.setString(1, valueMap.get("citationId").toString());
            statement.setString(2, (valueMap.get("applicationNumber") == null) ? null : valueMap.get("applicationNumber").toString().replaceAll("\"", ""));
            statement.setString(3, (valueMap.get("registerNumber") == null) ? null : valueMap.get("registerNumber").toString().replaceAll("\"", ""));
            statement.setString(4, (valueMap.get("publicationNumber") == null) ? null : valueMap.get("publicationNumber").toString().replaceAll("\"", ""));
            statement.setString(5, valueMap.get("startDocId").toString());
            statement.setNull(6, Types.VARCHAR);
            statement.setString(7, (valueMap.get("citedType") == null) ? null : valueMap.get("citedType").toString().replaceAll("\"", ""));
            statement.setString(8, (valueMap.get("examinerCitationState") == null) ? null : valueMap.get("examinerCitationState").toString().replaceAll("\"", ""));
            statement.setString(9, (valueMap.get("country") == null) ? null : getStr(valueMap.get("country").toString().replaceAll("\"", ""), 25));
            statement.setString(10, (valueMap.get("documentNumber") == null) ? null : getStr(valueMap.get("documentNumber").toString().replaceAll("\"", "").trim(), 25));
            statement.setString(11, (valueMap.get("kind") == null) ? null : getStr(valueMap.get("kind").toString().replaceAll("\"", "").trim(), 25));
            statement.setString(12, (valueMap.get("text") == null) ? null : getStr(valueMap.get("text").toString().replaceAll("\"", "").trim(), 1024));
            statement.setString(13, (valueMap.get("patentNumber") == null) ? null : getStr(valueMap.get("patentNumber").toString().replaceAll("\"", "").trim(), 25));

            statement.addBatch();

//            String  str = valueMap.get("text").toString().replaceAll("\"", "").trim();
//
//            if (str.length() > 200)
//                System.out.println(str);
        }

        // execute the batch
        int[] updateCounts = statement.executeBatch();


//		checkUpdateCounts(updateCounts);

        // since there were no errors, commit
//		conn.commit();

    }

    private String getStr(String text, int i) {
        if (text.length() > i)
            return text.substring(0, i - 1);
        else
            return text;
    }

//	public static void checkUpdateCounts(int[] updateCounts) {
//		for (int i = 0; i < updateCounts.length; i++) {
//			if (updateCounts[i] >= 0) {
//				System.out.println("OK; updateCount=" + updateCounts[i]);
//			} else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
//				System.out.println("OK; updateCount=Statement.SUCCESS_NO_INFO");
//			} else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
//				System.out.println("Failure; updateCount=Statement.EXECUTE_FAILED");
//			}
//		}
//	}
}