package es.tmp.sep;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.tmp.person.kipo.TaskBase;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class InsertSep extends TaskBase {
    @Override
    public int jobMain() {
        System.out.println("InsertSep");

        int limit_size = 20000;

        List<QueueVO> list = insertSepInfo(limit_size);
        if (list != null) {
            System.out.println("Update Count : " + list.size());
        }

        return 0;
    }

    private List<QueueVO> insertSepInfo(int limit_size) {
        ESConfig esConfig = new ESConfig("es205");
        DataFarmDB dataFarmDB = new DataFarmDB();

        Map<String, ESConfig> esConfigMap = new HashMap<>();
        esConfigMap.put("es188", new ESConfig("es188"));
        esConfigMap.put("es189", new ESConfig("es189"));
        esConfigMap.put("es18", new ESConfig("es18"));

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String updateDate = getDate();
        System.out.println(updateDate);

        String tableName = "sep.z_sep_total_distinct_202406";

        String query = "select docdbFamilyId, group_concat(distinct patentId separator ' ## ') patentIds from " + tableName +
                " where status is null and docdbFamilyId is not null  " +
                " group by docdbFamilyId limit " + limit_size;
        System.out.println(query);

        List<QueueVO> queueVOS = null;

        try {
            Statement st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, Set<String>> familyMap = new HashMap<>();

            while (rs.next()) {
                String docdbFamilyId = rs.getString("docdbFamilyId");
                if (docdbFamilyId == null)
                    continue;

                String patentIds = rs.getString("patentIds");
                if (patentIds == null)
                    continue;

                familyMap.put(docdbFamilyId, new HashSet<String>(Arrays.asList(patentIds.split(" ## "))));
            }

            if (familyMap.isEmpty())
                return null;

            System.out.println(new ObjectMapper().writeValueAsString(familyMap));

            for (String familyId : familyMap.keySet()) {
//                Set<String> patentIds = familyMap.get(familyId);

                String query02 = "select docdbFamilyId, declaredate, stdorgancd, stdno, pdfPath " +
                        " ,group_concat(declareradjust separator ' ## ') declareradjust " +
                        " ,group_concat(declarercountryadjust separator ' ## ') declarercountryadjust " +
                        " ,group_concat(distinct stdaddinfo separator ' ## ') stdaddinfo " +
                        " ,group_concat(distinct patnoorg separator ' ## ') patnoorg " +
                        " ,group_concat(distinct patentId separator ' ## ') patentId " +
                        " ,group_concat(distinct sginfoclass separator ' ## ') sginfoclass " +
                        " ,group_concat(distinct iprpolicyclass separator ' ## ') iprpolicyclass " +
                        " ,group_concat(distinct iprpolicy separator ' ## ') iprpolicy " +
                        " ,group_concat(distinct dtltech1 separator ' ## ') dtltech1 " +
                        " ,group_concat(distinct dtltech2 separator ' ## ') dtltech2 " +
                        " ,group_concat(distinct claims separator ' ## ') claims " +
                        " ,group_concat(distinct IllustrativeSection separator ' ## ') IllustrativeSection " +
                        " ,group_concat(distinct ETSIDecl separator ' ## ') ETSIDecl " +
                        " ,group_concat(distinct sisvel_family separator ' ## ') sisvelFamily " +
                        " ,group_concat(distinct sisvelFamilyMember separator ' ## ') sisvelFamilyMember " +
                        " ,group_concat(distinct exemplaryPatent separator ' ## ') exemplaryPatent " +
                        " ,group_concat(distinct technicalStandard separator ' ## ') technicalStandard " +
                        " ,group_concat(distinct patentId separator ' ## ') patentIds " +
                        " from " + tableName +
                        " where docdbFamilyId in ('" + familyId + "') " +
                        " group by declaredate, stdorgancd, stdno, pdfPath order by declaredate desc";

//                System.out.println(query02);

                ResultSet rs2 = st.executeQuery(query02);
                List<SepInfoVO> sepInfoVOList = new ArrayList<>();
                Set<String> patentIds = null;
                int seq = 0;
                while (rs2.next()) {
                    String stdorgancd = rs2.getString("stdorgancd");
                    if (stdorgancd == null)
                        continue;

                    SepInfoVO sepInfoVO = new SepInfoVO();
                    sepInfoVO.setStdOrg(stdorgancd);
                    sepInfoVO.setSepFamilyId(familyId);
                    sepInfoVO.setSeq(++seq);

                    // 등재 : L, 선언 : D
                    if (stdorgancd.matches("^(MPEG-LA|SISVEL|VIA LICENSING|WPC|Access Advance|VIA-LA)$"))
                        sepInfoVO.setSepType("L");
                    else
                        sepInfoVO.setSepType("D");

                    String pIds = rs2.getString("patentIds");
                    if (pIds != null)
                        patentIds = new HashSet<>(Arrays.asList(pIds.split(" ## ")));


                    // 표준 정보
                    String stdno = rs2.getString("stdno");
                    if (stdno != null && !"".equals(stdno)) {
                        sepInfoVO.setSepNo(stdno);
                        List<String> stdnoList = removeNullValue(stdno);
                        if (!stdnoList.isEmpty()) {
                            if (stdorgancd.matches("^ITU-T$"))
                                sepInfoVO.setSepTech(stdnoList);
//                            else
//                                sepInfoVO.setSepDesc(stdnoList);
                        }
                    }

                    String stdaddinfo = rs2.getString("stdaddinfo");
                    if (stdaddinfo != null && !"".equals(stdaddinfo)) {
                        List<String> stdaddinfoList = removeNullValue(stdaddinfo);
                        if (!stdaddinfoList.isEmpty()) {
                            sepInfoVO.setSepDesc(stdaddinfoList);
//                            if (stdorgancd.matches("^ITU-T$"))
//                                sepInfoVO.setSepDesc(stdaddinfo);
//                            else
//                                sepInfoVO.setSepDesc(stdaddinfo);
                        }
                    }

                    String patnoorg = rs2.getString("patnoorg");
//                    if (patnoorg == null) {
//                        patnoorg = rs2.getString("appno");
//                    }

                    if (patnoorg != null && !"".equals(patnoorg)) {
                        List<String> patnoorgList = removeNullValue(patnoorg);
                        sepInfoVO.setSepPatentNumber(patnoorgList);
                    }

                    String patentId = rs2.getString("patentId");
                    if (patentId != null && !"".equals(patentId)) {
                        List<String> patentIdList = removeNullValue(patentId);
                        sepInfoVO.setPatentIds(patentIdList);
                    }

                    String pdfPath = rs2.getString("pdfPath");
                    if (pdfPath != null)
                        sepInfoVO.setPdfPath(pdfPath);

                    String declareradjust = rs2.getString("declareradjust");
                    String declarercountryadjust = rs2.getString("declarercountryadjust");
                    if (declareradjust != null && !"".equals(declareradjust) && declarercountryadjust != null && !"".equals(declarercountryadjust)) {
                        List<String> declareradjustList = removeNullValue(declareradjust);
                        List<String> declarercountryadjustList = removeNullValue(declarercountryadjust);
                        List<String> nameList = new ArrayList<>();
                        List<String> ccList = new ArrayList<>();

                        if (declareradjustList.size() != declarercountryadjustList.size()) {
                            System.out.println("declareradjustList declarercountryadjustList count Not Match : " + familyId);
                            nameList = declareradjustList;
                            ccList = declarercountryadjustList;
                        } else {
                            Map<String, String> declarerMap = new HashMap<>();
                            for (int i = 0, len = declareradjustList.size(); i < len; i++) {
                                String name = declareradjustList.get(i);
                                if (name == null || "".equals(name))
                                    continue;

                                String cc = declarercountryadjustList.get(i);
                                if (cc == null || "".equals(cc))
                                    continue;

                                declarerMap.put(name.trim(), cc.trim());
                            }

                            for (String name : declarerMap.keySet()) {
                                String cc = declarerMap.get(name);
                                if (cc == null)
                                    continue;

                                nameList.add(name);
                                ccList.add(cc);
                            }
                        }

                        if (!nameList.isEmpty())
                            sepInfoVO.setSepDeclarer(nameList);

                        if (!ccList.isEmpty())
                            sepInfoVO.setSepDeclarerCountry(ccList);
                    }

//                    String declarercountryadjust = rs2.getString("declarercountryadjust");
//                    if (declarercountryadjust != null && !"".equals(declarercountryadjust)) {
//                        List<String> declarercountryadjustList  = removeNullValue(declarercountryadjust);
//                        sepInfoVO.setSepDeclarerCountry(declarercountryadjustList);
//                    }

                    String declaredate = rs2.getString("declaredate");
                    if (declaredate != null && !"".equals(declaredate)) {
                        sepInfoVO.setSepDeclareDate(declaredate);
                    }

                    String sginfoclass = rs2.getString("sginfoclass");
                    if (sginfoclass != null && !"".equals(sginfoclass)) {
                        List<String> sginfoclassList = removeNullValue(sginfoclass);
                        sepInfoVO.setStudyGroupClass(sginfoclassList);
                    }

                    String iprpolicyclass = rs2.getString("iprpolicyclass");
                    if (iprpolicyclass != null && !"".equals(iprpolicyclass)) {
                        List<String> iprpolicyclassList = removeNullValue(iprpolicyclass);
                        sepInfoVO.setIprPolicyClass(iprpolicyclassList);
                    }

                    String iprpolicy = rs2.getString("iprpolicy");
                    if (iprpolicy != null && !"".equals(iprpolicy)) {
                        List<String> iprpolicyList = removeNullValue(iprpolicy);
                        sepInfoVO.setIprPolicyClassDesc(iprpolicyList);
                    }

                    String dtltech1 = rs2.getString("dtltech1");
                    if (dtltech1 != null && !"".equals(dtltech1)) {
                        List<String> dtltech1List = removeNullValue(dtltech1);
                        sepInfoVO.setDtlTech1(dtltech1List);
                    }

                    String dtltech2 = rs2.getString("dtltech2");
                    if (dtltech2 != null && !"".equals(dtltech2)) {
                        List<String> dtltech2List = removeNullValue(dtltech2);
                        sepInfoVO.setDtlTech2(dtltech2List);
                    }

                    String claims = rs2.getString("claims");
                    if (claims != null && !"".equals(claims)) {
                        List<String> claimsList = removeNullValue(claims);
                        sepInfoVO.setClaims(claimsList);
                    }

                    String IllustrativeSection = rs2.getString("IllustrativeSection");
                    if (IllustrativeSection != null && !"".equals(IllustrativeSection)) {
                        List<String> IllustrativeSectionList = removeNullValue(IllustrativeSection);
                        sepInfoVO.setIllustrativeSection(IllustrativeSectionList);
                    }

                    String ETSIDecl = rs2.getString("ETSIDecl");
                    if (ETSIDecl != null && !"".equals(ETSIDecl)) {
                        List<String> ETSIDeclList = removeNullValue(ETSIDecl);
                        sepInfoVO.setEtsiDecl(ETSIDeclList);
                    }

                    String technicalStandard = rs2.getString("technicalStandard");
                    if (technicalStandard != null && !"".equals(technicalStandard)) {
                        List<String> technicalStandardList = removeNullValue(technicalStandard);
                        sepInfoVO.setTechincalStandard(technicalStandardList);
                    }

                    String sisvelFamily = rs2.getString("sisvelFamily");
                    if (sisvelFamily != null && !"".equals(sisvelFamily)) {
                        List<String> sisvelFamilyList = removeNullValue(sisvelFamily);
                        sepInfoVO.setSisvelFamily(sisvelFamilyList);
                    }

                    String sisvelFamilyMember = rs2.getString("sisvelFamilyMember");
                    if (sisvelFamilyMember != null && !"".equals(sisvelFamilyMember)) {
                        List<String> sisvelFamilyMemberList = removeNullValue(sisvelFamilyMember);
                        sepInfoVO.setSisvelFamilyMember(sisvelFamilyMemberList);
                    }

                    String exemplaryPatent = rs2.getString("exemplaryPatent");
                    if (exemplaryPatent != null && !"".equals(exemplaryPatent)) {
                        List<String> exemplaryPatentList = removeNullValue(exemplaryPatent);
                        sepInfoVO.setExemplaryPatent(exemplaryPatentList);
                    }

//                    System.out.println(new ObjectMapper().writeValueAsString(sepInfoVO));
                    sepInfoVOList.add(sepInfoVO);
                }

                if (!sepInfoVOList.isEmpty()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("sepInfo", sepInfoVOList);
                    map.put("sepUpdateDate", updateDate);
                    queueVOS = esUpdate(esConfig, esConfigMap, map, familyId, patentIds);
                    updateDB(conn, tableName, familyId);

                    if (queueVOS != null)
                        dataFarmDB.insertESQueue(queueVOS);
                }

            }

            for (ESConfig esCon : esConfigMap.values())
                esCon.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return queueVOS;
    }

    private static List<String> removeNullValue(String stdaddinfo) {
        return Arrays.stream(stdaddinfo.split(" ## ")).filter(a -> !"".equals(a)).map(a -> a = a.trim()).collect(Collectors.toList());
    }

    private static void updateDB(Connection conn, String tableName, String familyId) {
        if (familyId == null && "".equals(familyId))
            return;

        String query = "UPDATE " + tableName + " SET status = 9" +
                " WHERE docdbFamilyId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            preparedStatement.setString(1, familyId);
            preparedStatement.addBatch();

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static List<QueueVO> esUpdate(ESConfig esConfig, Map<String, ESConfig> esConfigMap, Map<String, Object> sepInfoMap, String familyId, Set<String> patentIdSet) {
        String[] include = {"documentId", "patentId"};
        String type = "patent";
        String[] indexies = {"kipo", "docdb", "jpo", "uspto", "sipo", "pct", "epo", "dpma", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo", "uspto_past", "jpo_past"};

        String[] familyFields = {"familyId", "docdbFamilyId"};

        String start = "";
        String end = "";

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .should(QueryBuilders.boolQuery()
                        .mustNot(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.termsQuery("patentId", patentIdSet)))
                .should(QueryBuilders.multiMatchQuery(familyId, familyFields)
                );

//        System.out.println(qb);

        SearchResponse resp = esConfig.client.prepareSearch(indexies)
                .setTypes(type)
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(include, null)
                .setSize(3000)
                .execute()
                .actionGet();

        System.out.println(resp.getHits().getTotalHits());

        try {
            List<QueueVO> queueVOList = new ArrayList<>();

            for (SearchHit hit : resp.getHits().getHits()) {
                boolean sepOriginFlag = false;
                Object id = hit.getSource().get("patentId");
                if (id != null) {
                    if (patentIdSet.contains(id.toString()))
                        sepOriginFlag = true;
                }

                sepInfoMap.put("sepOriginFlag", sepOriginFlag);

                String json = new ObjectMapper().writeValueAsString(sepInfoMap);

                String index = hit.getIndex();
                ESConfig datafarmESConfig = null;
                if (index.matches("^(kipo|epo|dpma|docdb|uspto|jpo_past|uspto_past)$"))
                    datafarmESConfig = esConfigMap.get("es188");
                else if (index.matches("^(sipo|aupo|capo|frpo|gbpo|inpo|itpo|rupo|tipo)$"))
                    datafarmESConfig = esConfigMap.get("es189");
                else if (index.matches("^(jpo|pct)$"))
                    datafarmESConfig = esConfigMap.get("es18");
                else
                    datafarmESConfig = null;

                if (datafarmESConfig == null) {
                    System.out.println(index);
                    continue;
                }
//                System.out.println(index + " : " + hit.getId() + " : " + json);

                datafarmESConfig.bulkProcessor.add(new UpdateRequest(index, type, hit.getId())
                        .doc(json.getBytes())
                        .upsert(json.getBytes()));


                QueueVO queueVO = new QueueVO();
                queueVO.setLevel(0);
                queueVO.setEsindex(hit.getIndex());
                queueVO.setDocumentId(hit.getId());
                queueVOList.add(queueVO);

            }

            if (!queueVOList.isEmpty())
                return queueVOList;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return null;
    }


}
