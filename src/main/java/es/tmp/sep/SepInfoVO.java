package es.tmp.sep;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.v1.FtermDeseralizer;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SepInfoVO {
    public String stdOrg;  // 표준화기구
    public String sepNo; //  표준 정보 (프로그램명, 표준번호, 특허풀)
    public String sepFamilyId;  // docdbFamilyNumber
    public String pdfPath; // pdf 경로
    public int seq; //표준그룹 시퀀스
    public String stdOrgCode; // 표준화기구 코드
    public String sepType; // 표준타입/분류 (선언,등재) 등재 : L, 선언 : D
    public String sepDeclareDate; //선언일

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sepDeclarer ;  //선언자 (기업 기관)

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sepDeclarerCountry ; //선언자 국적
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sepDesc;  // 표준 번호/설명
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sepTech;  // 표준 기술   stdaddinfo
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> studyGroupClass;  // 위원회, 연구반 정보 분류  , sginfoclass
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> studyGroupClassKo;  //연구반 정보 설명 , sginfoclass
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> studyGroupClassEn;  //연구반 정보 설명 , sginfoclass
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> iprPolicyClass; // 라이선스정보 (IPR정책분류)
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> iprPolicyClassDesc; // 라이선스 설명
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> dtlTech1;  // 세부기술1
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> dtlTech2;  // 세부기술2
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> designatedCountry;  // 특허 지정국가
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sepPatentNumber;   //특허번호(원본)
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> claims;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> illustrativeSection;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> etsiDecl;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> techincalStandard;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sisvelFamily;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> sisvelFamilyMember;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> exemplaryPatent;
    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> patentIds;   // patentIds

    public String getStdOrgCode() {
        if (this.stdOrg != null){
            if (this.stdOrg.matches("ISO/IEC JTC1"))
                return "ISIE";
            else if (this.stdOrg.matches("MPEG-LA"))
                return "MPEG";
            else if (this.stdOrg.matches("SISVEL"))
                return "SIS";
            else if (this.stdOrg.matches("Access Advance"))
                return "ACC";
            else if (this.stdOrg.matches("(VIA LICENSING|VIA-LA)"))
                return "VIA";
            else
                return this.stdOrg.replace("-","");
        }

        return stdOrgCode;
    }
}
