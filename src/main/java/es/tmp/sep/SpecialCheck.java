package es.tmp.sep;

import es.config.DBHelper;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class SpecialCheck {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_local();

        ESConfig esConfig = new ESConfig("es227");

        String tableName = "data_work.special_report_distinct_patt_id";

        String query = "select patentId from " + tableName + " where patentId is not null ";

        System.out.println(query);

        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> patentIds = new HashSet<>();
            while (rs.next()) {
                String patentId = rs.getString("patentId");
                if (patentId == null)
                    continue;

                patentIds.add(patentId);
            }

            if (patentIds.isEmpty())
                return;

            System.out.println(patentIds.size());

            Set<String> uPatentIds = esCheck(esConfig,patentIds);
            if (uPatentIds == null)
                return;

            System.out.println(uPatentIds.size());

            updateDB(conn,tableName,uPatentIds);


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String tableName, Set<String> uPatentIds) {
        String query = "UPDATE " + tableName + " SET status = 1" +
                " WHERE patentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String patentId : uPatentIds) {
                preparedStatement.setString(1, patentId);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Set<String> esCheck(ESConfig esConfig, Set<String> patentIds) {
        String[] indexies = {"kipo", "jpo", "uspto", "sipo", "epo", "dpma", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo","pct"};
        String type = "patent";
        String[] fields = {"patentId"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("sepInfo"))
                .filter(QueryBuilders.termsQuery("patentId",patentIds))
                ;

        SearchResponse scrollResp = esConfig.client.prepareSearch(indexies)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1000)
                .execute()
                .actionGet();

        System.out.println("Total Hit : " + scrollResp.getHits().getTotalHits());

        Set<String> hitPatnetId = new HashSet<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object patentId = hit.getSource().get("patentId");
                if (patentId == null)
                    continue;

                hitPatnetId.add(patentId.toString());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!hitPatnetId.isEmpty()){
            patentIds.removeAll(hitPatnetId);
        }

        if (!patentIds.isEmpty())
            return patentIds;

        return null;
    }
}
