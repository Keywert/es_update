package es.tmp.priority;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.v1.PatentVO;
import es.model.v1.PriorityClaim;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class USEarliest {
    public static void main(String[] args) {
        ESConfig esConfig = new ESConfig("es188");
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();
        DataFarmDB dataFarmDB = new DataFarmDB();

        // tipo
        String index = "uspto";
        String type = "patent";

        String[] includes = {
                "priorityClaims",
                "documentId"
                , "documentType"
                , "documentTypeCN"
                , "documentTypeDE"
                , "applicationNumber"
                , "applicationDate"
                , "publicationNumber"
                , "registerNumber"
                , "publishingORG",
                "fileName",
                "patentId"
        };

        String[] docIds = {"us09788560b2"};

        String startDate = "20000101";
        String endDate = "20001231";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.existsQuery("earliestPriorityClaim"))
//                .filter(QueryBuilders.idsQuery().addIds(docIds))
                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
//                .must(QueryBuilders.existsQuery("priorityClaims"))
//                .must(QueryBuilders.matchQuery("priorityClaims.sequence","2"))
//                .must(QueryBuilders.matchQuery("priorityClaims.dataFormat","docdb"))
                ;

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setFetchSource(includes, null)
                .setSize(10)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        int count = 0;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<QueueVO> queueVOList = new ArrayList<>();

            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 100 == 0)
                        break;
//                        System.out.println(count);

                    List<PriorityClaim> totalPriorityClaims = new ArrayList<>();
                    PriorityClaim earliestPriorityClaim = null;

                    PatentVO patentVO = objectMapper.readValue(hit.getSourceAsString(), PatentVO.class);

                    List<PriorityClaim> priorityClaims = patentVO.getPriorityClaims();
                    if (priorityClaims != null)
                        totalPriorityClaims.addAll(priorityClaims);

                    String applicationNumber = patentVO.getApplicationNumber();
                    if (totalPriorityClaims.isEmpty()) {
                        // Base 처
                        PriorityClaim priorityClaimBase = new PriorityClaim();
                        priorityClaimBase.setBaseFlag(true);

                        if (applicationNumber != null)
                            priorityClaimBase.setPriorityApplicationNumber(applicationNumber);

                        String applicationDate = patentVO.getApplicationDate();
                        if (applicationDate != null)
                            priorityClaimBase.setPriorityApplicationDate(applicationDate);

                        String publishingORG = patentVO.getPublishingORG();
                        if (publishingORG != null)
                            priorityClaimBase.setPriorityApplicationCountry(publishingORG);

                        String patentId = patentVO.getPatentId();
                        if (patentId != null)
                            priorityClaimBase.setPatentId(patentId);

                        totalPriorityClaims.add(priorityClaimBase);
                    }

                    if (hit.getId().matches("^us.+")) {
                        PriorityClaim earliestRelatedDoc = usReleatedDoc(conn, applicationNumber.replaceAll("[/|\\-]", ""));
                        if (earliestRelatedDoc != null)
                            totalPriorityClaims.add(earliestRelatedDoc);
                    }

                    if (totalPriorityClaims.isEmpty()) {
                        System.out.println(hit.getId() + "totalPriorityClaims Empty.");
                        continue;
                    }

                    if (totalPriorityClaims.size() == 1)
                        earliestPriorityClaim = totalPriorityClaims.get(0);
                    else {
                        Optional<PriorityClaim> optionalPriorityClaim = totalPriorityClaims
                                .stream().filter(x -> x.getPriorityApplicationDate() != null)
                                .sorted(Comparator.comparing(PriorityClaim::getPriorityApplicationDate))
                                .findFirst();

                        if (optionalPriorityClaim.isPresent())
                            earliestPriorityClaim = optionalPriorityClaim.get();
                    }

                    if (earliestPriorityClaim == null)
                        continue;

                    Map<String, Object> jsonMap = new HashMap<>();
                    jsonMap.put("earliestPriorityClaim", earliestPriorityClaim);
                    String json = objectMapper.writeValueAsString(jsonMap);
                    System.out.println(hit.getId() + " : " + json);

                    esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", hit.getId())
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                    QueueVO queueVO = new QueueVO();
                    queueVO.setEsindex(index);
                    queueVO.setDocumentId(hit.getId());
                    queueVO.setLevel(9);
                    queueVOList.add(queueVO);

                    if (queueVOList.size() > 10)
                        dataFarmDB.insertESQueue(queueVOList);

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            System.out.println(count);
            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static PriorityClaim usReleatedDoc(Connection conn, String apnum) {
        String query = "select * from USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN " +
                " where application_number = '" + apnum + "' and con_filing_date != '' and con_filing_date not like '0%' " +
                " order by con_filing_date limit 1";

//        System.out.println(query);

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String applicationNumber = rs.getString("con_application_number");
                String applicationDate = rs.getString("con_filing_date");
//                System.out.println(applicationNumber + " : " + applicationDate);

                if (applicationNumber != null && applicationDate != null) {
                    PriorityClaim priorityClaim = new PriorityClaim();
                    priorityClaim.setPriorityApplicationNumber(applicationNumber);
                    priorityClaim.setPriorityApplicationDate(applicationDate.replaceAll("-", ""));

                    String country = "US";
                    if (applicationNumber.contains("PCT"))
                        country = "WO";

                    priorityClaim.setPriorityApplicationCountry(country);

                    return priorityClaim;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
