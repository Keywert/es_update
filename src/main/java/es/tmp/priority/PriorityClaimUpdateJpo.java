package es.tmp.priority;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.v1.PatentVO;
import es.model.v1.PriorityClaim;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.NumberUtils;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class PriorityClaimUpdateJpo {
    public static void main(String[] args) {
        ESConfig esConfig = new ESConfig("es18");
        String index = "jpo";
        String type = "patent";

        DataFarmDB dataFarmDB = new DataFarmDB();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        String[] includes = {
                "priorityClaims",
                "documentId"
                , "documentType"
                , "documentTypeCN"
                , "documentTypeDE"
                , "applicationNumber"
                , "applicationDate"
                , "publicationNumber"
                , "registerNumber"
                , "publishingORG",
                "fileName",
                "patentId",
                "originalApplicationNumber",
                "originalApplicationDate",
                "originalApplicationKind"
        };
        String[] docIds = {"jp2020545090b2","jp2021543952b2","jp2022199705a"};

        String startDate = "20050101";
        String endDate = "20251231";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.matchQuery("priorityClaims.priorityApplicationCountry", "US"))
//                .filter(QueryBuilders.existsQuery("originalApplicationNumber"))
                .mustNot(QueryBuilders.existsQuery("earliestPriorityClaim"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
//                .filter(QueryBuilders.matchQuery("priorityClaims.priorityApplicationCountry","CA"))
//                .filter(QueryBuilders.existsQuery("originalApplicationNumber"))
//                .filter(QueryBuilders.existsQuery("priorityClaims"))
//                .filter(QueryBuilders.idsQuery().addIds(docIds))
                ;

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setFetchSource(includes, null)
                .setSize(10)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        int count = 0;
//        long start = System.currentTimeMillis();

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<QueueVO> queueVOList = new ArrayList<>();

            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
//                    start = System.currentTimeMillis();

                    if (++count % 1000 == 0) {
                        System.out.println(count);
//                        long end = System.currentTimeMillis();
//                        System.out.println("실행 시간 : " + (end - start) / 1000.0 + " 평균 시간 : " + ((end - start) / 1000.0) / 100);
//                        start = System.currentTimeMillis();
                    }

                    List<PriorityClaim> totalPriorityClaims = new ArrayList<>();
                    PriorityClaim earliestPriorityClaim = null;

                    PatentVO patentVO = objectMapper.readValue(hit.getSourceAsString(), PatentVO.class);
                    List<PriorityClaim> priorityClaims = patentVO.getPriorityClaims();
                    List<PriorityClaim> priorityClaimsNorm = null;
                    if (priorityClaims != null) {
                        priorityClaimsNorm = priorityClaimsNorm(priorityClaims);
                        if (priorityClaimsNorm != null)
                            totalPriorityClaims.addAll(priorityClaimsNorm);
                    }

                    String docId = hit.getId();
                    if (docId.matches("^(kr|jp).+")) {
                        String originalApplicationNumber = patentVO.getOriginalApplicationNumber();
                        if (originalApplicationNumber != null) {
//                            System.out.println(originalApplicationNumber + " : " + patentVO.getOriginalApplicationDate());
                            PriorityClaim priorityClaim = new PriorityClaim();
                            priorityClaim.setPriorityApplicationCountry(docId.substring(0, 2).toUpperCase());
                            priorityClaim.setPriorityApplicationDate(patentVO.getOriginalApplicationDate());
                            if (originalApplicationNumber.matches("^(10|20|30)(19|20)[0-9]{2}[0-9]{7}$")) {
                                originalApplicationNumber = originalApplicationNumber.substring(0, 2) + "-" + originalApplicationNumber.substring(2, 6) + "-" + originalApplicationNumber.substring(6);
                            } else if (originalApplicationNumber.matches("^(19|20)[0-9]{2}[0-9]{6}$")){
                                originalApplicationNumber = originalApplicationNumber.substring(0, 4) + "-" + originalApplicationNumber.substring(4) ;
                            }
                            priorityClaim.setPriorityApplicationNumber(originalApplicationNumber);
                            String kind = patentVO.getOriginalApplicationKind();
                            if (kind != null) {
                                priorityClaim.setKind(kind);
                            }

                            totalPriorityClaims.add(priorityClaim);
                        }
                    }

                    if (hit.getId().matches("^us.+")) {
                        String applicationNumber = patentVO.getApplicationNumber();

                        PriorityClaim earliestRelatedDoc = usReleatedDoc(conn, applicationNumber.replaceAll("[/|\\-]", ""));
                        if (earliestRelatedDoc != null)
                            totalPriorityClaims.add(earliestRelatedDoc);
                    }

                    String applicationNumber = patentVO.getApplicationNumber();
                    if (totalPriorityClaims.isEmpty()) {
                        // Base 처
                        PriorityClaim priorityClaimBase = new PriorityClaim();
                        priorityClaimBase.setBaseFlag(true);

                        String applicationDate = patentVO.getApplicationDate();
                        if (applicationDate != null)
                            priorityClaimBase.setPriorityApplicationDate(applicationDate);

                        String publishingORG = patentVO.getPublishingORG();
                        if (publishingORG != null) {
                            priorityClaimBase.setPriorityApplicationCountry(publishingORG);
                            if (publishingORG.matches("^KR$")
                                    && applicationNumber.matches("^(10|20|30)(19|20)[0-9]{2}[0-9]{7}$")) {
                                applicationNumber = applicationNumber.substring(0, 2) + "-" + applicationNumber.substring(2, 6) + "-" + applicationNumber.substring(6);
                            }
                        }

                        if (applicationNumber != null) {
                            priorityClaimBase.setPriorityApplicationNumber(applicationNumber);
                        }

                        String patentId = patentVO.getPatentId();
                        if (patentId != null)
                            priorityClaimBase.setPatentId(patentId);

                        totalPriorityClaims.add(priorityClaimBase);
                    }

                    if (totalPriorityClaims.isEmpty()) {
                        System.out.println(hit.getId() + "totalPriorityClaims Empty.");
                        continue;
                    }

                    if (totalPriorityClaims.size() == 1)
                        earliestPriorityClaim = totalPriorityClaims.get(0);
                    else {
                        Optional<PriorityClaim> optionalPriorityClaim = totalPriorityClaims
                                .stream().filter(x -> x.getPriorityApplicationDate() != null)
                                .sorted(Comparator.comparing(PriorityClaim::getPriorityApplicationDate))
                                .findFirst();

                        if (optionalPriorityClaim.isPresent())
                            earliestPriorityClaim = optionalPriorityClaim.get();
                    }

                    if (earliestPriorityClaim == null)
                        continue;

                    Map<String, Object> jsonMap = new HashMap<>();
                    jsonMap.put("earliestPriorityClaim", earliestPriorityClaim);

                    if (priorityClaimsNorm != null)
                        jsonMap.put("priorityClaims", priorityClaimsNorm);

                    String json = objectMapper.writeValueAsString(jsonMap);
//                    System.out.println(hit.getId() + " : " + json);
                    esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", hit.getId())
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

//                    QueueVO queueVO = new QueueVO();
//                    queueVO.setEsindex(index);
//                    queueVO.setDocumentId(hit.getId());
//                    queueVO.setLevel(20);
//                    queueVOList.add(queueVO);
//
//                    if (queueVOList.size() > 10) {
//                        dataFarmDB.insertESQueue(queueVOList);
//                        queueVOList.clear();
//                    }

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            System.out.println(count);
            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static PriorityClaim usReleatedDoc(Connection conn, String apnum) {
        String query = "select * from USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN " +
                " where application_number = '" + apnum + "' and con_filing_date != '' and con_filing_date not like '0%' " +
                " order by con_filing_date limit 1";

//        System.out.println(query);

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String applicationNumber = rs.getString("con_application_number");
                String applicationDate = rs.getString("con_filing_date");
//                System.out.println(applicationNumber + " : " + applicationDate);

                if (applicationNumber != null && applicationDate != null) {
                    PriorityClaim priorityClaim = new PriorityClaim();
                    priorityClaim.setPriorityApplicationDate(applicationDate.replaceAll("-", ""));

                    String country = "US";
                    if (applicationNumber.contains("PCT"))
                        country = "WO";

                    String apNorm = NumberUtils.getNumber(country, applicationNumber, "AN");

                    priorityClaim.setPriorityApplicationNumber(apNorm);
                    priorityClaim.setPriorityApplicationCountry(country);

                    return priorityClaim;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    private static List<PriorityClaim> priorityClaimsNorm(List<PriorityClaim> priorityClaims) {
        if (priorityClaims == null)
            return null;
        try {
            List<PriorityClaim> priorityClaimList = new ArrayList<>();
            for (PriorityClaim priorityClaim : priorityClaims) {
                String country = priorityClaim.getPriorityApplicationCountry();
                if (country != null) {
                    if (!country.matches("^[A-Z]{2}$")) {
                        System.out.println(new ObjectMapper().writeValueAsString(priorityClaim));

                    }

                    if (country.matches("^[A-z]{2}$")) {
                        String apnum = priorityClaim.getPriorityApplicationNumber();
                        String apNorm = NumberUtils.getNumber(country, apnum, "AN");
                        priorityClaim.setPriorityApplicationNumber(apNorm);
                    }
                }

                priorityClaimList.add(priorityClaim);
            }

            if (!priorityClaimList.isEmpty())
                return priorityClaimList;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
