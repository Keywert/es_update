package es.tmp.legal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.ESDocIdCheck;
import es.module.utility.MakeDocID;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LegalStatusJPUpdate {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es18"));
//        esConfigs.add(new ESConfig("es13"));
//        esConfigs.add(new ESConfig("es43"));

        if (esConfigs.isEmpty())
            return;

        DataFarmDB dataFarmDB = new DataFarmDB();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();
        String tableName = "JPO.APPLICATION";

//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.now().minus(Period.ofDays(6));

        // C0360A08, C0360A06? 심사중(C03500), 등록(C078000), 공개 (000001), 등록(000002,C0360A01)
        // 거절 (C03502,C0360A02) ,취하 (C0360A09,C0360A04,C0360A11) ,무효 (C0360A32,C078015,C078004,C0360A31), 각하(C0360A45,C0360A43)
        // 포기(C0360A05,C078003)

        String query = "select * from " + tableName +
                " where ES_APPLY = 0  " +
//                " and STATUS_CODE in ('000002','C078000') " +
//                " and APPLICATION_NUMBER like '200%'"+
//                " and STATUS_CODE not in ('000001') " +
//                " and STATUS_CODE not in ('C03500','C078000','000001','000002','C0360A06','C0360A08') " +
//                " and STATUS_CODE not in ('000001','C03500') " +
                //'C0360A09','C03502','000004','C078001', 'C078002', 'C078007', 'C078008', 'C078012', 'C078013','C0360A05', 'C078003'
//                " and STATUS_CODE in ('000004','C078001', 'C078002', 'C078007', 'C078008', 'C078012', 'C078013') " +
//                " and STATUS_CODE in ('C0360A09','C03502','000004','C078001', 'C078002', 'C078007', 'C078008', 'C078012', 'C078013','C0360A05', 'C078003') " +
//                " and APPLICATION_NUMBER not like '19%' " +
//                " and APPLICATION_NUMBER = '2012-525504' " +
                " limit 50000";

        String index = "jpo";
        String type = "patent";

        System.out.println(query);

        Statement st = null;
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, PatentFamilyVO> map = new HashMap<>();

            List<String> appNums = new ArrayList<>();

            while (rs.next()) {
                if (++count % 100 == 0) {
                    System.out.println(count);
//                    Thread.sleep(1000);
                }

                String applicationNumber = rs.getString("APPLICATION_NUMBER");
                String documentType = rs.getString("TYPE");
                String registration = rs.getString("STATUS_CODE");

                if (registration == null || "".equals(registration))
                    continue;

                if (applicationNumber == null || "".equals(applicationNumber))
                    continue;

                appNums.add(documentType + applicationNumber);

                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();
                patentFamilyVO.setLegalStatus(registration);

                List<String> docIds = MakeDocID.getDocIds(index, applicationNumber, documentType);

                if (docIds == null)
                    continue;

//                String json = objectMapper.writeValueAsString(patentFamilyVO);

                for (String docId : docIds)
                    map.put(docId, patentFamilyVO);

                if (map.size() > 100) {
                    List<String> hitDocIds = ESDocIdCheck.getJPHitDocIds(esConfigs.get(0), index, map);

                    updateES(esConfigs, index, map, hitDocIds, dataFarmDB);

//                    Thread.sleep(100);

                    updateDB(conn, appNums);

                    map.clear();
                    appNums.clear();
                }
            }

            if (!map.isEmpty()) {
                List<String> hitDocIds = ESDocIdCheck.getJPHitDocIds(esConfigs.get(0), index, map);

                updateES(esConfigs, index, map, hitDocIds, dataFarmDB);
                Thread.sleep(1000);
                updateDB(conn, appNums);
            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            System.out.println(count);

        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void updateDB(Connection conn, List<String> appNums) {
        String query = "UPDATE JPO.APPLICATION SET ES_APPLY = 1 " +
                " WHERE TYPE = ? and APPLICATION_NUMBER = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String apnum : appNums) {
                if (!apnum.matches("^(P|U)(19|20)[0-9]{2}-[0-9]{6}$"))
                    continue;

//                System.out.println(apnum);
//                System.out.println(apnum.substring(0,1) + " : " + apnum.substring(1));

                preparedStatement.setString(1, apnum.substring(0, 1));
                preparedStatement.setString(2, apnum.substring(1));
                preparedStatement.addBatch();

            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateES(List<ESConfig> esConfigs, String index, Map<String, PatentFamilyVO> map, List<String> docIds, DataFarmDB dataFarmDB) {
        if (map == null)
            return;
        try {
            List<QueueVO> queueVOList = new ArrayList<>();

            for (String docId : docIds) {
//            System.out.println(docId);
                PatentFamilyVO patentFamilyVO = map.get(docId);
                if (patentFamilyVO == null)
                    continue;

                String json = new ObjectMapper().writeValueAsString(patentFamilyVO);

//                System.out.println(docId + " : " + json);

                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", docId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(docId);
                queueVO.setLevel(5);
                queueVOList.add(queueVO);
            }

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
