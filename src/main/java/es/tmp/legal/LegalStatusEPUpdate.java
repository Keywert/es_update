package es.tmp.legal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.ESDocIdCheck;
import es.module.utility.MakeDocID;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LegalStatusEPUpdate {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es13"));
//        esConfigs.add(new ESConfig("es43"));

        if (esConfigs.isEmpty())
            return;

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        DataFarmDB dataFarmDB = new DataFarmDB();

        String tableName = "EPO.APPLICATION";

        String query = "select * from " + tableName + " where ES_APPLY = 0 " +
                " and STATUS_CODE not in (255,8,7,6,5,3,2) limit 50000";

        System.out.println(query);

        String index = "epo";
        String type = "patent";

        Statement st = null;
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String,String> map = new HashMap<>();

            List<String> appNums = new ArrayList<>();

            while (rs.next()) {
                if (++count % 100 == 0) {
                    System.out.println(count);
                }

                String applicationNumber = rs.getString("APPLICATION_NUMBER");
                String registration = rs.getString("STATUS_CODE");

                if (registration == null || "".equals(registration))
                    continue;

                if (applicationNumber == null || "".equals(applicationNumber))
                    continue;

                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();
                patentFamilyVO.setLegalStatusEP(registration);

                List<String> docIds = MakeDocID.getDocIds(index, applicationNumber,"P");

                if (docIds == null)
                    continue;

                String json = objectMapper.writeValueAsString(patentFamilyVO);
                json = json.replaceAll("EP","");

                for (String docId : docIds)
                    map.put(docId,json);

                appNums.add(applicationNumber);

                if (map.size() > 100) {
                    List<String> hitDocIds = ESDocIdCheck.getHitDocIds(esConfigs.get(0),index,map);
                    updateES(esConfigs, index,map, hitDocIds,dataFarmDB);
//                    Thread.sleep(1000);
                    updateDB(conn,appNums);

                    map.clear();
                    appNums.clear();
                }


            }

            if(!map.isEmpty()) {
                List<String> hitDocIds = ESDocIdCheck.getHitDocIds(esConfigs.get(0),index,map);

                updateES(esConfigs, index,map, hitDocIds, dataFarmDB);
                Thread.sleep(1000);
                updateDB(conn,appNums);
            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            System.out.println(count);

        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private static void updateDB(Connection conn, List<String> appNums) {
        String query = "UPDATE EPO.APPLICATION SET ES_APPLY = 1 " +
                " WHERE APPLICATION_NUMBER = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String apnum : appNums) {
                if (!apnum.matches("^(19|20)[0-9]{2}-[0-9]{6}$"))
                    continue;

//                System.out.println(apnum);

                preparedStatement.setString(1, apnum);
                preparedStatement.addBatch();

            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateES(List<ESConfig> esConfigs, String index, Map<String, String> map, List<String> docIds, DataFarmDB dataFarmDB) {
        if (map == null)
            return;
        List<QueueVO> queueVOList = new ArrayList<>();

        for (String docId : docIds ) {
            String json = map.get(docId);

            if (json == null)
                continue;

//            System.out.println(docId + " : " + json);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", docId)
                        .doc(json.getBytes())
                        .upsert(json.getBytes()));

            QueueVO queueVO = new QueueVO();
            queueVO.setEsindex(index);
            queueVO.setDocumentId(docId);
            queueVO.setLevel(5);
            queueVOList.add(queueVO);
        }

        if (!queueVOList.isEmpty())
            dataFarmDB.insertESQueue(queueVOList);
    }
}
