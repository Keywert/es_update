package es.tmp.legal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.DateUtil;
import es.module.utility.ESDocIdCheck;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class LegalStatusUSUpdate {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es13"));
//        esConfigs.add(new ESConfig("es43"));
//        esConfigs.add(new ESConfig("es225"));
//        esConfigs.add(new ESConfig("es226"));
//        esConfigs.add(new ESConfig("es167"));

        if (esConfigs.isEmpty())
            return;

        DataFarmDB dataFarmDB = new DataFarmDB();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        String tableName = "USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA";
        String query = "select * from " + tableName + " A left outer join USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE B on A.application_number = B.application_number " +
                " where status is null and appl_status_code <> '' "
//                    " where status = 11 "
//                " where  appl_status_code <> '' and A.application_number in ('09064922','09060242','09064469','09064922','09118641','09200515','09202972','09208193','09223808','09274163','09405317','09433129','09438211','09468546','09472820','09473112','09477797','09486654','09494916','09497302','09499356','09502630','09508785','09514005','09538923','09555123','09583185','09619504','09665580','09668761','09673211','09682022','09683198','09683727','09685925','09729071')"
//                + " and appl_status_code not in ('172' ,'174' ,'180' ,'181' ,'182' ,'40' ,'404' ,'406' ,'407' ,'408' ,'41' ,'423' ,'424' ,'425' ,'426' ,'427' ,'428' ,'71' ,'80' ,'82','823' ,'824' ,'825' ,'826' ,'827' ,'83' ,'830' ,'833' ,'834' ,'837' ,'840' ,'841' ,'842' ,'843' ,'844' ,'845' ,'846' ,'847' ,'848','849' ,'850' ,'852' ,'854' ,'857' ,'859' ,'860' ,'861' ,'862' ,'864' ,'865' ,'870' ,'871' ,'872' ,'873' ,'874' ,'877' ,'878' ,'879','880' ,'881' ,'882' ,'883' ,'884' ,'885' ,'886' ,'887' ,'888' ,'889' ,'89' ,'890' ,'891' ,'892' ,'899' ,'90' ,'92' ,'93') "
                + " limit 500000";

        System.out.println(query);
        String index = "uspto";
        String type = "patent";

        Statement st = null;
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);
//            System.out.println(query);

            Map<String, String> map = new HashMap<>();

            List<String> appNums = new ArrayList<>();

            while (rs.next()) {
                if (++count % 10000 == 0) {
                    System.out.println(count);
                }

                String applicationNumber = rs.getString("application_number");
                String applicationDate = rs.getString("filing_date");
                String publicationDate = rs.getString("earliest_pgpub_date");
                String registerNumber = rs.getString("patent_number");
                String expireDate = rs.getString("expiration_date");

                String registration = rs.getString("appl_status_code");

                if (registration == null || "".equals(registration))
                    continue;

                if (applicationNumber == null || "".equals(applicationNumber))
                    continue;

                if (!applicationNumber.matches("^[0-9]{8}$")) {
                    System.out.println("Not Matched applicationNumber : " + applicationNumber);
                    continue;
                }

                appNums.add(applicationNumber);

                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();
                patentFamilyVO.setLegalStatus(registration);

                if (!patentFamilyVO.getLegalStatus().matches("^(IT|ID|II|IW|IA|DM)$")) {
                    if (expireDate != null && expireDate.matches("^(19|20)[0-9]{6}$")) {
//                        System.out.println(expireDate);
                        if (DateUtil.convertDateToInt(expireDate) < DateUtil.getCurrentIntegerTime()) {
                            patentFamilyVO.setLegalStatus("소멸");
                        }
                    }
                }

                String json = objectMapper.writeValueAsString(patentFamilyVO);

//                System.out.println(expireDate +" "+ applicationNumber + " : " + json);

                map.put(applicationNumber.substring(0, 2) + "-" + applicationNumber.substring(2), json);

                if (map.size() > 100) {
                    Map<String, String> docIdMap = ESDocIdCheck.getUSdocIds(esConfigs.get(0), index, map);

                    updateES(esConfigs, index, docIdMap,dataFarmDB);

                    Thread.sleep(1000);

                    updateDB(conn, appNums);

                    appNums.clear();
                    map.clear();
                }
            }

            if (!map.isEmpty()) {
                Map<String, String> docIdMap = ESDocIdCheck.getUSdocIds(esConfigs.get(0), index, map);

                updateES(esConfigs, index, docIdMap, dataFarmDB);
                Thread.sleep(1000);
                updateDB(conn, appNums);
            }


            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            System.out.println("Finish Count : " + count);


        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


    private static void updateDB(Connection conn, List<String> appNums) {
        String query = "UPDATE USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA SET status = 1 " +
                " WHERE application_number = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String apnum : appNums) {
                preparedStatement.setString(1, apnum);
                preparedStatement.addBatch();

            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateES(List<ESConfig> esConfigs, String index, Map<String, String> map, DataFarmDB dataFarmDB) {
        if (map == null)
            return;

        Iterator<String> iterator = map.keySet().iterator();
        List<QueueVO> queueVOList = new ArrayList<>();

        while (iterator.hasNext()) {
            String docId = iterator.next();
            String json = map.get(docId);

//            System.out.println(docId + " : " + json);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", docId)
                        .doc(json.getBytes())
                        .upsert(json.getBytes()));


            QueueVO queueVO = new QueueVO();
            queueVO.setEsindex(index);
            queueVO.setDocumentId(docId);
            queueVO.setLevel(5);
            queueVOList.add(queueVO);
        }

        if (!queueVOList.isEmpty())
            dataFarmDB.insertESQueue(queueVOList);

    }

}
