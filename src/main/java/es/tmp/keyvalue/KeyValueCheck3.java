package es.tmp.keyvalue;

import es.config.DBHelper;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class KeyValueCheck3 {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        ESConfig es188 = new ESConfig("es188");
        ESConfig es189 = new ESConfig("es189");
        ESConfig es18 = new ESConfig("es18");

        Map<String,ESConfig> esConfigMap = new HashMap<>();
        esConfigMap.put("epo",es188);
        esConfigMap.put("kipo",es188);
        esConfigMap.put("uspto",es188);
        esConfigMap.put("jpo",es18);
        esConfigMap.put("sipo",es189);

        Map<String, String> map = new HashMap<>();
        map.put("epo", "iprating.epo_rating");
        map.put("kipo", "iprating.kipo_rating");
        map.put("uspto","iprating.uspto_rating");
        map.put("jpo","iprating.jpo_rating");
        map.put("sipo","iprating.sipo_rating");

//        try {
        for (String index : map.keySet()) {
            String tableName = map.get(index);
            if (tableName == null)
                continue;

            System.out.println(index + " : " + tableName);
            Set<String> patentIdSet = getNotExistGradeInfo2(esConfigMap.get(index), index);
            if (patentIdSet != null) {
                System.out.println(patentIdSet.size());
                updateDB2(conn, tableName, patentIdSet);
            }

//            Set<String> apnSet = getNotExistGradeInfo(esConfigMap.get(index), index);
//            if (apnSet != null) {
//                System.out.println(apnSet.size());
//                updateDB(conn, tableName, apnSet);
//            }
        }

//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    private static void updateDB2(Connection conn, String tableName, Set<String> patentIdSet) {
        String query = "UPDATE " + tableName + " SET status = 8" +
                " WHERE patentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            int count = 0;

            for (String patentId : patentIdSet) {
                preparedStatement.setString(1, patentId);
                preparedStatement.addBatch();

                if (++count % 1000 == 0) {
                    System.out.println(count);
                    preparedStatement.executeBatch();
                    preparedStatement.clearBatch();
                }
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateDB(Connection conn, String tableName, Set<String> apnSet) {
        String query = "UPDATE " + tableName + " SET status = 8" +
                " WHERE APPLNO = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            int count = 0;

            for (String apn : apnSet) {
                preparedStatement.setString(1, apn);
                preparedStatement.addBatch();

                if (++count % 1000 == 0)
                    preparedStatement.executeBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Set<String> getNotExistGradeInfo2(ESConfig esConfig, String index) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("patentId"))
                .filter(QueryBuilders.rangeQuery("patentDate").from("20200101").to("20241231"))
                .mustNot(QueryBuilders.existsQuery("gradeInfo"))
                .filter(QueryBuilders.matchQuery("patentType", "B"));

        String[] fields = {"patentId"};
        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1000)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
        Set<String> set = new HashSet<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object patentId = hit.getSource().get("patentId");
                if (patentId == null)
                    continue;

                set.add(patentId.toString());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!set.isEmpty())
            return set;

        return null;
    }



    private static Set<String> getNotExistGradeInfo(ESConfig esConfig, String index) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.rangeQuery("patentDate").from("20200101").to("20241231"))
                .mustNot(QueryBuilders.existsQuery("gradeInfo"))
                .filter(QueryBuilders.matchQuery("patentType", "B"));

        String[] fields = {"applicationNumber","patentId"};
        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
        Set<String> apnSet = new HashSet<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object applicationNumber = hit.getSource().get("applicationNumber");
                if (applicationNumber == null)
                    continue;

                String apn = norm(index, applicationNumber.toString());
//                System.out.println(apn);

                apnSet.add(apn);

            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!apnSet.isEmpty())
            return apnSet;

        return null;
    }

    private static String norm(String index, String applicationNumber) {
        if (index.matches("epo")) {
            if (applicationNumber.matches("^(19|20)[0-9]{2}-[0-9]{6}$"))
                return "EP" + applicationNumber.substring(2).replaceAll("-", "");
        } else if (index.matches("uspto")) {
            if (applicationNumber.matches("^[0-9]{2}-[0-9]{6}$"))
                return "US" + applicationNumber.replaceAll("-", "");
        } else if (index.matches("kipo")) {
            if (applicationNumber.matches("^(19|20)[0-9]{2}-[0-9]{7}$")) {
                return "10" + applicationNumber.replaceAll("-", "");
            }
        }

        return null;
    }
}
