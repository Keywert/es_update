package es.tmp.keyvalue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.model.enums.WipoIpc;
import es.module.keyvalue.GradeEnum;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GradeVO {
    public String grade;
    public String gradeT;
    public String gradeR;
    public String gradeU;

    public String smart3Grade;
    public String smart3GradeT;
    public String smart3GradeR;
    public String smart3GradeU;

    public String getClass1() {
        WipoIpc wipoIpc = WipoIpc.findByFieldName(class3);
        if (wipoIpc == wipoIpc.EMPTY)
            return null;

        return wipoIpc.getClass1();
    }

    public String class1;

    public String getClass2() {
        WipoIpc wipoIpc = WipoIpc.findByFieldName(class3);
        if (wipoIpc == wipoIpc.EMPTY)
            return null;

        return wipoIpc.getClass2();
    }

    public String class2;

    public String getClass3() {
        WipoIpc wipoIpc = WipoIpc.findByFieldName(class3);
        if (wipoIpc == wipoIpc.EMPTY)
            return null;

        return wipoIpc.name();
    }


    public String class3;

    public String updateDate;

    public boolean aliveFlag;

    public boolean smart3Flag;


    public String getGrade() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(grade);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }

    public String getGradeT() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(gradeT);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }
    public String getGradeR() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(gradeR);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }
    public String getGradeU() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(gradeU);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }
    public String getSmart3Grade() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3Grade);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }
    public String getSmart3GradeT() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3GradeT);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }
    public String getSmart3GradeR() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3GradeR);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }
    public String getSmart3GradeU() {
        GradeEnum gradeEnum = GradeEnum.findByGradeEnum(smart3GradeU);
        if (gradeEnum == GradeEnum.EMPTY)
            return null;
        return gradeEnum.name();
    }

}
