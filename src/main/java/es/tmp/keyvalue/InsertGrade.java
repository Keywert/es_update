package es.tmp.keyvalue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class InsertGrade {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        List<ESConfig> esConfigs = new ArrayList<>();
//        esConfigs.add(new ESConfig("es205"));
//        esConfigs.add(new ESConfig("es18"));
//        esConfigs.add(new ESConfig("es13"));
//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es18"));

        DataFarmDB dataFarmDB = new DataFarmDB();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String updateDate = dtf.format(localDate);
        System.out.println("updateDate : " + updateDate);

//        String index = "kipo";
//        esConfigs.add(new ESConfig("es188"));
//        String type = "patent";
//        String bibloTable = "citation.bibliographic_kipo";
//        String gradeTable = "iprating.kipo_rating";
//        String query = "select * from " + bibloTable + " A inner join " + gradeTable + " B on A.applicationNumber = B.APPLNO " +
//                " where B.status = 8 and B.WERT_GRADE is not null ";

//                " and B.register_date > '20230501' ";

//        String index  = "uspto";
//        esConfigs.add(new ESConfig("es188"));
//        String type = "patent";
//        String bibloTable = "citation.bibliographic_uspto";
//        String gradeTable = "iprating.uspto_rating";
//        String query = "select * from " + bibloTable + " A inner join " + gradeTable + " B on A.patentId = B.patentId "+
//                " where B.status = 8  and B.WERT_GRADE is not null and B.patentId is not null ";
//                "and B.DOCID like 'US117%'";

//        String index  = "epo";
//        esConfigs.add(new ESConfig("es188"));
//        String type = "patent";
//        String bibloTable = "citation.bibliographic_epo";
//        String gradeTable = "iprating.epo_rating";
//        String query = "select * from " + bibloTable + " A inner join " + gradeTable + " B on A.patentId = B.patentId "+
//                " where B.status = 8  and B.WERT_GRADE is not null and B.patentId is not null " ;
//                "and B.DOCID like 'US1159%'";

        String index  = "jpo";
        esConfigs.add(new ESConfig("es18"));
        String type = "patent";
        String bibloTable = "citation.bibliographic_jpo";
        String gradeTable = "iprating.jpo_rating";
        String query = "select * from " + bibloTable + " A inner join " + gradeTable + " B on A.patentId = B.patentId "+
                " where B.status = 8  and B.WERT_GRADE is not null and B.patentId != '' " ;
//
//        String index  = "sipo";
//        esConfigs.add(new ESConfig("es189"));
//        String type = "patent";
//        String bibloTable = "citation.bibliographic_sipo";
//        String gradeTable = "iprating.sipo_rating";
//        String query = "select * from " + bibloTable + " A inner join " + gradeTable + " B on A.patentId = B.patentId "+
//                " where B.status = 8  and B.WERT_GRADE is not null and B.patentId != ''" ;

        if (esConfigs.isEmpty())
            return;

        System.out.println(query);

        int count = 0;
        Statement st = null;

        ObjectMapper objectMapper = new ObjectMapper();

        List<String> list = new ArrayList<>();

        String preApplicationNumber = "";

        List<QueueVO> queueVOList = new ArrayList<>();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (++count % 1000 == 0)
                    System.out.println(count);

                String documentId = rs.getString("documentId");
                if (documentId == null)
                    continue;

                String applicationNumber = rs.getString("APPLNO");
                if (applicationNumber == null)
                    continue;

                list.add(applicationNumber);

                GradeVO gradeVO = new GradeVO();

                Integer status = rs.getInt("status");

                gradeVO.setSmart3Flag(true);
                gradeVO.setAliveFlag(true);

                gradeVO.setGrade(rs.getString("WERT_GRADE"));
                gradeVO.setGradeT(rs.getString("WERT_T_GRADE"));
                gradeVO.setGradeR(rs.getString("WERT_R_GRADE"));
                gradeVO.setGradeU(rs.getString("WERT_U_GRADE"));
                gradeVO.setSmart3Grade(rs.getString("GRADE"));
                gradeVO.setSmart3GradeT(rs.getString("T_GRADE"));
                gradeVO.setSmart3GradeR(rs.getString("R_GRADE"));
                gradeVO.setSmart3GradeU(rs.getString("U_GRADE"));

                gradeVO.setClass1(rs.getString("TC_NAME1"));
                gradeVO.setClass2(rs.getString("TC_NAME2"));
                String TC_NAME3 = rs.getString("TC_NAME3");
                if (TC_NAME3 == null)
                    TC_NAME3 = rs.getString("TC_NAME2");
                gradeVO.setClass3(TC_NAME3);

//                gradeVO.setUpdateDate(rs.getString("UPDATE_DATE"));
                gradeVO.setUpdateDate(updateDate);


                Map<String, Object> map = new HashMap<>();

                map.put("gradeInfo", gradeVO);
                map.put("keyvalueUpdateDate", updateDate);

                String json = objectMapper.writeValueAsString(map);

//                System.out.println(documentId + " : " + json);

//                UpdateRequest updateRequest = new UpdateRequest(index, type, documentId)
//                        .doc(json.getBytes())
//                        .upsert(json.getBytes());
//                esConfig.bulkProcessor.add(updateRequest);


                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(documentId);
                queueVO.setLevel(5);

                queueVOList.add(queueVO);


                if (list.size() > 200 && preApplicationNumber != applicationNumber) {
                    updateDB(conn, gradeTable, list);
                    dataFarmDB.insertESQueue(queueVOList);
                    queueVOList.clear();
                    Thread.sleep(500);
                    list.clear();
                }
                preApplicationNumber = applicationNumber;
            }

            if (!list.isEmpty())
                updateDB(conn, gradeTable, list);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);


            System.out.println("Finish Count : " + count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String table, List<String> list) {
        String query = "UPDATE " + table + " SET status = 2 " +
                "WHERE APPLNO = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String id : list) {
                preparedStatement.setString(1, id);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
