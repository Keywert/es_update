package es.tmp.es;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.DBHelper;
import es.config.ESConfig;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ESRemoveOpoFamily2 {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es209"));
        esConfigs.add(new ESConfig("es18"));
        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es189"));
        esConfigs.add(new ESConfig("es232"));

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String[] indexies = {"pct","kipo", "jpo", "uspto", "sipo", "epo", "dpma", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo", "uspto_past", "jpo_past", "paj", "docdb"};

        String tableName = "family.opo_extended_error";
        String query = "select extendFamilyId from " + tableName + " where extendFamilyId = '380737170'";
        System.out.println(query);
        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> familyIdSet = new HashSet<>();
            while (rs.next()) {
                String extendFamilyId = rs.getString("extendFamilyId");
                if (extendFamilyId == null)
                    continue;

                familyIdSet.add(extendFamilyId);
            }

            if (familyIdSet.isEmpty())
                return;

            System.out.println(familyIdSet.size());

            for (String extendFamilyId : familyIdSet) {
                System.out.println(extendFamilyId);

                for (ESConfig esConfig : esConfigs) {
                    Map<String, String> removeDocIdMap = removeESField(esConfig, indexies, "extendFamilyId", extendFamilyId);
                    if (removeDocIdMap != null)
                        insertDB(conn, removeDocIdMap, extendFamilyId);
                }

                updateDB(conn, tableName, extendFamilyId);
            }


            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void insertDB(Connection conn, Map<String, String> map, String extendFamilyId) {
        String insertQuery = "INSERT ignore into family.opo_extended_docid_error " +
                "(" +
                "es_index, documentId, extendFamilyId" +
                ") " +
                " values(?,?,?) ";

        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (String docId : map.keySet()) {
                statement.setString(1, map.get(docId));
                statement.setString(2, docId);
                statement.setString(3, extendFamilyId);

                statement.addBatch();
            }
            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void updateDB(Connection conn, String table, String familyId) {
        String query = "UPDATE " + table + " SET status = 1 " +
                "WHERE  extendFamilyId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            preparedStatement.setString(1, familyId);
            preparedStatement.addBatch();

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Map<String, String> removeESField(ESConfig esConfig, String[] indexies, String fieldName, String extendFamilyId) {
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.matchQuery(fieldName, extendFamilyId));

        String type = "patent";

        SearchResponse scrollResp = esConfig.client.prepareSearch()
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setSize(1)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            int count = 0;
            Map<String, String> map = new HashMap<>();

            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 1000 == 0)
                        System.out.println(count);

                    String index = hit.getIndex();
                    if (index.matches("^opo_.+$"))
                        continue;

                    System.out.println(hit.getIndex() + " : " + hit.getId());
                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
                    if (jsonNode == null)
                        continue;

                    JsonNode extendFamilyNode = jsonNode.get("extendFamilyId");
                    if (extendFamilyNode == null)
                        continue;
                    ((ObjectNode) jsonNode).remove("extendFamilyId");

                    JsonNode extendedFamilyId = jsonNode.get("extendedFamilyId");
                    if (extendedFamilyId != null)
                        ((ObjectNode) jsonNode).put("completeFamilyId", "KW"+ extendedFamilyId.textValue());

                    esConfig.bulkProcessor.add(new IndexRequest(hit.getIndex(), type, hit.getId())
                            .source(objectMapper.writeValueAsString(jsonNode)));

                    map.put(hit.getId(), hit.getIndex());

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!map.isEmpty())
                return map;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
