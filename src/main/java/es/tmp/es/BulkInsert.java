package es.tmp.es;

import es.config.ESConfig;
import org.elasticsearch.action.update.UpdateRequest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

//import org.elasticsearch.common.netty.util.internal.SystemPropertyUtil;
//import org.elasticsearch.common.settings.ImmutableSettings;

/**
 * Created by hoon on 2015-06-11.
 */
public class BulkInsert {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("Log");
        FileHandler fileHandler;

//        DBHelper dbHelper = new DBHelper();
//        Connection conn = dbHelper.getConn_local();

        String tableName = "tmp.t_20200623_1";

        if (args.length == 0) {
            System.out.println("Usage : java -jar bulk.jar [Input Paths] [IndexName]");
            return;
        }

        ArrayList<File> files = getFilepathList(args[0]);

        if (files == null) {
            System.out.println("File Not Exists ~!");
            return;
        }

        String index = args[1];
        String type = "patent";
        String clustName = "es29"; //,, opo_kipo
//        String clustName = "es28"; //opo_uspto, opo_jpo, opo_other, opo_epo
//                String clustName = "es18"; // opo_kipo


//        String clustName = "es168"; //
//        String clustName = "es206"; //opo_jpo
//        String clustName = "es218"; //opo_epo
//        String clustName = "es188";



//        String clustName = "es168";
//        String clustName = "es206";
//        String clustName = "es18";
//        String clustName = "es218";

//        String clustName = "es20";
//        String clustName = "es50";
//        String clustName = "es51";
//        String clustName = "es218";
//        String clustName = "es189";

        ESConfig esConfig = new ESConfig(clustName);
        System.out.println("ES Cluster Name : " + clustName + ", Input File Path : " + args[0] + " " + args[1]);
        System.out.println("File count : " + files.size());


        int i = 0;
        for (File f : files) {
            if (++i % 10000 == 0) {
//                System.out.println(i);
                System.out.println(i + " : " + f.getAbsolutePath());
            }

//            if (i < 740000)
//                continue;

            //
            try {
//                esConfig.bulkProcessor.add(new IndexRequest(index, type, f.getName().replaceFirst("\\.json", "")).source(Files.readAllBytes(Paths.get(f.getAbsolutePath()))));
                esConfig.bulkProcessor.add((new UpdateRequest(index, type, f.getName().replaceFirst("\\.json", ""))
                        .doc(Files.readAllBytes(Paths.get(f.getAbsolutePath()))))
                        .upsert(Files.readAllBytes(Paths.get(f.getAbsolutePath()))));

            } catch (IOException e) {
                logger.info(f.getName() + " : " + e.toString());
                e.printStackTrace();
                return;
            }
        }


        try {
            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("inserted Count: " + i);
        System.out.println("BulkInsert Finished~!");
        esConfig.client.close();
    }

    public static ArrayList getFilepathList(String filepath) {
        ArrayList arrayList = new ArrayList();

        File dir = new File(filepath);

        if (dir.exists() == false) {
            System.out.println("Not exist File Path.");
            return null;
        }

        visitDir(arrayList, dir);

        return arrayList;
    }

    public static void visitDir(ArrayList arrayList, File file) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles) {
                visitDir(arrayList, f);
            }
        } else {
//            if( file.getName().matches("^kr(\\d){11}(\\w)(\\d)?\\.json$"))
//                arrayList.add(file);
            if (file.getName().matches(".+\\.json$"))
                arrayList.add(file);
//             jpo
//            if (file.getName().matches(".+[^5-9]\\.json$"))
//                arrayList.add(file);
        }
    }
}



