package es.tmp.es;

import es.module.db.DataFarmDB;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import es.config.ESConfig;

public class IndexFileCheck {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage : java -jar IndexFileCheck.jar [Input Paths] [IndexName]");
            return;
        }

        String index = args[1];
        if (!index.matches("^(uspto|kipo|aupo|capo|frpo|gbpo|inpo|itpo|rupo|tipo|dpma|pct|epo|jpo|sipo|jpo_past|uspto_past|docdb|paj)$")) {
            System.out.println(index + " is not a valid index name");
            return;
        }

        Map<String, ESConfig> datafarmESMap = new HashMap<>();
        datafarmESMap.put("es18", new ESConfig("es18"));
        datafarmESMap.put("es188", new ESConfig("es188"));
        datafarmESMap.put("es189", new ESConfig("es189"));

        DataFarmDB dataFarmDB = new DataFarmDB();
        String clusterName = dataFarmDB.selectClusterInfo(index, 1);
        if (clusterName == null)
            return;

        System.out.println(clusterName);
        ESConfig esConfig = datafarmESMap.get(clusterName);
        if (esConfig == null) {
            return;
        }

        ArrayList<File> files = getFilepathList(args[0]);

        if (files == null) {
            System.out.println("File Not Exists ~!");
            return;
        }

        System.out.println("File count : " + files.size());

        int count = 0;

        HashMap<String, String> idsMap = new HashMap<>();
        for (File f : files) {
            String filename = f.getName().replaceFirst("\\.json", "");
            idsMap.put(filename, f.getAbsolutePath());
        }

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(idsMap.keySet()));

        String[] fields = {"documentId"};

        SearchResponse response = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(100000))
                .setQuery(bqb)
                .setSize(1000)
                .setFetchSource(fields, null)
                .execute()
                .actionGet();

        do {
            for (SearchHit hit : response.getHits().getHits()) {
                idsMap.remove(hit.getId());
            }
            response = esConfig.client.prepareSearchScroll(response.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (response.getHits().getHits().length != 0);

        System.out.println("Copy File Size : " + idsMap.size());

        Iterator<String> iterator = idsMap.keySet().iterator();

        while(iterator.hasNext()) {
            String key = iterator.next();

            if (count++ % 3000 == 0)
                System.out.println(count);

            String outPath = String.format( args[0] + "_checked/%s/%s/%s/",
                    key.substring(0, 2),key.substring(2, 6), key.substring(6));

            File dest = new File(outPath);
            if (!dest.exists())
                dest.mkdirs();
            outPath += key+".json";

            new File(idsMap.get(key)).renameTo(new File(outPath));
        }

        System.out.println("Copied Count : "+ count);

    }

    public static ArrayList getFilepathList(String filepath) {
        ArrayList arrayList = new ArrayList();

        File dir = new File(filepath);

        if (dir.exists() == false) {
            System.out.println("Not exist File Path.");
            return null;
        }

        visitDir(arrayList, dir);

        return arrayList;
    }

    public static void visitDir(ArrayList arrayList, File file) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles)
                visitDir(arrayList, f);

        } else {
//            if( file.getName().matches("^kr(\\d){11}(\\w)(\\d)?\\.json$"))
//                arrayList.add(file);
            if (file.getName().matches(".+\\.json$"))
                arrayList.add(file);
//             jpo
//            if (file.getName().matches(".+[^5-9]\\.json$"))
//                arrayList.add(file);
        }
    }
}
