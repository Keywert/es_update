package es.tmp.es;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class ESPutMapping {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));
        esConfigs.add(new ESConfig("es209"));

//        String[] indexis = {"tipo", "docdb", "dpma", "inpo", "paj", "itpo", "aupo", "sipo", "pct","uspto_past", "epo", "jpo_past", "jpo", "gbpo", "uspto", "kipo", "capo", "rupo", "frpo"};
//        String[] indexis = {"kipo", "uspto", "epo", "pct"};
//
//        esConfigs.add(new ESConfig("es18"));
//        String[] indexis = {"jpo","pct"};

//        esConfigs.add(new ESConfig("es188"));
//        String[] indexis = {"docdb", "dpma", "kipo", "epo", "jpo_past","uspto_past","paj"};

//        esConfigs.add(new ESConfig("es189"));
//        String[] indexis = {"tipo", "inpo","itpo", "aupo", "sipo", "gbpo", "capo", "rupo", "frpo"};

//        esConfigs.add(new ESConfig("es188"));
        String[] indexis = {"epo"};

//        ESConfig testES = new ESConfig("es167");

//        String mapping = "{\"properties\":{\"earliestPriorityClaim\":{\"properties\":{\"applicationCountry\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"dataFormat\":{\"type\":\"string\"},\"docdbApplicationNumber\":{\"type\":\"string\"},\"epodocApplicationNumber\":{\"type\":\"string\"},\"kind\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"nationalApplicationNumber\":{\"type\":\"string\"},\"originalApplicationNumber\":{\"type\":\"string\"},\"otherApplicationNumber\":{\"type\":\"string\"},\"priorityApplicationCountry\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"priorityApplicationDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"priorityApplicationNumber\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"sequence\":{\"type\":\"string\"},\"baseFlag\":{\"type\":\"boolean\"}}}}}";
//        String mapping = "{\"properties\":{\"applicantCount\":{\"type\":\"long\"},\"usAssigneeCount\":{\"type\":\"long\"},\"currentApplicantCount\":{\"type\":\"long\"},\"rightHolderCount\":{\"type\":\"long\"},\"currentRightHolderCount\":{\"type\":\"long\"},\"inventorCount\":{\"type\":\"long\"},\"agentCount\":{\"type\":\"long\"},\"examinerCount\":{\"type\":\"long\"}}}";
//        String mapping = "{\"properties\":{\"assignment\":{\"properties\":{\"assignmentFlag\":{\"type\":\"boolean\"},\"licenseFlag\":{\"type\":\"boolean\"},\"pledgeFlag\":{\"type\":\"boolean\"}}}}}";
//        String mapping = "{\"properties\":{\"sequence\":{\"properties\":{\"seqContent\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"seqFlag\":{\"type\":\"boolean\"}}},\"seqUpdateDate\":{\"type\":\"date\",\"format\":\"basic_date\"}}}";
//        String mapping = "{\"properties\":{\"litigationInfo\":{\"properties\":{\"actionTypes\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"hasLitigation\":{\"type\":\"boolean\"},\"caseIds\":{\"type\":\"string\",\"index\":\"not_analyzed\"}}}}}";
        String mapping = "{\"properties\":{\"upInfo\":{\"properties\":{\"unitaryType\":{\"type\":\"string\",\"index\":\"not_analyzed\"},\"upEffectDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"upRegistrationDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"upRegistrationFlag\":{\"type\":\"boolean\"},\"upRevocationDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"upcOptOutDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"upcOptOutFlag\":{\"type\":\"boolean\"},\"upcOptWithdrawalDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"upcOptWithdrawalFlag\":{\"type\":\"boolean\"},\"uprDecisionDate\":{\"type\":\"date\",\"format\":\"basic_date\"},\"uprFilingDate\":{\"type\":\"date\",\"format\":\"basic_date\"}}}}}";
//        putMapping(testES, "uspto", mapping);

        for (ESConfig esConfig : esConfigs)
            for (String index : indexis)
                putMapping(esConfig, index, mapping);

    }

    private static void putMapping(ESConfig esConfig, String index, String mapping) {
        try {
//            System.out.println(mapping);

            XContentBuilder builder = XContentFactory.jsonBuilder();
//            builder.startObject();
//            {
//                builder.startObject("properties");
//                {
//                    builder.startObject("blogId");{
//                    builder.field("type", "integer");
//                }
//                    builder.endObject();
//                    builder.startObject("isGuestPost");{
//                    builder.field("type", "boolean");
//                }
//                    builder.endObject();
//                    builder.startObject("voteCount");{
//                    builder.field("type", "integer");
//                }
//                    builder.endObject();
//                }
//                builder.endObject();
//            }
//            builder.endObject();


            Map<String, Object> map = jsonToMap(mapping);
            PutMappingRequest putMappingRequest = new PutMappingRequest(index);
            putMappingRequest
//                    .type("patent").source(mapping,XContentType.JSON)
                    .type("patent").source(map)
            ;
            AcknowledgedResponse acknowledgedResponse = esConfig.client.admin().indices().putMapping(putMappingRequest).get();
            System.out.println("Put Mapping response : " + acknowledgedResponse.isAcknowledged());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Map<String, Object> jsonToMap(String json) throws Exception
    {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String,Object>>() {};

        return objectMapper.readValue(json, typeReference);
    }

}
