package es.tmp.es;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.Applicant;
import es.model.bulk.Patent;
import es.model.enums.WipoIpc;
import es.module.dataproc.InsertPatentToDB;
import es.module.dataproc.norm.RepresentApn;
import es.module.db.DataFarmDB;
import es.module.file.FileProc;
import es.module.kipris.KiprisAPI;
import es.module.utility.PatentID;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.regex.Matcher;

public class BulkMain {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("Log");
        FileHandler fileHandler;

        if (args.length == 0) {
            System.out.println("Usage : java -jar bulk.jar [Input Paths] [IndexName]");
            return;
        }

        Map<String, ESConfig> datafarmESMap = new HashMap<>();
        datafarmESMap.put("es18", new ESConfig("es18"));
        datafarmESMap.put("es188", new ESConfig("es188"));
        datafarmESMap.put("es189", new ESConfig("es189"));

        String index = args[1];
        if (!index.matches("^(uspto|kipo|aupo|capo|frpo|gbpo|inpo|itpo|rupo|tipo|dpma|pct|epo|jpo|sipo|jpo_past|uspto_past|docdb|paj)$")) {
            System.out.println(index + " is not a valid index name");
            return;
        }

        DataFarmDB dataFarmDB = new DataFarmDB();
        String clusterName = dataFarmDB.selectClusterInfo(index, 1);
        if (clusterName == null)
            return;

        System.out.println(clusterName);

        ESConfig esConfig = datafarmESMap.get(clusterName);
        if (esConfig == null) {
            return;
        }

        String type = "patent";
        KiprisAPI kiprisAPI = new KiprisAPI();
        RepresentApn representApn = new RepresentApn();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        System.out.println(dtf.format(localDate) + " Input File Path : " + args[0] + " " + args[1]);

        ArrayList<File> files = getFilepathList(args[0]);

        if (files == null) {
            System.out.println("File Not Exists ~!");
            return;
        }
        System.out.println("File count : " + files.size());

        ObjectMapper objectMapper = new ObjectMapper();

        ArrayList<Patent> patents = new ArrayList<>();

        Matcher matcher;

        String org = "";
        int count = 0;
        for (File f : files) {
            if (++count % 500 == 0) {
                System.out.println(count + " " + f.getAbsolutePath());
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
//            if (count < 29000)
//                continue;

//            if (count % 500 == 0) {
//                try {
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }

            org = getORG(f.getName());
            if (index.matches("sipo_person"))
                org = index;
//            System.out.println("Org : " + org);

            try {
                String json = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())), "UTF-8");
//                System.out.println(json);

                if (org != null) {
                    JsonNode jsonNode = objectMapper.readTree(json);
//                  System.out.println(objectMapper.writeValueAsString(jsonNode));

                    // 1. 문헌ID, 문헌번호, 문헌일 추가, 업데이트 일자 추가
                    ((ObjectNode) jsonNode).put("lastUpdateDate", dtf.format(localDate));
                    insertPatentInfo(jsonNode);
                    json = objectMapper.writeValueAsString(jsonNode);

                    Patent patent = objectMapper.readValue(json, Patent.class);
                    patent.setOrg(org);

                    Map<String, Patent> map = new HashMap<>();
                    map.put(patent.getDocumentId(), patent);
                    String registerNumber = patent.getRegisterNumber();

                    if (patent.getDocumentId().matches("^(kr|us|ep|jp).+$")) {
                        List<Patent> patentList = patent.getPairPatent(esConfig);
                        if (patentList != null) {
                            for (Patent pairPatent : patentList) {
                                if (patent.merge(pairPatent))
                                    map.put(patent.getDocumentId(), patent);
                            }
                        }
                    }

                    Map<String, List<Applicant>> additionalMap = new HashMap<>();
                    if (patent.getDocumentId().matches("^kr.+$")) {
                        String applicationNumber = patent.getApplicationNumber();
//                        System.out.println(applicationNumber);
                        List<Applicant> personList = new ArrayList<>();
//                        List<Applicant> rApplicants = kiprisAPI.rpstApplicantInfoV2(applicationNumber);
//                        if (rApplicants != null)
//                            System.out.println("rApplicants : " + objectMapper.writeValueAsString(rApplicants));

                        List<Applicant> cApplicants = kiprisAPI.patentApplicantInfo(applicationNumber);
                        if (cApplicants != null) {
//                            System.out.println("cApplicants : " + objectMapper.writeValueAsString(cApplicants));
                            additionalMap.put("cApplicants", cApplicants);
                            personList.addAll(cApplicants);
                        }

                        if (registerNumber != null) {
                            List<Applicant> lastRightHolders = kiprisAPI.registrationLastRightHolderInfo(registerNumber);
                            if (lastRightHolders != null) {
//                                System.out.println("lastRightHolders : " + objectMapper.writeValueAsString(lastRightHolders));
                                additionalMap.put("lastRightHolders", lastRightHolders);
                                personList.addAll(lastRightHolders);
                            }
                        }

                        if (!personList.isEmpty())
                            additionalMap.put("personList", personList);

                    } else if (patent.getDocumentId().matches("^jp.+$")) {
                        List<Applicant> applicantInfo = representApn.getJPApplicantInfo(conn, patent.getApplicantInfo());
                        if (applicantInfo != null)
                            patent.setApplicantInfo(applicantInfo);
                    }

                    /// data
                    patent.addData(additionalMap);
                    List<Applicant> applicantInfo = patent.getApplicantInfo();
                    if (applicantInfo != null) {
//                        System.out.println(objectMapper.writeValueAsString(applicantInfo));
                        ArrayNode arrayNode = objectMapper.valueToTree(applicantInfo);
                        ((ObjectNode) jsonNode).put("applicantInfo", arrayNode);
                    }

                    Applicant firstApplicantInfo = patent.getFirstApplicantInfo();
                    if (firstApplicantInfo != null) {
                        ObjectNode objectNode = objectMapper.valueToTree(firstApplicantInfo);
                        ((ObjectNode) jsonNode).put("firstApplicantInfo", objectNode);
                    }

                    List<Applicant> assigneeInfo = patent.getAssigneeInfo();
                    if (assigneeInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(assigneeInfo);
                        ((ObjectNode) jsonNode).put("assigneeInfo", arrayNode);
                    }

                    List<Applicant> currentApplicantInfo = patent.getCurrentApplicantInfo();
                    if (currentApplicantInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(currentApplicantInfo);
                        ((ObjectNode) jsonNode).put("currentApplicantInfo", arrayNode);
                    }

                    List<Applicant> rightHolderInfo = patent.getRightHolderInfo();
                    if (rightHolderInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(rightHolderInfo);
                        ((ObjectNode) jsonNode).put("rightHolderInfo", arrayNode);
                    }

                    List<Applicant> currentRightHolderInfo = patent.getCurrentRightHolderInfo();
                    if (currentRightHolderInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(currentRightHolderInfo);
                        ((ObjectNode) jsonNode).put("currentRightHolderInfo", arrayNode);
                    } else {
                        if (patent.getDocumentId().matches("^jp.+$")) {
                            String patentType = patent.getPatentType();
                            if (patentType.matches("^(B|Y)$")) {
                                List<Applicant> applicantList = patent.getApplicantInfo();
                                if (applicantList != null) {
                                    ArrayNode arrayNode = objectMapper.valueToTree(applicantList);
                                    ((ObjectNode) jsonNode).put("rightHolderInfo", arrayNode);
                                    ((ObjectNode) jsonNode).put("currentRightHolderInfo", arrayNode);
                                }
                            }
                        }
                    }

                    Applicant firstRightHolderInfo = patent.getFirstRightHolderInfo();
                    if (firstRightHolderInfo != null) {
                        ObjectNode objectNode = objectMapper.valueToTree(firstRightHolderInfo);
                        ((ObjectNode) jsonNode).put("firstRightHolderInfo", objectNode);
                    }

                    List<Applicant> inventorInfo = patent.getInventorInfo();
                    if (inventorInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(inventorInfo);
                        ((ObjectNode) jsonNode).put("inventorInfo", arrayNode);
                    }

                    List<Applicant> agentInfo = patent.getAgentInfo();
                    if (agentInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(agentInfo);
                        ((ObjectNode) jsonNode).put("agentInfo", arrayNode);
                    }

                    List<Applicant> upInventorInfo = patent.getUpInventorInfo();
                    if (upInventorInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(upInventorInfo);
                        ((ObjectNode) jsonNode).put("upInventorInfo", arrayNode);
                    }

                    List<Applicant> upRightHolderInfo = patent.getUpRightHolderInfo();
                    if (upRightHolderInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(upRightHolderInfo);
                        ((ObjectNode) jsonNode).put("upRightHolderInfo", arrayNode);
                    }

                    List<Applicant> upAgentInfo = patent.getUpAgentInfo();
                    if (upAgentInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(upAgentInfo);
                        ((ObjectNode) jsonNode).put("upAgentInfo", arrayNode);
                    }

                    List<Applicant> examinerInfo = patent.getExaminerInfo();
                    if (examinerInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(examinerInfo);
                        ((ObjectNode) jsonNode).put("examinerInfo", arrayNode);
                    }

                    String docdbFamilyId = patent.getDocdbFamilyId();
                    if (docdbFamilyId != null) {
                        ((ObjectNode) jsonNode).put("docdbFamilyId", docdbFamilyId);
                    }

                    String extendedFamilyId = patent.getExtendedFamilyId();
                    if (extendedFamilyId != null) {
                        ((ObjectNode) jsonNode).put("extendedFamilyId", extendedFamilyId);
                    }

                    if (patent.getDocumentId().matches("^(jp).+$")) {
                        List<String> summary = patent.getSummary();
                        if (summary != null) {
                            ArrayNode arrayNode = objectMapper.valueToTree(summary);
                            ((ObjectNode) jsonNode).put("summary", arrayNode);
                        }

                        List<String> summaryJP = patent.getSummaryJP();
                        if (summaryJP != null) {
                            ArrayNode arrayNode = objectMapper.valueToTree(summaryJP);
                            ((ObjectNode) jsonNode).put("summaryJP", arrayNode);
                        }
                    }

                    List<Applicant> representApplicantInfo = patent.getRepresentApplicantInfo();
                    if (representApplicantInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
                        ((ObjectNode) jsonNode).put("representApplicantInfo", arrayNode);
                    } else {
                        if (patent.getDocumentId().matches("^(kr|jp).+$")) {
                            representApplicantInfo = representApn.representApplicantInfo(conn, patent);
                            if (representApplicantInfo != null) {
                                ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
                                ((ObjectNode) jsonNode).put("representApplicantInfo", arrayNode);
                            }
                        }
                    }

                    // Claim 수 처리
                    if (patent.getClaimCount() != null) {
                        String cc = patent.getClaimCount();
                        if (!cc.matches("^[0-9]+$")) {
                            cc = patent.getClaimCount().replaceAll("[^0-9]", "");
//                            System.out.println(patent.getClaimCount() + " : " +cc);
                            ((ObjectNode) jsonNode).put("claimCount", cc);
                        }
                    }


                    // 상세설명 크기 처리
                    int dsc_size = 0;
                    boolean largeFlag = false;
                    if (jsonNode != null) {
                        JsonNode dsc = jsonNode.get("description");
//                        System.out.println(dsc.toString().getBytes().length);
                        if (dsc != null) {
                            dsc_size = dsc.toString().getBytes().length;

                            // 1.25 MB
                            if (dsc_size > 1310720) {
//                                if (dsc_size / (1024 * 1024) > 0) {
                                largeFlag = true;

                                if (patent.getDocumentId().matches("^(kr|jp).+$")) {
                                    ((ObjectNode) jsonNode).put("descriptionLarge", dsc);
                                    ((ObjectNode) jsonNode).remove("description");
                                }

                                if (patent.getDocumentId().matches("^(jp).+$")) {
                                    ((ObjectNode) jsonNode).put("descriptionJPLarge", jsonNode.get("descriptionJP"));
                                    ((ObjectNode) jsonNode).remove("descriptionJP");
                                }
                            }
                        }
                    }

                    ((ObjectNode) jsonNode).put("descriptionByteSize", dsc_size);
                    ((ObjectNode) jsonNode).put("descriptionLargeFlag", largeFlag);


                    patent.setUpdateLevel(9);
                    if (!patent.getESExist(esConfig)) {
                        ((ObjectNode) jsonNode).put("firstInsertDate", dtf.format(localDate));
                        int level = 2;
//                        // 번역 및 추가 프로세스 필요
//                        if (org.matches("^(epo|uspto|jpo)$"))
//                            level = 2;

                        patent.setUpdateLevel(level);
                    } else if (patent.getDocumentId().matches("^(kr|jp).+$"))
                        ((ObjectNode) jsonNode).remove("legalStatus");


                    patents.add(patent);

                    if (patents.size() > 10) {
                        insertPatent(dbHelper, patents, org);
                        patents.clear();
                    }

//                    ((ObjectNode) jsonNode).put("firstInsertDate", "20200101");

                    //                    patent.print();
                    json = objectMapper.writeValueAsString(jsonNode);
//                    System.out.println(patent.getDocumentId());
//                    System.out.println(patent.getDocumentId() + " : " + json);

                }

//                System.out.println(f.getPath() + " : " + json.getBytes().length);
                esConfig.bulkProcessor.add((new UpdateRequest(index, type, f.getName().replaceFirst("\\.json", ""))
                        .doc(json.getBytes()))
                        .upsert(json.getBytes()));


            } catch (IOException e) {
                System.out.println(f.getAbsolutePath());
                e.printStackTrace();
            }
        }

        if (!patents.isEmpty())
            insertPatent(dbHelper, patents, org);

        try {

            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("inserted Count: " + count);
        System.out.println("BulkInsert Finished~!");
    }

    private static List<Patent> getPairPatent(ESConfig esConfig, Patent patent) {
        System.out.println(patent.getPatentId());

        String patentId = patent.getPatentId();
        if (patentId == null)
            return null;

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.idsQuery().addIds(patent.getDocumentId()))
                .filter(QueryBuilders.matchQuery("patentId", patentId));

        SearchResponse scrollResp = esConfig.client.prepareSearch(patent.getOrg())
                .setTypes("patent")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
//                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        ObjectMapper objectMapper = new ObjectMapper();
        List<Patent> patents = new ArrayList<>();

        try {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Patent pairPatent = objectMapper.readValue(hit.getSourceAsString(), Patent.class);

                patents.add(pairPatent);
                System.out.println(hit.getSourceAsString());

            }

            if (!patents.isEmpty())
                return patents;


        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    private static void insertPatent(DBHelper dbHelper, ArrayList<Patent> patents, String org) {
        if (patents == null || patents.isEmpty())
            return;

        if (org.matches("sipo_person"))
            return;

        InsertPatentToDB.insertESQueue(dbHelper.getConn_aurora2(), patents);

        if (org.matches("(docdb)"))
            return;

        // 서지사항 db insert
        InsertPatentToDB.insertBiblio(dbHelper.getConn_aurora2(), patents);

        // 인용 db insert
//        InsertPatentToDB.insertCitation(dbHelper.getConn_aurora2(), patents);

//         법적상태 db upsert
        InsertPatentToDB.upsertLegalStatus(dbHelper, patents);

        // 현재권리자
        InsertPatentToDB.updateRightholder(dbHelper, patents);

        // US relatedDoc insert
        if (org.matches("uspto"))
            InsertPatentToDB.insertUSRelatedDoc(dbHelper, patents);

        if (org.matches("kipo")) {
            InsertPatentToDB.insertKRDocid(dbHelper.getConn_aurora(), patents);
            InsertPatentToDB.insertKRDocid(dbHelper.getConn_aurora2(), patents);
        }

//        System.out.println(documentId.asText());


    }

    private static String getORG(String documentId) {
        if (documentId.matches("^kr.+$"))
            return "kipo";
        else if (documentId.matches("^usps.+$"))
            return "uspto_past";
        else if (documentId.matches("^jpps.+$"))
            return "jpo_past";
        else if (documentId.matches("^us.+$"))
            return "uspto";
        else if (documentId.matches("^jp.+$"))
            return "jpo";
        else if (documentId.matches("^paj.+$"))
            return "paj";
        else if (documentId.matches("^wo.+$"))
            return "pct";
        else if (documentId.matches("^ep.+$"))
            return "epo";
        else if (documentId.matches("^cn.+$"))
            return "sipo";
        else if (documentId.matches("^de.+$"))
            return "dpma";
        else if (documentId.matches("^au.+$"))
            return "aupo";
        else if (documentId.matches("^ca.+$"))
            return "capo";
        else if (documentId.matches("^fr.+$"))
            return "frpo";
        else if (documentId.matches("^gb.+$"))
            return "gbpo";
        else if (documentId.matches("^in.+$"))
            return "inpo";
        else if (documentId.matches("^it.+$"))
            return "itpo";
        else if (documentId.matches("^ru.+$"))
            return "rupo";
        else if (documentId.matches("^tw.+$"))
            return "tipo";
        else if (documentId.matches("^[A-Z]{2}.+$"))
            return "docdb";

        return null;
    }

    public static void insertPatentInfo(JsonNode jsonNode) {
        JsonNode patentNumber = jsonNode.get("registerNumber");
        if (patentNumber == null)
            patentNumber = jsonNode.get("publicationNumber");

        JsonNode patentDate = jsonNode.get("registerDate");
        if (patentDate == null)
            patentDate = jsonNode.get("publicationDate");

        JsonNode documentId = jsonNode.get("documentId");
        if (documentId != null)
            ((ObjectNode) jsonNode).put("keywertId", PatentID.getKeywertId(documentId.asText()));

        JsonNode applicationNumber = jsonNode.get("applicationNumber");

        String patentId = "";
        if (documentId != null && applicationNumber != null)
            patentId = PatentID.getPatentId(documentId.asText(), applicationNumber.asText());

        if (!patentId.isEmpty())
            ((ObjectNode) jsonNode).put("patentId", patentId);

        if (patentNumber != null)
            ((ObjectNode) jsonNode).put("patentNumber", patentNumber.asText());

        if (patentDate != null)
            ((ObjectNode) jsonNode).put("patentDate", patentDate.asText());


//        System.out.println(patentId + " : " + patentNumber.asText() + " : " + patentDate.asText());

        JsonNode mainIpc = jsonNode.get("mainIpc");
        if (mainIpc != null) {
            WipoIpc wipoIpc = WipoIpc.findByWipoIpc(mainIpc.asText());

            if (WipoIpc.EMPTY.equals(wipoIpc))
                wipoIpc = WipoIpc.findByWipoIpc(mainIpc.asText().substring(0, 4));

            if (!WipoIpc.EMPTY.equals(wipoIpc)) {
                ((ObjectNode) jsonNode).put("wipo_5_category", wipoIpc.getSector_code());
                ((ObjectNode) jsonNode).put("wipo_35_category", wipoIpc.name());
            }
        }
    }

    public static ArrayList getFilepathList(String filepath) {
        ArrayList arrayList = new ArrayList();

        File dir = new File(filepath);

        if (dir.exists() == false) {
            System.out.println("Not exist File Path.");
            return null;
        }

        visitDir(arrayList, dir);

        return arrayList;
    }

    public static void visitDir(ArrayList arrayList, File file) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles)
                visitDir(arrayList, f);

        } else {
//            if( file.getName().matches("^kr(\\d){11}(\\w)(\\d)?\\.json$"))
//                arrayList.add(file);
            if (file.getName().matches(".+\\.json$"))
                arrayList.add(file);
//             jpo
//            if (file.getName().matches(".+[^5-9]\\.json$"))
//                arrayList.add(file);
        }
    }
}
