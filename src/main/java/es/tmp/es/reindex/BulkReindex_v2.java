package es.tmp.es.reindex;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class BulkReindex_v2 {
    public static void main(String[] args) throws IOException {
        Logger logger = Logger.getLogger("Log");
        FileHandler fileHandler;


        if (args.length == 0) {
            System.out.println("Usage : java -jar reindex.jar [indexName] [startDate] [endDate]");
            return;
        }

        ESConfig es_src = new ESConfig("es188");
//        ESConfig es_dest = new ESConfig("es205");
        ESConfig es_dest = new ESConfig("es28");

//        ESConfig es_dest = new ESConfig("es18");


        String index = args[0];
//        String type = "patent";
        String type = "patent";

        String startDate = args[1];
        String endDate = args[2];


        System.out.println(index + " : " + startDate + " ~ " + endDate);

//        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .must(QueryBuilders.prefixQuery("documentId", "usps0re"));

        String[] docIds = {"us09752985b2"};


//        String [] dates = {"19860225","19860304","19840110","19840103","19840117"};
        String[] dates = {"19831206","19840306","19841113","19841106","19841120","19841211","19841225","19841204","19860225"};
        QueryBuilder qb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.termsQuery("patentDate", dates))
//                .filter(QueryBuilders.rangeQuery("patentDate").from(start).to(end))
//                .filter(QueryBuilders.idsQuery().addIds(docIds))
//                .mustNot(QueryBuilders.existsQuery("currentRightHolderInfo.seName"))
//                .mustNot(QueryBuilders.existsQuery("patentDate"))
//                .must(QueryBuilders.matchAllQuery())
                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
//                .filter(QueryBuilders.rangeQuery("firstInsertDate").from(startDate).to(endDate))
//                .filter(
//                        QueryBuilders.boolQuery()
//                                .should(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("lastUpdateDate").from(startDate).to(endDate))
//                )
//                .should(QueryBuilders.rangeQuery("lastUpdateDate").from(startDate).to(endDate))
//                .should(QueryBuilders.rangeQuery("registerDate").from(startDate).to(endDate))
//                .should(QueryBuilders.rangeQuery("openDate").from(startDate).to(endDate))
                ;

//        QueryBuilder qb1 = QueryBuilders.boolQuery()
//                .mustNot(QueryBuilders.existsQuery("announcementDate"))
//                .mustNot(QueryBuilders.existsQuery("publicationDate"))
//                .mustNot(QueryBuilders.existsQuery("registerDate"))
//                .mustNot(QueryBuilders.existsQuery("openDate"));

//            jpps1985
        String pre = "jpps1989";

//        QueryBuilder qb1 = QueryBuilders.boolQuery()
//                        .should(QueryBuilders.prefixQuery("documentId",pre));

//        String[] dates = {"20180514"};
//        QueryBuilder qb = QueryBuilders.termsQuery("insertDate", dates);

        BoolQueryBuilder mQub = QueryBuilders.boolQuery().filter(QueryBuilders.matchAllQuery());

        TermQueryBuilder queryBuilders = QueryBuilders.termQuery("claims.claimNumber", "500");

        String[] fields = {"documentId", "patentNumber"};//"description"};

        ArrayList<String> documentIds = new ArrayList<>();

        SearchResponse scrollResp = es_dest.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(3000)
                .execute()
                .actionGet();

        System.out.println("Destination Hits : " + scrollResp.getHits().getTotalHits());

        Map<String, String> map = new HashMap<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
//                Object patentNumber = hit.getSource().get("patentNumber");
//                if (patentNumber == null) {
//                    System.out.println(hit.getId() + " patentNumber missing");
//                    continue;
//                }
//                map.put(patentNumber.toString(), hit.getId());

                documentIds.add(hit.getId());
            }
            scrollResp = es_dest.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .must(QueryBuilders.matchAllQuery())
//                .filter(QueryBuilders.termsQuery("patentDate", dates))
//                .must(QueryBuilders.idsQuery().addIds(docIds))
//                .mustNot(QueryBuilders.existsQuery("patentDate"))
//patentDate                .filter(
//                        QueryBuilders.boolQuery()
//                                .should(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("lastUpdateDate").from(startDate).to(endDate))
//                )
//                .filter(QueryBuilders.rangeQuery("firstInsertDate").from(startDate).to(endDate))
//                .mustNot(QueryBuilders.existsQuery("patentDate"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
//                .should(QueryBuilders.rangeQuery("lastUpdateDate").from(startDate).to(endDate))
//                .should(QueryBuilders.rangeQuery("registerDate").from(startDate).to(endDate))
//                .should(QueryBuilders.rangeQuery("openDate").from(startDate).to(endDate))
//                .mustNot(QueryBuilders.termsQuery("patentNumber", map.keySet()))
                .mustNot(QueryBuilders.idsQuery().addIds(documentIds))
                ;

//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery()
//                .mustNot(QueryBuilders.existsQuery("announcementDate"))
//                .mustNot(QueryBuilders.existsQuery("publicationDate"))
//                .mustNot(QueryBuilders.existsQuery("registerDate"))
//                .mustNot(QueryBuilders.existsQuery("openDate"))
//                .mustNot(QueryBuilders.idsQuery().addIds(documentIds));

        QueryBuilder qb2 = QueryBuilders.boolQuery()
                .must(QueryBuilders.prefixQuery("documentId", "usps0RE"))
//                .must(QueryBuilders.prefixQuery("documentTypeUS","I"))
//                .should(QueryBuilders.prefixQuery("documentId",pre))
                .mustNot(QueryBuilders.idsQuery().addIds(documentIds));

//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery()
//                .should(QueryBuilders.termsQuery("insertDate", dates))
//                .mustNot(QueryBuilders.idsQuery().addIds(documentIds));


//        String[] includes = {};
//        String[] excludes = {"drawings"};

        scrollResp = es_src.client.prepareSearch(index)
//                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(1)
//                .setFetchSource(includes,excludes)
                .execute()
                .actionGet();

        System.out.println("Start Reindex Count : " + scrollResp.getHits().getTotalHits());
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                if (++count % 1000 == 0) {
                    System.out.println(count);
//                    try {
//                        Thread.sleep(15000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }
//                System.out.println(hit.getId());

                JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                BulkMain.insertPatentInfo(jsonNode);

//                ((ObjectNode) jsonNode).put("documentId", hit.getId().toLowerCase());

//                ((ObjectNode) jsonNode).put(key, map.get(key));

                String json = objectMapper.writeValueAsString(jsonNode);
//                System.out.println(documentIds + " : " + json);
                es_dest.bulkProcessor.add(new IndexRequest(index, hit.getType(), hit.getId()).source(json));


//                System.out.println(hit.getId() + " : " +json);

//                es_dest.bulkProcessor.add(new IndexRequest(index,type,hit.getId()).source(json));

            }
            scrollResp = es_src.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        System.out.println("Finish Count : " + count);

        try {
            es_dest.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
