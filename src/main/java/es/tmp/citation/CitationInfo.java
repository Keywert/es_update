package es.tmp.citation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.v1.FtermDeseralizer;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CitationInfo {
    // APP : 출원서 인용
    // EXA : 심사관 인용
    // SEA : 조사보고서 인용
    // OPP : 이의신청서 인용
    // OTH : 기타 문서 인용

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> category;

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> citationSource;

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> citationStep;

    public String citationId;
    public String lastUpdateDate;

    public CitationVO from;
    public CitationVO to;
}
