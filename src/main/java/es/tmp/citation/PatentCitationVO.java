package es.tmp.citation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentCitationVO {
    public List<CitationVO> citationB1Info;
    public List<CitationVO> citationF1Info;
    public List<CitationVO> citationNPInfo;

    public Integer citationB1Count;
    public Integer citationF1Count;
    public Integer citationNPCount;

    public String getCitationUpdateDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        return dtf.format(localDate);
    }

    public String citationUpdateDate;
}
