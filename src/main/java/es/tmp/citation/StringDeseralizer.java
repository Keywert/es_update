package es.tmp.citation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StringDeseralizer extends JsonDeserializer {
    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode node = p.readValueAsTree();
        List<String> list = new ArrayList<>();
        if (node instanceof ArrayNode) {
            String[] objects = new ObjectMapper().readValue(node.toString(), String[].class);
            list.addAll(Arrays.asList(objects));
        } else if (node instanceof JsonNode) {
            String fterm = new ObjectMapper().readValue(node.toString(), String.class);
            list.add(fterm);
        }

        if (!list.isEmpty())
            return list.get(0);

        return null;
    }
}
