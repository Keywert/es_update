package es.tmp.citation;

import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.tmp.ESHealthCheck;
import es.tmp.update.ESDataCheck;
import es.tmp.update.ESIndexing;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ESReindexCitationMain {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));

        if (esConfigs.isEmpty())
            return;

        int size = esConfigs.size();

        ESConfig esConfigSrc = new ESConfig("es29");
        DataFarmDB dataFarmDB = new DataFarmDB();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String updateDate = dtf.format(localDate);

        int workSize = 1000;
        int queue_size = 30000;

        for (int i = 0; i < workSize; i++) {
            long start = System.currentTimeMillis();
            int result = esReindex(esConfigSrc, esConfigs, dataFarmDB, queue_size, updateDate);
            if (result == -1)
                break;
            else if (result == 0)
                System.out.println("Citation Reindexing is Finished~!");


            long end = System.currentTimeMillis();
            System.out.println("실행 시간 : " + (end - start) / 1000.0 + " 평균 시간 : " + ((end - start) / 1000.0) / queue_size);
            System.out.println();
        }

        try {
            for (ESConfig esConfig : esConfigs) {
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static int esReindex(ESConfig esConfigSrc, List<ESConfig> esConfigs, DataFarmDB dataFarmDB, int queue_size, String updateDate) {
        ESHealthCheck esHealthCheck = new ESHealthCheck();
        for (ESConfig esConfig : esConfigs)
            if (!esHealthCheck.healthCheck(esConfig)) {
                System.out.println(esConfig.getSettings().get("cluster.name"));
                System.out.println(" Real Cluster Health Status is not normal.");
                return -1;
            }

        if (!esHealthCheck.healthCheck2(esConfigSrc)) {
            System.out.println(" DataFarm ES Health Status is not normal.");
            return -1;
        }

        // 2. Queue 확인
        List<String> docIds = dataFarmDB.selectCitationQueue(queue_size);
        if (docIds == null)
            return -1;

        ESIndexing esIndexing = new ESIndexing();

        String index = "citation_v2";

        int count = esIndexing.citationIndexing(esConfigSrc, esConfigs, index, docIds, updateDate);
//        if (count != docIds.size()) {
//            System.out.println(count + " - " + docIds.size() + " - " + index + " Update Fail~!");
//            return -1;
//        }

        System.out.println(count + " - " + docIds.size() + " - " + index + " indexing Success ~!");

        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Update 확인
        ESDataCheck esDataCheck = new ESDataCheck();
        if (!esDataCheck.citationUpdateDataCheck(esConfigs, docIds, count, updateDate)) {
            System.out.println("ES Data Update Fail~! - retry ");
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (!esDataCheck.citationUpdateDataCheck(esConfigs, docIds, count, updateDate)) {
                System.out.println("ES Data Update Fail~! ");
                return -1;
            }
        }

        dataFarmDB.citationUpdateDB(docIds);
        System.out.println("ES Update Success");

        return 0;
    }

}
