package es.tmp.citation;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.v1.Applicant;
import es.model.v1.PatentVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.apache.commons.lang3.StringEscapeUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CitationBulkUpdate {
    public static void main(String[] args) {
        ESConfig esCitation = new ESConfig("es225");

        // kipo, epo, dpma, uspto, docdb, uspto_past
//        String index = "kipo";
//        ESConfig esConfig = new ESConfig("es188");

        // aupo, capo, frpo, gbpo, inpo, itpo, rupo, tipo, sipo, pct,
//        String index = "sipo";
//        ESConfig esConfig = new ESConfig("es189");

        // jpo
        String index = "jpo";
        ESConfig esConfig = new ESConfig("es18");

        // uspto
//        String index = "uspto";
//        ESConfig esConfig = new ESConfig("es167");

        // pct
//        String index = "pct";
//        ESConfig esConfig = new ESConfig("es206");

        DataFarmDB dataFarmDB = new DataFarmDB();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();


        Set<String> patentIds = getUpdatePatentIds(conn,index);
        if (patentIds == null)
            return;

        System.out.println(index + " - PatentId Count : " + patentIds.size());

        Set<String> set = new HashSet<>();
        int count = 0;
        for (String patentId : patentIds) {
            set.add(patentId);
            if (++count%1000 == 0)
                System.out.println(count);

            if (set.size() > 100) {
                Map<String, Set<String>> patentIdMap = getESPatent(esConfig, index, set);
                if (patentIdMap != null)
                    esCitationUpdate(esCitation, index, esConfig, patentIdMap, dataFarmDB);

                updateDB(conn,index,set);
                set.clear();
            }
        }

        if (!set.isEmpty()) {
            Map<String, Set<String>> patentIdMap = getESPatent(esConfig, index, set);
            if (patentIdMap != null)
                esCitationUpdate(esCitation, index, esConfig, patentIdMap, dataFarmDB);


            updateDB(conn,index,set);
        }

        try {
            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String index, Set<String> set) {
        String query = "UPDATE citation.updated_patent SET status = 99 " +
                " WHERE  patentId = ? and es_index = ? ";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for ( String patentId : set) {
                preparedStatement.setString(1, patentId);
                preparedStatement.setString(2, index);

                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static Set<String> getUpdatePatentIds(Connection conn, String index) {
//        String query = "select patentId from citation.updated_patent where patentId regexp '^PCT(K|Q|R|T|V|X|Y|Z).+$' and es_index = '"+index+"' and status = 0 ";
//        String query = "select patentId from citation.updated_patent where patentId like 'KR10202%' and es_index = '"+index+"' and status = 1 order by status";
        String query = "select patentId from citation.updated_patent where es_index = '"+index+"' and status = 1 order by status";

        Set<String> patentIds = new HashSet<>();

        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, Applicant> map = new HashMap<>();

            while (rs.next()) {
                String patentId = rs.getString("patentId");
                if (patentId == null || "".equals(patentId))
                    continue;

                patentIds.add(patentId);
            }

            if (!patentIds.isEmpty())
                return patentIds;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }


        return null;
    }

    private static Set<String> getCitationPatentIds(ESConfig esCitation, String prefix) {
        String citationIndex = "citation_v2";
        String type = "patent";

        ObjectMapper objectMapper = new ObjectMapper();
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .should(QueryBuilders.prefixQuery("to.patentId", prefix))
                .should(QueryBuilders.prefixQuery("from.patentId", prefix));

        SearchResponse scrollResp = esCitation.client.prepareSearch(citationIndex)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
//                .setFetchSource(fields, null)
                .setSize(200)
                .execute()
                .actionGet();

        try {

            Set<String> patentIds = new HashSet<>();
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    CitationInfo citationInfo = objectMapper.readValue(hit.getSourceAsString(), CitationInfo.class);
                    if (citationInfo == null)
                        continue;

                    CitationVO from = citationInfo.getFrom();
                    if (from != null) {
                        String patentId = from.getPatentId();
                        if (patentId != null && !"".equals(patentId) && patentId.matches("^" + prefix + ".*")) {
                            patentIds.add(patentId);
                        }
                    }

                    CitationVO to = citationInfo.getTo();
                    if (from != null) {
                        String patentId = to.getPatentId();
                        if (patentId != null && !"".equals(patentId) && patentId.matches("^" + prefix + ".*")) {
                            patentIds.add(patentId);
                        }
                    }
                }
                scrollResp = esCitation.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!patentIds.isEmpty())
                return patentIds;

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Set<String> getPatentIds(ESConfig esConfig, String index, int size) {
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("citationDoc"))
                .mustNot(
                        QueryBuilders.boolQuery()
                                .should(QueryBuilders.existsQuery("citationNPInfo"))
                                .should(QueryBuilders.existsQuery("citationB1Info"))
                                .should(QueryBuilders.existsQuery("citationF1Info"))
                );

        String[] fields = {"patentId"};
        Set<String> set = new HashSet<>();

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        Map<String, Set<String>> patentIdMap = new HashMap<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object patentId = hit.getSource().get("patentId");
                if (patentId == null)
                    continue;

                set.add(patentId.toString());

                if (set.size() > size)
                    return set;

            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!set.isEmpty())
            return set;

        return null;
    }

    private static void esCitationUpdate(ESConfig esCitation, String index, ESConfig esConfig, Map<String, Set<String>> patentIdMap, DataFarmDB dataFarmDB) {
        if (patentIdMap == null || patentIdMap.isEmpty())
            return;
        try {
            Map<String, List<CitationVO>> citationB1Map = getESCitation(esCitation, "B1", patentIdMap.keySet());
            Map<String, List<CitationVO>> citationF1Map = getESCitation(esCitation, "F1", patentIdMap.keySet());

            Map<String, PatentCitationVO> patentVOMap = new HashMap<>();
            if (citationB1Map != null) {
                for (String key : citationB1Map.keySet()) {
                    List<CitationVO> citationVOS = citationB1Map.get(key);
                    if (citationVOS == null)
                        continue;

                    List<CitationVO> npCitations = new ArrayList<>();
                    List<CitationVO> citations = new ArrayList<>();

                    for (CitationVO c : citationVOS) {
                        if (c.isEmpty())
                            continue;

                        String text = c.text;
                        if (text != null && !"".equals(text))
                            npCitations.add(c);
                        else
                            citations.add(c);
                    }

                    PatentCitationVO patentVO = patentVOMap.get(key);
                    if (patentVO == null)
                        patentVO = new PatentCitationVO();

                    if (!npCitations.isEmpty()) {
                        List<CitationVO> rCitations = removeDuplicateNC(npCitations);

                        if (rCitations != null) {
                            patentVO.setCitationNPInfo(rCitations);
                            patentVO.setCitationNPCount(rCitations.size());
                        }
                    }

                    if (!citations.isEmpty()) {
                        patentVO.setCitationB1Info(citations);
                        patentVO.setCitationB1Count(citations.size());
                    }

                    patentVOMap.put(key, patentVO);
                }
            }

            if (citationF1Map != null) {
                for (String key : citationF1Map.keySet()) {
                    List<CitationVO> citationVOS = citationF1Map.get(key);
                    if (citationVOS == null)
                        continue;

                    PatentCitationVO patentVO = patentVOMap.get(key);
                    if (patentVO == null)
                        patentVO = new PatentCitationVO();

                    patentVO.setCitationF1Info(citationVOS);
                    patentVO.setCitationF1Count(citationVOS.size());

                    patentVOMap.put(key, patentVO);
                }
            }

            List<QueueVO> queueVOList = new ArrayList<>();
            for (String key : patentIdMap.keySet()) {
                PatentCitationVO patentVO = patentVOMap.get(key);
                if (patentVO == null)
                    continue;

                if (patentVO.getCitationB1Info() == null && patentVO.getCitationF1Info() == null && patentVO.getCitationNPInfo() == null)
                    continue;

                Set<String> docIds = patentIdMap.get(key);
                if (docIds == null)
                    continue;

                String json = new ObjectMapper().writeValueAsString(patentVO);

                for (String docId : docIds) {
//                    System.out.println(docId + " : " + json);
                    esConfig.bulkProcessor.add((new UpdateRequest(index, "patent", docId)
                            .doc(json.getBytes()))
                            .upsert(json.getBytes()));

                    QueueVO queueVO = new QueueVO();
                    queueVO.setEsindex(index);
                    queueVO.setDocumentId(docId);
                    queueVO.setLevel(7);

                    queueVOList.add(queueVO);
                    if (queueVOList.size() > 10) {
                        dataFarmDB.insertESQueue(queueVOList);
                        queueVOList.clear();
                    }
                }
            }

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    private static List<CitationVO> removeDuplicateNC(List<CitationVO> npCitations) {
        Map<String, CitationVO> ncMap = new HashMap<>();


        for (CitationVO citationVO : npCitations) {
            String text = citationVO.getText();

            text = text
                    .replaceAll(System.getProperty("line.separator"), " ")
                    .replaceAll("<style[^>]*>(.*?)", " ")
                    .replaceAll("</p>", " </p>")
                    .replaceAll("<(/)?(claim|li|LI|T|t|pre|pre)[^>|]*>", " ")
                    .replaceAll("<(/)?([A-z]*)(-[A-z]*)?(-[A-z]*)?(-[A-z]*)?(\\s[A-z]*(\\s)?(\\s)?=[^>]*)?(\\s)*(/)?>", " ")
                    .replaceAll("<(\\?)?([A-z]*)[^>]*\\?>", " ")
                    .replaceAll("<(/)?exch:p[^>]*>", " ")
                    .replaceAll("\\s+", " ")
                    .trim();

            text = StringEscapeUtils.unescapeHtml3(text);

            if (!text.isEmpty()) {
                citationVO.setText(text);
                ncMap.put(text, citationVO);
            }
        }

        if (!ncMap.isEmpty())
            return new ArrayList<>(ncMap.values());

        return null;
    }

    private static Map<String, Set<String>> getESPatent(ESConfig esConfig, String index, Set<String> set) {
        if (set == null || set.isEmpty())
            return null;

        BoolQueryBuilder bqb = QueryBuilders.boolQuery().filter(QueryBuilders.termsQuery("patentId", set));
        String[] fields = {"patentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
//                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        Map<String, Set<String>> patentIdMap = new HashMap<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object patentId = hit.getSource().get("patentId");
                if (patentId == null)
                    continue;

                Set<String> docIds = patentIdMap.get(patentId.toString());
                if (docIds == null)
                    docIds = new HashSet<>();

                docIds.add(hit.getId());
                patentIdMap.put(patentId.toString(), docIds);
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!patentIdMap.isEmpty())
            return patentIdMap;


        return null;
    }

    private static void esCitationUpdate2(ESConfig esCitation, ESConfig esConfig, Set<String> set) {
        Map<String, List<CitationVO>> citationB1Map = getESCitation(esCitation, "B1", set);
        Map<String, List<CitationVO>> citationF1Map = getESCitation(esCitation, "F1", set);

        Map<String, PatentCitationVO> patentVOMap = new HashMap<>();

        if (citationB1Map != null) {
            for (String key : citationB1Map.keySet()) {
                List<CitationVO> citationVOS = citationB1Map.get(key);
                if (citationVOS == null)
                    continue;

                PatentCitationVO patentVO = patentVOMap.get(key);
                if (patentVO == null)
                    patentVO = new PatentCitationVO();

                patentVO.setCitationB1Info(citationVOS);
                patentVO.setCitationB1Count(citationVOS.size());

                patentVOMap.put(key, patentVO);
            }
        }

        if (citationF1Map != null) {
            for (String key : citationF1Map.keySet()) {
                List<CitationVO> citationVOS = citationF1Map.get(key);
                if (citationVOS == null)
                    continue;

                PatentCitationVO patentVO = patentVOMap.get(key);
                if (patentVO == null)
                    patentVO = new PatentCitationVO();

                patentVO.setCitationF1Info(citationVOS);
                patentVO.setCitationF1Count(citationVOS.size());

                patentVOMap.put(key, patentVO);
            }
        }

        if (patentVOMap.isEmpty())
            return;

        for (String key : patentVOMap.keySet()) {
            PatentCitationVO patentVO = patentVOMap.get(key);
            if (patentVO == null)
                continue;

            List<CitationVO> citationB1Info = patentVO.getCitationB1Info();
//            if (citationB1Info != null)

            try {
                System.out.println(key + " : " + new ObjectMapper().writeValueAsString(patentVO));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }


        }
    }

    private static Map<String, List<CitationVO>> getESCitation(ESConfig esCitation, String searchType, Set<String> set) {
        if (set.isEmpty())
            return null;

        String citationIndex = "citation_v2";
        String type = "patent";

        String searchField = "from.patentId";
        if (searchType.equals("F1"))
            searchField = "to.patentId";

        ObjectMapper objectMapper = new ObjectMapper();
        BoolQueryBuilder bqb = QueryBuilders.boolQuery().filter(QueryBuilders.termsQuery(searchField, set));

        SearchResponse scrollResp = esCitation.client.prepareSearch(citationIndex)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
//                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        try {
            Map<String, List<CitationVO>> citationMap = new HashMap<>();

            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    CitationInfo citationInfo = objectMapper.readValue(hit.getSourceAsString(), CitationInfo.class);
                    if (citationInfo == null)
                        continue;


                    CitationVO citation = null;

                    CitationVO from = citationInfo.getFrom();
                    CitationVO to = citationInfo.getTo();
                    String patentId = "";
                    if (searchType.equals("B1")) {
                        citation = to;
                        patentId = from.getPatentId();
                    } else if (searchType.equals("F1")) {
                        citation = from;
                        patentId = to.getPatentId();
                    }

                    if (patentId == null || "".equals(patentId))
                        continue;

                    List<String> category = citationInfo.getCategory();
                    if (category != null)
                        citation.setCategory(category);

                    List<String> citationSource = citationInfo.getCitationSource();
                    if (citationSource != null)
                        citation.setCitationSource(citationSource);

                    List<String> citationStep = citationInfo.getCitationStep();
                    if (citationStep != null)
                        citation.setCitationStep(citationStep);

                    List<CitationVO> list = citationMap.get(patentId);
                    if (list == null)
                        list = new ArrayList<>();

                    list.add(citation);
                    citationMap.put(patentId, list);

                }
                scrollResp = esCitation.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!citationMap.isEmpty())
                return citationMap;

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
