package es.tmp.citation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.v1.FtermDeseralizer;
import lombok.Data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CitationVO {
    public String applicationNumber;

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> citationNumber;

    public String country;

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> documentId;
    public String kind;

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> patentDate;

//    public List<String> getPatentNumbers() {
//        if (this.citationNumber != null){
//            if (this.patentNumbers == null) {
//                List<String> list = new ArrayList<>();
//
//            }
//
//        }
//
//        return patentNumbers;
//    }

    public List<String> patentNumbers;

    public String patentId;

    // to
    public String text;

    public String getReprDocumentId() {
        if (this.reprDocumentId == null && this.documentId != null) {
            List<String> docIds = this.getDocumentId();
            if (docIds.size() == 1)
                return docIds.get(0);

            return removeDup(docIds);
        }

        return reprDocumentId;
    }

    private String removeDup(List<String> docIds) {
        Map<String,List<String>> map = new HashMap<>();
        map.put("US", Arrays.asList("B9", "B2", "B1", "B8", "A9", "A2", "A1"));
        map.put("EP", Arrays.asList("B9", "B3", "B2", "B1", "B8", "A9", "A1", "A2", "A8", "A4"));
        map.put("DE", Arrays.asList("T", "U", "I", "D", "C", "B", "T1", "A"));
        map.put("TW", Arrays.asList("B", "U", "S", "A", "K2"));
        map.put("FR", Arrays.asList("B2", "B4", "B1", "B3", "E", "F", "M", "A2", "A4", "A6", "A8", "A1", "A3", "A5", "A7", "A"));
        map.put("RU", Arrays.asList("C9", "C3", "C2", "C1", "C", "C8", "U9", "U1", "U8", "A", "A1", "A8", "A3", "K3", "K1"));
        map.put("GB", Arrays.asList("C3", "C2", "C", "B", "B8", "A9", "A", "A8"));
        map.put("CA", Arrays.asList("A", "A2", "B", "C", "E", "F"));
        map.put("AU", Arrays.asList("C9", "C8", "C", "C1", "C4", "B9", "B8", "B1", "B2", "B3", "B4", "B", "A9", "A6", "A2", "A4", "A1", "A3", "A5", "A", "A8", "D0"));
        map.put("IT", Arrays.asList("C9", "C3", "C2", "C1", "C", "C8", "U9", "U1", "U8", "A", "A1", "A8", "A3", "K2"));
        map.put("IN", Arrays.asList("A1", "B"));
        map.put("WO", Arrays.asList("A9", "A1", "A2", "A4", "A8", "A3"));
        map.put("OTHER", Arrays.asList("B", "E", "H", "S", "P", "C", "Y", "I", "F"));

        List<String> types = map.get(docIds.get(0).substring(0,2).toUpperCase());
        if(types != null) {
            for (String type : types)
                for (String docid : docIds)
                    if (docid.matches(".+"+type.toLowerCase()+"$"))
                        return docid;
        } else {
            types = map.get("OTHER");
            for (String type : types)
                for (String docid : docIds)
                    if (docid.matches(".+" + type.toLowerCase() + "[0-9]?$"))
                        return docid;
        }

        return docIds.get(0);
    }


    @JsonDeserialize(using = StringDeseralizer.class)
    public String reprDocumentId;
    public String scopus;

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> category;
    public List<String> citationSource;
    public List<String> citationStep;

//    // insert
//    public List<String> docIds;

    public boolean isEmpty() {
        if (this.text == null && this.citationNumber == null && this.patentId == null && this.scopus == null)
            return true;

        return false;
    }
}
