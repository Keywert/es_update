package es.tmp.person.assignment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.utility.ESDocIdCheck;
import es.model.Applicant;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class KRAssignmentMain {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        List<ESConfig> esConfigs = new ArrayList<>();
//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es20"));
        esConfigs.add(new ESConfig("es188"));

        ObjectMapper objectMapper = new ObjectMapper();

        String tableName = "KIPRIS.RIGHT_HOLDER";

        String query = "SELECT distinct REGISTRATION_NUMBER FROM " + tableName +
                " where status is null " +
//                " and REGISTRATION_NUMBER in ('1000063560000', '1003445790000')" +
//                " and APPLICATION_NUMBER in ('10315250' ,'13310755' )" +
//                " where APPLICATION_NUMBER in ('15167415','15862751','15187119','15046630' )" +
                " limit 50000";

        System.out.println(query);

        String index = "kipo";
        String type = "patent";

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> regNumSet = new HashSet<>();
            while (rs.next()) {
                String REGISTRATION_NUMBER = rs.getString("REGISTRATION_NUMBER");
                if (REGISTRATION_NUMBER == null || "".equals(REGISTRATION_NUMBER))
                    continue;

                regNumSet.add(REGISTRATION_NUMBER);
            }

            for (String regNum : regNumSet) {
//                System.out.println(regNum);
                AssignmentVO assignmentVO = getAssignmentInfo(conn, regNum);
                if (assignmentVO == null) {
                    updateDB(conn,tableName,regNum,9);
                    continue;
                }
//                try {
//                    System.out.println(new ObjectMapper().writeValueAsString(assignmentVO));
//                } catch (JsonProcessingException e) {
//                    e.printStackTrace();
//                }

                Set<String> docIds = ESDocIdCheck.getKRRegDocIds(esConfigs.get(0), index, regNum);
                if (docIds == null) {
                    updateDB(conn,tableName,regNum,9);
                    continue;
                }

                Map<String,Object> jsonMap = new HashMap<>();
                jsonMap.put("assignment", assignmentVO);

                String json = objectMapper.writeValueAsString(jsonMap);

                System.out.println(docIds + " : " + json);

//                for (String docId : docIds)
//                    for (ESConfig esConfig : esConfigs)
//                        esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", docId)
//                                .doc(json.getBytes())
//                                .upsert(json.getBytes()));

                updateDB(conn,tableName,regNum,1);
            }


            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


    }

    private static void updateDB(Connection conn, String tableName, String regNum, Integer status) {
        String query = "UPDATE "+tableName+" SET status = ? " +
                " WHERE REGISTRATION_NUMBER = ?";

        if (regNum == null || "".equals(regNum))
            return;

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            if (status == null)
                status = 1;

            preparedStatement.setInt(1, status);
            preparedStatement.setString(2, regNum);

            preparedStatement.addBatch();

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static AssignmentVO getAssignmentInfo(Connection conn, String regNum) {
        if (regNum == null || "".equals(regNum))
            return null;

        try {
            String query = "select * from KIPRIS.RIGHT_HOLDER where REGISTRATION_NUMBER = '" + regNum + "'";
            Statement st = null;
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<Applicant> assigneeInfo = new ArrayList<>();
            List<Applicant> assignorInfo = new ArrayList<>();
            List<Applicant> exLicenseeInfo = new ArrayList<>();
            List<Applicant> nexLicenseeInfo = new ArrayList<>();

            while (rs.next()) {
                String PERTINENT_PARTITION = rs.getString("PERTINENT_PARTITION");
                if (PERTINENT_PARTITION == null)
                    continue;

                String RANK_CORRELATOR_TYPE = rs.getString("RANK_CORRELATOR_TYPE");
                if (RANK_CORRELATOR_TYPE == null)
                    continue;

                Integer RANK_CORRELATOR_SERIAL_NUMBER = rs.getInt("RANK_CORRELATOR_SERIAL_NUMBER");
                if (RANK_CORRELATOR_SERIAL_NUMBER == null)
                    continue;

                Integer RANK_NUMBER = rs.getInt("RANK_NUMBER");
                if (RANK_NUMBER == null)
                    continue;

                String RANK_CORRELATOR_NAME = rs.getString("RANK_CORRELATOR_NAME");
                if (RANK_CORRELATOR_NAME == null)
                    continue;

                String RANK_CORRELATOR_ADDRESS = rs.getString("RANK_CORRELATOR_ADDRESS");
                String RANK_CORRELATOR_NUMBER = rs.getString("RANK_CORRELATOR_NUMBER");
                String REGISTRATION_DATE = rs.getString("REGISTRATION_DATE");

                Applicant person = new Applicant();
                person.setName(RANK_CORRELATOR_NAME);
                person.setAddress(RANK_CORRELATOR_ADDRESS);
                person.setDate(REGISTRATION_DATE);
                person.setRankNumber(RANK_NUMBER);

                if (RANK_CORRELATOR_NUMBER != null && !"".equals(RANK_CORRELATOR_NUMBER))
                    person.setCode(RANK_CORRELATOR_NUMBER);

                person.setSequence(RANK_CORRELATOR_SERIAL_NUMBER.toString());

                if (PERTINENT_PARTITION.matches("^권리자란$") && RANK_NUMBER != 1) {
                    if (RANK_CORRELATOR_TYPE.matches("^(권리자)$"))
                        assignorInfo.add(person);
                    else if (RANK_CORRELATOR_TYPE.matches("^(승계청)$")) {
                        person.setName("대한민국 " + RANK_CORRELATOR_NAME);
                        assignorInfo.add(person);
                    }else if (RANK_CORRELATOR_TYPE.matches("^(의무자)$"))
                        assigneeInfo.add(person);
                } else if (PERTINENT_PARTITION.matches("^전용권자란$") && RANK_CORRELATOR_TYPE.matches("^(권리자)$"))
                    exLicenseeInfo.add(person);
                else if (PERTINENT_PARTITION.matches("^통상권자란$") && RANK_CORRELATOR_TYPE.matches("^(권리자)$"))
                    nexLicenseeInfo.add(person);
            }

            boolean flag = false;
            AssignmentVO assignmentVO = new AssignmentVO();
            if (!assigneeInfo.isEmpty()) {
                assignmentVO.setAssigneeInfo(assigneeInfo);
                flag = true;
            }

            if (!assignorInfo.isEmpty()) {
                assignmentVO.setAssignorInfo(assignorInfo);
                flag = true;
            }

            if (!exLicenseeInfo.isEmpty()) {
                assignmentVO.setExLicenseeInfo(exLicenseeInfo);
                flag = true;
            }

            if (!nexLicenseeInfo.isEmpty()) {
                assignmentVO.setNexLicenseeInfo(nexLicenseeInfo);
                flag = true;
            }

            if (!flag)
                return null;

            return assignmentVO;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
