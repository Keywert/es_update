package es.tmp.person.assignment;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.model.Applicant;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssignmentVO {
    public Boolean assignmentFlag;
    public Boolean licenseFlag;
    public Boolean pledgeFlag;

    public List<Applicant> assigneeInfo;
    public List<Applicant> assignorInfo;
    public List<Applicant> exLicenseeInfo;
    public List<Applicant> nexLicenseeInfo;
    public List<Applicant> pledgeeInfo;

    public List<History> history;

}
