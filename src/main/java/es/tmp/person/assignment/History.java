package es.tmp.person.assignment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import es.model.Applicant;


import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class History {
    //
    public String historyType;
    public String rankNumber;

    // 서류
    public String docName;
    public String docNameCode;

    // 접수일
    public String receiptDate;

    // 등록원인명 등록원인코드 등록원인일 등록목적
    public String regName;
    public String regNameCode;
    public String regDate;
    public String regPupos;

    // 실시권 사용기간 시작일 종료
    public String licenseStartDate;
    public String licenseEndDate;

    // 실시내용, 실시지역명;
    public String licenseContents;
    public String licenseArea;
    public String licenseETC;

    // 국가승계관리자성명
    public String rsmptMangerName;

    // 국가승계관리청명
    public String rsmptMangementAgency;

    // 승계청명
    public String rsmptOfficeName;

    // 질권채권액내용
    public String pledgeAmount;

    // 질권채권변제일자
    public String pledgeRepaymentDate;

    // 질권특약사항
    public String pledgeSpecialContents;

    // 인먕정보
    public List<Applicant> personInfo;
}
