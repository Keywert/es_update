package es.tmp.person.uspto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssignmentVO {
    String rfID;
    String applicationNumber;
    String excutionDate;
    Integer conveyanceType;

    String assignorsName;
    String assignorsAddr1;
    String assignorsAddr2;

    String assigneeName;
    String assigneeAddr1;
    String assigneeAddr2;
    String assigneeCity;
    String assigneeState;
    String assigneeCountry;
    String assigneePostcode;
}
