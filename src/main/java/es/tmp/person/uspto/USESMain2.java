package es.tmp.person.uspto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.Applicant;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.ESDocIdCheck;
import es.module.utility.NameNormalizer;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class USESMain2 {
    public static void main(String[] args) {
        int size = 1;

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();
//        Connection local_conn = dbHelper.getConn_local();
        DataFarmDB dataFarmDB = new DataFarmDB();

        int count = 0;

        ObjectMapper objectMapper = new ObjectMapper();
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es205"));
//        esConfigs.add(new ESConfig("es13"));
//        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es167"));

        if (esConfigs.isEmpty())
            return;

        String tableName = "USPTO.APPLICATION_ASSIGNMENT";

        Set<String> appNumSet = selectApnum(conn, tableName);
        if (appNumSet == null)
            return;

        System.out.println("Application Size : " + appNumSet.size());

        String index = "uspto";
        String type = "patent";

        try {
            ArrayList<String> appNums = new ArrayList<>();
            for (String applicationNumber : appNumSet) {
                appNums.add(applicationNumber);
                if (appNums.size() > 100) {
                    Map<String, Set<String>> docIdMap = ESDocIdCheck.getESdocIds(esConfigs.get(0), index, appNums);
                    if (docIdMap != null)
                        updateES(conn, esConfigs, objectMapper, index, docIdMap, dataFarmDB);

                    Thread.sleep(1000);

                    updateDB(conn, appNums);

                    appNums.clear();
                }
            }

            if (!appNums.isEmpty()) {
                Map<String, Set<String>> docIdMap = ESDocIdCheck.getESdocIds(esConfigs.get(0), index, appNums);
                if (docIdMap != null)
                    updateES(conn, esConfigs, objectMapper, index, docIdMap, dataFarmDB);

                Thread.sleep(1000);

                updateDB(conn, appNums);

                appNums.clear();
            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        for (int i = 0; i < size; i++) {


//        08952272

//            String query = "SELECT distinct APPLICATION_NUMBER FROM " + tableName +
//                    " where status is null and APPLICATION_NUMBER != '' "
//                    " where  APPLICATION_NUMBER in ('12236914','16842642','12621311','15077892')"
//                    " where APPLICATION_NUMBER in ('18071282')"
//                  +  '12045729', '  " limit 20000"
        ;

//            String query = "select * from data_work.patent_koipa_2023_cu_AP_RE where APPLICATION_NUMBER like 'US11%' ";

//            System.out.println(query);
//
//            Statement st = null;
//
//            try {
//                st = conn.createStatement();
//                st.setFetchSize(100);
//                ResultSet rs = st.executeQuery(query);
//
//                ArrayList<String> appNums = new ArrayList<>();
//
//                while (rs.next()) {
//                    if (++count % 1000 == 0)
//                        System.out.println(count);
//
//                    String applicationNumber = rs.getString("APPLICATION_NUMBER");
//                    if (applicationNumber == null || "".equals(applicationNumber))
//                        continue;
//
//                    applicationNumber = applicationNumber.replaceAll("(US|/)", "");
//
////                    System.out.println(applicationNumber);
//
//                    if (applicationNumber.matches("^[0-9]{8}$"))
//                        applicationNumber = applicationNumber.substring(0, 2) + "-" + applicationNumber.substring(2);
//
//                    appNums.add(applicationNumber);
//
//                    if (appNums.size() > 100) {
//                        Map<String, Set<String>> docIdMap = ESDocIdCheck.getESdocIds(esConfigs.get(0), index, appNums);
//                        if (docIdMap != null)
//                            updateES(conn, esConfigs, objectMapper, index, docIdMap, dataFarmDB);
//
//                        Thread.sleep(1000);
//
//                        updateDB(conn, appNums);
//
//                        appNums.clear();
//                    }
//                }
//
//                if (!appNums.isEmpty()) {
//                    Map<String, Set<String>> docIdMap = ESDocIdCheck.getESdocIds(esConfigs.get(0), index, appNums);
//                    if (docIdMap != null)
//                        updateES(conn, esConfigs, objectMapper, index, docIdMap, dataFarmDB);
//
//                    Thread.sleep(1000);
//
//                    updateDB(conn, appNums);
//
//                    appNums.clear();
//                }
//
//
//                for (ESConfig esConfig : esConfigs)
//                    esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
//
//
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        }

        System.out.println("Finish Count : " + count);

    }

    private static Set<String> selectApnum(Connection conn, String tableName) {
        String query = "SELECT distinct APPLICATION_NUMBER FROM " + tableName +
                " where status is null and APPLICATION_NUMBER != '' ";
        System.out.println(query);

        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> appNums = new HashSet<>();

            while (rs.next()) {
                String applicationNumber = rs.getString("APPLICATION_NUMBER");
                if (applicationNumber == null || "".equals(applicationNumber))
                    continue;

                applicationNumber = applicationNumber.replaceAll("(US|/)", "");

//                    System.out.println(applicationNumber);

                if (applicationNumber.matches("^[0-9]{8}$"))
                    applicationNumber = applicationNumber.substring(0, 2) + "-" + applicationNumber.substring(2);

                appNums.add(applicationNumber);
            }

            if (!appNums.isEmpty())
                return appNums;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void updateDB(Connection conn, List<String> appNums) {
        String query = "UPDATE USPTO.APPLICATION_ASSIGNMENT SET status = 1 " +
                " WHERE application_number = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String apnum : appNums) {
//                System.out.println( apnum + " : " + query);
                if ("".equals(apnum))
                    continue;

                preparedStatement.setString(1, apnum.replaceAll("-", ""));
                preparedStatement.addBatch();

            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateES(Connection conn, List<ESConfig> esConfigs, ObjectMapper objectMapper, String
            index, Map<String, Set<String>> map, DataFarmDB dataFarmDB) {
        if (map == null)
            return;

        Iterator<String> iterator = map.keySet().iterator();
        List<QueueVO> queueVOList = new ArrayList<>();
        long start = System.currentTimeMillis();

        while (iterator.hasNext()) {
            String appNum = iterator.next();

            Set<String> docIds = map.get(appNum);
            if (docIds == null)
                continue;


            String json = getAssignmentInfo(conn, objectMapper, appNum);
            if (json == null)
                continue;

//            System.out.println(docIds + " : " + json);
//            System.out.println("getAssignmentInfo time : " +  ((System.currentTimeMillis() - start) / 1000.0) );
//            start = System.currentTimeMillis();


            for (String docId : docIds) {
                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest("uspto", "patent", docId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));


                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(docId);
                queueVO.setLevel(5);
                queueVOList.add(queueVO);
            }

//            System.out.println("ES insert time : " +  ((System.currentTimeMillis() - start) / 1000.0) );
//            start = System.currentTimeMillis();

            if (queueVOList.size() > 10) {
                dataFarmDB.insertESQueue(queueVOList);
                queueVOList.clear();
            }
        }

        if (!queueVOList.isEmpty())
            dataFarmDB.insertESQueue(queueVOList);


    }

    private static String getAssignmentInfo(Connection conn, ObjectMapper objectMapper, String appNum) {
        if (appNum == null)
            return null;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        appNum = appNum.replaceAll("-", "");

        String query = "select * " +
                " from USPTO.APPLICATION_ASSIGNMENT A inner join USPTO.APPLICATION_ASSIGNOR B  inner join USPTO.APPLICATION_ASSIGNEE C inner join USPTO.CONVEYANCE_TYPE D" +
                "    on A.RF_ID = B.RF_ID and A.RF_ID = C.RF_ID and A.CONVEYANCE_TYPE = D.CONVEYANCE_TYPE" +
                " where A.APPLICATION_NUMBER = '" + appNum + "' and B.APPLICATION_NUMBER = '" + appNum + "' and C.APPLICATION_NUMBER = '" + appNum + "'" +
                " order by EXECUTION_DATE, A.RF_ID";

//        System.out.println(query);

        Statement st;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            int count = 0;

            Map<String, Object> jsonMap = new HashMap<>();


            Map<String, Applicant> rightholderMap = new HashMap<>();

            Map<String, Applicant> rightholderMap2 = new HashMap<>();
            // 전체 양도인 이름
            Set<String> assignorsNameAll = new HashSet<>();

            Map<String, Applicant> assigneeMap = new HashMap<>();
            Map<String, Applicant> assignorMap = new HashMap<>();


            Set<String> assignorsNames = new HashSet<>();
            Set<String> assigneeNames = new HashSet<>();

            Set<String> assignors = new HashSet<>();
            Map<String, Applicant> assignees = new HashMap<>();

            String currentExcutionDate = "";
            String preExcutionDate = "";

            String preRfid = "";

            while (rs.next()) {
                ++count;
                AssignmentVO assignmentVO = new AssignmentVO();

                String applicationNumber = rs.getString("A.APPLICATION_NUMBER");
                if (applicationNumber != null && !"".equals(applicationNumber))
                    assignmentVO.setApplicationNumber(applicationNumber);

                String rfID = rs.getString("A.RF_ID");
                if (rfID != null && !"".equals(rfID))
                    assignmentVO.setRfID(rfID);

                String excutionDate = rs.getString("B.EXECUTION_DATE");
                if (excutionDate != null && !"".equals(excutionDate))
                    assignmentVO.setExcutionDate(excutionDate);

                Integer conveyanceType = rs.getInt("D.IS_FINAL_CONVEYANCE");
                if (conveyanceType != null && !"".equals(conveyanceType))
                    assignmentVO.setConveyanceType(conveyanceType);

                // Assignors
                List<String> assignorsAddrs = new ArrayList<>();
                String assignorsName = rs.getString("B.NAME");
                if (assignorsName != null && !"".equals(assignorsName)) {
                    assignmentVO.setAssignorsName(assignorsName);
                    assignorsNames.add(assignorsName);
                }

                String assignorsAddr1 = rs.getString("B.ADDRESS1");
                if (assignorsAddr1 != null && !"".equals(assignorsAddr1.trim())) {
                    assignmentVO.setAssignorsAddr1(assignorsAddr1);
                    assignorsAddrs.add(assignorsAddr1);
                }

                String assignorsAddr2 = rs.getString("B.ADDRESS2");
                if (assignorsAddr2 != null && !"".equals(assignorsAddr2.trim())) {
                    assignmentVO.setAssignorsAddr2(assignorsAddr2);
                    assignorsAddrs.add(assignorsAddr2);
                }


                // Assignee
                List<String> assigneeAddrs = new ArrayList<>();
                String assigneeName = rs.getString("C.NAME");
                if (assigneeName != null && !"".equals(assigneeName.trim())) {
                    assignmentVO.setAssigneeName(assigneeName);
                    assigneeNames.add(assigneeName);
                }

                String assigneeAddr1 = rs.getString("C.ADDRESS1");
                if (assigneeAddr1 != null && !"".equals(assigneeAddr1.trim())) {
                    assignmentVO.setAssigneeAddr1(assigneeAddr1);
                    assigneeAddrs.add(assigneeAddr1);
                }

                String assigneeAddr2 = rs.getString("C.ADDRESS2");
                if (assigneeAddr2 != null && !"".equals(assigneeAddr2.trim())) {

                    assignmentVO.setAssigneeAddr2(assigneeAddr2);
                    assigneeAddrs.add(assigneeAddr2);

                }

                String assigneeCity = rs.getString("C.CITY");
                if (assigneeCity != null && !"".equals(assigneeCity.trim())) {
                    assignmentVO.setAssigneeCity(assigneeCity);
                    assigneeAddrs.add(assigneeCity);
                }

                String assigneeState = rs.getString("C.STATE");
                if (assigneeState != null && !"".equals(assigneeState.trim())) {
                    assignmentVO.setAssigneeCity(assigneeState);
                    assigneeAddrs.add(assigneeState);
                }

                String assigneePostcode = rs.getString("C.POSTCODE");
                if (assigneePostcode != null && !"".equals(assigneePostcode.trim())) {
                    assignmentVO.setAssigneePostcode(assigneePostcode);
                    assigneeAddrs.add(assigneePostcode);
                }

                String assigneeCountry = rs.getString("C.COUNTRY_NAME");
                if (assigneeCountry != null && !"".equals(assigneeCountry.trim())) {
                    assignmentVO.setAssigneeCountry(assigneeCountry);
                    assigneeAddrs.add(assigneeCountry);
                }

                Applicant assignee = new Applicant();
                assignee.setName(assigneeName);
                assignee.setEName(assigneeName);

                if ("".equals(assigneeCountry))
                    assignee.setCountry("US");
                else
                    assignee.setCountry(assigneeCountry);

                if (!assigneeAddrs.isEmpty()) {
                    assignee.setAddress(String.join(", ", assigneeAddrs));
                }

                Applicant assignor = new Applicant();
                assignor.setName(assignorsName);
                assignor.setEName(assignorsName);

                if (!assignorsAddrs.isEmpty())
                    assignor.setAddress(String.join(", ", assignorsAddrs));

                String assignorsNameNorm = normName(assigneeNames, assignorsName);
                String assigneeNameNorm = normName(assigneeNames, assigneeName);

                // 현재권리자 추가
                if (conveyanceType == 1) {
                    if (preRfid.isEmpty())
                        preRfid = rfID;

                    if (!preRfid.equals(rfID)) {
                        preRfid = rfID;

                        for (String n : assignors)
                            rightholderMap.remove(n);

                        rightholderMap.putAll(assignees);
                        assignors.clear();
                        assignees.clear();
                    }

//                    if (assignorsName != null && !"".equals(assignorsName)) {
//                        if (!assignorsName.equals(assigneeName))
//                            assignors.add(assignorsName.replaceAll("[,|\\.|\\s+|\\-]", ""));
//                    }
//
//                    if (assigneeName != null && !"".equals(assigneeName))
//                        rightholderMap.put(assigneeName.replaceAll("[,|\\.|\\s+|\\-]", ""), assignee);

                    if (assignorsNameNorm != null && !"".equals(assignorsNameNorm)) {
                        if (!assignorsNameNorm.equals(assigneeNameNorm))
                            assignors.add(assignorsNameNorm);
                    }

                    if (assigneeNameNorm != null && !"".equals(assigneeNameNorm))
                        rightholderMap.put(assigneeNameNorm, assignee);

                }

                // 양도인 제거
                if (!assignors.isEmpty())
                    for (String n : assignors)
                        rightholderMap.remove(n);

                // 양수인 추가
                if (!assignees.isEmpty())
                    rightholderMap.putAll(assignees);

                assigneeMap.put(assigneeNameNorm, assignee);
                assignorMap.put(assignorsNameNorm, assignor);

//                try {
//                    System.out.println(new ObjectMapper().writeValueAsString(assignmentVO));
//                } catch (JsonProcessingException e) {
//                    e.printStackTrace();
//                }
            }

            Map<String, Object> assignmentInfo = new HashMap<>();
            assignmentInfo.put("assignees", assigneeMap.values());
            assignmentInfo.put("assignors", assignorMap.values());

//            for (String name : assignorsNameAll) {
//                rightholderMap2.remove(name);
//            }
//
//            if (!rightholderMap2.isEmpty()) {
//                jsonMap.put("currentRightHolderInfo", rightholderMap2.values());
//
//                for (Applicant applicant : rightholderMap2.values()) {
//                    jsonMap.put("firstRightHolderInfo", applicant);
//                    break;
//                }
//            }

//            System.out.println(objectMapper.writeValueAsString(rightholderMap2.values()));

            if (!assignmentInfo.isEmpty())
                jsonMap.put("assignmentInfo", assignmentInfo);

//            if (!rightholderMap2.isEmpty())
//                rightholderMap.putAll(rightholderMap2);


            if (!rightholderMap.isEmpty()) {
                jsonMap.put("currentRightHolderInfo", rightholderMap.values());

                for (Applicant applicant : rightholderMap.values()) {
                    jsonMap.put("firstRightHolderInfo", applicant);
                    break;
                }
            }


//            try {
//                System.out.println(new ObjectMapper().writeValueAsString(jsonMap));
//            } catch (JsonProcessingException e) {
//                e.printStackTrace();
//            }

//            System.out.println("Finish Count : " + count);

            if (!jsonMap.isEmpty()) {
                jsonMap.put("lastUpdateDate", dtf.format(localDate));
                jsonMap.put("personInfoUpdateDate", dtf.format(localDate));

                String json = objectMapper.writeValueAsString(jsonMap);
                jsonMap = null;
                rightholderMap = null;
                assigneeMap = null;
                assignorMap = null;
                rightholderMap = null;
                rightholderMap2 = null;
                assignorsNames = null;
                assigneeNames = null;
                assignors = null;
                assignees = null;
                return json;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String normName(Set<String> assigneeNames, String name) {
        NameNormalizer nameNormalizer = new NameNormalizer();

        String normName = nameNormalizer.nameNorm(name);

        for (String n : assigneeNames) {
            String normN = nameNormalizer.nameNorm(n);
            if (n.contains(normName) || name.contains(n) || normN.contains(normName) || normName.contains(n))
                return normN;
        }

        return normName;
    }

}
