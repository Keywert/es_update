package es.tmp.person.kipo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CodeVO {
    public String applicationNumber;
    public String type;
    public String rankNumber;
    public String actionDate;
    public String version;
    public String versionDate;

    public String office;
    public String status;
    public String dataSource;
    public String symbolPosition;
    public String value;
    public String level;
    public String edition;

    public String qualifyingCharacter;


    public String code;
    public String seq;
    public String text;

    public String section;
    public String mainClass;
    public String subClass;


    public String mainGroup;
    public String subGroup;

    public Integer groupSerialNumber;
    public Integer breakdownSerialNumber;


}
