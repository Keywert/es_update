package es.tmp.person.kipo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.Applicant;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.NumberUtils;
import es.tmp.person.assignment.AssignmentVO;
import es.tmp.person.assignment.History;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class KipoAssignmentTask extends TaskBase {
    @Override
    public int jobMain() {
        System.out.println("KipoAssignmentTask");

        try {
            Set<String> regNumSet = null;

            String tableName = "KIPRIS.REGISTRATION_INFO_V2";

            DBHelper dbHelper = new DBHelper();
            Connection conn = dbHelper.getConn_aurora();
            ESConfig esConfig = new ESConfig("es188");

            DataFarmDB dataFarmDB = new DataFarmDB();
            int count = 0;
//            do {
            for (int i = 0, len = 100; i < len; i++) {
                int size = 1000;
                regNumSet = null;
                regNumSet = getRegNums(conn, size);
                if (regNumSet == null)
                    break;

                assignmentUpdate(conn, esConfig, dataFarmDB, regNumSet);
                updateDB(conn, tableName, regNumSet);

                count += size;
                System.out.println(count);
            }

//            } while (regNumSet != null);

            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
        } catch (
                InterruptedException e) {
            e.printStackTrace();
        }


        return 0;
    }

    private static void updateDB(Connection conn, String tableName, Set<String> regNumSet) {
        String query = "UPDATE " + tableName + " SET status = 1" +
                " WHERE RGSTNO = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String reg : regNumSet) {
                preparedStatement.setString(1, reg);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void assignmentUpdate(Connection conn, ESConfig esConfig, DataFarmDB dataFarmDB, Set<String> regNumSet) {
        if (regNumSet.isEmpty())
            return;

        String index = "kipo";
        String type = "patent";

        ObjectMapper objectMapper = new ObjectMapper();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String assignmentUpdateDate = dtf.format(localDate);

        List<QueueVO> queueVOList = new ArrayList<>();

        for (String regNum : regNumSet) {
            String query = "select * " +
//                    "  A.RGSTNO, A.PTPRT_CD_NM, A.RKNO, A.RGST_TPCD_NM,A.DOC_NM, A.RCPT_DT,A.IDSAPP_FG,A.DISAPP_CS_CONT,RGST_CS_NM, RGST_CS_DT, RGST_PUPOS, PLDG_CDAMT_CONT," +
//                    "       PLDG_SPCNT_MTR, B.RANK_CORRELATOR_TYPE,B.RANK_CORRELATOR_SERIAL_NUMBER,B.RANK_CORRELATOR_NAME ,B.RANK_CORRELATOR_ADDRESS,B.REGISTRATION_DATE, " +
//                    "       B.TRANSFER_DATE" +
                    " from KIPRIS.REGISTRATION_INFO_V2 A left join KIPRIS.RIGHT_HOLDER B " +
                    "                                             on A.RGSTNO = B.REGISTRATION_NUMBER and A.PTPRT_CD_NM = B.PERTINENT_PARTITION and A.RKNO = B.RANK_NUMBER " +
                    " where A.PTPRT_CD != '20901' and  A.RGSTNO = '" + regNum + "'" +
                    " order by RGSTNO, PTPRT_CD_NM, RKNO, B.RANK_CORRELATOR_TYPE, B.RANK_CORRELATOR_SERIAL_NUMBER ";

            Statement st = null;
            try {
                st = conn.createStatement();
                st.setFetchSize(100);
                ResultSet rs = st.executeQuery(query);

                Set<String> set = new HashSet<>();

                List<Applicant> assignorInfo = new ArrayList<>();
                List<Applicant> assigneeInfo = new ArrayList<>();
                List<Applicant> exLicenseeInfo = new ArrayList<>();
                List<Applicant> nexLicenseeInfo = new ArrayList<>();
                List<Applicant> pledgeeInfo = new ArrayList<>();

                Map<String, History> historyMap = new HashMap<>();

                while (rs.next()) {
                    // 등록번호
                    String RGSTNO = rs.getString("RGSTNO");
                    if (RGSTNO == null)
                        continue;

                    // 해당란, 권리자, 권리자란, 통상권자란, 전용권자란
                    String PTPRT_CD_NM = rs.getString("PTPRT_CD_NM");
                    if (PTPRT_CD_NM == null)
                        continue;

                    // 순위번호
                    String RKNO = rs.getString("RKNO");
                    if (RKNO == null)
                        continue;

//                    System.out.println(RGSTNO + " : " + PTPRT_CD_NM + " : " + RKNO);

                    String key = PTPRT_CD_NM + RKNO;
                    History history = historyMap.get(key);
                    if (history == null)
                        history = new History();

                    history.setHistoryType(PTPRT_CD_NM);
                    history.setRankNumber(RKNO);

                    // 서류코드
                    String DOC_CD = rs.getString("DOC_CD");
                    if (DOC_CD != null && !"".equals(DOC_CD))
                        history.setDocNameCode(DOC_CD);

                    // 서류명
                    String DOC_NM = rs.getString("DOC_NM");
                    if (DOC_NM != null && !"".equals(DOC_NM))
                        history.setDocName(DOC_NM);

                    // 접수일
                    String RCPT_DT = rs.getString("RCPT_DT");
                    if (RCPT_DT != null && !"".equals(RCPT_DT))
                        history.setReceiptDate(RCPT_DT);

                    // 등록원인코드 RGST_CSCD
                    String RGST_CSCD = rs.getString("RGST_CSCD");
                    if (RGST_CSCD != null && !"".equals(RGST_CSCD))
                        history.setRegNameCode(RGST_CSCD);

                    // 등록원인명
                    String RGST_CS_NM = rs.getString("RGST_CS_NM");
                    if (RGST_CS_NM != null && !"".equals(RGST_CS_NM))
                        history.setRegName(RGST_CS_NM);

                    // 등록원인일자
                    String RGST_CS_DT = rs.getString("RGST_CS_DT");
                    if (RGST_CS_DT != null && !"".equals(RGST_CS_DT))
                        history.setRegDate(RGST_CS_DT);

                    // 등록목적
                    String RGST_PUPOS = rs.getString("RGST_PUPOS");
                    if (RGST_PUPOS != null && !"".equals(RGST_PUPOS))
                        history.setRegPupos(RGST_PUPOS);

                    // 실시권사용기간시작일자
                    String LCNS_USPRD_STRT_DT = rs.getString("LCNS_USPRD_STRT_DT");
                    if (LCNS_USPRD_STRT_DT != null && !"".equals(LCNS_USPRD_STRT_DT))
                        history.setLicenseStartDate(LCNS_USPRD_STRT_DT);

                    // 실시권사용기간종료일자
                    String LCNS_USPRD_END_DT = rs.getString("LCNS_USPRD_END_DT");
                    if (LCNS_USPRD_END_DT != null && !"".equals(LCNS_USPRD_END_DT))
                        history.setLicenseEndDate(LCNS_USPRD_END_DT);

                    // 실시내용코드
//                    String WRKG_CONT_CD = rs.getString("WRKG_CONT_CD");

                    // 실시내용
                    String WRKG_CONT = rs.getString("WRKG_CONT");
                    if (WRKG_CONT != null && !"".equals(WRKG_CONT))
                        history.setLicenseContents(WRKG_CONT);

                    // 실시지역명
                    String WRKG_RGS_NM = rs.getString("WRKG_RGS_NM");
                    if (WRKG_RGS_NM != null && !"".equals(WRKG_RGS_NM))
                        history.setLicenseArea(WRKG_RGS_NM);

                    String ETC_LCNS_MTR = rs.getString("ETC_LCNS_MTR");
                    if (ETC_LCNS_MTR != null && !"".equals(ETC_LCNS_MTR))
                        history.setLicenseETC(ETC_LCNS_MTR);

                    // 국가승계관리자성명
                    String CNTRY_RSMPT_ADMNR_NM = rs.getString("CNTRY_RSMPT_ADMNR_NM");
                    if (CNTRY_RSMPT_ADMNR_NM != null && !"".equals(CNTRY_RSMPT_ADMNR_NM))
                        history.setRsmptMangerName(CNTRY_RSMPT_ADMNR_NM);

                    // 국가승계관리청명
                    String CNTRY_RSMPT_INTND_NM = rs.getString("CNTRY_RSMPT_INTND_NM");
                    if (CNTRY_RSMPT_INTND_NM != null && !"".equals(CNTRY_RSMPT_INTND_NM))
                        history.setRsmptMangementAgency(CNTRY_RSMPT_INTND_NM);

                    // 승계청명
                    String RSMPT_OFFC_NM = rs.getString("RSMPT_OFFC_NM");
                    if (RSMPT_OFFC_NM != null && !"".equals(RSMPT_OFFC_NM))
                        history.setRsmptOfficeName(RSMPT_OFFC_NM);


                    // 질권채권액내용
                    String PLDG_CDAMT_CONT = rs.getString("PLDG_CDAMT_CONT");
                    if (PLDG_CDAMT_CONT != null && !"".equals(PLDG_CDAMT_CONT))
                        history.setPledgeAmount(PLDG_CDAMT_CONT);

                    // 질권채권변제일자
                    String PLDG_EXPTN_DT = rs.getString("PLDG_EXPTN_DT");
                    if (PLDG_EXPTN_DT != null && !"".equals(PLDG_EXPTN_DT))
                        history.setPledgeRepaymentDate(PLDG_EXPTN_DT);

                    // 질권특약사항
                    String PLDG_SPCNT_MTR = rs.getString("PLDG_SPCNT_MTR");
                    if (PLDG_SPCNT_MTR != null && !"".equals(PLDG_SPCNT_MTR))
                        history.setPledgeSpecialContents(PLDG_SPCNT_MTR);

                    // 심판예고심판번호
                    String TRPRE_TRLNO = rs.getString("TRPRE_TRLNO");

                    // 심판예고청구일자
                    String TRPRE_DMND_DT = rs.getString("TRPRE_DMND_DT");

                    // 심판예고사건표시내용
                    String TRPRE_EVT_INDIC_CONT = rs.getString("TRPRE_EVT_INDIC_CONT");

                    // 심판예고청구취지
                    String TRPRE_DMND_OBJT = rs.getString("TRPRE_DMND_OBJT");

                    // 심결확정심판번호
                    String DOTDC_TRLNO = rs.getString("DOTDC_TRLNO");

                    // 심결확정일자
                    String DOTDC_DT = rs.getString("DOTDC_DT");

                    // 심결확정무효청구항수
                    String DOTDC_INVLD_DMND_ITCNT = rs.getString("DOTDC_INVLD_DMND_ITCNT");

                    // 심결확정요지
                    String DOTDC_ABS = rs.getString("DOTDC_ABS");

                    // 취하심판번호
                    String WTHDW_TRLNO = rs.getString("WTHDW_TRLNO");

                    // 취하일자
                    String WTHDW_DT = rs.getString("WTHDW_DT");

                    // 촉탁사건표시내용
                    String SEIZR_EVT_INDIC_CONT = rs.getString("SEIZR_EVT_INDIC_CONT");

                    // 촉탁사건표시내용
                    String CNTN_TMEXT_APPLNO = rs.getString("CNTN_TMEXT_APPLNO");

                    // 존속기간연장출원번호
                    String CNDRT_EXAPL_DT = rs.getString("CNDRT_EXAPL_DT");

                    // 존속기간연장결정일자
                    String CNTN_TMEXT_DCSN_DT = rs.getString("CNTN_TMEXT_DCSN_DT");

                    // 존속기간연장일수
                    String CNTN_TMEXT_DAYS = rs.getString("CNTN_TMEXT_DAYS");

                    String PLDG_IR_AGMNT_CONT = rs.getString("PLDG_IR_AGMNT_CONT");   // 질권이자약정내용
                    String PLDG_PNTY_AGMNT_CONT = rs.getString("PLDG_PNTY_AGMNT_CONT");   // 질권위약금약정내용
                    String DCSN_COARG_INVLD_PTCLS_CONT = rs.getString("DCSN_COARG_INVLD_PTCLS_CONT");   // 결정등록취소무효청구항내용
                    String REMN_CLAUS_CONT = rs.getString("REMN_CLAUS_CONT");   // 유지항내용
                    String DCSN_DEL_ITCNT = rs.getString("DCSN_DEL_ITCNT");   // 결정삭제항수
                    String DCSN_RGST_DLCLS_CONT = rs.getString("DCSN_RGST_DLCLS_CONT");   // 결정등록삭제항내용
                    String MD_ITRGT_REC_DT = rs.getString("MD_ITRGT_REC_DT");   // 마드리드국제등록기록일자
                    String MD_CNDRT_EXPTN_DT = rs.getString("MD_CNDRT_EXPTN_DT");   // 마드리드존속기간만료일자
                    String MD_LTST_RNWL_DT = rs.getString("MD_LTST_RNWL_DT");   // 마드리드최근갱신일자
                    String MD_ITRGT_EFOCR_DT = rs.getString("MD_ITRGT_EFOCR_DT");   // 마드리드국제등록효력발생일자
                    String ANXTN_TGT_INTNL_RGSTNO = rs.getString("ANXTN_TGT_INTNL_RGSTNO");   // 병합대상국제등록번호


                    /// KIPRIS.RIGHT_HOLDER
                    Applicant person = new Applicant();
                    person.setRankNumber(Integer.valueOf(RKNO));


                    // 일련번호
                    String RANK_CORRELATOR_SERIAL_NUMBER = rs.getString("RANK_CORRELATOR_SERIAL_NUMBER");
                    if (RANK_CORRELATOR_SERIAL_NUMBER != null)
                        person.setSequence(RANK_CORRELATOR_SERIAL_NUMBER);

                    // 이름
                    String RANK_CORRELATOR_NAME = rs.getString("RANK_CORRELATOR_NAME");
                    if (RANK_CORRELATOR_NAME != null)
                        person.setName(RANK_CORRELATOR_NAME);


                    // 주소
                    String RANK_CORRELATOR_ADDRESS = rs.getString("RANK_CORRELATOR_ADDRESS");
                    if (RANK_CORRELATOR_ADDRESS != null)
                        person.setAddress(RANK_CORRELATOR_ADDRESS);


                    // 번호
                    String RANK_CORRELATOR_NUMBER = rs.getString("RANK_CORRELATOR_NUMBER");
                    if (RANK_CORRELATOR_NUMBER != null && !"".equals(RANK_CORRELATOR_NUMBER))
                        person.setCode(RANK_CORRELATOR_NUMBER);

                    // 번호
                    String REGISTRATION_DATE = rs.getString("REGISTRATION_DATE");
                    if (REGISTRATION_DATE != null && !"".equals(REGISTRATION_DATE))
                        person.setDate(REGISTRATION_DATE);


                    // 종류
                    String RANK_CORRELATOR_TYPE = rs.getString("RANK_CORRELATOR_TYPE");
                    if (RANK_CORRELATOR_TYPE != null) {
                        person.setCorrelatorType(RANK_CORRELATOR_TYPE);
                    }

                    if (person.getName() != null) {
                        List<Applicant> personList = history.getPersonInfo();
                        if (personList == null)
                            personList = new ArrayList<>();

                        personList.add(person);
                        history.setPersonInfo(personList);
                    }
                    if (RANK_CORRELATOR_TYPE != null) {
                        if (DOC_CD.matches("^(0030271|0030272|0030273|0030274|0030353|0030354|0030355|0030356)$")) {
                            if (RANK_CORRELATOR_TYPE.matches("^(권리자)$"))
                                assigneeInfo.add(person);
                            else if (RANK_CORRELATOR_TYPE.matches("^(승계청)$")) {
                                person.setName("대한민국 " + RANK_CORRELATOR_NAME);
                                assigneeInfo.add(person);
                            } else if (RANK_CORRELATOR_TYPE.matches("^(의무자)$"))
                                assignorInfo.add(person);

                        } else if (DOC_CD.matches("^(0030231|0030275|0030276|0030277|0030278)$") && RANK_CORRELATOR_TYPE != null && RANK_CORRELATOR_TYPE.matches("^(권리자)$")) {
                            exLicenseeInfo.add(person);
                        } else if (DOC_CD.matches("^(0030233|0030283|0030285|0030425)$") && RANK_CORRELATOR_TYPE.matches("^(권리자)$")) {
                            nexLicenseeInfo.add(person);
                        } else if (DOC_CD.matches("^(0030291|0030311|0030312|0030313|0030315|0030317|0030319)$") && RANK_CORRELATOR_TYPE.matches("^(권리자)$")) {
                            pledgeeInfo.add(person);
                        }
                    }


                    historyMap.put(key, history);

                }

                AssignmentVO assignmentVO = new AssignmentVO();
                boolean flag = false;
                if (!assigneeInfo.isEmpty()) {
//                    System.out.println("assigneeInfo");
//                    System.out.println(new ObjectMapper().writeValueAsString(assigneeInfo));
                    assignmentVO.setAssigneeInfo(assigneeInfo);
                    flag = true;

                    assignmentVO.setAssignmentFlag(true);
                }

                if (!assignorInfo.isEmpty()) {
//                    System.out.println("assignorInfo");
//                    System.out.println(new ObjectMapper().writeValueAsString(assignorInfo));
                    assignmentVO.setAssignorInfo(assignorInfo);
                    flag = true;
                }

                if (!exLicenseeInfo.isEmpty()) {
//                    System.out.println("exLicenseeInfo");
//                    System.out.println(new ObjectMapper().writeValueAsString(exLicenseeInfo));
                    assignmentVO.setExLicenseeInfo(exLicenseeInfo);
                    assignmentVO.setLicenseFlag(true);
                    flag = true;
                }

                if (!nexLicenseeInfo.isEmpty()) {
//                    System.out.println("nexLicenseeInfo");
//                    System.out.println(new ObjectMapper().writeValueAsString(nexLicenseeInfo));
                    assignmentVO.setNexLicenseeInfo(nexLicenseeInfo);
                    assignmentVO.setLicenseFlag(true);
                    flag = true;
                }

                if (!pledgeeInfo.isEmpty()) {
//                    System.out.println("pledgeeInfo");
//                    System.out.println(new ObjectMapper().writeValueAsString(pledgeeInfo));
                    assignmentVO.setPledgeeInfo(pledgeeInfo);
                    assignmentVO.setPledgeFlag(true);
                    flag = true;
                }

                if (!historyMap.isEmpty()) {
//                    System.out.println("historyMap");
//                    System.out.println(new ObjectMapper().writeValueAsString(historyMap.values()));
                    assignmentVO.setHistory(historyMap.values().stream().collect(Collectors.toList()));
                    flag = true;
                }

                if (flag) {
                    Set<String> docIds = getDocumentIds(esConfig, regNum);
                    if (docIds != null) {
                        Map<String, Object> jsonMap = new HashMap<>();
                        jsonMap.put("assignment", assignmentVO);
                        jsonMap.put("assignmentUpdateDate", assignmentUpdateDate);
                        String json = objectMapper.writeValueAsString(jsonMap);

//                        index = "test_kipo_assignment";

                        for (String id : docIds) {
//                            System.out.println(regNum + " - " + id + " : " + json);

                            esConfig.bulkProcessor.add(new UpdateRequest(index, type, id)
                                    .doc(json.getBytes())
                                    .upsert(json.getBytes()));


                            QueueVO queueVO = new QueueVO();
                            queueVO.setEsindex("kipo");
                            queueVO.setDocumentId(id);
                            queueVO.setLevel(4);

                            queueVOList.add(queueVO);
                            if (queueVOList.size() > 10) {
                                dataFarmDB.insertESQueue(queueVOList);
                                queueVOList.clear();
                            }

                        }
                    }
                }

                if (!queueVOList.isEmpty())
                    dataFarmDB.insertESQueue(queueVOList);


            } catch (SQLException e) {
                e.printStackTrace();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }

    }

    private static Set<String> getDocumentIds(ESConfig esConfig, String registerNumber) {
        String index = "kipo";
        String type = "patent";

        boolean patentFlag = true;
        String docType = "[A|B][0-9]?";
        if (registerNumber.matches("^20.+")) {
            docType = "[U|Y][0-9]?";
            patentFlag = false;
        }
        String number = NumberUtils.getNumber("KR", registerNumber, "RN");

//        System.out.println(number + " : " +docType);

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.regexpQuery("documentType", docType))
                .filter(QueryBuilders.termsQuery("registerNumber", number));

        String[] fields = {"applicationNumber"};//"description"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

//        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        Set<String> documentIds = new HashSet<>();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            Object applicationNumber = hit.getSource().get("applicationNumber");

            if (applicationNumber == null)
                continue;

            if (patentFlag) {
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "a");
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "b1");
            } else {
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "u");
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "y1");
            }
        }

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(documentIds);

        scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        Set<String> docIdSet = new HashSet<>();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            docIdSet.add(hit.getId());
        }

        if (!docIdSet.isEmpty())
            return docIdSet;

        return null;
    }


    private Set<String> getRegNums(Connection conn, int size) {
        String query = "select distinct RGSTNO from KIPRIS.REGISTRATION_INFO_V2 " +
                " where status is null limit " + size;
//                " where status is null and RGSTNO regexp '^10[1-9][0-9]+$' limit " + size;
//        " where status is null and RGSTNO like '101901435%' limit " + size;

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> set = new HashSet<>();
            while (rs.next()) {
                String RGSTNO = rs.getString("RGSTNO");
                if (RGSTNO == null)
                    continue;

                set.add(RGSTNO);
            }

            if (!set.isEmpty())
                return set;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
