package es.tmp.person.kipo;

import es.model.Applicant;
import es.module.utility.CodeUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class KiprisApi {
    public static Map<String, Object> xmlpath = new HashMap<String, Object>();
    public final static String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";

    public static List<Applicant> patentApplicantInfo(String applicationNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("applicantInfo", "//body/items/patentApplicantInfo");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/patUtiModInfoSearchSevice/patentApplicantInfo";
//        String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        String xmlResult = null;

        try {
            apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
            System.out.println("RESULT => " + xmlResult);

//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpathMap.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpathMap.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    Applicant applicantVO = new Applicant();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("ApplicantNumber")) {
                            applicantVO.setCode(value);
                        } else if (name.matches("ApplicantName")) {
                            applicantVO.setName(value);
                        } else if (name.matches("ApplicantEnglishsentenceName")) {
                            applicantVO.setEnName(value);
                        } else if (name.matches("ApplicantAddress")) {
                            applicantVO.setAddress(value);
                        } else if (name.matches("ApplicantCountryName")) {
                            applicantVO.setCountry(value);
                        }
                    }

                    applicantVOS.add(applicantVO);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }

    public static List<Applicant> rpstApplicantInfoV2(String applicationNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("applicantInfo", "//body/items/rpstApplicantV2Info");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RpstApplicantService/rpstApplicantInfoV2";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        Applicant applicantVO = new Applicant();
        String xmlResult = null;

        try {
            apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
//            System.out.println("RESULT => " + xmlResult);

//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpathMap.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpathMap.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("rpstApplicantNumber")) {
                            applicantVO.setCode(value);
                        } else if (name.matches("rpstApplicantName")) {
                            applicantVO.setName(value);
                        } else if (name.matches("rpstApplicantNameEng")) {
                            applicantVO.setEnName(value);
                        }
                    }
                }
            }

            String applicantCode = applicantVO.getCode();

            if (applicantCode == null)
                return null;

            if (!applicantCode.contains("|")) {
                applicantVO.setSequence("1");
                applicantVOS.add(applicantVO);
            } else {
                String [] codes = applicantCode.split("\\|");
                String [] names_ko = applicantVO.getEnName().split("\\|");

                String [] names_en = null;
                if (applicantVO.getEnName() != null)
                    names_en = applicantVO.getEnName().split("\\|");

                for (int i=0,len=codes.length; i<len; i++) {
                    Applicant applicant = applicantVO.clone();
                    applicant.setSequence(String.valueOf(i));
                    applicant.setCode(codes[i]);
                    applicant.setName(names_ko[i]);

                    if (names_en != null)
                        if (i < names_en.length)
                            applicant.setEnName(names_en[i]);
                    applicantVOS.add(applicant);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }

    public List<Applicant> registrationLastRightHolderInfo(String registerNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("rightholderInfo", "//body/items/registrationLastRightHolderInfo");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationLastRightHolderInfo";
//        String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        String xmlResult = null;

        try {
            apiUrl += "?registrationNumber=" + registerNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
            System.out.println("RESULT => " + xmlResult);

//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpathMap.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpathMap.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    Applicant applicantVO = new Applicant();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("lastRightHolderNumber")) {
                            applicantVO.setCode(value);
                        } else if (name.matches("lastRightHolderName")) {
                            applicantVO.setName(value);
                        } else if (name.matches("lastRightHolderAddress")) {
                            applicantVO.setAddress(value);
                        } else if (name.matches("lastRightHolderCountry")) {
                            applicantVO.setCountry(value);
                        }
                    }

                    applicantVOS.add(applicantVO);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }

    public List<Applicant> registrationRightHolderInfo(String registerNumber) {
        Map<String, Object> xmlpathMap = new HashMap<>();
        xmlpathMap.put("registrationRightHolderInfo", "//body/items/registrationLastRightHolderInfo");
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationRightHolderInfo";
//        String accessKey = "efhU4iUG8trH97AIMuZIUgZguasnJMoiTtbY7HDhDtE=";

        List<Applicant> applicantVOS = new ArrayList<>();
        String xmlResult = null;

        try {
            apiUrl += "?registrationNumber=" + registerNumber + "0000" + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
            System.out.println("RESULT => " + xmlResult);




        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;
    }


    public String getBibliographySumryInfoSearch(String apNum) {
        String apiUrl = "http://plus.kipris.or.kr/kipo-api/kipi/patUtiModInfoSearchSevice/getBibliographySumryInfoSearch";
        String xmlResult = null;
        try {
            apiUrl += "?applicationNumber=" + apNum + "&ServiceKey=" + accessKey;
            URL url = new URL(apiUrl);

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
//            System.out.println("RESULT => " + xmlResult);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return xmlResult;
    }
    public String getBibliographyDetailInfoSearch(String apNum) {
        String apiUrl = "http://plus.kipris.or.kr/kipo-api/kipi/patUtiModInfoSearchSevice/getBibliographyDetailInfoSearch";
        String xmlResult = null;
        try {
            apiUrl += "?applicationNumber=" + apNum + "&ServiceKey=" + accessKey;
            URL url = new URL(apiUrl);

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
            System.out.println("RESULT => " + xmlResult);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }


    public List<ApplicantVO> getLastRightHolderInfo(String registerNumber) {
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationLastRightHolderInfo";
        apiUrl += "?registrationNumber=" + registerNumber + "&accessKey=" + accessKey;

        List<ApplicantVO> applicantVOS = new ArrayList<>();

        xmlpath.put("registrationLastRightHolderInfo", "//body/items/registrationLastRightHolderInfo");


        try {
            URL url = new URL(apiUrl);

            String xmlResult = null;

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();
            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");

//            System.out.println("RESULT => " + xmlResult);


            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();

                    ApplicantVO applicantVO = new ApplicantVO();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

//                        System.out.println(name + " : " + value);

                        if (value.isEmpty())
                            continue;

                        if (name.matches("lastRightHolderNumber")) {
                            applicantVO.setApplicantCode(value);
                        } else if (name.matches("lastRightHolderName")) {
                            applicantVO.setApplicantName_ko(value);
                        } else if (name.matches("lastRightHolderAddress")) {
                            applicantVO.setAddress(value);
                        } else if (name.matches("lastRightHolderCountry")) {
                            applicantVO.setCountry(value);
                        }
                    }

                    if (applicantVO.getApplicantName_ko() == null)
                        continue;

                    applicantVOS.add(applicantVO);

                }
            }


            if (!applicantVOS.isEmpty())
                return applicantVOS;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return null;

    }

    public List<CodeVO> getCurrentIpc(String applicationNumber) {
//http://plus.kipris.or.kr/openapi/rest/patUtiModInfoSearchSevice/patentCpcInfo?applicationNumber=1020060118886&accessKey=write your key
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/patUtiModInfoSearchSevice/patentIpcInfo";
        apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;

        List<ApplicantVO> applicantVOS = new ArrayList<>();

        xmlpath.put("patentIpcInfo", "//body/items/patentIpcInfo");

        try {
            URL url = new URL(apiUrl);

            String xmlResult = null;

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();
            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");

//            System.out.println("RESULT => " + xmlResult);


            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            List<CodeVO> codeVOS = new ArrayList<>();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    CodeVO codeVO = new CodeVO();
                    codeVO.setApplicationNumber(applicationNumber);
                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

//                        System.out.println(name + " : " + value);

                        if (value.isEmpty())
                            continue;

                        if (name.matches("InternationalpatentclassificationNumber")) {
                            codeVO.setText(value);
                            String code = CodeUtil.getIpc(value);
                            if (code != null)
                                if (!code.matches("^[A-z][0-9]{2}[A-z]-[0-9]{3,4}/[0-9|A-z]+$"))
                                    System.out.println("getCode : " + applicationNumber + " - " +  value + " - " + code);

                            codeVO.setCode(code);

                        } else if (name.matches("InternationalpatentclassificationDate"))
                            codeVO.setVersionDate(value);
                    }

                    if (codeVO.getCode() != null)
                        codeVOS.add(codeVO);
                }
            }

            if (!codeVOS.isEmpty())
                return codeVOS;

//            if (!applicantVOS.isEmpty())
//                return applicantVOS;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return null;
    }

    public List<CodeVO> getIpc(String applicationNumber) {
//http://plus.kipris.or.kr/openapi/rest/PatentClassificationInfoService/ipcClassificationInfo?applicationNumber=1020140000774&accessKey=write your key
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/PatentClassificationInfoService/ipcClassificationInfo";
        apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;

        List<ApplicantVO> applicantVOS = new ArrayList<>();

        xmlpath.put("ipcClassificationInfo", "//body/items/ipcClassificationInfo");

        try {
            URL url = new URL(apiUrl);

            String xmlResult = null;

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();
            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");

//            System.out.println(applicationNumber + " - " +"RESULT => " + xmlResult);


            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            List<CodeVO> codeVOS = new ArrayList<>();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    CodeVO codeVO = new CodeVO();
                    codeVO.setApplicationNumber(applicationNumber);
                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

//                        System.out.println(name + " : " + value);

                        if (value.isEmpty())
                            continue;

                        if (name.matches("ipcCode")) {
                            codeVO.setText(value);
                            String code = CodeUtil.getIpc(value);
                            if (code == null)
                                code = value;
                            else if (!code.matches("^[A-z][0-9]{2}[A-z]-[0-9]{3,4}/[0-9|A-z]+$"))
                                System.out.println("getCode : " + applicationNumber + " - " +  value + " - " + code);

                            codeVO.setCode(code);

                        } else if (name.matches("versionDate"))
                            codeVO.setVersionDate(value);
                        else if (name.matches("version"))
                            codeVO.setVersion(value);
                        else if (name.matches("groupSerialNumber"))
                            codeVO.setGroupSerialNumber(Integer.valueOf(value));
                        else if (name.matches("breakdownSerialNumber"))
                            codeVO.setBreakdownSerialNumber(Integer.valueOf(value));

                    }

                    if (codeVO.getCode() != null)
                        codeVOS.add(codeVO);
                }
            }

            if (!codeVOS.isEmpty())
                return codeVOS;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return null;
    }

    public List<CodeVO> getCurrentCpc(String applicationNumber) {
//http://plus.kipris.or.kr/openapi/rest/patUtiModInfoSearchSevice/patentCpcInfo?applicationNumber=1020060118886&accessKey=write your key
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/patUtiModInfoSearchSevice/patentCpcInfo";
        apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;

        List<ApplicantVO> applicantVOS = new ArrayList<>();

        xmlpath.put("patentCpcInfo", "//body/items/patentCpcInfo");

        try {
            URL url = new URL(apiUrl);

            String xmlResult = null;

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();
            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");

//            System.out.println("RESULT => " + xmlResult);


            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            List<CodeVO> codeVOS = new ArrayList<>();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    CodeVO codeVO = new CodeVO();
                    codeVO.setApplicationNumber(applicationNumber);
                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

//                        System.out.println(name + " : " + value);

                        if (value.isEmpty())
                            continue;

                        if (name.matches("CooperativepatentclassificationNumber")) {
                            codeVO.setText(value);
                            String code = CodeUtil.getCpc(value);
                            if (!code.matches("^[A-z][0-9]{2}[A-z]-[0-9]{3,4}/[0-9|A-z]+$"))
                                System.out.println("getCode : " + applicationNumber + " - " +  value + " - " + code);

                            codeVO.setCode(code);
                        } else if (name.matches("CooperativepatentclassificationDate"))
                            codeVO.setVersionDate(value);
                    }

                    if (codeVO.getCode() != null)
                        codeVOS.add(codeVO);
                }
            }

            if (!codeVOS.isEmpty())
                return codeVOS;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return null;
    }

    public List<CodeVO> getCpc(String applicationNumber) {
//http://plus.kipris.or.kr/openapi/rest/PatentClassificationInfoService/cpcClassificationInfo?applicationNumber=1020140000774&accessKey=write your key
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/PatentClassificationInfoService/cpcClassificationInfo";
        apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;

        List<ApplicantVO> applicantVOS = new ArrayList<>();

        xmlpath.put("cpcClassificationInfo", "//body/items/cpcClassificationInfo");

        try {
            URL url = new URL(apiUrl);

            String xmlResult = null;

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();
            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");

//            System.out.println("RESULT => " + xmlResult);


            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            List<CodeVO> codeVOS = new ArrayList<>();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    CodeVO codeVO = new CodeVO();
                    codeVO.setApplicationNumber(applicationNumber);
                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

//                        System.out.println(name + " : " + value);

                        if (value.isEmpty())
                            continue;

                        if (name.matches("cpcCode")) {
                            codeVO.setText(value);
                            String code = CodeUtil.getCpc(value);
                            if (!code.matches("^[A-z][0-9]{2}[A-z]-[0-9]{3,4}/[0-9|A-z]+$"))
                                System.out.println("getCode : " + applicationNumber + " - " +  value + " - " + code);

                            codeVO.setCode(code);

                        } else if (name.matches("versionDate"))
                            codeVO.setVersionDate(value);
                        else if (name.matches("version"))
                            codeVO.setVersion(value);
                        else if (name.matches("groupSerialNumber"))
                            codeVO.setGroupSerialNumber(Integer.valueOf(value));
                        else if (name.matches("breakdownSerialNumber"))
                            codeVO.setBreakdownSerialNumber(Integer.valueOf(value));

                    }

                    if (codeVO.getCode() != null)
                        codeVOS.add(codeVO);
                }
            }

            if (!codeVOS.isEmpty())
                return codeVOS;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return null;
    }

    public String getKPA(String applicationNumber) {
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/KpaBibliographicService/summation";

        String expression = "//body/items/summation";

        String xmlResult = null;
        try {
            apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
            System.out.println(applicationNumber + " RESULT => " + xmlResult);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));
            XPath xPath = XPathFactory.newInstance().newXPath();

            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

            int nodeLen = nodeList.getLength();
            if (nodeLen == 0)
                return null;

            for (int i = 0; i < nodeLen; i++) {
                Node node = nodeList.item(i);

               return node.getTextContent();
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return xmlResult;
    }

    public List<FeeVO> getFeeData(String regnum, String apnum) {
        // http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationFeeInfo?registrationNumber=2000642310000&accessKey=write your key
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RegistrationService/registrationFeeInfo";
        apiUrl += "?registrationNumber=" + regnum + "&accessKey=" + accessKey;

        List<ApplicantVO> applicantVOS = new ArrayList<>();

        xmlpath.put("registrationFeeInfo", "//body/items/registrationFeeInfo");

        try {
            URL url = new URL(apiUrl);

            String xmlResult = null;

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();
            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");

            System.out.println("RESULT => " + xmlResult);


            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            List<FeeVO> feeVOS = new ArrayList<>();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();
                    FeeVO feeVO = new FeeVO();
                    feeVO.setPatentNumber(regnum);
                    feeVO.setApplicationNumber(apnum);
                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

//                        System.out.println(name + " : " + value);
                        if (value.isEmpty())
                            continue;

//                        registrationDate : 20090212
//                        startAnnual : 1
//                        lastAnnual : 3
//                        paymentDegree : 1
//                        paymentFee : 95400
//                        paymentDate : 20090213

                        if (name.matches("registrationDate")) {
                            feeVO.setRegistrationDate(value);
                        } else if (name.matches("startAnnual"))
                            feeVO.setStart(Integer.valueOf(value));
                        else if (name.matches("lastAnnual"))
                            feeVO.setLast(Integer.valueOf(value));
                        else if (name.matches("paymentDegree"))
                            feeVO.setDegree(Integer.valueOf(value));
                        else if (name.matches("paymentFee"))
                            feeVO.setPaymentFee(value);
                        else if (name.matches("paymentDate"))
                            feeVO.setPaymentDate(value);

                    }

                    if (feeVO.getLast() != null)
                        feeVOS.add(feeVO);
                }
            }

            if (!feeVOS.isEmpty())
                return feeVOS;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return null;
    }
}
