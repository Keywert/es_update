package es.tmp.person.kipo;

import java.util.ArrayList;
import java.util.List;

public class TestTask {
    public static void main(String[] args) {
        List<TaskBase> jobList = new ArrayList<>();

        // 법적상태
//        jobList.add(new KipoLegalStatusUpdate());
//        jobList.add(new UsptoLegalStatusUpdate());
//        jobList.add(new JpoLegalStatusUpdate());
//        jobList.add(new EpoLegalStatusUpdate());

        // Sep
//        jobList.add(new InsertSep());


        // Assignment
        jobList.add(new KipoAssignmentTask());

        for (TaskBase taskBase : jobList) {
            taskBase.jobMain();
            System.out.println();
        }

    }
}
