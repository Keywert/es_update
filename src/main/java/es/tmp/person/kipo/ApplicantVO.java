package es.tmp.person.kipo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicantVO implements Cloneable{
    public String applicationNumber;
    public String applicationDate;
    public String country;
    public String seq;
    public String applicantCode;
    public String applicantName_ko;
    public String applicantName_en;
    public String inventionTitle_ko;
    public String inventionTitle_en;

    public String address;

    public String corporation_num;
    public String business_reg_num;


    public ApplicantVO clone() throws CloneNotSupportedException {
        ApplicantVO applicatnVO = (ApplicantVO) super.clone();
        applicatnVO.applicationNumber  = this.applicationNumber;
        applicatnVO.applicationDate  = this.applicationDate;
        applicatnVO.country  = this.country;
        applicatnVO.seq  = this.seq;
        applicatnVO.applicantCode  = this.applicantCode;
        applicatnVO.applicantName_ko  = this.applicantName_ko;
        applicatnVO.applicantName_en  = this.applicantName_en;
        applicatnVO.inventionTitle_ko  = this.inventionTitle_ko;
        applicatnVO.inventionTitle_en  = this.inventionTitle_en;
        return applicatnVO;
    }
}
