package es.tmp.person.kipo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.Applicant;
import es.model.bulk.Patent;
import es.module.datafarm.ESData;
import es.module.dataproc.norm.RepresentApn;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.kipris.KiprisAPI;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class KipoAdminName {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es13"));

        if (esConfigs.isEmpty())
            return;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String updateDate = dtf.format(localDate);

        KiprisAPI kiprisAPI = new KiprisAPI();
        RepresentApn representApn = new RepresentApn();
        DataFarmDB dataFarmDB = new DataFarmDB();

        String index = "kipo";
        String type = "patent";

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String startDate = "20230901";
        String upd_date = "2023-07-01";

        String endDate = "20231231";

        String tableName = "patent.KIPO_ADMIN_INT_HISTORY_KR";

        String query = "select distinct applicationNumber from " + tableName +
                " where " +
                " status is null and " +
                " (recvForwName like '%특허고객번호 정보변경%' or recvForwName like '%출원인%' )  " +
                " and recvForwDate  > '" + startDate + "'"
//              " and (recvForwDate  > '" + startDate + "' or upd_date > '" + upd_date + "' ) "
//                  " and status is null  "
//                 "  applicationNumber in ( " +
//                " '1020120072002','1020120072002','1020190009065','1020190009066','1020200168299','1020210019652'" +
//                " ) "
                ;

        System.out.println(query);

        Statement st = null;
        int count = 0;

        ESData esData = new ESData();
        List<QueueVO> queueVOList = new ArrayList<>();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> appNums = new HashSet<>();

            while (rs.next()) {
                String applicationNumber = rs.getString("applicationNumber");
                if (applicationNumber == null)
                    continue;

                appNums.add(applicationNumber);
            }

            if (appNums.isEmpty())
                return;

            System.out.println(appNums.size());
            ObjectMapper objectMapper = new ObjectMapper();
            for (String applicationNumber : appNums) {
                System.out.println(applicationNumber);
                if (++count % 10 == 0)
                    System.out.println(count);

//                if (count < 50000)
//                    continue;

                List<Patent> patents = esData.getPatent(esConfigs.get(0), index, applicationNumber);
                if (patents == null)
                    continue;

                String registerNumber = null;
                List<String> docIds = new ArrayList<>();
                for (Patent patent : patents) {
                    String id = patent.getDocumentId();
                    if (id != null)
                        docIds.add(id);

                    String rn = patent.getRegisterNumber();
                    if (rn != null)
                        registerNumber = rn;
                }

                System.out.println(applicationNumber + " : " + registerNumber);

                Map<String, List<Applicant>> additionalMap = new HashMap<>();
                List<Applicant> personList = new ArrayList<>();

                List<Applicant> cApplicants = kiprisAPI.patentApplicantInfo(applicationNumber);
                if (cApplicants != null) {
                    additionalMap.put("cApplicants", cApplicants);
                    personList.addAll(cApplicants);
                }

                if (registerNumber != null) {
                    List<Applicant> lastRightHolders = kiprisAPI.registrationLastRightHolderInfo(registerNumber);
                    if (lastRightHolders != null) {
                        additionalMap.put("lastRightHolders", lastRightHolders);
                        personList.addAll(lastRightHolders);
                    }
                }

                if (!personList.isEmpty())
                    additionalMap.put("personList", personList);

                Patent patent = patents.get(0);

                Map<String, Object> map = new HashMap<>();

                /// data
                patent.addData(additionalMap);
                List<Applicant> applicantInfo = patent.getApplicantInfo();
                if (applicantInfo != null) {
                    ArrayNode arrayNode = objectMapper.valueToTree(applicantInfo);
                    map.put("applicantInfo", arrayNode);
                }

                Applicant firstApplicantInfo = patent.getFirstApplicantInfo();
                if (firstApplicantInfo != null) {
                    ObjectNode objectNode = objectMapper.valueToTree(firstApplicantInfo);
                    map.put("firstApplicantInfo", objectNode);
                }

                List<Applicant> currentApplicantInfo = patent.getCurrentApplicantInfo();
                if (currentApplicantInfo != null) {
                    ArrayNode arrayNode = objectMapper.valueToTree(currentApplicantInfo);
                    map.put("currentApplicantInfo", arrayNode);
                }

                List<Applicant> rightHolderInfo = patent.getRightHolderInfo();
                if (rightHolderInfo != null) {
                    ArrayNode arrayNode = objectMapper.valueToTree(rightHolderInfo);
                    map.put("rightHolderInfo", arrayNode);
                }

                List<Applicant> currentRightHolderInfo = patent.getCurrentRightHolderInfo();
                if (currentRightHolderInfo != null) {
                    ArrayNode arrayNode = objectMapper.valueToTree(currentRightHolderInfo);
                    map.put("currentRightHolderInfo", arrayNode);
                } else {
                    if (patent.getDocumentId().matches("^jp.+$")) {
                        String patentType = patent.getPatentType();
                        if (patentType.matches("^(B|Y)$")) {
                            List<Applicant> applicantList = patent.getApplicantInfo();
                            if (applicantList != null) {
                                ArrayNode arrayNode = objectMapper.valueToTree(applicantList);
                                map.put("rightHolderInfo", arrayNode);
                                map.put("currentRightHolderInfo", arrayNode);
                            }
                        }
                    }
                }

                Applicant firstRightHolderInfo = patent.getFirstRightHolderInfo();
                if (firstRightHolderInfo != null) {
                    ObjectNode objectNode = objectMapper.valueToTree(firstRightHolderInfo);
                    map.put("firstRightHolderInfo", objectNode);
                }


                List<Applicant> representApplicantInfo = patent.getRepresentApplicantInfo();
                if (patent.getDocumentId().matches("^(kr|jp).+$")) {
                    representApplicantInfo = representApn.representApplicantInfo(conn, patent);
                    if (representApplicantInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
                        map.put("representApplicantInfo", arrayNode);
                    }
                }

//                if (representApplicantInfo != null) {
//                    ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
//                    map.put("representApplicantInfo", arrayNode);
//                } else {
//                    if (patent.getDocumentId().matches("^(kr|jp).+$")) {
//                        representApplicantInfo = representApn.representApplicantInfo(conn, patent);
//                        if (representApplicantInfo != null) {
//                            ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
//                            map.put("representApplicantInfo", arrayNode);
//                        }
//                    }
//                }

                map.put("personInfoUpdateDate",updateDate);

                String json = objectMapper.writeValueAsString(map);
                System.out.println(applicationNumber + " : " + json);
                for (ESConfig esConfig : esConfigs)
                    for (String docid : docIds) {
                        esConfig.bulkProcessor.add((new UpdateRequest(index, type, docid)
                                .doc(json.getBytes()))
                                .upsert(json.getBytes()));

                        QueueVO queueVO = new QueueVO();
                        queueVO.setEsindex("kipo");
                        queueVO.setDocumentId(docid);
                        queueVO.setLevel(4);

                        queueVOList.add(queueVO);
                        if (queueVOList.size() > 10) {
                            dataFarmDB.insertESQueue(queueVOList);
                            queueVOList.clear();
                        }
                    }
            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            if (!appNums.isEmpty())
                updateDB2(conn,appNums);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    private static void updateDB2(Connection conn, Set<String> apNums) {
        String tableName = "patent.KIPO_ADMIN_INT_HISTORY_KR";

        String query = "UPDATE " + tableName + " SET status= 9 " +
                "WHERE applicationNumber = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String apn : apNums) {
                preparedStatement.setString(1, apn);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
