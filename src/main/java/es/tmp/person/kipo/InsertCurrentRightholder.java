package es.tmp.person.kipo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.Applicant;
import es.model.bulk.Patent;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.module.utility.CountryCode;
import es.module.utility.NumberUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import es.module.kipris.KiprisAPI;

public class InsertCurrentRightholder {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        List<ESConfig> esConfigs = new ArrayList<>();

//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es13"));

        if (esConfigs.isEmpty())
            return;

        DataFarmDB dataFarmDB = new DataFarmDB();

        String index = "kipo";
        String type = "patent";

        KiprisApi kiprisApi = new KiprisApi();

        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
            LocalDate localDate = LocalDate.now();
            String updDate = dtf.format(localDate);

            String updateQuery01 = "update KIPRIS.LAST_RIGHT_HOLDER A inner join KIPRIS.RIGHT_HOLDER B on A.REGISTRATION_NUMBER = B.REGISTRATION_NUMBER" +
                    " set A.status = 2" +
                    " where A.status is null and B.RANK_NUMBER != 1 ";

            String updateQuery02 = "update KIPRIS.LAST_RIGHT_HOLDER set status = 3 where status is null";

            PreparedStatement preparedStmt = conn.prepareStatement(updateQuery01);
            preparedStmt.executeUpdate();
            System.out.println(updateQuery01);

            preparedStmt.clearParameters();

            preparedStmt = conn.prepareStatement(updateQuery02);
            preparedStmt.executeUpdate();
            System.out.println(updateQuery02);

            String tableName = "KIPRIS.LAST_RIGHT_HOLDER";
            String query = "select distinct REGISTRATION_NUMBER from " + tableName + " where "
                  +  " status = 2 "
//                    " LAST_RIGHT_HOLDER_NAME like '%일리아스%' ";
//                    + " REGISTRATION_NUMBER in ('1002501790000','1002502440000','1002504290000','1002504300000','1002504310000','1002504320000','1002504330000','1002505390000','1002505860000','1002509380000','1002518730000','1002518740000','1002518750000','1002518760000','1002521470000','1002524830000','1002525680000','1002526150000','1002526860000','1002530590000','1002531240000','1002536190000','1002537200000','1002539020000','1002540620000','1002541300000','1002543130000','1002547020000','1002547540000','1002547700000','1002548360000','1002548380000','1002551600000','1002552820000','1002553090000','1002553550000','1002554850000','1002560810000','1002562870000','1002565120000','1002570610000','1002571710000','1002572200000','1002573020000','1002573710000','1002574440000','1002579920000','1002580380000','1002580640000','1002582050000','1002583010000','1002584030000','1002588300000','1002588340000','1002592220000','1002593110000','1002594680000','1002597030000','1002600030000','1002601180000','1002601200000','1002604620000','1002604630000','1002605860000','1002605880000','1002612580000','1002613540000','1002615240000','1002615320000','1002615330000','1002616570000','1002616780000','1002617400000','1002617820000','1002617830000','1002619270000','1002619280000','1002621430000','1002621500000','1002622610000','1002624270000','1002627900000','1002629020000','1002629140000','1002629770000','1002632730000','1002632860000','1002633320000','1002633970000','1002634580000','1002635750000','1002635890000','1002638420000','1002638650000','1002641090000','1002642430000','1002642440000','1002642450000','1002644440000','1002644960000','1002645430000','1002646190000','1002647450000','1002647600000','1002654710000','1002654830000','1002655320000','1002655330000','1002655340000','1002656600000','1002659840000','1002661050000','1002662320000','1002665430000','1002669500000','1002671690000','1002675010000','1002676180000','1002676980000','1002679590000','1002679600000','1002680480000','1002681410000','1002681860000','1002685200000','1002685260000','1002685920000','1002686140000','1002686320000','1002686350000','1002686370000','1002689700000','1002690400000','1002691810000','1002694120000','1002695750000','1002696970000','1002697460000','1002697750000','1002698370000','1002698380000','1002700760000','1002701010000','1002703110000','1002704250000','1002708100000','1002713460000','1002714120000','1002715350000','1002720430000','1002721100000','1002721850000','1002721860000','1002721880000','1002721900000','1002724190000','1002726150000','1002727810000','1002730310000','1002730320000','1002736910000','1002737410000','1002737420000','1002737430000','1002738100000','1002739120000','1002740750000','1002741220000','1002742760000','1002743090000','1002743120000','1002744930000','1002745790000','1002745800000','1002745810000','1002747560000','1002758070000','1002758300000','1002759300000','1002760220000','1002760430000','1002760660000','1002760930000','1002761270000','1002762760000','1002768610000','1002775580000','1002778370000','1002780340000','1002781090000','1002782200000','1002783800000','1002785250000','1002786830000','1002787050000','1002787430000','1002789290000','1002790810000','1002791880000','1002792200000','1002792220000','1002794880000','1002796850000','1002797800000','1002798440000','1002800800000','1002800810000','1002802000000','1002809010000','1002809470000','1002811880000','1002813080000','1002813980000','1002817230000','1002817250000','1002822670000','1002824630000','1002824640000','1002825870000','1002828610000','1002828620000','1002830450000','1002833760000','1002834200000','1002834450000','1002835230000','1002835240000','1002835250000','1002838310000','1002838320000','1002838510000','1002838520000','1002838550000','1002840490000','1002841670000','1002841720000','1002841900000','1002845560000','1002845580000','1002845590000','1002848140000','1002849250000','1002851770000','1002852080000','1002852670000','1002854190000','1002854200000','1002854210000','1002854220000','1002854230000','1002854240000','1002858970000','1002862210000','1002864490000','1002866210000','1002866330000','1002866490000','1002866500000','1002874140000','1002874330000','1002875660000','1002875800000','1002876330000','1002878020000','1002878030000','1002880950000','1002882730000','1002886530000','1002886580000','1002888890000','1002888940000','1002890230000','1002891000000','1002891010000','1002893080000','1002894360000','1002898040000','1002898450000','1002899840000','1002900480000','1002901210000','1002903160000','1002904460000','1002905370000','1002905620000','1002906890000','1002907500000','1002915210000','1002920790000','1002925820000','1002926170000','1002927340000','1002927390000','1002931050000','1002932040000','1002932880000','1002935950000','1002935970000','1002937410000','1002938750000','1002938760000','1002940960000','1002943570000','1002944590000','1002945590000','1002945600000','1002945720000','1002946170000','1002948570000','1002949830000','1002950190000','1002951850000','1002954930000','1002955190000','1002956020000','1002957410000','1002958700000','1002959830000','1002960880000','1002968870000','1002968990000','1002969020000','1002969260000','1002971280000','1002971880000','1002972820000','1002972840000','1002972850000','1002973580000','1002975500000','1002979190000','1002982480000','1002983240000','1002988580000','1002989640000','1002991140000','1002994180000','1002994360000','1002995580000','1002997170000','1002998120000','1002998630000','1003002000000','1003006140000','1003006390000','1003006550000','1003007520000','1003007850000','1003012780000','1003012850000','1003013620000','1003015470000','1003019910000','1003020790000','1003020800000','1003022220000','1003025460000','1003026870000','1003028530000','1003030160000','1003031940000','1003032930000','1003034150000','1003036330000','1003038760000','1003039510000','1003040100000','1003041460000','1003041470000','1003042910000','1003042920000','1003044710000','1003046000000','1003046010000','1003047620000','1003048490000','1003051010000','1003051070000','1003053940000','1003054410000','1003054530000','1003054870000','1003059010000','1003059190000','1003059960000','1003060280000','1003060870000','1003062360000','1003062660000','1003067030000','1003069320000','1003069990000','1003071320000','1003073100000','1003073360000','1003073800000','1003075080000','1003075100000','1003075130000','1003077210000','1003079240000','1003079660000','1003079980000','1003083880000','1003084050000','1003084870000','1003086850000','1003086950000','1003088710000','1003096010000','1003096880000','1003103900000','1003104010000','1003104740000','1003106660000','1003106670000','1003107560000','1003108890000','1003111890000','1003113180000','1003118000000','1003118010000','1003118400000','1003118990000','1003119000000','1003119240000','1003120370000','1003120910000','1003121480000','1003123650000','1003124450000','1003126060000','1003126070000','1003127120000','1003128750000','1003129050000','1003130660000','1003131870000','1003132200000','1003132390000','1003134470000','1003137370000','1003137800000','1003138320000','1003142800000','1003142810000','1003142820000','1003144010000','1003146140000','1003147210000','1003148500000','1003148590000','1003148600000','1003148730000','1003148750000','1003149360000','1003149710000','1003150740000','1003153330000','1003159660000','1003160310000','1003160410000','1003161120000','1003161390000','1003161490000','1003162170000','1003162180000','1003162190000','1003162570000','1003164660000','1003164670000','1003166220000','1003167530000','1003170590000','1003171920000','1003174530000','1003175640000','1003176790000','1003177130000','1003180410000','1003181610000','1003181770000','1003182500000','1003185660000','1003190280000','1003190670000','1003190720000','1003191490000','1003193840000','1003199660000','1003201220000','1003201520000','1003202490000','1003202820000','1003203200000','1003203260000','1003205760000','1001076180000','1001095520000','1001095530000','1001120690000','1001137280000','1001153030000','1001161740000','1001162390000','1001174980000','1001174990000','1001175200000','1001191070000','1001200980000','1001222710000','1001276630000','1001281650000','1001286810000','1001287190000','1001296190000','1001313920000','1001325890000','1001340510000') "
//                    + " and REGISTRATION_NUMBER not in ('1011665790000') "
                    + " limit 300000";

            System.out.println(query);

            Statement st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);
            Map<String, ArrayList<Applicant>> map = new HashMap<>();

            Set<String> regSet = new HashSet<>();
            while (rs.next()) {
                String registerNumber = rs.getString("REGISTRATION_NUMBER");
                if (registerNumber != null)
                    regSet.add(registerNumber);
            }

            if (regSet.isEmpty())
                return;

            System.out.println("REG Size : " + regSet.size());
            ObjectMapper objectMapper = new ObjectMapper();

            List<QueueVO> queueVOList = new ArrayList<>();


            for (String registerNumber : regSet) {
//                List<Applicant> lastRightHolders = kiprisApi.registrationLastRightHolderInfo(registerNumber);
                List<Applicant> lastRightHolders = getlastRightHolderInfo(conn, registerNumber);
                if (lastRightHolders != null) {
//                    System.out.println("lastRightHolders : " + objectMapper.writeValueAsString(lastRightHolders));

                    List<Applicant> currentRightHolderInfo = new ArrayList<>();
                    for (Applicant person : lastRightHolders) {
                        String code = person.getCode();
                        if (code != null && !"".equals(code)) {
                            Applicant applicant = esGetPerson(esConfigs.get(0), code);
                            if (applicant != null) {
                                currentRightHolderInfo.add(applicant);
                            } else
                                currentRightHolderInfo.add(person);
                        } else
                            currentRightHolderInfo.add(person);
                    }


                    if (!currentRightHolderInfo.isEmpty()) {
                        Map<String, Object> jsonMap = new HashMap<>();
                        jsonMap.put("currentRightHolderInfo", currentRightHolderInfo);
                        jsonMap.put("rightHolderUpdateDate", updDate);
                        jsonMap.put("firstRightHolderInfo", currentRightHolderInfo.get(0));

                        String json = objectMapper.writeValueAsString(jsonMap);

                        Set<String> docIds = getDocumentIds(esConfigs.get(0), registerNumber);
//                        System.out.println(registerNumber);

                        if (docIds == null)
                            continue;

                        for (String id : docIds) {
                            System.out.println(id + " : " + json);

                            for (ESConfig esConfig : esConfigs)
                                esConfig.bulkProcessor.add(new UpdateRequest(index, type, id)
                                        .doc(json.getBytes())
                                        .upsert(json.getBytes()));


                                QueueVO queueVO = new QueueVO();
                                queueVO.setEsindex(index);
                                queueVO.setDocumentId(id);
                                queueVO.setLevel(5);
                                queueVOList.add(queueVO);

                        }
                    }
                }

                updateDB(conn, registerNumber);

                if (!queueVOList.isEmpty()) {
                    dataFarmDB.insertESQueue(queueVOList);
                    queueVOList.clear();
                }

            }


            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (SQLException | InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private static void updateDB(Connection conn, String registerNumber) {
        String query = "UPDATE KIPRIS.LAST_RIGHT_HOLDER SET status=1 " +
                " WHERE REGISTRATION_NUMBER = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            preparedStatement.setString(1, registerNumber);
            preparedStatement.addBatch();


            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static List<Applicant> getlastRightHolderInfo(Connection conn, String registerNumber) {
        String query = "select * from KIPRIS.LAST_RIGHT_HOLDER where REGISTRATION_NUMBER = '" + registerNumber + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<Applicant> lastRightHolders = new ArrayList<>();
            while (rs.next()) {
                Applicant lastRightholderVO = new Applicant();
                lastRightholderVO.setName(rs.getString("LAST_RIGHT_HOLDER_NAME"));
                lastRightholderVO.setCode(rs.getString("LAST_RIGHT_HOLDER_NUMBER"));
                lastRightholderVO.setAddress(rs.getString("LAST_RIGHT_HOLDER_ADDRESS"));
                String country = rs.getString("LAST_RIGHT_HOLDER_COUNTRY");
                country = CountryCode.countryCode.get(country);
                lastRightholderVO.setCountry(country);
                lastRightHolders.add(lastRightholderVO);
            }

            if (!lastRightHolders.isEmpty())
                return lastRightHolders;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Applicant esGetPerson(ESConfig esConfig, String code) {
        String index = "kipo";
        String type = "patent";

        String[] include = {"currentApplicantInfo"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("currentApplicantInfo.eName"))
                .filter(QueryBuilders.matchQuery("currentApplicantInfo.code", code));

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(include, null)
                .setSize(2)
                .addSort("patentDate", SortOrder.DESC)
                .execute()
                .actionGet();

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Patent patent = objectMapper.readValue(hit.getSourceAsString(), Patent.class);
                if (patent == null)
                    continue;

                List<Applicant> applicantList = patent.getCurrentApplicantInfo();
                if (applicantList == null)
                    continue;

                for (Applicant applicant : applicantList) {
                    String applicantCode = applicant.getCode();
                    if (applicantCode == null || "".equals(applicantCode))
                        continue;

                    if (applicantCode.equals(code)) {
                        String eName = applicant.geteName();
                        if (eName != null)
                            return applicant;
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void insertUpdateDB(Connection conn, String tableName, Map<String, ArrayList<Applicant>> map) {
        String query = "UPDATE " + tableName + " SET status=1 " +
                " WHERE REGISTRATION_NUMBER = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : map.keySet()) {
                preparedStatement.setString(1, docId);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateLastRightholder(List<ESConfig> esConfigs, Map<String, ArrayList<Applicant>> map) {
        String index = "kipo";
        String type = "patent";

        Iterator<String> iterator = map.keySet().iterator();

        ObjectMapper objectMapper = new ObjectMapper();

        ArrayList<String> documentId = new ArrayList<>();

        while (iterator.hasNext()) {
            String registerNumber = iterator.next().toString();

            ArrayList<Applicant> list = map.get(registerNumber);

            if (list == null)
                continue;

            if (list.size() == 0)
                continue;

            Map<String, Object> jsonMap = new HashMap<>();

            jsonMap.put("currentRightHolderInfo", list);
            String json = "";

            try {
                json = objectMapper.writeValueAsString(jsonMap);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            Set<String> docIds = getDocumentIds(esConfigs.get(0), registerNumber);

            for (String id : docIds) {
                System.out.println(id + " : " + json);

                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, id)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

            }
        }
    }

    private static Set<String> getDocumentIds(ESConfig esConfig, String registerNumber) {
        String index = "kipo";
        String type = "patent";

        boolean patentFlag = true;
        String docType = "[A|B][0-9]?";
        if (registerNumber.matches("^20.+")) {
            docType = "[U|Y][0-9]?";
            patentFlag = false;
        }
        String number = NumberUtils.getNumber("KR", registerNumber, "RN");

//        System.out.println(number + " : " +docType);

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.regexpQuery("documentType", docType))
                .filter(QueryBuilders.termsQuery("registerNumber", number));

        String[] fields = {"applicationNumber"};//"description"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

//        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        Set<String> documentIds = new HashSet<>();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            Object applicationNumber = hit.getSource().get("applicationNumber");

            if (applicationNumber == null)
                continue;

            if (patentFlag) {
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "a");
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "b1");
            } else {
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "u");
                documentIds.add("kr" + applicationNumber.toString().replaceAll("-", "") + "y1");
            }
        }

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(documentIds);

        scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        Set<String> docIdSet = new HashSet<>();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            docIdSet.add(hit.getId());
        }

        if (!docIdSet.isEmpty())
            return docIdSet;

        return null;
    }

}
