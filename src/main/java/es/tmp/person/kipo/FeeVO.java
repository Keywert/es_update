package es.tmp.person.kipo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeeVO {
    public String getPatentNumber() {
        if (this.patentNumber == null)
            return null;

        String number = this.patentNumber;
        if (number.matches("^(10|20)[0-9]{7}0000$"))
            return number.substring(0,2)+"-"+number.substring(2,9);

        return patentNumber;
    }

    public String patentNumber;
    public String applicationNumber;
    public String registrationDate;
    public Integer start;
    public String paymentFee;
    public Integer last;


    public Integer degree;

    public String paymentDate;

    public String calcPaymentDate() {
        try {
            if (this.start != null && this.last != null) {
                int year = last - start;
                if (year == 0)
                    year = 1;

                if (this.paymentDate != null)
                    return addDate(this.paymentDate, year, 0, 0);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String addDate(String strDate, int year, int month, int day) throws Exception {

        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyyMMdd");

        Calendar cal = Calendar.getInstance();

        Date dt = dtFormat.parse(strDate);

        cal.setTime(dt);

        cal.add(Calendar.YEAR, year);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.DATE, day);

        return dtFormat.format(cal.getTime());
    }

    public String getLastText() {
        if (this.last != null)
            return String.valueOf(this.last) + "년차 납입완료";

        return null;
    }
}
