package es.tmp.person.kipo;

import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.tmp.ESHealthCheck;
import org.elasticsearch.action.update.UpdateRequest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Map;

public class TaskBase {
    public String index;
    public Integer level;
    public String taskName;

    public int jobMain() {
        return 0;
    }



    public boolean esHealthCheck(ESConfig esConfig) {
        if (new ESHealthCheck().healthCheck2(esConfig))
            return true;

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new ESHealthCheck().healthCheck2(esConfig);
    }

    public void step() {
    }

    public void updateStep(ESConfig esConfig, DataFarmDB dataFarmDB) {
    }

    public void create() {
    }

    public String getDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }

    public int getUpdateCount() {
        return 0;
    }

    public String getIndex() {
        return null;
    }

    public void updateES(ESConfig esConfig, String index, String type, Map<String, String> map) {
        if (map == null)
            return;
        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            String docId = iterator.next();
            String json = map.get(docId);

            esConfig.bulkProcessor.add(new UpdateRequest(index, type, docId)
                    .doc(json.getBytes())
                    .upsert(json.getBytes()));

        }
    }

}
