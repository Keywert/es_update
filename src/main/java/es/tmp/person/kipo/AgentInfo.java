package es.tmp.person.kipo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.config.ESConfig;
import es.module.utility.MakeDocID;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.config.DBHelper;
import es.model.Applicant;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AgentInfo {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

        String tableName = "KIPRIS.bibliographyPersonsInfo";

        List<ESConfig> esConfigs = new ArrayList<>();

//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es205"));
//        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es188"));

        DataFarmDB dataFarmDB = new DataFarmDB();

        if (esConfigs.isEmpty())
            return;

        String index = "kipo";
        String type = "patent";

        String start = "20230101";
        String end = "20241231";

        String[] fields = {"applicationNumber", "documentType"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.rangeQuery("patentDate").from(start).to(end))
//                .filter(QueryBuilders.existsQuery("agentInfo"))
                .mustNot(QueryBuilders.existsQuery("agentInfo.address"));

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();
        List<QueueVO> queueVOList = new ArrayList<>();

        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 100 == 0)
                        System.out.println(count);

                    String docId = hit.getId();
                    String applicationNumber = "";
                    if (docId.matches("^kr[0-9]{11}[a|b][0-9]?$"))
                        applicationNumber = "10" + docId.substring(2, 13);
                    else
                        applicationNumber = "20" + docId.substring(2, 13);

                    List<Applicant> agentInfo = kiprisAgentInfo(conn, tableName, applicationNumber);
                    if (agentInfo != null) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("agentInfo", agentInfo);
                        List<String> docIds = MakeDocID.getDocumentIds(esConfigs.get(0),index, applicationNumber);
                        String json = objectMapper.writeValueAsString(map);

                        if (docIds == null)
                            continue;

                        for (String id : docIds) {
//                            System.out.println(id + " : " + json);
                            for (ESConfig esConfig : esConfigs) {
                                esConfig.bulkProcessor.add(new UpdateRequest(index, "patent", id)
                                        .doc(json.getBytes())
                                        .upsert(json.getBytes()));

                            }

                            QueueVO queueVO = new QueueVO();
                            queueVO.setEsindex(index);
                            queueVO.setDocumentId(id);
                            queueVO.setLevel(3);

                            queueVOList.add(queueVO);
                            if (queueVOList.size() > 10) {
                                dataFarmDB.insertESQueue(queueVOList);
                                queueVOList.clear();
                            }
                        }
                    }
                }
                scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static List<Applicant> kiprisAgentInfo(Connection conn, String tableName, String appNum) {
        if (appNum == null)
            return null;

//        if (appNum.matches("^(10|20)(19|20)[0-9]{2}70[0-9]{5}$")) {
////            System.out.println(appNum);
//            appNum = appNum.substring(0, 6) + "07" + appNum.substring(8);
////            System.out.println(appNum);
//        }else
//            return null;


        String query = "select * from " + tableName
                + " where applicationNumber ='" + appNum + "' " +
                " and personType = '대리인' and seq1 = (select max(seq1) from " + tableName + " where personType = '대리인' and applicationNumber ='" + appNum + "' ) ";

//        System.out.println(query);

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            List<Applicant> applicants = new ArrayList<>();

            while (rs.next()) {
                Applicant applicant = new Applicant();

                Integer seq2 = rs.getInt("seq2");
                if (seq2 != null)
                    applicant.setSequence(seq2.toString());

                String patentCustomerNumber = rs.getString("patentCustomerNumber");
                if (patentCustomerNumber != null)
                    applicant.setCode(patentCustomerNumber);

                String korName = rs.getString("korName");
                if (korName != null)
                    applicant.setName(korName);

                String engName = rs.getString("engName");
                if (engName != null)
                    applicant.setEnName(engName);

                String address = rs.getString("address");
                if (address != null)
                    applicant.setAddress(address);

                String country = rs.getString("country");
                if (country != null)
                    applicant.setCountry(country);


                if (applicant.getName() != null)
                    applicants.add(applicant);

            }

            if (!applicants.isEmpty())
                return applicants;


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }
}
