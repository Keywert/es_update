package es.tmp.keylense;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Drawing {
    public Boolean symbolFlag;
    public String insertDate;

    public String drawingId;
    public String countryCode;
    public String documentType;
    public String documentKind;
    public String documentNumber;
    public String linkedDocumentId;

    public String preDrawingId;

//    public DrawingSymbolMeta drawingSymbolMeta;

    public void setSymbolDescMap(Map<String, String> symbolDescMap) {
        if (symbolDescMap !=null && drawingSymbols != null) {
            List<DrawingSymbol> newDrawingSymbols = new ArrayList<>();
            for (DrawingSymbol drawingSymbol : drawingSymbols) {
                if (drawingSymbol.getSymbols() != null) {
                    List<Symbol> newSymbol = new ArrayList<>();
                    for (Symbol symbol : drawingSymbol.getSymbols()) {
                        symbol.setSymbolDesc(symbolDescMap.get(symbol.getSymbol()));
                        newSymbol.add(symbol);
                    }
                    drawingSymbol.setSymbols(newSymbol);
                }
                newDrawingSymbols.add(drawingSymbol);
            }
            drawingSymbols = newDrawingSymbols;
        }

        this.symbolDescMap = null;
    }

    Map<String,String> symbolDescMap;

    Set<String> symbolSet;
    List<DrawingSymbol> drawingSymbols;

    public List<DrawingVO> drawings;                      // 도면


}
