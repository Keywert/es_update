package es.tmp.keylense;

public class DrawingSymbolVO {

    private String documentId;
    private String countryCode;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String imgFile;
    private String imgName;
    private int width;
    private int height;
    private int posX1;
    private int posY1;
    private int posX2;
    private int posY2;
    private int posX3;
    private int posY3;
    private int posX4;
    private int posY4;
    private String symbol;
    private String symbolDesc;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentKind() {
        return documentKind;
    }

    public void setDocumentKind(String documentKind) {
        this.documentKind = documentKind;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getImgFile() {
        return imgFile;
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName.toLowerCase();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getPosX1() {
        return posX1;
    }

    public void setPosX1(int posX1) {
        this.posX1 = posX1;
    }

    public int getPosY1() {
        return posY1;
    }

    public void setPosY1(int posY1) {
        this.posY1 = posY1;
    }

    public int getPosX2() {
        return posX2;
    }

    public void setPosX2(int posX2) {
        this.posX2 = posX2;
    }

    public int getPosY2() {
        return posY2;
    }

    public void setPosY2(int posY2) {
        this.posY2 = posY2;
    }

    public int getPosX3() {
        return posX3;
    }

    public void setPosX3(int posX3) {
        this.posX3 = posX3;
    }

    public int getPosY3() {
        return posY3;
    }

    public void setPosY3(int posY3) {
        this.posY3 = posY3;
    }

    public int getPosX4() {
        return posX4;
    }

    public void setPosX4(int posX4) {
        this.posX4 = posX4;
    }

    public int getPosY4() {
        return posY4;
    }

    public void setPosY4(int posY4) {
        this.posY4 = posY4;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbolDesc() {
        return symbolDesc;
    }

    public void setSymbolDesc(String symbolDesc) {
        this.symbolDesc = symbolDesc;
    }

    @Override
    public String toString() {
        return "DrawingSymbolVO{" +
                "documentId='" + documentId + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", documentType='" + documentType + '\'' +
                ", documentKind='" + documentKind + '\'' +
                ", documentNumber='" + documentNumber + '\'' +
                ", imgFile='" + imgFile + '\'' +
                ", imgName='" + imgName + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", posX1=" + posX1 +
                ", posY1=" + posY1 +
                ", posX2=" + posX2 +
                ", posY2=" + posY2 +
                ", posX3=" + posX3 +
                ", posY3=" + posY3 +
                ", posX4=" + posX4 +
                ", posY4=" + posY4 +
                ", symbol='" + symbol + '\'' +
                ", symbolDesc='" + symbolDesc + '\'' +
                '}';
    }

}
