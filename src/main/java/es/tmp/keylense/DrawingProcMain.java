package es.tmp.keylense;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class DrawingProcMain {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es18"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es43"));

        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));

        ESConfig esPatent = new ESConfig("es13");

        DBHelper dbHelper = new DBHelper();
        long start = System.currentTimeMillis();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now().minus(Period.ofDays(10));
        System.out.println(localDate);
        Connection conn = dbHelper.getConn_aurora2();

        String table_list = "drawingsymbols.TABLE_LIST";

        String query = "select * from " + table_list
//                + " where country_code in ('KR') and status is null  "
                + " where country_code not in ('CN','EP') and status is null  "
//                + " where table_name in ('USGRANT_REDBOOK_20230919_I20230919_resized_jpg') "
//                + " and table_name not like 'PAJ%'"
                + " order by ins_date limit 200";
        System.out.println(query);

        Statement st = null;
        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (++count % 100 == 0) {
                    System.out.println(count);
                }

                String table_name = rs.getString("table_name");
                if (table_name == null)
                    continue;

                String country_code = rs.getString("country_code");
                if (country_code == null)
                    continue;


                Statement st1 = null;
                Statement st2 = null;

                String m_table_name = "drawingsymbols.`" + table_name + "`";

                System.out.println(table_name);

                try {

                    String getQuery = "select country_code,document_type,document_kind,document_number from " + m_table_name
                            + " WHERE symbol_desc is null  "
                            + " group by country_code,document_type,document_kind,document_number"
                            + " limit 1000000 ";

                    System.out.println(getQuery);

                    st1 = conn.createStatement();
                    st1.setFetchSize(100);
                    st2 = conn.createStatement();
                    st2.setFetchSize(100);

                    ResultSet rs1 = st1.executeQuery(getQuery);
                    Map<String, DocInfo> map = new HashMap<>();

                    while (rs1.next()) {
                        if (++count % 1000 == 0)
                            System.out.println(count);

                        String country_code1 = rs1.getString("country_code");
                        if (country_code1 == null)
                            continue;

                        String document_number1 = rs1.getString("document_number");
                        if (document_number1 == null)
                            continue;

                        String document_type1 = rs1.getString("document_type");
                        if (document_type1 == null)
                            continue;

                        String document_kind1 = rs1.getString("document_kind");


                        DocInfo docInfo = new DocInfo();
                        docInfo.setCountryCode(country_code1);
                        docInfo.setDocumentNumber(document_number1);
                        docInfo.setDocumentType(document_type1);
                        docInfo.setDocumentKind(document_kind1);

                        if (country_code.matches("US"))
                            map.put(document_number1, docInfo);
                        else
                            map.put(document_number1 + document_type1.toLowerCase(), docInfo);

//                        System.out.println(country_code1 + " - " + document_type1 + " - " + document_number1);

                        if (map.size() > 100) {
                            List<DocInfo> docInfos = getDocIds(esPatent, country_code, map);

                            System.out.println("docInfos len : " + docInfos.size());
                            if (!docInfos.isEmpty()) {
                                insertES(esConfigs, st2, country_code, m_table_name, docInfos);
                            }

//                            updateDB(conn, m_table_name, map);

                            map.clear();
                        }
                    }

                    if (!map.isEmpty()) {
                        List<DocInfo> docInfos = getDocIds(esPatent, country_code, map);

                        if (!docInfos.isEmpty()) {
                            insertES(esConfigs, st2, country_code, m_table_name, docInfos);
                        }

//                        updateDB(conn, m_table_name, map);
                    }


                    System.out.println(count);
                    long end = System.currentTimeMillis();
                    System.out.println("실행 시간 : " + (end - start) / 1000.0 + " 평균 시간 : " + ((end - start) / 1000.0) / count);
                    System.out.println("Finish all work~~!");


                } catch (SQLException e) {
                    e.printStackTrace();
                }


                updateTableList(conn, table_list, table_name);

            }

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            System.out.println(count);

        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void updateTableList(Connection conn, String tableName, String table_name) {
        String query = "UPDATE " + tableName + " SET status = 1" +
                " WHERE table_name = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            preparedStatement.setString(1, table_name);

            preparedStatement.addBatch();

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateDB(Connection conn, String tableName, Map<String, DocInfo> map) {
        String query = "UPDATE " + tableName + " SET symbol_desc = 'Y'" +
                " WHERE country_code = ? and document_type = ? and document_kind = ? and document_number = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (DocInfo docInfo : map.values()) {
                String document_number = docInfo.getDocumentNumber();
                String country_code = docInfo.getCountryCode();
                String document_type = docInfo.getDocumentType();
                String document_kind = docInfo.getDocumentKind();

                preparedStatement.setString(1, country_code);
                preparedStatement.setString(2, document_type);
                preparedStatement.setString(3, document_kind);
                preparedStatement.setString(4, document_number);

                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertES(List<ESConfig> esConfigs, Statement st2, String cc, String symbolTable, List<DocInfo> docInfos) {
        ObjectMapper objectMapper = new ObjectMapper();

        String index = "";
        if (cc.matches("(kipo|KR)"))
            index = "drawing_kipo";
        else if (cc.matches("(jpo|JP)"))
            index = "drawing_jpo";
        else if (cc.matches("(uspto|US)"))
            index = "drawing_uspto";
        else if (cc.matches("(sipo|CN)"))
            index = "drawing_sipo";
        else if (cc.matches("(epo|EP)"))
            index = "drawing_epo";
        else
            System.out.println("index not found : " + cc);

        if (index.isEmpty())
            return;

        String type = "patent";

        for (DocInfo docInfo : docInfos) {
            String docId = docInfo.getDocumentId();
            String document_number1 = docInfo.getDocumentNumber();
            String country_code1 = docInfo.getCountryCode();
            String document_type1 = docInfo.getDocumentType();
            String document_kind1 = docInfo.getDocumentKind();

            String innerQuery = "select * from " + symbolTable
                    + " where country_code = '" + country_code1 + "' "
                    + " and document_type = '" + document_type1 + "' "
                    + " and document_kind = '" + document_kind1 + "' "
                    + " and document_number = '" + document_number1 + "' ";

//            System.out.println(innerQuery);

            try {

                ResultSet rs = st2.executeQuery(innerQuery);

                Set<String> symbolSet = new HashSet<>();
                Map<String, DrawingSymbol> drawingSymbolMap = new HashMap<>();

                Drawing drawing = null;

                while (rs.next()) {
                    String country_code = rs.getString("country_code");
                    if (country_code == null)
                        continue;

                    String document_number = rs.getString("document_number");
                    if (document_number == null)
                        continue;

                    String document_type = rs.getString("document_type");
                    if (document_type == null)
                        continue;

                    String document_kind = rs.getString("document_kind");
                    if (document_kind == null)
                        continue;

                    String relative_dir = rs.getString("relative_dir");
                    if (relative_dir == null)
                        continue;

                    String drawing_name = rs.getString("drawing_name");
                    if (drawing_name == null)
                        continue;

//                    System.out.println(country_code + " " + document_number + " " + drawing_name);

                    String IMG_FILE = "/" + relative_dir + "/" + drawing_name;

                    Integer pos_x1 = rs.getInt("pos_x1");
                    if (pos_x1 == null)
                        pos_x1 = 0;

                    Integer pos_y1 = rs.getInt("pos_y1");
                    if (pos_y1 == null)
                        pos_y1 = 0;

                    Integer pos_x2 = rs.getInt("pos_x2");
                    if (pos_x2 == null)
                        pos_x2 = 0;

                    Integer pos_y2 = rs.getInt("pos_y2");
                    if (pos_y2 == null)
                        pos_y2 = 0;

                    Integer pos_x3 = rs.getInt("pos_x3");
                    if (pos_x3 == null)
                        pos_x3 = 0;

                    Integer pos_y3 = rs.getInt("pos_y3");
                    if (pos_y3 == null)
                        pos_y3 = 0;

                    Integer pos_x4 = rs.getInt("pos_x4");
                    if (pos_x4 == null)
                        pos_x4 = 0;

                    Integer pos_y4 = rs.getInt("pos_y4");
                    if (pos_y4 == null)
                        pos_y4 = 0;


                    String symbol = rs.getString("symbol");
                    if (symbol != null && !"".equals(symbol))
                        symbolSet.add(symbol);

//                    System.out.println(symbol);

//                    String symbol_desc = rs.getString("symbol_desc");

                    Symbol symbolVO = new Symbol();
                    symbolVO.setPosX1(pos_x1);
                    symbolVO.setPosY1(pos_y1);
                    symbolVO.setPosX2(pos_x2);
                    symbolVO.setPosY2(pos_y2);
                    symbolVO.setPosX3(pos_x3);
                    symbolVO.setPosY3(pos_y3);
                    symbolVO.setPosX4(pos_x4);
                    symbolVO.setPosY4(pos_y4);
                    symbolVO.setPosY2(pos_y2);
                    symbolVO.setSymbol(symbol);
//                    symbolVO.setSymbolDesc(symbol_desc);

                    if (drawing == null) {
                        drawing = new Drawing();

                        String drawingId = "kdw_" + docId;
                        drawing.setDrawingId(drawingId);
                        drawing.setDocumentNumber(document_number);
                        drawing.setLinkedDocumentId(docId);
                        drawing.setCountryCode(country_code);
                        drawing.setDocumentType(document_type);
                        drawing.setDocumentKind(document_kind);

                        drawing.setSymbolFlag(false);
                        drawing.setInsertDate(getDate());

                        DrawingSymbol drawingSymbol = new DrawingSymbol();
                        drawingSymbol.setImgFileOrigin(IMG_FILE);

                        List<Symbol> symbols = new ArrayList<>();
                        symbols.add(symbolVO);
                        drawingSymbol.setSymbols(symbols);
                        drawingSymbolMap.put(IMG_FILE, drawingSymbol);
                    } else {
                        DrawingSymbol drawingSymbol = drawingSymbolMap.get(IMG_FILE);
                        if (drawingSymbol != null) {
                            List<Symbol> symbols = drawingSymbol.getSymbols();
                            symbols.add(symbolVO);
                            drawingSymbol.setSymbols(symbols);
                        } else {
                            drawingSymbol = new DrawingSymbol();
                            drawingSymbol.setImgFileOrigin(IMG_FILE);
                            List<Symbol> symbols = new ArrayList<>();
                            symbols.add(symbolVO);
                            drawingSymbol.setSymbols(symbols);
                        }

                        drawingSymbolMap.put(IMG_FILE, drawingSymbol);
                    }
                }

                if (drawing != null) {
                    if (!symbolSet.isEmpty())
                        drawing.setSymbolSet(symbolSet);

                    if (!drawingSymbolMap.isEmpty()) {
                        List<DrawingSymbol> drawingSymbols = new ArrayList<>();
                        drawingSymbols.addAll(drawingSymbolMap.values());
                        drawing.setDrawingSymbols(drawingSymbols);
                    }

                    String json = objectMapper.writeValueAsString(drawing);
//                    System.out.println(drawing.getDrawingId() + " : "+ json);
                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new UpdateRequest(index, type, drawing.getDrawingId())
                                .doc(json.getBytes())
                                .upsert(json.getBytes()));

                }

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    private static List<DocInfo> getDocIds(ESConfig esPatent, String country_code, Map<String, DocInfo> map) {
        if (map.isEmpty())
            return null;

        List<DocInfo> list = new ArrayList<>();

        if (country_code.matches("(KR|kipo)")) {
            for (DocInfo docInfo : map.values()) {
                String docId = "kr" + docInfo.getDocumentNumber().replaceAll("-", "") + docInfo.getDocumentKind();
                docId = docId.toLowerCase();

                if (docId.matches("^kr(19|20)[0-9]{9}[a|b|u|y][0-9]?$"))
                    docInfo.setDocumentId(docId);

                list.add(docInfo);
            }

            return list;
        } else if (country_code.matches("(JP|jpo)")) {
            return getJPDocIds(esPatent, map);
        } else if (country_code.matches("(US|uspto)")) {
            return getUSDocIds(esPatent, map);
        }

        return null;
    }

    private static List<DocInfo> getUSDocIds(ESConfig esPatent, Map<String, DocInfo> map) {
        String index = "uspto";
        String type = "patent";
        String[] fields = {"documentId", "registerNumber", "publicationNumber", "documentType", "openNumber"};

        Set<String> pubNums = new HashSet<>();
        for (DocInfo docInfo : map.values()) {
            pubNums.add(docInfo.getDocumentNumber());
        }

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .should(QueryBuilders.termsQuery("registerNumber", pubNums))
                .should(QueryBuilders.termsQuery("publicationNumber", pubNums));

        SearchResponse scrollResp = esPatent.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object docNum = hit.getSource().get("registerNumber");
                if (docNum == null)
                    docNum = hit.getSource().get("publicationNumber");

                DocInfo docInfo = map.get(docNum.toString());
                if (docInfo != null) {
                    docInfo.setDocumentId(hit.getId());
                    map.put(docNum.toString(), docInfo);
                }
            }
            scrollResp = esPatent.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        return new ArrayList<>(map.values());
    }


    private static List<DocInfo> getJPDocIds(ESConfig esPatent, Map<String, DocInfo> map) {
        String index = "jpo";
        String type = "patent";
        String[] fields = {"documentId", "registerNumber", "publicationNumber", "documentType", "documentTypeJP", "announcementNumber"};


        Set<String> pubNums = new HashSet<>();
        for (String docNum : map.keySet()) {
            String number = docNum.replaceAll("[A-z][0-9]?$", "");

            if (docNum.contains("s")) {
                if (number.matches("^(19|20)[0-9]{8}$"))
                    number = "WO" + number.substring(0, 4) + "/" + number.substring(4);
            } else {
                if (number.matches("^(19|20)[0-9]{8}$"))
                    number = number.substring(0, 4) + "-" + number.substring(4);
                else if (number.matches("^0[0-9]{7}$"))
                    number = number.substring(1);
            }
            pubNums.add(number);
        }


        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .should(QueryBuilders.termsQuery("registerNumber", pubNums))
                .should(QueryBuilders.termsQuery("announcementNumber", pubNums))
                .should(QueryBuilders.termsQuery("publicationNumber", pubNums));


        SearchResponse scrollResp = esPatent.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();


        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
//                System.out.println(hit.getSourceAsString());
                Object docNum = hit.getSource().get("announcementNumber");
                if (docNum == null)
                    docNum = hit.getSource().get("registerNumber");

                if (docNum == null)
                    docNum = hit.getSource().get("publicationNumber");

                Object docType = hit.getSource().get("documentTypeJP");
                if (docType == null)
                    continue;

                String key = docNum.toString();

                if (key.matches("^[0-9]{7}$"))
                    key = "0" + key;

                key = key.replaceAll("[^0-9]", "") + docType.toString();
                key = key.toLowerCase();

                DocInfo docInfo = map.get(key);
                if (docInfo != null) {
                    docInfo.setDocumentId(hit.getId());
                    map.put(key, docInfo);
                }

//                System.out.println(hit.getId() + " : " + key);
            }
            scrollResp = esPatent.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

//        try {
//            System.out.println("df" + new ObjectMapper().writeValueAsString(map));
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

        return new ArrayList<>(map.values());
    }


    private static String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
        Date date = new Date();
        String today = simpleDateFormat.format(date);
        return today;
    }

}
