package es.tmp.keylense;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DrawingVO implements Serializable {
    private String drawingId;           // 이미지 id
    private String drawingNumber;       // 이미지 id - 일본 데이터 일부
    private String imgFile;             // 이미지 경로
    private String imgId;               // 이미지 id 2
    private String imgHe;               // 이미지 높이
    private String imgWi;               // 이미지 파일
    private String imgAlt;              // 파일
    private String inline;              // 파일
    private String orientation;         // 파일
    private String original;
    private boolean visible = true;     // 보여주기 여부 - 미국 데이터에 의해서 시작

    public String getFigureNumber() {
        if (!StringUtils.isEmpty(this.drawingNumber))
            return this.drawingNumber;
        else if (!StringUtils.isEmpty(this.drawingId)) {
            // 표기상 길경우 문제가 발생 특히 미국
            if (this.drawingId.length() < 8)
                return this.drawingId;
            else
                return null;
        }

        return null;
    }
}
