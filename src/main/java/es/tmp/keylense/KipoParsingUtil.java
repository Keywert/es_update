package es.tmp.keylense;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class KipoParsingUtil {

    public static List<String> getNonBracketTagFilter(String xml, String baseTag) {
        xml = xml.replace(">", "> ")
                .replace("<", " <")
                .replace("\\n", "\\\\n ")
                .replace("\\r", "\\\\r ")
                .replace(":", " : ")
                .replace("：", " ： ")
                .replace(";", " ; ")
                .replace(",", " , ")
                .replace(".", " . ");

        Document doc = Jsoup.parse(xml, "", Parser.xmlParser());  // String -> XML

        return doc.select(baseTag).stream()
                .map(Element::text)
                .collect(Collectors.toList());
    }

    /**
     * 공백단위로 짜른 단어의 빈도수를 체크하여 가장 많은 빈도수의 단어만 리턴
     **/
    public static Map<String, String> wordRanklFilter2(String symbol, List<String> parsList, boolean retryFlag) {
        Map<String, String> rtnMap = new HashMap();

        Map<String, Integer> countMap = new HashMap<>();
        Map<String, Integer> maxLengthMap = new HashMap<>();

        String[] arrRemoveWord = {"제","도","의"};
        String[] arrRemoveMatchesWord = {"도[0-9]{1,2}", "도 [0-9]{1,2}", "도 [0-9]{1,2}의", "도[0-9]{1,2}의", "표 [0-9]{1,2}의", "표[0-9]{1,2}의", "실시예 [0-9]{1,2}"
                ,"실시예[0-9]{1,2}", "실시예 [0-9]{1,2}의", "실시예[0-9]{1,2}의", "[0-9]{1,2}의"};

        /* 단어별 빈도수 카운트 */
        for (String parsStr : parsList) {
            String[] temp = parsStr.split("\\|");
            String symbolStr = temp[0];
            String symbolDescStr = temp[1];

            if (symbol.equals(symbolStr)) {
                Integer count = countMap.get(symbolDescStr);
                countMap.put(symbolDescStr, count == null ? 1 : count + 1);
            }

        }

//        System.out.println("[WORD RANK COUNT]["+retryFlag+"]["+symbol+"]"+countMap);

        Collection values = countMap.values();
        if (values.isEmpty()) {
            rtnMap.put(symbol, "");
            return rtnMap;
        }

        int maxValue = (int) Collections.max(values);

        /* 빈도수가 가장 많은 것만 추출하면서 단어별 길이 담기 */
        for (Map.Entry<String, Integer> elem : countMap.entrySet()) {
            if (maxValue == elem.getValue()) {
                maxLengthMap.put(elem.getKey(), elem.getKey().length());
            }
        }
        String str;

        /* 단어별 길이가 가장 긴 것으로 오름차순 정렬 */
        List<String> maxKeySetList = new ArrayList(maxLengthMap.keySet());
        maxKeySetList.sort((o1, o2) -> (maxLengthMap.get(o2).compareTo(maxLengthMap.get(o1))));

        /*
         * 한번만 추출된 단어처리
         * 기본 두 단어로 함 / 두번째 단어의 끝이 '은는이가의에게와를을'인 경우에는 첫번째 단어만 선택
         * */
        if (maxValue == 1) {
            String[] arrWordStr = maxKeySetList.get(0).split(" ");
            String resultWordStr = arrWordStr[arrWordStr.length - 1];

            if (arrWordStr.length - 2 > -1) {
                String wordStr = arrWordStr[arrWordStr.length - 2];
                //끝 문자열이 아래와 같으면 제외 시킴
                String[] arrFirstWord = {"은", "는", "이", "가", "의", "게", "에", "와", "를", "을"};
                for (String setWord : arrFirstWord) {
                    if (wordStr.length() >= 1) {
                        String listWord = wordStr.substring(wordStr.length() - 1);

                        if (listWord.lastIndexOf(setWord) > -1) {
                            wordStr = "";
                        }
                    }

                }
                resultWordStr = wordStr + " " + resultWordStr;
            }

            if (retryFlag) {
                str = resultWordStr.trim();
            } else {
                str = "";
            }
        } else {
            /* 한번 이상 반복되면서
             *  가장 많은 빈도수
             *  가장 긴 구성어를 추출
             * */
            str = maxKeySetList.get(0);
        }

        //한단어에 지워야할 단어체크
        for (String removeWord : arrRemoveWord) {
            if (str.equals(removeWord)) {
                str = "";
            }
        }
        for (String removeMatchesWord : arrRemoveMatchesWord) {
            if (str.matches(removeMatchesWord)) {
                str = "";
            }
        }
        str = str.replace("<claim>","").replace("<ClaimText>","");

        rtnMap.put(symbol, str);

        return rtnMap;
    }

    /**
     * 공백단위로 짜른 단어의 빈도수를 체크하여 가장 많은 빈도수의 단어만 리턴
     **/
    public static Map<String, String> nonBracketWordRanklFilter(String symbol, List<String> parsList) {
        Map<String, String> rtnMap = new HashMap();

        Map<String, Integer> countMap = new HashMap<>();
        Map<String, Integer> maxLengthMap = new HashMap<>();
        String[] arrRemoveWord = {"제","도","의"};

        // 2. 구성어의 가장 앞단어가 불용어이면 제외
        String[] arrStopWord = {"상기", "및", "바람직", "것도", "것으로", "것은", "것을", "것이", "것이다"
                , "게다가", "곧", "그", "그까짓", "그깟", "그나마", "그냥", "그다지", "그래", "그래도", "그래서"
                , "그래야", "그러고", "그러나", "그러다", "그러다가", "그러면", "그러므로", "그러자", "그럭저럭", "그런"
                , "그런데", "그럼", "그럼에도", "그렇게", "그렇니까", "그렇다면", "그렇지만", "그리", "그리고"
                , "그야말로", "그외", "그저", "그제서야", "더구나", "더군다나", "됐다", "되게", "되고", "되는"
                , "되는대로", "되도록", "되레", "되려", "되어", "되었다", "된다", "또", "또는", "또다시", "또한"
                , "물론", "아마도", "아울러", "왜냐하면", "있다", "있어", "있어서", "있었다", "있으며", "있을"
                , "이루어진", "있는", "구성하는", "위한", "형성하는", "하는", "형성된", "일어나는", "제공하는", "구비하는"
                , "추가로", "포함하는", "특징으로", "특징으로하는", "나타내는", "가능한", "설치되는", "이다", "의해"
                , "의한", "의하여", "있으나", "된", "적어도", "어느", "돌려서", "내지", "통하여", "통해", "따라", "혹은", "는"
                , "위하여", "않아서", "않아", "않고", "않으며", "따르면", "하였다", "따라서", "위해", "있고", "있어", "있으며"
                , "바와" ,"같이", "같은", "있도록", "한다", "가지며", "이때", "사이에" ,"따른", "대한", "만들어", "경우", "경우에"
                , "이를", "대하여", "대략", "토록", "가지며", "여전히", "예시적으로", "함으로써", "만큼", "여기서", "하고"};

        String[] arrContainStopWord = {"하는", "바람직", "한다", "하여", "되면", "되는", "거나", "시켜", "함으로써", "만큼", "하고"};
        String[] arrRemoveMatchesWord = {"도[0-9]{1,2}", "도 [0-9]{1,2}", "도 [0-9]{1,2}의", "도[0-9]{1,2}의", "표 [0-9]{1,2}의", "표[0-9]{1,2}의", "실시예 [0-9]{1,2}"
                ,"실시예[0-9]{1,2}", "실시예 [0-9]{1,2}의", "실시예[0-9]{1,2}의", "[0-9]{1,2}의"};

        /* 단어별 빈도수 카운트 */
        for (String parsStr : parsList) {
            String[] temp = parsStr.split("\\|");
            String symbolStr = temp[0];
            String symbolDescStr = temp[1];

            if (symbol.equals(symbolStr)) {
                Integer count = countMap.get(symbolDescStr);
                countMap.put(symbolDescStr, count == null ? 1 : count + 1);
            }

        }

//        System.out.println("[nonBracketWordRanklFilter COUNT]["+symbol+"]"+countMap);

        Collection values = countMap.values();
        if (values.isEmpty()) {
            rtnMap.put(symbol, "");
            return rtnMap;
        }

        int maxValue = (int) Collections.max(values);

        /* 빈도수가 가장 많은 것만 추출하면서 단어별 길이 담기 */
        for (Map.Entry<String, Integer> elem : countMap.entrySet()) {
            if (maxValue == elem.getValue()) {
                maxLengthMap.put(elem.getKey(), elem.getKey().length());
            }
        }
        String str;

        /* 단어별 길이가 가장 긴 것으로 오름차순 정렬 */
        List<String> maxKeySetList = new ArrayList(maxLengthMap.keySet());
        maxKeySetList.sort((o1, o2) -> (maxLengthMap.get(o2).compareTo(maxLengthMap.get(o1))));

        str = maxKeySetList.get(0);

        String[] chkStrArr = str.split(" ");
        str = "";

        chkWord :
        for (int j=0; j<chkStrArr.length; j++) {
            String chkStr = chkStrArr[j];
            //구성어 첫단어
            if (j == 0) {
                for (String stopWord : arrStopWord) {
                    if (chkStr.equals(stopWord)) {
                        chkStr = "";
                        continue chkWord;
                    }
                }
                for (String containStopWord : arrContainStopWord) {
                    if (chkStr.contains(containStopWord)) {
                        chkStr = "";
                        continue chkWord;
                    }
                }
            }
            //구성어 마지막단어
            if (j == chkStrArr.length - 1) {
                for (String stopWord : arrStopWord) {
                    if (chkStr.equals(stopWord)) {
                        chkStr = "";
                        continue chkWord;
                    }
                }
                for (String containStopWord : arrContainStopWord) {
                    if (chkStr.contains(containStopWord)) {
                        chkStr = "";
                        continue chkWord;
                    }
                }
            }
            str = str + " " + chkStr;
            str.trim();
        }

        if (str.indexOf("-") == 1) {
            str = str.substring(str.indexOf("-")+1);
        }

        //한단어에 지워야할 단어체크
        for (String removeWord : arrRemoveWord) {
            if (str.equals(removeWord)) {
                str = "";
            }
        }
        for (String removeMatchesWord : arrRemoveMatchesWord) {
            if (str.matches(removeMatchesWord)) {
                str = "";
            }
        }
        str = str.replace("<claim>","").replace("<ClaimText>","");

        rtnMap.put(symbol, str.trim());

        return rtnMap;
    }

    /**
     * 텍스트에 존재하는 괄호 모두 제거 모듈 <p>
     */
    public static String deleteBracketTextByPattern(String text) {

        /* 괄호안에 문장만 찾는 정규식 */
        Pattern bracketPattern = Pattern.compile("\\([^()]+\\)");

        Matcher matcher = bracketPattern.matcher(text);

        String pureText = text;

        while (matcher.find()) {
            int startIndex = matcher.start();
            int endIndex = matcher.end();

            String removeTextArea = pureText.substring(startIndex, endIndex);
            pureText = pureText.replace(removeTextArea, "");
            matcher = bracketPattern.matcher(pureText);
        }

        return pureText;
    }

    /**
     * 텍스트에 존재하는 괄호 내용 모두 추출 <p>
     */
    public static List<String> findBracketTextByPattern(String text) {

        /* 괄호안에 문장만 찾는 정규식 */
        Pattern bracketPattern = Pattern.compile("\\([^()]+\\)");

        ArrayList<String> bracketTextList = new ArrayList<>();

        Matcher matcher = bracketPattern.matcher(text);

        String findText;

        while (matcher.find()) {
            int startIndex = matcher.start();
            int endIndex = matcher.end();

            findText = text.substring(startIndex, endIndex);

            /* 추출된 괄호 데이터를 삽입 */
            bracketTextList.add(findText);
        }

        return bracketTextList;
    }

    public static List<Integer> findSymbolIdxList(String symbolStr, String desc) {
        List<Integer> symbolIdxList = new ArrayList<>();
        List<String> tableTagList = new ArrayList<>();
        List<String> mathsTagList = new ArrayList<>();
        List<String> spanTagList = new ArrayList<>();

        String[] arrFrontExcludWord = {"도", "도면", "표", "화학식", "수식", "식", "실시예", "도 ", "도면 ", "표 ", "화학식 ", "수식 ", "식 ", "실시예 ", "제", "제 "};
        String[] arrBackExcludWord = {"도는", "도의", "실시예", "화학식", " 도는", " 도의", " 실시예"};

        int tempIdx = 0;
        List<String> bracketList = KipoParsingUtil.findBracketTextByPattern(desc);

        //symbolStr = deleteBracketTextByPattern(symbolStr);
        symbolStr = symbolStr.replace("(", "").replace(")","");

        // 표 안에 있는 부호인지 체크
        Pattern tableTagPattern = Pattern.compile("<tables[^>](.*?)</tables>");
        Matcher tableTagMatcher = tableTagPattern.matcher(desc);
        while (tableTagMatcher.find()) {
            tableTagList.add(tableTagMatcher.start()+"|"+tableTagMatcher.end());
        }

        // 수식 안에 있는 부호인지 체크
        Pattern mathsTagPattern = Pattern.compile("<maths[^>](.*?)</maths>");
        Matcher mathsTagMatcher = mathsTagPattern.matcher(desc);
        while (mathsTagMatcher.find()) {
            mathsTagList.add(mathsTagMatcher.start()+"|"+mathsTagMatcher.end());
        }

        // span태그 안에 있는 부호인지 체크
        Pattern spanTagPattern = Pattern.compile("<span[^>](.*?)</span>");
        Matcher spanTagMatcher = spanTagPattern.matcher(desc);
        while (spanTagMatcher.find()) {
            spanTagList.add(spanTagMatcher.start()+"|"+spanTagMatcher.end());
        }

        stopWord:
        for (String bracketStr : bracketList) {
            if (bracketStr.contains(symbolStr)) {
                String firstWord;
                String lastWord;
                int firstWordIdx;
                int lastWordIdx;
                int wordIdx = bracketStr.indexOf(symbolStr);

                if (wordIdx == 0) {
                    wordIdx = 1;
                }

                firstWordIdx = wordIdx - 1;
                firstWord = String.valueOf(bracketStr.charAt(firstWordIdx));

                if ((wordIdx + symbolStr.length()) == bracketStr.length()) {
                    lastWordIdx = wordIdx + symbolStr.length() - 1;
                    lastWord = String.valueOf(bracketStr.charAt(lastWordIdx));
                } else {
                    lastWordIdx = wordIdx + symbolStr.length();
                    lastWord = String.valueOf(bracketStr.charAt(lastWordIdx));
                }

                if (Pattern.matches("[(, ]", firstWord) && Pattern.matches("[), ]", lastWord)) {
                    tempIdx = desc.indexOf(bracketStr, tempIdx + 1);
//                        System.out.println("["+symbolStr+"]["+bracketStr+"]["+firstWord+"]["+lastWord+"]["+tempIdx+"]["+subStr+"]");
                    // 표 안에 있는 부호인지 체크
                    for (String tableTagStr : tableTagList) {
                        String[] tableTagArr = tableTagStr.split("\\|");
                        int startTableTagIdx = Integer.parseInt(tableTagArr[0]);
                        int endTableTagIdx = Integer.parseInt(tableTagArr[1]);

                        if (startTableTagIdx < tempIdx && endTableTagIdx > tempIdx) {
                            tempIdx = -1;
                        }
                    }

                    // 수식 안에 있는 부호인지 체크
                    for (String mathsTagStr : mathsTagList) {
                        String[] mathsTagArr = mathsTagStr.split("\\|");
                        int startMathsTagIdx = Integer.parseInt(mathsTagArr[0]);
                        int endMathsTagIdx = Integer.parseInt(mathsTagArr[1]);

                        if (startMathsTagIdx < tempIdx && endMathsTagIdx > tempIdx) {
                            tempIdx = -1;
                        }
                    }

                    // span태그 안에 있는 부호인지 체크
                    for (String spanTagStr : spanTagList) {
                        String[] spanTagArr = spanTagStr.split("\\|");
                        int startSpanTagIdx = Integer.parseInt(spanTagArr[0]);
                        int endSpanTagIdx = Integer.parseInt(spanTagArr[1]);

                        if (startSpanTagIdx < tempIdx && endSpanTagIdx > tempIdx) {
                            tempIdx = -1;
                        }
                    }

                    for (String excludeWord : arrFrontExcludWord) {
                        if (bracketStr.contains(excludeWord + symbolStr)) {
                            tempIdx = -1;
                        }
                    }

                    for (String excludeWord : arrBackExcludWord) {
                        if (bracketStr.contains(symbolStr + excludeWord)) {
                            tempIdx = -1;
                        }
                    }

                    if (tempIdx > -1)
                        symbolIdxList.add(tempIdx);
                }
            }
        }
        return symbolIdxList;
    }

    public static String docTagInsert(String desc, List<String> symbolIdxList) {
        String forwardDesc = "";
        String backDesc = "";

        int idx = 0;

        for (String symbolIdxStr : symbolIdxList) {

            String[] arrSymbolInfo = symbolIdxStr.split("\\|");
            String symbolStr = arrSymbolInfo[0];
            String oriSymbolStr =  arrSymbolInfo[0];
            symbolStr = symbolStr.replaceAll("[()]", "");
            int symbolIdx = Integer.parseInt(arrSymbolInfo[1]) + idx;

            forwardDesc = desc.substring(0, symbolIdx);
            backDesc = desc.substring(symbolIdx);

            String replaceTxt = "<span class=\"symbol symbol_"+oriSymbolStr+"\" symbol=\""+oriSymbolStr+"\">"+symbolStr+"</span>";

            idx = idx + (replaceTxt.length() - symbolStr.length());

            backDesc = backDesc.replaceFirst(symbolStr,replaceTxt);

            desc = forwardDesc + backDesc;
        }

        return desc;
    }

}
