package es.tmp.keylense;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SymbolParser {
    public Map<String, String> symbolDocIndexPars2(PatentVO patentVO, Set<String> symboSet) {
        List<String> symbolList = new ArrayList<>(symboSet);
        List<String> nonSymbolList = new ArrayList<>();
        List<String> retrySymbolList = new ArrayList<>();
        List<String> tagIdxList = new ArrayList<>();
        List<String> parsingList = new ArrayList<>();
        List<String> nonBracketParsingList = new ArrayList<>();

        Map<String, String> rtnMap = new HashMap<>();

        String desc = "";
        if (patentVO.getDescription() != null)
            desc = patentVO.getDescription().get(0);
//        String desc = patentVO.getDescription()[0];

        String summary = "";
        if (patentVO.getSummary() != null)
            summary = patentVO.getSummary().get(0);

        String claims = patentVO.getClaimText();
        if (claims == null)
            claims = "";


        //<p 태그 self close 되어 있는건 전부 삭제
        desc = desc.replaceAll("<p([^\\/>]+)\\/>", "");

        List<String> descList = new ArrayList<>();
        descList.add(desc);

        List<String> symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "reference-signs-list");

        if (symbolDescList.size() == 0) {
            symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "drawingDescription");
            if (symbolDescList.size() == 0) {
                symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "description-of-drawings");
                if (symbolDescList.size() == 0) {
                    symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "DescriptionDrawings");
                    if (symbolDescList.size() == 0) {
                        symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "PCTDescriptionDrawings");
                        if (symbolDescList.size() == 0) {
                            symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "DrawingDescription");
                        }
                    }
                }
            }
        }


        /**괄호 있는 부호 파싱 Description **/
        if (!"".equals(desc))
            for (String symbolStr : symbolList) {
                krBracketParser(symbolStr, symbolList, desc, tagIdxList, parsingList);
                tagIdxList = tagIdxList.stream().distinct().collect(Collectors.toList());
                tagIdxList.clear();
            }

        /**괄호 있는 부호 파싱 Summary **/
        if (!"".equals(summary))
            for (String symbolStr : symbolList) {
                krBracketParser(symbolStr, symbolList, summary, tagIdxList, parsingList);
                tagIdxList = tagIdxList.stream().distinct().collect(Collectors.toList());
                tagIdxList.clear();
            }
        /**괄호 있는 부호 파싱 Claims **/
        if (!"".equals(claims))
            for (String symbolStr : symbolList) {
                krBracketParser(symbolStr, symbolList, claims, tagIdxList, parsingList);
                tagIdxList = tagIdxList.stream().distinct().collect(Collectors.toList());
                tagIdxList.clear();
            }

        for (String symbolStr : symbolList) {
            rtnMap.putAll(KipoParsingUtil.wordRanklFilter2(symbolStr, parsingList, false));
        }

        // 추출 못하거나 1번만 나온 부호 추출
        for (String symbolStr : symbolList) {
            for (Map.Entry<String, String> elem : rtnMap.entrySet()) {
                if (symbolStr.equals(elem.getKey()) && !"".equals(elem.getValue())) {
                    symbolStr = "";
                }
            }
            if (!"".equals(symbolStr))
                nonSymbolList.add(symbolStr);
        }

//        System.out.println("[nonSymbolList]"+nonSymbolList);
//
        /**괄호 없는 부호 파싱**/
        for (String nonSymbolStr : nonSymbolList) {
            krNonBracketParser(nonSymbolStr, symbolDescList, symbolList, "：:;,. ", nonBracketParsingList);
        }
//        System.out.println("[RESULT2]"+nonBracketParsingList);

        for (String nonSymbolStr : nonSymbolList) {
            Map<String, String> nonBracketMap = KipoParsingUtil.nonBracketWordRanklFilter(nonSymbolStr, nonBracketParsingList);
            for (Map.Entry<String, String> elem : nonBracketMap.entrySet()) {
                if (nonSymbolStr.equals(elem.getKey()) && !"".equals(elem.getValue())) {
                    rtnMap.put(elem.getKey(), elem.getValue());
                } else {
                    retrySymbolList.add(nonSymbolStr);
                }
            }
        }

        /**도면부호 설명 태그 입력**/
//        patentVO.setDescription(patentVO.getDescription());

//        System.out.println("[TAGINSERT]"+patentVO.getDescription());

        /** 한번 추출된 문장을 도면 부호의 설명에서 추출 못했을때 다시 괄호 있는 부호 파싱**/
        parsingList.clear();
        if (!"".equals(desc))
            for (String retrySymbolStr : retrySymbolList) {
                krBracketParser(retrySymbolStr, symbolList, desc, tagIdxList, parsingList);
            }
        if (!"".equals(summary))
            for (String retrySymbolStr : retrySymbolList) {
                krBracketParser(retrySymbolStr, symbolList, summary, tagIdxList, parsingList);
            }
        if (!"".equals(claims))
            for (String retrySymbolStr : retrySymbolList) {
                krBracketParser(retrySymbolStr, symbolList, claims, tagIdxList, parsingList);
            }

        for (String retrySymbolStr : retrySymbolList) {
            Map<String, String> bracketMap = KipoParsingUtil.wordRanklFilter2(retrySymbolStr, parsingList, true);
            for (Map.Entry<String, String> elem : bracketMap.entrySet()) {
                if (retrySymbolStr.equals(elem.getKey()) && !"".equals(elem.getValue())) {
                    rtnMap.put(elem.getKey(), elem.getValue());
                }
            }
        }

        if (!rtnMap.isEmpty())
            return rtnMap;

        return null;
    }

    public void symbolDocIndexPars(PatentVO patentVO, List<DrawingSymbolVO> drawingSymbolList) {
        List<String> symbolList = new ArrayList<>();
        List<String> nonSymbolList = new ArrayList<>();
        List<String> retrySymbolList = new ArrayList<>();
        List<String> tagIdxList = new ArrayList<>();
        List<String> parsingList = new ArrayList<>();
        List<String> nonBracketParsingList = new ArrayList<>();

        Map<String, String> rtnMap = new HashMap<>();

        String desc = "";
        if (patentVO.getDescription() != null)
            desc = patentVO.getDescription().get(0);
//        String desc = patentVO.getDescription()[0];

        String summary = "";
        if (patentVO.getSummary() != null)
            summary = patentVO.getSummary().get(0);

        String claims = patentVO.getClaimText();
        if (claims == null)
            claims = "";

        //<p 태그 self close 되어 있는건 전부 삭제
        desc = desc.replaceAll("<p([^\\/>]+)\\/>", "");

        List<String> descList = new ArrayList<>();
        descList.add(desc);

        List<String> symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "reference-signs-list");

        if (symbolDescList.size() == 0) {
            symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "drawingDescription");
            if (symbolDescList.size() == 0) {
                symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "description-of-drawings");
                if (symbolDescList.size() == 0) {
                    symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "DescriptionDrawings");
                    if (symbolDescList.size() == 0) {
                        symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "PCTDescriptionDrawings");
                        if (symbolDescList.size() == 0) {
                            symbolDescList = KipoParsingUtil.getNonBracketTagFilter(desc, "DrawingDescription");
                        }
                    }
                }
            }
        }

        for (DrawingSymbolVO symbolVO : drawingSymbolList) {
            if (!"".equals(symbolVO.getSymbol())) {
                symbolList.add(symbolVO.getSymbol());
            }
        }

        /**괄호 있는 부호 파싱 Description **/
        for (String symbolStr : symbolList) {
            krBracketParser(symbolStr, symbolList, desc, tagIdxList, parsingList);
            tagIdxList = tagIdxList.stream().distinct().collect(Collectors.toList());
            tagIdxList.clear();
        }
        /**괄호 있는 부호 파싱 Summary **/
        for (String symbolStr : symbolList) {
            krBracketParser(symbolStr, symbolList, summary, tagIdxList, parsingList);
            tagIdxList = tagIdxList.stream().distinct().collect(Collectors.toList());
            tagIdxList.clear();
        }
        /**괄호 있는 부호 파싱 Claims **/
        for (String symbolStr : symbolList) {
            krBracketParser(symbolStr, symbolList, claims, tagIdxList, parsingList);
            tagIdxList = tagIdxList.stream().distinct().collect(Collectors.toList());
            tagIdxList.clear();
        }

        for (String symbolStr : symbolList) {
            rtnMap.putAll(KipoParsingUtil.wordRanklFilter2(symbolStr, parsingList, false));
        }

        // 추출 못하거나 1번만 나온 부호 추출
        for (String symbolStr : symbolList) {
            for (Map.Entry<String, String> elem : rtnMap.entrySet()) {
                if (symbolStr.equals(elem.getKey()) && !"".equals(elem.getValue())) {
                    symbolStr = "";
                }
            }
            if (!"".equals(symbolStr))
                nonSymbolList.add(symbolStr);
        }

//        System.out.println("[nonSymbolList]"+nonSymbolList);
//
        /**괄호 없는 부호 파싱**/
        for (String nonSymbolStr : nonSymbolList) {
            krNonBracketParser(nonSymbolStr, symbolDescList, symbolList, "：:;,. ", nonBracketParsingList);
        }
//        System.out.println("[RESULT2]"+nonBracketParsingList);

        for (String nonSymbolStr : nonSymbolList) {
            Map<String, String> nonBracketMap = KipoParsingUtil.nonBracketWordRanklFilter(nonSymbolStr, nonBracketParsingList);
            for (Map.Entry<String, String> elem : nonBracketMap.entrySet()) {
                if (nonSymbolStr.equals(elem.getKey()) && !"".equals(elem.getValue())) {
                    rtnMap.put(elem.getKey(), elem.getValue());
                } else {
                    retrySymbolList.add(nonSymbolStr);
                }
            }
        }

        /**도면부호 설명 태그 입력**/
//        patentVO.setDescription(patentVO.getDescription());

//        System.out.println("[TAGINSERT]"+patentVO.getDescription());

        /** 한번 추출된 문장을 도면 부호의 설명에서 추출 못했을때 다시 괄호 있는 부호 파싱**/
        parsingList.clear();
        for (String retrySymbolStr : retrySymbolList) {
            krBracketParser(retrySymbolStr, symbolList, desc, tagIdxList, parsingList);
        }
        for (String retrySymbolStr : retrySymbolList) {
            krBracketParser(retrySymbolStr, symbolList, summary, tagIdxList, parsingList);
        }
        for (String retrySymbolStr : retrySymbolList) {
            krBracketParser(retrySymbolStr, symbolList, claims, tagIdxList, parsingList);
        }

        for (String retrySymbolStr : retrySymbolList) {
            Map<String, String> bracketMap = KipoParsingUtil.wordRanklFilter2(retrySymbolStr, parsingList, true);
            for (Map.Entry<String, String> elem : bracketMap.entrySet()) {
                if (retrySymbolStr.equals(elem.getKey()) && !"".equals(elem.getValue())) {
                    rtnMap.put(elem.getKey(), elem.getValue());
                }
            }
        }

        /**최종**/
        for (DrawingSymbolVO symbolVO : drawingSymbolList) {
            for (Map.Entry<String, String> elem : rtnMap.entrySet()) {
                if (symbolVO.getSymbol().equals(elem.getKey())) {
                    symbolVO.setSymbolDesc(elem.getValue());
                    System.out.println("[" + symbolVO.getSymbol() + "] : [" + symbolVO.getSymbolDesc() + "]");
                }
            }
        }
    }


    public void krBracketParser(String symbolStr, List<String> symbolList, String desc, List<String> tagIdxList, List<String> parsingList2) {
        List<String> parsingList = new ArrayList<>();

        int index;

        List<Integer> symbolIdxList = KipoParsingUtil.findSymbolIdxList(symbolStr, desc);

        for (int symbolIdx : symbolIdxList) {
            index = symbolIdx;

            if (index == -1)
                continue;

            boolean fiveWordFlag = true;

            int baseWordIdx = index;
            int lastWordIdx = index;
            int startWordIdx;

            int wordCnt = 0;

            String beforeWord = "";

            //1.무조건 공백으로 부호 앞 5단어까지 짜르기, 인덱스가 0이면 멈춤
            fiveWordContinue:
            while (fiveWordFlag) {
                boolean stopWordFlag = true;
                if (baseWordIdx >= 0 && desc.charAt(baseWordIdx) == ' ' || baseWordIdx == 0) {
                    if (wordCnt <= 4) {
                        startWordIdx = baseWordIdx;
                        String baseDescStr = desc.substring(startWordIdx, lastWordIdx).trim();

                        //2.뒤에서부터 아래 문자가 있으면 거기서부터 자르기
                        //case : (16B)(16A) 이고 16A을 찾을때 앞 (16B)삭제
                        if (baseDescStr.length() - 1 == baseDescStr.lastIndexOf(")")) {
                            if (!"".equals(baseDescStr)) {
                                int endIndex = baseDescStr.length() - (symbolStr.length() + 2);
                                for (String chkSymbolStr : symbolList) {
                                    if (!"".equals(chkSymbolStr) && baseDescStr.contains(chkSymbolStr)) {
                                        //앞뒤 단어를 체크 하여 부호인지 여부 판단
                                        if (baseDescStr.indexOf(chkSymbolStr) > -1) {
                                            String forwardWord = "";
                                            String backWord = "";

                                            if ((baseDescStr.indexOf(chkSymbolStr) - 1) > -1)
                                                forwardWord = String.valueOf(baseDescStr.charAt(baseDescStr.indexOf(chkSymbolStr) - 1));
                                            if ((baseDescStr.indexOf(chkSymbolStr) + chkSymbolStr.length()) < baseDescStr.length())
                                                backWord = String.valueOf(baseDescStr.charAt(baseDescStr.indexOf(chkSymbolStr) + chkSymbolStr.length()));
//                                            System.out.println("[ONEONE_BRACKET]["+symbolStr+"]["+startWordIdx+"]["+forwardWord+"]["+backWord+"]["+baseDescStr+"]");
                                            if (Pattern.matches("[(, ]", forwardWord) && Pattern.matches("[), ]", backWord)) {
                                                if ((baseDescStr.length() - (chkSymbolStr.length() + 2)) > 0) {
                                                    baseDescStr = baseDescStr.substring(0, baseDescStr.length() - (chkSymbolStr.length() + 2));
                                                    break;
                                                }
                                            }

                                            //앞단어 부호인지 여부 체크(부호에 태그가 입력되어 있는경우)
                                            if (Pattern.matches("[\"]", forwardWord) && Pattern.matches("[\"]", backWord)) {
//                                                System.out.println("[ONEONE]["+symbolStr+"]["+startWordIdx+"]["+forwardWord+"]["+backWord+"]["+baseDescStr+"]");
                                                if (baseDescStr.contains("</span>")) {
                                                    int charIdx = 0;
                                                    int charBlankIdx = 0;
                                                    while (true) {
                                                        if ((startWordIdx - charIdx) < 0)
                                                            break ;
                                                        if ('(' == desc.charAt(startWordIdx - charIdx)) {
                                                            charBlankIdx = 1;
                                                            lastWordIdx = startWordIdx - charIdx;
                                                        }
                                                        if (charBlankIdx == 1) {
                                                            if (' ' == desc.charAt(startWordIdx - charIdx)) {
                                                                baseWordIdx = baseWordIdx - charIdx;
                                                                continue fiveWordContinue;
                                                            }
                                                        }
                                                        charIdx++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //부호 앞단어에 ( 있으면 (부터 자름
                        if (baseDescStr.lastIndexOf("(") > -1 && baseDescStr.lastIndexOf(")") == -1) {
                            baseDescStr = baseDescStr.substring(baseDescStr.lastIndexOf("(") + 1).trim();
                        }


                        if (baseDescStr.lastIndexOf(")") > -1) {
                            for (String chkSymbolStr : symbolList) {
                                if (!"".equals(chkSymbolStr) && baseDescStr.contains(chkSymbolStr)) {
                                    //앞뒤 단어를 체크 하여 부호인지 여부 판단
                                    if (baseDescStr.indexOf(chkSymbolStr) > -1) {
                                        String forwardWord = "";
                                        String backWord = "";

                                        if ((baseDescStr.indexOf(chkSymbolStr) - 1) > -1)
                                            forwardWord = String.valueOf(baseDescStr.charAt(baseDescStr.indexOf(chkSymbolStr) - 1));

                                        if ((baseDescStr.indexOf(chkSymbolStr) + chkSymbolStr.length()) < baseDescStr.length())
                                            backWord = String.valueOf(baseDescStr.charAt(baseDescStr.indexOf(chkSymbolStr) + chkSymbolStr.length()));

                                        if (Pattern.matches("[(,_\" ]", forwardWord) && Pattern.matches("[),\" ]", backWord)) {
                                            baseDescStr = baseDescStr.substring(baseDescStr.lastIndexOf(")") + 1).trim();
                                            stopWordFlag = false;
                                            break;
                                        }
//System.out.println("########################["+chkSymbolStr+"]["+forwardWord+"]["+backWord+"]"+baseDescStr);
                                        if ("".equals(forwardWord) && Pattern.matches("[),\" ]", backWord)) {
                                            baseDescStr = baseDescStr.substring(baseDescStr.lastIndexOf(")") + 1).trim();
                                            stopWordFlag = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
//                        System.out.println("[SPLIT1]["+wordCnt+"]["+stopWordFlag+"]["+symbolStr+"]["+startWordIdx+"]["+ baseDescStr+"]");
                        if (!stopWordFlag)
                            break;

                        //3.특수문자 체크
                        Pattern specialExpPattern = Pattern.compile("[~!@#$%^&*=+\\[\\]{}:;,.?\\\\|\n\"]");
                        Matcher specialExpMatcher = specialExpPattern.matcher(baseDescStr);
                        int specialExpIdx = -1;

                        while (specialExpMatcher.find()) {
                            specialExpIdx = specialExpMatcher.start();
                        }

                        if (specialExpIdx > -1) {
                            baseDescStr = baseDescStr.substring(specialExpIdx + 1).trim();
                            stopWordFlag = false;

                            //3-1. 특수문자에서 태그인지 여부 판단하여 제거
                            int tagIdx = -1;
                            Pattern tagPattern = Pattern.compile("[<>]");
                            Matcher tagMatcher = tagPattern.matcher(baseDescStr);
                            while (tagMatcher.find()) {
                                tagIdx = tagMatcher.start();
                            }

                            if (tagIdx > -1) {
                                baseDescStr = baseDescStr.substring(tagIdx + 1).trim();
                                stopWordFlag = true;
                            }
                        }

                        baseDescStr = baseDescStr.replace("<br/>", "");

//                        System.out.println("[SPLIT2]["+wordCnt+"]["+stopWordFlag+"]["+symbolStr+"]["+specialExpIdx+"]"+ baseDescStr);
                        if (!stopWordFlag)
                            break;

                        //4. 설정한 글자부터 자름
                        String[] arrStopWord = {"cm", "nm", "mm", "cm", "mg", "g", "kg", "cc", "ml", "dl", "l"
                                , "°C", "K", "atm", "Pa", "psi"};

                        for (String setStopWord : arrStopWord) {
                            if (baseDescStr.lastIndexOf(setStopWord) > -1) {
                                if (baseDescStr.lastIndexOf(setStopWord) - 1 > -1) {
                                    String regExp = "^[0-9]+$";
                                    String chkStr = String.valueOf(baseDescStr.charAt(baseDescStr.lastIndexOf(setStopWord) - 1));
                                    if (chkStr.matches(regExp)) {
                                        baseDescStr = baseDescStr.substring(baseDescStr.lastIndexOf(setStopWord) + setStopWord.length()).trim();
                                        stopWordFlag = false;
                                    }
                                }
                            }
                        }
//                        System.out.println("[SPLIT3]["+wordCnt+"]["+stopWordFlag+"]["+symbolStr+"]"+ baseDescStr);
                        if (!stopWordFlag)
                            break;

                        //5.첫 글자가 설정한 글자이면 그 다음부터 자름
                        String[] arrExcludWord = {"도", "도면", "표", "화학식", "수식", "식", "도 ", "도면 ", "표 ", "화학식 ", "수식 ", "식 ", "제", "제 "};

                        for (String setExcludeWord : arrExcludWord) {
//                            baseDescStr = baseDescStr.replaceAll(setExcludeWord + "[0-9]", "");
                        }
//                        System.out.println("[SPLIT4]["+wordCnt+"]["+stopWordFlag+"]["+symbolStr+"]"+ baseDescStr);

                        //6.첫 단어가 설정한 글자이면 그 다음부터 자름
                        String[] arrFirstWord = {"상기", "및", "바람직", "것도", "것으로", "것은", "것을", "것이", "것이다"
                                , "게다가", "곧", "그 ", "그까짓", "그깟", "그나마", "그냥", "그다지", "그래", "그래도", "그래서"
                                , "그래야", "그러고", "그러나", "그러다", "그러다가", "그러면", "그러므로", "그러자", "그럭저럭", "그런"
                                , "그런데", "그럼", "그럼에도", "그렇게", "그렇니까", "그렇다면", "그렇지만", "그리", "그리고"
                                , "그야말로", "그외", "그저", "그제서야", "더구나", "더군다나", "됐다", "되게", "되고", "되는"
                                , "되는대로", "되도록", "되레", "되려", "되어", "되었다", "된다", "또", "또는", "또다시", "또한"
                                , "물론", "아마도", "아울러", "왜냐하면", "있다", "있어", "있어서", "있었다", "있으며", "있을"
                                , "이루어진", "있는", "구성하는", "위한", "형성하는", "형성된", "일어나는", "제공하는", "구비하는"
                                , "추가로", "포함하는", "특징으로", "특징으로하는", "하는", "나타내는", "가능한", "설치되는", "이다", "의해"
                                , "의한", "의하여", "있으나", "된", "적어도", "어느", "돌려서", "내지", "통하여", "통해", "따라", "혹은", "않는"
                                , "위하여", "않아서", "않아", "않고", "않으며", "따르면", "하였다", "따라서", "위해", "있고", "있어", "있으며"
                                , "바와", "같이", "같은", "있도록", "한다", "가지며", "이때", "사이에", "따른", "대한", "만들어", "경우", "경우에"
                                , "이를", "대하여", "대략", "토록", "가지며", "하는", "바람직", "한다", "하여", "되면", "되는", "거나", "시켜"
                                , "여전히", "예시적으로", "함으로써", "만큼", "여기서", "하고"};

                        for (String setFirstWord : arrFirstWord) {
                            if (baseDescStr.indexOf(setFirstWord) > -1) {
                                baseDescStr = baseDescStr.substring(baseDescStr.indexOf(setFirstWord) + setFirstWord.length()).trim();
                                stopWordFlag = false;
                            }
                        }
//                        System.out.println("[SPLIT5]["+wordCnt+"]["+stopWordFlag+"]["+symbolStr+"]"+ baseDescStr);
                        if (!stopWordFlag)
                            break;
//                            System.out.println("[SPLIT6]["+stopWordFlag+"]["+symbolStr+"]"+ baseDescStr);
                        if (!"".equals(baseDescStr)) {
                            if (!beforeWord.equals(baseDescStr)) {
                                parsingList.add(baseDescStr);
                                tagIdxList.add(symbolStr + "|" + index);
                                parsingList2.add(symbolStr + "|" + baseDescStr);
                            }
                            beforeWord = baseDescStr;
//                            System.out.println("[SPLIT]["+baseWordIdx+"]["+index+"]["+stopWordFlag+"]["+wordCnt+"]["+symbolStr+"]"+parsingList);
                        }

                    }
                    if (wordCnt == 4)
                        fiveWordFlag = false;

                    wordCnt++;
                }
                if (baseWordIdx == 0)
                    fiveWordFlag = false;

                baseWordIdx--;
            }
        }

        symbolIdxList.clear();
    }

    public void krNonBracketParser(String symbolStr, List<String> descList, List<String> symbolList, String delimeter, List<String> parsingList) {
        String mapValue = "";

        List<String> filterDescList = new ArrayList<>();
        List<String> fiveWordList = new ArrayList<>();

        symbolStr = symbolStr.replaceAll("[()]", "");

        String[] arrStopUnit = {"cm", "nm", "mm", "cm", "mg", "g", "kg", "cc", "ml", "dl", "l"
                , "°C", "K", "atm", "Pa", "psi"};

        String[] arrExcludWord = {"도", "도면", "표", "화학식", "수식", "식", "실시예", "제"};

//        System.out.println("[krNonBracketParser]["+symbolStr+"]["+filterDescList+"]");

        fiveWordOut:
        for (String desc : descList) {
            desc = desc.replaceAll("(\t|\\\\t|\r\n|\\\\r|\\\\n|\n\r|\n|\r|\\\\|↵)", " ");
            List<Integer> fiveWordIdxList = new ArrayList<>();
            int startFiveWordIdx = 0;
            int fiveWordCnt = 0;

            boolean flag = false;

            String frontWord = "";
            String backWord = "";
            int replaceFlag = 0;

            StringTokenizer filterDescToken = new StringTokenizer(desc, delimeter);

            //:;, 기준으로 짜름
            while (filterDescToken.hasMoreElements()) {
                fiveWordList.add(filterDescToken.nextToken());
            }

            for (int i = 0; i < fiveWordList.size(); i++) {
                String chkStr = fiveWordList.get(i);
                if (fiveWordList.get(i).equals(symbolStr)) {
                    if (i != 0) {
                        frontWord = fiveWordList.get(i - 1);
                    }
                    if (i + 1 < fiveWordList.size()) {
                        backWord = fiveWordList.get(i + 1);
                    }

                    for (String excludeWrodStr : arrExcludWord) {
                        if (frontWord.equals(excludeWrodStr)) {
                            replaceFlag++;
                        }
                        if (backWord.equals(excludeWrodStr)) {
                            replaceFlag++;
                        }
                    }

                    if (replaceFlag == 0)
                        fiveWordIdxList.add(i);
                }
//                if (chkStr.matches("[\\[\\s]" + symbolStr)) {
//                    fiveWordIdxList.add(i);
//                }
            }
//            System.out.println("[krNonBracketParser fiveWordList]["+symbolStr+"]["+fiveWordList+"]["+fiveWordIdxList+"]");
            for (Integer startFiveWord : fiveWordIdxList) {
                startFiveWordIdx = startFiveWord;
                fiveWordContinue:
                for (int i = startFiveWordIdx; i < fiveWordList.size(); i++) {
                    String fiveWordStr = fiveWordList.get(i);
                    if (i != startFiveWordIdx) {
                        if (fiveWordCnt < 5) {
                            // 1.5단어 모으다가 부호가 나오면 멈춤
//                            System.out.println("[krNonBracketParser][STEP-]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]");
                            if (flag) {
                                for (String symbolListStr : symbolList) {
                                    if (symbolListStr.equals(fiveWordStr)) {
                                        break fiveWordContinue;
                                    }
                                }
                            }
//                            System.out.println("[krNonBracketParser][STEP0]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]");
                            for (String symbolListStr : symbolList) {
                                if (symbolListStr.equals(fiveWordStr)) {
                                    flag = false;
                                    continue fiveWordContinue;
                                } else {
                                    flag = true;
                                }
                            }
//                            System.out.println("[krNonBracketParser][STEP1]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]");
                            for (String stopUnitStr : arrStopUnit) {
                                if (fiveWordStr.contains(stopUnitStr)) {
                                    Pattern unitPattern = Pattern.compile("[0-9 ]");
                                    Matcher unitMatcher = unitPattern.matcher(fiveWordStr);
                                    int unitIdx = -1;
                                    while (unitMatcher.find()) {
                                        unitIdx = unitMatcher.start();
                                    }
                                    if (unitIdx == -1) {
                                        break fiveWordContinue;
                                    }
                                }
                            }
//                            System.out.println("[krNonBracketParser][STEP2]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]");
                            Pattern specialExpPattern = Pattern.compile("[~!@#$%^&*=+\\[\\]{}:;,.?\\\\|\n\"]");
                            Matcher specialExpMatcher = specialExpPattern.matcher(fiveWordStr);
                            int specialExpIdx = -1;

                            while (specialExpMatcher.find()) {
                                specialExpIdx = specialExpMatcher.start();
                            }

                            if (specialExpIdx > -1) {
                                fiveWordStr = fiveWordStr.substring(specialExpIdx + 1).trim();
                                break fiveWordContinue;
                            }

//                            System.out.println("[krNonBracketParser][STEP3]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]["+specialExpIdx+"]");
                            if (fiveWordStr.indexOf("(") == -1 && fiveWordStr.lastIndexOf(")") > -1) {
//                                mapValue = "";
//                                break fiveWordContinue;
                            }

                            for (String symbolListStr : symbolList) {
                                if (fiveWordStr.indexOf("(" + symbolListStr + ")") > -1) {
                                    fiveWordStr = fiveWordStr.substring(0, fiveWordStr.indexOf("(" + symbolListStr + ")"));
                                    break fiveWordContinue;
                                }
                            }
//                            System.out.println("[krNonBracketParser][STEP4]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]["+fiveWordStr.indexOf("(")+"]["+fiveWordStr.lastIndexOf(")")+"]");
                            if (fiveWordCnt == 0)
                                mapValue = mapValue + fiveWordStr;
                            else
                                mapValue = mapValue + " " + fiveWordStr;

                            if (!"".equals(mapValue)) {
                                parsingList.add(symbolStr + "|" + mapValue.trim());
//                                System.out.println("[krNonBracketParser]["+symbolStr+"]["+fiveWordCnt+"]["+fiveWordStr+"]["+flag+"]["+parsingList+"]");
                            }


                            fiveWordCnt++;
                        }
                    }
                }
                mapValue = "";
                fiveWordCnt = 0;
            }
        }
    }
}
