package es.tmp.keylense;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocInfo {
    public String countryCode;
    public String documentNumber;
    public String documentType;

    public String getDocumentKind() {
        if (documentKind == null)
            return "";

        return documentKind;
    }

    public String documentKind;

    public String getDocumentId() {
        if (documentId == null) {
            return this.countryCode + this.documentNumber + this.documentType+"_nan";
        }
        return documentId;
    }

    public String documentId;

}
