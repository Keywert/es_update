package es.tmp.keylense;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ESUpdateSymbolDesc {

    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es18"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es43"));

        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));
//        ESConfig esDrawing = new ESConfig("es18");
        ESConfig esPatent = new ESConfig("es13");

        SymbolParser symbolParser = new SymbolParser();

        long start = System.currentTimeMillis();

        String drawinIndex = "drawing_kipo";
        String patentIndex = "kipo";
        String type = "patent";

        //          1998, 1999, 2002, 2003,2004, 2005, 2010, 2012, 2013, 2014, 2015, 2016, 2017, 2019, 2020
        // 재작업필요  1960, 1880, 1990_1993, 1994_1997, 2000 ,2018,
        // 확인필요   2009


        String[] ids = {
//                "kdw_kr19850001726a"
//                ,
//                "kdw_kr20130148076a" ,"kdw_kr20120138402a","kdw_kr20190033985b1"
                                "kdw_kr20200117496a"

        };


        String data = "kdw_kr20";
//        String data = "kdw_kr2017002";
//        String data = "kdw_kr2009009";


        System.out.println(data);

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("symbolFlag", false))
//                .must(QueryBuilders.prefixQuery("drawingId", data))
//                .mustNot(QueryBuilders.existsQuery("drawingSymbols.symbols.symbolDesc"))
//                .must(QueryBuilders.idsQuery().addIds(ids))
                ;

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(drawinIndex)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
//                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        ObjectMapper objectMapper = new ObjectMapper();

        int count = 0;
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 50 == 0)
                        System.out.println(count);

                    Drawing drawing = objectMapper.readValue(hit.getSourceAsString(), Drawing.class);
                    if (drawing == null)
                        continue;

                    String docId = drawing.getLinkedDocumentId();
                    if (docId == null)
                        continue;

//                    System.out.println(docId);

                    PatentVO patentVO = getPatent(esPatent, docId);
                    if (patentVO != null) {
                        Set<String> symbolSet = drawing.getSymbolSet();
                        if (symbolSet != null) {
                            Map<String, String> symbolMap = symbolParser.symbolDocIndexPars2(patentVO, symbolSet);
//                            System.out.println(objectMapper.writeValueAsString(symbolMap));
                            if (symbolMap != null)
                                drawing.setSymbolDescMap(symbolMap);
                        }
//                    List<DrawingVO> drawingVOS = patentVO.getDrawings();
//                    if (drawingVOS != null)
//                        drawing.setDrawings(drawingVOS);
                        drawing.setSymbolFlag(true);
                    } else {
                        drawing.setSymbolFlag(false);
                    }

                    String json = objectMapper.writeValueAsString(drawing);

//                    System.out.println(objectMapper.writeValueAsString(drawing));
                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new UpdateRequest(drawinIndex, type, drawing.getDrawingId())
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                }
                scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            System.out.println(count);

            long end = System.currentTimeMillis();
            System.out.println("실행 시간 : " + (end - start) / 1000.0 + " 평균 시간 : " + ((end - start) / 1000.0) / count);
            System.out.println("Finish all work~~!");

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            esPatent.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static PatentVO getPatent(ESConfig esPatent, String docId) {
        List<String> docIds = new ArrayList<>();
        docIds.add(docId);

        if(docId.matches("^kr(19|20)[0-9]{2}70[0-9]{5}$"))
            docIds.add(docId.substring(0,6)+"07"+docId.substring(8));

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(docIds);

        String [] fields = {"description","summary","claims","drawings","applicationNumber","documentId"};

        SearchResponse scrollResp = esPatent.client.prepareSearch()
                .setTypes("patent")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(2)
                .execute()
                .actionGet();

        ObjectMapper objectMapper = new ObjectMapper();

//        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
        try {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                PatentVO patentVO = objectMapper.readValue(hit.getSourceAsString(), PatentVO.class);
                if (patentVO != null)
                    return patentVO;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }
}
