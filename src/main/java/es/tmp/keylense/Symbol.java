package es.tmp.keylense;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Symbol {
    public Integer posX1;
    public Integer posY1;
    public Integer posX2;
    public Integer posY2;
    public Integer posX3;
    public Integer posY3;
    public Integer posX4;
    public Integer posY4;
    public String symbol;
    public String symbolDesc;
}
