package es.tmp.keylense;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.bulk.Claim;
import es.model.v1.CitationVO;
import es.model.v1.FtermDeseralizer;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentVO {

    private boolean patent;
    private boolean register;
    private String documentId;
    private String documentNumber;
    public List<Claim> claims;                 // 청구범위 (independentClaimText, claimText)

    public List<CitationVO> citationDoc;

    public String getClaimText() {
        if (claims != null) {
            claimText = "";
            for(Claim claim : claims) {
                String temp = claim.getIndependentClaimText();
                if (temp == null)
                    temp = claim.getClaimText();

                if (temp != null)
                    claimText += temp;
            }
        }
        return claimText;
    }

    public String claimText;
    public List<DrawingVO> drawings;                      // 도면

//    private String claims;
@JsonDeserialize(using = FtermDeseralizer.class)
    private List<String> description;

    @JsonDeserialize(using = FtermDeseralizer.class)
    private List<String> summary;
    private List<String> imgNames;

    public boolean isPatent() {
        return patent;
    }

    public void setPatent(boolean patent) {
        this.patent = patent;
    }

    public boolean isRegister() {
        return register;
    }

    public void setRegister(boolean register) {
        this.register = register;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }



    public List<String> getImgNames() {
        return imgNames;
    }

    public void setImgNames(List<String> imgNames) {
        this.imgNames = imgNames;
    }

    @Override
    public String toString() {
        return "PatentVO{" +
                "patent=" + patent +
                ", register=" + register +
                ", documentId='" + documentId + '\'' +
                ", documentNumber='" + documentNumber + '\'' +
                ", claims='" + claims + '\'' +
                ", summary='" + summary + '\'' +
                ", description='" + description + '\'' +
                ", imgNames=" + imgNames +
                '}';
    }

}
