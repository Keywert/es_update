package es.tmp.keylense;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DrawingSymbol {
    public Boolean visible;
    public Boolean notVisible;

    public String getImgFileOrigin() {
        if (imgFileOrigin != null) {
            String str = imgFileOrigin
                    .replaceAll("/PP","/pp")
                    .replaceAll("/RE", "/re");
            return str;
        }

        return imgFileOrigin;
    }

    public String imgFileOrigin;

    public String getImgName() {
        if (imgName != null) {
            if (imgName.matches(".+_l\\.(png|PNG|jpg|JPG)$"))
                return imgName.replaceAll("_l\\.",".");

            return imgName;
        }

        if (this.getImgFileOrigin() != null && !"".equals(this.getImgFileOrigin())) {
//            System.out.println(imgFile);
            String[] strs = this.getImgFileOrigin().split("/");
//            System.out.println(strs[strs.length-1]);
            return strs[strs.length-1];
        }
        return imgName;
    }

    public String imgName;

    public String getImgFile() {
        if (imgFile != null)
            return imgFile;

        if (this.getImgFileOrigin() != null) {
            if (this.getImgFileOrigin().matches(".*\\.(png|PNG|jpg|JPG)$"))
                return this.getImgFileOrigin();
        }
        return imgFile;
    }

    public String imgFile;
    public String imgFileL;
    public String getImgFileL() {
        if (imgFileL != null)
            return imgFileL;

        if (this.getImgFileOrigin() != null && !"".equals(this.getImgFileOrigin())) {
            return this.getImgFileOrigin().replaceAll("(_l)?\\.(jpg|JPG|png|PNG)$", "_l.jpg");

        }

        return imgFileL;
    }
    public String imgFileM;
    public String getImgFileM() {
        if (imgFileM != null)
            return imgFileM;

        if (this.getImgFileOrigin() != null) {
            return this.getImgFileOrigin().replaceAll("(_l)?\\.(jpg|JPG|png|PNG)$", "_m.jpg");
        }

        return imgFileM;
    }
    public String imgFileS;
    public String getImgFileS() {
        if (imgFileS != null)
            return imgFileS;

        if (this.getImgFileOrigin() != null) {
            return this.getImgFileOrigin().replaceAll("(_l)?\\.(jpg|JPG|png|PNG)$", "_s.jpg");
        }
        return imgFileS;
    }
    public Integer width;
    public Integer height;
    List<Symbol> symbols;
}
