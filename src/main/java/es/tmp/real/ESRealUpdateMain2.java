package es.tmp.real;

import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.tmp.ESHealthCheck;
import es.tmp.update.ESDataCheck;
import es.tmp.update.ESIndexing;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ESRealUpdateMain2 {
    public static void main(String[] args) {
        // 1. ES Health Check
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));

        //alert 리스트
        esConfigs.add(new ESConfig("es209"));

        if (esConfigs.isEmpty())
            return;

        int size = esConfigs.size();
//        System.out.println(esConfigs.size());

        Map<String,ESConfig> datafarmESMap = new HashMap<>();
        datafarmESMap.put("es18",new ESConfig("es18"));
        datafarmESMap.put("es188",new ESConfig("es188"));
        datafarmESMap.put("es189",new ESConfig("es189"));

        DataFarmDB dataFarmDB = new DataFarmDB();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        String updateDate = dtf.format(localDate);

        int workSize = 500;
        int queue_size = 1000;

        int[] levels = {0};
//        int[] levels = {0,1};
//        int[] levels = {0,1,3,4};
//        int[] levels = {0,1,3,4,5,6,7,8,9};
//        int[] levels = {5,6,7,8,9};
        int levelIndex = 0;

        for (int i = 0; i < workSize; i++) {
            long start = System.currentTimeMillis();

            List<ESConfig> esc = esConfigs;
            if (levels[levelIndex] != 1)
                esc = esConfigs.subList(0,size-1);

            System.out.println( i + "/" + workSize + " - " + levels[levelIndex] + " level ES Server Count : " +  esc.size());

            int result = esReindex(datafarmESMap,esc,levels[levelIndex], dataFarmDB, queue_size, updateDate);
            if (result == -1)
                break;
            else if (result == 0) {
                System.out.println(levels[levelIndex] + " level is Finished~!");
                levelIndex++;
                if (levelIndex > levels.length-1)
                    break;
            }

            long end = System.currentTimeMillis();
            System.out.println("실행 시간 : " + (end - start) / 1000.0 + " 평균 시간 : " + ((end - start) / 1000.0) / queue_size);
            System.out.println();
        }

        try {
            for (ESConfig esConfig : esConfigs) {
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
            }

            for (ESConfig esConfig : datafarmESMap.values()) {
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static int esReindex(Map<String, ESConfig> datafarmESMap, List<ESConfig> esConfigs, int level, DataFarmDB dataFarmDB, int queue_size, String updateDate) {
        ESHealthCheck esHealthCheck = new ESHealthCheck();
        if (!esHealthCheck.esClusterHealthCheck(esConfigs))
            return -1;

//        for (ESConfig esConfig : esConfigs)
//            if (!esHealthCheck.healthCheck(esConfig)) {
//                System.out.println(esConfig.getSettings().get("cluster.name"));
//                System.out.println(" Real Cluster Health Status is not normal.");
//                return -1;
//            }

        // 2. Queue 확인
        Map<String, List<String>> map = dataFarmDB.selectQueue(level,queue_size);
        if (map == null)
            return 0;

        // 3. esUpdate
        ESIndexing esIndexing = new ESIndexing();
        for (String index : map.keySet()) {
            List<String> docIds = map.get(index);
            if (docIds == null || docIds.isEmpty())
                continue;

            String clusterName = dataFarmDB.selectClusterInfo(index, 1);
            if (clusterName == null)
                continue;

            System.out.println(index + " : " + clusterName);
            ESConfig esConfigSrc = datafarmESMap.get(clusterName);
//            ESConfig esConfigSrc = new ESConfig(clusterName);
            if (esConfigSrc == null)
                return -1;

            if (!esHealthCheck.healthCheck2(esConfigSrc)) {
                System.out.println(clusterName + " Cluster Health Status is not normal.");
                continue;
            }

            int count = esIndexing.indexing(esConfigSrc, esConfigs, index, docIds, updateDate ,level);
            if (count != docIds.size()) {
                System.out.println(count + " - " + docIds.size() + " - " + index + " Update Fail~!");
                return -1;
            }

            System.out.println(count + " - " + docIds.size() + " - " + index + " indexing Success ~!");

            if (level == 1) {
                esIndexing.firstDateUpdate(esConfigSrc,index,updateDate,docIds);
            }
        }

        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Update 확인
        ESDataCheck esDataCheck = new ESDataCheck();
        if (!esDataCheck.updateDataCheck(esConfigs, map, updateDate)) {
            System.out.println("ES Data Update Fail~! - retry ");
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (!esDataCheck.updateDataCheck(esConfigs, map, updateDate)) {
                System.out.println("ES Data Update Fail~! ");
                return -1;
            }
        }


        dataFarmDB.updateDB(map);
        System.out.println("ES Update Success");
        System.out.println();

        return 1;
    }


    public static int esReindexbk(List<ESConfig> esConfigs, int level, int queue_size, String updateDate, int indexLevel) {
        ESHealthCheck esHealthCheck = new ESHealthCheck();
        for (ESConfig esConfig : esConfigs)
            if (!esHealthCheck.healthCheck(esConfig)) {
                System.out.println(" Real Cluster Health Status is not normal.");
                return -1;
            }

        // 2. Queue 확인
        DataFarmDB dataFarmDB = new DataFarmDB();
        Map<String, List<String>> map = dataFarmDB.selectQueue(level, queue_size);
        if (map == null)
            return -1;

        // 3. esUpdate
        ESIndexing esIndexing = new ESIndexing();
        for (String index : map.keySet()) {
            List<String> docIds = map.get(index);
            if (docIds == null || docIds.isEmpty())
                continue;

            String clusterName = dataFarmDB.selectClusterInfo(index, 1);
            if (clusterName == null)
                continue;

            System.out.println(index + " : " + clusterName);
            ESConfig esConfigSrc = new ESConfig(clusterName);
            if (esConfigSrc == null)
                return -1;

            if (!esHealthCheck.healthCheck2(esConfigSrc)) {
                System.out.println(clusterName + " Cluster Health Status is not normal.");
                continue;
            }

            int count = esIndexing.indexing(esConfigSrc, esConfigs, index, docIds, updateDate, indexLevel);
            if (count != docIds.size()) {
                System.out.println(count + " - " + docIds.size() + " - " + index + " Update Fail~!");
                return -1;
            }

            System.out.println(count + " - " + docIds.size() + " - " + index + " indexing Success ~!");
        }

        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Update 확인
        ESDataCheck esDataCheck = new ESDataCheck();
        if (!esDataCheck.updateDataCheck(esConfigs, map, updateDate)) {
            System.out.println("ES Data Update Fail~!");
            return -1;
        }

        dataFarmDB.updateDB(map);
        System.out.println("ES Update Success");


        return 0;
    }

}
