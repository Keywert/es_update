package es.tmp.pair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class EpoUpdate {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();

        esConfigs.add(new ESConfig("es188"));
        if (esConfigs.isEmpty())
            return;

        DataFarmDB dataFarmDB = new DataFarmDB();

        String index = "epo";
        String type = "patent";

        String startDate = "20230101";
        String endDate = "20251231";

        String[] fields = {"documentId", "applicationNumber", "claims", "summary", "description"};
        String[] documentIds = {"ep2019195814b1"};

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(documentIds);

        QueryBuilder getApNumberQb = QueryBuilders.boolQuery()
                .must(
                        QueryBuilders.boolQuery()
                                .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("summary")))
                                .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("description")))
                                .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("claims")))
                )
//                .must(QueryBuilders.existsQuery("internationalApplicationNumber"))
                .must(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
                ;

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(getApNumberQb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        int count = 0;


        Map<String, Map<String, Object>> map = new HashMap<>();
        ArrayList<String> docIds = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                if (++count % 10000 == 0)
                    System.out.println(count);

                Object applicationNumber = hit.getSource().get("applicationNumber");
                if (applicationNumber == null)
                    continue;


                map.put(applicationNumber.toString(), hit.getSource());
                docIds.add(hit.getId());

                if (map.size() > 50) {
                    update(esConfigs,dataFarmDB, docIds, map);
                    map.clear();
                    docIds.clear();
                }
            }
            scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        update(esConfigs,dataFarmDB, docIds, map);

        try {
            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static void update(List<ESConfig> esConfigs,DataFarmDB dataFarmDB, ArrayList<String> docIds, Map<String, Map<String, Object>> map) {
        String index = "epo";
        String type = "patent";

        String[] fields = {"applicationNumber", "claims", "summary", "description", "summaries"};

        QueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("summary"))
                .filter(QueryBuilders.existsQuery("description"))
                .filter(QueryBuilders.existsQuery("claims"))
                .filter(QueryBuilders.termsQuery("applicationNumber", map.keySet()))
                .mustNot(QueryBuilders.existsQuery("referenceDocumentId"))
                .mustNot(QueryBuilders.termsQuery("documentId", docIds));

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

//        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());
//
        ObjectMapper objectMapper = new ObjectMapper();
        int count = 0;
        List<QueueVO> queueVOList = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object applicationNumber = hit.getSource().get("applicationNumber");
                if (applicationNumber == null)
                    continue;

                Map<String, Object> destMap = map.get(applicationNumber.toString());
                Map<String, Object> srcMap = hit.getSource();

                // 존재하는 필드 삭제 "claims", "summary", "description" 삭제
                for (String key : destMap.keySet())
                    srcMap.remove(key);

                if (srcMap.isEmpty())
                    continue;

                //  "claims", "summary", "description" 필드 삭제
                for (String key : fields)
                    destMap.remove(key);

                destMap.putAll(srcMap);

                Object documentId = destMap.get("documentId");

                if (documentId == null)
                    continue;


                destMap.put("referenceData", srcMap.keySet());
                destMap.put("referenceDocumentId", hit.getId());

                destMap.remove("documentId");

                try {
                    count++;
                    String json = objectMapper.writeValueAsString(destMap);

                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId.toString())
                                .doc(json.getBytes())
                                .upsert(json.getBytes()));

                    QueueVO queueVO = new QueueVO();
                    queueVO.setEsindex("epo");
                    queueVO.setDocumentId(documentId.toString());
                    queueVO.setLevel(9);
                    queueVOList.add(queueVO);

                    if (queueVOList.size() > 10) {
                        dataFarmDB.insertESQueue(queueVOList);
                        queueVOList.clear();
                    }

                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
            scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);
        if (!queueVOList.isEmpty())
            dataFarmDB.insertESQueue(queueVOList);
        System.out.println("Update Count : " + count);

    }
}
