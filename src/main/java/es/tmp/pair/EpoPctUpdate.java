package es.tmp.pair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class EpoPctUpdate {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();

        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es205"));
//        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es13"));

        ESConfig pctESConfig = new ESConfig("es18");

        if (esConfigs.isEmpty())
            return;

        DataFarmDB dataFarmDB = new DataFarmDB();


        String index = "epo";
        String type = "patent";

        String startDate = "20200101";
        String endDate = "20251231";

        String[] fields = {"documentId", "claims", "summary", "description", "internationalApplicationNumber", "applicationNumber", "inventionTitles"};
//        String[] fields = { "documentId", "summary", "internationalApplicationNumber","applicationNumber"};

        String[] ids = {
                "ep2021738329a1"
        };
        QueryBuilder getApNumberQb = QueryBuilders.boolQuery()
//                .must(QueryBuilders.idsQuery().addIds(ids))
                .filter(
                        QueryBuilders.boolQuery()
                                .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("summary")))
                                .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("description")))
                                .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("claims")))
                )
                .filter(QueryBuilders.existsQuery("internationalApplicationNumber"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
                ;

        System.out.println(getApNumberQb);

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(getApNumberQb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();


        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        Map<String, Map<String, Object>> map = new HashMap<>();
        ArrayList<String> docIds = new ArrayList<>();

        int count = 0;
        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                if (++count % 10000 == 0)
                    System.out.println(count);

                Object applicationNumber = hit.getSource().get("internationalApplicationNumber");
                if (applicationNumber == null)
                    continue;

//                System.out.println(hit.getId() +" : "+ applicationNumber);
                map.put(applicationNumber.toString(), hit.getSource());
                docIds.add(hit.getId());

                if (map.size() > 50) {
                    update(esConfigs,pctESConfig,dataFarmDB, docIds, map);
                    map.clear();
                    docIds.clear();
                }
            }
            scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!map.isEmpty())
            update(esConfigs,pctESConfig,dataFarmDB, docIds, map);

        try {
            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static void update(List<ESConfig> esConfigs,ESConfig pctesConfig,DataFarmDB dataFarmDB, ArrayList<String> docIds, Map<String, Map<String, Object>> map) {
        String index = "pct";
        String type = "patent";

        String[] fields = {"applicationNumber", "claims", "summary", "description", "summaries", "inventionTitles"};
//        String[] fields = { "applicationNumber", "summary"};


        QueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.existsQuery("summary"))
                .must(QueryBuilders.existsQuery("description"))
                .must(QueryBuilders.existsQuery("claims"))
                .must(QueryBuilders.termsQuery("applicationNumber", map.keySet()));

        SearchResponse scrollResp = pctesConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        ObjectMapper objectMapper = new ObjectMapper();
        int count = 0;
        List<QueueVO> queueVOList = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object applicationNumber = hit.getSource().get("applicationNumber");
                if (applicationNumber == null)
                    continue;


                Map<String, Object> destMap = map.get(applicationNumber.toString());
                Map<String, Object> srcMap = hit.getSource();

                // 존재하는 필드 삭제 "claims", "summary", "description" 삭제
                for (String key : destMap.keySet())
                    srcMap.remove(key);

                if (srcMap.isEmpty())
                    continue;

                //  "claims", "summary", "description" 필드 삭제
                for (String key : fields)
                    destMap.remove(key);

                destMap.putAll(srcMap);

                Object documentId = destMap.get("documentId");

                if (documentId == null)
                    continue;


                destMap.put("referenceData", srcMap.keySet());
                destMap.put("referenceDocumentId", hit.getId());

                destMap.remove("documentId");
                destMap.remove("internationalApplicationNumber");

                try {
                    count++;
                    String json = objectMapper.writeValueAsString(destMap);
//                    System.out.println(json);

//                    if (json.contains(".tif"))
//                        continue;

//                    System.out.println(documentId + " : " + json);
                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new UpdateRequest("epo", type, documentId.toString())
                                .doc(json.getBytes())
                                .upsert(json.getBytes()));

                    QueueVO queueVO = new QueueVO();
                    queueVO.setEsindex("epo");
                    queueVO.setDocumentId(documentId.toString());
                    queueVO.setLevel(9);
                    queueVOList.add(queueVO);

                    if (queueVOList.size() > 10) {
                        dataFarmDB.insertESQueue(queueVOList);
                        queueVOList.clear();
                    }

                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
            scrollResp = pctesConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(100000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!queueVOList.isEmpty())
            dataFarmDB.insertESQueue(queueVOList);

        System.out.println("Update Count : " + count);

    }
}
