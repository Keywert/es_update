package es.tmp;

import es.config.ESConfig;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.List;

public class ESHealthCheck {
    public boolean esClusterHealthCheck(List<ESConfig> esConfigs) {
        int c = 5;
        try {
            while (c-- != 0) {
                if(essHealthCheck(esConfigs))
                    return true;

                Thread.sleep(30000);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean essHealthCheck(List<ESConfig> esConfigs) {
        for (ESConfig esConfig : esConfigs)
            if (!healthCheck(esConfig)) {
                System.out.println(esConfig.getSettings().get("cluster.name"));
                System.out.println(" Real Cluster Health Status is not normal.");
                return false;
            }

        return true;
    }

    public boolean healthCheck(ESConfig esConfig) {
        if (!getStatus(esConfig))
            return false;

        if (!searchTimeCheck(esConfig))
            return false;

        return true;
    }

    private boolean searchTimeCheck(ESConfig esConfig) {
        String[] index = {"kipo", "aupo", "jpo"};
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("inventionTitle", "car"));

        SearchResponse response = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setSize(0)
                .execute()
                .actionGet();

        if (response.getTook().getMillis() < 3000)
            return true;

        System.out.println("Search Time : " + response.getTook());

        return false;
    }

    private boolean getStatus(ESConfig esConfig) {
        ClusterHealthResponse healths = esConfig.client.admin().cluster().prepareHealth().get();
//        String clusterName = healths.getClusterName();
//        int numberOfDataNodes = healths.getNumberOfDataNodes();
//        int numberOfNodes = healths.getNumberOfNodes();

        ClusterHealthStatus status = healths.getStatus();
        if (!status.equals(ClusterHealthStatus.GREEN)) {
//            if (!status.equals(ClusterHealthStatus.GREEN) && !status.equals(ClusterHealthStatus.YELLOW)) {

            System.out.println("ES Cluster Status : " + status);
            return false;
        }

        return true;
    }

    public boolean healthCheck2(ESConfig esConfig) {
        ClusterHealthResponse healths = esConfig.client.admin().cluster().prepareHealth().get();
//        String clusterName = healths.getClusterName();
//        int numberOfDataNodes = healths.getNumberOfDataNodes();
//        int numberOfNodes = healths.getNumberOfNodes();

        ClusterHealthStatus status = healths.getStatus();
        if (status.equals(ClusterHealthStatus.RED)) {
            System.out.println("ES Cluster Status : " + status);
            return false;
        }

        return true;
    }

    public String healthCheck3(ESConfig esConfig) {
        ClusterHealthResponse healths = esConfig.client.admin().cluster().prepareHealth().get();
//        String clusterName = healths.getClusterName();
//        int numberOfDataNodes = healths.getNumberOfDataNodes();
//        int numberOfNodes = healths.getNumberOfNodes();

        ClusterHealthStatus status = healths.getStatus();
        if (!status.equals(ClusterHealthStatus.GREEN)) {
            System.out.println("ES Cluster Status : " + status);
            return status.toString();
        }

        return "GREEN";
    }

}
