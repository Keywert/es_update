package es.tmp.family.wert;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.model.PriorityClaim;
import es.module.utility.NumberUtil_Priority;
import es.module.utility.NumberUtils;
import lombok.Data;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentFamilyVO {
    public HashSet<String> citationNumbers;
    public String citationRegisterNumber;
    public String citationDocumentId;                  // 문서 ID
    public String documentId;                  // 문서 ID
    public String publishingORG;               // 발간청

    public String getCountryCode() {
        if (countryCode == null)
            countryCode = publishingORG;

        return countryCode;
    }

    public String mainIpc;
    public List<String> subIpc;

    public String countryCode;                 // - docdb 국가
    public String openNumber;                  // 공개번호
    public String publicationNumber;           // (openNumber가 없을 시)공개번호
    public String registerNumber;              // 등록번호f
    public String registerDate;                // 등록일자
    public String openDate;                   // 공개일자(To)
    public String publicationDate;            // 공개일자(To)
    public String documentType;                // 문헌종류
    public String documentTypeJP;              // 문헌종류 일본만
    public String documentTypeUS;              // 문헌종류 일본만

    public String applicationType;
    public String epoPublicationNumber;

    public String originalApplicationNumber;
    public String originalApplicationDate;
    public String originalApplicationKind;

    public String documentNumber;
    public String applicationCountryCode;
    public String standardApplicationNumber;
    public String inventionTitle;


    public String getFamilyId() {
        if (!"".equals(familyId) && familyId != null)
            return familyId;
        else if (!"".equals(docdbFamilyId) && docdbFamilyId != null)
            return docdbFamilyId;
        else
            return null;
    }

    public String familyId;
    public String docdbFamilyId;
    public String inpadocFamilyId;
    public String extendedFamilyId;
    public String epoApplicationNumber;

    public String patentType;
    public String patentNumber;
    public String patentDate;


    public String applicationNumber;           // 번호
    public String applicationDate;            // 출원일
    public String patentId;

    public Boolean getFamilyFlag() {
        if (familyFlag == null)
            return false;
        return familyFlag;
    }

    public Boolean familyFlag;
    public String index;

    public String announcementNumber;          // 공고 번호
    public String announcementDate;            // 공고 일자

    public String inventionTitleJP;            // 발명의 일본문헌
    public String inventionTitleCN;            // 발명의 중국문헌
    public String inventionTitleKO;            // 발명의 중국문헌 / 번역
    public String inventionTitleDE;            // 발명의 중국문헌 / 번역
    private String internationalApplicationNumber;      // 국제 출원번호
    private String internationalApplicationDate;        // 국제 출원일자
    private String internationalPublicationNumber;        // 국제 공개번호
    private String internationalPublicationDate;          // 국제 공개일자
    public List<PriorityClaim> priorityClaims;             // 우선권 관련정보
    public String priorityClaimCountry;
    public String priorityClaimNumber;
    public String priorityClaimDate;
    public String priorityClaimMasterNumber;

    public String getApplicationNumber() {
        if (applicationNumber != null)
            return applicationNumber;
        else if (epoApplicationNumber != null)
            return epoApplicationNumber;
        else if (originalApplicationNumber != null)
            return originalApplicationNumber;

        return null;
    }

    public Integer getDocType() {
        if (documentType == null)
            return 1;
        else if (documentType.matches("^[U|Y][0-9]?$"))
            return 2;
        else if (documentType.matches("^[S][0-9]?$"))
            return 3;

        return 1;
    }

    public Integer docType;  // defalut 1:특허, 2:실용신안, 3:디자인

    public Set<String> getApnums() {
        Set<String> numberSet = new HashSet<>();
        String cc = this.getCountryCode();
        String type = this.getPatentType();
        // docdb를 제외한 국가는 원본 출원을 입력, docdb는 가공해서 입력
        String applicationNumber = this.getApplicationNumber();
        if (applicationNumber != null) {
            numberSet.add(applicationNumber);
            numberSet.add(applicationNumber.replaceAll("-", ""));

            if (cc.matches("KR") && !"docdb".equals(this.getIndex())) {
                if (type.matches("^(A|B)$"))
                    numberSet.add("10" + applicationNumber.replaceAll("-", ""));
                else
                    numberSet.add("20" + applicationNumber.replaceAll("-", ""));
            }
        }

        if ("docdb".equals(this.getIndex())) {
            String apnum = NumberUtils.getNumber(cc, applicationNumber, "AN");
            if (apnum != null)
                numberSet.add(apnum);
        }

        String epoApplicationNumber = this.getEpoApplicationNumber();
        if (epoApplicationNumber != null) {
            numberSet.add(epoApplicationNumber);
            String apnum = NumberUtils.getNumber(cc, epoApplicationNumber, "AN");
            if (apnum != null)
                numberSet.add(apnum);
        }

        String originalApplicationNumber = this.getOriginalApplicationNumber();
        if (originalApplicationNumber != null) {
            if ("docdb".equals(this.getIndex())) {
                String apnum = NumberUtils.getNumber(cc, originalApplicationNumber, "AN");
                if (apnum != null)
                    numberSet.add(apnum);
            }
        }

        return numberSet;
    }

    public Set<String> apnums;

    public boolean comparePatent(PatentFamilyVO patentFamilyVO) {
        if (patentFamilyVO == null)
            return false;

        if (patentFamilyVO.getCountryCode() == null)
            return false;

        if (!this.getCountryCode().equals(patentFamilyVO.getCountryCode()))
            return false;

        if (this.getDocType() != patentFamilyVO.getDocType())
            return false;

        System.out.println(patentFamilyVO.getDocumentId());
        for (String apNum : this.getApnums()) {
            System.out.println(apNum);
            if (patentFamilyVO.getApnums().contains(apNum))
                return true;
        }


        return false;
    }

//    public BoolQueryBuilder getBQB() {return  null;}

    public BoolQueryBuilder makeBQB() {
        List<BoolQueryBuilder> bqbList = new ArrayList<>();

        String index = this.getIndex();
        String country = this.getPublishingORG();
        if (country == null)
            country = this.getCountryCode();

        if (country == null)
            return null;
        String patType = this.getPatentType();

        String[] country_fields = {"countryCode", "publishingORG"};

        List<String> docTypes = new ArrayList<>();
        String[] pType = {"A", "B"};
        String[] uType = {"U", "Y"};
        String[] eType = {"E"};
        String[] sType = {"S"};
        String[] tType = {"P"};

        if (patType == null)
            patType = "A";

        if (patType.matches("^(A|B)$"))
            docTypes.addAll(Arrays.asList(pType));
        else if (patType.matches("^(U|Y)$"))
            docTypes.addAll(Arrays.asList(uType));
        else if (patType.matches("^(S)$"))
            docTypes.addAll(Arrays.asList(sType));
        else if (patType.matches("^(E)$"))
            docTypes.addAll(Arrays.asList(eType));
        else if (patType.matches("^(P)$"))
            docTypes.addAll(Arrays.asList(tType));

        String appDate = this.getApplicationDate();
        String pDate = this.getPatentDate();

        // 출원번호
        Set<String> appNums = this.getApnums();
        if (!appNums.isEmpty()) {
            Set<String> numSet =  NumberUtils.checkNumber(country,appNums,"AN");

            BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                    .mustNot(QueryBuilders.termQuery("_index", "docdb"))
                    .filter(QueryBuilders.multiMatchQuery(country, country_fields))
                    .filter(QueryBuilders.termsQuery("patentType",docTypes))
                    .filter(QueryBuilders.boolQuery()
                            .should(QueryBuilders.termsQuery("applicationNumber", appNums))
                    );

            if (appDate != null && !"".equals(appDate))
                bq01.filter(QueryBuilders.rangeQuery("applicationDate").from(appDate).to(appDate));

            bqbList.add(bq01);

            BoolQueryBuilder bq02 = QueryBuilders.boolQuery()
                    .filter(QueryBuilders.termQuery("_index", "docdb"))
                    .filter(QueryBuilders.termQuery("applicationCountry", country.toLowerCase()))
                    .filter(QueryBuilders.termQuery("publicationCountry", country))
                    .filter(QueryBuilders.termsQuery("patentType",docTypes))
                    .filter(QueryBuilders.multiMatchQuery(country, country_fields))
                    .filter(QueryBuilders.boolQuery()
                            .should(QueryBuilders.termsQuery("applicationNumber", appNums))
                            .should(QueryBuilders.termsQuery("originalApplicationNumber", appNums))
                            .should(QueryBuilders.termsQuery("epoApplicationNumber", appNums)));

            if (appDate != null && !"".equals(appDate))
                bq02.filter(QueryBuilders.rangeQuery("applicationDate").from(appDate).to(appDate));

            bqbList.add(bq02);
        }

        // 문헌번호
        if (!country.matches("JP")) {
            Set<String> pubNums = this.getPnums();
            if (!pubNums.isEmpty()) {
                BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                        .mustNot(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.multiMatchQuery(country, country_fields))
                        .filter(QueryBuilders.termsQuery("patentType", docTypes))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("patentNumber", pubNums))
                                .should(QueryBuilders.termsQuery("publicationNumber", pubNums))
                                .should(QueryBuilders.termsQuery("registerNumber", pubNums))
                        );

                if (pDate != null && !"".equals(pDate))
                    bq01.filter(QueryBuilders.rangeQuery("patentDate").from(pDate).to(pDate));

                bqbList.add(bq01);

                BoolQueryBuilder bq02 = QueryBuilders.boolQuery()
                        .filter(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.termQuery("applicationCountry", country.toLowerCase()))
                        .filter(QueryBuilders.termQuery("publicationCountry", country))
                        .filter(QueryBuilders.termsQuery("patentType", docTypes))
                        .filter(QueryBuilders.multiMatchQuery(country, country_fields))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("epoPublicationNumber", pubNums))
                                .should(QueryBuilders.termsQuery("patentNumber", pubNums))
                                .should(QueryBuilders.termsQuery("publicationNumber", pubNums))
                                .should(QueryBuilders.termsQuery("registerNumber", pubNums))
                        );

                if (pDate != null && !"".equals(pDate))
                    bq02.filter(QueryBuilders.rangeQuery("patentDate").from(pDate).to(pDate));

                bqbList.add(bq02);
            }
        }

        // 원출원번호
        String originalApplicationNumber = this.getOriginalApplicationNumber();
        if (originalApplicationNumber != null && !"docdb".equals(this.getIndex())) {
            Set<String> numSet = new HashSet<>();
            String otype = this.getOriginalApplicationKind();
            if(otype == null)
                otype = "A";
            String type = this.getPatentType();
            List<String> oriTypes = new ArrayList<>();
            String cc = this.getCountryCode();

            if ("KR".equals(cc) || "JP".equals(cc)) {
                if (originalApplicationNumber.matches("^20-?(19|20)[0-9]{2}-?[0-9]{7}\\s?[U|Y]?$"))
                    oriTypes.addAll(Arrays.asList(uType));
                else if (originalApplicationNumber.matches("^[A-z]{2}[0-9]+[U|Y][0-9]?$"))
                    oriTypes.addAll(Arrays.asList(uType));
                else if (originalApplicationNumber.matches("^[0-9]+[U|Y][0-9]?$"))
                    oriTypes.addAll(Arrays.asList(uType));
                else if (otype.matches("^U[0-9]?$"))
                    oriTypes.addAll(Arrays.asList(uType));
                else
                    oriTypes.addAll(Arrays.asList(pType));

                numSet.add(originalApplicationNumber);
                numSet.add(originalApplicationNumber.replaceAll("-",""));

//                System.out.println(originalApplicationNumber);

                String apnum = NumberUtils.getNumber(cc, originalApplicationNumber, "AN");
                if (apnum != null) {
                    numSet.add(apnum);
                    if ("KR".equals(cc) && apnum.matches("^(10|20)-(19|20)[0-9]{2}-[0-9]{7}$")) {
                        numSet.add(apnum.substring(3));
                    }
                }

                BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                        .mustNot(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.multiMatchQuery(cc, country_fields))
                        .filter(QueryBuilders.termsQuery("patentType",oriTypes))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("applicationNumber", numSet))
                        );

                bqbList.add(bq01);

                BoolQueryBuilder bq02 = QueryBuilders.boolQuery()
                        .filter(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.termQuery("applicationCountry", cc.toLowerCase()))
                        .filter(QueryBuilders.termQuery("publicationCountry", cc))
                        .filter(QueryBuilders.termsQuery("patentType",oriTypes))
                        .filter(QueryBuilders.multiMatchQuery(cc, country_fields))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("applicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("originalApplicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("epoApplicationNumber", numSet)));

                bqbList.add(bq02);
            }
        }

        // 우선권번호
        List<PriorityClaim> priorityClaims = this.getPriorityClaims();
        if (priorityClaims != null) {
            for (PriorityClaim priorityClaim : priorityClaims) {
                Set<String> numSet = new HashSet<>();
                Set<String> oriTypes = new HashSet<>();
                String num = priorityClaim.getPriorityApplicationNumber();
                String docdbNum = priorityClaim.getDocdbApplicationNumber();
                String pcc = priorityClaim.getPriorityApplicationCountry();
                String pad = priorityClaim.getPriorityApplicationDate();


                if (pcc == null)
                    continue;

                if (this.getApplicationNumber() != null)
                    if (this.getApplicationNumber().equals(num))
                        continue;


                String nationalApplicationNumber = priorityClaim.getNationalApplicationNumber();
                if (nationalApplicationNumber != null)
                    numSet.add(nationalApplicationNumber);

                String epodocApplicationNumber = priorityClaim.getEpodocApplicationNumber();
                if (epodocApplicationNumber != null)
                    numSet.add(epodocApplicationNumber);

                String date = priorityClaim.getPriorityApplicationDate();

                if (date != null)
                    date = date.replaceAll("[^0-9]", "");

                if (num != null) {
                    num = num.replaceAll("[\\s|,]", "");
                    numSet.add(num);
                    numSet.add(num.replaceAll("-", ""));
                    String number = NumberUtil_Priority.getNumber(pcc, num, "AN").replaceAll("\\.", "");
                    if (number != null) {
                        numSet.add(number);
                        if (number.matches("^PCT/[A-z]{2}(19|20)[0-9]{2}/[0-9]{6}$"))
                            pcc = "WO";
                    }

                    number = NumberUtils.getNumber(pcc, num, "AN").replaceAll("\\.", "");
                    if (number != null)
                        numSet.add(number);
                }

                if (docdbNum != null) {
                    docdbNum = docdbNum.replaceAll("[\\s|,]", "");
                    numSet.add(docdbNum);
                    numSet.add(docdbNum.replaceAll("-", ""));
                    String docdbNumber = NumberUtil_Priority.getNumber(pcc, num, "AN");
                    if (docdbNumber != null) {
                        numSet.add(docdbNumber.replaceAll("\\.", ""));
                        if (docdbNumber.matches("^PCT/[A-z]{2}(19|20)[0-9]{2}/[0-9]{6}$"))
                            pcc = "WO";
                    }

                    docdbNumber = NumberUtils.getNumber(pcc, docdbNumber, "AN");
                    if (docdbNumber != null)
                        numSet.add(docdbNumber.replaceAll("\\.", ""));
                }

                String ankr = "";
                // 특허 실용 구분
                if (pcc != null && !"".equals(pcc)) {
                    if (pcc.matches("^(KR|JP)$")) {
                        for (String apnum : numSet) {
                            if (apnum.matches("^(10|20)-(19|20)[0-9]{2}-[0-9]{7}$")) {
                                ankr = apnum.replaceAll("^(10|20)-", "");
                            }

                            if (apnum.matches("^20-?(19|20)[0-9]{2}-?[0-9]{7}\\s?[U|Y]?[0-9]?$")) {
                                oriTypes.addAll(Arrays.asList(uType));
                                break;
                            }

                            if (apnum.matches("^(JP|KR)[0-9]+[U|Y][0-9]?$")) {
                                oriTypes.addAll(Arrays.asList(uType));
                                break;
                            }

                            if (apnum.matches("^[0-9]+[U|Y][0-9]?$")) {
                                oriTypes.addAll(Arrays.asList(uType));
                                break;
                            }
                        }
                    } else if (pcc.matches("CN")) {
                        oriTypes.addAll(Arrays.asList(pType));

                        for (String apnum : numSet) {
                            if (apnum.matches("^(19|20)[0-9]{2}-20[0-9]{6}$")) {
                                oriTypes.addAll(Arrays.asList(uType));
                                break;
                            }
                        }
                    }
                }

                if (!"".equals(ankr))
                    numSet.add(ankr);

                if (oriTypes.isEmpty())
                    oriTypes.addAll(Arrays.asList(pType));

                BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                        .mustNot(QueryBuilders.matchQuery("documentType","D0"))
                        .mustNot(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.multiMatchQuery(pcc, country_fields))
                        .filter(QueryBuilders.termsQuery("patentType", oriTypes))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("applicationNumber", numSet))
                        );

                if (pad != null && !"".equals(pad))
                    bq01.filter(QueryBuilders.rangeQuery("applicationDate").from(pad).to(pad));

                bqbList.add(bq01);

                BoolQueryBuilder bq02 = QueryBuilders.boolQuery()
                        .mustNot(QueryBuilders.matchQuery("documentType","D0"))
                        .filter(QueryBuilders.termQuery("_index", "docdb"))
                        .filter(QueryBuilders.termQuery("applicationCountry", pcc.toLowerCase()))
                        .filter(QueryBuilders.termQuery("publicationCountry", pcc))
                        .filter(QueryBuilders.termsQuery("patentType", oriTypes))
                        .filter(QueryBuilders.multiMatchQuery(pcc, country_fields))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("applicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("originalApplicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("epoApplicationNumber", numSet)));

                if (pad != null && !"".equals(pad))
                    bq02.filter(QueryBuilders.rangeQuery("applicationDate").from(pad).to(pad));

                bqbList.add(bq02);

                BoolQueryBuilder bq03 = QueryBuilders.boolQuery()
                        .filter(QueryBuilders.termQuery("priorityClaims.priorityApplicationCountry", pcc))
                        .filter(QueryBuilders.termsQuery("priorityClaims.priorityApplicationNumber", numSet))
                        ;

                if (pad != null && !"".equals(pad))
                    bq03.filter(QueryBuilders.rangeQuery("priorityClaims.priorityApplicationDate").from(pad).to(pad));

//                System.out.println(bq03);

                bqbList.add(bq03);

            }
        }

        // 국제  internationalApplicationNumber", "internationalPublicationNumber",
        if (internationalApplicationNumber != null) {
            Set<String> numSet = new HashSet<>();
            if (internationalApplicationNumber.matches("^PCT.+"))
                numSet.add(internationalApplicationNumber);

            String apnum = NumberUtils.getNumber("PCT", internationalApplicationNumber, "AN");
            if (apnum != null)
                if (apnum.matches("^PCT.+"))
                    numSet.add(apnum);

            if (!numSet.isEmpty()) {
                BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                        .filter(QueryBuilders.multiMatchQuery("WO", country_fields))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("applicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("internationalApplicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("epoApplicationNumber", numSet)));

                bqbList.add(bq01);
            }
        }

        if (internationalPublicationNumber != null) {
            if (internationalPublicationNumber.matches("^WO.+")) {
                Set<String> numSet = new HashSet<>();
                numSet.add(internationalPublicationNumber);
                String pnum = NumberUtils.getNumber("PCT", internationalPublicationNumber, "PN");
                if (pnum != null)
                    numSet.add(pnum);

                BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                        .filter(QueryBuilders.multiMatchQuery("WO", country_fields))
                        .filter(QueryBuilders.boolQuery()
                                .should(QueryBuilders.termsQuery("patentNumber", numSet))
                                .should(QueryBuilders.termsQuery("internationalPublicationNumber", numSet))
                                .should(QueryBuilders.termsQuery("publicationNumber", numSet)));

                bqbList.add(bq01);
            }
        }


        if (!bqbList.isEmpty()) {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
            for (BoolQueryBuilder bq1 : bqbList) {

                boolQueryBuilder.should(bq1);
            }

            return boolQueryBuilder;
        }


        return null;
    }

    private Set<String> getPnums() {
        Set<String> pubNums = new HashSet<>();
        // docdb를 제외한 국가는 원본 출원을 입력, docdb는 가공해서 입력
        String patentNumber = this.getPatentNumber();
        if (patentNumber != null) {
            pubNums.add(patentNumber);
            pubNums.add(patentNumber.replaceAll("-", ""));
        }

        if ("docdb".equals(this.getIndex())) {
            String pnum = NumberUtils.getNumber(this.getCountryCode(), patentNumber, "PN");
            if (pnum != null)
                pubNums.add(pnum);
        }

        String epoPublicationNumber = this.getEpoPublicationNumber();
        if (epoPublicationNumber != null) {
            pubNums.add(epoPublicationNumber);
            String pnum = NumberUtils.getNumber(this.getCountryCode(), epoPublicationNumber, "PN");
            if (pnum != null)
                pubNums.add(pnum);
        }

        return pubNums;
    }
}
