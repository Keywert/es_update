package es.tmp.family.wert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

public class FamilyToDB {
    final static int JOB_SIZE = 20000;
    final static boolean updateflag = true;
    final static boolean singleflag = false;


    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        esConfigs.add(new ESConfig("es225"));
        if (esConfigs.isEmpty())
            return;

        // 패밀리 작업 문헌 - 우선권번호, 국제출원, 원출원번호가 있는 문헌을 대상을 추출
        Set<String> esDocIds = getTargetDocIds(esConfigs.get(0), JOB_SIZE);
        if (esDocIds == null)
            return;
////////////
        Set<String> docIds = selectFamilyDB(dbHelper, esDocIds);
        if (docIds == null)
            return;

        // 직접 문헌ID를 입력하여 패밀리 그룹핑 확인
//        Set<String> docIds = new HashSet<>();
//        docIds.add("us20240393406a1");
//        docIds.add("us20230384393a1");
//        docIds.add("us20180248138a1");

        List<PatentFamilyVO> patentList = null;
        int count = 0;
        for (String id : docIds) {
            System.out.println(id);
            if (++count % 10 == 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            HashMap<String, PatentFamilyVO> patentVOMap = GetFamilyPatents.getPatents2(esConfigs.get(0), null, id);
            if (patentVOMap == null)
                continue;

            patentList = checkFamily(conn, patentVOMap);
            if (patentList != null) {
                insertFamily(dbHelper, patentList);
            }
        }
    }

    private static void insertFamily(DBHelper dbHelper, List<PatentFamilyVO> patentList) {
        String insertQuery = "insert into family.extendedFamily (documentId, esindex, docdbFamilyId, extendedFamilyId, inventionTitle) " +
                " values ( ?, ?, ?, ?, ? ) " +
                " on duplicate key update " +
                " extendedFamilyId = values(extendedFamilyId), " +
//                " status = null "
                " status = IF (extendedFamilyId = values(extendedFamilyId), IF(status is null, null, 1) , null ) ";

        Connection conn = dbHelper.getConn_aurora2();

        try {
            PreparedStatement statement = null;

            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (PatentFamilyVO patentVO : patentList) {
                statement.setString(1, patentVO.getDocumentId());
                statement.setString(2, patentVO.getIndex());
                statement.setString(3, patentVO.getFamilyId());
                statement.setString(4, patentVO.getExtendedFamilyId());
                String iv = patentVO.getInventionTitle();
                if (iv != null && iv.length() > 256)
                    iv = iv.substring(0, 255);
                statement.setString(5, iv);

                statement.addBatch();
            }

            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static boolean checkFamilyList(List<PatentFamilyVO> patentList) {
        Set<String> familyIds = new HashSet<>();
        for (PatentFamilyVO familyVO : patentList) {
            String docdbFamilyId = familyVO.getFamilyId();
            if (docdbFamilyId == null)
                continue;

            familyIds.add(docdbFamilyId);
            if (familyIds.size() > 1)
                return true;
        }

        return false;
    }


    private static List<PatentFamilyVO> checkFamily(Connection conn, HashMap<String, PatentFamilyVO> patentVOMap) {
        System.out.println(patentVOMap.size());

        List<PatentFamilyVO> patentFamilyVOList = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
        PatentFamilyVO firstPatent = null;

        Set<String> familyIdSet = new HashSet<>();
        String firstExtendedFamilyId = null;
        Date firstDate = null;
        try {
            Iterator<String> iterator = patentVOMap.keySet().iterator();
            while (iterator.hasNext()) {
                String docId = iterator.next();
                PatentFamilyVO patentVO = patentVOMap.get(docId);
                System.out.println(patentVO.getDocdbFamilyId());
                if (firstPatent == null) {
                    firstPatent = patentVO;
                    String docdbFamilyId = patentVO.getDocdbFamilyId();
                    if (docdbFamilyId != null)
                        familyIdSet.add(docdbFamilyId);
                }

                String applicationDate = patentVO.getApplicationDate();
                if (applicationDate == null)
                    continue;

                String countryCode = patentVO.getCountryCode();
                if (countryCode == null)
                    continue;

                String docdbFamilyId = patentVO.getDocdbFamilyId();
                if (docdbFamilyId == null) {
                    for (PatentFamilyVO patentFamilyVO : patentVOMap.values()) {
                        String familyId = patentFamilyVO.getFamilyId();
                        if (familyId == null)
                            continue;

                        familyIdSet.add(familyId);

                        if (patentVO.comparePatent(patentFamilyVO)) {
                            patentVO.setDocdbFamilyId(patentFamilyVO.getFamilyId());
                            patentVO.setExtendedFamilyId(patentFamilyVO.getExtendedFamilyId());

                            System.out.println(patentVO.getDocumentId() + " : " + patentFamilyVO.getDocumentId());

                            patentVOMap.put(docId, patentVO);
                            break;
                        }
                    }
                } else
                    familyIdSet.add(docdbFamilyId);

                Date appDate = dateFormat.parse(applicationDate);
                if (firstDate == null || firstDate.getTime() >= appDate.getTime()) {
                    firstDate = appDate;
                    firstPatent = patentVO;
                    if (patentVO.getExtendedFamilyId() != null)
                        firstExtendedFamilyId = patentVO.getExtendedFamilyId();
                }

//            System.out.println(patentVO.getDocumentId());
                try {
                    ObjectMapper jsonMapper = new ObjectMapper();
//                    jsonMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
                    System.out.println(jsonMapper.writeValueAsString(patentVO));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

                patentVOMap.put(docId, patentVO);
            }

            if (firstExtendedFamilyId == null) {
                firstExtendedFamilyId = getExtendedFamilyId2(conn, firstPatent.getDocdbFamilyId(), familyIdSet);
            }

            if (firstExtendedFamilyId == null) {
                System.out.println("Get firstExtendedFamilyId Fail~!");
                return null;
            }

            for (PatentFamilyVO patentFamilyVO : patentVOMap.values()) {
                patentFamilyVO.setExtendedFamilyId(firstExtendedFamilyId);
                patentFamilyVOList.add(patentFamilyVO);
            }

            updateDB(conn, firstExtendedFamilyId, familyIdSet);

            if (!patentFamilyVOList.isEmpty())
                return patentFamilyVOList;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String getExtendedFamilyId2(Connection conn, String first, Set<String> familyIdSet) {
        try {
            conn.setAutoCommit(false);
            String extendedFamilyId = null;

            if (!familyIdSet.isEmpty()) {
                String familyIds = "";
                for (String id : familyIdSet) {
                    familyIds += ",'" + id + "'";
                }

                System.out.println("getExtendedFamilyId2 : " + familyIdSet);

                String query = "select * from family.familyidPair  "
                        + " where familyId in (" + familyIds.substring(1) + ") ";
                System.out.println(query);
                Statement st = null;

                st = conn.createStatement();
                st.setFetchSize(100);
                ResultSet rs = st.executeQuery(query);

                while (rs.next()) {
                    String familyId = rs.getString("familyId");
                    if (familyId == null)
                        continue;

                    extendedFamilyId = rs.getString("inpadocFamilyId");
                    if (extendedFamilyId == null && "".equals(extendedFamilyId))
                        continue;

                    if (first != null)
                        if (first.equals(familyId))
                            break;

                    System.out.println(familyId + " : " + extendedFamilyId);
                }
            }

            // exists extendedFamilyId
            if (extendedFamilyId == null || "".equals(extendedFamilyId)) {
                String maxExtendedFamilyId = getMaxExtendedId(conn);
                if (maxExtendedFamilyId == null)
                    return null;

                extendedFamilyId = maxExtendedFamilyId;
            }

            if (!"".equals(extendedFamilyId)) {
                updateDB(conn, extendedFamilyId, familyIdSet);
                insertDB(conn,extendedFamilyId,familyIdSet);
                conn.setAutoCommit(true);
                return extendedFamilyId;
            }

        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }

        return null;
    }

    private static void updateDB(Connection conn, String extendedFamilyId, Set<String> familyIdSet) {
        String tableName = "family.familyidPair";

        if (!familyIdSet.isEmpty()) {
            String familyIds = "";
            for (String id : familyIdSet) {
                familyIds += ",'" + id + "'";
            }

            String query = "UPDATE " + tableName + " SET inpadocFamilyId = ? "
                    + " where familyId in (" + familyIds.substring(1) + ") ";

            PreparedStatement preparedStatement = null;

            try {
                preparedStatement = conn.prepareStatement(query);
                preparedStatement.clearParameters();
                preparedStatement.setString(1, extendedFamilyId);
                preparedStatement.addBatch();

                preparedStatement.executeBatch();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    private static void insertDB(Connection conn, String newExtendedFamilyId, Set<String> familyIdSet) {
        String tableName = "family.familyidPair";

        String insertQuery = "INSERT into " + tableName +
                "(" +
                "familyId, inpadocFamilyId " +
                ") " +
                "values(?,?) " +
                " on duplicate key update " +
                "  inpadocFamilyId = values(inpadocFamilyId)";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            if (!familyIdSet.isEmpty())
                for (String familyId : familyIdSet) {
                    statement.setString(1, familyId);
                    statement.setString(2, newExtendedFamilyId);

                    statement.addBatch();
                }
            else {
                statement.setString(1, "");
                statement.setString(2, newExtendedFamilyId);

                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private static String getMaxExtendedId(Connection conn) {
        try {
            String query = "select max(inpadocFamilyId) maxExtendedFamilyId from family.familyidPair ";
//            System.out.println(query);
            Statement st = null;
            st = conn.createStatement();

            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);
            String maxExtendedFamilyId = null;
            while (rs.next()) {
                maxExtendedFamilyId = rs.getString("maxExtendedFamilyId");
            }

            if (maxExtendedFamilyId != null) {
                System.out.println(maxExtendedFamilyId);
                String newExtendedFamilyId = "KW" + String.format("%012d", Integer.parseInt(maxExtendedFamilyId.replaceAll("KW", "")) + 1);
                System.out.println(maxExtendedFamilyId + " : " + newExtendedFamilyId);

                return newExtendedFamilyId;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Set<String> selectFamilyDB(DBHelper dbHelper, Set<String> esDocIds) {
        if (esDocIds.isEmpty())
            return null;

        String ids = "'" + String.join("','", esDocIds) + "'";

        try {
            Connection conn = dbHelper.getConn_aurora2();

            String query = "select documentId from family.extendedFamily where documentId in (" + ids + ")";

//            System.out.println(query);
            Statement st = null;
            st = conn.createStatement();
            st.setFetchSize(100);

            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String documentId = rs.getString("documentId");
                if (documentId != null)
                    esDocIds.remove(documentId);
            }

            if (!esDocIds.isEmpty())
                return esDocIds;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }


    private static Set<String> getTargetDocIds(ESConfig esConfig, int jobSize) {
        // 원출원, 국제출원, 우선권번호
        String start = "20220101";
        BoolQueryBuilder bqb01 = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(start))
                .must(QueryBuilders.boolQuery()
                        .should(QueryBuilders.existsQuery("priorityClaims"))
                        .should(QueryBuilders.existsQuery("internationalApplicationNumber"))
                        .should(QueryBuilders.existsQuery("originalApplicationNumber")));

        // 국체출원, 우선권번호
        BoolQueryBuilder bqb02 = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(start))
                .must(QueryBuilders.boolQuery()
                        .should(QueryBuilders.existsQuery("priorityClaims"))
                        .should(QueryBuilders.existsQuery("internationalApplicationNumber")));

        // 국체출원, 우선권번호 + 국가제한
        String[] ccs = {"kr","jp","ep","us"};
        BoolQueryBuilder bqb03 = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termsQuery("priorityClaims.priorityApplicationCountry",ccs))
                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(start))
                .must(QueryBuilders.boolQuery()
                        .should(QueryBuilders.existsQuery("priorityClaims"))
                        .should(QueryBuilders.existsQuery("internationalApplicationNumber")));

        Map<String,BoolQueryBuilder> queryMap = new HashMap<>();
        queryMap.put("kipo", bqb01);
        queryMap.put("jpo", bqb01);
        queryMap.put("uspto", bqb02);
        queryMap.put("epo", bqb02);
        queryMap.put("sipo", bqb03);
        queryMap.put("pct", bqb03);
        queryMap.put("dpma", bqb03);
        queryMap.put("aupo", bqb03);
        queryMap.put("capo", bqb03);
        queryMap.put("frpo", bqb03);
        queryMap.put("gbpo", bqb03);
        queryMap.put("inpo", bqb03);
        queryMap.put("itpo", bqb03);
        queryMap.put("rupo", bqb03);
        queryMap.put("tipo", bqb03);

        // 국가 우선순위
        String[] indexs = {"kipo","jpo","uspto","epo","sipo","pct","dpma", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo"};
        Set<String> docIds = null;
        for (String index : indexs) {
            docIds = esGetDocIds(esConfig,index,queryMap.get(index),jobSize);
            if (docIds != null & docIds.size() > 0) {
                System.out.println(index + " : " + docIds.size());
                return docIds;
            }
        }

        return null;
    }

    private static Set<String> esGetDocIds(ESConfig esConfig, String index, BoolQueryBuilder bqb, int jobSize) {
        Set<String> docIds = new HashSet<>();
        String[] fields = {"documentId"};
        SearchResponse response = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .addSort("patentDate", SortOrder.ASC)
                .setSize(jobSize)
                .execute()
                .actionGet();

        for (SearchHit hit : response.getHits().getHits()) {
            docIds.add(hit.getId());
        }

        if (!docIds.isEmpty())
            return docIds;

        return null;
    }


    private static Set<String> getTargetDocIds1(ESConfig esConfig, int jobSize) {
        Set<String> docIds = new HashSet<>();
//        String[] indexies = {"kipo", "docdb", "jpo", "uspto", "sipo", "pct", "epo", "dpma", "aupo", "capo",
//        "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo", "uspto_past", "jpo_past"};
        String[] indexies = {"kipo","jpo","epo","uspto"};
//        String[] indexies = {"kipo","jpo"};
//                String[] indexies = {"sipo"};

        String[] fields = {"documentId"};

        String start = "20220101";
        String end = "20241231";

//        String start = "20220901";
//        String end = "20231231";

        String[] ccs = {"kr","jp","ep","us"};
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .mustNot(QueryBuilders.existsQuery("extendFamilyId"))
                .filter(QueryBuilders.termsQuery("priorityClaims.priorityApplicationCountry",ccs))
                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
                .filter(QueryBuilders.rangeQuery("patentDate").from(start).to(end))
                .must(QueryBuilders.boolQuery()
                        .should(QueryBuilders.existsQuery("priorityClaims"))
                        .should(QueryBuilders.existsQuery("internationalApplicationNumber"))
//                        .should(QueryBuilders.existsQuery("originalApplicationNumber"))
                )
                ;

//        System.out.println(bqb);
        String[] ids = {
                "cn116744711a"
        };

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(ids);

        SearchResponse response = esConfig.client.prepareSearch(indexies)
                .setTypes("patent")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .addSort("patentDate", SortOrder.ASC)
                .setSize(jobSize)
                .execute()
                .actionGet();

        for (SearchHit hit : response.getHits().getHits()) {
            docIds.add(hit.getId());
        }

        if (!docIds.isEmpty())
            return docIds;

        return null;
    }
}
