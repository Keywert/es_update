package es.tmp.family.wert;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import es.model.PriorityClaim;
import es.module.utility.CountryCode;
import es.module.utility.NumberUtil_Priority;
import es.module.utility.NumberUtils;
import es.module.utility.PriorityUtil;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetFamilyPatents {

    static String[] includes = {"priorityClaims", "internationalApplicationNumber", "internationalPublicationNumber", "epoPublicationNumber",
            "originalApplicationNumber", "familyId", "docdbFamilyId", "extendedFamilyId", "documentId", "patentNumber", "patentType",
            "applicationNumber", "epoApplicationNumber", "countryCode", "documentType", "applicationDate", "relatedDocuments", "familyFlag"
            , "publishingORG", "inventionTitle","publicationDate","patentDate", "originalApplicationDate","originalApplicationKind"
    };

    static String[] indexies = {"kipo", "docdb", "jpo", "uspto", "sipo", "pct", "epo", "dpma", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo", "uspto_past", "jpo_past"};
    static String[] indexies_without_docdb = {"kipo", "jpo", "uspto", "sipo", "pct", "epo", "dpma"};

    static String[] country_fields = {"countryCode", "publishingORG"};
    static String[] family_fields = {"familyId", "docdbFamilyId"};

    static String[] exp_familyId = {"12867556","9469366","12546897",};


    static String[] countries = {"KR", "JP", "US", "CN", "WO", "EP", "DE"};

    static List<String> docTypes;
    static String[] pType = {"A", "B"};
    static String[] uType = {"U", "Y"};
    static String[] eType = {"E"};
    static String[] sType = {"S"};
    static String[] tType = {"P"};

    static ObjectMapper mapper = new ObjectMapper();

    public static HashMap<String, PatentFamilyVO> getPatents2(ESConfig esConfig, HashMap<String, PatentFamilyVO> patentVOMap, String id) {
        BoolQueryBuilder bqb;
        try {
            if (patentVOMap == null){
                bqb = QueryBuilders.boolQuery()
//                        .must(QueryBuilders.boolQuery()
//                                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
//                                .must(QueryBuilders.matchQuery("_index","kipo"))
//                                .must(QueryBuilders.existsQuery("originalApplicationNumber"))
//                                .must(QueryBuilders.existsQuery("docdbFamilyId")))
//                        .must(bqb)
//                        .must(QueryBuilders.matchQuery("_index","uspto"))
//                        .must(QueryBuilders.existsQuery("docdbFamilyId"))
//                        .must(QueryBuilders.matchQuery("priorityClaims.priorityApplicationCountry","KR"))
//                        .must(QueryBuilders.prefixQuery("priorityClaims.priorityApplicationNumber","10-"))
//                        .must(QueryBuilders.rangeQuery("patentDate").from("20200101").to("20211231"))
//                        .must(QueryBuilders.idsQuery().addIds("cn109798491a"))
                        .filter(QueryBuilders.idsQuery().addIds(id))
//                        .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
                ;

                SearchResponse response = esConfig.client.prepareSearch(indexies)
                        .setTypes("patent")
                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                        .setQuery(bqb)
                        .setFetchSource(includes, null)
                        .setSize(1)
                        .execute()
                        .actionGet();

                for (SearchHit hit : response.getHits().getHits()) {
                    PatentFamilyVO patentVO = mapper.readValue(hit.getSourceAsString(), PatentFamilyVO.class);
                    if (patentVO == null)
                        break;
                    System.out.println("Start DocId : " + patentVO.getDocumentId());

                    patentVO.setIndex(hit.getIndex());

                    if (hit.getIndex().equals("docdb")) { // 결과가 docdb이고 패밀리ID가 없으면 종료
                        String familyId = patentVO.getFamilyId();
                        if (familyId == null)
                            continue;
                    }

                    patentVOMap = new HashMap<>();
                    patentVOMap.put(hit.getId(), patentVO);
                    if (patentVOMap.size() == 1)
                        break;
                }

            }else{
                bqb = QueryBuilders.boolQuery();
                bqb.mustNot(QueryBuilders.idsQuery().addIds(patentVOMap.keySet()));  // 현재 포함된 특허는 제외
                bqb.mustNot(QueryBuilders.matchQuery("countryCode","ZA"));  // 현재 포함된 특허는 제외

                if (patentVOMap.size() > 100)
                    return patentVOMap;

                Iterator<String> iterator = patentVOMap.keySet().iterator();

                Set<String> familyIdSet = new HashSet<>(); // docdb 패밀리 ID
                Set<String> extendedIdSet = new HashSet<>(); // extended 패밀리 ID
                List<BoolQueryBuilder> bqbList = new ArrayList<>();

                while (iterator.hasNext()) {
                    String key = iterator.next();
                    PatentFamilyVO patentVO = patentVOMap.get(key);

                    Boolean flag = patentVO.getFamilyFlag();  // 방문한 특허의 경우 제외
                    if (flag == true)
                        continue;

                    // familyId, docdbFamily
                    String familyId = patentVO.getFamilyId();
                    if (familyId != null)
                        familyIdSet.add(familyId);

                    System.out.println(familyId);

                    // extended FamilyId
                    String extendedFamilyId = patentVO.getExtendedFamilyId();
                    if (extendedFamilyId != null)
                        extendedIdSet.add(extendedFamilyId);

                    System.out.println("patentVO.makeBQB " + patentVO.getDocumentId());
                    BoolQueryBuilder bqb_patent = patentVO.makeBQB();
                    if (bqb_patent == null)
                        continue;

//                    System.out.println(bqb_patent);

                    bqbList.add(bqb_patent);

                    patentVO.setFamilyFlag(true);
                    patentVOMap.put(key,patentVO);
                }

                if (bqbList.isEmpty())
                    return patentVOMap;

                for (BoolQueryBuilder bq1 : bqbList){
                    bqb.should(bq1);
                }

                if (!familyIdSet.isEmpty()) {
                    bqb.should(QueryBuilders.termsQuery("familyId", familyIdSet));
                    bqb.should(QueryBuilders.termsQuery("docdbFamilyId", familyIdSet));
                }

                if (!extendedIdSet.isEmpty())
                    bqb.should(QueryBuilders.termsQuery("extendedFamilyId", extendedIdSet));

                SearchResponse response = esConfig.client.prepareSearch(indexies)
                        .setTypes("patent")
                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                        .setQuery(bqb)
                        .setFetchSource(includes, null)
                        .setSize(10000)
                        .execute()
                        .actionGet();

//                System.out.println(bqb);

                long hitCount = response.getHits().getTotalHits();

                if (hitCount == 0)
                    return patentVOMap;

                for (SearchHit hit : response.getHits().getHits()) {
                    PatentFamilyVO patentVO = null;
                    System.out.println(hit.getId());
                    patentVO = mapper.readValue(hit.getSourceAsString(), PatentFamilyVO.class);
                    if (patentVO == null)
                        break;

                    patentVO.setIndex(hit.getIndex());

                    if (hit.getIndex().equals("docdb")) {
                        String familyId = patentVO.getFamilyId();
                        if (familyId == null)
                            continue;
//                            return patentVOMap;
                    }
                    patentVOMap.put(hit.getId(), patentVO);
                }
            }

            if (patentVOMap == null)
                return null;

            if (!patentVOMap.isEmpty())
                getPatents2(esConfig, patentVOMap, id);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return patentVOMap;
    }

    public static HashMap<String, PatentFamilyVO> getPatents(ESConfig esConfig, HashMap<String, PatentFamilyVO> patentVOMap) {
        BoolQueryBuilder bqb;
        try {
            if (patentVOMap == null) {  // 처음 특허 찾는 조건 familyId 가 존재하지 않는 특허
                bqb = QueryBuilders.boolQuery()
//                        .must(QueryBuilders.boolQuery()
//                                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
//                                .must(QueryBuilders.matchQuery("_index","kipo"))
//                                .must(QueryBuilders.existsQuery("originalApplicationNumber"))
//                                .must(QueryBuilders.existsQuery("docdbFamilyId")))
//                        .must(bqb)
                        .must(QueryBuilders.idsQuery().addIds("us20190335837a1"))
//                        .must(QueryBuilders.idsQuery().addIds("kr20060078700a"))
//                        .must(QueryBuilders.idsQuery().addIds("kr19967000536a"))
//                        .must(QueryBuilders.idsQuery().addIds("WO201200020871A1"))us20170304385a1
//                        .must(QueryBuilders.idsQuery().addIds("us010641456b2"))
//                        .must(QueryBuilders.idsQuery().addIds("kr20190087902a"))
//                        .must(QueryBuilders.idsQuery().addIds("kr20190143650a")) // first input
//                        .must(QueryBuilders.idsQuery().addIds("kr20200035128a")) //
//                        .must(QueryBuilders.idsQuery().addIds("cn103151735a")) //
//                        .must(QueryBuilders.idsQuery().addIds("kr19967005239a")) //
//                        .must(QueryBuilders.idsQuery().addIds("us20200211566a1")) //
//                        .must(QueryBuilders.idsQuery().addIds("kr20020005325b1"))
//                        .must(QueryBuilders.idsQuery().addIds("us010681353b2"))
//                        .must(QueryBuilders.idsQuery().addIds("us20200033584a1"))
                //us20190319212a1 wo2020045945a1
//                cn103151735a us20200211566a1 ,kr20080002672b1, us20190097094a1 ,us20200050789a1, us20190097094a1

//                        .must(QueryBuilders.idsQuery().addIds("us20200139737a1"))
//                        .should(kr20207006050a
//                                QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("_index","docdb"))
//                                                        .must(QueryBuilders.termsQuery("countryCode",countries))
//                                                        .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
//                                                        .mustNot(QueryBuilders.termsQuery("familyId", ""))
//                        ).should(
//                                QueryBuilders.boolQuery().must(QueryBuilders.termsQuery("_index",indexies_without_docdb))
//                                        .mustNot(QueryBuilders.existsQuery("docdbFamilyId"))
//                        )
                ;

                SearchResponse response = esConfig.client.prepareSearch(indexies)
                        .setTypes("patent")
                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                        .setQuery(bqb)
                        .setFetchSource(includes, null)
                        .setSize(1)
                        .execute()
                        .actionGet();


                for (SearchHit hit : response.getHits().getHits()) {
                    PatentFamilyVO patentVO = mapper.readValue(hit.getSourceAsString(), PatentFamilyVO.class);
                    if (patentVO == null)
                        break;
                    System.out.println("Start DocId : " + patentVO.getDocumentId());

                    patentVO.setIndex(hit.getIndex());

                    if (hit.getIndex().equals("docdb")) { // 결과가 docdb이고 패밀리ID가 없으면 종료
                        String familyId = patentVO.getFamilyId();
                        if (familyId == null)
                            return null;
                    }

                    String patentType = patentVO.getPatentType();
                    if (patentType == null || "".equals(patentType))
                        patentType = "A";

                    System.out.println(patentType);

                    docTypes = new ArrayList<>();

                    if (patentType.matches("^(A|B)$"))
                        docTypes.addAll(Arrays.asList(pType));
                    else if (patentType.matches("^(U|Y)$"))
                        docTypes.addAll(Arrays.asList(uType));
                    else if (patentType.matches("^(S)$"))
                        docTypes.addAll(Arrays.asList(sType));
                    else if (patentType.matches("^(E)$"))
                        docTypes.addAll(Arrays.asList(eType));
                    else if (patentType.matches("^(P)$"))
                        docTypes.addAll(Arrays.asList(tType));

                    patentVOMap = new HashMap<>();
                    patentVOMap.put(hit.getId(), patentVO);
                }
            } else { // 특허 맵이 비어있지 않을경우
                bqb = QueryBuilders.boolQuery();
                bqb.mustNot(QueryBuilders.idsQuery().addIds(patentVOMap.keySet()));  // 현재 포함된 특허는 제외

                Iterator<String> iterator = patentVOMap.keySet().iterator();

                Map<String, Set<String>> numberMap = new HashMap<>(); //국가별 출원번호
                Map<String, Set<String>> pnumberMap = new HashMap<>(); //국가별 문헌번호

                Set<String> familyIdSet = new HashSet<>(); // docdb 패밀리 ID
                Set<String> extendedIdSet = new HashSet<>(); // extended 패밀리 ID

//                BoolQueryBuilder bqb2 = MakeFamilyQuery.makeFamilyQuery(patentVOMap);
//                if (bqb2 == null)
//                    return null;

                while (iterator.hasNext()) {
                    String key = iterator.next();
                    PatentFamilyVO patentVO = patentVOMap.get(key);

                    Boolean flag = patentVO.getFamilyFlag();  // 방문한 특허의 경우 제외
                    if (flag == true)
                        continue;

                    String docId = patentVO.getDocumentId();

                    // familyId, docdbFamily
                    String familyId = patentVO.getFamilyId();
                    if (familyId != null)
                        familyIdSet.add(familyId);

                    // extended FamilyId
                    String extendedFamilyId = patentVO.getExtendedFamilyId();
                    if (extendedFamilyId != null)
                        extendedIdSet.add(extendedFamilyId);

                    // 국가 코드
                    String countryCode = patentVO.getCountryCode();
                    if (countryCode == null)
                        countryCode = docId.substring(0, 2).toUpperCase();

                    // 출원번호
                    Set<String> appNums = new HashSet<>();

                    // 원출원번호
                    Set<String> oappNums = new HashSet<>();

                    // 문헌번호
                    Set<String> pubNums = new HashSet<>();

                    // docdb를 제외한 국가는 원본 출원을 입력, docdb는 가공해서 입력
                    String applicationNumber = patentVO.getApplicationNumber();
                    if ("docdb".equals(patentVO.getIndex())) {
                        String apnum = NumberUtils.getNumber(countryCode, applicationNumber, "AN");
                        if (apnum != null)
                            appNums.add(apnum);
                    } else
                        appNums.add(applicationNumber);

                    // docdb를 제외한 국가는 원본 출원을 입력, docdb는 가공해서 입력
                    String patentNumber = patentVO.getPatentNumber();
                    if ("docdb".equals(patentVO.getIndex())) {
                        String pnum = NumberUtils.getNumber(countryCode, patentNumber, "PN");
                        if (pnum != null)
                            pubNums.add(pnum);
                    } else {
                        if (patentNumber != null)
                            pubNums.add(patentNumber);
                    }

                    String epoPublicationNumber = patentVO.getEpoPublicationNumber();
                    if (epoPublicationNumber != null) {
                        pubNums.add(epoPublicationNumber);
                        String pnum = NumberUtils.getNumber(countryCode, epoPublicationNumber, "PN");
                        if (pnum != null)
                            pubNums.add(pnum);
                    }

                    String epoApplicationNumber = patentVO.getEpoApplicationNumber();
                    if (epoApplicationNumber != null) {
                        appNums.add(epoApplicationNumber);
                        String apnum = NumberUtils.getNumber(countryCode, epoApplicationNumber, "AN");
                        if (apnum != null)
                            appNums.add(apnum);
                    }

                    String originalApplicationNumber = patentVO.getOriginalApplicationNumber();
                    if (originalApplicationNumber != null) {
                        String apnum = NumberUtils.getNumber(countryCode, originalApplicationNumber, "AN");
                        if (apnum != null && !applicationNumber.equals(apnum)) {
                            oappNums.add(apnum);
                            if ("KR".equals(countryCode) && apnum.matches("^(10|20)-(19|20)[0-9]{2}-[0-9]{7}$")) {
                                oappNums.add(apnum.substring(3));
                            }
                        }
                    }


                    if (!appNums.isEmpty()) {
                        numberMap.put(countryCode, appNums);
                    }
                    if (!pubNums.isEmpty()) {
                        pnumberMap.put(countryCode, pubNums);
                    }

                    List<PriorityClaim> priorityClaims = patentVO.getPriorityClaims();
                    if (priorityClaims != null) {
                        for (PriorityClaim priorityClaim : priorityClaims) {
                            String num = priorityClaim.getPriorityApplicationNumber();
                            String docdbNum = priorityClaim.getDocdbApplicationNumber();
                            String country = priorityClaim.getPriorityApplicationCountry();
                            if (country == null)
                                continue;
                            String date = priorityClaim.getPriorityApplicationDate();

                            if (date != null)
                                date = date.replaceAll("[^0-9]", "");

                            country = PriorityUtil.countryNormalize(country);
                            String CC = CountryCode.countryCode.get(country);

                            if (CC != null)
                                country = CC;

                            Set<String> numberSet = numberMap.get(country);
                            if (numberSet == null)
                                numberSet = new HashSet<>();

                            country = country.replaceAll("[^A-z]", "");

                            if (num != null) {
                                num = num.replaceAll("[\\s|,]", "");
                                numberSet.add(num);
                            }

                            if (docdbNum != null) {
                                docdbNum = docdbNum.replaceAll("[\\s|,]", "");
                                numberSet.add(docdbNum);
                            }

                            String number = NumberUtil_Priority.getNumber(country, num, "AN").replaceAll("\\.", "");
                            if (number != null)
                                numberSet.add(number);

                            number = NumberUtils.getNumber(country, num, "AN").replaceAll("\\.", "");
                            numberSet.add(number);

                            String docdbNumber = NumberUtil_Priority.getNumber(country, num, "AN").replaceAll("\\.", "");
                            if (docdbNumber != null)
                                numberSet.add(docdbNumber);

                            if (!numberSet.isEmpty())
                                numberMap.put(country, numberSet);
                        }
                    }

//                    System.out.println(applicationNumber);
                    //// 국제  internationalApplicationNumber", "internationalPublicationNumber",
                    String internationalApplicationNumber = patentVO.getInternationalApplicationNumber();
                    Set<String> ianSet = new HashSet<>();
                    if (internationalApplicationNumber != null) {
                        Set<String> numberSet = numberMap.get("WO");
                        if (numberSet == null)
                            numberSet = new HashSet<>();

                        numberSet.add(internationalApplicationNumber);
                        String apnum = NumberUtils.getNumber("PCT", internationalApplicationNumber, "AN");
                        if (apnum != null)
                            numberSet.add(apnum);

                        numberMap.put("WO", numberSet);
                    }

                    String internationalPublicationNumber = patentVO.getInternationalPublicationNumber();
                    if (internationalPublicationNumber != null) {
                        Set<String> numberSet = pnumberMap.get("WO");
                        if (numberSet == null)
                            numberSet = new HashSet<>();

                        numberSet.add(internationalPublicationNumber);
                        String pnum = NumberUtils.getNumber("PCT", internationalApplicationNumber, "AN");
                        if (pnum != null)
                            numberSet.add(pnum);

                        pnumberMap.put("WO", numberSet);
                    }

                }
                Matcher matcher;


                if (!numberMap.isEmpty()) {
                    Iterator<String> ir = numberMap.keySet().iterator();

                    System.out.println(new ObjectMapper().writeValueAsString(numberMap));

                    while (ir.hasNext()) {
                        String cc = ir.next();
                        Set<String> pnumSet = pnumberMap.get(cc);
                        System.out.println(new ObjectMapper().writeValueAsString(pnumSet));
                        if (pnumSet == null) {
                            pnumSet = new HashSet<>();
                            pnumSet.add("noNumber");
                        }

                        if (cc.matches("WO")) {
                            Set<String> applicationCountrySet = new HashSet<>();
                            Set<String> numSet = numberMap.get(cc);

                            for (String code : numSet) {
                                matcher = Pattern.compile("[A-z]{2}").matcher(code);
                                while (matcher.find()) {
                                    String tmp = matcher.group();
                                    if (tmp.matches("WO|PC"))
                                        continue;

                                    applicationCountrySet.add(tmp);
                                }
                            }

                            BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                                    .filter(QueryBuilders.termsQuery("applicationCountry", applicationCountrySet))
                                    .filter(QueryBuilders.multiMatchQuery(cc, country_fields))
                                    .filter(QueryBuilders.boolQuery()
                                            .should(QueryBuilders.termsQuery("patentNumber", pnumSet))
                                            .should(QueryBuilders.termsQuery("applicationNumber", numSet))
//                                        .should(QueryBuilders.termsQuery("internationalApplicationNumber", numSet))
                                            .should(QueryBuilders.termsQuery("epoApplicationNumber", numSet)));

                            bqb = bqb.should(bq01);

                            BoolQueryBuilder bq02 = QueryBuilders.boolQuery()
                                    .filter(QueryBuilders.matchQuery("_index", "pct"))
                                    .filter(QueryBuilders.boolQuery()
                                            .should(QueryBuilders.termsQuery("patentNumber", pnumSet))
                                            .should(QueryBuilders.termsQuery("applicationNumber", numSet))
//                                        .should(QueryBuilders.termsQuery("internationalApplicationNumber", numSet))
                                            .should(QueryBuilders.termsQuery("epoApplicationNumber", numSet)));
                            bqb = bqb.should(bq02);

                        } else {
                            Set<String> numSet = numberMap.get(cc);

                            BoolQueryBuilder bq01 = QueryBuilders.boolQuery()
                                    .filter(QueryBuilders.multiMatchQuery(cc, country_fields))
                                    .filter(QueryBuilders.termsQuery("patentType", docTypes))
                                    .filter(QueryBuilders.boolQuery()
                                            .should(QueryBuilders.termsQuery("epoPublicationNumber", pnumSet))
                                            .should(QueryBuilders.termsQuery("patentNumber", pnumSet))
                                            .should(QueryBuilders.termsQuery("applicationNumber", numSet))
//                                        .should(QueryBuilders.termsQuery("internationalApplicationNumber", numSet))
                                            .should(QueryBuilders.termsQuery("epoApplicationNumber", numSet)));

                            bqb = bqb.should(bq01);
                        }
                    }
                } else
                    return patentVOMap;

                if (!familyIdSet.isEmpty()) {
                    bqb = bqb.should(QueryBuilders.termsQuery("familyId", familyIdSet));
                    bqb = bqb.should(QueryBuilders.termsQuery("docdbFamilyId", familyIdSet));
                }

                if (!extendedIdSet.isEmpty())
                    bqb = bqb.should(QueryBuilders.termsQuery("extendedFamilyId", extendedIdSet));


                String[] exids = {"16537182",""};
                bqb = bqb.must(QueryBuilders.termsQuery("docdbFamilyId",exids));
                bqb = bqb.must(QueryBuilders.termsQuery("fmailyId",exids));

                SearchResponse response = esConfig.client.prepareSearch(indexies)
                        .setTypes("patent")
                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                        .setQuery(bqb)
                        .setFetchSource(includes, null)
                        .setSize(10000)
                        .execute()
                        .actionGet();

                System.out.println(bqb);
                long hitCount = response.getHits().getTotalHits();

                if (hitCount == 0)
                    return patentVOMap;

                for (SearchHit hit : response.getHits().getHits()) {
                    PatentFamilyVO patentVO = null;
//                    System.out.println(hit.getId());
                    patentVO = mapper.readValue(hit.getSourceAsString(), PatentFamilyVO.class);
                    if (patentVO == null)
                        break;

                    patentVO.setIndex(hit.getIndex());

                    if (hit.getIndex().equals("docdb")) {
                        String familyId = patentVO.getFamilyId();
                        if (familyId == null)
                            continue;
//                            return patentVOMap;
                    }
                    patentVOMap.put(hit.getId(), patentVO);
                }
            }

            if (patentVOMap == null)
                return null;

            if (!patentVOMap.isEmpty())
                getPatents(esConfig, patentVOMap);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return patentVOMap;
    }
}
