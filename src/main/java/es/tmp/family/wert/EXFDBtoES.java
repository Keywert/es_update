package es.tmp.family.wert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class EXFDBtoES {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();
        DataFarmDB dataFarmDB = new DataFarmDB();

        Map<String, ESConfig> esConfigMap = new HashMap<>();
        esConfigMap.put("es18", new ESConfig("es18"));
        esConfigMap.put("es188", new ESConfig("es188"));
        esConfigMap.put("es189", new ESConfig("es189"));

        // DOCDB의 documentId가 중복되서 docdb를 마지막에 실행하기 위해 순서대로 실행
        String[] querys = {
                "select * from family.extendedFamily where status is null and esindex in ('kipo','epo','uspto','dpma','jpo_past','uspto_past') ",
                "select * from family.extendedFamily where status is null and esindex in ('aupo','capo','frpo','gbpo','inpo','itpo','rupo','tipo','sipo')",
                "select * from family.extendedFamily where status is null and esindex in ('jpo','pct') ",
                "select * from family.extendedFamily where status is null and esindex in ('docdb')"
        };

        String[] ess = {"es188", "es189", "es18", "es188"};

        try {
            for (int i = 0; i < querys.length; i++) {
                String query = querys[i];
                ESConfig esConfig = esConfigMap.get(ess[i]);

                Statement st = conn.createStatement();
                st.setFetchSize(100);
                ResultSet rs = st.executeQuery(query);

                ObjectMapper objectMapper = new ObjectMapper();

                Set<String> updateDocIds = new HashSet<>();
                List<QueueVO> queueVOList = new ArrayList<>();

                while (rs.next()) {
                    Map<String, String> map = new HashMap<>();

                    String documentId = rs.getString("documentId");
                    if (documentId == null)
                        continue;

                    updateDocIds.add(documentId);
                    String esindex = rs.getString("esindex");
                    if (esindex == null)
                        continue;

                    String docdbFamilyId = rs.getString("docdbFamilyId");
                    if (docdbFamilyId != null && !esindex.matches("docdb"))
                        map.put("docdbFamilyId", docdbFamilyId);

                    String extendedFamilyId = rs.getString("extendedFamilyId");
                    if (extendedFamilyId == null)
                        continue;

                    map.put("extendedFamilyId", extendedFamilyId);

                    // Docdb
                    Set<String> docIds = new HashSet<>();
                    docIds.add(documentId);

                    String json = objectMapper.writeValueAsString(map);
                    System.out.println(docIds + " : " + json);

                    for (String docId : docIds) {
                        esConfig.bulkProcessor.add(new UpdateRequest(esindex, "patent", docId)
                                .doc(json.getBytes())
                                .upsert(json.getBytes()));

                        QueueVO queueVO = new QueueVO();
                        queueVO.setEsindex(esindex);
                        queueVO.setDocumentId(docId);
                        queueVO.setLevel(3);

                        queueVOList.add(queueVO);
                        if (queueVOList.size() > 100) {
                            dataFarmDB.insertESQueue(queueVOList);
                            queueVOList.clear();
                        }
                    }


                    if (updateDocIds.size() > 10) {
                        updateDB(conn, updateDocIds);
                        updateDocIds.clear();
                    }
                }

                if (!updateDocIds.isEmpty())
                    updateDB(conn, updateDocIds);

                if (!queueVOList.isEmpty())
                    dataFarmDB.insertESQueue(queueVOList);
            }

            for (ESConfig esConfig : esConfigMap.values())
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private static void updateDB(Connection conn, Set<String> docIds) {
        String query = "UPDATE family.extendedFamily SET status= 1 " +
                "WHERE documentId = ? ";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : docIds) {
                preparedStatement.setString(1, docId);

                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Set<String> getDocIds(ESConfig esConfig, String index, String documentId, String docdbFamilyId, String extendedFamilyId) {
        String[] fields = {"documentId"};
        Set<String> docIds = new HashSet<>();

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(documentId))
                .mustNot(QueryBuilders.matchQuery("extendedFamilyId", extendedFamilyId));

//        if (docdbFamilyId != null)
//            bqb.should(QueryBuilders.matchQuery("docdbFamilyId",docdbFamilyId));

//        System.out.println(bqb);

        SearchResponse response = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        for (SearchHit hit : response.getHits().getHits()) {
            docIds.add(hit.getId());
        }

        if (!docIds.isEmpty())
            return docIds;

        return null;
    }
}
   