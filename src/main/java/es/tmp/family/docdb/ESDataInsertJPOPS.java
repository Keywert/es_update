package es.tmp.family.docdb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ESDataInsertJPOPS {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es18"));

        if (esConfigs.isEmpty())
            return;

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String tableName = "family.jpo_family_past";
        String index = "jpo_past";
        String type = "patent";

//        String query = "select * from " +  tableName + " A inner join JPO.RIGHT_HOLDER_LIST B "
//                + " on A.applicationNumber_norm = B.APPLICATION_NUMBER "
//                + " where A.documentType regexp '^[A|B][0-9]?$' and B.TYPE = 'P' "
//                + " and A.status is null "
//                + " limit 10 ";

        String query = "select * from " + tableName + " where status is null and docdbFamilyId is not null" +
//                " and documentId in ('jp2019097351a') " +
                " limit 3000000";

        System.out.println(query);


//        String query = "select * from " +  tableName + " A left join JPO.RIGHT_HOLDER_LIST B "
//                + " on A.applicationNumber_norm = B.APPLICATION_NUMBER "
//                + " where B.APPLICATION_NUMBER is null "
////                + " where B.APPLICATION_NUMBER is null and A.documentType regexp '^[A|B][0-9]?$' and B.TYPE = 'P' "
//                + " and A.status is null "
//                + " limit 5000000 "
//                ;

        Statement st = null;

        int count = 0;

        ArrayList<String> docIds = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (++count % 1000 == 0) {
                    System.out.println(count);

                }
                String documentId = rs.getString("documentId");

                docIds.add(documentId);
                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();

                patentFamilyVO.setPatentId(rs.getString("patentId"));

                patentFamilyVO.setApplicationDate(rs.getString("applicationDate"));

                patentFamilyVO.setDocdbDocumentId(rs.getString("docdbDocId"));

                String patentNumber = rs.getString("registerNumber");
//                if (patentNumber == null)
//                    patentNumber = rs.getString("announcementNumber");

                if (patentNumber == null)
                    patentNumber = rs.getString("publicationNumber");

                if (patentNumber == null)
                    patentNumber = rs.getString("openNumber");

                if (patentNumber != null && !"".equals(patentNumber))
                    patentFamilyVO.setPatentNumber(patentNumber);


                String patentDate = rs.getString("registerDate");
//                if (patentDate == null)
//                    patentDate = rs.getString("announcementDate");

                if (patentDate == null)
                    patentDate = rs.getString("publicationDate");

                if (patentDate == null)
                    patentDate = rs.getString("openDate");

                if (patentDate != null && !"".equals(patentDate))
                    patentFamilyVO.setPatentDate(patentDate);

//                patentFamilyVO.setAnnouncementNumber(rs.getString("announcementNumber"));
//                patentFamilyVO.setAnnouncementDate(rs.getString("announcementDate"));

                patentFamilyVO.setRegisterNumber(rs.getString("registerNumber"));
                patentFamilyVO.setRegisterDate(rs.getString("registerDate"));

                patentFamilyVO.setPublicationNumber(rs.getString("publicationNumber"));
                patentFamilyVO.setPublicationDate(rs.getString("publicationDate"));

                patentFamilyVO.setDocdbFamilyId(rs.getString("docdbFamilyId"));
                patentFamilyVO.setExtendedFamilyId(rs.getString("inpadocFamilyId"));

                patentFamilyVO.setInternationalApplicationNumber(rs.getString("internationalApplicationNumber"));
                patentFamilyVO.setInternationalApplicationDate(rs.getString("internationalApplicationDate"));
                patentFamilyVO.setInternationalPublicationNumber(rs.getString("internationalPublicationNumber"));
                patentFamilyVO.setInternationalPublicationDate(rs.getString("internationalPublicationDate"));

//                patentFamilyVO.setLegalStatus(rs.getString("legalStatus"));

                String json = objectMapper.writeValueAsString(patentFamilyVO);

//                System.out.println(documentId +" : " +json);

                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));


                if (docIds.size() > 100) {
                    updateDB(conn, tableName, docIds);
                    docIds.clear();
//                    Thread.sleep(10000);
                }


            }

            if (!docIds.isEmpty())
                updateDB(conn, tableName, docIds);

            System.out.println(count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }

    }

    private static void updateDB(Connection conn, String tableName , ArrayList<String> docIds) {
        String query = "UPDATE " + tableName + " SET status = 1" +
                " WHERE documentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String documentId : docIds) {
                preparedStatement.setString(1,documentId);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
