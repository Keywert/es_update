package es.tmp.family.docdb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ESDataInsertSIPO {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es189"));

        if (esConfigs.isEmpty())
            return;

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        DataFarmDB dataFarmDB = new DataFarmDB();
        List<QueueVO> queueVOList = new ArrayList<>();

        String tableName = "family.sipo_family";
        String index = "sipo";
        String type = "patent";

        String query = "select * from " + tableName +
                " where status is null and docdbFamilyId is not null " +
//        " and (publicationDate > '20220501' or registerDate > '20220501') " +
//                + " and documentId like 'cn111486402a' "
                 " limit 200000"
                ;

        Statement st = null;
        System.out.println(query);

        int count = 0;

        ArrayList<String> docIds = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (++count % 1000 == 0) {
                    System.out.println(count);

                }
                String documentId = rs.getString("documentId");

                docIds.add(documentId);
                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();

                patentFamilyVO.setPatentId(rs.getString("patentId"));


                patentFamilyVO.setRegisterNumber(rs.getString("registerNumber"));
                patentFamilyVO.setRegisterDate(rs.getString("registerDate"));

                patentFamilyVO.setPublicationNumber(rs.getString("publicationNumber"));
                patentFamilyVO.setPublicationDate(rs.getString("publicationDate"));


                patentFamilyVO.setDocdbFamilyId(rs.getString("docdbFamilyId"));
//                patentFamilyVO.setInpadocFamilyId(rs.getString("inpadocFamilyId"));
                patentFamilyVO.setExtendedFamilyId(rs.getString("inpadocFamilyId"));
                patentFamilyVO.setInternationalApplicationNumber(rs.getString("internationalApplicationNumber"));
                patentFamilyVO.setInternationalApplicationDate(rs.getString("internationalApplicationDate"));
                patentFamilyVO.setInternationalPublicationNumber(rs.getString("internationalPublicationNumber"));
                patentFamilyVO.setInternationalPublicationDate(rs.getString("internationalPublicationDate"));

//                patentFamilyVO.setLegalStatus(rs.getString("legalStatus"));

                String json = objectMapper.writeValueAsString(patentFamilyVO);

//                System.out.println(documentId +" : " +json);
                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(documentId);
                queueVO.setLevel(4);
                queueVOList.add(queueVO);

                if (docIds.size() > 100) {
                    updateDB(conn, tableName, docIds);
                    docIds.clear();

                    dataFarmDB.insertESQueue(queueVOList);
                    queueVOList.clear();
//                    Thread.sleep(1000);
                }
            }

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            if (!docIds.isEmpty())
                updateDB(conn, tableName, docIds);

            System.out.println(count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }

    }

    private static void updateDB(Connection conn, String tableName , ArrayList<String> docIds) {
        String query = "UPDATE " + tableName + " SET status = 1" +
                " WHERE documentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String documentId : docIds) {
                preparedStatement.setString(1,documentId);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
