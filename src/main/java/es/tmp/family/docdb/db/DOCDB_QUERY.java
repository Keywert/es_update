package es.tmp.family.docdb.db;
import es.config.DBHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DOCDB_QUERY {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();
        PreparedStatement preparedStmt = null;

        List<String> queries = getDocdbQuery();
        if (queries == null)
            return;

        try {
            for (String query : queries) {
                query = query.replaceAll("\\s+"," ");
                System.out.println(query);

                preparedStmt = conn.prepareStatement(query);
                preparedStmt.clearParameters();
                preparedStmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getDocdbQuery() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String date = localDate.toString().replaceAll("-","");
        System.out.println(date);
        List<String> queries = new ArrayList<>();

        // 신규 DOCDB 추가
        queries.add("update family.docdb_family_new A inner join family.familyidPair B on A.familyId = B.familyId set A.inpadocFamilyId_change = B.inpadocFamilyId where A.inpadocFamilyId_change is null and B.inpadocFamilyId != ''");

        queries.add("insert into family.docdb_family_kr (documentId, applicationType, epoPublicationNumber, originalApplicationNumber, documentType, applicationNumber, documentNumber, applicationCountryCode, standardApplicationNumber, familyId, epoApplicationNumber, countryCode, publicationDate, publicationNumber, applicationDate, inpadocFamilyId, status, familyId_change, inpadocFamilyId_change) " +
                " select documentId, applicationType, epoPublicationNumber, originalApplicationNumber, documentType, applicationNumber, documentNumber, applicationCountryCode, standardApplicationNumber, familyId, epoApplicationNumber, countryCode, publicationDate, publicationNumber, applicationDate, inpadocFamilyId, status, familyId_change, inpadocFamilyId_change from family.docdb_family_new where documentId like 'KR%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_jp select * from family.docdb_family_new where documentId like 'JP%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_us select * from family.docdb_family_new where documentId like 'US%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_ep select * from family.docdb_family_new where documentId like 'EP%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_cn select * from family.docdb_family_new where documentId like 'CN%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_wo select * from family.docdb_family_new where documentId like 'WO%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_de select * from family.docdb_family_new where documentId like 'DE%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_au select * from family.docdb_family_new where documentId like 'AU%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_ca select * from family.docdb_family_new where documentId like 'CA%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_fr select * from family.docdb_family_new where documentId like 'FR%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_gb select * from family.docdb_family_new where documentId like 'GB%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_in select * from family.docdb_family_new where documentId like 'IN%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_it select * from family.docdb_family_new where documentId like 'IT%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_ru select * from family.docdb_family_new where documentId like 'RU%' on duplicate key update familyId_change = values(familyId)");
        queries.add("insert into family.docdb_family_ti select * from family.docdb_family_new where documentId like 'TW%' on duplicate key update familyId_change = values(familyId)");

        // KIPO
        queries.add("update family.kipo_family A inner join family.docdb_family_kr B on A.applicationNumber = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.kipo_family A inner join family.familyidPair B on A.docdbFamilyId = B.familyId set A.inpadocFamilyId = B.inpadocFamilyId where A.inpadocFamilyId is null and B.inpadocFamilyId != ''");
        queries.add("update family.kipo_family set internationalPublicationNumber = replace(internationalPublicationNumber, ' ', ''), internationalApplicationDate = applicationDate  where internationalPublicationNumber regexp '^WO (19|20)[0-9]{2}/[0-9]{6}$'");

        // USPTO
        queries.add("update family.uspto_family set applicationNumber_norm = replace(applicationNumber, '-', '')  where applicationNumber_norm is null");
        queries.add("update family.uspto_family A inner join family.docdb_family_us B on A.applicationNumber_norm = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.uspto_family A inner join family.docdb_family_us B on A.registerNumber = B.publicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.uspto_family A inner join family.familyidPair B on A.docdbFamilyId = B.familyId set A.inpadocFamilyId = B.inpadocFamilyId where A.inpadocFamilyId is null and B.inpadocFamilyId != ''");

        // EPO
        queries.add("update family.epo_family A inner join family.docdb_family_ep B on A.publicationNumber = B.publicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.epo_family A inner join family.docdb_family_ep B on A.registerNumber = B.publicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.epo_family A inner join family.familyidPair B on A.docdbFamilyId = B.familyId set A.inpadocFamilyId = B.inpadocFamilyId where A.inpadocFamilyId is null and B.inpadocFamilyId != ''");

        // sipo
        queries.add("update family.sipo_family A inner join family.docdb_family_cn B on A.publicationNumber = B.publicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and A.applicationDate = B.applicationDate");
        queries.add("update family.sipo_family A inner join family.docdb_family_cn B on A.registerNumber = B.publicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null and A.applicationDate = B.applicationDate");
        queries.add("update family.sipo_family A inner join family.docdb_family_cn B on A.publicationNumber = B.publicationNumber or A.registerNumber = B.publicationNumber set A.docdbFamilyId = B.familyId where A.docdbFamilyId is null");
        queries.add("update family.sipo_family A inner join family.familyidPair B on A.docdbFamilyId = B.familyId set A.inpadocFamilyId = B.inpadocFamilyId where A.inpadocFamilyId is null and B.inpadocFamilyId != ''");

        // JPO
        queries.add("update family.jpo_family set applicationNumber_norm = replace(applicationNumber, '-', '')  where applicationNumber_norm is null");
        queries.add("update family.jpo_family A inner join family.docdb_family_jp B on A.applicationNumber_norm = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.documentType regexp '^[A|B][0-9]?$' and B.documentType regexp '^[A|B][0-9]?$' and A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.jpo_family A inner join family.docdb_family_jp B on A.applicationNumber_norm = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.documentType regexp '^[U|Y][0-9]?$' and B.documentType regexp '^[U|Y][0-9]?$' and A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.jpo_family A inner join family.docdb_family_jp B on A.applicationNumber = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.documentType regexp '^[A|B][0-9]?$' and B.documentType regexp '^[A|B][0-9]?$' and A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.jpo_family A inner join family.docdb_family_jp B on A.applicationNumber = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.documentType regexp '^[U|Y][0-9]?$' and B.documentType regexp '^[U|Y][0-9]?$' and A.docdbFamilyId is null");
        queries.add("update family.jpo_family A inner join family.docdb_family_jp B on A.applicationNumber = B.originalApplicationNumber set A.docdbFamilyId = B.familyId where A.documentType regexp '^[A|B][0-9]?$' and B.documentType regexp '^[A|B][0-9]?$' and A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.jpo_family A inner join family.docdb_family_jp B on A.applicationNumber = B.originalApplicationNumber set A.docdbFamilyId = B.familyId  where A.documentType regexp '^[U|Y][0-9]?$' and B.documentType regexp '^[U|Y][0-9]?$' and A.docdbFamilyId is null and B.familyId != ''");
        queries.add("update family.jpo_family A inner join family.familyidPair B on A.docdbFamilyId = B.familyId set A.inpadocFamilyId = B.inpadocFamilyId where A.inpadocFamilyId is null and B.inpadocFamilyId != ''");



        if (!queries.isEmpty())
            return queries;

        return null;
    }
}
