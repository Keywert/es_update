package es.tmp.family.docdb.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.Patent;
import es.module.db.DataFarmDB;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PatentToDB {
    public static void main(String[] args) {
        Map<String,ESConfig> datafarmESMap = new HashMap<>();
        datafarmESMap.put("es18",new ESConfig("es18"));
        datafarmESMap.put("es188",new ESConfig("es188"));
        datafarmESMap.put("es189",new ESConfig("es189"));
        DataFarmDB dataFarmDB = new DataFarmDB();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String[] fields = {"documentId", "documentType", "publicationNumber", "publicationDate"
                , "registerNumber", "registerDate", "openNumber", "openDate", "applicationNumber", "applicationDate"
                ,  "familyId", "extendedFamilyId"
                , "originalApplicationNumber"
                , "docdbFamilyId"
                , "internationalApplicationNumber", "internationalApplicationDate", "internationalPublicationNumber", "internationalPublicationDate"
        };

//        String[] indexArr = {"epo", "uspto", "kipo","jpo","sipo"};
        String[] indexArr = {"sipo"};

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now().minus(Period.ofDays(100));
        String updateDate = dtf.format(localDate);

        for (String index : indexArr) {
            String clusterName = dataFarmDB.selectClusterInfo(index, 1);
            if (clusterName == null)
                continue;

            System.out.println(index + " : " + clusterName);
            ESConfig esConfig = datafarmESMap.get(clusterName);

            String type = "patent";
            String tableName = "family." + index + "_family";
            System.out.println(index + " - " + tableName);

            QueryBuilder qb = QueryBuilders.boolQuery()
                    .mustNot(QueryBuilders.existsQuery("docdbFamilyId"))
                    .filter(QueryBuilders.rangeQuery("patentDate").from(updateDate))
                    ;

            SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                    .setTypes(type)
                    .setSearchType(SearchType.SCAN)
                    .setScroll(new TimeValue(200000))
                    .setQuery(qb)
                    .setFetchSource(fields, null)
                    .setSize(10)
                    .execute()
                    .actionGet();

            System.out.println("Total Count : " + scrollResp.getHits().getTotalHits());

            int count = 0;

            ObjectMapper objectMapper = new ObjectMapper();

            ArrayList<Patent> patents = new ArrayList<>();

            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    try {
                        if (++count % 1000 == 0)
                            System.out.println(count);

                        Patent patent = objectMapper.readValue(hit.getSourceAsString(), Patent.class);
                        patents.add(patent);

                        if (patents.size() > 200) {
                            InsertDB.insertFamily2(conn, tableName, patents);
                            patents.clear();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();

            } while (scrollResp.getHits().getHits().length != 0);

            if (!patents.isEmpty())
                InsertDB.insertFamily2(conn, tableName, patents);

            System.out.println("Finish Count : " + count);
        }
    }

}
