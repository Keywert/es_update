package es.tmp.family.docdb.db;

import es.model.bulk.CitationDoc;
import es.model.bulk.Patent;
import es.model.v1.PriorityClaim;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InsertDB {
    public static void insertData(Connection conn, ArrayList<Patent> patent) {

    }

    public static void insertCitationData(Connection conn, String index, ArrayList<Patent> patents) {
        if (patents.isEmpty())
            return;

        String biTable = getBibloTableName(index);
//        String ciTable = getCitationTableName(index);


//        insertFamily(conn, "patent.family_patstat", patents);
        insertBiblio(conn, biTable, patents);
//        insertCitation(conn, ciTable, patents);

    }


    private static String getBibloTableName(String index) {
        if (index == null)
            return null;

        return "citation.bibliographic_" + index;
    }

    private static String getCitationTableName(String index) {
        if (index == null)
            return null;

        return "citation.citation_" + index;
    }

    private static void insertCitation(Connection conn, String table, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into " + table +
                "(" +
                "documentId, sequence ,patentId, citedType, examinerCitationState, citedPhase, country,documentNumber," +
                "documentType, date, name, text" +
                ") " +
                " values(?,?,?,?,?,?,?,?,?,?,?,?) " +
                " ON DUPLICATE KEY UPDATE " +
                " name = name";

        PreparedStatement statement = null;

        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

//            System.out.println(insertQuery);

            for (Patent patent : patents) {
                List<CitationDoc> citationDocs = patent.getCitationDoc();

                if (citationDocs == null)
                    continue;


                int seq = 0;
                for (CitationDoc citation : citationDocs) {
                    if ("U".equals(citation.getCitedType()))
                        continue;

                    ++seq;

                    statement.setString(1, patent.getDocumentId());
                    statement.setInt(2, (citation.getSequence() == null) ? seq : citation.getSequence());
                    statement.setString(3, (patent.getPatentId() == null) ? null : patent.getPatentId());
                    statement.setString(4, (citation.getCitedType() == null) ? null : citation.getCitedType());
                    statement.setString(5, (citation.getExaminerCitationState() == null) ? null : citation.getExaminerCitationState());
                    statement.setString(6, (citation.getCitedPhase() == null) ? null : citation.getCitedPhase());
                    statement.setString(7, (citation.getCountry() == null) ? null : citation.getCountry());
                    statement.setString(8, (citation.getDocumentNumber() == null) ? null : citation.getDocumentNumber());
                    statement.setString(9, (citation.getKind() == null) ? null : citation.getKind());
                    statement.setString(10, (citation.getDate() == null) ? null : citation.getDate());
                    statement.setString(11, (citation.getName() == null) ? null : citation.getName());

                    String text = citation.getText();
                    if (text != null) {
                        if (text.length() > 1000)
                            text = text.substring(0, 1000);
                    }

                    statement.setString(12, (text == null) ? null : text);

                    statement.addBatch();
                }
            }

            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertBiblio(Connection conn, String table, ArrayList<Patent> patents) {
        String insertQuery = "INSERT ignore into " + table +
                "(" +
                "documentId,patentId , documentType, publicationNumber, publicationDate, registerNumber, registerDate,openNumber," +
                "openDate, applicationNumber, applicationDate" +
                ") " +
                " values(?,?,?,?,?,?,?,?,?,?,?) "
                ;


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String documentId = patent.getDocumentId();
                if (documentId == null)
                    continue;

                statement.setString(1, patent.getDocumentId());
                statement.setString(2, (patent.getPatentId() == null) ? null : patent.getPatentId());
                statement.setString(3, (patent.getDocumentType() == null) ? null : patent.getDocumentType());
                statement.setString(4, (patent.getPublicationNumber() == null) ? null : patent.getPublicationNumber());
                statement.setString(5, (patent.getPublicationDate() == null) ? null : patent.getPublicationDate());
                statement.setString(6, (patent.getRegisterNumber() == null) ? null : patent.getRegisterNumber());
                statement.setString(7, (patent.getRegisterDate() == null) ? null : patent.getRegisterDate());
                statement.setString(8, (patent.getOpenNumber() == null) ? null : patent.getOpenNumber());
                statement.setString(9, (patent.getOpenDate() == null) ? null : patent.getOpenDate());
                statement.setString(10, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                statement.setString(11, (patent.getApplicationDate() == null) ? null : patent.getApplicationDate());

                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void insertFamily(Connection conn, String table, ArrayList<Patent> patents) {
        String insertQuery = "INSERT ignore into " + table +
                "(" +
                "documentId,patentId , documentType, publicationNumber, publicationDate, registerNumber, registerDate,openNumber," +
                "openDate, applicationNumber, applicationDate, docdbFamilyId, inpadocFamilyId " +
                ") " +
                " values(?,?,?,?,?,?,?,?,?,?,?,?,?) ";
//                +
//                " ON DUPLICATE KEY UPDATE " +
//                " docdbFamilyId = docdbFamilyId, inpadocFamilyId = inpadocFamilyId";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String documentId = patent.getDocumentId();
                if (documentId == null)
                    continue;

                statement.setString(1, patent.getDocumentId());
                statement.setString(2, (patent.getPatentId() == null) ? null : patent.getPatentId());
                statement.setString(3, (patent.getDocumentType() == null) ? null : patent.getDocumentType());
                statement.setString(4, (patent.getPublicationNumber() == null) ? null : patent.getPublicationNumber());
                statement.setString(5, (patent.getPublicationDate() == null) ? null : patent.getPublicationDate());
                statement.setString(6, (patent.getRegisterNumber() == null) ? null : patent.getRegisterNumber());
                statement.setString(7, (patent.getRegisterDate() == null) ? null : patent.getRegisterDate());
//                statement.setString(8, (patent.getOpenNumber() == null) ? null : patent.getOpenNumber());
                statement.setString(8, (patent.getAppln_nr_epodoc() == null) ? null : patent.getAppln_nr_epodoc());
                statement.setString(9, (patent.getOpenDate() == null) ? null : patent.getOpenDate());
                statement.setString(10, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                statement.setString(11, (patent.getApplicationDate() == null) ? null : patent.getApplicationDate());
                statement.setString(12, (patent.getDocdbFamilyId() == null) ? null : patent.getDocdbFamilyId());
                statement.setString(13, (patent.getInpadocFamilyId() == null) ? null : patent.getInpadocFamilyId());

                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertFamilyDocdb(Connection conn, String table, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into " + table +
                "(" +
                "documentId, documentType, publicationNumber, publicationDate, registerNumber, registerDate,openNumber," +
                "openDate, applicationNumber, applicationDate, docdbFamilyId, inpadocFamilyId " +
                ") " +
                " values(?,?,?,?,?,?,?,?,?,?,?,?) " +
                " ON DUPLICATE KEY UPDATE " +
                " docdbFamilyId = docdbFamilyId, inpadocFamilyId = inpadocFamilyId";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String documentId = patent.getDocumentId();
                if (documentId == null)
                    continue;

                statement.setString(1, patent.getDocumentId());
                statement.setString(2, (patent.getDocumentType() == null) ? null : patent.getDocumentType());
                statement.setString(3, (patent.getPublicationNumber() == null) ? null : patent.getPublicationNumber());
                statement.setString(4, (patent.getPublicationDate() == null) ? null : patent.getPublicationDate());
                statement.setString(5, (patent.getRegisterNumber() == null) ? null : patent.getRegisterNumber());
                statement.setString(6, (patent.getRegisterDate() == null) ? null : patent.getRegisterDate());
//                statement.setString(8, (patent.getOpenNumber() == null) ? null : patent.getOpenNumber());
                statement.setString(7, (patent.getAppln_nr_epodoc() == null) ? null : patent.getAppln_nr_epodoc());
                statement.setString(8, (patent.getOpenDate() == null) ? null : patent.getOpenDate());
                statement.setString(9, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                statement.setString(10, (patent.getApplicationDate() == null) ? null : patent.getApplicationDate());
                statement.setString(11, (patent.getDocdbFamilyId() == null) ? null : patent.getDocdbFamilyId());
                statement.setString(12, (patent.getInpadocFamilyId() == null) ? null : patent.getInpadocFamilyId());

                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertFamily2(Connection conn, String table, ArrayList<Patent> patents) {
        String insertQuery = "INSERT into " + table +
                "(" +
                "documentId,patentId , documentType, publicationNumber, publicationDate, registerNumber, registerDate,openNumber," +
                "openDate, applicationNumber, applicationDate, docdbFamilyId, inpadocFamilyId, " +
                "internationalApplicationNumber, internationalApplicationDate, internationalPublicationNumber, internationalPublicationDate " +
                ", legalStatus " +
                ") " +
                " values(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?) " +
                " ON DUPLICATE KEY UPDATE " +
                " docdbFamilyId = values(docdbFamilyId)" +
                " ,status = null " +
//                ", inpadocFamilyId = values(inpadocFamilyId)," +
//                " , status = null" +
                " ";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String documentId = patent.getDocumentId();
                if (documentId == null)
                    continue;

                statement.setString(1, patent.getDocumentId());
                statement.setString(2, (patent.getPatentId() == null) ? null : patent.getPatentId());
                statement.setString(3, (patent.getDocumentType() == null) ? null : patent.getDocumentType());
                statement.setString(4, (patent.getPublicationNumber() == null) ? null : patent.getPublicationNumber());
                statement.setString(5, (patent.getPublicationDate() == null) ? null : patent.getPublicationDate());
                statement.setString(6, (patent.getRegisterNumber() == null) ? null : patent.getRegisterNumber());
                statement.setString(7, (patent.getRegisterDate() == null) ? null : patent.getRegisterDate());
                statement.setString(8, (patent.getOpenNumber() == null) ? null : patent.getOpenNumber());
//                statement.setString(8, (patent.getAppln_nr_epodoc() == null) ? null : patent.getAppln_nr_epodoc());
                statement.setString(9, (patent.getOpenDate() == null) ? null : patent.getOpenDate());
                statement.setString(10, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                statement.setString(11, (patent.getApplicationDate() == null) ? null : patent.getApplicationDate());
                statement.setString(12, (patent.getDocdbFamilyId() == null) ? null : patent.getDocdbFamilyId());
                statement.setString(13, (patent.getExtendedFamilyId() == null) ? null : patent.getExtendedFamilyId());
                statement.setString(14, (patent.getInternationalApplicationNumber() == null) ? null : patent.getInternationalApplicationNumber());
                statement.setString(15, (patent.getInternationalApplicationDate() == null) ? null : patent.getInternationalApplicationDate());
                statement.setString(16, (patent.getInternationalPublicationNumber() == null) ? null : patent.getInternationalPublicationNumber());
                statement.setString(17, (patent.getInternationalPublicationDate() == null) ? null : patent.getInternationalPublicationDate());
                statement.setString(18, (patent.getLegalStatus() == null) ? null : patent.getLegalStatus());


                statement.addBatch();
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertPriorityClaims(Connection conn, String tableName, ArrayList<Patent> patents) {
        String insertQuery = "INSERT ignore into " + tableName +
                "(" +
                "documentId ,seq ,applicationNumber ,priorityApplicationNumber ,priorityApplicationCountry ,priorityApplicationDate " +
                " ,docdbFamilyId ,inpadocFamilyId ,extendedFamilyId" +
                ") " +
                " values(?,?,?,?,?, ?,?,?,?) ";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for (Patent patent : patents) {
                String documentId = patent.getDocumentId();
                if (documentId == null)
                    continue;

                List<PriorityClaim> priorityClaims = patent.getPriorityClaims();
                if (priorityClaims == null || priorityClaims.isEmpty())
                    continue;

                int seq = 0;

                for (PriorityClaim priorityClaim : priorityClaims) {
                    statement.setString(1, patent.getDocumentId());
                    statement.setInt(2, ++seq);
                    statement.setString(3, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                    statement.setString(4, (priorityClaim.getPriorityApplicationNumber() == null) ? null : priorityClaim.getPriorityApplicationNumber());
                    statement.setString(5, (priorityClaim.getPriorityApplicationCountry() == null) ? null : priorityClaim.getPriorityApplicationCountry());
                    statement.setString(6, (priorityClaim.getPriorityApplicationDate() == null) ? null : priorityClaim.getPriorityApplicationDate());
                    statement.setString(7, (patent.getDocdbFamilyId() == null) ? null : patent.getDocdbFamilyId());
                    statement.setString(8, (patent.getInpadocFamilyId() == null) ? null : patent.getInpadocFamilyId());
                    statement.setString(9, (patent.getExtendedFamilyId() == null) ? null : patent.getExtendedFamilyId());

                    statement.addBatch();
                }
            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void originalApplicationNumber(Connection conn, String tableName, ArrayList<Patent> patents) {
        String insertQuery = "INSERT ignore into " + tableName +
                "(" +
                "documentId, applicationNumber ,originalApllicationNumber  " +
                " ,docdbFamilyId ,inpadocFamilyId ,extendedFamilyId, originalApplicationDate" +
                ") " +
                " values(?,?,?,?,?, ?,?) ";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();
            ;
            for (Patent patent : patents) {
                String documentId = patent.getDocumentId();
                if (documentId == null)
                    continue;

                statement.setString(1, patent.getDocumentId());
                statement.setString(2, (patent.getApplicationNumber() == null) ? null : patent.getApplicationNumber());
                statement.setString(3, (patent.getOriginalApplicationNumber() == null) ? null : patent.getOriginalApplicationNumber());
                statement.setString(4, (patent.getDocdbFamilyId() == null) ? null : patent.getDocdbFamilyId());
                statement.setString(5, (patent.getInpadocFamilyId() == null) ? null : patent.getInpadocFamilyId());
                statement.setString(6, (patent.getExtendedFamilyId() == null) ? null : patent.getExtendedFamilyId());
                statement.setString(7, (patent.getOriginalApplicationDate() == null) ? null : patent.getOriginalApplicationDate());

                statement.addBatch();

            }

            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
