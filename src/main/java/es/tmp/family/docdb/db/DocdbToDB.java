package es.tmp.family.docdb.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.v1.PatentVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class DocdbToDB {
    public static void main(String[] args) {
        ESConfig esConfig = new ESConfig("es188");

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now().minus(Period.ofDays(7));
        String updateDate = dtf.format(localDate);

        String index = "docdb";
        String type = "patent";

        String[] includes = {
                "documentId"
                ,"applicationType"
                ,"epoPublicationNumber"
                ,"originalApplicationNumber"
                ,"documentType"
                ,"applicationNumber"
                ,"documentNumber"
                ,"applicationCountryCode"
                ,"standardApplicationNumber"
                ,"familyId"
                ,"epoApplicationNumber"
                ,"countryCode"
                ,"publicationDate"
                ,"publicationNumber"
                ,"applicationDate"
                ,"inpadocFamilyId"
                ,"extendedFamilyId"

        };
;

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.rangeQuery("firstInsertDate").from(updateDate))
                .mustNot(QueryBuilders.existsQuery("extendedFamilyId"))
                ;

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(includes, null)
                .setSize(50)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        int count = 0;

        ArrayList<PatentVO> patentVOArrayList = new ArrayList<>();

        ObjectMapper mapper = new ObjectMapper();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                if (++count % 1000 == 0)
                    System.out.println(count);


                try {
                    PatentVO patentVO = mapper.readValue(hit.getSourceAsString(), PatentVO.class);
                    if (patentVO == null)
                        continue;


                    patentVOArrayList.add(patentVO);

                    if (patentVOArrayList.size() > 100) {
                        insertDB(conn, patentVOArrayList);
                        patentVOArrayList.clear();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!patentVOArrayList.isEmpty())
            insertDB(conn, patentVOArrayList);

        System.out.println("Total Count : " + count);



    }

    private static void insertDB(Connection conn, ArrayList<PatentVO> patentVOArrayList) {
        String tableName = "family.docdb_family_new";

        String insertQuery = "INSERT ignore INTO " + tableName +
                "(" +
                "documentId ,applicationType ,epoPublicationNumber ,originalApplicationNumber ,documentType ,applicationNumber ,documentNumber ,applicationCountryCode ," +
                "standardApplicationNumber ,familyId ,epoApplicationNumber ,countryCode ,publicationDate ,publicationNumber ,applicationDate, inpadocFamilyId, inpadocFamilyId_change" +
                ") " +
                "values(?,?,?,?,?, ?,?,?,?,?, " +
                "       ?,?,?,?,?, ?,? ) " ;
//                 " ON DUPLICATE KEY UPDATE " +
//                " familyId_change = values(familyId) , status =9 ";
                ;


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();


            for (PatentVO patentVO : patentVOArrayList) {
                if (patentVO.documentId == null) {
                    continue;
                }

                statement.setString(1, patentVO.getDocumentId());
                statement.setString(2, patentVO.getApplicationType());
                statement.setString(3, patentVO.getEpoPublicationNumber());
                statement.setString(4, patentVO.getOriginalApplicationNumber());
                statement.setString(5, patentVO.getDocumentType());
                statement.setString(6, patentVO.getApplicationNumber());
                statement.setString(7, patentVO.getDocumentNumber());
                statement.setString(8, patentVO.getApplicationCountryCode());
                statement.setString(9, patentVO.getStandardApplicationNumber());
                statement.setString(10,patentVO.getFamilyId());
                statement.setString(11,patentVO.getEpoApplicationNumber());
                statement.setString(12,patentVO.getCountryCode());
                statement.setString(13,patentVO.getPublicationDate());
                statement.setString(14,patentVO.getPublicationNumber());
                statement.setString(15,patentVO.getApplicationDate());
                statement.setString(16,patentVO.getInpadocFamilyId());
                statement.setString(17,patentVO.getExtendedFamilyId());


                statement.addBatch();
//
//                if (patentVO.getPriorityClaimNumber() != null)
//                    System.out.println(patentVO.getPriorityClaimNumber());

            }


            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
