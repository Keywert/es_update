package es.tmp.family.docdb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.config.ESConfig;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class InsertInpadocFamily {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();

        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es205"));
//        esConfigs.add(new ESConfig("es51"));


        if (esConfigs.isEmpty())
            return;

        DataFarmDB dataFarmDB = new DataFarmDB();

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

//        String tableName = "family.docdb_family_20220113";
        String tableName = "family.docdb_family_new";


        String query = "select * from " + tableName +
                " where inpadocFamilyId_change is not null " +
                " and status is null " +
//                " and documentId not regexp '^(AU|US|CN|JP|EP|CA|FR|DE|KR|GB|IN|IT|TW|RU|WO).+$' " +
//                " and documentId = 'KR000000690140B1' " +
                "  ";
//                " and documentId like 'JP%'";

//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
//        LocalDate localDate = LocalDate.now();

        System.out.println(query);
        String index = "docdb";
        String type = "patent";

        int count = 0;
        Statement st = null;

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            ArrayList<String> docIds = new ArrayList<>();
            List<QueueVO> queueVOList = new ArrayList<>();

            while (rs.next()) {
                if (++count % 1000 == 0)
                    System.out.println(count);

                String documentId = rs.getString("documentId");
                if (documentId == null)
                    continue;

                Map<String, String> map = new HashMap<>();

//                String inpadocFamilyId = rs.getString("inpadocFamilyId");
//                if (inpadocFamilyId != null && !"".equals(inpadocFamilyId))
//                    map.put("inpadocFamilyId", inpadocFamilyId);


                String extendedFamilyId = rs.getString("inpadocFamilyId_change");
                if (extendedFamilyId != null && !"".equals(extendedFamilyId))
                    map.put("extendedFamilyId", extendedFamilyId);

                if (map.isEmpty())
                    continue;

//                map.put("lastUpdateDate",dtf.format(localDate));

                String json = objectMapper.writeValueAsString(map);

//                System.out.println(documentId +" : " +json);

                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                docIds.add(documentId);

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(documentId);
                queueVO.setLevel(8);
                queueVOList.add(queueVO);
//
                if (docIds.size() > 1000) {
                    updateDB(conn, tableName, docIds);
                    dataFarmDB.insertESQueue(queueVOList);
                    queueVOList.clear();
                    docIds.clear();
                }
            }

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            if (!docIds.isEmpty())
                updateDB(conn, tableName, docIds);

            System.out.println("Finish Count : " + count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }

    }

    private static void updateDB(Connection conn, String tableName, ArrayList<String> docIds) {
        String query = "UPDATE " + tableName + " SET status = 2" +
                " WHERE documentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : docIds) {
                preparedStatement.setString(1, docId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
