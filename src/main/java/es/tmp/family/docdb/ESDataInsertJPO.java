package es.tmp.family.docdb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.PatentFamilyVO;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ESDataInsertJPO {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es18"));

        if (esConfigs.isEmpty())
            return;

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();


        DataFarmDB dataFarmDB = new DataFarmDB();
        List<QueueVO> queueVOList = new ArrayList<>();


        String tableName = "family.jpo_family";
//        String tableName = "family.jpo_missing_patentNumber";
        String index = "jpo";
        String type = "patent";

//        String query = "select * from " +  tableName + " A inner join JPO.RIGHT_HOLDER_LIST B "
//                + " on A.applicationNumber_norm = B.APPLICATION_NUMBER "
//                + " where A.documentType regexp '^[A|B][0-9]?$' and B.TYPE = 'P' "
//                + " and A.status is null "
//                + " limit 10 ";

        String query = "select * from " + tableName +
                " where status is null and docdbFamilyId is not null" +

//                " and (publicationDate > '20220501' or registerDate > '20220501') " +
//                " and publicationDate = '20220113' " +
//                " and applicationNumber regexp '^(19|20)[0-9]{2}-5[0-9]{5}$'" +
//                " and inpadocFamilyId is not null " +
//                " and documentId in ('jp2021144508a','jp2013216224b2','jp2014218815b2','jp2015011332b2','jp2015087291b2','jp2015088055b2','jp2015097829b2','jp2015166643b2','jp2015548822b2','jp2015557539b2','jp2016001684b2','jp2016004269b2','jp2016012202b2','jp2016181594b2','jp2016232573b2','jp2016233827b2','jp2016530645b2','jp2016539956b2','jp2016540563b2','jp2016551767b2','jp2016559879b2','jp2016564446b2','jp2016564448b2','jp2016564569b2','jp2016564575b2','jp2016567364b2','jp2016567769b2','jp2016569934b2','jp2016572302b2','jp2017135738b2','jp2017135751b2','jp2017139197b2','jp2017149644b2','jp2017150902b2','jp2017154022b2','jp2017175162b2','jp2017503617b2','jp2017505836b2','jp2017507009b2','jp2017509591b2','jp2017518541b2','jp2017531839b2','jp2017540574b2','jp2017544584b2','jp2017548261b2','jp2017548401b2','jp2017553223b2','jp2017553231b2','jp2017556831b2','jp2017557093b2','jp2017564677b2','jp2018044572b2','jp2018072510b2','jp2018087973b2','jp2018132270b2','jp2018139094a','jp2018139094b2','jp2018155174a','jp2018179963a','jp2018211118a','jp2018211435a','jp2018211435b2','jp2018502252b2','jp2018504640b2','jp2018506438b2','jp2018508228b2','jp2018509608b2','jp2018510516b2','jp2018513337b2','jp2018514809b2','jp2018520536b2','jp2018525543b2','jp2018527730b2','jp2018527739b2','jp2018532448a','jp2018532448b2','jp2018533924a','jp2018533924b2','jp2018536264a','jp2018536264b2','jp2018537450a','jp2018537450b2','jp2018538085a','jp2018538085b2','jp2018538596a','jp2018538596b2','jp2018539933a','jp2018544154a','jp2018544154b2','jp2018544300a','jp2018544300b2','jp2018556490a','jp2018563551a','jp2018563551b2','jp2018568716a','jp2018568716b2','jp2019031207a','jp2019039298a','jp2019039298b2','jp2019090728a','jp2019090728b2','jp2019101176a','jp2019101176b2','jp2019105034a','jp2019105259a','jp2019105259b2','jp2019106906a','jp2019124976a','jp2019125723a','jp2019125723b2','jp2019166744a','jp2019166744b2','jp2019170084a','jp2019189151a','jp2019189151b2','jp2019197218a','jp2019197218b2','jp2019197219a','jp2019197219b2','jp2019208986a','jp2019208986b2','jp2019230341a','jp2019230341b2','jp2019507917a','jp2019507917b1','jp2019508858a','jp2019508858b2','jp2019520386a','jp2019520386b2','jp2019528746a','jp2019531391a','jp2019531391b1','jp2019541143a','jp2019541143b2','jp2019542559a','jp2019549462a','jp2019549562a','jp2019555464a','jp2019565867b2','jp2019565897a','jp2019566197a','jp2019567335a','jp2019570547a','jp2020004195b2','jp2020006109a','jp2020006109b2','jp2020007484b2','jp2020009842a','jp2020019148a','jp2020019148b2','jp2020020583a','jp2020021119a','jp2020023081a','jp2020025917a','jp2020087116a','jp2020110392a','jp2020111174a','jp2020111834a','jp2020151758a','jp2020160424a','jp2020160470a','jp2020160470b2','jp2020173121a','jp2020177399a','jp2020180095a','jp2020181624a','jp2020188526a','jp2020189237a','jp2020199659a','jp2020202383a','jp2020208048a','jp2020208183a','jp2020209892a','jp2020209897a','jp2020214835a','jp2020500687b2','jp2020501133a','jp2020519993a','jp2020520115a','jp2020523378a','jp2020526382a','jp2020541882a','jp2020542002a','jp2020542110a','jp2020542790a','jp2020544823a','jp2020545003a','jp2020545148a','jp2020546926a','jp2020548916a','jp2020549577a','jp2020551327a','jp2020560277a','jp2020564165a','jp2020565329a','jp2020565352a','jp2020569190a','jp2020569190b1','jp2020569823a','jp2021000326a','jp2021000428a','jp2021000555a','jp2021000555b2','jp2021001187a','jp2021009667a','jp2021014690a','jp2021019197a','jp2021022952a','jp2021023812a','jp2021031816a','jp2021033098a','jp2021037945a','jp2021042955a','jp2021049350a','jp2021056664a','jp2021059095a','jp2021059095b2','jp2021063880a','jp2021063975a','jp2021064509a','jp2021066885a','jp2021069760a','jp2021078485a','jp2021080099a','jp2021083133a','jp2021088059a','jp2021094240a','jp2021102162a','jp2021103691a','jp2021113222a','jp2021114157a','jp2021116343a','jp2021116806a','jp2021117213a','jp2021117492a','jp2021120073a','jp2021120074a','jp2021122349a','jp2021142843a','jp2021148619a','jp2021152763a','jp2021162502a','jp2021175480a','jp2021177028a','jp2021180745a','jp2021182236a','jp2021182622a','jp2021190070a','jp2021196864a','jp2021201968a','jp2021211037a','jp2021502857a','jp2021509185a','jp2021509187a','jp2021510174a','jp2021512751a','jp2021514571a','jp2021514571b2','jp2021518191a','jp2021519121a','jp2021523748a','jp2021525702a','jp2021529740a','jp2021531519a','jp2021534623a','jp2021539600a','jp2021543997a','jp2022003486a') " +
                " limit 50000";

        System.out.println(query);


//        String query = "select * from " +  tableName + " A left join JPO.RIGHT_HOLDER_LIST B "
//                + " on A.applicationNumber_norm = B.APPLICATION_NUMBER "
//                + " where B.APPLICATION_NUMBER is null "
////                + " where B.APPLICATION_NUMBER is null and A.documentType regexp '^[A|B][0-9]?$' and B.TYPE = 'P' "
//                + " and A.status is null "
//                + " limit 5000000 "
//                ;

        Statement st = null;

        int count = 0;

        ArrayList<String> docIds = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (++count % 20 == 0) {
                    System.out.println(count);
                    Thread.sleep(1000);

                }
                String documentId = rs.getString("documentId");

                docIds.add(documentId);
                PatentFamilyVO patentFamilyVO = new PatentFamilyVO();

                patentFamilyVO.setPatentId(rs.getString("patentId"));

                patentFamilyVO.setApplicationDate(rs.getString("applicationDate"));

                String patentNumber = rs.getString("registerNumber");
//                if (patentNumber == null)
//                    patentNumber = rs.getString("announcementNumber");

                if (patentNumber == null)
                    patentNumber = rs.getString("publicationNumber");

                if (patentNumber == null)
                    patentNumber = rs.getString("openNumber");

                if (patentNumber != null && !"".equals(patentNumber))
                    patentFamilyVO.setPatentNumber(patentNumber);


                String patentDate = rs.getString("registerDate");
//                if (patentDate == null)
//                    patentDate = rs.getString("announcementDate");

                if (patentDate == null)
                    patentDate = rs.getString("publicationDate");

                if (patentDate == null)
                    patentDate = rs.getString("openDate");

                if (patentDate != null && !"".equals(patentDate))
                    patentFamilyVO.setPatentDate(patentDate);

//                patentFamilyVO.setAnnouncementNumber(rs.getString("announcementNumber"));
//                patentFamilyVO.setAnnouncementDate(rs.getString("announcementDate"));

                patentFamilyVO.setRegisterNumber(rs.getString("registerNumber"));
                patentFamilyVO.setRegisterDate(rs.getString("registerDate"));

                patentFamilyVO.setPublicationNumber(rs.getString("publicationNumber"));
                patentFamilyVO.setPublicationDate(rs.getString("publicationDate"));

                patentFamilyVO.setDocdbFamilyId(rs.getString("docdbFamilyId"));
                patentFamilyVO.setExtendedFamilyId(rs.getString("inpadocFamilyId"));

                patentFamilyVO.setInternationalApplicationNumber(rs.getString("internationalApplicationNumber"));
                patentFamilyVO.setInternationalApplicationDate(rs.getString("internationalApplicationDate"));
                patentFamilyVO.setInternationalPublicationNumber(rs.getString("internationalPublicationNumber"));
                patentFamilyVO.setInternationalPublicationDate(rs.getString("internationalPublicationDate"));

//                patentFamilyVO.setLegalStatus(rs.getString("legalStatus"));

                String json = objectMapper.writeValueAsString(patentFamilyVO);

//                System.out.println(documentId +" : " +json);

                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));


                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(documentId);
                queueVO.setLevel(4);
                queueVOList.add(queueVO);

                if (docIds.size() > 20) {
                    updateDB(conn, tableName, docIds);
                    docIds.clear();

                    dataFarmDB.insertESQueue(queueVOList);
                    queueVOList.clear();
//                    Thread.sleep(1000);
                }
            }

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            if (!docIds.isEmpty())
                updateDB(conn, tableName, docIds);

            System.out.println(count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }

    }

    private static void updateDB(Connection conn, String tableName , ArrayList<String> docIds) {
        String query = "UPDATE " + tableName + " SET status = 1" +
                " WHERE documentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String documentId : docIds) {
                preparedStatement.setString(1,documentId);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

