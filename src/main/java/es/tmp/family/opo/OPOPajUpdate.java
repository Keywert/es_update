package es.tmp.family.opo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class OPOPajUpdate {
    public static void main(String[] args) {
        DataFarmDB dataFarmDB = new DataFarmDB();
        ESConfig esConfig = new ESConfig("es188");
        Set<String> exceptFamilyIdSet = dataFarmDB.selectExceptFamily();

        ESConfig esConfigOPO = new ESConfig("es28");
        String index = "paj";
        String type = "patent";

        String opo_index = "opo_jpo";

        String start = "20240701";
        String end = "20251231";

        String[] fields = {
                "mainFamilyId", "main-family", "main-familyCount", "mainFamilyEarliestDate",
                "extendFamilyId", "extended-family", "extended-familyCount", "extendFamilyEarliestDate",
                "docdbFamilyId", "docdbFamilyInfo", "docdbFamilyInfoCount",
                "completeFamilyId", "complete-family", "complete-familyCount", "completeFamilyEarliestDate",
                "patentNumber", "documentType","patentDate"
        };

        String[] docTypes = {"A","B","a","a1","b","b1","b2"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.prefixQuery("_id","jp19"))
                .filter(QueryBuilders.termsQuery("documentType",docTypes))
//                .filter(QueryBuilders.termsQuery("extendFamilyId", "225997340"))
                .mustNot(QueryBuilders.termsQuery("extendFamilyId", exceptFamilyIdSet))
//                .filter(QueryBuilders.rangeQuery("patentDate").from("20000101").to("20241231"))
                .filter(QueryBuilders.rangeQuery("opoPfChangeDate").from(start).to(end))
                .filter(QueryBuilders.existsQuery("extendFamilyId"));

        SearchResponse scrollResp = esConfigOPO.client.prepareSearch(opo_index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

        System.out.println("Total Hit : " + scrollResp.getHits().getTotalHits());
        List<QueueVO> queueVOList = new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();
        int count = 0;

        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count%1000 == 0)
                        System.out.println(count);

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString());
                    JsonNode extendFamilyId = jsonNode.get("extendFamilyId");
                    if (extendFamilyId == null)
                        continue;

                    JsonNode patentNumber = jsonNode.get("patentNumber");
                    if (patentNumber == null)
                        continue;

                    JsonNode documentType = jsonNode.get("documentType");
                    if (documentType == null)
                        continue;

                    String docId = checkFamily(esConfig, index, extendFamilyId.asText(),patentNumber.asText(),documentType.asText());
                    if (docId == null)
                        continue;

                    ((ObjectNode) jsonNode).remove("patentNumber");
                    ((ObjectNode) jsonNode).remove("documentType");
                    ((ObjectNode) jsonNode).remove("patentDate");

                    String json = objectMapper.writeValueAsString(jsonNode);

                    System.out.println(extendFamilyId  + " : " + docId + " : " + json);

                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, docId)
                            .doc(json.getBytes())
                            .upsert(json.getBytes()));

                    QueueVO queueVO = new QueueVO();
                    queueVO.setEsindex(index);
                    queueVO.setDocumentId(docId);
                    queueVO.setLevel(8);

                    queueVOList.add(queueVO);
                    if (queueVOList.size() > 5) {
                        dataFarmDB.insertESQueue(queueVOList);
                        queueVOList.clear();
                    }

                }
                scrollResp = esConfigOPO.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String checkFamily(ESConfig esConfig, String index, String extendFamilyId, String patentNumber, String documentType) {
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.boolQuery()
                        .should(QueryBuilders.termsQuery("patentNumber",patentNumber))
                        .should(QueryBuilders.termsQuery("registerNumber",patentNumber))
                        .should(QueryBuilders.termsQuery("publicationNumber",patentNumber)))
                .filter(QueryBuilders.matchQuery("documentType",documentType))
                .filter(QueryBuilders.rangeQuery("patentDate").from("20000101").to("20241231"))
                .mustNot(QueryBuilders.matchQuery("extendFamilyId", extendFamilyId));

//        System.out.println(bqb);

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes("patent")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

        for (SearchHit hit : scrollResp.getHits().getHits()) {
            return hit.getId();
        }

        return null;
    }


}
