package es.tmp.litigation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.config.ESConfig;
import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class InsertLitigation {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String type = "patent";

        DataFarmDB dataFarmDB = new DataFarmDB();
        Map<String, ESConfig> datafarmESMap = new HashMap<>();
        datafarmESMap.put("es18",new ESConfig("es18"));
        datafarmESMap.put("es188",new ESConfig("es188"));
        datafarmESMap.put("es189",new ESConfig("es189"));

        String tableName = "litigation.src_for_patent";
        String query = "select document_id, group_concat(distinct action_type separator '###') actionTypes, group_concat(distinct case_id separator '###') caseIds from litigation.src_for_patent " +
                " where document_id in (select distinct document_id from litigation.src_for_patent where level is null) " +
                " group by document_id ";

        Statement st = null;

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);
            List<QueueVO> queueVOList = new ArrayList<>();
            Set<String> docIds = new HashSet<>();

            while (rs.next()) {
                String documentId = rs.getString("document_id");
                if (documentId == null)
                    continue;

                String actionTypes = rs.getString("actionTypes");
                if (actionTypes == null)
                    continue;

                String caseIds = rs.getString("caseIds");
                if (caseIds == null)
                    continue;

                String index = getIndex(documentId);
                if (index == null) {
                    System.out.println(documentId);
                    continue;
                }

                String clusterName = dataFarmDB.selectClusterInfo(index, 1);
                if (clusterName == null)
                    continue;

                ESConfig esConfig = datafarmESMap.get(clusterName);
                Map<String,Object> jsonMap = new HashMap<>();
                LitigationVO litigationVO = new LitigationVO();
                litigationVO.setHasLitigation(true);
                litigationVO.setActionTypes(Arrays.asList(actionTypes.split("###")));
                litigationVO.setCaseIds(Arrays.asList(caseIds.split("###")));

                jsonMap.put("litigationInfo",litigationVO);

                String json = objectMapper.writeValueAsString(jsonMap);
                System.out.println(index + " - "+ documentId +  " : " + json);

                esConfig.bulkProcessor.add(new UpdateRequest(index, type, documentId)
                        .doc(json.getBytes())
                        .upsert(json.getBytes()));

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(documentId);
                queueVO.setLevel(4);

                docIds.add(documentId);

                queueVOList.add(queueVO);
                if (queueVOList.size() > 100) {
                    dataFarmDB.insertESQueue(queueVOList);
                    updateDB(conn,tableName,docIds);

                    docIds.clear();
                    queueVOList.clear();
                }

            }

            if (!queueVOList.isEmpty()) {
                updateDB(conn,tableName,docIds);
                dataFarmDB.insertESQueue(queueVOList);
            }

            for (ESConfig esConfig : datafarmESMap.values())
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String tableName, Set<String> docIds) {
        String query = "UPDATE  "+ tableName +" SET " +
                " level = 99 "+
                "  WHERE document_id = ?";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(query);
            statement.clearParameters();

            for (String docId : docIds) {
                statement.setString(1, docId); // family_c_count

                statement.addBatch();
            }
            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static String getIndex(String documentId) {
        if (documentId.matches("^kr.+$"))
            return "kipo";
        else if (documentId.matches("^usps.+$"))
            return "uspto_past";
        else if (documentId.matches("^jpps.+$"))
            return "jpo_past";
        else if (documentId.matches("^us.+$"))
            return "uspto";
        else if (documentId.matches("^jp.+$"))
            return "jpo";
        else if (documentId.matches("^paj.+$"))
            return "paj";
        else if (documentId.matches("^wo.+$"))
            return "pct";
        else if (documentId.matches("^pct.+$"))
            return "pct";
        else if (documentId.matches("^ep.+$"))
            return "epo";
        else if (documentId.matches("^cn.+$"))
            return "sipo";
        else if (documentId.matches("^de.+$"))
            return "dpma";
        else if (documentId.matches("^au.+$"))
            return "aupo";
        else if (documentId.matches("^ca.+$"))
            return "capo";
        else if (documentId.matches("^fr.+$"))
            return "frpo";
        else if (documentId.matches("^gb.+$"))
            return "gbpo";
        else if (documentId.matches("^in.+$"))
            return "inpo";
        else if (documentId.matches("^it.+$"))
            return "itpo";
        else if (documentId.matches("^ru.+$"))
            return "rupo";
        else if (documentId.matches("^tw.+$"))
            return "tipo";
        else if (documentId.matches("^[A-Z]{2}.+$"))
            return "docdb";

        return null;
    }
}
