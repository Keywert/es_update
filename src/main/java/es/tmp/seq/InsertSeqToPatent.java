package es.tmp.seq;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class InsertSeqToPatent {
    public static void main(String[] args) {
        ESConfig esSeqConfig = new ESConfig("es28");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String seqIndex = "seq_detail";

        Map<String, ESConfig> datafarmESMap = new HashMap<>();
        datafarmESMap.put("es18", new ESConfig("es18"));
        datafarmESMap.put("es188", new ESConfig("es188"));
//        datafarmESMap.put("es189", new ESConfig("es189"));

        String[] indexs = {"kipo","epo","uspto","pct"};
        String type = "patent";

        DataFarmDB dataFarmDB = new DataFarmDB();
        try {
            for (String index : indexs) {
                String clusterName = dataFarmDB.selectClusterInfo(index, 1);
                if (clusterName == null)
                    continue;

                System.out.println(index + " : " + clusterName);

                ESConfig esConfig = datafarmESMap.get(clusterName);

                for (int i = 0, len = 100; i < len; i++) {
                    Set<String> targetIds = getSeqTargetIds(esSeqConfig, esConfig, index);
                    if (targetIds == null)
                        break;

                    System.out.println(targetIds.size());

                    String[] ids = {"kr20147035189a"};
                    List<QueueVO> queueVOList = new ArrayList<>();

                    for (String id : targetIds) {
                        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                                .filter(QueryBuilders.matchQuery("documentId", id))
                                .filter(QueryBuilders.existsQuery("seqContent"))
                                ;

                        String[] fields = {"documentId", "seqNumber", "seqLength", "seqContent"};

                        SearchResponse scrollResp = esSeqConfig.client.prepareSearch(seqIndex)
                                .setTypes(type)
                                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//                        .setScroll(new TimeValue(300000))
                                .setQuery(bqb)
                                .setSize(10000)
                                .setFetchSource(fields, null)
                                .addSort("seqNumber", SortOrder.ASC)
                                .execute()
                                .actionGet();

                        ObjectMapper objectMapper = new ObjectMapper();

                        long sumLength = 0;

                        List<String> seqContentList = new ArrayList<>();
                        for (SearchHit hit : scrollResp.getHits().getHits()) {
                            SequenceVO sequenceVO = objectMapper.readValue(hit.getSourceAsString(), SequenceVO.class);
                            List<String> seqContent = sequenceVO.getSeqContent();
                            if (seqContent == null)
                                continue;

                            for (String seq : seqContent) {
                                sumLength = sumLength + seq.getBytes().length;
                                if (sumLength > 524288)
                                    break;

                                seqContentList.add(seq);
                            }

                            if (sumLength > 524288)
                                break;

                        }

                        if (!seqContentList.isEmpty()) {
                            Map<String, Object> sequence = new HashMap<>();
                            sequence.put("seqContent", seqContentList);
                            sequence.put("seqFlag", true);

                            Map<String, Object> jsonMap = new HashMap<>();
                            jsonMap.put("sequence", sequence);
                            jsonMap.put("lastUpdateDate", dtf.format(localDate));
                            jsonMap.put("seqUpdateDate", dtf.format(localDate));

                            String json = objectMapper.writeValueAsString(jsonMap);

                            esConfig.bulkProcessor.add(new UpdateRequest(index, type, id)
                                    .doc(json.getBytes())
                                    .upsert(json.getBytes()));

                            QueueVO queueVO = new QueueVO();
                            queueVO.setEsindex(index);
                            queueVO.setDocumentId(id);
                            queueVO.setLevel(4);

                            queueVOList.add(queueVO);
                            if (queueVOList.size() > 10) {
                                dataFarmDB.insertESQueue(queueVOList);
                                queueVOList.clear();
                            }
                        }
                    }

                    if (!queueVOList.isEmpty())
                        dataFarmDB.insertESQueue(queueVOList);
                }
            }
            for (ESConfig esConfig : datafarmESMap.values())
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Set<String> getSeqTargetIds(ESConfig esSeqConfig, ESConfig esConfig, String index) {
        Set<String> docIds = getESDocIds(esConfig, index);
        if (docIds == null)
            docIds = new HashSet<>();

//        System.out.println(docIds.size());

        Set<String> seqDocIds = getSeqAggDocIds(esSeqConfig, index, docIds);
        if (seqDocIds == null)
            return null;

//        System.out.println(seqDocIds.size());

        if (!seqDocIds.isEmpty())
            return seqDocIds;

        return null;
    }

    private static Set<String> getSeqAggDocIds(ESConfig esConfig, String index, Set<String> docIds) {
        String sepIndex = "seq_detail";
        String type = "patent";

        String cc = "us";
        if (index.matches("kipo"))
            cc = "kr";
        else if (index.matches("pct"))
            cc = "wo";
        else if (index.matches("epo"))
            cc = "ep";

        String[] fields = {"documentId"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("seqContent"))
                .filter(QueryBuilders.prefixQuery("documentId", cc))
                .mustNot(QueryBuilders.termsQuery("documentId", docIds));

        SearchResponse response = esConfig.client.prepareSearch(sepIndex)
                .setTypes(type)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .addAggregation(AggregationBuilders.terms("docIdAgg").field("documentId").size(10000))
                .setFetchSource(fields, null)
                .setSize(0)
                .execute()
                .actionGet();

        Set<String> hitDocIds = new HashSet<>();

        Terms agg = response.getAggregations().get("docIdAgg");
        for (Terms.Bucket term : agg.getBuckets()) {
            hitDocIds.add(term.getKey().toString());
        }

        if (!hitDocIds.isEmpty())
            return hitDocIds;

        return null;
    }

    private static Set<String> getESDocIds(ESConfig esConfig, String index) {
        String type = "patent";
        String[] fields = {"documentId"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.existsQuery("sequence"));

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1000)
                .execute()
                .actionGet();

        Set<String> docIds = new HashSet<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                docIds.add(hit.getId());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!docIds.isEmpty())
            return docIds;

        return null;
    }

    private static Set<String> getSeqDocIds(ESConfig esConfig, String index) {
        String sepIndex = "seq_detail";
        String type = "patent";

        String cc = "us";
        if (index.matches("kipo"))
            cc = "kr";
        else if (index.matches("pct"))
            cc = "wo";
        else if (index.matches("epo"))
            cc = "ep";

        String[] fields = {"documentId"};
        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.prefixQuery("documentId", cc));

        SearchResponse scrollResp = esConfig.client.prepareSearch(sepIndex)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(1000)
                .execute()
                .actionGet();

        System.out.println("seq_detail Hits : " + scrollResp.getHits().getTotalHits());

        Set<String> docIds = new HashSet<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Object documentId = hit.getSource().get("documentId");
                if (documentId != null)
                    docIds.add(documentId.toString());
            }
            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!docIds.isEmpty())
            return docIds;


        return null;
    }
}
