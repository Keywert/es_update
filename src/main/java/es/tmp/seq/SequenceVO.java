package es.tmp.seq;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SequenceVO {
    public String documentId;
    public Integer seqNumber;
    public Integer seqLength;
    public List<String> seqContent;

}
