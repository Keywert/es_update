package es.tmp.update;

import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ESDataCheck {
    public boolean updateDataCheck(List<ESConfig> esConfigs, Map<String, List<String>> map, String updateDate) {
        List<String> docIds = new ArrayList<>();
        for (String index : map.keySet()) {
            List<String> docId = map.get(index);
            if (docId != null)
                docIds.addAll(docId);
        }
        String[] fields = {
                "familyId"
        };

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.rangeQuery("lastUpdateDate").from(updateDate).to(updateDate))
                .filter(QueryBuilders.idsQuery().addIds(docIds));

        for (ESConfig esConfig : esConfigs) {
            SearchResponse response = esConfig.client.prepareSearch(map.keySet().toArray(new String[0]))
                    .setTypes("patent")
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setScroll(new TimeValue(200000))
                    .setQuery(bqb)
                    .setFetchSource(fields, null)
                    .setSize(0)
                    .execute()
                    .actionGet();

            long count = response.getHits().getTotalHits();

            if (docIds.size() != count) {
                System.out.println(esConfig.getSettings().get("cluster.name"));
                return false;
            }
        }

        return true;
    }

    public boolean classUpdateDataCheck(List<ESConfig> esConfigs, List<String> docIds, int hitCount, String updateDate) {
        String[] fields = {
                "documentId"
        };

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.rangeQuery("lastUpdateDate").from(updateDate).to(updateDate))
                .filter(QueryBuilders.idsQuery().addIds(docIds))
                ;

        String index = "class_integ";

        for (ESConfig esConfig : esConfigs) {
            SearchResponse response = esConfig.client.prepareSearch(index)
                    .setTypes("patent")
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setScroll(new TimeValue(200000))
                    .setQuery(bqb)
                    .setFetchSource(fields, null)
                    .setSize(0)
                    .execute()
                    .actionGet();

            long count = response.getHits().getTotalHits();

            if (hitCount != count) {
                System.out.println(esConfig.getSettings().get("cluster.name") + " : " + hitCount + " - " +count );
                return false;
            }
        }

        return true;
    }

    public boolean seqUpdateDataCheck(List<ESConfig> esConfigs, List<String> docIds, int hitCount, String updateDate) {
        String[] fields = {
                "documentId"
        };

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.rangeQuery("lastUpdateDate").from(updateDate).to(updateDate))
                .filter(QueryBuilders.termsQuery("documentId",docIds))
                ;

        String index = "seq_detail";

        for (ESConfig esConfig : esConfigs) {
            SearchResponse response = esConfig.client.prepareSearch(index)
                    .setTypes("patent")
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setScroll(new TimeValue(200000))
                    .setQuery(bqb)
                    .setFetchSource(fields, null)
                    .setSize(0)
                    .execute()
                    .actionGet();

            long count = response.getHits().getTotalHits();

            if (hitCount != count) {
                System.out.println(esConfig.getSettings().get("cluster.name") + " : " + hitCount + " - " +count );
                return false;
            }
        }

        return true;
    }

    public boolean citationUpdateDataCheck(List<ESConfig> esConfigs, List<String> docIds, int hitCount, String updateDate) {
        String[] fields = {
                "documentId"
        };

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(docIds))
                ;

        String index = "citation_v2";

        for (ESConfig esConfig : esConfigs) {
            SearchResponse response = esConfig.client.prepareSearch(index)
                    .setTypes("patent")
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setScroll(new TimeValue(200000))
                    .setQuery(bqb)
                    .setFetchSource(fields, null)
                    .setSize(0)
                    .execute()
                    .actionGet();

            long count = response.getHits().getTotalHits();

            if (hitCount != count) {
                System.out.println(esConfig.getSettings().get("cluster.name") + " : " + hitCount + " - " +count );
                return false;
            }
        }

        return true;
    }
}
