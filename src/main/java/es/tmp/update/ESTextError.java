package es.tmp.update;

import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ESTextError {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();


//        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));


        if (esConfigs.isEmpty())
            return;

        String index = "uspto";
        String type = "patent";

        String[] docIds = {"us20240243277a1"};

        String[] fields = {"claims", "description","summaryCN","summaries","summary","inventionTitle"};

        IdsQueryBuilder iqb = QueryBuilders.idsQuery().addIds(docIds);

        String start = "20220501";
        String end = "20220830";

//        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.rangeQuery("patentDate").from(start).to(end));

        String[] srfields = {"inventionTitle", "claims.claimText","claims.independentClaimText","description"};

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.boolQuery()
                        .filter(QueryBuilders.multiMatchQuery("(3d",srfields)))
//                .filter(QueryBuilders.rangeQuery("patentDate").from(start).to(end))
                ;


        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(iqb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

        int count = 0;

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());


        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                if (++count % 10000 == 0)
                    System.out.println(count);

                String json = hit.getSourceAsString();
//                System.out.println(json);
                if (
//                        json.contains(" ")
//                        || json.contains(" ")
//                        ||
                        json.contains("\u2028") || json.contains("‑") || json.contains(" ") || json.contains("(3D)")
                ) {
                    System.out.println(hit.getId());
                    json = json.replaceAll(" ", " ")
                            .replaceAll("  "," ")
                            .replaceAll(" ", " ")
                            .replaceAll("\u2028", " ")
                            .replaceAll("‑","-")
                            .replaceAll("##","")
                            .replaceAll("\\(3D\\)", "( 3D )")
                            .replaceAll("\\(2D\\)", "( 2D )")
                    ;

                    System.out.println(json);
//
                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new UpdateRequest(index, type, hit.getId())
                                .doc(json.getBytes())
                                .upsert(json.getBytes()));
                }

            }
            scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        System.out.println(count);

        try {
            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
