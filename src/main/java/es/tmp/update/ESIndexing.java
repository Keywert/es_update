package es.tmp.update;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ESIndexing {
    public int indexing(ESConfig es_src, List<ESConfig> esConfigs, String index, List<String> docIds, String updateDate, int level) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery().filter(QueryBuilders.idsQuery().addIds(docIds));

        SearchResponse scrollResp = es_src.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(1)
                .execute()
                .actionGet();

        System.out.println("Start Reindex Count : " + scrollResp.getHits().getTotalHits());
        int count = 0;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    ++count;
//                    if (++count % 1000 == 0) {
//                        System.out.println(count);
//                    }

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
                    if (level == 1)
                        ((ObjectNode) jsonNode).put("firstInsertDate", updateDate);


                    ((ObjectNode) jsonNode).put("lastUpdateDate", updateDate);

                    String json = objectMapper.writeValueAsString(jsonNode);
                    for (ESConfig esConfig : esConfigs)
//                    es_dest.bulkProcessor.add(new IndexRequest(index, hit.getType(), hit.getId()).source(json));
                        esConfig.bulkProcessor.add((new UpdateRequest(index, type, hit.getId())
                                .doc(json.getBytes()))
                                .upsert(json.getBytes()));


                }
                scrollResp = es_src.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);


            return count;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int classIndexing(ESConfig es_src, List<ESConfig> esConfigs, String index, List<String> docIds, String updateDate) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery().filter(QueryBuilders.idsQuery().addIds(docIds));

        SearchResponse scrollResp = es_src.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(100)
                .execute()
                .actionGet();

        System.out.println("Start Reindex Count : " + scrollResp.getHits().getTotalHits());
        int count = 0;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    ++count;
//                    if (++count % 1000 == 0) {
//                        System.out.println(count);
//                    }

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
                    ((ObjectNode) jsonNode).put("lastUpdateDate", updateDate);

                    String json = objectMapper.writeValueAsString(jsonNode);
                    for (ESConfig esConfig : esConfigs)
//                    es_dest.bulkProcessor.add(new IndexRequest(index, hit.getType(), hit.getId()).source(json));
                        esConfig.bulkProcessor.add((new UpdateRequest(index, type, hit.getId())
                                .doc(json.getBytes()))
                                .upsert(json.getBytes()));


                }
                scrollResp = es_src.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            return count;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int seqIndexing(ESConfig es_src, List<ESConfig> esConfigs, String index, List<String> docIds, String updateDate) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termsQuery("documentId", docIds));

        SearchResponse scrollResp = es_src.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(3)
                .execute()
                .actionGet();

        System.out.println("Start Reindex Count : " + scrollResp.getHits().getTotalHits());
        int count = 0;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    ++count;

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    if (level == 1)
//                        ((ObjectNode) jsonNode).put("firstInsertDate", updateDate);

//                    ((ObjectNode) jsonNode).put("lastUpdateDate", updateDate);

                    String json = objectMapper.writeValueAsString(jsonNode);
                    for (ESConfig esConfig : esConfigs)
//                    es_dest.bulkProcessor.add(new IndexRequest(index, hit.getType(), hit.getId()).source(json));
                        esConfig.bulkProcessor.add((new UpdateRequest(index, type, hit.getId())
                                .doc(json.getBytes()))
                                .upsert(json.getBytes()));

                }
                scrollResp = es_src.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);


            return count;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void firstDateUpdate(ESConfig esConfig, String index, String updateDate, List<String> docIds) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.rangeQuery("firstInsertDate").from(updateDate).to(updateDate))
                .filter(QueryBuilders.termsQuery("documentId", docIds));

        String[] fields = {"documentId"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        System.out.println("FirstInsertDate Update Doc Count : " + scrollResp.getHits().getTotalHits());

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, String> map = new HashMap<>();
            map.put("firstInsertDate", updateDate);

            String json = objectMapper.writeValueAsString(map);
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    esConfig.bulkProcessor.add((new UpdateRequest(index, type, hit.getId())
                            .doc(json.getBytes()))
                            .upsert(json.getBytes()));
                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return;
    }

    public int citationIndexing(ESConfig es_src, List<ESConfig> esConfigs, String index, List<String> docIds, String updateDate) {
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(docIds));

        SearchResponse scrollResp = es_src.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(3)
                .execute()
                .actionGet();

        System.out.println("Start Reindex Count : " + scrollResp.getHits().getTotalHits());
        int count = 0;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    ++count;

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    if (level == 1)
//                        ((ObjectNode) jsonNode).put("firstInsertDate", updateDate);

//                    ((ObjectNode) jsonNode).put("lastUpdateDate", updateDate);

                    String json = objectMapper.writeValueAsString(jsonNode);
                    for (ESConfig esConfig : esConfigs)
//                    es_dest.bulkProcessor.add(new IndexRequest(index, hit.getType(), hit.getId()).source(json));
                        esConfig.bulkProcessor.add((new UpdateRequest(index, type, hit.getId())
                                .doc(json.getBytes()))
                                .upsert(json.getBytes()));

                }
                scrollResp = es_src.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);


            return count;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
