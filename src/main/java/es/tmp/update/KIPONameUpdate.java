package es.tmp.update;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.DBHelper;
import es.config.ESConfig;
import es.model.bulk.Patent;
import es.module.dataproc.norm.RepresentApn;
import es.module.db.QueueVO;
import es.module.kipris.KiprisAPI;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import es.module.db.DataFarmDB;
import org.elasticsearch.search.SearchHit;
import es.model.Applicant;
import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class KIPONameUpdate {
    public static void main(String[] args) {
        //  표준출원인별 출원건수 그래프 그릴때 출원인명 사라지는 현상 확인 요청
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es188"));
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String updateDate = dtf.format(localDate);

        KiprisAPI kiprisAPI = new KiprisAPI();
        RepresentApn representApn = new RepresentApn();
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();

//        DataFarmDB dataFarmDB = new DataFarmDB();

        String index = "kipo";
        String type = "patent";

        QueryBuilder qb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.idsQuery().addIds("kr20200013042b1"))
                .filter(QueryBuilders.boolQuery()
                        .should(QueryBuilders.matchQuery("firstApplicantInfo.seName.raw","")))
//                        .should(QueryBuilders.matchQuery("applicantInfo.seName.raw", ""))
//                        .should(QueryBuilders.matchQuery("currentApplicantInfo.seName.raw", ""))
//                        .should(QueryBuilders.matchQuery("rightHolderInfo.seName.raw", ""))
//                        .should(QueryBuilders.matchQuery("currentRightHolderInfo.seName.raw", "")))
                ;

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(qb)
//                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        System.out.println("Destination Hits : " + scrollResp.getHits().getTotalHits());

        int count = 0;
        ObjectMapper objectMapper = new ObjectMapper();
        List<QueueVO> queueVOList = new ArrayList<>();

        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
//                    System.out.println(hit.getId());
                    if (++count % 1000 == 0) {
                        System.out.println(count);
//                        Thread.sleep(30000);
                    }

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString());

//                    System.out.println(hit.getId());
                    Patent patent = objectMapper.readValue(hit.getSourceAsString(), Patent.class);
                    if (patent == null)
                        continue;

                    Map<String, Patent> map = new HashMap<>();
                    map.put(patent.getDocumentId(), patent);
                    patent.setOrg(hit.getIndex());
                    String registerNumber = patent.getRegisterNumber();

                    // Pair Data
                    if (hit.getId().matches("^(kr|us|ep|jp).+$")) {
                        List<Patent> patentList = patent.getPairPatent(esConfigs.get(0));
                        if (patentList != null) {
                            for (Patent pairPatent : patentList) {
                                if (patent.merge(pairPatent))
                                    map.put(patent.getDocumentId(), patent);
                            }
                        }
                    }

                    Map<String, List<Applicant>> additionalMap = new HashMap<>();
                    if (hit.getId().matches("^kr.+$")) {
                        String applicationNumber = patent.getApplicationNumber();
//                        System.out.println(applicationNumber);
                        List<Applicant> personList = new ArrayList<>();
//                        List<Applicant> rApplicants = kiprisAPI.rpstApplicantInfoV2(applicationNumber);
//                        if (rApplicants != null)
//                            System.out.println("rApplicants : " + objectMapper.writeValueAsString(rApplicants));

                        List<Applicant> cApplicants = kiprisAPI.patentApplicantInfo(applicationNumber);
                        if (cApplicants != null) {
//                            System.out.println("cApplicants : " + objectMapper.writeValueAsString(cApplicants));
                            additionalMap.put("cApplicants", cApplicants);
                            personList.addAll(cApplicants);
                        }

                        if (registerNumber != null) {
                            List<Applicant> lastRightHolders = kiprisAPI.registrationLastRightHolderInfo(registerNumber);
                            if (lastRightHolders != null) {
//                                System.out.println("lastRightHolders : " + objectMapper.writeValueAsString(lastRightHolders));
                                additionalMap.put("lastRightHolders", lastRightHolders);
                                personList.addAll(lastRightHolders);
                            }
                        }

                        if (!personList.isEmpty())
                            additionalMap.put("personList", personList);

                    }else if (hit.getId().matches("^jp.+$")){
                        List<Applicant> applicantInfo   = representApn.getJPApplicantInfo(conn,patent.getApplicantInfo());
                        if (applicantInfo != null)
                            patent.setApplicantInfo(applicantInfo);
                    }

                    /// data
                    patent.addData(additionalMap);

                    List<Applicant> applicantInfo = patent.getApplicantInfo();
                    if (applicantInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(applicantInfo);
                        ((ObjectNode) jsonNode).put("applicantInfo", arrayNode);
                    }

                    Applicant firstApplicantInfo = patent.getFirstApplicantInfo();
                    if (firstApplicantInfo != null) {
                        ObjectNode objectNode = objectMapper.valueToTree(firstApplicantInfo);
                        ((ObjectNode) jsonNode).put("firstApplicantInfo", objectNode);
                    }

                    List<Applicant> currentApplicantInfo = patent.getCurrentApplicantInfo();
                    if (currentApplicantInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(currentApplicantInfo);
                        ((ObjectNode) jsonNode).put("currentApplicantInfo", arrayNode);
                    }

                    List<Applicant> rightHolderInfo = patent.getRightHolderInfo();
                    if (rightHolderInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(rightHolderInfo);
                        ((ObjectNode) jsonNode).put("rightHolderInfo", arrayNode);
                    }

                    List<Applicant> currentRightHolderInfo = patent.getCurrentRightHolderInfo();
                    if (currentRightHolderInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(currentRightHolderInfo);
                        ((ObjectNode) jsonNode).put("currentRightHolderInfo", arrayNode);
                    } else {
                        if (hit.getId().matches("^jp.+$")){
                            String patentType = patent.getPatentType();
                            if (patentType.matches("^(B|Y)$")) {
                                List<Applicant> applicantList = patent.getApplicantInfo();
                                if (applicantList != null) {
                                    ArrayNode arrayNode = objectMapper.valueToTree(applicantList);
                                    ((ObjectNode) jsonNode).put("rightHolderInfo", arrayNode);
                                    ((ObjectNode) jsonNode).put("currentRightHolderInfo", arrayNode);
                                }
                            }
                        }
                    }

                    Applicant firstRightHolderInfo = patent.getFirstRightHolderInfo();
                    if (firstRightHolderInfo != null) {
                        ObjectNode objectNode = objectMapper.valueToTree(firstRightHolderInfo);
                        ((ObjectNode) jsonNode).put("firstRightHolderInfo", objectNode);
                    }

                    List<Applicant> inventorInfo = patent.getInventorInfo();
                    if (inventorInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(inventorInfo);
                        ((ObjectNode) jsonNode).put("inventorInfo", arrayNode);
                    }

                    List<Applicant> agentInfo = patent.getAgentInfo();
                    if (agentInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(agentInfo);
                        ((ObjectNode) jsonNode).put("agentInfo", arrayNode);
                    }

                    List<Applicant> examinerInfo = patent.getExaminerInfo();
                    if (examinerInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(examinerInfo);
                        ((ObjectNode) jsonNode).put("examinerInfo", arrayNode);
                    }

                    String docdbFamilyId = patent.getDocdbFamilyId();
                    if (docdbFamilyId != null) {
                        ((ObjectNode) jsonNode).put("docdbFamilyId", docdbFamilyId);
                    }

                    String extendedFamilyId = patent.getExtendedFamilyId();
                    if (extendedFamilyId != null) {
                        ((ObjectNode) jsonNode).put("extendedFamilyId", extendedFamilyId);
                    }

                    if (hit.getId().matches("^(jp).+$")) {
                        List<String> summary = patent.getSummary();
                        if (summary != null) {
                            ArrayNode arrayNode = objectMapper.valueToTree(summary);
                            ((ObjectNode) jsonNode).put("summary", arrayNode);
                        }

                        List<String> summaryJP = patent.getSummaryJP();
                        if (summaryJP != null) {
                            ArrayNode arrayNode = objectMapper.valueToTree(summaryJP);
                            ((ObjectNode) jsonNode).put("summaryJP", arrayNode);
                        }
                    }

                    List<Applicant> representApplicantInfo = patent.getRepresentApplicantInfo();
                    if (representApplicantInfo != null) {
                        ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
                        ((ObjectNode) jsonNode).put("representApplicantInfo", arrayNode);
                    } else {
                        if (hit.getId().matches("^(kr|jp).+$")) {
                            representApplicantInfo = representApn.representApplicantInfo(conn,patent);
                            if (representApplicantInfo != null){
                                ArrayNode arrayNode = objectMapper.valueToTree(representApplicantInfo);
                                ((ObjectNode) jsonNode).put("representApplicantInfo", arrayNode);
                            }
                        }
                    }


                    String json = objectMapper.writeValueAsString(jsonNode);
//                    System.out.println(hit.getId() + " : " + json);
                    System.out.println(hit.getId());
                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new IndexRequest(index, type, hit.getId()).source(json));
                }
                scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            System.out.println( "Reindex Count : " + scrollResp.getHits().getTotalHits() + " Finish Count : " + count);

            for (ESConfig esConfig : esConfigs) {
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
