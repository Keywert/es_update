package es.tmp.update;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ESDataUpdate {
    public static void main(String[] args) {
        DataFarmDB dataFarmDB = new DataFarmDB();

        // 중간 서버 인덱스
        // es188 : kipo, epo, uspto, dpma, uspto_past, jpo_past, docdb, paj
        // es189 : aupo,capo,frpo,gbpo,inpo,itpo,rupo,tipo,sipo
        // es18 : jpo, pct
        ESConfig esConfig = new ESConfig("es18");

        String index = "pct";
        String type = "patent";

        String start = "19000101";
        String end = "20091231";

        // 일자
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String updateDate = dtf.format(localDate);

        String[] docIds = {"wo2024094763a1"};

        QueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(docIds))
                ;

        String[] fields = {"documentId", "documentTypeDE", "documentType"};//"description"};

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(qb)
                .setFetchSource(fields, null)
                .setSize(10)
                .execute()
                .actionGet();

        System.out.println(scrollResp.getHits().getTotalHits());

        System.out.println("Destination Hits : " + scrollResp.getHits().getTotalHits());
        try {
            List<QueueVO> queueVOList = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();
            int count = 0;
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count%1000 == 0)
                        System.out.println(count);

                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString());
                    if (jsonNode == null)
                        continue;

                    JsonNode patentCaseMetadata = jsonNode.get("patentCaseMetadata");
                    if (patentCaseMetadata == null)
                        continue;

                    System.out.println(hit.getSourceAsString());


//                    Map<String, Object> jsonMap = new HashMap<>();
//
//                    jsonMap.put("description", descList);
//
////                    jsonMap.put("publicationNumber", publicationNumber);
////                    jsonMap.put("legalStatus", "IW");
////                    jsonMap.put("activeFlag", false);
//
//                    String json = new ObjectMapper().writeValueAsString(jsonMap);
//
//                    System.out.println(hit.getId() + " : " + json);
//                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, hit.getId())
//                            .doc(json.getBytes())
//                            .upsert(json.getBytes()));
////
//                    QueueVO queueVO = new QueueVO();
//                    queueVO.setEsindex(index);
//                    queueVO.setDocumentId(hit.getId());
//                    queueVO.setLevel(0);
//
//                    queueVOList.add(queueVO);
//                    if (queueVOList.size() >= 10) {
//                        dataFarmDB.insertESQueue(queueVOList);
//                        queueVOList.clear();
//                    }

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
