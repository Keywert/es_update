package es.tmp.update.inventionTitle;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InventionTitleCheck {
    public static void main(String[] args) {
        ESConfig esConfig = new ESConfig("es188");

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String[] includes = {
                "inventionTitles"
        };
        String index = "kipo";
        String type = "patent";

        String startDate = "20100101";
        String endDate = "20241231";

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .must(
                        QueryBuilders.nestedQuery(
                                "inventionTitles",
                                QueryBuilders.boolQuery()
                                        .must(QueryBuilders.matchQuery("inventionTitles.lang", "EN"))
                                        .must(QueryBuilders.regexpQuery("inventionTitles.text","[가-힣]+"))
//                                        .mustNot(QueryBuilders.regexpQuery("inventionTitles.text", "[A-z|0-9| ]+"))
                        )
                )
//                .filter(QueryBuilders.idsQuery().addIds("kr20207018326b1"))
                .must(QueryBuilders.rangeQuery("patentDate").from(startDate).to(endDate))
                .must(QueryBuilders.existsQuery("inventionTitles"));


        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setFetchSource(includes, null)
                .setSize(10)
                .execute()
                .actionGet();


        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        int count = 1;
        ObjectMapper mapper = new ObjectMapper();

        ArrayList<Map<String, String>> patents = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString());
//                    System.out.println(hit.getSourceAsString());

                    Map<String, String> patent = new HashMap<>();

                    String apNum = getApplicationNumber(hit.getId());

                    patent.put("documentId", hit.getId());
                    patent.put("applicationNumber", apNum);

                    JsonNode node = jsonNode.get("inventionTitles");

                    for (int i = 0; i < node.size(); i++) {
                        JsonNode lang = node.get(i).get("lang");
                        JsonNode text = node.get(i).get("text");

                        if (lang == null || text == null)
                            continue;

                        if (lang.asText().matches("[K|k][R|r|O|o]"))
                            patent.put("inventionTitle_ko",text.asText());
                        else if (lang.asText().matches("[E|e][N|n]"))
                            patent.put("inventionTitle_en",text.asText());

                    }

//                    System.out.println(objectMapper.writeValueAsString(patent));

                    patents.add(patent);

                    if (patents.size() > 100 ){
                        insertDB(conn,patents);
                        patents.clear();
                    }


                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!patents.isEmpty())
                insertDB(conn,patents);



        } catch (IOException e) {
            e.printStackTrace();
        }


//        if (!docIds.isEmpty())
//            getInventionTitle(conn,docIds);


    }

    private static void insertDB(Connection conn, ArrayList<Map<String, String>> patentVOArrayList) {
        String tableName = "kipo_inventionTitle";

        String insertQuery = "INSERT into " + tableName +
                "(" +
                "documentId," +
                "application_number, inventionTitle_ko, inventionTitle_en " +
                ") " +
                "values(?,?,?,? ) " +
                "ON DUPLICATE KEY UPDATE " +
                "`application_number` = `application_number`";


        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();


            for (Map<String,String> patent : patentVOArrayList) {

                statement.setString(1, patent.get("documentId"));
                statement.setString(2, patent.get("applicationNumber"));
                statement.setString(3, patent.get("inventionTitle_ko"));
                statement.setString(4, patent.get("inventionTitle_en"));

                statement.addBatch();
            }


            // execute the batch
            int[] updateCounts = statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void getInventionTitle(Connection conn, ArrayList<String> docIds) {
        String tableName = "patent.KIPO_ADMIN_REPRESENT_APPLICANT";

        String query = "SELECT * FROM " + tableName +
                " where applicationNumber in (" + "1020190002055" + ") and inventionTitle_en is not null";

        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String title_en = rs.getString("inventionTitle_en");
                System.out.println(title_en);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    private static String getApplicationNumber(String id) {
        if (id.matches("^kr(\\d){11}[a|b](\\d)?$"))
            return "10" + id.substring(2, 13);
        else if ((id.matches("^kr(\\d){11}[y|u](\\d)?$")))
            return "20" + id.substring(2, 13);


        return null;
    }
}
