package es.tmp.update.inventionTitle;

import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import es.tmp.person.kipo.ApplicantVO;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GetInventionTitleEN {
    public static Map<String, Object> xmlpath = new HashMap<String, Object>();

    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String tableName = "kipo_inventionTitle";

        String query = "SELECT * FROM " + tableName + " where inventionTitle_en is null "
//                + " and documentId like 'kr20%' ";
//                 " and documentId = 'kr20100000388u' limit 1";
        ;
        System.out.println(query);

        Statement st = null;

        xmlpath.put("applicantInfo", "//body/items/rpstApplicantV2Info");
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            ArrayList<String> appNums = new ArrayList<>();

            ArrayList<Map<String, String>> arrayList = new ArrayList<>();


            int count = 0;

            while (rs.next()) {
                String applicationNumber = rs.getString("application_number");
//                System.out.println(applicationNumber);

                ArrayList<ApplicantVO> applicantVOList = getRepresentInfo(applicationNumber);

                if (applicantVOList == null)
                    continue;

                Map<String, String> doc = new HashMap<>();

                doc.put("applicationNumber", applicationNumber);
                if (applicantVOList.get(0).getInventionTitle_en() == null)
                    doc.put("inventionTitle_en", "미제공");
                else {
                    doc.put("inventionTitle_en", applicantVOList.get(0).getInventionTitle_en());
                    System.out.println(applicationNumber + " : " + applicantVOList.get(0).getInventionTitle_en());
                }
                arrayList.add(doc);

                ++count;

                if (arrayList.size() > 50) {
                    System.out.println(count);
                    updateDB(conn, tableName, arrayList);
                    arrayList.clear();
                }
            }

            if (!arrayList.isEmpty())
                updateDB(conn, tableName, arrayList);


            System.out.println("Finish Count : " + count);


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String table, ArrayList<Map<String, String>> arrayList) {
        String query = "UPDATE " + table + " SET inventionTitle_en = ? " +
                "WHERE  application_number = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (Map<String, String> doc : arrayList) {
                String ive = doc.get("inventionTitle_en");
                ive = fullWidthToHalfWidth(ive);
                ive = ive.trim();
                if (ive.matches("^(\\?|\\,|\\.)$"))
                    continue;

                preparedStatement.setString(1, ive);
                preparedStatement.setString(2, doc.get("applicationNumber"));

                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String fullWidthToHalfWidth(String fullWidth) {
        StringBuilder halfWidth = new StringBuilder();

        for (char c : fullWidth.toCharArray()) {
            if (c >= 0xFF01 && c <= 0xFF5E) {
                // Convert full-width characters to half-width by subtracting 0xFEE0
                halfWidth.append((char) (c - 0xFEE0));
            } else if (c == 0x3000) {
                // Convert full-width space to half-width space
                halfWidth.append(' ');
            } else {
                // Keep non-full-width characters as is
                halfWidth.append(c);
            }
        }

        return halfWidth.toString();
    }


    private static ArrayList<ApplicantVO> getRepresentInfo(String applicationNumber) {
        String apiUrl = "http://plus.kipris.or.kr/openapi/rest/RpstApplicantService/rpstApplicantInfoV2";
        String accessKey = "XF2qSLslwodSQLAQandtNUlg7WHk=rfF6yq06elR22s=";

        ArrayList<ApplicantVO> applicantVOS = new ArrayList<>();
        ApplicantVO applicantVO = new ApplicantVO();
        String xmlResult = null;

        try {
            apiUrl += "?applicationNumber=" + applicationNumber + "&accessKey=" + accessKey;
            URL url = new URL(apiUrl);

//            Thread.sleep(500);

            //REST API URL을 읽어들여 결과 출력한다

            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");    //인코딩
            BufferedReader reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            String tmpStr = null;

            while ((line = reader.readLine()) != null) {
                tmpStr = line.toString();
                tmpStr = tmpStr.replaceAll(" ", "");

                if (!tmpStr.equals("")) buffer.append(line).append("\r\n");
            }
            reader.close();

            //REST API 결과값
            xmlResult = buffer.toString();

            xmlResult = xmlResult.replaceAll("(\\r|\\n|\\t)", "")
                    .replaceAll(">\\s+<", "><");
//            System.out.println("RESULT => " + xmlResult);


//            ObjectMapper mapper = new ObjectMapper();
//            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.setConfig(xmlMapper.getSerializationConfig().withRootName(""));
//            Map<Object,Object> map = xmlMapper.readValue(xmlResult, Map.class);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setValidating(false);
            builderFactory.setIgnoringComments(true);
            builderFactory.setIgnoringElementContentWhitespace(true);
            builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
            builderFactory.setFeature("http://xml.org/sax/features/validation", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlResult.getBytes()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Iterator<String> iterator = xmlpath.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String expression = xmlpath.get(key).toString();

                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

                int nodeLen = nodeList.getLength();
                if (nodeLen == 0)
                    continue;

                for (int i = 0; i < nodeLen; i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodeList = node.getChildNodes();

                    for (int j = 0, len = childNodeList.getLength(); j < len; j++) {
                        String name = childNodeList.item(j).getNodeName();
                        String value = childNodeList.item(j).getTextContent().trim();

                        if (value.isEmpty())
                            continue;

                        if (name.matches("applicationNumber")) {
                            applicantVO.setApplicationNumber(value);
                        } else if (name.matches("applicationDate")) {
                            applicantVO.setApplicationDate(value);
                        } else if (name.matches("rpstApplicantNumber")) {
                            applicantVO.setApplicantCode(value);
                        } else if (name.matches("rpstApplicantName")) {
                            applicantVO.setApplicantName_ko(value);
                        } else if (name.matches("rpstApplicantNameEng")) {
                            applicantVO.setApplicantName_en(value);
                        } else if (name.matches("titleOfInvention")) {
                            applicantVO.setInventionTitle_ko(value);
                        } else if (name.matches("titleOfInventionEng")) {
//                            System.out.println(value);
                            applicantVO.setInventionTitle_en(value);
                        }
                    }
                }
            }

            String applicantCode = applicantVO.getApplicantCode();

            if (applicantCode == null)
                return null;

            if (!applicantCode.contains("|")) {
                applicantVO.setSeq("1");
                applicantVOS.add(applicantVO);
            } else {
                String[] codes = applicantCode.split("\\|");
                String[] names_ko = applicantVO.getApplicantName_ko().split("\\|");

                String[] names_en = null;
                if (applicantVO.getApplicantName_en() != null)
                    names_en = applicantVO.getApplicantName_en().split("\\|");

                for (int i = 0, len = codes.length; i < len; i++) {
                    ApplicantVO applicant = applicantVO.clone();
                    applicant.setSeq(String.valueOf(i));
                    applicant.setApplicantCode(codes[i]);
                    applicant.setApplicantName_ko(names_ko[i]);

                    if (names_en != null)
                        if (i < names_en.length)
                            applicant.setApplicantName_en(names_en[i]);
                    applicantVOS.add(applicant);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(xmlResult);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        if (applicantVOS.isEmpty())
            return null;

        return applicantVOS;

    }

    public static String xmlTransformer(Node node) {
        StringWriter writer = new StringWriter();

        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(writer));

        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

}
