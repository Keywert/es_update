package es.tmp.update.inventionTitle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;

import org.elasticsearch.action.update.UpdateRequest;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class UpdateKipoIvt {
    public static void main(String[] args) {
        DataFarmDB dataFarmDB = new DataFarmDB();
        int level = 3;

        ESConfig esConfig = new ESConfig("es188");
        String index = "kipo";
        String type = "patent";

        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        String tableName = "patent.kipo_inventionTitle";
        String query = "select * from " + tableName
                + " where insertStatus is null and inventionTitle_en is not null " +
                " and inventionTitle_en not in('미제공', '`','aaa','·', '-', '.','?',',','..','**','*','!','·','K','KK','KKK','...','()','29-1-2, 36-2','AAAAA','','omitted')" +
                " and inventionTitle_en not regexp '^.*[가-힣]+.*$'";

        System.out.println(query);
        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Set<String> set = new HashSet<>();
            List<QueueVO> queueVOList = new ArrayList<>();

            ObjectMapper objectMapper = new ObjectMapper();

            while (rs.next()) {
                String documentId = rs.getString("documentId");
                if (documentId == null)
                    continue;

                String inventionTitle_en = rs.getString("inventionTitle_en");
                if (inventionTitle_en == null || "".equals(inventionTitle_en))
                    continue;

                String inventionTitle_ko = rs.getString("inventionTitle_ko");
                if (inventionTitle_ko == null)
                    continue;

                set.add(documentId);

                List<Map<String, String>> list = new ArrayList<>();
                Map<String, String> inventionTitleKOMap = new HashMap<>();
                inventionTitleKOMap.put("lang", "KR");
                inventionTitleKOMap.put("text", inventionTitle_ko);
                list.add(inventionTitleKOMap);

                Map<String, String> inventionTitleENMap = new HashMap<>();
                inventionTitleENMap.put("lang", "EN");
                inventionTitleENMap.put("text", inventionTitle_en);
                list.add(inventionTitleENMap);

                Map<String, Object> jsonMap = new HashMap<>();
                jsonMap.put("inventionTitles", list);

                String json = objectMapper.writeValueAsString(jsonMap);

                System.out.println(documentId + " : " + json);

                esConfig.bulkProcessor.add((new UpdateRequest(index, type, documentId)
                        .doc(json.getBytes()))
                        .upsert(json.getBytes()));

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex("kipo");
                queueVO.setDocumentId(documentId);
                queueVO.setLevel(level);

                queueVOList.add(queueVO);
                if (queueVOList.size() > 10) {
                    dataFarmDB.insertESQueue(queueVOList);
                    updateDB(conn,tableName,set);
                    queueVOList.clear();
                    set.clear();
                }

            }

//            if (!set.isEmpty())
//                esCheck(esConfig,index,set);
            if (!set.isEmpty())
                updateDB(conn,tableName,set);

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

            esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    private static void updateDB(Connection conn, String tableName, Set<String> docIds) {
        String query = "UPDATE " + tableName + " SET insertStatus = 1" +
                " WHERE documentId = ?";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.clearParameters();

            for (String docId : docIds) {
                preparedStatement.setString(1, docId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
