package es.tmp.updateSample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.config.ESConfig;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RemoveField {
    public static void main(String[] args) {
        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es13"));
        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es43"));
        esConfigs.add(new ESConfig("es227"));
        esConfigs.add(new ESConfig("es225"));
        esConfigs.add(new ESConfig("es226"));
        esConfigs.add(new ESConfig("es232"));

        esConfigs.add(new ESConfig("es189"));

        if (esConfigs.isEmpty())
            return;

        String index = "inpo";
        String type = "patent";

        String[] docIds = {"in450781a1"};
//                String[] docIds = {"kr20010026634y1"};

//        String[] docIds = {"us20220330886a1"};

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(docIds))
//                .filter(QueryBuilders.prefixQuery("documentType","A"))
//                .filter(QueryBuilders.matchQuery("publ-type","Grant"))
//                .filter(QueryBuilders.existsQuery("publicationNumber"))
//                .filter(QueryBuilders.existsQuery("publicationDate"))
//                .mustNot(QueryBuilders.existsQuery("registerDate"))
//                .mustNot(QueryBuilders.existsQuery("registerNumber"))
                ;
        ;
//                .filter(QueryBuilders.existsQuery("litigationInfo"))
//                .filter(QueryBuilders.idsQuery().addIds(docIds))
//                .filter(QueryBuilders.existsQuery("caseIds"));
//                .filter(QueryBuilders.existsQuery("extendFamilyId"))
//                .filter(QueryBuilders.existsQuery("ipcCom"))
//                .filter(QueryBuilders.rangeQuery("extendFamilyEarliestDate").from("19930101").to("20231231"))
        ;

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(200000))
                .setQuery(qb)
                .setSize(1)
                .execute()
                .actionGet();

        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            int count = 0;
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 1000 == 0)
                        System.out.println(count);

//                    System.out.println(hit.getId());
                    JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString().getBytes());
//                    JsonNode publicationNumber = jsonNode.get("publicationNumber");
//                    if (publicationNumber != null) {
//                        ((ObjectNode) jsonNode).put("registerNumber",publicationNumber);
//                        ((ObjectNode) jsonNode).remove("publicationNumber");
//                    }
//
                    JsonNode publicationDate = jsonNode.get("publicationDate");
                    if (publicationDate != null) {
                        ((ObjectNode) jsonNode).put("registerDate",publicationDate);
                        ((ObjectNode) jsonNode).remove("publicationDate");
                    }

//                    JsonNode registerNumber = jsonNode.get("registerNumber");
//                    if (registerNumber != null) {
//                        ((ObjectNode) jsonNode).put("publicationNumber",registerNumber);
//                    }
//
//                    JsonNode registerDate = jsonNode.get("registerDate");
//                    if (registerDate != null) {
//                        ((ObjectNode) jsonNode).put("publicationDate",registerDate);
//                    }


//                    JsonNode suitInfo = jsonNode.get("suitInfo");
//                    if(suitInfo != null)
//                        ((ObjectNode) jsonNode).remove("suitInfo");
//                    else
//                        continue;


                    System.out.println(hit.getIndex() + " : " + hit.getId() + " - " + objectMapper.writeValueAsString(jsonNode));

                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new IndexRequest(hit.getIndex(), type, hit.getId())
                                .source(objectMapper.writeValueAsString(jsonNode)));

                }
                scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            System.out.println("Finish Count : " + count);

            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
