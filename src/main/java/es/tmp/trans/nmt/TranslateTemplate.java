package es.tmp.trans.nmt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TranslateTemplate {
    String url;

    String key;

    String format;

    String profile;

    String target = "ko";

    String source = "auto";

    public String getUrl() {
        return "http://" + this.url + "/translation/text/translate" + "?" + "source=" + source + "&key=" + key + "&target=" + target + "&profile=" + profile;
    }
}
