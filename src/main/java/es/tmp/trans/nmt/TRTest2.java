package es.tmp.trans.nmt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TRTest2 {
    public static void main(String[] args) {
        String apiUrl = "175.209.232.194:8903";    // 각자 상황에 맞는 IP & url 사용
        String key = "5f3d2f94-866f-41b7-9ead-707a800d9192";
        String profile = "569d7428-da02-46de-94e2-1743ffa93679"; // enko

        TranslateTemplate translateTemplate = new TranslateTemplate();
        translateTemplate.setUrl(apiUrl);
        translateTemplate.setKey(key);
        translateTemplate.setSource("en");
        translateTemplate.setTarget("ko");
        translateTemplate.setProfile(profile);
        translateTemplate.setKey(key);

//        String profile = "4fae1a48-f3e3-4c4a-9291-b6f59f601be8";

        String sendData = "This is a book.";

        List<String> list = new ArrayList<>();
        list.add(sendData);
        list.add(sendData);
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("input", sendData);

        JSONObject result = new JSONObject();
        try {
            TranslateCommand command = new TranslateCommand();

            command.setInput(list);

            String input = new ObjectMapper().writeValueAsString(command);
            System.out.println(input);

            String msgMap = sendREST(translateTemplate.getUrl(), input);
            System.out.println(msgMap);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String sendREST(String sendUrl, String jsonValue) throws IllegalStateException {

        String inputLine = null;
        StringBuffer outResult = new StringBuffer();

        try {
            URL url = new URL(sendUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes("UTF-8"));
            os.flush();

            // 리턴된 결과 읽기
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null) {
                outResult.append(inputLine);
            }

            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return outResult.toString();
    }


}
