package es.tmp.trans.ibmt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.systran.trans.app.SysTranslator;
import com.systrangroup.common.api.mt.client.enko.app.TransE2K;
import com.systrangroup.common.api.mt.client.enko.net.TransClientE2K;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.w3c.dom.Node;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class USPTOTranslator_v2 {
    public static String[] fields = {"inventionTitle"};

    public static void main(String[] args) {
        DataFarmDB dataFarmDB = new DataFarmDB();
        SysTranslator translator = new SysTranslator(SysTranslator.TRN_E2K);
        translator.setTransPort(9898);
        translator.setTransHost("112.175.148.39");

        // 키프리스 버전
        TransE2K translator2 = new TransE2K(); // 클라이언트 인스턴스 생성
        translator2.setTransHost("112.175.148.39");    // 번역 서버 IP
        translator2.setTransPort(6800);            // 번역서버 port(디폴트)

        List<ESConfig> esConfigs = new ArrayList<>();
        esConfigs.add(new ESConfig("es188"));
//        esConfigs.add(new ESConfig("es167"));
//        esConfigs.add(new ESConfig("es218"));

        if (esConfigs.isEmpty())
            return;

        // 미국, 유럽 인덱스의 한글 발명의명칭이 없는 문헌을 ibmt로 번역
        String [] indexs = {"uspto","epo"};
        String type = "patent";

        for (String index : indexs) {
            BoolQueryBuilder qb = QueryBuilders.boolQuery()
                    .must(QueryBuilders.existsQuery("inventionTitle"))
                    .mustNot(QueryBuilders.existsQuery("inventionTitleKO"))
                    ;

            SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                    .setTypes(type)
                    .setSearchType(SearchType.SCAN)
                    .setScroll(new TimeValue(2000000))
                    .setQuery(qb)
                    .setFetchSource(fields, null)
                    .setSize(30)
                    .execute()
                    .actionGet();

            System.out.println( index + " Total Hits : " + scrollResp.getHits().getTotalHits());

            int count = 0;
            long start = System.currentTimeMillis();
            int size = 100;

            Map<String, String> map = new HashMap<>();

            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    count++;

                    Object inventionTitle = hit.getSource().get("inventionTitle");

                    if (inventionTitle == null)
                        continue;

                    String ivt = inventionTitle.toString().trim();

                    if ("".equals(ivt))
                        continue;

                    ivt = ivt.replaceAll("\\s+"," ")
                            .replaceAll("(—|-|–|-)","-")
                            .replaceAll("(’|‘|“|”|′|″)","'");


                    map.put(hit.getId(), ivt);

                    if (map.size() > size) {
                        translate(esConfigs, index, translator2, map, dataFarmDB);
//                    translate(esConfig, index, translator, map);
                        map.clear();

                        System.out.println(count + " - " + size + "건 평균시간 : " + ((System.currentTimeMillis() - start) / 1000.0) / size);
                        start = System.currentTimeMillis();
                    }
                }
                scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(2000000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);

            if (!map.isEmpty())
                translate(esConfigs, index, translator2, map, dataFarmDB);
        }

        try {
            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void translate(List<ESConfig> esConfigs, String index, TransE2K translator, Map<String, String> map, DataFarmDB dataFarmDB) {
        ObjectMapper objectMapper = new ObjectMapper();
        Vector vList = new Vector();

        String type = "patent";

        if (map.isEmpty())
            return;

        TreeMap<String, String> tm = new TreeMap<>(map);

        Iterator<String> iterator = tm.keySet().iterator();
        ArrayList<String> ids = new ArrayList<>();
        List<QueueVO> queueVOList = new ArrayList<>();

        try {
            while (iterator.hasNext()) {
                String docId = iterator.next();
                ids.add(docId);
                vList.add(tm.get(docId));
            }

            if (vList.isEmpty())
                return;

            Vector vOutput = translator.transList(vList, TransClientE2K.TYPE_ABS_EK);
//            Vector vOutput = translator.translate(vList);

            if (vOutput.size() == ids.size()) {
                for (int i = 0, len = vOutput.size(); i < len; i++) {
                    Map<String, Object> jsonMap = new HashMap<>();
                    jsonMap.put("inventionTitleKO", vOutput.get(i));

                    String json = objectMapper.writeValueAsString(jsonMap);
                    json = json.replaceAll("(` | ')","'");

                    for (ESConfig esConfig : esConfigs)
                        esConfig.bulkProcessor.add(new UpdateRequest(index, type, ids.get(i))
                                .doc(json.getBytes())
                                .upsert(json.getBytes()));


                    QueueVO queueVO = new QueueVO();
                    queueVO.setEsindex(index);
                    queueVO.setDocumentId(ids.get(i));
                    queueVO.setLevel(4);

                    queueVOList.add(queueVO);
                    if (queueVOList.size() > 5) {
                        dataFarmDB.insertESQueue(queueVOList);
                        queueVOList.clear();
                    }
                }
            }

            if (!queueVOList.isEmpty())
                dataFarmDB.insertESQueue(queueVOList);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return;
    }

    public static String xmlTransformer(Node node) {
        StringWriter writer = new StringWriter();

        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(writer));

        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

}
