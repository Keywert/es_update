package es.tmp.trans.ibmt;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.systran.trans.app.SysTranslator;
import es.config.DBHelper;
import es.config.ESConfig;
import es.module.db.DataFarmDB;
import es.module.db.QueueVO;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class JPOTranslator {

    public static String[] fields = {
            "summaryJP"
            , "descriptionJP"
            , "inventionTitleJP"
            , "claims"
            , "agentInfo"
            , "applicantInfo", "inventorInfo", "citationDoc", "examinerNameJP",
            "overflowJP", "examiner-groupJP", "appeal-examiner-groupJP", "drawingDescriptionJP"
    };

    public static void main(String[] args) {
        DataFarmDB dataFarmDB = new DataFarmDB();
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora2();

        SysTranslator translator = new SysTranslator(SysTranslator.TRN_J2K);
        translator.setTransPort(9898);
//        translator.setTransHost("112.175.148.61");
        translator.setTransHost("112.175.148.39");

        List<ESConfig> esConfigs = new ArrayList<>();

//        esConfigs.add(new ESConfig("es51"));
//        esConfigs.add(new ESConfig("es205"));
        esConfigs.add(new ESConfig("es18"));
//        esConfigs.add(new ESConfig("es13"));


        if (esConfigs.isEmpty())
            return;

        String index = "jpo";
        String type = "patent";

        String startDate = "20230101";
        String endDate = "20231231";

        String [] ids= {"jp2023000459u"};

        BoolQueryBuilder qb = QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("description", "本"))
                .filter(QueryBuilders.rangeQuery("firstInsertDate").from(startDate).to(endDate))
                .mustNot(QueryBuilders.idsQuery().addIds(ids))
//                .must(
//                        QueryBuilders.boolQuery()
//                                .should(QueryBuilders.rangeQuery("publicationDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("registerDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("openDate").from(startDate).to(endDate)))
                ;


//                .must(QueryBuilders.termQuery("summary.raw", "해"))
//                .must(
//                        QueryBuilders.boolQuery()
//                                .should(QueryBuilders.rangeQuery("publicationDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("registerDate").from(startDate).to(endDate))
//                                .should(QueryBuilders.rangeQuery("openDate").from(startDate).to(endDate)))
                ;


//        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                            .should(QueryBuilders.termQuery("description.raw","첩반주"));

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .must(QueryBuilders.wildcardQuery("inventionTitleJP","該"))
                .must(QueryBuilders.wildcardQuery("inventionTitle.raw","해*"));

        BoolQueryBuilder bqb1 = QueryBuilders.boolQuery()
                .must(QueryBuilders.wildcardQuery("inventionTitleJP","該"))
                .must(QueryBuilders.wildcardQuery("inventionTitle.raw","해*"));


        String [] str = {
                "jp2023000459u"
        };

        IdsQueryBuilder idsQb = QueryBuilders.idsQuery().addIds(str);

        TermsQueryBuilder tqb = QueryBuilders.termsQuery("applicantInfo.name","セイコーエプソン株式会社");

        WildcardQueryBuilder wqb = QueryBuilders.wildcardQuery("description","*の*");

//        BoolQueryBuilder qb = QueryBuilders.boolQuery()
////                .must(QueryBuilders.termQuery("claims.claimText.raw", "해당"))
//                .must(QueryBuilders.wildcardQuery("description","*方*"))
////                .mustNot(QueryBuilders.idsQuery().addIds(str))
//                ;

//        System.out.println(qb);

        SearchResponse scrollResp = esConfigs.get(0).client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(2000000))
                .setQuery(idsQb)
                .setFetchSource(fields, null)
                .setSize(1)
                .execute()
                .actionGet();

//        System.out.println(esConfig.client.prepareSearch(index)
//                .setTypes(type)
//                .setSearchType(SearchType.SCAN)
//                .setScroll(new TimeValue(2000000))
//                .setQuery(qb)
//                .setFetchSource(fields, null)
//                .setSize(5));


        System.out.println("Total Hits : " + scrollResp.getHits().getTotalHits());

        int count = 0;
        List<QueueVO> queueVOList = new ArrayList<>();

        do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                System.out.println(hit.getId());
                count++;

//                System.out.println(hit.getSourceAsString());
                String translated_text = translate(hit,translator);
                System.out.println(translated_text);

                for (ESConfig esConfig : esConfigs)
                    esConfig.bulkProcessor.add(new UpdateRequest(index, type, hit.getId())
                            .doc(translated_text.getBytes())
                            .upsert(translated_text.getBytes()));

                QueueVO queueVO = new QueueVO();
                queueVO.setEsindex(index);
                queueVO.setDocumentId(hit.getId());
                queueVO.setLevel(0);
                queueVOList.add(queueVO);

            }
            scrollResp = esConfigs.get(0).client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(2000000)).execute().actionGet();
        } while (scrollResp.getHits().getHits().length != 0);

        if (!queueVOList.isEmpty())
            dataFarmDB.insertESQueue(queueVOList);

        System.out.println("Total Count : " + count);

        try {
            for (ESConfig esConfig : esConfigs)
                esConfig.bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static String translate(SearchHit hit, SysTranslator translator) {
        ObjectMapper objectMapper = new ObjectMapper();
        Vector vList = new Vector();
        Map<String, String> jsonMap = new HashMap<>();

        String json = "";

//        System.out.println(hit.getSourceAsString());
        try {
            JsonNode jsonNode = objectMapper.readTree(hit.getSourceAsString());

            for (String key : fields) {
                if (jsonNode.get(key) == null) {
                    continue;
                }
                JsonNode node = jsonNode.get(key);
                vList.clear();

                if (node.size() == 0) {
                    String text = node.asText();
                    if (text.isEmpty())
                        text = " ";

                    vList.add(text);
                    Vector vOutput = translator.translate(vList);
                    ((ObjectNode) jsonNode).put(key.replaceFirst("JP", ""), vOutput.get(0).toString());
                    continue;
                }


                if (key.matches("agentInfo|applicantInfo|inventorInfo|citationDoc|claims")) {
                    Vector vOutput = null;
                    for (int i = 0; i < node.size(); i++) {
                        Iterator<String> fieldNames = node.get(i).fieldNames();
                        vList.clear();
                        ArrayList<String> array = new ArrayList<String>();
                        while (fieldNames.hasNext()) {
                            String fieldName = fieldNames.next();
                            if (fieldName.matches("nameJP|addressJP|text|claimTextJP" +
                                    "|independentClaimTextJP|departmentJP|orgNameJP" +
                                    "|lastNameJP|firstNameJP|middleNameJP")) {
                                String text = node.get(i).get(fieldName).asText();
                                if (text.isEmpty())
                                    text = " ";
                                vList.add(text);
                                array.add(fieldName);
                            }
                        }
                        vOutput = translator.translate(vList);

                        for (int j = 0; j < array.size(); j++) {
                            ((ObjectNode) node.get(i)).put(array.get(j).replaceFirst("JP", ""), vOutput.get(j).toString());
                        }
                    }

                    ((ObjectNode) jsonNode).put(key, node);
                    continue;
                }

                if (key.matches("summaryJP")) {
                    Vector vOutput = null;
                    String text = node.get(0).asText();
                    vList.clear();

                    InputSource is = new InputSource(new StringReader(text));
                    Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
                    Node childNode = document.getFirstChild();
                    NodeList nodeList = childNode.getChildNodes();

                    List<String> translatedList = new ArrayList<>();

                    for (int i = 0, len = nodeList.getLength(); i < len; i++) {
                        String t = nodeList.item(i).getTextContent().trim();
                        if (t.isEmpty()){
                            translatedList.add(t);
                            continue;
                        }

                        String[] strs = t.split("。|｡");

                        for (String str : strs)
                            vList.add( str + "。 ");

                        vOutput = translator.translate(vList);

                        String temp = "";
                        for (int j = 0, outlen = vOutput.size(); j < outlen; j++) {
                            temp +=  vOutput.get(j).toString() + " ";
                        }

                        translatedList.add(temp);

                        vList.clear();
                        vOutput.clear();
                    }

                    for (int i = 0, len = nodeList.getLength(); i < len; i++) {
                        childNode.getChildNodes().item(i).setTextContent(translatedList.get(i));
                    }

                    ArrayNode arrayNode = objectMapper.createArrayNode();
                    arrayNode.add(xmlTransformer(childNode).replaceAll(">\\s+</EMI>","/>"));

                    ((ObjectNode) jsonNode).put(key.replaceFirst("JP", ""), arrayNode);

                    continue;
                }

                if (key.matches("descriptionJP")) {
                    Vector vOutput = new Vector();
                    String origin = node.get(0).asText();
//                    builder.parse(new ByteArrayInputStream(text.getBytes()));

//                    System.out.println(origin);

                    String text = "";

                    String[] strs = origin.split("。|｡");

                    int count = 0;

                    for (String str : strs) {
//                        System.out.println(++count + " : " + str + "。 ");
                        vList.add( str + "。 ");

                        if (vList.size() == 30) {
                            vOutput = translator.translate(vList);
//
                            for (int i = 0, len = vOutput.size(); i < len; i++) {
                                text +=  vOutput.get(i).toString() + " ";
                            }
                            vList.clear();
                        }
                    }

                    vOutput = translator.translate(vList);
//
                    for (int i = 0, len = vOutput.size(); i < len; i++) {
                        text +=  vOutput.get(i).toString() + " ";
                    }

                    text = text.replaceAll(">\\s+</EMI>", "/>")
                            .replaceAll("&lt;", "<")
                            .replaceAll("&gt;", ">")
                            .replaceAll("\\.$","");
                    ArrayNode arrayNode = objectMapper.createArrayNode();
                    arrayNode.add(text);

                    ((ObjectNode) jsonNode).put(key.replaceFirst("JP", ""), arrayNode);
                    continue;
                }

            }

            if (jsonNode == null)
                return null;

            json = objectMapper.writeValueAsString(jsonNode);

//            System.out.println(objectMapper.writeValueAsString(jsonNode));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }



        return json;
    }

    public static String xmlTransformer(Node node) {
        StringWriter writer = new StringWriter();

        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(writer));

        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

}
