package es.tmp.trans.ibmt;

import com.systran.trans.app.SysTranslator;

import java.util.ArrayList;

public class TranslateTest {
    public static void main(String[] args) {
        SysTranslator translator = new SysTranslator(SysTranslator.TRN_J2K);
//        SysTranslator translator = new SysTranslator(SysTranslator.TRN_E2K);
        translator.setTransPort(9898);
//        translator.setTransHost("211.62.58.220");
//        translator.setTransHost("211.62.58.221");
        translator.setTransHost("112.175.148.38");
//        translator.setTransHost("112.175.148.39");

//        SysTranslator translator = new SysTranslator(SysTranslator.TRN_E2K);
//        translator.setTransPort(9898);
//        translator.setTransHost("112.175.148.39");


        String str1 = "前記複数本の第1の電極指、前記複数本の第1のダミー電極指、前記複数本の第2の電極指及び前記複数本の第2のダミー電極指において、前記第1の端子に接続されているバスバー側の電極指ギャップ部においては、電極指の先端に、電極指側縁から弾性波伝搬方向に突出している凸部が設けられておらず、前記第2の端子に接続されているバスバー側の電極指ギャップ部においては、電極指の先端には上記凸部が設けられている、弾性波素子。";
//
//        TransE2K translator2 = new TransE2K(); // 클라이언트 인스턴스 생성
//        translator2.setTransHost("211.62.58.220");    // 번역 서버 IP
//        translator2.setTransPort(6800);            // 번역서버 port(디폴트)
//
//        Vector<String> data = new Vector<>();
//        data.add(str1);
//        data = translator2.transList(data, TransClientE2K.TYPE_ABS_EK);
//        data.forEach(System.out::println);

        System.out.println(translator.translate(str1));


        String text = "1. A method of reducing motor dysfunction associated with Parkinson's disease in a patient with Parkinson's disease comprising: " +
                "a) enzymatically dispersing umbilical cord matrix to provide enzymatically dispersed umbilical cord matrix cells;\n"+
                "b) culturing the enzymatically dispersed umbilical cord matrix cells in the presence of epidermal growth factor (EGF) and platelet derived growth factor (PDGF) to proliferate the umbilical cord matrix cells;\n"+
                "c) culturing the enzymatically dispersed umbilical cord matrix cells on a substrate surface and removing non-adherent cells;\n"+
                "d) culturing adherent cells from (c) to select for a population of umbilical cord matrix cells that comprise cells that are negative for CD34 and CD45, positive for telomerase activity, can be expanded in vitro, and maintained in culture through repeated passages;\n"+
                "e) isolating the adherent cells from d);\n"+
                "f) introducing the isolated adherent cells from e) into the substantia nigra region of the midbrain striatum in a patient with Parkinson's disease, and\n"+
                "g) allowing the cells to integrate into the substantia nigra region of the midbrain striatum of the patient whereby motor dysfunction associated with Parkinson's disease in the patient is reduced.\n";


        String text1 = "1. A method of reducing motor dysfunction associated with Parkinson's disease in a patient with Parkinson's disease comprising :a) enzymatically dispersing umbilical cord matrix to provide enzymatically dispersed umbilical cord matrix cells; b) culturing the enzymatically dispersed umbilical cord matrix cells in the presence of epidermal growth factor (EGF) and platelet derived growth factor (PDGF) to proliferate the umbilical cord matrix cells;c) culturing the enzymatically dispersed umbilical cord matrix cells on a substrate surface and removing non-adherent cells;d) culturing adherent cells from (c) to select for a population of umbilical cord matrix cells that comprise cells that are negative for CD34 and CD45, positive for telomerase activity, can be expanded in vitro, and maintained in culture through repeated passages; e) isolating the adherent cells from d);f) introducing the isolated adherent cells from e) into the substantia nigra region of the midbrain striatum in a patient with Parkinson's disease, andg) allowing the cells to integrate into the substantia nigra region of the midbrain striatum of the patient whereby motor dysfunction associated with Parkinson's disease in the patient is reduced. A method of reducing motor dysfunction associated with Parkinson's disease in a patient with Parkinson's disease comprising :a) enzymatically dispersing umbilical cord matrix to provide enzymatically dispersed umbilical cord matrix cells;b) culturing the enzymatically dispersed umbilical cord matrix cells in the presence of epidermal growth factor (EGF) and platelet derived growth factor (PDGF) to proliferate the umbilical cord matrix cells;c) culturing the enzymatically dispersed umbilical cord matrix cells on a substrate surface and removing non-adherent cells;d) culturing adherent cells from (c) to select for a population of umbilical cord matrix cells that comprise cells that are negative for CD34 and CD45, positive for telomerase activity, can be expanded in vitro, and maintained in culture through repeated passages;e) isolating the adherent cells from d);f) introducing the isolated adherent cells from e) into the substantia nigra region of the midbrain striatum in a patient with Parkinson's disease, andg) allowing the cells to integrate into the substantia nigra region of the midbrain striatum of the patient whereby motor dysfunction associated with Parkinson's disease in the patient is reduced.";


        String text2 = "The amount of the organic acid having one to five carbon atoms for use in crystallization of 1-aminocyclopropanecarboxylic acid is generally from about 1 to about 50 parts by weight, and preferably from about 5 to about 10 parts by weight to 1 part by weight of 1-aminocyclopropanecarboxylic acid contained in the crude 1-aminocyclopropanecarboxylic acid, while it varies depending typically on the type and amount of impurities contained in the crude 1-aminocyclopropanecarboxylic acid and the amount of the poor solvent.";

        String text5 = " ";

        String text3 = "該哺乳類がヒト,イヌ,ネコ,ウサギ及びげっ歯類よりなる群より選ばれるものである,請求項1のアトピー性皮膚炎治療剤。";

        String text4 = "This is '1-aminocyclopropanecarboxylic.";

        String text56= "清水 初志";


        String test = "Hyaluronan synthase genes and expression thereof in Bacillus hosts";

//        System.out.println(test.toUpperCase());
//
//        System.out.println(translator.translate(test));

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("複数のフォルダのいずれかに格納されるファイルであって、前記複数のフォルダを、それぞれが異なる次元を示すN個(Nは3以上の整数)の階層を含むツリー形状で表わした場合に、前記ファイルが格納されるフォルダが位置する階層に至るまでの各階層をそれぞれ表わすN個の格納場所情報と、各格納場所情報に対応して、前記ファイルが格納されるフォルダに至るまでの各階層に位置するフォルダを示す値とが付加されたファイルの格納場所を表示させる処理をコンピュータに実行させるプログラムであって、");
        arrayList.add("前記ファイルに付加されているN個の格納場所情報の内、縦軸および横軸として、各々1つの格納場所情報の指定を受付ける軸指定ステップと、");
        arrayList.add("前記N個の格納場所情報の内、前記ファイルを特定できる格納場所情報の指定を受付ける固定値指定ステップと、");
        arrayList.add("前記固定値指定ステップで指定を受付けた格納場所情報で特定される各ファイルの格納場所を示すN個の格納場所情報の内、前記縦軸および前記横軸として指定を受付けた格納場所情報に対応する前記値を取得する取得ステップと、");
        arrayList.add("前記取得された格納場所情報に対応する前記値に基づいて、前記固定値指定ステップで指定を受付けた格納場所情報で特定される各ファイルを示す情報を、前記縦軸および前記横軸にて構成される2次元座標上に表示する表示ステップとを前記コンピュータに実行させる、文書管理プログラム。");

//        for (String str : arrayList)
//            System.out.println(translator.translate(str));

//        String str = "画像を生成する画像生成システムであって、 ぼかし処理の対象となるぼかし対象画像の情報を記憶するぼかし対象画像情報記憶部と、前記ぼかし対象画像に対して複数回のぼかし処理を行うぼかし処理部とを含み、前記ぼかし処理部が、 前記複数回のぼかし処理のうちの第Kのぼかし処理で得られたぼかし画像のピクセル値を増加させる変換処理を行い、変換処理が施されたぼかし画像に対して次の第K+1のぼかし処理を行うことを特徴とする画像生成システム。";

//        String str = "<claim id=\\\"clm-00001-zh\\\" num=\\\"1\\\"><claim-text>1.一种智能英语口语练习亭系统装置，包括英语口语练习亭本体，其特征在于：还包括<br/>智能触摸点播系统，所述英语口语练习亭本体包括设备主体(31)、功能主体(32)、设备附件<br/>(33)、声音采集装置(34)、声音录制装置(35)、声音播放装置(36)、自动调节音量装置(37)、<br/>口语练习播放内容展示装置(38)、口语练习录制播放装置(39)、语音识别与分析装置<br/>(310)、口语自动评分及智能纠错装置(311)和口语练习练习人机交互装置(312)，所述设备<br/>主体(31)包括钣金框架(11)、空调系统(12)、灯光系统(13)、门控系统(14)、紫外线杀菌系<br/>统(15)和主机系统(16)，所述功能主体(32)包括点播屏(41)、显示屏(42)、触控机(43)、立<br/>体音响(44)、门控系统(45)、紫外线杀菌系统(46)和安防系统(47)，所述设备附件(33)包括<br/>两个麦克风(21)、两个头戴式耳机(22)、空调遥控器(23)、两个欧式座椅(24)、两个吧台<br/>(25)、安防摄像头(26)、紫外线杀菌装置(27)和门控系统(28)，所述智能触摸点播系统包括<br/>点播设备、点播内容服务器和云系统存储服务器，所述智能触摸点播系统包括点播设备、点<br/>播内容服务器和云系统存储服务器依次进行连接，所述点播设备包括信息接收模块(51)和<br/>请求发送模块(52)，所述点播内容服务器依次连接信息接收模块(51)和请求发送模块<br/>(52)，所述点播内容服务器包括英语口语练习内容分类管理单元(61)、英语口语练习内容<br/>索索单元(62)、英语口语练习声音录制单元(63)、英语口语练习录音播放单元(64)、英语口<br/>语练习音量控制单元(65)、英语口语练习自动评分及智能纠错单元(66)、机构管理单元<br/>(67)和用户分享单元(68)，所述云系统存储服务器包括存储加密单元(71)、DRN保护单元<br/>(72)和分布式数据库(73)。</claim-text></claim>";
        String str = "<abstract date-changed=\\\"20190419\\\" format=\\\"original\\\" id=\\\"abstr_xml_chi\\\" lang=\\\"chi\\\"><p num=\\\"0\\\">本发明公开了一种智能英语口语练习亭系统装置，包括英语口语练习亭本体和智能触摸点播系统，本发明通过打通线上与线下的距离，走近每个英语口语练习者的身边，能够大幅提升用户的英语口语练习体验，将客厅文化转变为一场未来最具热点的全民文化活动，解决人们在现有环境不满足的情况下，能够在有限的环境中大声英语口语练习，提供一种专业级的英语口语练习声音录制，并且能够根据用户英语口语练习的作品智能设置英语口语练习情景图，自动调节背景音乐音量和人声的比例，达到很好的英语口语练习效果，并且采用独有的文件存储加密单元和DRM保护单元，确保点播内容版权安全，即使将文件下载到本地，不经认证也无法使用。</p></abstract>";
//        System.out.println(str);
//        System.out.println(translator.translate(str));

//        光開始剤
//                その分子中に
//        その分子中に
//                それ自身に架橋しかつ架橋した
//        静水

//        str = "</p><p num=\\\"0080\\\">［実施例５］<br/>  次に、実施例５として、成形体プレス法を用いて電極埋設部材１を製造した別の例について説明する。 <br/>  接続部材が直径８ｍｍ、厚み０．５ｍｍのモリブデンのバルク体とすること、及び緩衝部材をＡｌＮ原料粉とモリブデン（Ｍｏ）の粉末を体積比７０％：３０％で混合した後に成形し、直径１２ｍｍ、厚み１．５ｍｍの円板に片面から直径８ｍｍ、深さ０．５ｍｍのザグリ加工を施した凹部状部材を準備することとしたこと以外は実施例１と同じ工程とした。 </p><p num=\\\"0081\\\">［比較例］<br/>  次に、上記実施例に対する比較例について説明する。 <br/>  本比較例においては、上述の実施例１において、緩衝部材を接続部材の周囲に配置せず、緩衝部材を含まない従来の製法による電極埋設部材を作製した。 </p><p num=\\\"0082\\\">(評価)<br/>  実施例１〜５及び比較例で作製した電極埋設部材を用いて、プロセス温度が６００℃である半導体製造プロセスに使用した。 </p><p num=\\\"0083\\\">  使用開始後３か月経過後に端子部の断面をＳＥＭ観察したところ、実施例１〜５ともクラックは確認されなかったが、比較例においては、接続部材の縁部から電極埋設部材の表面に向かうクラックの進展が確認された。 </p><p num=\\\"0084\\\">Ａ．実施形態：<br/>Ａ−１．静電チャック１０００の構成：<br/>  図８は、本実施形態における静電チャック１０００の外観構成を概略的に示す斜視図であり、図９は、本実施形態における静電チャック１０００のＸＺ断面構成を概略的に示す説明図である。 図８及び図９には、方向を特定するための互いに直交するＸＹＺ軸が示されている。 本明細書では、便宜的に、Ｚ軸正方向を上方向といい、Ｚ軸負方向を下方向というものとするが、静電チャック１０００は実際にはそのような向きとは異なる向きで設置されてもよい。 </p><p num=\\\"0085\\\">  静電チャック１０００は、対象物（例えばウエハ１５００）を静電引力により吸着して保持する装置であり、例えば半導体製造装置の真空チャンバー内でウエハ１５００を固定するために使用される。 静電チャック１０００は、所定の配列方向（本実施形態では上下方向（Ｚ軸方向））に並べて配置されたセラミックス板１０１０およびベース板１０２０を備える。 セラミックス板１０１０とベース板１０２０とは、セラミックス板１０１０の下面（以下、「セラミックス側接着面Ｓ２」という）とベース板１０２０の上面（以下、「ベース側接着面Ｓ３」という）とが上記配列方向に対向するように配置されている。 静電チャック１０００は、さらに、セラミックス板１０１０のセラミックス側接着面Ｓ２とベース板１０２０のベース側接着面Ｓ３との間に配置された接着層１０３０を備える。 </p><p num=\\\"0086\\\">  セラミックス板１０１０は、例えば円形平面の板状部材であり、セラミックスにより形成されている。 セラミックス板１０１０の直径は、例えば５０ｍｍ〜５００ｍｍ程度（通常は２００ｍｍ〜３５０ｍｍ程度）であり、セラミックス板１０１０の厚さは、例えば２ｍｍ〜１０ｍｍ程度である。 </p><p num=\\\"0087\\\">  セラミックス板１０１０の形成材料としては、種々のセラミックスが用いられ得るが、強度や耐摩耗性、耐プラズマ性、後述するベース板１０２０の形成材料との関係等の観点から、例えば、酸化アルミニウム（アルミナ、Ａｌ<sub>２</sub>Ｏ<sub>３</sub>）または窒化アルミニウム（ＡｌＮ）を主成分とするセラミックスが用いられることが好ましい。 なお、ここでいう主成分とは、含有割合（重量割合）の最も多い成分を意味する。 </p><p num=\\\"0088\\\">  セラミックス板１０１０の内部には、導電性材料（例えば、タングステンやモリブデン等）により形成された一対の内部電極１０４０が設けられている。 一対の内部電極１０４０に電源（図示せず）から電圧が印加されると、静電引力が発生し、この静電引力によってウエハ１５００がセラミックス板１０１０の上面（以下、「吸着面Ｓ１」という）に吸着固定される。 </p><p num=\\\"0089\\\">  また、セラミックス板１０１０の内部には、導電性材料（例えば、タングステンやモリブデン等）により形成された抵抗発熱体で構成されたヒータ１０５０が設けられている。 ヒータ１０５０に電源（図示せず）から電圧が印加されると、ヒータ１０５０が発熱することによってセラミックス板１０１０が温められ、セラミックス板１０１０の吸着面Ｓ１に保持されたウエハ１５００が温められる。 これにより、ウエハ１５００の温度制御が実現される。 なお、ヒータ１０５０は、セラミックス板１０１０の吸着面Ｓ１をできるだけ満遍なく温めるため、例えばＺ方向視で略同心円状に配置されている。 </p><p num=\\\"0090\\\">  ベース板１０２０は、例えばセラミックス板１０１０と同径の、または、セラミックス板１０１０より径が大きい円形平面の板状部材であり、セラミックスとアルミニウム合金とから構成された複合材料により形成されている。 ベース板１０２０の直径は、例えば２２０ｍｍ〜５５０ｍｍ程度（通常は２２０ｍｍ〜３５０ｍｍ程度）であり、ベース板１０２０の厚さは、例えば２０ｍｍ〜４０ｍｍ程度である。 </p><p num=\\\"0091\\\">  ベース板１０２０の形成材料としては、金属や種々の複合材料が用いられ得る。 金属としては、Ａｌ（アルミニウム）やＴｉ（チタン）が用いられることが好ましい。 複合材料としては、炭化ケイ素（ＳｉＣ）を主成分とする多孔質セラミックスに、アルミニウムを主成分とするアルミニウム合金を溶融して加圧浸透させた複合材料が用いられることが好ましい。 複合材料に含まれるアルミニウム合金は、Ｓｉ（ケイ素）やＭｇ（マグネシウム）を含んでいてもよいし、性質等に影響の無い範囲でその他の元素を含んでいてもよい。 </p><p num=\\\"0092\\\">  ベース板１０２０の内部には冷媒流路１０２１が形成されている。";
//        System.out.println(str);
//        System.out.println(translator.translate(str));
//
//        str = "护壁支柱组合件";
//        System.out.println(str);
//        System.out.println(translator.translate(str));
//
//        str = "便座構造、及び当該便座構造を備えるトイレ装置";
//        System.out.println(str);
//        System.out.println(translator.translate(str));


//        System.out.println();
//        String str2 = "According to one aspect of the present invention, the ASIC @b_st@22@b_en@ includes an electrochromic (EC) drive circuit @b_st@50@b_en@, generally shown in @figref_st@FIG.2@figref_en@, for driving one or more electrochromic elements, such as the series connected inside electrochromic element (IEC) @b_st@14@b_en@ and outside electrochromic element (OEC) @b_st@30@b_en@.";
//        System.out.println(translator.translate(str2));
//        System.out.println();

//        String str3 = " @-13-@  @-6-@  @-16-@ BACKGROUND @-4-@  @-19-@ Unmanned vehicles may provide different operations, some of which may be autonomous and support various applications. For example, an unmanned aerial vehicle (UAV) may be used as an autonomous delivery vehicle associated with an electronic marketplace. The electronic marketplace may offer items and selectable delivery methods. Based on a selected delivery method, the UAV may be deployed to deliver a purchased item. The UAV may be configured to autonomously perform various delivery-related operations, such as autonomously flying between a source and a destination.";
//        System.out.println(str3);
//        System.out.println(translator.translate(str3));
//
//        String str4 = "CROSS REFERENCE OF RELATED APPLICATION";
//        System.out.println(translator.translate(str4));
//
//        String str5 = "For example, an unmanned aerial vehicle (UAV) may be used as an autonomous delivery vehicle associated with an electronic marketplace.";
//        System.out.println(str5);
//        System.out.println(translator.translate(str5));

    }
}


