package es.tmp.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import es.config.DBHelper;

public class PED {
    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_aurora();
        PreparedStatement preparedStmt = null;

        List<String> queries = getQuery();

        if (queries == null)
            return;

        try {
            for (String query : queries) {
                query = query.replaceAll("\\s+"," ");

                System.out.println(query);

                preparedStmt = conn.prepareStatement(query);
                preparedStmt.clearParameters();
                preparedStmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getQuery() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String date = localDate.toString().replaceAll("-","");
        System.out.println(date);

        List<String> queries = new ArrayList<>();
        //# step 1 - 연차료 처리
        //## Patent Expired Due to NonPayment of Maintenance Fees
        String query01 = "insert into USPTO_ADMIN.USPTO_GRANT_MAINTENANCE_FEE_DATA_EXP (application_number, maintenance_fee_event_code, maintenance_fee_event_entry_date,status) " +
                " select application_number, maintenance_fee_event_code,maintenance_fee_event_entry_date, 1 " +
                " from USPTO_ADMIN.USPTO_GRANT_MAINTENANCE_FEE_EVENTS where maintenance_fee_event_code = 'EXP.' and status is null " +
                " order by application_number, maintenance_fee_event_entry_date " +
                " on duplicate key update maintenance_fee_event_entry_date = values(maintenance_fee_event_entry_date), status = 1";
        queries.add(query01);

        String query02 = "update USPTO_ADMIN.USPTO_GRANT_MAINTENANCE_FEE_DATA_EXP A inner join USPTO_ADMIN.USPTO_GRANT_MAINTENANCE_FEE_EVENTS B on A.application_number = B.application_number " +
                " set A.status = null where A.maintenance_fee_event_entry_date < B.maintenance_fee_event_entry_date and B.maintenance_fee_event_code like 'M%'";
        queries.add(query02);

        String query03 = "update USPTO_ADMIN.USPTO_GRANT_MAINTENANCE_FEE_EVENTS " +
                " set status = 1 where maintenance_fee_event_code = 'EXP.' and status is null ";
        queries.add(query03);

        //# step 2 - 우선 출원일 업데이트
        String query04_01 = "insert ignore into USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN_FIRST_DATE " +
                " select * from USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN where status is null and con_filing_date != '' and con_filing_date not like '0%' " +
                " order by application_number, con_filing_date";
        queries.add(query04_01);

        String query04 = "insert ignore into USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN_FIRST " +
                " select * from USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN where status is null and con_application_number not like 'PCT%' and con_filing_date != '' " +
                " and  continuation_type != 'PRO' and con_filing_date not like '0%' and child_document_status_code is null " +
                " order by application_number, con_filing_date";
        queries.add(query04);

        String query05 = "update USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN " +
                " set status= 1 where status is null";
        queries.add(query05);

        String query06_01 = "update USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA A inner join USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN_FIRST_DATE B on A.application_number = B.application_number " +
                " set A.parent_filing_date = B.con_filing_date, A.parent_application_number = B.con_application_number " +
                " where A.parent_filing_date is null";
        queries.add(query06_01);

        String query06 = "update USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA A inner join USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN_FIRST B on A.application_number = B.application_number " +
                " set A.con_application_date = B.con_filing_date, A.con_application_number = B.con_application_number " +
                " where A.con_application_date is null";
        queries.add(query06);

        //# step 3 - TD Setting
        String query07 = "update `USPTO_ADMIN`.`USPTO_PATENT_APPLICATION_DATA` as A inner join `USPTO_ADMIN`.`USPTO_PATENT_TERM_ADJ` B on A.application_number =B.application_number " +
                " set A.td_status = 1 where A.td_status is null and B.pta_event_code = 'DIST'";
        queries.add(query07);

        String query08 = "create table `USPTO_ADMIN`.`USPTO_PATENT_EXPIRATION_DATE_new` like `USPTO_ADMIN`.`USPTO_PATENT_EXPIRATION_DATE`";
        queries.add(query08);

//        # step 4 - 기본 존속기간 만료일 계산
        String query09 = "INSERT INTO `USPTO_ADMIN`.`USPTO_PATENT_EXPIRATION_DATE_new` " +
                " select " +
                "    APPLICATION.application_number, " +
                "    CASE " +
                "        WHEN (FEE.status = 1 AND FEE.maintenance_fee_event_entry_date IS NOT NULL) " +
                "            THEN FEE.maintenance_fee_event_entry_date " +
                "        WHEN ( APPLICATION.patent_number like 'D%' AND CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '2015-05-12' < CAST(APPLICATION.filing_date AS char(10))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 15 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (APPLICATION.patent_number like 'D%' AND CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '2015-05-13' > CAST(APPLICATION.filing_date AS char(10))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 14 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '1978-06-08' > CAST(APPLICATION.filing_date AS char(10)) ) " +
                "            THEN date_add(date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) , INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (CAST(APPLICATION.con_application_date AS char(10)) IS NOT NULL  AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10)) AND (date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) > date_add(CAST(con_application_date AS char(10)),INTERVAL + 20 YEAR ))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) , INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (CAST(APPLICATION.con_application_date AS char(10)) IS NOT NULL AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10))  AND ( date_add(CAST(APPLICATION.con_application_date AS char(10)),INTERVAL + 20 YEAR ) > date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.con_application_date AS char(10)),INTERVAL + 20 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL  AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10)) AND (date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) > date_add(CAST(APPLICATION.filing_date AS char(10)),INTERVAL + 20 YEAR ))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) , INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10))  AND ( date_add(CAST(APPLICATION.filing_date AS char(10)),INTERVAL + 20 YEAR ) > date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.filing_date AS char(10)),INTERVAL + 20 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        WHEN (CAST(APPLICATION.con_application_date AS char(10)) IS NOT NULL AND '1995-06-08' < CAST(APPLICATION.filing_date AS char(10))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.con_application_date AS char(10)),INTERVAL + 20 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        ELSE " +
                "            date_add(date_add(APPLICATION.filing_date,INTERVAL + 20 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        END  expiration_date, " +
                "    CASE " +
                "        WHEN (FEE.status = 1) " +
                "            THEN 'FEE' " +
                "        WHEN (APPLICATION.td_status = 1) " +
                "            THEN 'TD' " +
                "        WHEN (PTE_SUMMARY.extension_total_quantity IS NOT NULL) " +
                "            THEN 'PTE' " +
                "        WHEN (SUMMARY.patent_term_adjustment IS NOT NULL) " +
                "            THEN 'PTA' " +
                "        ELSE " +
                "            '' " +
                "        END  pta_type, " +
                "    CASE WHEN (FEE.status = 1) THEN 1 ELSE 0 END | " +
                "    CASE WHEN (SUMMARY.patent_term_adjustment IS NOT NULL) THEN 2 ELSE 0 END | " +
                "    CASE WHEN (PTE_SUMMARY.extension_total_quantity IS NOT NULL) THEN 4 ELSE 0 END | " +
                "    CASE WHEN (APPLICATION.td_status = 1) THEN 8 ELSE 0 END  as code, " +
                "    CASE " +
                "        WHEN ( APPLICATION.patent_number like 'D%' AND CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '2015-05-12' < CAST(APPLICATION.filing_date AS char(10))) " +
                "            THEN date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 15 YEAR ) " +
                "        WHEN (APPLICATION.patent_number like 'D%' AND CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '2015-05-13' > CAST(APPLICATION.filing_date AS char(10))) " +
                "            THEN date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 14 YEAR ) " +
                "        WHEN (CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '1978-06-08' > CAST(APPLICATION.filing_date AS char(10)) ) " +
                "            THEN date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) " +
                "        WHEN (CAST(APPLICATION.con_application_date AS char(10)) IS NOT NULL  AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10)) AND (date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) > date_add(CAST(con_application_date AS char(10)),INTERVAL + 20 YEAR ))) " +
                "            THEN date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) " +
                "        WHEN (CAST(APPLICATION.con_application_date AS char(10)) IS NOT NULL AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10))  AND ( date_add(CAST(APPLICATION.con_application_date AS char(10)),INTERVAL + 20 YEAR ) > date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ))) " +
                "            THEN date_add(CAST(APPLICATION.con_application_date AS char(10)),INTERVAL + 20 YEAR ) " +
                "        WHEN (CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL  AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10)) AND (date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) > date_add(CAST(APPLICATION.filing_date AS char(10)),INTERVAL + 20 YEAR ))) " +
                "            THEN date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ) " +
                "        WHEN (CAST(APPLICATION.filing_date AS char(10)) IS NOT NULL AND '1995-06-08' > CAST(APPLICATION.filing_date AS char(10))  AND ( date_add(CAST(APPLICATION.filing_date AS char(10)),INTERVAL + 20 YEAR ) > date_add(CAST(APPLICATION.patent_issue_date AS char(10)),INTERVAL + 17 YEAR ))) " +
                "            THEN date_add(CAST(APPLICATION.filing_date AS char(10)),INTERVAL + 20 YEAR ) " +
                "        WHEN (CAST(APPLICATION.con_application_date AS char(10)) IS NOT NULL AND '1995-06-08' < CAST(APPLICATION.filing_date AS char(10))) " +
                "            THEN date_add(date_add(CAST(APPLICATION.con_application_date AS char(10)),INTERVAL + 20 YEAR ), INTERVAL +  IFNULL(SUMMARY.patent_term_adjustment,0) + IFNULL(PTE_SUMMARY.extension_total_quantity,0) DAY ) " +
                "        ELSE " +
                "            date_add(APPLICATION.filing_date,INTERVAL + 20 YEAR ) " +
                "        END  expiration_date_origin " +
                " from `USPTO_ADMIN`.`USPTO_PATENT_APPLICATION_DATA` as APPLICATION " +
                "         LEFT JOIN `USPTO_ADMIN`.`USPTO_PATENT_PTA_SUMMARY` SUMMARY " +
                "                   ON (APPLICATION.application_number = SUMMARY.application_number) " +
                "         LEFT JOIN `USPTO_ADMIN`.`USPTO_PATENT_PTE_SUMMARY` PTE_SUMMARY " +
                "                   ON (APPLICATION.application_number = PTE_SUMMARY.application_number) " +
                "         LEFT JOIN `USPTO_ADMIN`.`USPTO_GRANT_MAINTENANCE_FEE_DATA_EXP` FEE " +
                "                   ON (APPLICATION.application_number = FEE.application_number) " +
                " where APPLICATION.filing_date != '' and APPLICATION.patent_issue_date != '' " +
                " ON DUPLICATE KEY " +
                "    UPDATE " +
                "        expiration_date=values(expiration_date), " +
                "        pta_type=values(pta_type), " +
                "        code=code|values(code)";
        queries.add(query09);

//# step 5 - TD 존속기간 만료일 계산
//         (1) PTA, TD가 둘다 있는 경우 : 해당 특허의 존속시간에 PTA를 더 했을때 나오는 가장 긴 날짜와 관련문헌들의 PTA를 더한 존속기간 중 가장 짧은 날짜를 비교하여
//        -> 해당 특허의 존속기간이 긴 경우에는 관련문헌의 가장 짧은 날짜를 만료일로 하고,
//        -> 해당 특허의 존속기간이 짧은 경우에는, 해당 특허의 PTA 연장된 존속기간을 만료일로 함
//        (2) TD가 있는 경우에, 선행특허가 연차료 불납으로 조기 소멸되면, 후행특허도 같이 해당 소멸일로 소멸로 변경되어아 함

        String query10 = "update USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE_new C inner join " +
                "    (select A.application_number,B.expiration_date ,B.expiration_date_origin from USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA A inner join " +
                "                                                               USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE_new B on A.con_application_number = B.application_number " +
                "    where A.td_status = 1 and B.code not in (1,3,5,7,9,11,13,15)) D " +
                "    on C.application_number =D.application_number " +
                " set C.expiration_date = CASE WHEN (D.expiration_date < C.expiration_date) THEN D.expiration_date ELSE C.expiration_date END " +
                " where C.expiration_date != D.expiration_date ";
        queries.add(query10);

        String query11 = "insert ignore into USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE_new (application_number, code) " +
                " select A.application_number, 2 from  `USPTO_ADMIN`.`USPTO_PATENT_PTA_SUMMARY` A left outer join USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE B " +
                "     on A.application_number =B.application_number where B.application_number is null and A.application_number != '' ";
        queries.add(query11);

        String query12 = "insert ignore into USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE_new (application_number, code) " +
                " select A.application_number, 4 from  `USPTO_ADMIN`.`USPTO_PATENT_PTE_SUMMARY` A left outer join USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE B " +
                "     on A.application_number =B.application_number where B.application_number is null and A.application_number != '' ";
        queries.add(query12);

        // 테이블 교체
        String query13 = "rename table USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE to USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE_" + date +
                " ,USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE_new to USPTO_ADMIN.USPTO_PATENT_EXPIRATION_DATE ";
        queries.add(query13);

        // PCT 처리
        String query14 = "update USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA " +
                        " set status = 1 " +
                         " where status is null and application_number like 'PCT%' ";
        queries.add(query14);

        // Related Doc Type 세팅
        String query15 = "update USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN A inner join " +
                " USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN B on A.con_application_number = B.application_number and A.application_number = B.con_application_number " +
                " set A.continuation_type = B.continuation_type " +
                " where A.continuation_type is null and B.continuation_type is not null";
        queries.add(query15);


//        // 공개 데이터 처리
//        String query16 = "update USPTO_ADMIN.USPTO_PATENT_APPLICATION_DATA " +
//                " set status = 1 " +
//                " where status is null and appl_status_code <> '' and " +
//                "        appl_status_code in ('1' ,'100' ,'11' ,'12' ,'145' ,'148' ,'157' ,'158' ,'159' ,'16' ,'17' ,'18' ,'19' ,'195' ,'197','20' ,'200' " +
//                "        ,'201' ,'218' ,'25' ,'251' ,'252' ,'254' ,'256' ,'258' ,'259' ,'260' ,'264' ,'265','266' ,'27' ,'271' ,'275' " +
//                "        ,'28' ,'280' ,'29' ,'30' ,'31' ,'310' ,'311' ,'32' ,'33' ,'37' ,'38','39' ,'551' ,'552' ,'554' ,'555' ,'556' " +
//                "        ,'558' ,'559' ,'560' ,'566' ,'568' ,'569' ,'65' ,'66' ,'97') ";
//        queries.add(query16);

        return queries;
    }
}
