package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import java.util.List;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DartIpInfo {
    // 유형
    @JsonDeserialize(using = ArrayDeserializer.class)
    List<String> firstActionTypes;

    // 존재 여부
    Boolean hasLitigation;

    // 패밀리 정보 - docdb family
    String docdbFamilyId;

    // 패밀리 정보 - inpadoc
    String inpadocFamilyId;

    // URL 정보
    String dartsUrl;

}
