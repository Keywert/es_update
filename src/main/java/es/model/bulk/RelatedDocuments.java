package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RelatedDocuments {
    String date;
    String country;
    String documentNumber;
    String documentType;
    String patentType;
    String kind;
    String type;
    String documentId;
    String parentStatus;

    String typeName;
    public Boolean isAliveLink = false;

    public String getTypeName() {
        if (StringUtils.isBlank(type)) return null;

        // TODO: enum화
        switch (type) {
            case "division":
                typeName = "분할출원";
                break;
            case "continuation":
                typeName = "계속출원";
                break;
            case "continuationInPart":
                typeName = "일부계속출원";
                break;
            case "continuing-reissue":
                typeName = "재발행의 계속출원";
                break;
            case "reissue":
                typeName = "재발행";
                break;
            case "divisional reissue":
                typeName = "분할-재발행";
                break;
            case "reexamination":
            case "reexamination reissue merger":
                typeName = "재심사";
                break;
            case "substitution":
                typeName = "대체출원";
                break;
            case "provisional application":
                typeName = "임시출원";
                break;
            case "utility model basis":
            case "correction":
                typeName = "미출원";
                break;
            case "addition":
            default:
                typeName = null;
                break;
        }

        return typeName;
    }
}