package es.model.bulk;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FamilyVO {

    @JsonProperty("countryCode")
    private String country;
    @JsonGetter("country")
    public String getCountry() {
        return country;
    }
    @JsonSetter("countryCode")
    public void setCountryCode(String country) {
        this.country = country;
    }
    @JsonSetter("country")
    public void setCountry(String country) {
        this.country = country;
    }

    private String applicationNumber;
    public  String extendedFamilyId;

    private String appln_nr_epodoc;
    @JsonGetter("appln_nr_epodoc")
    public String getAppln_nr_epodoc() {
        return appln_nr_epodoc;
    }
    @JsonSetter("appln_nr_epodoc")
    public void setAppln_nr_epodoc(String appln_nr_epodoc) {
        this.appln_nr_epodoc = appln_nr_epodoc;
    }
    @JsonSetter("epoApplicationNumber")
    public void setEpoApplicationNumber(String appln_nr_epodoc) {
        this.appln_nr_epodoc = appln_nr_epodoc;
    }


    public String publishingORG;
    private String epoPublicationNumber;

    private String appln_nr;

    public String getCountryCode() {
        if (this.publishingORG != null)
            return this.getPublishingORG();

        return countryCode;
    }

    public String countryCode;

    public String getDocumentType() {
        if (this.documentTypeUS != null)
            return documentTypeUS;

        if (this.documentTypeDE != null)
            return documentTypeDE;

        if (this.documentTypeCN != null)
            return documentTypeCN;

        return documentType;
    }

    public String documentTypeUS;
    public String documentTypeDE;
    public String documentTypeCN;

    private String documentType;
    private String docdbFamilyCount;
    private String reg_phase;
    private String docdbFamilyCitationCount;

    public String patentNumber;

    public String getPublicationNumber() {
        if (this.patentNumber != null)
            return patentNumber;
        return publicationNumber;
    }

    public String patentDate;
//    public String getPublicationNumber() {
//        if (this.patentNumber != null)
//            return patentNumber;
//        return publicationNumber;
//    }

    public String publicationNumber;

    private String docdbFamilyId;
    @JsonGetter("docdbFamilyId")
    public String getDocdbFamilyId() {
        return docdbFamilyId;
    }
    @JsonSetter("docdbFamilyId")
    public void setDocdbFamilyId(String docdbFamilyId) {
        this.docdbFamilyId = docdbFamilyId;
    }

    public String getFamilyId() {
        if(this.familyId == null)
            return this.getDocdbFamilyId();
        return familyId;
    }

    public String getPatentId() {
        if (this.patentId == null)
            return this.getDocumentId();
        return patentId;
    }

    public String documentId;
    public String patentId;

    public String getPatentType() {
        if (this.patentType == null)
            return this.getDocumentType();
        return patentType;
    }

    public String patentType;



    public String familyId;


    private String originalApplicationNumber;
    @JsonGetter("originalApplicationNumber")
    public String getOriginalApplicationNumber() {
        return originalApplicationNumber;
    }
    @JsonSetter("originalApplicationNumber")
    public void setOriginalApplicationNumber(String originalApplicationNumber) {
        this.originalApplicationNumber = originalApplicationNumber;
    }
    @JsonSetter("appln_nr_original")
    public void setAppln_nr_original(String originalApplicationNumber) {
        this.originalApplicationNumber = originalApplicationNumber;
    }


    public String getFamilyNumber() {
        String applicationNumber = this.getPublicationNumber();
        String documentType = this.getDocumentType();
        if (documentType == null)
            documentType = "A";
        documentType = documentType.replaceAll("[W]", "A");

        String country = this.getCountryCode();
        if (country == null)
            country = this.getCountry();

        String familyNumber = "";

        if (applicationNumber == null || applicationNumber.isEmpty()) {
            applicationNumber = this.getApplicationNumber();

            if (applicationNumber == null || applicationNumber.isEmpty())
                applicationNumber = this.getApplicationNumber();

            familyNumber = country + applicationNumber + documentType;

        } else if (country.matches("US")) {
            applicationNumber = this.getPublicationNumber();

            if (applicationNumber == null || applicationNumber.isEmpty())
                applicationNumber = this.getApplicationNumber();

            familyNumber = country + applicationNumber + documentType;

        } else {
            applicationNumber = applicationNumber.replaceAll("[A-z]$", "");
            familyNumber = country + applicationNumber + documentType;
        }

        return familyNumber;
    }

    public String familyNumber;


    private String inpadocFamilyId;

}