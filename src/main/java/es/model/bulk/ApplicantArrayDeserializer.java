package es.model.bulk;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import es.model.Applicant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ApplicantArrayDeserializer extends JsonDeserializer {
    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.readValueAsTree();
        List<Applicant> arrayList = new ArrayList<>();
        if (node instanceof ArrayNode) {
            Applicant[] objects = new ObjectMapper().readValue(node.toString(), Applicant[].class);
            arrayList.addAll(Arrays.asList(objects));
        } else if (node instanceof JsonNode) {
            Applicant object = new ObjectMapper().readValue(node.toString(), Applicant.class);
            arrayList.add(object);
        }

        return arrayList;
    }
}
