package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

public class SuitVO {
    public String caseID;                  // Case ID
    public String caseNo;                  // Case Number
    public String caseFlag;                  // Case Number
    public String dateOfFiled;                  //

    public String courtName;                  // courtName
    public String issueDate;                  // issueDate
    public String casestatus;                  // casestatus  Closed

    public List<SuitPatent> patentsInfo;
    public List<SuitParty> party;


}
