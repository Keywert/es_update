package es.model.bulk;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper=false)
public class SuitPatent {
    public String patentNumber;
    public List<String> inventorName;
    public String dateOfPublished;
    public List<String> ipcCode;
    public String applicationDate;
    public List<String> priorityDate;

    public String getPatentNumber() {
        return patentNumber.replaceAll("^(US)","");
    }

}
