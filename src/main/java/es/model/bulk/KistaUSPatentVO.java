package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.Applicant;
import es.model.v1.PriorityClaim;
import es.module.utility.CountryCode;
import es.module.utility.NumberUtil_Priority;
import es.module.utility.PriorityUtil;
import lombok.Data;

import java.util.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KistaUSPatentVO {
    public String citationDocumentId;                  // 문서 ID
    public String documentId;                  // 문서 ID
    public String publishingORG;               // 발간청
    public String countryCode;                 // - docdb 국가
    public String openNumber;                  // 공개번호
    public String publicationNumber;           // (openNumber가 없을 시)공개번호
    public String registerNumber;              // 등록번호
    public String registerDate;                // 등록일자
    public String openDate;                   // 공개일자(To)
    public String publicationDate;            // 공개일자(To)
    public String documentType;                // 문헌종류
    public String documentTypeJP;              // 문헌종류 일본만
    public String documentTypeUS;              // 문헌종류 일본만


    public String applicationNumber;           // 번호
    public String applicationDate;            // 출원일

    public SuitInfoVO suitInfo;

    /**
     * 추가 (한국 일본) 등록 상태 에서만 적용되는 번호 및 일자 개념
     **/
    public String announcementNumber;          // 공고 번호
    public String announcementDate;            // 공고 일자

    public String inventionTitleJP;            // 발명의 일본문헌
    public String inventionTitleCN;            // 발명의 중국문헌
    public String inventionTitleKO;            // 발명의 중국문헌 / 번역

    private String internationalApplicationNumber;      // 국제 출원번호
    private String internationalApplicationDate;        // 국제 출원일자
    private String internationalPublicationNumber;        // 국제 공개번호
    private String internationalPublicationDate;          // 국제 공개일자

    private String inventionTitle;                        // 발명의 명칭

    private List<CitationDoc> citationDoc;      // 인용문헌 (number, country)

    public Map<Integer, String> getCitation() {
        if (this.citationDoc != null) {
            ArrayList<String> patcitList = new ArrayList<>();
            ArrayList<String> npatcitList = new ArrayList<>();
            for (CitationDoc citationVO : this.citationDoc) {
                String citedType = citationVO.getCitedType();

                if (citedType.matches("P")) {
                    String country = citationVO.getCountry();
                    String docNum = citationVO.getDocumentNumber();
                    String type = citationVO.getKind();
                    String citedPhase = citationVO.getCitedPhase();

                    if (docNum != null)
                        docNum = docNum.replaceAll("-", "");

                    if (type == null)
                        type = "";

                    if (docNum == null)
                        continue;

                    if (country == null)
                        country = "";

                    String number = country + docNum + type;
                    patcitList.add(number);

                } else {
                    String text = citationVO.getText();

                    if (text != null && !text.trim().isEmpty()) {
                        if (!text.trim().matches("^[0-9]{1,2}$"))
                            npatcitList.add(text.trim());
                    }
                }
            }
            Map<Integer, String> result = new HashMap<>();
            result.put(1, String.valueOf(patcitList.size()));
            result.put(2, (patcitList.size() == 0) ? null : String.join("||", patcitList));
            result.put(3, String.valueOf(npatcitList.size()));
            result.put(4, (npatcitList.size() == 0) ? null : String.join("||", npatcitList));
            return result;
        }
        return null;
    }


    public String mainIpc;                     // IPC (Main)
    public String mainUpc;                     // UPC (Main)
    public String mainCpc;                     // CPC (Main)
    public List<String> subIpc;                // IPC (Sub)
    public List<String> subCpc;                // CPC (Sub)
    public List<String> subUpc;                // CPC (Sub)

    public String mainFi;                      // mainFI(JP)
    //    public List<String> fTerm;                 // fTerm(JP)
    public List<String> themeCode;             // 테마코드(JP)

    public List<Applicant> applicantInfo;          // 출원인
    public String applicantNames;
    public String applicantCount;
    public String applicantCountry;
    public String applicantCountryCount;
    public String applicantLocalStatus;
    public String applicantAddress;


    public List<Applicant> currentApplicantInfo;   // 최근 출원인
    public List<Applicant> rightHolderInfo;

    public List<Applicant> inventorInfo;         // 미국 출원인 - 권리자
    public String inventorNamesCountry;
    public String inventorNames;
    public String inventorCount;
    public String inventorCountry;
    public String inventorCountryCount;

    public List<Applicant> currentRightHolderInfo;    // 현재 권리인 ver revision 1.3.7
    public String currentRightHolderNames;
    public String currentRightHolderCount;
    public String currentRightHolderCountry;
    public String currentRightHolderCountryCount;
    public String currentRightHolderLocalStatus;
    public String currentRightHolderAddress;


    public List<Applicant> assigneeInfo;         // 미국 출원인 - 권리자

    public String claimCount;                  // 청구항 갯수

    public Boolean examinationRequestStatus;   // 심사청구 상태
    public String examinationRequestDate;      // 심사청구 일자


    public List<Claim> claims;                 // 청구범위 (independentClaimText, claimText)


    public List<PriorityClaim> priorityClaims;             // 우선권 관련정보
    public String priorityClaimCountry;
    public String priorityClaimNumber;
    public String priorityClaimDate;
    public String priorityClaimMasterNumber;


    public String docdbFamilyId;
    public String inpadocFamilyId;

    public String wipo_5_category;
    public String wipo_35_category;

    public GradeInfo gradeInfo;

    public String legalStatus;

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> summary;                 // 청구범위 (independentClaimText, claimText)

    public String getSummaryText() {
        if (summary == null)
            return null;
        String text = summary.get(0);

        return text.replace(System.getProperty("line.separator"), "")
                .replaceAll(">\\s*<", "><")
                .replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z|\\-]*=[^>]*)?(\\s)*(/)?>", "")
                .replaceAll("\\s+", " ")
                .replaceAll("^(내용 없음|내용 없음|내용없음|내용없음|요약없슴|요약 없음|요약없음|\\.|요약없음|없음)\\.?$", "")
                .trim();
    }

    public String getClaimFirstText() {
        if (claims == null)
            return null;

        String text = "";
        for (Claim claim : this.getClaims()) {
            if (claim.claimState.matches("I|U")) {
                text += " " + claim.getIndependentClaimText();
                if (text == null)
                    text += " " + claim.getClaimText();
            }
        }

        return text.replace(System.getProperty("line.separator"), "")
                .replaceAll("null", "")
                .replaceAll("</?claim-text>", "")
                .replaceAll(">\\s*<", "><")
                .replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z|\\-]*=[^>]*)?(\\s)*(/)?>", "")
                .replaceAll("\\s+", " ")
                .trim();
    }

    public String getClaimText() {
        if (claims == null)
            return null;

        String text = "";
        for (Claim claim : this.getClaims()) {
            String claimText = claim.getIndependentClaimText();
            if (claimText == null)
                claimText = claim.getClaimText();

            if (claimText != null)
                text += " " + claimText;
        }

        return text.replace(System.getProperty("line.separator"), "")
                .replaceAll("null", "")
                .replaceAll("</?claim-text>", "")
                .replaceAll(">\\s*<", "><")
                .replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z|\\-]*=[^>]*)?(\\s)*(/)?>", "")
                .replaceAll("\\s+", " ")
                .trim();
    }


    public String getApplicationNumber() {
        if (applicationNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+b(\\d)*$"))
            return "10" + applicationNumber.replaceAll("-", "");

        return applicationNumber;
    }

    public String getRegisterNumber() {
        if (registerNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+b(\\d)*$"))
            return "10" + registerNumber.replaceAll("-", "");

        return registerNumber;
    }

    public String getOpenNumber() {
        if (openNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+b(\\d)*$"))
            return "10" + openNumber.replaceAll("-", "");

        return openNumber;
    }

    public void setPriorityClaimInfos() {
        if (priorityClaims == null)
            return;

        ArrayList<String> numberList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        ArrayList<String> dateList = new ArrayList<>();
        ArrayList<String> masterNumberList = new ArrayList<>();

        for (PriorityClaim priorityClaim : priorityClaims) {
            String num = priorityClaim.getPriorityApplicationNumber();
            String country = priorityClaim.getPriorityApplicationCountry();
            String date = priorityClaim.getPriorityApplicationDate();

            if (date != null)
                date = date.replaceAll("[^0-9]", "");

            country = PriorityUtil.countryNormalize(country);
            String countryCode = CountryCode.countryCode.get(country);

            if (countryCode != null)
                country = countryCode;

            if (country != null)
                country = country.replaceAll("[^A-z]", "");

            if (num != null)
                num = num.replaceAll("[\\s|,]", "");
            String number = num;
            if (country != null)
                number = NumberUtil_Priority.getNumber(country, num, "AN").replaceAll("\\.", "");
//
//            if (country.matches("JP")) {
//                System.out.println(country + " : " + num + " -  " + number);
//            }

            priorityClaim.setPriorityApplicationNumber(number);
            priorityClaim.setPriorityApplicationCountry(country);
            numberList.add(number);
            countryList.add(country);
            dateList.add(date);

            masterNumberList.add("[" + country + "]" + number + "(" + date + ")");

        }

//        System.out.println(countryList);


        this.setPriorityClaimNumber(String.join("||", numberList));
        this.setPriorityClaimCountry(String.join("||", countryList));
        this.setPriorityClaimDate(String.join("||", dateList));
        this.setPriorityClaimMasterNumber(String.join("||", masterNumberList));


    }


    public void setApplicantInfos() {
        List<Applicant> applicants = null;

        if (assigneeInfo != null) {
            applicants = assigneeInfo;
        } else if (applicantInfo != null) {
            applicants = applicantInfo;
        } else if (rightHolderInfo != null) {
            applicants = rightHolderInfo;
        }

        if (applicants == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();
        ArrayList<String> localStatus = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();


        for (Applicant applicant : applicants) {
            String name = applicant.getSeName();
            String country = applicant.getCountry();

            if (name == null)
                name = applicant.getSrName();

            if (name == null)
                continue;

            nameList.add(name);

            if (country != null) {
//                System.out.println(country);
                countryList.add(country);
                countrySet.add(country);
            }

            if (applicant.getCountry() == null)
                localStatus.add("내국인");
            else if (applicant.getCountry().matches("US"))
                localStatus.add("내국인");
            else
                localStatus.add("외국인");

            if (applicant.getAddress() != null)
                address.add(applicant.getAddress().split("\\s")[0].replaceAll(",", ""));

        }

        if (nameList == null)
            return;

        this.setApplicantNames(String.join("||", nameList));
        this.setApplicantCount(String.valueOf(applicants.size()));
        this.setApplicantCountry(String.join("||", countryList));
        this.setApplicantCountryCount(String.valueOf(countrySet.size()));
        this.setApplicantLocalStatus(String.join("||", localStatus));
        this.setApplicantAddress(String.join("||", address));
    }

    public void setCurrentRightHolderInfos() {
        List<Applicant> rightHolders = null;

        if (currentRightHolderInfo != null) {
            rightHolders = currentRightHolderInfo;
        } else if (rightHolderInfo != null) {
            rightHolders = rightHolderInfo;
        } else if (applicantInfo != null) {
            rightHolders = applicantInfo;
        } else if (assigneeInfo != null) {
            rightHolders = assigneeInfo;
        }

        if (rightHolders == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();
        ArrayList<String> localStatus = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();


        for (Applicant applicant : rightHolders) {
            String name = applicant.getName();
            String country = applicant.getCountry();

            if (name == null)
                name = applicant.getOrgName();

            if (name == null)
                continue;

            nameList.add(name);

            if (country != null) {
                countryList.add(country);
                countrySet.add(country);
            } else
                countryList.add("null");

            if (applicant.getCountry() == null)
                localStatus.add("내국인");
            else if (applicant.getCountry().matches("US"))
                localStatus.add("내국인");
            else
                localStatus.add("외국인");

            if (applicant.getAddress() != null && !"".equals(applicant.getAddress()))
                address.add(applicant.getAddress().split("\\s")[0].replaceAll(",", ""));

        }

        if (nameList.isEmpty())
            return;

        this.setCurrentRightHolderNames(String.join("||", nameList));
        this.setCurrentRightHolderCount(String.valueOf(rightHolders.size()));
        this.setCurrentRightHolderCountry(String.join("||", countryList));
        this.setCurrentRightHolderCountryCount(String.valueOf(countrySet.size()));
        this.setCurrentRightHolderLocalStatus(String.join("||", localStatus));

        if (!address.isEmpty())
            this.setCurrentRightHolderAddress(String.join("||", address));

    }

    public void setInventorInfos() {
        List<Applicant> inventors = null;

        if (inventorInfo != null) {
            inventors = inventorInfo;
        } else if (applicantInfo != null) {
            inventors = applicantInfo;
        }

        if (inventors == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> nameCountryList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();


        for (Applicant applicant : inventors) {
            String name = applicant.getName();
            String country = applicant.getCountry();

            if (name == null)
                name = applicant.getOrgName();

            if (name != null) {
                nameList.add(name);

                if (country != null) {
                    nameCountryList.add(name + "[" + country + "]");
                    countryList.add(country);
                    countrySet.add(country);
                } else {
                    countryList.add("null");
                    nameCountryList.add(name);

                }
            }
        }

        if (nameList == null)
            return;

        this.setInventorNamesCountry(String.join("||", nameCountryList));
        this.setInventorNames(String.join("||", nameList));
        this.setInventorCount(String.valueOf(inventors.size()));
        this.setInventorCountry(String.join("||", countryList));
        this.setInventorCountryCount(String.valueOf(countrySet.size()));
    }


    public String getIpcAll() {
        ArrayList<String> ipcs = new ArrayList<>();

        if (mainIpc != null)
            ipcs.add(mainIpc);

        if (subIpc != null)
            for (String ipc : subIpc)
                ipcs.add(ipc);

        if (ipcs.size() == 0)
            return null;

        return String.join("||", ipcs);
    }

    public String getCpcAll() {
        ArrayList<String> cpcs = new ArrayList<>();

        if (mainCpc != null)
            cpcs.add(mainIpc);

        if (subCpc != null)
            for (String ipc : subCpc)
                cpcs.add(ipc);

        if (cpcs.size() == 0)
            return null;

        return String.join("||", cpcs);
    }

    public String getPriorityClaimNumber() {
        ArrayList<String> numbers = new ArrayList<>();

        if (priorityClaims == null)
            return null;


        for (PriorityClaim priorityClaim : priorityClaims)
            numbers.add(priorityClaim.getPriorityApplicationNumber());

        return String.join("||", numbers);

    }
}

