package es.model.bulk;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;

public class ObjectDeserializer extends JsonDeserializer {
    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.readValueAsTree();
        String str = null;
        if (node instanceof ArrayNode) {
            String[] object = new ObjectMapper().readValue(node.toString(), String[].class);
            str = object[0];
        } else if (node instanceof JsonNode) {
            String object = new ObjectMapper().readValue(node.toString(), String.class);
            str = object;
        }

        return str;
    }
}
