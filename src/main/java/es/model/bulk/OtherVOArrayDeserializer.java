package es.model.bulk;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OtherVOArrayDeserializer extends JsonDeserializer {
    @Override
    public Object deserialize(JsonParser p, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = p.readValueAsTree();
        List<OtherVO> arrayList = new ArrayList<>();
        if (node instanceof ArrayNode) {
            OtherVO[] objects = new ObjectMapper().readValue(node.toString(), OtherVO[].class);
            arrayList.addAll(Arrays.asList(objects));
        } else if (node instanceof JsonNode) {
            OtherVO other = new ObjectMapper().readValue(node.toString(), OtherVO.class);
            arrayList.add(other);
        }

        if (arrayList.isEmpty())
            return null;

        return arrayList;
    }
}
