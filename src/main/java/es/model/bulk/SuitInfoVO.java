package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SuitInfoVO {
    ArrayList<String> caseIds;
    ArrayList<String> npeNames;
    String lastSuitDate;
    boolean npeFlag;
    boolean suitFlag;
    int suitCount;

}
