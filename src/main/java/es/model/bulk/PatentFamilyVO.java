package es.model.bulk;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.model.enums.LegalStatusEPEnum;
import es.model.enums.LegalStatusEnum;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentFamilyVO {
    public String country;
    public String applicationDate;

    public String announcementDate;
    public String announcementNumber;

    public String registerNumber;
    public String registerDate;

    public String publicationNumber;
    public String publicationDate;

    public String patentType;
    public String documentType;
    public String documentTypeUS;
    public String docdbDocumentId;


    public String docdbFamilyId;
    public String extendedFamilyId;
    public String inpadocFamilyId;
    public String patentId;

    public String getPatentNumber() {
        if (announcementNumber != null && !"".equals(announcementNumber))
            patentNumber = announcementNumber;
        else if (registerNumber != null && !"".equals(registerNumber))
            patentNumber = registerNumber;
        else if (publicationNumber != null && !"".equals(publicationNumber))
            patentNumber = publicationNumber;
        else
            return null;

        if (patentNumber.matches("^(10|20)[0-9]{7}$"))
            return patentNumber.substring(2);

        if (patentNumber.matches("^(10|20)(19|20)[0-9]{9}$"))
            return patentNumber.substring(2,6) + "-" + patentNumber.substring(6);

        return patentNumber;
    }


    public String patentNumber;

    public String getPatentDate() {
        if (announcementDate != null && !"".equals(announcementDate))
            patentDate = announcementDate;
        else if (registerDate != null && !"".equals(registerDate))
            patentDate = registerDate;
        else if (publicationDate != null && !"".equals(publicationDate))
            patentDate = publicationDate;
        else
            return null;

        return patentDate;
    }

    public String patentDate;

    public String internationalApplicationDate;
    public String internationalApplicationNumber;
    public String internationalPublicationDate;
    public String internationalPublicationNumber;

    public String getApplicationDate() {
        return internationalApplicationDate;
    }

    public String getLegalStatus() {
        if (legalStatus == null)
            return null;

        if (patentId == null) {
            LegalStatusEnum legalStatusEnum = LegalStatusEnum.findByLegalEnum(legalStatus);
//            System.out.println(legalStatus);

            if (legalStatusEnum != null && !legalStatusEnum.equals(LegalStatusEnum.EMPTY))
                return legalStatusEnum.name();
        }

        if (patentId.matches("^EP.+")) {
            LegalStatusEPEnum legalStatusEPEnum = LegalStatusEPEnum.findByLegalEnum(legalStatus);
            if (legalStatusEPEnum !=null && !legalStatusEPEnum.equals(LegalStatusEPEnum.EMPTY))
                return legalStatusEPEnum.name();
        } else {
            LegalStatusEnum legalStatusEnum = LegalStatusEnum.findByLegalEnum(legalStatus);

            if (legalStatusEnum !=null && !legalStatusEnum.equals(LegalStatusEnum.EMPTY))
                return legalStatusEnum.name();
        }

        return null;
    }


    public String legalStatus;

    public Boolean getActiveFlag() {
        if(legalStatus == null)
            return null;

        if (patentId == null) {
            LegalStatusEnum legalStatusEnum = LegalStatusEnum.findByLegalEnum(legalStatus);

            if (legalStatusEnum !=null && !legalStatusEnum.equals(LegalStatusEnum.EMPTY))
                return legalStatusEnum.active;
        }

        if (patentId.matches("^EP.+")) {
            LegalStatusEPEnum legalStatusEPEnum = LegalStatusEPEnum.findByLegalEnum(legalStatus);

            if (legalStatusEPEnum !=null && !legalStatusEPEnum.equals(LegalStatusEnum.EMPTY))
                return legalStatusEPEnum.active;
        } else {
            LegalStatusEnum legalStatusEnum = LegalStatusEnum.findByLegalEnum(legalStatus);

            if (legalStatusEnum !=null && !legalStatusEnum.equals(LegalStatusEnum.EMPTY))
                return legalStatusEnum.active;
        }

        return null;
    }

    public Boolean activeFlag;


    public String legalStatusEP;
    public String getLegalStatusEP() {
        LegalStatusEPEnum legalStatusEPEnum = LegalStatusEPEnum.findByLegalEnum(legalStatusEP);
        if (legalStatusEPEnum !=null && !legalStatusEPEnum.equals(LegalStatusEPEnum.EMPTY))
            return legalStatusEPEnum.name();

        return null;
    }


    public Boolean activeFlagEP;
    public Boolean getActiveFlagEP() {
        if(legalStatusEP == null)
            return null;

        LegalStatusEPEnum legalStatusEPEnum = LegalStatusEPEnum.findByLegalEnum(legalStatusEP);

        if (legalStatusEPEnum !=null && !legalStatusEPEnum.equals(LegalStatusEnum.EMPTY))
            return legalStatusEPEnum.active;

        return null;
    }

    public String getLastUpdateDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        return dtf.format(localDate);
    }

    public String lastUpdateDate;

    public Boolean dataCheckFlag;
}
