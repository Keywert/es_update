package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Claim {
    public Boolean image;              // 청구항 상태
    public Integer claimNumber;            // 청구항 번호
    public String claimState;              // 청구항 상태
    public String claimText;               // 종속항
    public String claimTextJP;             // 종속항
    public String claimTextCN;             // 종속항
    public String claimTextTR;             // 번역
    public String claimTextKO;             // 번역
    public List<Integer> topClaimNumbers;  // 종속항의 상위청구항 상태를 나타냄
    public String claimStatusCategory;  // 종속항의 상위청구항 상태를 나타냄

    public String independentClaimText;    // 독립항
    public String independentClaimTextJP;    // 독립항
    public String independentClaimTextCN;    // 독립항
    public String independentClaimTextTR;    // 번역
    public String independentClaimTextKO;

    public Boolean error;

    public String originClaimNumber;

    public String getClaimText() {
        String text = "";

        if (independentClaimText != null)
            text = independentClaimText;
        else if (claimText !=null)
            text = claimText;

        if (text.isEmpty())
            return null;

        return text.replace(System.getProperty("line.separator"), "")
                .replaceAll(">\\s*<", "><")
                .replaceAll("<(/)?([a-zA-Z|\\-]*)(\\s[a-zA-Z|\\-]*=[^>]*)?(\\s)*(/)?>", "")
                .replaceAll("\\s+", " ")
                .trim();
    }

}
