package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.Sets;
import es.config.ESConfig;
import es.model.Applicant;
import es.model.Person;
import es.model.code.CodeVO;
import es.model.v1.PriorityClaim;
import es.module.utility.PatentID;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Patent {
    public String org;
    public HashSet<String> citationNumbers;
    public String citationRegisterNumber;
    public String citationDocumentId;                  // 문서 ID
    public String documentId;                  // 문서 ID
    public String publishingORG;               // 발간청
    public String countryCode;                 // - docdb 국가
    public String openNumber;                  // 공개번호
    public String publicationNumber;           // (openNumber가 없을 시)공개번호
    public String registerNumber;              // 등록번호
    public String registerDate;                // 등록일자
    @JsonDeserialize(using = ObjectDeserializer.class)
    public String openDate;                   // 공개일자(To)
    public String publicationDate;            // 공개일자(To)
    public String documentType;                // 문헌종류
    public String documentTypeJP;              // 문헌종류 일본만
    public String documentTypeUS;              // 문헌종류 일본만
    public Integer updateLevel;

    public String codeUpdateDate;

    public String issueDate;
    public String patentDate;

    public String fileName;
    public String lastUpdateDate;

    public String patentNumber;

    public List<Drawing> drawings;

    public List<Patent> b1List;
    public List<Patent> f1List;


    public String appln_nr_epodoc;
    @JsonDeserialize(using = ObjectDeserializer.class)
    public String originalApplicationNumber;
    public String originalApplicationDate;

    public String applicationNumber;           // 번호
    public String applicationDate;            // 출원일

    public SuitInfoVO suitInfo;

    public String patentId;
    public String update_date;
    public String patentType;

    public String applicantCode;

    public String legalStatus;

    public List<Claim> claims;                 // 청구범위 (independentClaimText, claimText)
    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> description;                 // 청구범위 (independentClaimText, claimText)

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> summary;                 // 청구범위 (independentClaimText, claimText)

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> summaryJP;                 // 청구범위 (independentClaimText, claimText)

    /**
     * 추가 (한국 일본) 등록 상태 에서만 적용되는 번호 및 일자 개념
     **/
    public String announcementNumber;          // 공고 번호
    public String announcementDate;            // 공고 일자

    public String inventionTitleJP;            // 발명의 일본문헌
    public String inventionTitleCN;            // 발명의 중국문헌

    @JsonDeserialize(using = ObjectDeserializer.class)
    public String inventionTitleKO;            // 발명의 명칭 / 번역

    private String internationalApplicationNumber;      // 국제 출원번호
    private String internationalApplicationDate;        // 국제 출원일자
    private String internationalPublicationNumber;        // 국제 공개번호
    private String internationalPublicationDate;          // 국제 공개일자

    private String inventionTitle;                        // 발명의 명칭

    private List<CitationDoc> citationDoc;      // 인용문헌 (number, country)


    public String mainIpc;                     // IPC (Main)
    public String mainUpc;                     // UPC (Main)
    public String mainCpc;                     // CPC (Main)
    public List<String> subIpc;                // IPC (Sub)
    public List<String> subCpc;                // CPC (Sub)
    public List<String> subUpc;                // CPC (Sub)

    public List<CodeVO> ipcInfo;
    public List<CodeVO> cpcInfo;

    public String mainFi;                      // mainFI(JP)
    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<CodeVO> subFi;


    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> fTerm;                 // fTerm(JP)
    public List<String> themeCode;             // 테마코드(JP)


    public void setApplicantInfo(List<Applicant> applicantInfo) {
        if (applicantInfo != null && !applicantInfo.isEmpty())
            this.applicantInfo = applicantInfo;
    }

    public List<Applicant> getApplicantInfo() {
        if (this.applicantInfo != null && this.applicantInfo.isEmpty())
            return null;
        return applicantInfo;
    }

    public List<Applicant> applicantInfo;          // 출원인
    public Applicant firstApplicantInfo;   // 제1출원인
    public Applicant firstRightHolderInfo;   // 제1출원인


    public String applicantNames;
    public String applicantCount;
    public String applicantCountry;
    public String applicantCountryCount;
    public String applicantLocalStatus;
    public String applicantAddress;

    public String apNames() {
        if (this.applicantInfo != null) {
            HashSet<String> nameSet = new HashSet<>();
            for (Applicant applicant : this.getApplicantInfo()) {
                String t_name = applicant.getSrName();
                if (t_name != null && !"".equals(t_name))
                    nameSet.add(t_name);

                String seName = applicant.getSeName();
                if (seName != null && !"".equals(seName))
                    nameSet.add(seName);

            }

            if (!nameSet.isEmpty())
                return String.join(" ## ", nameSet);
        }

        return null;
    }


    public void setCurrentApplicantInfo(List<Applicant> currentApplicantInfo) {
        if (currentApplicantInfo != null && !currentApplicantInfo.isEmpty())
            this.currentApplicantInfo = currentApplicantInfo;
    }

    public List<Applicant> getCurrentApplicantInfo() {
        if (this.currentApplicantInfo != null && this.currentApplicantInfo.isEmpty())
            return null;

        return currentApplicantInfo;
    }

    public List<Applicant> currentApplicantInfo;   // 최근 출원인

    public void setRightHolderInfo(List<Applicant> rightHolderInfo) {
        if (rightHolderInfo != null && !rightHolderInfo.isEmpty())
            this.rightHolderInfo = rightHolderInfo;

    }

    public List<Applicant> rightHolderInfo;

    public List<Applicant> getInventorInfo() {
        if (this.currentInventorInfo != null)
            return this.getCurrentInventorInfo();
        return inventorInfo;
    }

    public void setInventorInfo(List<Applicant> inventorInfo) {
        if (inventorInfo != null && !inventorInfo.isEmpty())
            this.inventorInfo = inventorInfo;
    }

    public List<Applicant> inventorInfo;         // 발명자

    public void setCurrentInventorInfo(List<Applicant> currentInventorInfo) {
        if (currentInventorInfo != null && !currentInventorInfo.isEmpty())
            this.currentInventorInfo = currentInventorInfo;

    }

    public List<Applicant> currentInventorInfo;   // 발명자

    public String inventorNames;
    public String inventorCount;
    public String inventorCountry;
    public String inventorCountryCount;

    public List<Applicant> currentRightHolderInfo;    // 현재 권리인 ver revision 1.3.7
    public String currentRightHolderNames;
    public String currentRightHolderAddress;


    public void setAssigneeInfo(List<Applicant> assigneeInfo) {
        if (assigneeInfo != null && !assigneeInfo.isEmpty())
            this.assigneeInfo = assigneeInfo;

    }


    public List<Applicant> getAssigneeInfo() {
        if (this.assigneeInfo != null && this.assigneeInfo.isEmpty())
            return null;
        return assigneeInfo;
    }

    public List<Applicant> assigneeInfo;         // 미국 출원인 - 권리자


    public List<Applicant> getAgentInfo() {
        if (this.currentAgentInfo != null)
            return this.getCurrentAgentInfo();
        return agentInfo;
    }

    public void setAgentInfo(List<Applicant> agentInfo) {
        if (agentInfo != null && !agentInfo.isEmpty())
            this.agentInfo = agentInfo;
    }

    @JsonDeserialize(using = ApplicantArrayDeserializer.class)
    public List<Applicant> agentInfo;         // agent
    public List<Applicant> currentAgentInfo;         // agent


    public String examinerName;
    public String examinerNameJP;


    public String claimCount;                  // 청구항 갯수

    public Boolean examinationRequestStatus;   // 심사청구 상태
    public String examinationRequestDate;      // 심사청구 일자

    public List<Applicant> representApplicantInfo;             // 우선권 관련정보


    /// EPO UNITARY
    public List<Applicant> upInventorInfo;         // 발명자
    public List<Applicant> upRightHolderInfo;         // 발명자
    public List<Applicant> upAgentInfo;         // 발명자

    public List<Applicant> getExaminerInfo() {
        if (this.examinerInfo == null) {
            List<Applicant> examiners = new ArrayList<>();
            if (this.examinerName != null) {
                Applicant applicant = new Applicant();
                applicant.setName(this.examinerName);

                if (!this.documentId.matches("^(kr|jp|cn).+$"))
                    applicant.setEName(this.examinerName);

                if (this.examinerNameJP != null) {
                    applicant.setNameJP(this.examinerNameJP);
                }

                examiners.add(applicant);
            }

            if (!examiners.isEmpty())
                return examiners;
        }
        return examinerInfo;
    }

    public void setExaminerInfo(List<Applicant> examinerInfo) {
        if (examinerInfo != null && !examinerInfo.isEmpty())
            this.examinerInfo = examinerInfo;
    }

    @JsonDeserialize(using = ApplicantArrayDeserializer.class)
    public List<Applicant> examinerInfo;             // 우선권 관련정보

    public List<PriorityClaim> priorityClaims;             // 우선권 관련정보
    public String priorityClaimCountry;
    public String priorityClaimNumber;
    public String priorityClaimDate;
    public String priorityClaimMasterNumber;

//    public List<RelatedDocuments> relatedDocuments;

    // 피인용 심사관여부
    public Integer examinerCitationState;

    public String familyId;

    public String getDocdbFamilyId() {
        if (this.familyId != null)
            return familyId;
        return docdbFamilyId;
    }

    public String docdbFamilyId;
    public String inpadocFamilyId;

    public String extendFamilyId;
    public String mainFamilyId;
    public String completeFamilyId;

    @JsonDeserialize(using = ObjectDeserializer.class)
    public String extendedFamilyId;

    // dart Ip 정보
    public DartIpInfo dartipInfo;

    public GradeInfo gradeInfo;


    public String getPatentId() {
        if (this.patentId != null)
            return this.patentId;

        if (applicationNumber == null || documentId == null)
            return null;

        String id = PatentID.getPatentId(documentId, applicationNumber);

        return id;
    }


    public String getApplicationNumber() {
        if (applicationNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + applicationNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + applicationNumber.replaceAll("-", "");

        return applicationNumber;
    }

    public String getRegisterNumber() {
        if (registerNumber == null)
            return null;

        if (documentId == null)
            return registerNumber;

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + registerNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + registerNumber.replaceAll("-", "");

        return registerNumber;
    }

    public String getOpenNumber() {
        if (openNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + openNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + openNumber.replaceAll("-", "");

        return openNumber;
    }

    public String getPublicationNumber() {
        if (publicationNumber == null)
            return null;

        if (publicationNumber.matches("^(10|20)-[0-9]{4}-[0-9]{7}$"))
            return publicationNumber.replaceAll("-", "");

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + publicationNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + publicationNumber.replaceAll("-", "");

        return publicationNumber;
    }


    public List<Patent> getPairPatent(ESConfig esConfig) {
        String patentId = this.getPatentId();
        if (patentId == null)
            return null;

        String docId = this.getDocumentId();
        if (docId == null)
            return null;

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.idsQuery().addIds(docId))
                .filter(QueryBuilders.matchQuery("patentId", patentId));

        SearchResponse scrollResp = esConfig.client.prepareSearch(this.getOrg())
                .setTypes("patent")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
//                .setFetchSource(fields, null)
                .setSize(100)
                .execute()
                .actionGet();

        ObjectMapper objectMapper = new ObjectMapper();
        List<Patent> patents = new ArrayList<>();

        try {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                Patent pairPatent = objectMapper.readValue(hit.getSourceAsString(), Patent.class);

                patents.add(pairPatent);
//                System.out.println(hit.getSourceAsString());

            }

            if (!patents.isEmpty())
                return patents;


        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    public void print() {
        List<Applicant> applicantInfo = this.getApplicantInfo();

        System.out.println(this.documentId + " : " + this.getPatentId());

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            if (applicantInfo != null) {
                System.out.println("applicantInfo : " + objectMapper.writeValueAsString(applicantInfo));
            }

            List<Applicant> currentApplicantInfo = this.getCurrentApplicantInfo();
            if (currentApplicantInfo != null)
                System.out.println("currentApplicantInfo : " + objectMapper.writeValueAsString(currentApplicantInfo));

            List<Applicant> rightHolderInfo = this.getRightHolderInfo();
            if (rightHolderInfo != null)
                System.out.println("rightHolderInfo : " + objectMapper.writeValueAsString(rightHolderInfo));

            List<Applicant> currentRightHolderInfo = this.getCurrentRightHolderInfo();
            if (currentRightHolderInfo != null)
                System.out.println("currentRightHolderInfo : " + objectMapper.writeValueAsString(currentRightHolderInfo));

            List<Applicant> assigneeInfo = this.getAssigneeInfo();
            if (assigneeInfo != null)
                System.out.println("assigneeInfo : " + objectMapper.writeValueAsString(assigneeInfo));

            List<Applicant> inventorInfo = this.getInventorInfo();
            if (inventorInfo != null)
                System.out.println("inventorInfo : " + objectMapper.writeValueAsString(inventorInfo));

            List<Applicant> representApplicantInfo = this.getRepresentApplicantInfo();
            if (representApplicantInfo != null)
                System.out.println("representApplicantInfo : " + objectMapper.writeValueAsString(representApplicantInfo));

            List<Applicant> examinerInfo = this.getExaminerInfo();
            if (examinerInfo != null)
                System.out.println("examinerInfo : " + objectMapper.writeValueAsString(examinerInfo));

            List<Applicant> agentInfo = this.getAgentInfo();
            if (agentInfo != null)
                System.out.println("agentInfo : " + objectMapper.writeValueAsString(agentInfo));

            Applicant firstApplicantInfo = this.getFirstApplicantInfo();
            if (firstApplicantInfo != null)
                System.out.println("firstApplicantInfo : " + objectMapper.writeValueAsString(firstApplicantInfo));

            Applicant firstRightHolderInfo = this.getFirstRightHolderInfo();
            if (firstRightHolderInfo != null)
                System.out.println("firstRightHolderInfo : " + objectMapper.writeValueAsString(firstRightHolderInfo));

            System.out.println("---------------------------------------------------------");
            System.out.println();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public boolean merge(Patent pairPatent) {
        boolean updateFlag = false;
        // 현재출원인
        if (this.currentApplicantInfo == null && pairPatent.currentApplicantInfo != null && !pairPatent.currentApplicantInfo.isEmpty()) {
            this.setCurrentApplicantInfo(pairPatent.getCurrentApplicantInfo());
            updateFlag = true;
        }

        // 출원인
        if (this.applicantInfo == null && pairPatent.applicantInfo != null && !pairPatent.applicantInfo.isEmpty()) {
            this.setApplicantInfo(pairPatent.getApplicantInfo());
            updateFlag = true;
        }

        // 양수인
        if (this.assigneeInfo == null && pairPatent.assigneeInfo != null && !pairPatent.assigneeInfo.isEmpty()) {
            this.setAssigneeInfo(pairPatent.getAssigneeInfo());
            updateFlag = true;
        }

        // 최초권리자
        if (this.rightHolderInfo == null && pairPatent.rightHolderInfo != null && !pairPatent.rightHolderInfo.isEmpty()) {
            this.setRightHolderInfo(pairPatent.getRightHolderInfo());
            updateFlag = true;
        }

        // 최종권리자
        if (this.currentRightHolderInfo == null && pairPatent.currentRightHolderInfo != null && !pairPatent.currentRightHolderInfo.isEmpty()) {
            this.setCurrentRightHolderInfo(pairPatent.getCurrentRightHolderInfo());
            updateFlag = true;
        }

        // 대표출원인
        if (this.representApplicantInfo == null && pairPatent.representApplicantInfo != null) {
            this.setRepresentApplicantInfo(pairPatent.getRepresentApplicantInfo());
            updateFlag = true;
        }

        // summary
        if (this.summary == null && pairPatent.summary != null) {
            this.setSummary(pairPatent.getSummary());
            updateFlag = true;
        }

        // summaryJP
        if (this.summaryJP == null && pairPatent.summaryJP != null) {
            this.setSummaryJP(pairPatent.getSummaryJP());
            updateFlag = true;
        }

        // claims
        if (this.claims == null && pairPatent.claims != null) {
            this.setClaims(pairPatent.getClaims());
            updateFlag = true;
        }

        // claims
        if (this.description == null && pairPatent.description != null) {
            this.setDescription(pairPatent.getDescription());
            updateFlag = true;
        }

        // docdbFamilyId
        if (this.docdbFamilyId == null && pairPatent.docdbFamilyId != null) {
            this.setDocdbFamilyId(pairPatent.getDocdbFamilyId());
            updateFlag = true;
        }

        // extendedFamilyId
        if (this.extendedFamilyId == null && pairPatent.extendedFamilyId != null) {
            this.setExtendedFamilyId(pairPatent.getExtendedFamilyId());
            updateFlag = true;
        }

        // extendFamilyId
        if (this.extendFamilyId == null && pairPatent.extendFamilyId != null) {
            this.setExtendFamilyId(pairPatent.getExtendFamilyId());
            updateFlag = true;
        }

        // mainFamilyId
        if (this.mainFamilyId == null && pairPatent.mainFamilyId != null) {
            this.setMainFamilyId(pairPatent.getMainFamilyId());
            updateFlag = true;
        }

        // completeFamilyId
        if (this.completeFamilyId == null && pairPatent.completeFamilyId != null) {
            this.setCompleteFamilyId(pairPatent.getCompleteFamilyId());
            updateFlag = true;
        }

        return updateFlag;
    }

    public void addData(Map<String, List<Applicant>> additionalMap) {
        List<Applicant> personList = additionalMap.get("personList");
        if (personList == null)
            personList = new ArrayList<>();


        if (this.applicantInfo != null)
            personList.addAll(this.getApplicantInfo());

        if (this.rightHolderInfo != null)
            personList.addAll(this.getRightHolderInfo());

        if (this.assigneeInfo != null)
            personList.addAll(this.getAssigneeInfo());

        if (this.currentApplicantInfo != null)
            personList.addAll(this.getCurrentApplicantInfo());

        if (this.currentRightHolderInfo != null)
            personList.addAll(this.getCurrentRightHolderInfo());

        if (this.applicantInfo != null) {
            List<Applicant> list = new ArrayList<>();

            for (Applicant base : this.applicantInfo) {
                for (Applicant addition : personList) {
                    if (base.cmp(addition))
                        base.addInfo(addition);

                }
                list.add(base);
            }
            this.setApplicantInfo(list);
        }

        if (this.currentApplicantInfo != null) {
            List<Applicant> list = new ArrayList<>();

            for (Applicant base : this.currentApplicantInfo) {
                for (Applicant addition : personList) {
                    if (base.cmp(addition))
                        base.addInfo(addition);

                }
                list.add(base);
            }
            this.setCurrentApplicantInfo(list);
        }

        if (this.rightHolderInfo != null) {
            List<Applicant> list = new ArrayList<>();

            for (Applicant base : this.rightHolderInfo) {
                for (Applicant addition : personList) {
                    if (base.cmp(addition))
                        base.addInfo(addition);

                }
                list.add(base);
            }
            this.setRightHolderInfo(list);
        }

        if (this.currentRightHolderInfo != null) {
            List<Applicant> list = new ArrayList<>();

            for (Applicant base : this.currentRightHolderInfo) {
                for (Applicant addition : personList) {
                    if (base.cmp(addition))
                        base.addInfo(addition);

                }
                list.add(base);
            }
            this.setCurrentRightHolderInfo(list);
        }

        List<Applicant> lastRightHolders = additionalMap.get("lastRightHolders");
        if (lastRightHolders != null) {
            List<Applicant> list = new ArrayList<>();

            for (Applicant base : lastRightHolders) {
                for (Applicant addition : personList) {
                    if (base.cmp(addition))
                        base.addInfo(addition);

                }
                list.add(base);
            }
            lastRightHolders = list;
            this.setCurrentRightHolderInfo(list);
        }


        if (this.applicantInfo == null) {
            if (additionalMap.get("cApplicants") != null)
                this.setApplicantInfo(additionalMap.get("cApplicants"));
            else if (this.currentApplicantInfo != null && !this.currentApplicantInfo.isEmpty())
                this.setApplicantInfo(this.getCurrentApplicantInfo());

            if (this.documentId.matches("^us.+$")) {
                if (this.assigneeInfo != null && !this.assigneeInfo.isEmpty())
                    this.setApplicantInfo(this.assigneeInfo);
                else if (this.inventorInfo != null && !this.inventorInfo.isEmpty())
                    this.setApplicantInfo(this.inventorInfo);
            }

        }

        if (this.currentApplicantInfo == null) {
//            if (additionalMap.get("cApplicants") != null)
//                this.setCurrentApplicantInfo(additionalMap.get("cApplicants"));
//            else
            if (this.assigneeInfo != null && !this.assigneeInfo.isEmpty()) {
                this.setCurrentApplicantInfo(this.getAssigneeInfo());
            } else if (this.applicantInfo != null && !this.applicantInfo.isEmpty()) {
                this.setCurrentApplicantInfo(this.getApplicantInfo());
            } else if (additionalMap.get("cApplicants") != null)
                this.setCurrentApplicantInfo(additionalMap.get("cApplicants"));
        }

        if (additionalMap.get("cApplicants") != null)
            this.setCurrentApplicantInfo(additionalMap.get("cApplicants"));

        if (this.rightHolderInfo == null) {
            if (lastRightHolders != null)
                this.setRightHolderInfo(lastRightHolders);
            else if (this.currentRightHolderInfo != null)
                this.setRightHolderInfo(this.getCurrentRightHolderInfo());
        }

        String docId = this.getDocumentId();
        if (this.currentRightHolderInfo == null) {
            if (docId != null && !docId.matches("^(ep).+$")) {
                if (lastRightHolders != null)
                    this.setCurrentRightHolderInfo(lastRightHolders);
                else if (this.rightHolderInfo != null)
                    this.setCurrentRightHolderInfo(this.getRightHolderInfo());
            }
        }

        // 영문 이름 추가
        if (docId != null && !docId.matches("^(kr|jp).+$")) {
            if (this.applicantInfo != null) {
                List<Applicant> personInfo = addEnName(this.applicantInfo);
                if (personInfo != null)
                    this.setApplicantInfo(personInfo);
            }

            if (this.currentApplicantInfo != null) {
                List<Applicant> personInfo = addEnName(this.currentApplicantInfo);
                if (personInfo != null)
                    this.setCurrentApplicantInfo(personInfo);
            }

            if (this.rightHolderInfo != null) {
                List<Applicant> personInfo = addEnName(this.rightHolderInfo);
                if (personInfo != null)
                    this.setRightHolderInfo(personInfo);
            }

            if (this.currentRightHolderInfo != null) {
                List<Applicant> personInfo = addEnName(this.currentRightHolderInfo);
                if (personInfo != null)
                    this.setCurrentRightHolderInfo(personInfo);
            }

            if (this.assigneeInfo != null) {
                List<Applicant> personInfo = addEnName(this.assigneeInfo);
                if (personInfo != null)
                    this.setAssigneeInfo(personInfo);
            }

            if (this.inventorInfo != null) {
                List<Applicant> personInfo = addEnName(this.inventorInfo);
                if (personInfo != null)
                    this.setInventorInfo(personInfo);
            }

            if (docId != null && !docId.matches("^(cn).+$")) {
                if (this.agentInfo != null) {
                    List<Applicant> personInfo = addEnName(this.agentInfo);
                    if (personInfo != null)
                        this.setAgentInfo(personInfo);
                }

                if (this.examinerInfo != null) {
                    List<Applicant> t_examiners = new ArrayList<>();
                    for (Applicant examiner : this.examinerInfo) {
                        String t_name = examiner.getName();
                        if (t_name != null) {
                            examiner.setEName(t_name);
                        } else {
                            Person primaryExaminer = examiner.getPrimaryExaminer();
                            if (primaryExaminer != null) {
                                String exName = primaryExaminer.getName();
                                if (exName != null) {
                                    examiner.setName(exName);
                                    examiner.setEName(exName);
                                }
                            } else {
                                String fName = examiner.getFirstName();
                                String lName = examiner.getLastName();

                                if (fName != null && lName != null) {
                                    String exName = fName + " " + lName;
                                    examiner.setName(exName);
                                    examiner.setEName(exName);
                                }
                            }
                        }
                        t_examiners.add(examiner);
                    }
                    if (!t_examiners.isEmpty())
                        this.examinerInfo = t_examiners;
                }
            }
        }

//        if ((this.applicantInfo != null || this.assigneeInfo != null)) {
//            List<Applicant> applicants = this.getApplicantInfo();
//            if (applicants == null)
//                applicants = this.getAssigneeInfo();
//
//            if (applicants != null && !applicants.isEmpty())
//                this.setFirstApplicantInfo(applicants.get(0));
//
//        }

        if (this.currentApplicantInfo != null) {
            List<Applicant> applicants = this.getCurrentApplicantInfo();
            if (applicants != null)
                this.setFirstApplicantInfo(applicants.get(0));
        }

        if (this.currentRightHolderInfo != null) {
            List<Applicant> applicants = this.getCurrentRightHolderInfo();
            if (applicants != null)
                this.setFirstRightHolderInfo(applicants.get(0));
        }

    }

    private List<Applicant> addEnName(List<Applicant> personInfo) {
        List<Applicant> persons = new ArrayList<>();
        for (Applicant person : personInfo) {
            String t_name = person.getrName();
            if (t_name != null)
                person.setEName(t_name);

            persons.add(person);
        }
        if (!persons.isEmpty())
            return persons;

        return null;
    }


    public boolean getESExist(ESConfig esConfig) {
        String docId = this.getDocumentId();
        if (docId == null)
            return false;

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.idsQuery().addIds(docId));
        String[] fields = {documentId};

        SearchResponse response = esConfig.client.prepareSearch(this.getOrg())
                .setTypes("patent")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setFetchSource(fields, null)
                .setSize(0)
                .execute()
                .actionGet();

        long hitcount = response.getHits().getTotalHits();
        if (hitcount != 0)
            return true;

        return false;
    }

    public void sipoNameUpdate(Connection conn) {
        Set<String> nameCNSet = new HashSet<>();

        if (this.applicantInfo != null)
            nameCNSet.addAll(extractCnNameSet(this.getApplicantInfo()));

        if (this.rightHolderInfo != null)
            nameCNSet.addAll(extractCnNameSet(this.getRightHolderInfo()));

        if (this.currentRightHolderInfo != null)
            nameCNSet.addAll(extractCnNameSet(this.getCurrentRightHolderInfo()));

        if (this.currentApplicantInfo != null)
            nameCNSet.addAll(extractCnNameSet(this.getCurrentApplicantInfo()));

        if (this.inventorInfo != null)
            nameCNSet.addAll(extractCnNameSet(this.getInventorInfo()));

        if (nameCNSet.isEmpty())
            return;

        Map<String, Applicant> cnMap = getSipoEnName(conn, nameCNSet);
        if (cnMap == null)
            return;

//        try {
//            System.out.println(new ObjectMapper().writeValueAsString(cnMap));
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

        if (this.applicantInfo != null) {
            List<Applicant> person = sipoAddName(cnMap, this.applicantInfo);
            if (person != null)
                this.setApplicantInfo(person);
        }

        if (this.rightHolderInfo != null) {
            List<Applicant> person = sipoAddName(cnMap, this.rightHolderInfo);
            if (person != null)
                this.setRightHolderInfo(person);
        }

        if (this.currentApplicantInfo != null) {
            List<Applicant> person = sipoAddName(cnMap, this.currentApplicantInfo);
            if (person != null)
                this.setCurrentApplicantInfo(person);
        }

        if (this.currentRightHolderInfo != null) {
            List<Applicant> person = sipoAddName(cnMap, this.currentRightHolderInfo);
            if (person != null)
                this.setCurrentRightHolderInfo(person);
        }

        if (this.inventorInfo != null) {
            List<Applicant> person = sipoAddInventorName(cnMap, this.inventorInfo);
            if (person != null)
                this.setInventorInfo(person);
        }
    }

    private List<Applicant> sipoAddInventorName(Map<String, Applicant> cnMap, List<Applicant> personInfos) {
        if (this.applicantInfo == null || cnMap == null)
            return null;

        List<Applicant> resultPersonInfos = new ArrayList<>();
        for (Applicant applicant : personInfos) {
            if (applicant.getName() == null) {
                String nameCN = applicant.getOrgNameCN();
                if (nameCN == null)
                    nameCN = applicant.getNameCN();

                if (nameCN != null) {
                    Applicant applicantCN = cnMap.get(nameCN);
                    if (applicantCN != null) {
                        applicant.addInventorCnInfo(applicantCN);
                    }
                }
            }

            resultPersonInfos.add(applicant);
        }

        return null;
    }


    private List<Applicant> sipoAddName(Map<String, Applicant> cnMap, List<Applicant> personInfos) {
        if (this.applicantInfo == null || cnMap == null)
            return null;

        List<Applicant> resultPersonInfos = new ArrayList<>();
        for (Applicant applicant : personInfos) {
//            if (applicant.getName() == null) {
            String nameCN = applicant.getOrgNameCN();
            System.out.println(nameCN);
            if (nameCN == null)
                nameCN = applicant.getNameCN();

            if (nameCN != null) {
                Applicant applicantCN = cnMap.get(nameCN);
                if (applicantCN != null) {
                    applicant.addCnInfo(applicantCN);
                }
            }
//            }

            resultPersonInfos.add(applicant);
        }

        if (!resultPersonInfos.isEmpty())
            return resultPersonInfos;

        return null;
    }


    private Map<String, Applicant> getSipoEnName(Connection conn, Set<String> nameCNSet) {
        if (nameCNSet.isEmpty())
            return null;

        String cnName_str = StringUtils.join(nameCNSet, "','");
        String query = "select * from person_info.sipo_names where orgNameCN in('" + cnName_str + "')";
//        System.out.println(query);

        int count = 0;
        Statement st = null;

        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            Map<String, Applicant> map = new HashMap<>();

            while (rs.next()) {
                if (++count % 100 == 0)
                    System.out.println(count);

                String orgNameCN = rs.getString("orgNameCN");
                if (orgNameCN == null || "".equals(orgNameCN))
                    continue;

                String name = rs.getString("name");
                if (name == null || "".equals(name))
                    continue;

                Applicant person = new Applicant();
                person.setOrgNameCN(orgNameCN);
                person.setName(name);
                person.setEnName(name);

                String nexis_key = rs.getString("nexis_key");
                if (nexis_key != null)
                    person.setKey(nexis_key);

                String orgName = rs.getString("orgName");
                if (orgName != null)
                    person.setOrgName(orgName);

                String orgName_docdb = rs.getString("orgName_docdb");
                if (orgName_docdb != null)
                    person.setOrgName_docdb(orgName_docdb);

                String orgName_docdba = rs.getString("orgName_docdba");
                if (orgName_docdba != null)
                    person.setOrgName_docdba(orgName_docdba);

                String orgname_normalized = rs.getString("orgname_normalized");
                if (orgname_normalized != null)
                    person.setOrgnameNormalized(orgname_normalized);

                String orgname_standardized = rs.getString("orgname_standardized");
                if (orgname_standardized != null)
                    person.setOrgnameStandardized(orgname_standardized);

//                try {
//                    System.out.println(new ObjectMapper().writeValueAsString(person));
//                } catch (JsonProcessingException e) {
//                    e.printStackTrace();
//                }
                map.put(orgNameCN, person);
            }

            if (!map.isEmpty())
                return map;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }


        return null;
    }

    public Set<String> extractCnNameSet(List<Applicant> personInfoList) {
        Set<String> set = new HashSet<>();
        for (Applicant applicant : personInfoList) {
//            String name = applicant.getName();
//            if (name != null)
//                continue;

            String orgNameCN = applicant.getOrgNameCN();
            if (orgNameCN != null)
                set.add(orgNameCN);

            String nameCN = applicant.getNameCN();
            if (nameCN != null)
                set.add(nameCN);
        }

        return set;
    }

//    public boolean getNameCheck() {
//        if (this.applicantInfo != null) {
//            List<Applicant> applicants = this.getApplicantInfo();
//            if (applicants.size() == 1)
//                return false;
//
//            for (Applicant applicant : applicants) {
//                String name = applicant.getOrgName();
//                if (name.matches("^.+(CO\\.,| LLC| INC\\.| CORP\\.).*$"))
//                    continue;
//
//                if (name.contains(", ")) {
//                    return true;
//                }
//            }
//        }
//
//        return false;
//    }

    public Set<String> makeIpcCom() {
        List<String> list = new ArrayList<>();
        if (this.mainIpc != null) {
            if (this.mainIpc.matches("^[A-z][0-9]{2}[A-z].*$"))
                list.add(this.mainIpc.substring(0, 4));
        }

        if (this.subIpc != null) {
            List<String> subIpcs = this.getSubIpc();
            for (String ipc : subIpcs) {
                if (ipc == null)
                    continue;
                if (ipc.matches("^[A-z][0-9]{2}[A-z].*$"))
                    list.add(ipc.substring(0, 4));
            }
        }
        Set<List<String>> result = new HashSet<>();
        Set<String> set = new HashSet<>();
        combinations(list, new ArrayList<>(), result, 2, 0);

        for (List<String> ipcSet : result) {
            ArrayList<String> al = new ArrayList<>(ipcSet);
            Collections.sort(al);
            set.add(StringUtils.join(al, "_"));
        }

        if (!set.isEmpty())
            return set;

        return null;
    }

    public static void combinations(List<String> values, List<String> current, Set<List<String>> accumulator, int size, int pos) {
        if (current.size() == size) {
            List<String> toAdd = current.stream().collect(Collectors.toList());
            accumulator.addAll(Collections.singleton(toAdd));
            return;
        }
        for (int i = pos; i <= values.size() - size + current.size(); i++) {
            current.add(values.get(i));
            combinations(values, current, accumulator, size, i + 1);
            current.remove(current.size() - 1);
        }
    }

    public PriorityClaim earliestPriorityClaim(Connection conn) {
        List<PriorityClaim> totalPriorityClaims = new ArrayList<>();
        PriorityClaim earliestPriorityClaim = null;

        List<PriorityClaim> priorityClaims = this.getPriorityClaims();
        if (priorityClaims != null)
            totalPriorityClaims.addAll(priorityClaims);

        String docId = this.getDocumentId();
        if (docId.matches("^(kr|jp).+")) {
            String originalApplicationNumber = this.getOriginalApplicationNumber();
            if (originalApplicationNumber != null) {
                System.out.println(originalApplicationNumber);
                PriorityClaim priorityClaim = new PriorityClaim();
                priorityClaim.setPriorityApplicationCountry(docId.substring(0, 2).toUpperCase());
                priorityClaim.setPriorityApplicationDate(this.getOriginalApplicationDate());
                priorityClaim.setPriorityApplicationNumber(originalApplicationNumber);

//                String docType = this.getOri
//                priorityClaim.setPriorityApplicationNumber(this.getPriorityClaimNumber());
                totalPriorityClaims.add(priorityClaim);
            }
        }

        String applicationNumber = this.getApplicationNumber();
        if (totalPriorityClaims.isEmpty()) {
            // Base 처
            PriorityClaim priorityClaimBase = new PriorityClaim();
            priorityClaimBase.setBaseFlag(true);

            if (applicationNumber != null)
                priorityClaimBase.setPriorityApplicationNumber(applicationNumber);

            String applicationDate = this.getApplicationDate();
            if (applicationDate != null)
                priorityClaimBase.setPriorityApplicationDate(applicationDate);

            String publishingORG = this.getPublishingORG();
            if (publishingORG != null)
                priorityClaimBase.setPriorityApplicationCountry(publishingORG);

            String patentId = this.getPatentId();
            if (patentId != null)
                priorityClaimBase.setPatentId(patentId);

            totalPriorityClaims.add(priorityClaimBase);
        }

        if (docId.matches("^us.+")) {
            PriorityClaim earliestRelatedDoc = usReleatedDoc(conn, applicationNumber.replaceAll("[/|\\-]", ""));
            if (earliestRelatedDoc != null)
                totalPriorityClaims.add(earliestRelatedDoc);
        }

        if (totalPriorityClaims.isEmpty()) {
            System.out.println(this.getDocumentId() + "totalPriorityClaims Empty.");
            return null;
        }

        if (totalPriorityClaims.size() == 1)
            earliestPriorityClaim = totalPriorityClaims.get(0);
        else {
            Optional<PriorityClaim> optionalPriorityClaim = totalPriorityClaims
                    .stream().filter(x -> x.getPriorityApplicationDate() != null)
                    .sorted(Comparator.comparing(PriorityClaim::getPriorityApplicationDate))
                    .findFirst();

            if (optionalPriorityClaim.isPresent())
                earliestPriorityClaim = optionalPriorityClaim.get();
        }

        if (earliestPriorityClaim != null)
            return earliestPriorityClaim;

        return null;
    }

    private static PriorityClaim usReleatedDoc(Connection conn, String apnum) {
        String query = "select * from USPTO_ADMIN.USPTO_PATENT_CONTINUITY_PARENTS_CHILDREN " +
                " where application_number = '" + apnum + "' and con_filing_date != '' and con_filing_date not like '0%' " +
                " order by con_filing_date limit 1";

        System.out.println(query);

        Statement st = null;
        try {
            st = conn.createStatement();
            st.setFetchSize(100);
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String applicationNumber = rs.getString("con_application_number");
                String applicationDate = rs.getString("con_filing_date");
                System.out.println(applicationNumber + " : " + applicationDate);

                if (applicationNumber != null && applicationDate != null) {
                    PriorityClaim priorityClaim = new PriorityClaim();
                    priorityClaim.setPriorityApplicationNumber(applicationNumber);
                    priorityClaim.setPriorityApplicationDate(applicationDate.replaceAll("-", ""));

                    String country = "US";
                    if (applicationNumber.contains("PCT"))
                        country = "WO";

                    priorityClaim.setPriorityApplicationCountry(country);

                    return priorityClaim;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String stdApplicantNames;
    public String applicantNameOrigin;
    public String firstApplicantName;
    public String firstApplicantCountry;

    public void setApplicantInfos() {
        List<Applicant> applicants = null;

        if (currentApplicantInfo != null)
            applicants = currentApplicantInfo;
        else if (assigneeInfo != null) {
            applicants = assigneeInfo;
        } else if (applicantInfo != null) {
            applicants = applicantInfo;
        }

        if (applicants == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();
        ArrayList<String> localStatus = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();
        ArrayList<String> nameListOri = new ArrayList<>();
        ArrayList<String> stdNameList = new ArrayList<>();

        for (Applicant applicant : applicants) {
            String name = applicant.getrName();
            String country = applicant.getCountry();
            if (country == null)
                country = " - ";

            String oName = applicant.getoName();
            String stdName = applicant.getSrName();

            if (name == null)
                name = applicant.getSrName();

            if (name == null)
                name = applicant.getoName();

            if (name == null)
                continue;

            nameList.add(name);
            nameListOri.add(oName);
            stdNameList.add(stdName);

            if (country != null) {
//                System.out.println(country);
                countryList.add(country);
                countrySet.add(country);
            }

            if (applicant.getCountry() == null)
                localStatus.add("내국인");
            else if (applicant.getCountry().matches("US"))
                localStatus.add("내국인");
            else
                localStatus.add("외국인");

            if (applicant.getAddress() != null)
                address.add(applicant.getAddress().split("\\s")[0].replaceAll(",", ""));

        }

        if (nameList.isEmpty())
            return;

        this.setApplicantNames(String.join("|", nameList));
        this.setApplicantCount(String.valueOf(applicants.size()));
        this.setApplicantCountry(String.join("|", countryList));
        this.setApplicantCountryCount(String.valueOf(countrySet.size()));
        this.setApplicantLocalStatus(String.join("|", localStatus));
        this.setApplicantAddress(String.join("|", address));
        this.setApplicantNameOrigin(String.join("|", nameListOri));
        this.setStdApplicantNames(String.join("|", stdNameList));
        this.setFirstApplicantName(nameList.get(0));

        if (!countryList.isEmpty())
            this.setFirstApplicantCountry(countryList.get(0));
    }

    public String family_patent_count_extended;
    public String family_country_count_extended;
    public String family_patent_total_number_extended;
    public String major_country_family_patent_count_extended;
    public String family_patent_count;
    public String family_country_count;
    public String family_c_count_extended;
    public String family_patent_total_number;
    public String major_country_family_patent_count;
    public String family_3_status;


    public void setFamilyInfos(ESConfig esConfig) {
        if (docdbFamilyId == null && extendedFamilyId == null)
            return;

//        List<FamilyVO> familyVOS = getFamilyExtend(esConfig, applicationNumber, docdbFamilyId, extendedFamilyId);
        List<FamilyVO> familyVOS = getFamilyEpoSimple(esConfig, applicationNumber, docdbFamilyId, extendedFamilyId);
        if (familyVOS == null)
            return;

        ArrayList<String> familyNumbers = new ArrayList<>();
        ArrayList<String> familyCountry = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();

        ArrayList<String> docdbFamilyNumbers = new ArrayList<>();
        ArrayList<String> docdbFamilyCountry = new ArrayList<>();
        HashSet<String> docdbCountrySet = new HashSet<>();

        for (FamilyVO familyVO : familyVOS) {
//            System.out.println(familyVO.getDocumentId());
            String familyId = familyVO.getFamilyId();

            String applicationNumber = familyVO.getPublicationNumber();
            String documentType = familyVO.getDocumentType();
            if (documentType == null)
                documentType = "A";
            documentType = documentType.replaceAll("[W]", "A");

            String country = familyVO.getCountryCode();
            if (country == null)
                country = familyVO.getCountry();

            if (country == null)
                continue;

            String familyNumber = "";

            if (applicationNumber == null || applicationNumber.isEmpty()) {
                applicationNumber = familyVO.getApplicationNumber();

                if (applicationNumber == null || applicationNumber.isEmpty())
                    applicationNumber = familyVO.getApplicationNumber();

                familyNumber = country + applicationNumber + documentType;

            } else if (country.matches("US")) {
                applicationNumber = familyVO.getPublicationNumber();

                if (applicationNumber == null || applicationNumber.isEmpty())
                    applicationNumber = familyVO.getApplicationNumber();

                familyNumber = country + applicationNumber + documentType;

            } else {
                applicationNumber = applicationNumber.replaceAll("[A-z]$", "");
                familyNumber = country + applicationNumber + documentType;
            }

            familyNumbers.add(familyNumber);
            familyCountry.add(country);
            countrySet.add(country);

//            System.out.println(docdbFamilyId + " : " + familyId);

            if (docdbFamilyId != null && !"".equals(docdbFamilyId) && familyId != null && !"".equals(familyId)) {
                if (docdbFamilyId.matches(familyId)) {
                    docdbFamilyNumbers.add(familyNumber);
                    docdbFamilyCountry.add(country);
                    docdbCountrySet.add(country);
                }
            }
        }

        HashSet<String> docdbFamilyNumberSet = Sets.newHashSet(docdbFamilyNumbers);
        HashSet<String> familyNumberSet = Sets.newHashSet(familyNumbers);

        this.setFamily_3_status("N");
        if (countrySet.contains("US") && countrySet.contains("JP") && countrySet.contains("EP"))
            this.setFamily_3_status("Y");

        this.setFamily_c_count_extended((countrySet.isEmpty() ? null : String.valueOf(countrySet.size())));
        this.setFamily_patent_count_extended((familyNumbers.isEmpty() ? null : String.valueOf(familyNumbers.size())));
        this.setFamily_patent_total_number_extended((familyNumbers.isEmpty() ? null : String.join(", ", familyNumbers)));
        this.setFamily_country_count_extended((familyCountry.isEmpty() ? null : getCountryCount(familyCountry)));

        this.setFamily_patent_count((docdbFamilyNumbers.isEmpty() ? null : String.valueOf(docdbFamilyNumbers.size())));
        this.setFamily_patent_total_number((docdbFamilyNumbers.isEmpty() ? null : String.join(", ", docdbFamilyNumbers)));
        this.setFamily_country_count((docdbFamilyCountry.isEmpty() ? null : getCountryCount(docdbFamilyCountry)));


    }

    private static List<FamilyVO> getFamilyEpoSimple(ESConfig esConfig, String original_application_number, String docdbFamilyId, String extendedFamilyId) {
        ObjectMapper mapper = new ObjectMapper();

        String[] index = {"uspto", "kipo", "aupo", "capo", "frpo", "gbpo", "inpo", "itpo", "rupo", "tipo", "dpma", "pct", "epo", "jpo", "sipo", "jpo_past", "uspto_past", "docdb"};
        String[] country = {"US", "KR", "AU", "CA", "FR", "GB", "IN", "IT", "RU", "TW", "DE", "WO", "EP", "JP", "CN"};
        String type = "patent";

        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        bqb = bqb.mustNot(QueryBuilders.boolQuery()
                .filter(QueryBuilders.termsQuery("countryCode", country))
                .filter(QueryBuilders.matchQuery("_index", "docdb")));

        String[] familyFields = {"docdbFamilyId", "familyId"};

        if (docdbFamilyId != null && !"".equals(docdbFamilyId))
            bqb = bqb.should(QueryBuilders.boolQuery().filter(QueryBuilders.multiMatchQuery(docdbFamilyId, familyFields)));

//        if (extendedFamilyId != null && !"".equals(extendedFamilyId))
//            bqb = bqb.should(QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("extendedFamilyId", extendedFamilyId)));

//        bqb = bqb.filter(QueryBuilders.existsQuery(""));

//        if (original_application_number != null && !"".equals(original_application_number))
//            bqb = bqb.mustNot(QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("original_application_number", original_application_number)));


        String[] includes = {
                "epoPublicationNumber", "applicationNumber", "countryCode", "inpadocFamilyId", "familyId", "epoApplicationNumber", "documentType", "documentId", "patentDate",
                "docdbFamilyCitationCount", "docdbFamilyCount", "extendedFamilyId", "publicationNumber", "patentNumber", "publishingORG", "patentId", "patentType", "docdbFamilyId",
                "documentTypeUS", "documentTypeDE", "documentTypeCN"
        };

//        String[] includes = {
//
//        };
//
//        String[] excludes = {
//                "description", "summary", "inventionTitles", "claims", "summaries"
//        };

//        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
//                .setTypes(type)
//                .setSearchType(SearchType.SCAN)
//                .setScroll(new TimeValue(200000))
//                .setQuery(bqb)
//                .setFetchSource(includes, null)
//                .addSort("patentDate",SortOrder.DESC)
//                .setSize(10)
//                .execute()
//                .actionGet();


        SearchResponse response = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(bqb)
                .setFetchSource(includes, null)
                .addSort("patentDate", SortOrder.DESC)
                .setSize(10000)
                .execute()
                .actionGet();

//        System.out.println(esConfig.client.prepareSearch(index)
//                .setTypes(type)
//                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//                .setQuery(bqb)
//                .setFetchSource(includes, null)
//                .addSort("applicationDate", SortOrder.ASC)
//                .setSize(10000));

//        System.out.println("Family Total Hits : " + scrollResp.getHits().getTotalHits());

//        Map<String, FamilyVO> map = new HashMap<>();

        ArrayList<FamilyVO> list = new ArrayList<>();

        try {
            for (SearchHit hit : response.getHits().getHits()) {
//                System.out.println(hit.getSourceAsString());
                FamilyVO familyVO = mapper.readValue(hit.getSourceAsString(), FamilyVO.class);
                if (familyVO == null)
                    continue;

                list.add(familyVO);

            }

            if (!list.isEmpty())
                return list;

        } catch (IOException e) {
            e.printStackTrace();
        }

//        do {
//            for (SearchHit hit : scrollResp.getHits().getHits()) {
//                try {
//                    System.out.println(hit.getSourceAsString());
//                    FamilyVO familyVO = mapper.readValue(hit.getSourceAsString(), FamilyVO.class);
//                    if (familyVO == null)
//                        continue;
//
////                    String patentType = familyVO.getPatentType();
////                    if (patentType !=null && patentType.matches("^[U|Y][0-9]?$"))
////                        continue;
//
////                    String key = familyVO.getEpoPublicationNumber();
////                    if (key == null)
////                        continue;
////
////                    key = key.replaceAll("[A-z]$", "");
//////                    System.out.println(key);
////
////                    map.put(key, familyVO);
//
////                    String patentId = familyVO.getPatentId();
////                    map.put(patentId,familyVO);
//                    list.add(familyVO);
////                    System.out.println(objectMapper.writeValueAsString(familyVO));
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
//        } while (scrollResp.getHits().getHits().length != 0);

//        System.out.println(map.size());


//        if (!map.isEmpty())
//            return map.values().stream().collect(Collectors.toList());

        return null;
    }

    private String getCountryCount(ArrayList<String> countryList) {
        if (countryList == null)
            return null;

        Map<String, Long> reuslt = countryList.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        List<String> list = new ArrayList<>();
        for (String country : reuslt.keySet()) {
            list.add(country + "(" + reuslt.get(country) + ")");
        }

//        System.out.println(String.join("||", list));

        if (!list.isEmpty())
            return String.join(", ", list);

        return null;
    }
}

