package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Drawing implements Serializable {
    private String drawingId;           // 이미지 id
    private String drawingNumber;           // 이미지 id
    private String imgFile;             // 이미지 경로
    private String imgId;               // 이미지 id 2
    private String imgHe;               // 이미지 높이
    private String imgWi;               // 이미지 파일
    private String imgAlt;              // 파일
    private String inline;              // 파일
    private String imgContent;              // 파일
    private String id;              // 파일
    private String num;              // 파일
    private String orientation;              // 파일
    private String original;              // 파일

    @JsonProperty("figure-labels")
    private String figureLabels;              // 파일


}
