package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssigmentVO {
    public List<PersonVO> assignees;
    public List<PersonVO> assignors;
}
