package es.model.bulk;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonVO extends StdNameNorm {
    private String postcode;
    public String sequence;
    public String attrRefType;
    public String role;
    int groupId;
    public String ADRS;
    public String PCODE;
    public String KR_RepresentativeName;
    public String KR_InventorInformation;
    public String agentSequence;
    public String agentRefType;
    public String status;
    public String department;

    public String seqNumber;

    protected String category;
    protected String id;

    protected String appealExaminerName;
    protected String type;
    protected String key;
    protected String name;
    protected String nameCN;
    protected String firstName;
    protected String lastName;
    protected String orgName;
    protected String orgNameCN;
    protected String orgNameCN_original;
    protected String orgName_docdb;
    protected String orgName_docdba;
    protected String orgName_origin;
    protected String orgName_origin_original;
    protected String orgName_original;

    protected String country;
    protected String country_docdb;
    protected String country_docdba;
    protected String country_original;

    protected String address;
    protected String registeredNumber;
    protected String addressJP;
    protected String nameJP;
    protected String name_origin;
    protected String enName;
    protected String originalName;
    protected String originalAddress;
    protected String nameEN;
    protected String addressEN;
    protected String applicantType;
    protected String applicantType_docdb;
    protected String applicantType_docdba;
    protected String applicantType_original;

    protected String codeCN;
    protected String codeEN;

    protected String middleName;
    protected String applicantAuthorityCategory;
    protected String citizenship;
    protected String designation;
    protected String email;
    protected String fax;
    protected String phone;
    protected String postalcode;
    protected String prefix;
    protected String residenceCountry;
    protected String rights;
    protected String state;
    protected String suffix;
    protected String telephone;
    public String businessNumber;
    public String corporateNumber;
    public String code;

    public String nameKR;
    public String applicantCodeKR;
    public String applicantCodeJP;

    public String keywertCodeKR;
    public String keywertCodeEN;
    public String keywertCodeTL;

    public String nameKROrigin;
    public String nameENOrigin;
    public String nameJPOrigin;

    @JsonProperty("orgname-standardized")
    protected String orgnameStandardized;

    @JsonProperty("orgname-normalized")
    protected String orgnameNormalized;

    @JsonProperty("orgname-normalized_lookup")
    protected String orgnameNormalizedLookup;

    @JsonProperty("orgname-standardized_lookup")
    protected String orgnameStandardizedLookup;

    protected String name_docdb;
    protected String name_docdba;

    protected String address_docdb;
    protected String address_docdba;
    protected String address_original;
    protected String addressCN;

    public String stdOrgName;
    public String key_lookup;
    public String orgName_lookup;

    public String getStdName() {
        String std = this.getName();
        if (std != null && !"".equals(std))
            return koNameNorm(std);

        return stdName;
    }

    public String stdName;
    public String stdNameKo;


    public String getOrilName () {
        if (nameJP != null)
            return nameJP;

        if (name_origin != null)
            return name_origin;

        if (nameCN != null)
            return nameCN;

        if (orgNameCN != null)
            return orgNameCN;

        if (originalName != null)
            return originalName;

        if (name_origin != null)
            return name_origin;

        return null;
    }

    public String getName () {
        if (name != null)
            return name;

        if (orgName != null)
            return orgName;

        if (enName != null)
            return enName;

        if (name_origin != null)
            return name_origin;

        if (name_docdb != null)
            return name_docdb;

        if (name_docdba != null)
            return name_docdba;

        if (nameJP != null)
            return nameJP;

        if (nameCN != null)
            return nameCN;

        if (orgNameCN != null)
            return orgNameCN;

        if (originalName != null)
            return originalName;

        if (name_origin != null)
            return name_origin;

        return null;
    }

    public String getAddress () {
        if (address != null)
            return address;

        if (originalAddress != null)
            return originalAddress;

        if (address_docdb != null)
            return address_docdb;

        if (address_docdba != null)
            return address_docdba;

        if (address_original != null)
            return address_original;

        return null;
    }
}
