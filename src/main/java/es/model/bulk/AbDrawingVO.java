package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AbDrawingVO {
    public String alt;
    public String amendStatus;
    public String drawingId;
    public String drawingType;
    public String figureStatusCategory;
    public String imgContent;
    public String imgFile;
    public String imgFormat;
    public String imgHe;
    public String imgId;
    public String imgWi;
    public String orientation;
    public String type;
}
