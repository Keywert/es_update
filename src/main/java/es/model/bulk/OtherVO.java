package es.model.bulk;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtherVO {
    // figures
    public String  numberFigures;
    public String  numberDrawingSheets;


    // searchFields
    public String  text;
    public String  type;
    public String  mainClassification;
    public String  additionalInfo;


    //termGrant
    public String  termExtension;
    public String  grantLength;
    public String  disclaimerText;
    public String  isTerminalDisclaimer;
    public String  termGrant;

}
