package es.model.bulk;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.Applicant;
import es.model.code.CodeVO;
import es.model.enums.WipoIpc;
import es.model.v1.AssigneeInfo;
import es.model.v1.PriorityClaim;
import es.module.utility.PatentID;
import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentVO {
    /**
     * 기본 데이터 and 생성데이터
     **/
    public String org;
    public String lastUpdateDate;

    public String documentId;                  // 문서 ID
    public String publishingORG;               // 발간청
    public String countryCode;                 // - docdb 국가

    public String documentType;                // 문헌종류
    public String documentTypeJP;              // 문헌종류 일본만
    public String documentTypeUS;              // 문헌종류 미국
    public String applicationType;              // 출원종
    public String applicationCountry;              // 출원국가


    public String keyword;              // 문헌종류 미국

    public String licenseGrant;

    public List<String> sequenceList;

    public String patentNumber;             // 문헌번호
    public String getPatentNumber() {
        if (this.patentNumber == null){
            if (this.getRegisterNumber() != null)
                return this.getRegisterNumber();
            else if (this.getAnnouncementNumber() != null)
                return this.getAnnouncementNumber();
            else if (this.getPublicationNumber() != null)
                return this.getPublicationNumber();
        }
        return patentNumber;
    }

    public String patentDate;              // 문헌일자
    public String getPatentDate() {
        if (this.patentDate == null){
            if (this.getRegisterDate() != null)
                return this.getRegisterDate();
            else if (this.getAnnouncementDate() != null)
                return this.getAnnouncementDate();
            else if (this.getPublicationDate() != null)
                return this.getPublicationDate();
        }

        return patentDate;
    }

    public String patentType;              // 문헌타입 ('A','B','U','Y','E','S','P')

    public String patentId;
    public String update_date;

    public Boolean examinationRequestStatus;   // 심사청구 상태
    public String examinationRequestDate;      // 심사청구 일자

    public String originalExaminationRequestDate;

    public String getWipo_35_category() {
        if (this.wipo_35_category != null )
            return this.wipo_35_category;

        if (this.mainIpc != null){
            WipoIpc wipoIpc = WipoIpc.findByWipoIpc(this.mainIpc);
            if (WipoIpc.EMPTY.equals(wipoIpc))
                wipoIpc = WipoIpc.findByWipoIpc(this.mainIpc.substring(0, 4));

            if (!WipoIpc.EMPTY.equals(wipoIpc))
                return wipoIpc.name();
        }

        return null;
    }

    public String wipo_35_category;

    public String getWipo_5_category() {
        if (this.wipo_5_category != null )
            return this.wipo_5_category;

        if (this.mainIpc != null){
            WipoIpc wipoIpc = WipoIpc.findByWipoIpc(this.mainIpc);
            if (WipoIpc.EMPTY.equals(wipoIpc))
                wipoIpc = WipoIpc.findByWipoIpc(this.mainIpc.substring(0, 4));

            if (!WipoIpc.EMPTY.equals(wipoIpc))
                return wipoIpc.getSector_code();
        }
        return null;
    }

    public String wipo_5_category;

    public String languageCode;

    public String getKeywertId() {
        if (this.documentId != null) {
            String id = PatentID.getKeywertId(documentId);

            return id;
        }

        return keywertId;
    }

    public String keywertId;


    /**
     * 법적상태
     **/
    public Boolean activeFlag;
    public Boolean activeFlagEP;

    public String legalStatus;
    public String regalStatus;

    /**
     * 기본 서지사항 번호, 일자, 타입 등
     **/
    public String applicationNumber;           // 출번번호
    public String applicationDate;            // 출원일자
    public String applicationNumberKind;       //
    public String aplicationCategory;       //

    public String openNumber;                  // 공개번호 ()
    public String openDate;                   // 공개일자 ()
    public String publicationNumber;           // 공개번호
    public String publicationDate;            // 공개일자

    public String registerNumber;              // 등록번호
    public String registerDate;                // 등록일자

    private String internationalApplicationNumber;      // 국제 출원번호
    private String internationalApplicationDate;        // 국제 출원일자
    private String internationalPublicationNumber;        // 국제 공개번호
    private String internationalPublicationDate;          // 국제 공개일자

    public String originalApplicationNumber;    //원출원번호
    public String originalApplicationDate;      //원출원일자
    public String originalApplicationKind;

    /**
     * 추가 (한국 일본) 등록 상태 에서만 적용되는 번호 및 일자 개념
     **/
    public String announcementNumber;          // 공고 번호
    public String announcementDate;            // 공고 일자


    public String internationalPublicationDate_origin;
    public String internationalApplicationNumber_origin;
    public String internationalApplicationDate_origin;
    public String originalExaminationRequestDate_origin;
    public String publicationDate_origin;
    public String translationSubmitDate_origin;
    public String openDate_origin;

    /**
     * 우선권번호
     **/
    public List<PriorityClaim> priorityClaims;             // 우선권 관련정보


    /**
     * 코드정보
     **/
    public String mainIpc;                     // IPC (Main)
    public String mainUpc;                     // UPC (Main)
    public String mainCpc;                     // CPC (Main)
    public List<String> subIpc;                // IPC (Sub)
    public List<String> subCpc;                // CPC (Sub)
    public List<String> subUpc;                // CPC (Sub)

    public String mainFi;                      // mainFI(JP)
    public List<String> fTerm;                 // fTerm(JP)
    public List<String> themeCode;             // 테마코드(JP)

    public List<CodeVO> ipcInfo;                // IPC (Sub)
    public List<CodeVO> cpcInfo;                // CPC (Sub)
    public CodeVO mainUpcInfo;                // Upc
    public CodeVO mainCpcInfo;                // CPC (Sub)
    public CodeVO mainIpcInfo;                // CPC (Sub)

    public String cpcVersionDate;                     // CPC (Main)

    public String locarnoEdition;                     // locarnoEdition
    public String locarno;                     // locarnoEdition



    public String techCpc;                     // CPC (Main)
    public String techIpc;                     // CPC (Main)

    public String subCpcValue;                     // CPC (Main)
    public String subIpcLevel;                     // CPC (Main)
    public String subIpcValue;                     // CPC (Main)
    public String mainCpcValue;                     // CPC (Main)
    public String mainIpcValue;                     // CPC (Main)
    public String ipcVersionDate;                     // CPC (Main)
    public String mainIpcLevel;                     // CPC (Main)
    public String ipcVersion;                     // CPC (Main)
    public String mainIpcVersion;                     // CPC (Main)

    /**
     * 인명정보
     **/
    public List<Applicant> agentInfo;
    public List<Applicant> examinerInfo;
    public List<Applicant> currentAgentInfo;
    public String agencyCode;
    public String agentName;

    public List<Applicant> applicantInfo;

    public List<Applicant> getCurrentApplicantInfo() {
        if (this.currentApplicantInfo == null) {
            if (this.applicantInfo != null)
                return this.getApplicantInfo();
//            else if (this.assigneeInfo != null)
//                return this.a
        }

        return currentApplicantInfo;
    }

    public List<Applicant> currentApplicantInfo;   // 최근 출원인


    public List<Applicant> inventorInfo;         // 미국 출원인 - 권리자
    public List<Applicant> currentInventorInfo;         // 미국 출원인 - 권리자

    public List<Applicant> rightHolderInfo;


    public List<Applicant> getCurrentRightHolderInfo() {
        if (this.currentRightHolderInfo == null){
            if (this.rightHolderInfo != null)
                return this.getRightHolderInfo();
        }
        return currentRightHolderInfo;
    }

    public List<Applicant> currentRightHolderInfo;         // 미국 출원인 - 권리자

    public List<AssigneeInfo> assigneeInfo;         // 미국 출원인 - 권리자

    public String examinerName;
    public List<Applicant> examiners;         // 미국 출원인 - 권리자

    public List<Applicant> representApplicantInfo;         // 미국 출원인 - 권리자

    public AssigmentVO assignmentInfo;


    /**
     * 인용, 피인용, 관련문헌
     **/
    private List<CitationDocVO> citationDoc;      // 인용문헌 (number, country)
    public List<RelatedDocuments> relatedDocuments;
    // 피인용 심사관여부
    public Integer examinerCitationState;


    /**
     * 패밀리 관련
     **/
    public String familyId;
    public String docdbFamilyId;
    public String inpadocFamilyId;
    public String extendedFamilyId;

    public Boolean familyBaseStatus;
    public Boolean familyCheck;

    /**
     * 텍스트 , 발명의 명칭, 요약, 청구항, 상세설명
     **/
    public String inventionTitle;            // 발명의 일본문헌
    public String inventionTitleEng;

    public String inventionTitleJP;            // 발명의 일본문헌
    public String inventionTitleCN;            // 발명의 중국문헌
    public String inventionTitleKO;            // 발명의 중국문헌 / 번역

    public List<Map<String,String>> inventionTitles;            // 발명

    public List<Map<String,String>> summaries;            // 요약

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> summary;                 // 청구범위 (independentClaimText, claimText)


    public List<Claim> claims;                 // 청구범위 (independentClaimText, claimText)
    public String claimCount;                  // 청구항 갯수

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> claimStatus;

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> description;                 // 청구범위 (independentClaimText, claimText)
    public List<String> drawingDescription;                 // 청구범위 (independentClaimText, claimText)


    /**
     * RND 정보
     **/
    public String  managementMachinery;
    public String  ministryName;
    public String researchPeriod;
    public String researchProjectName;
    public String researchSubjectName;
    public String researchManageExpertMachin;
    public List<String>  nationalFundingResearchProjectBag;

    public List<RndVO> rndInfo;

    /**
     * 도면정보
     **/

    public AbDrawingVO abstractDrawing;       //도면정보\
    public List<String>  abstractFigure;
    public String  abstractFigureId;


    public List<String> referenceSignsList;
    public List<DrawingVO> drawings;


    /**
     * 소송, 등급정보
     **/
    public DartIpInfo dartipInfo;
    public GradeInfo gradeInfo;
    public SuitInfoVO suitInfo;

    /**
     * 기타
     **/
    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> SequenceList;

    public String us374Date;
    public Boolean kipoTxtFlag;

    public List<String> designatedStates;
    public List<String> CombinationSetInfo;

    public List<String> internationalInfo;
    public String originalApplicationDate_origin;
    public String exemplaryClaim;

    public List<String> formulaMath;
    public List<String> formulaChemistry;


    @JsonDeserialize(using = OtherVOArrayDeserializer.class)
    public List<OtherVO>  figures;

    @JsonDeserialize(using = OtherVOArrayDeserializer.class)
    public List<OtherVO>  searchFields;

    @JsonDeserialize(using = OtherVOArrayDeserializer.class)
    public List<OtherVO>  termGrant;

    /**
     *  확인필요
     **/
    public String IPCInformationVersion;
    public String amendClaimCount;
    public String appealDate;

    public List<Applicant> appealExaminer;
    public String appealExaminerChiefName;
    public String appealKind;
    public String appealNumber;
    public String applicantCode;

    public String applicationCountryCode;
    public String applicationInformationKind;
    public String applicationSeriesCode;
    public String applnId;
    public String appln_nr_epodoc;
    public String approvalDate;
    public List<String> authorityCorrection;

    public String contributionRatio;
    public List<String> correctionInfo;

    public Boolean dataCheckFlag;
    public Boolean deduplication;

    public String desireTechnicalGuidance;
    public String desiredTechnologyTransfer;
    public String docdbFamilyCitationCount;
    public String docdbFamilyCount;

    public String examClaimCount;
    public String examinationStatus;

    public String figurePage;
    public String finalDisposition;
    public String firstActionTypes;

    public String technicalRequestIndicator;
    public String technologyAssign;
    public String technologyGuidance;
    public String translationSubmitDate;
    public String text;
    public String rndInfoNames;
    public String rndInfoNumbers;
    public String size;
    public String subjectID;
    public String examinationRequestDate_origin;

    public String getPatentId() {
        if (applicationNumber == null || documentId == null)
            return null;

        String id = PatentID.getPatentId(documentId,applicationNumber);

        return id;
    }

    public String getApNames(){
        if (this.applicantInfo != null){
            HashSet<String> nameSet = new HashSet<>();
            for (Applicant applicant : this.getApplicantInfo()) {
                String t_name = applicant.getSrName();
                if (t_name != null && !"".equals(t_name))
                    nameSet.add(t_name);
            }

            if (!nameSet.isEmpty())
                return String.join(" ## ",nameSet);
        }

        return null;
    }


//    public String getApplicationNumber() {
//        if (applicationNumber == null)
//            return null;
//
//        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
//            return "10" + applicationNumber.replaceAll("-", "");
//        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
//            return "20" + applicationNumber.replaceAll("-", "");
//
//        return applicationNumber;
//    }
//
//    public String getRegisterNumber() {
//        if (registerNumber == null)
//            return null;
//
//        if (documentId == null)
//            return registerNumber;
//
//        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
//            return "10" + registerNumber.replaceAll("-", "");
//        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
//            return "20" + registerNumber.replaceAll("-", "");
//
//        return registerNumber;
//    }
//
//    public String getOpenNumber() {
//        if (openNumber == null)
//            return null;
//
//        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
//            return "10" + openNumber.replaceAll("-", "");
//        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
//            return "20" + openNumber.replaceAll("-", "");
//
//        return openNumber;
//    }
//
//    public String getPublicationNumber() {
//        if (publicationNumber == null)
//            return null;
//
//        if (publicationNumber.matches("^(10|20)-[0-9]{4}-[0-9]{7}$"))
//            return publicationNumber.replaceAll("-","");
//
//        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
//            return "10" + publicationNumber.replaceAll("-", "");
//        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
//            return "20" + publicationNumber.replaceAll("-", "");
//
//        return publicationNumber;
//    }

}
