package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CitationDocVO {
    public String citedType;               // 인용타입
    public String examinerCitationState;   // 심사관인용
    public String documentNumber;          // 문헌번호
    public String kind;                    // 종류
    public String country;                 // 국가
    public String date;
    public String citedBy;
    public String mainIpc;
    public String mainUpc;
    public String name;
    public String text;
    public Integer sequence;
    public String citedPhase;
    public String category;


}