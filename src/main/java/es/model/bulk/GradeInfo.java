package es.model.bulk;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GradeInfo {
    String smart3Grade;

    String smart3GradeR;

    String smart3GradeU;

    String smart3GradeT;

    String grade;

    String gradeU;

    String gradeT;

    String gradeR;

    Boolean aliveFlag;

    String updateDate;

    String class1;
    String class2;
    String class3;

    String publishingDate;

    Boolean smart3Flag;
}
