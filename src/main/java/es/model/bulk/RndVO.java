package es.model.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RndVO {
    public Integer sequence;
    public String rndTaskNumber;            // 과제고유번호 subjectID
    public String rndDepartmentName;        // 부처명 ministryName
    public String rndProjectName;          // 연구사업명 researchProjectName
    public String rndTaskName;             // 연구과제명 researchSubjectName
    public String rndManagingInstituteName; // 주관기관 managementMachinery
    public String rndDuration;                  // 연구기간 researchPeriod
    public String rndSpecialInstituteName;   // 연구과제전문기관 researchManageExpertMachin
    public String rndTaskContribution;  // 기여율 contributionRatio
    public String rndAgencyDetailTaskNumber;  // KR_AgencyDetailSubjectID

    public String startDate;
    public String endDate;
}
