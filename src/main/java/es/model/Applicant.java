package es.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import es.module.utility.CountryCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class Applicant extends Person implements Cloneable {
    private String postcode;
    public String attrRefType;
    public String role;
    public Integer groupId;

    // assignment
    public Integer rankNumber;
    public String sequence;
    public String correlatorType;


    public String date;

    public Integer docCount;

    public String examinerGroup;
    public Person primaryExaminer;

    public void setOrganizationName(String organizationName) {
        if(organizationName == null)
            return;

        this.orgName = organizationName;
    }

    public String getPostcode() {
        if (postcode != null)
            return postcode.replaceAll("[0-9]", "*");

        return null;
    }

    public String getCountry() {
        if (country == null) {
            if (this.code != null && this.getNameJP() == null) {
                String c = this.getCode();
                if (c.matches("^[1-4][0-9]{11}$"))
                    return "KR";
            }

            return null;
        }

        if (country.equals("J["))
            country = "JP";

        if (country.matches("99")) {
            if (this.code != null) {
                String c = this.getCode();
                if (c.matches("^[1-4][0-9]{10,15}$"))
                    country = "KR";
            }
        }

        country = country.trim().replaceAll("\\s", "");

        if (country.matches("^[A-z]{2}$"))
            return country.toUpperCase();

        String countryCode = CountryCode.countryCode.get(country);

        if (countryCode != null)
            if (countryCode.matches("^[A-z]{2}$"))
                return countryCode;

        country = country.replaceAll("[^가-힣]", "");

        countryCode = CountryCode.countryCode.get(country);

        return countryCode;
    }

    public Applicant clone() throws CloneNotSupportedException {
        Applicant applicant = (Applicant) super.clone();
        applicant.country  = this.country;
        applicant.sequence = this.sequence;
        applicant.code  = this.code;
        applicant.name  = this.name;
        applicant.enName  = this.enName;
        return applicant;
    }

    public boolean cmp(Applicant addition) {
        if (this.getName() != null && addition.getName() != null)
            if (this.getName().equals(addition.getName()))
                return true;

        if (this.getCode() != null && addition.getCode() != null)
            if (this.getCode().equals(addition.getCode()))
                return true;

        if (this.getRegisteredNumber() != null && addition.getRegisteredNumber() != null)
            if (this.getRegisteredNumber().equals(addition.getRegisteredNumber()))
                return true;

        if (this.getNameJP() != null && addition.getNameJP() != null)
            if (this.getNameJP().equals(addition.getNameJP()))
                return true;

        if (this.getNameCN() != null && addition.getNameCN() != null)
            if (this.getNameCN().equals(addition.getNameCN()))
                return true;

        return false;
    }

    public void addInfo(Applicant addition) {
        if (this.getCode() == null && addition.getCode() != null)
            this.setCode(addition.getCode());

        if (this.getEnName() == null && addition.getEnName() != null)
            this.setEnName(addition.getEnName());

        if (this.getEnName() == null && addition.getNameEN() != null)
            this.setEnName(addition.getNameEN());

        if (this.getAddress() == null && addition.getAddress() != null)
            this.setAddress(addition.getAddress());

        if (this.getCountry() == null && addition.getCountry() != null)
            this.setCountry(addition.getCountry());


    }


    public String getApplicantCodeNumber() {
        if (this.code != null)
            return this.getCode();
        else if (this.registeredNumber != null)
            return this.getRegisteredNumber();
        else if (this.registeredNumber2 != null)
            return this.getRegisteredNumber2();


        return null;
    }

    public boolean cmp2(Applicant applicant) {
        if (this.getName() != null && applicant.getName() != null)
            if (this.getName().equals(applicant.getName()))
                return true;

        return false;
    }

    public void addCnInfo(Applicant addition ) {
        if (this.getName() == null && addition.getName() != null)
            this.setName(addition.getName());

        if (this.getKey() == null && addition.getKey() != null)
            this.setKey(addition.getKey());

        if (this.getEnName() == null && addition.getEnName() != null)
            this.setEnName(addition.getEnName());

        if (this.getOrgName() == null && addition.getOrgName() != null)
            this.setOrgName(addition.getOrgName());

        if (this.getOrgName_docdb() == null && addition.getOrgName_docdb() != null)
            this.setOrgName_docdb(addition.getOrgName_docdb());

        if (this.getOrgName_docdba() == null && addition.getOrgName_docdba() != null)
            this.setOrgName_docdba(addition.getOrgName_docdba());

        if (this.getOrgnameNormalized() == null && addition.getOrgnameNormalized() != null)
            this.setOrgnameNormalized(addition.getOrgnameNormalized());

        if (this.getOrgnameStandardized() == null && addition.getOrgnameStandardized() != null)
            this.setOrgnameStandardized(addition.getOrgnameStandardized());

    }

    public void addInventorCnInfo(Applicant addition ) {
        if (this.getName() == null && addition.getName() != null)
            this.setName(addition.getName());

        if (this.getKey() == null && addition.getKey() != null)
            this.setKey(addition.getKey());

    }
}