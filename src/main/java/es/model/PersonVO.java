package es.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import es.module.utility.CountryCode;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonVO {
    protected String name;
    public String nameKR;
    protected String orgName;
    protected String enName;
    protected String nameEN;
    public String applicantCodeKR;
    public String applicantCodeJP;
    protected String code;

    @JsonProperty("orgname-standardized")
    public String orgnameStandardized;

    protected String nameJP;
    protected String nameCN;


    protected String firstName;
    protected String lastName;
    protected String orgNameCN;
    protected String country;
    protected String address;
    protected String registeredNumber;
    protected String addressJP;
    protected String name_origin;
    protected String originalName;
    protected String originalAddress;
    protected String addressEN;

    public String keywertCodeKR;
    public String keywertCodeEN;
    public String keywertCodeTL;
    public String nameKROrigin;
    public String nameENOrigin;
    public String nameJPOrigin;

    public String businessNumber;
    public String type;

    private String postcode;
    public String sequence;
    public String attrRefType;

    public String getPostcode() {
        if (postcode != null)
            return postcode.replaceAll("[0-9]", "*");

        return null;
    }

    public String getCountry() {
        if (country == null)
            return null;

        country = country.trim().replaceAll("\\s", "");

        if (country.matches("^[A-z]{2}$"))
            return country;

        String countryCode = CountryCode.countryCode.get(country);

        if (countryCode != null)
            if (countryCode.matches("^[A-z]{2}$"))
                return countryCode;

        country = country.replaceAll("[^가-힣]", "");

        countryCode = CountryCode.countryCode.get(country);

        return countryCode;

    }

}
