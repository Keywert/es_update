package es.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.module.utility.CountryCode;
import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriorityClaim {
    String country;               // 우선권 번호
    String priorityApplicationNumber;               // 우선권 번호
    String applicationType;               // 우선권 번호
    String otherApplicationNumber;               // 우선권 번호
    String priorityDate;               // 우선권 번호
    String text;               // 우선권 번호


    public String getPriorityApplicationCountry() {
        if (priorityApplicationCountry != null && !"".equals(priorityApplicationCountry)) {
            if (priorityApplicationCountry.matches("^[A-z]{2}$"))
                return priorityApplicationCountry.toUpperCase();

            Matcher matcher = Pattern.compile("\\([A-z]{2}\\)").matcher(priorityApplicationCountry);
            if(matcher.find())
                return matcher.group().replaceAll("\\(|\\)","");

            String cc = CountryCode.countryCode.get(priorityApplicationCountry);
            if (cc == null)
                return cc;

            if (cc.matches("^[A-z]{2}$")){
                return cc.toUpperCase();
            }
            System.out.println("priorityApplicationCountry : " + priorityApplicationCountry);

        }
        return priorityApplicationCountry;
    }

    String priorityApplicationCountry;              // 우선권 국가
    String priorityApplicationDate;                 // 우선원 주장일

    String docdbApplicationNumber;                 // 우선권 번호
    String originalApplicationNumber;                 // 우선원 주장일
    String epodocApplicationNumber;
    String nationalApplicationNumber;
    public String priorityApplicationDate_original;

    String kind;

    String dataFormat;                              // sipo 관련 데이터 타입 - docdb 빼곤 스킵 상위단계에서
    String sequence;                                // sequence

    public void setApplicationCountry(String applicationCountry) {
        if(applicationCountry == null)
            return;

        this.priorityApplicationCountry = applicationCountry;
    }

    Boolean baseFlag;
    String patentId;

}
