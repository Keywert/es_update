package es.model.v1;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RndInfo {
    public Integer sequence;
    public String rndTaskNumber;            // 과제고유번호 subjectID
    public String rndDepartmentName;        // 부처명 ministryName
    public String rndProjectName;          // 연구사업명 researchProjectName
    public String rndTaskName;             // 연구과제명 researchSubjectName
    public String rndManagingInstituteName; // 주관기관 managementMachinery

    public void setRndDuration(String rndDuration) {
        this.rndDuration = rndDuration;

        if (rndDuration != null && !"".equals(rndDuration)) {
            Matcher matcher = Pattern.compile("\\s[0-9](월|일)").matcher(rndDuration);
            while (matcher.find()){
                String tmp = matcher.group().trim();
                rndDuration = rndDuration.replaceFirst(tmp,"0"+tmp);
            }

            String duration = rndDuration
                    .replaceAll("(\\.|-|년|월|일)(\\s)?","");

            matcher = Pattern.compile("(19|20)[0-9]{2}[0-1][0-9][0-3][0-9]").matcher(duration);
            List<String> list = new ArrayList<>();
            while (matcher.find()){
                list.add(matcher.group());
            }

            if (list.size() != 2){
                System.out.println(rndDuration + "      " + duration);
                return;
            }

//            System.out.println("정렬 전  : " + list.toString());
            list.sort(Comparator.naturalOrder());
//            System.out.println("오름차순 : " + list.toString());

            this.startDate = list.get(0);
            this.endDate = list.get(1);
        }
    }

    public String rndDuration;                  // 연구기간 researchPeriod
    public String rndSpecialInstituteName;   // 연구과제전문기관 researchManageExpertMachin
    public String rndTaskContribution;  // 기여율 contributionRatio
    public String rndAgencyDetailTaskNumber;  // KR_AgencyDetailSubjectID

    public String startDate;
    public String endDate;
}
