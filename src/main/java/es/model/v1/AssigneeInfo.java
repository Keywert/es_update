package es.model.v1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper=false)
public class AssigneeInfo extends Person {
    String sequence;
    String role;

    public void setOrganizationName(String organizationName) {
        if(organizationName == null)
            return;

        this.orgName = organizationName;
    }
}
