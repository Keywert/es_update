package es.model.v1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Joiner;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentInfo {
    String sequence;
    String job;
    String registeredNumber;
    String name;
    String nameJP;
    String nameCN;
    String name_origin;

    public String getName() {
        if(nameCN != null)
            return nameCN;

        if(name != null)
            return name;

        if(name_origin != null)
            return name_origin;

        return name;
    }

    public static String renderAgentCNString(List<String> names) {
        if(names != null && !names.isEmpty())
            return Joiner.on(", ").join(names);
        else
            return null;
    }
}
