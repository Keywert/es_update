package es.model.v1;

import lombok.Getter;

public enum CountryEnum {

    JPPAJ("JPPAJ", "paj", "PAJ", null),
    DOCDB("DOCDB", "docdb", "DOCDB", null),
    KR("한국", "kipo", "KR", 82),
    JP("일본", "jpo", "JP", 81),
    EP("유럽", "epo", "EP", null),
    US("미국", "uspto", "US", 01),
    // 같은 내용
    WO("국제특허", "pct", "PCT", null),
    // 필터 (DOCDB) 용
    PCT("국제특허", "pct", "WO", null),
    // 같은 워드
    CN("중국", "sipo", "CN", 86),
    //    WO("국제특허", "pct", "PCT", null),
    DE("DE", "dpma", "DE", null),
    GB("GB", "gbpo", "GB", null),
    FR("FR", "frpo", "FR", null),
    CA("CA", "capo", "CA", null),
    AU("AU", "aupo", "AU", null),
    TW("TW", "tipo", "TW", null),
    IT("IT", "itpo", "IT", null),
    IN("IN", "inpo", "IN", null),
    RU("RU", "rupo", "RU", null),

    MX("MX", "docdb", "MX", null),
    IB("IB", "docdb", "IB", null),
    ES("ES", "docdb", "ES", null),
    BD("BD", "docdb", "BD", null),
    SU("SU", "docdb", "SU", null),
    AT("AT", "docdb", "AT", null),
    SE("SE", "docdb", "SE", null),
    CH("CH", "docdb", "CH", null),
    BR("BR", "docdb", "BR", null),
    NL("NL", "docdb", "NL", null),
    BE("BE", "docdb", "BE", null),
    FI("FI", "docdb", "FI", null),
    DK("DK", "docdb", "DK", null),
    ZA("ZA", "docdb", "ZA", null),
    NO("NO", "docdb", "NO", null),
    PL("PL", "docdb", "PL", null),
    IL("IL", "docdb", "IL", null),
    HU("HU", "docdb", "HU", null),
    DD("DD", "docdb", "DD", null),
    CS("CS", "docdb", "CS", null),
    GR("GR", "docdb", "GR", null),
    PT("PT", "docdb", "PT", null),
    UA("UA", "docdb", "UA", null),
    AR("AR", "docdb", "AR", null),
    HK("HK", "docdb", "HK", null),
    NZ("NZ", "docdb", "NZ", null),
    IE("IE", "docdb", "IE", null),
    CZ("CZ", "docdb", "CZ", null),
    SG("SG", "docdb", "SG", null),
    RO("RO", "docdb", "RO", null),
    YU("YU", "docdb", "YU", null),
    EA("EA", "docdb", "EA", null),
    PE("PE", "docdb", "PE", null),
    MY("MY", "docdb", "MY", null),
    BG("BG", "docdb", "BG", null),
    HR("HR", "docdb", "HR", null),
    SK("SK", "docdb", "SK", null),
    PH("PH", "docdb", "PH", null),
    SI("SI", "docdb", "SI", null),
    CO("CO", "docdb", "CO", null),
    CL("CL", "docdb", "CL", null),
    MA("MA", "docdb", "MA", null),
    ID("ID", "docdb", "ID", null),
    EG("EG", "docdb", "EG", null),
    OA("OA", "docdb", "OA", null),
    AP("AP", "docdb", "AP", null),
    RS("RS", "docdb", "RS", null),
    EC("EC", "docdb", "EC", null),
    UY("UY", "docdb", "UY", null),
    IS("IS", "docdb", "IS", null),
    LV("LV", "docdb", "LV", null),
    LT("LT", "docdb", "LT", null),
    CY("CY", "docdb", "CY", null),
    CR("CR", "docdb", "CR", null),
    CU("CU", "docdb", "CU", null),
    GT("GT", "docdb", "GT", null),
    DO("DO", "docdb", "DO", null),
    SM("SM", "docdb", "SM", null),
    MC("MC", "docdb", "MC", null),
    ZM("ZM", "docdb", "ZM", null),
    ZW("ZW", "docdb", "ZW", null),
    PA("PA", "docdb", "PA", null),
    SV("SV", "docdb", "SV", null),
    HN("HN", "docdb", "HN", null),
    DZ("DZ", "docdb", "DZ", null),
    KE("KE", "docdb", "KE", null),
    MW("MW", "docdb", "MW", null),
    MT("MT", "docdb", "MT", null),
    TJ("TJ", "docdb", "TJ", null),
    GC("GC", "docdb", "GC", null),
    ME("ME", "docdb", "ME", null),
    MN("MN", "docdb", "MN", null),
    BA("BA", "docdb", "BA", null),
    VN("VN", "docdb", "VN", null),
    NI("NI", "docdb", "NI", null),
    TR("TR", "docdb", "TR", null),
    TL("TL", "docdb", "TL", null),
    TN("TN", "docdb", "TN", null),
    GE("GE", "docdb", "GE", null),
    MD("MD", "docdb", "MD", null),
    KZ("KZ", "docdb", "KZ", null),
    JO("JO", "docdb", "JO", null),
    LU("LU", "docdb", "JO", null),
    EE("EE", "docdb", "EE", null),
    SA("SA", "docdb", "SA", null),
    BY("BY", "docdb", "BY", null),
    UZ("UZ", "docdb", "UZ", null),
    KG("KG", "docdb", "KG", null),
    AM("AM", "docdb", "AM", null),
    TT("TT", "docdb", "TT", null),
    FA("FA", "docdb", "FA", null),
    MO("MO", "docdb", "MO", null),
    XH("XH", "docdb", "XH", null),
    TH("TH", "docdb", "TH", null);


    @Getter
    private String word;

    @Getter
    private String index;

    @Getter
    private String desc;

    @Getter
    private Integer code;

    CountryEnum(String word, String index, String desc, Integer code) {
        this.word = word;
        this.index = index;
        this.desc = desc;
        this.code = code;
    }

    public static String[] getAllIndex() {
        String index[] = new String[CountryEnum.values().length]; int idx = 0;
        for(CountryEnum countryEnum : CountryEnum.values())
            index[idx++] = countryEnum.getIndex();

        return index;
    }

    public static CountryEnum getEnumByIndex(String index) {
        for(CountryEnum countryEnum : CountryEnum.values())
            if(countryEnum.getIndex().replaceAll("(_v1|_v2)","").equalsIgnoreCase(index))
                return countryEnum;

        return null;
    }

    public static String getDocdbEnumCheck(Object index) {
        for(CountryEnum countryEnum : CountryEnum.values()) {
            if (!index.equals(CountryEnum.DOCDB))
                return countryEnum.getWord();
        }

        return null;
    }

}
