package es.model.v1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatstatFamilyVO {
    private String country;
    private String applicationNumber;
    private String appln_nr_epodoc;
    private String appln_nr;
    private String documentType;
    private String docdbFamilyCount;
    private String reg_phase;
    private String docdbFamilyCitationCount;
    private String docdbFamilyId;
    private String inpadocFamilyId;

}
