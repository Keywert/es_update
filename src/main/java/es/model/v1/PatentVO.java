package es.model.v1;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.bulk.Claim;
import es.model.bulk.SuitInfoVO;
import es.model.enums.LegalStatusEnum;
import es.module.utility.CountryCode;
import es.module.utility.NumberUtil_Priority;
import es.module.utility.PriorityUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentVO {
    public HashSet<String> citationNumbers;
    public String citationRegisterNumber;
    public String citationDocumentId;                  // 문서 ID
    public String documentId;                  // 문서 ID
    public String publishingORG;               // 발간청
    public String countryCode;                 // - docdb 국가
    public String openNumber;                  // 공개번호
    public String publicationNumber;           // (openNumber가 없을 시)공개번호
    public String registerNumber;              // 등록번호f
    public String registerDate;                // 등록일자
    public String openDate;                   // 공개일자(To)
    public String publicationDate;            // 공개일자(To)
    public String documentType;                // 문헌종류
    public String documentTypeJP;              // 문헌종류 일본만
    public String documentTypeUS;              // 문헌종류 일본만



    public String applicationType;
    public String epoPublicationNumber;
    @JsonDeserialize(using = ObjectDeserializer.class)
    public String originalApplicationNumber;
    public String originalApplicationDate;
    public String originalApplicationKind;
    public String documentNumber;
    public String applicationCountryCode;
    public String standardApplicationNumber;
    public String familyId;
    public String epoApplicationNumber;

    public String getOriginalApplicationNumber() {
        if (this.documentId != null) {
            if (this.getDocumentId().matches("^us.+"))
                return null;
        }

        if (this.originalApplicationNumber != null)
            return originalApplicationNumber.replaceAll("-","");

        return originalApplicationNumber;
    }

    public String applicationNumber;           // 번호
    public String applicationDate;            // 출원일

    public SuitInfoVO suitInfo;

    public String patentId;
    public String patentType;
    public String techIpc;


    /**
     * 추가 (한국 일본) 등록 상태 에서만 적용되는 번호 및 일자 개념
     **/
    public String announcementNumber;          // 공고 번호
    public String announcementDate;            // 공고 일자

    public String inventionTitleJP;            // 발명의 일본문헌
    public String inventionTitleCN;            // 발명의 중국문헌
    public String inventionTitleKO;            // 발명의 중국문헌 / 번역

    private String internationalApplicationNumber;      // 국제 출원번호
    private String internationalApplicationDate;        // 국제 출원일자
    private String internationalPublicationNumber;        // 국제 공개번호
    private String internationalPublicationDate;          // 국제 공개일자

    private String inventionTitle;                        // 발명의 명칭

    private List<CitationDoc> citationDoc;      // 인용문헌 (number, country)


    public String mainIpc;                     // IPC (Main)
    public String mainUpc;                     // UPC (Main)
    public String mainCpc;                     // CPC (Main)
    public List<String> subIpc;                // IPC (Sub)
    public List<String> subCpc;                // CPC (Sub)
    public List<String> subUpc;                // CPC (Sub)

    public String mainFi;                      // mainFI(JP)
    public List<String> subFi;                // IPC (Sub)

    @JsonDeserialize(using = FtermDeseralizer.class)
    public List<String> fTerm;                 // fTerm(JP)
    public List<String> themeCode;             // 테마코드(JP)

    public List<Applicant> applicantInfo;          // 출원인
    public String applicantNames;
    public String applicantCount;
    public String applicantCountry;
    public String applicantCountryCount;
    public String applicantLocalStatus;
    public String applicantAddress;


    public List<RndInfo> rndInfo;             // 테마코드(JP)

    public String legalStatus;

    public String getLegalStatus() {
        if (legalStatus == null)
            return legalStatus;

        LegalStatusEnum legalStatusEnum = LegalStatusEnum.findByLegalEnum(legalStatus);

        if (legalStatusEnum != null && !legalStatusEnum.equals(LegalStatusEnum.EMPTY))
            return legalStatusEnum.getDescKo();

        return legalStatus;
    }


    @JsonDeserialize(using = FtermDeseralizer.class)
    private List<String> summary;

    public String getSummaryText(){
        if (summary == null)
            return null;
        String text = summary.get(0);

        return text.replace(System.getProperty("line.separator"), "")
                .replaceAll(">\\s*<", "><")
                .replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z|\\-]*=[^>]*)?(\\s)*(/)?>", "")
                .replaceAll("\\s+", " ")
                .trim();
    }

    public List<Applicant> currentApplicantInfo;   // 최근 출원인
    public List<Applicant> rightHolderInfo;

    public List<Applicant> inventorInfo;         // 미국 출원인 - 권리자
    public String inventorNames;
    public String inventorCount;
    public String inventorCountry;
    public String inventorCountryCount;

    public List<Applicant> currentRightHolderInfo;    // 현재 권리인 ver revision 1.3.7
    public String currentRightHolderNames;
    public String currentRightHolderAddress;


    public List<AssigneeInfo> assigneeInfo;         // 미국 출원인 - 권리자

    public String claimCount;                  // 청구항 갯수
    public List<Claim> claims;                 // 청구범위 (independentClaimText, claimText)

    public String getClaimsText() {
        if (this.claims == null)
            return null;

        ArrayList<String> claimsTextList = new ArrayList<>();

        for (Claim claim : claims) {
            String claimText = claim.getClaimText();
            if (claimText == null || "".equals(claimText))
                continue;

            if (claimText.matches("(삭제)"))
                continue;

            claimsTextList.add(claim.getClaimNumber() + ". "+ claimText);

        }

        if (!claimsTextList.isEmpty())
            return StringUtils.join(claimsTextList," ");

        return null;
    }

    public Boolean examinationRequestStatus;   // 심사청구 상태
    public String examinationRequestDate;      // 심사청구 일자

    public String translationSubmitDate;

    public List<PriorityClaim> priorityClaims;             // 우선권 관련정보
    public String priorityClaimCountry;
    public String priorityClaimNumber;
    public String priorityClaimDate;
    public String priorityClaimMasterNumber;
    public PriorityClaim earliestPriorityClaim;

    // 피인용 심사관여부
    public Integer examinerCitationState;

    private String [] description;


    public String docdbFamilyId;
    public String inpadocFamilyId;
    public String extendedFamilyId;


    public void setPriorityClaimInfos() {
        if (priorityClaims == null)
            return;

        ArrayList<String> numberList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        ArrayList<String> dateList = new ArrayList<>();
        ArrayList<String> masterNumberList = new ArrayList<>();

        for (PriorityClaim priorityClaim : priorityClaims) {
            String num = priorityClaim.getPriorityApplicationNumber();
            String country = priorityClaim.getPriorityApplicationCountry();
            String date = priorityClaim.getPriorityApplicationDate();

            if (date != null)
                date = date.replaceAll("[^0-9]", "");

            country = PriorityUtil.countryNormalize(country);
            String countryCode = CountryCode.countryCode.get(country);

            if ( countryCode != null )
                country = countryCode;

            if (country != null)
                country = country.replaceAll("[^A-z]", "");

            if (num != null)
                num = num.replaceAll("[\\s|,]", "");

            String number = NumberUtil_Priority.getNumber(country, num, "AN").replaceAll("\\.","");
//
//            if (country.matches("JP")) {
//                System.out.println(country + " : " + num + " -  " + number);
//            }

            priorityClaim.setPriorityApplicationNumber(number);
            priorityClaim.setPriorityApplicationCountry(country);
            numberList.add(number);
            countryList.add(country);
            dateList.add(date);

            masterNumberList.add("[" + country + "]" + number + "(" + date + ")");

        }

//        System.out.println(countryList);


        this.setPriorityClaimNumber(String.join(" | ", numberList));
        this.setPriorityClaimCountry(String.join(" | ", countryList));
        this.setPriorityClaimDate(String.join(" | ", dateList));
        this.setPriorityClaimMasterNumber(String.join(" | ", masterNumberList));


    }

    public String getApplicationNumber() {
        if (applicationNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + applicationNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + applicationNumber.replaceAll("-", "");

        return applicationNumber;
    }

    public String getRegisterNumber() {
        if (registerNumber == null)
            return null;

        if (documentId == null)
            return registerNumber;

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + registerNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + registerNumber.replaceAll("-", "");

        return registerNumber;
    }

    public String getOpenNumber() {
        if (openNumber == null)
            return null;

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + openNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + openNumber.replaceAll("-", "");

        return openNumber;
    }

    public String getPublicationNumber() {
        if (publicationNumber == null)
            return null;

        if (publicationNumber.matches("^(10|20)-[0-9]{4}-[0-9]{7}$"))
            return publicationNumber.replaceAll("-","");

        if (documentId.matches("^kr(\\d)+[a|b](\\d)*$"))
            return "10" + publicationNumber.replaceAll("-", "");
        else if (documentId.matches("^kr(\\d)+[u|y](\\d)*$"))
            return "20" + publicationNumber.replaceAll("-", "");

        return publicationNumber;
    }

    public void setcurrentApplicantInfos() {
        List<Applicant> applicants = currentApplicantInfo;

        if (applicants == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();
        ArrayList<String> localStatus = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();
        ArrayList<String> codes = new ArrayList<>();
        ArrayList<String> addressJP = new ArrayList<>();


        for (Applicant applicant : applicants) {
            String name = applicant.getOrgName();
            String country = applicant.getCountry();

            if (name == null)
                name = applicant.getName();

            if (name == null)
                continue;

            name = name.replaceAll("\\s+"," ");

            nameList.add(name);

            if (country != null) {
                countryList.add(country);
                countrySet.add(country);
            }

            if (country == null)
                countryList.add(null);

            if (applicant.getCountry() == null)
                localStatus.add(null);
            else if (applicant.getCountry().matches("KR"))
                localStatus.add("내국인");
            else
                localStatus.add("외국인");

            if (applicant.getAddress() != null) {
//                address.add(applicant.getAddress().split("\\s")[0].replaceAll(",", ""));
                address.add(applicant.getAddress());
            }

            if (applicant.getAddressJP() != null)
                addressJP.add(applicant.getAddressJP());

            String code = applicant.getCode();
            if (code != null)
                codes.add(code);

        }

        if (nameList == null)
            return;

        this.setApplicantNames(String.join(" | ", nameList));
        this.setApplicantCount(String.valueOf(applicants.size()));
        this.setApplicantCountry(String.join(" | ", countryList));
        this.setApplicantCountryCount(String.valueOf(countrySet.size()));
        this.setApplicantLocalStatus(String.join(" | ", localStatus));
        this.setApplicantAddress(String.join(" | ", address));
        this.setCurrentApplicantCodes(String.join(" | ",codes));

    }

    public String currentApplicantCodes;

    public void setApplicantInfos() {
        List<Applicant> applicants = null;

        if (applicantInfo != null) {
            applicants = applicantInfo;
        } else if (rightHolderInfo != null) {
            applicants = rightHolderInfo;
        } else if (currentApplicantInfo != null) {
            applicants = currentApplicantInfo;
        }

        if (applicants == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();
        ArrayList<String> localStatus = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();
        ArrayList<String> addressJP = new ArrayList<>();


        for (Applicant applicant : applicants) {
            String name = applicant.getOrgName();
            String country = applicant.getCountry();

            if (name == null)
                name = applicant.getName();

            if (name == null)
                continue;

            name = name.replaceAll("\\s+"," ");

            nameList.add(name);

            if (country != null) {
                countryList.add(country);
                countrySet.add(country);
            }

            if (country == null)
                countryList.add(null);

            if (applicant.getCountry() == null)
                localStatus.add(null);
            else if (applicant.getCountry().matches("KR"))
                localStatus.add("내국인");
            else
                localStatus.add("외국인");

            if (applicant.getAddress() != null) {
//                address.add(applicant.getAddress().split("\\s")[0].replaceAll(",", ""));
                address.add(applicant.getAddress());
            }

            if (applicant.getAddressJP() != null)
                addressJP.add(applicant.getAddressJP());

        }

        if (nameList == null)
            return;

        this.setApplicantNames(String.join(" | ", nameList));
        this.setApplicantCount(String.valueOf(applicants.size()));
        this.setApplicantCountry(String.join(" | ", countryList));
        this.setApplicantCountryCount(String.valueOf(countrySet.size()));
        this.setApplicantLocalStatus(String.join(" | ", localStatus));
        this.setApplicantAddress(String.join(" | ", address));
        this.setApplicantAddressJP(String.join(" | ", addressJP));

    }

    public void setCurrentRightHolderInfos() {
        List<Applicant> rightHolders = null;


        if (currentRightHolderInfo != null) {
            rightHolders = currentRightHolderInfo;
        } else if (rightHolderInfo != null) {
            rightHolders = rightHolderInfo;
        }

        if (rightHolders == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();


        for (Applicant applicant : rightHolders) {
            nameList.add(applicant.getName());

            if (applicant.getAddress() != null)
                address.add(applicant.getAddress().split("\\s")[0].replaceAll(",", ""));

        }

        if (nameList == null)
            return;

        this.setCurrentRightHolderNames(String.join(" | ", nameList));
        this.setCurrentRightHolderAddress(String.join(" | ", address));


    }

    String inventorAddress;
    String applicantAddressJP;

    public void setInventorInfos() {
        List<Applicant> inventors = null;

        if (inventorInfo != null) {
            inventors = inventorInfo;
        } else if (inventorInfo != null) {
            inventors = inventorInfo;
        }

        if (inventors == null)
            return;

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<String> countryList = new ArrayList<>();
        HashSet<String> countrySet = new HashSet<>();
        ArrayList<String> addrList = new ArrayList<>();


        for (Applicant applicant : inventors) {
            nameList.add(applicant.getName());
            countryList.add(applicant.getCountry());
            countrySet.add(applicant.getCountry());
            addrList.add(applicant.getAddress());

        }

        if (nameList == null)
            return;

        this.setInventorNames(String.join(" | ", nameList));
        this.setInventorAddress(String.join(" | ", addrList));
        this.setInventorCount(String.valueOf(inventors.size()));
        this.setInventorCountry(String.join(" | ", countryList));
        this.setInventorCountryCount(String.valueOf(countrySet.size()));
    }


    public String getIpcAll() {
        ArrayList<String> ipcs = new ArrayList<>();

        if (mainIpc != null)
            ipcs.add(mainIpc);

        if (subIpc != null)
            for (String ipc : subIpc)
                ipcs.add(ipc);

        if (ipcs.size() == 0)
            return null;

        return String.join(" | ", ipcs);
    }

    public String getCpcAll() {
        ArrayList<String> cpcs = new ArrayList<>();

        if (mainCpc != null)
            cpcs.add(mainIpc);

        if (subCpc != null)
            for (String ipc : subCpc)
                cpcs.add(ipc);

        if (cpcs.size() == 0)
            return null;

        return String.join(" | ", cpcs);
    }

    public String getPriorityClaimNumber() {
        ArrayList<String> numbers = new ArrayList<>();

        if (priorityClaims == null)
            return null;


        for (PriorityClaim priorityClaim : priorityClaims)
            numbers.add(priorityClaim.getPriorityApplicationNumber());

        return String.join(" | ", numbers);

    }


    public String getRepresentClaim() {
        if (claims == null || claims.isEmpty())
            return null;


        for (Claim claim : claims) {
            if (claim.getClaimState().matches("(I|U)"))
                return claim.getIndependentClaimText();
        }

        return null;
    }

    public String getRndInfoNames() {
        if (this.rndInfo == null)
            return null;

        ArrayList<String> names = new ArrayList<>();

        for (RndInfo rndInfo : this.rndInfo) {
            String name = rndInfo.getRndTaskName();
            if (names != null && !"".equals(name))
                names.add(name);

        }

        if (!names.isEmpty())
            return String.join(" | ", names);

        return null;
    }

    public String getRndInfoNumbers() {
        if (this.rndInfo == null)
            return null;

        ArrayList<String> names = new ArrayList<>();

        for (RndInfo rndInfo : this.rndInfo) {
            String name = rndInfo.getRndTaskNumber();
            if (names != null && !"".equals(name))
                names.add(name);
        }

        if (!names.isEmpty())
            return String.join(" | ", names);

        return null;
    }


    public String getPatentFlag() {
        if (this.patentType.matches("B"))
            return "특허등록";
        else if (this.patentType.matches("A"))
            return "특허공개";
        else if (this.patentType.matches("U"))
            return "실용공개";
        else if (this.patentType.matches("Y"))
            return "실용등록";

        return "특허공개";
    }
}
