package es.model.v1;


import com.fasterxml.jackson.annotation.*;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FamilyVO {

    @JsonProperty("countryCode")
    private String country;
    @JsonGetter("country")
    public String getCountry() {
        return country;
    }
    @JsonSetter("countryCode")
    public void setCountryCode(String country) {
        this.country = country;
    }
    @JsonSetter("country")
    public void setCountry(String country) {
        this.country = country;
    }

    private String applicationNumber;

    private String appln_nr_epodoc;
    @JsonGetter("appln_nr_epodoc")
    public String getAppln_nr_epodoc() {
        return appln_nr_epodoc;
    }
    @JsonSetter("appln_nr_epodoc")
    public void setAppln_nr_epodoc(String appln_nr_epodoc) {
        this.appln_nr_epodoc = appln_nr_epodoc;
    }
    @JsonSetter("epoApplicationNumber")
    public void setEpoApplicationNumber(String appln_nr_epodoc) {
        this.appln_nr_epodoc = appln_nr_epodoc;
    }


    private String epoPublicationNumber;

    private String appln_nr;


    private String documentType;
    private String docdbFamilyCount;
    private String reg_phase;
    private String docdbFamilyCitationCount;

    private String docdbFamilyId;
    @JsonGetter("docdbFamilyId")
    public String getDocdbFamilyId() {
        return docdbFamilyId;
    }
    @JsonSetter("docdbFamilyId")
    public void setDocdbFamilyId(String docdbFamilyId) {
        this.docdbFamilyId = docdbFamilyId;
    }
    @JsonSetter("familyId")
    public void setFamilyId(String docdbFamilyId) {
        this.docdbFamilyId = docdbFamilyId;
    }


    private String originalApplicationNumber;
    @JsonGetter("originalApplicationNumber")
    public String getOriginalApplicationNumber() {
        return originalApplicationNumber;
    }
    @JsonSetter("originalApplicationNumber")
    public void setOriginalApplicationNumber(String originalApplicationNumber) {
        this.originalApplicationNumber = originalApplicationNumber;
    }
    @JsonSetter("appln_nr_original")
    public void setAppln_nr_original(String originalApplicationNumber) {
        this.originalApplicationNumber = originalApplicationNumber;
    }


    private String inpadocFamilyId;

    private String publicationNumber;

}
