package es.model.v1;

public class CitationResult {
    public String documentId;
    public String patcit_b1_count;
    public String patcit_b1_number;
    public String npatcit_b1_count;
    public String examiner_patcit_b1_count;
    public String examiner_patcit_b1_number;

    public String patcit_f1_count;
    public String patcit_f1_number;
    public String self_patcit_f1_count;
    public String self_patcit_f1_number;
    public String examiner_self_patcit_f1_count;
    public String examiner_self_patcit_f1_number;
}
