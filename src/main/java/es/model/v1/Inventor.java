package es.model.v1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Inventor extends Person {
    String nameCN;
    String sequence;

    @Override
    public String getName() {
        if(super.getName() != null)
            return super.getName();

        return nameCN;
    }
}
