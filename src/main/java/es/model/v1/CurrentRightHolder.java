package es.model.v1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentRightHolder {
    private String name;
    private String address;
    private Integer sequence;
    private Integer seqNumber;
    private String name_origin;

    public String getName() {
        if(name != null)
            return name;

        if(name_origin != null)
            return name_origin;

        return name;
    }

}

