package es.model.v1;

import lombok.Data;

@Data
public class CitationVO {
    public String applicationNumber;
    public String documentId;
    public String startDocId;
    public String endDocId;
    public String citedType;
    public String citedPhase;
    public String examinerCitationState;
    public String country;
    public String documentNumber;
    public String documentType;
    public String text;
    public String publicationDate;
    public String patentNumber;
    public String status;
    public String self_status;
    public String applicant_start_code;
    public String applicant_end_code;
    public String openNumber;
    public String registerNumber;


    public String getNumber() {
        if (registerNumber != null && !registerNumber.isEmpty()) {
            return registerNumber;
        } else if (openNumber != null && !registerNumber.isEmpty()) {
            return openNumber;
        } else
            return applicationNumber;

    }

    public String getType() {
        if (registerNumber != null)
            return "B1";
        else
            return "A";
    }
}
