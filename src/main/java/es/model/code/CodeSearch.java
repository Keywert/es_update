package es.model.code;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.config.DBHelper;
import es.config.ESConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CodeSearch {
    public static void main(String[] args) {
        ESConfig esConfig = new ESConfig("es218");
        String index = "code_test2";
        String type = "patent";

        DBHelper dbHelper = new DBHelper();
//        Connection conn = dbHelper.getConn_local_158();
        Connection conn = dbHelper.getConn_local_205();

//        String tableName = "code.IPC_RECOMMEND_202401";
//        String sType = "ipc";

//        String tableName = "code.CPC_RECOMMEND_202401";
//        String sType = "cpc";

//        String tableName = "code.UPC_RECOMMEND_202401";
//        String sType = "upc";

//        String tableName = "code.FI_RECOMMEND_202401";
//        String sType = "fi";

        String tableName = "code.FTERM_RECOMMEND_202401";
                String sType = "fterm";


        System.out.println(tableName);

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
//                .filter(QueryBuilders.idsQuery().addIds("ipc_h01p_001_38"))
                .filter(QueryBuilders.matchQuery("type", sType))
//                .filter(QueryBuilders.prefixQuery("code","5"))
//                .filter(QueryBuilders.matchQuery("code_norm", "A43D-025/00"))
                ;


        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(300000))
                .setQuery(bqb)
                .setSize(5)
                .execute()
                .actionGet();

        System.out.println("Total Hit : " + scrollResp.getHits().getTotalHits());
        ObjectMapper objectMapper = new ObjectMapper();

//        String[] types = {"IPC", "CPC"};
        String[] types = {"IPC", "CPC", "FTERM", "FI", "UPC"};
//        String[] types = {"IPC"};

        long start = System.currentTimeMillis();

        int count = 0;
        try {
            do {
                for (SearchHit hit : scrollResp.getHits().getHits()) {
                    if (++count % 200 == 0) {
                        System.out.println(count + " : " + ((System.currentTimeMillis() - start) / 1000.0));
                        start = System.currentTimeMillis();
                    }
//                    if (count < 19000)
//                        continue;


                    TESTCodeVO code = objectMapper.readValue(hit.getSourceAsString(), TESTCodeVO.class);

                    List<TESTCodeVO> codeVOS = new ArrayList<>();
                    for (String targetType : types) {
                        List<TESTCodeVO> codes = esSearch(esConfig, index, type, code, targetType);

                        if (codes != null) {
//                            System.out.println(targetType + " : " + codes.size());
                            codeVOS.addAll(codes);
                        }
                    }

//                    System.out.println(objectMapper.writeValueAsString(codeVOS));

//                    System.out.println(" ES time : " + ((System.currentTimeMillis() - start) / 1000.0));
                    start = System.currentTimeMillis();

                    if (!codeVOS.isEmpty())
                        insertDB(conn, tableName, code, codeVOS);

//                    System.out.println(" DB time : " + ((System.currentTimeMillis() - start) / 1000.0));

                }
                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(300000)).execute().actionGet();
            } while (scrollResp.getHits().getHits().length != 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void insertDB(Connection conn, String tableName, TESTCodeVO code, List<TESTCodeVO> codeVOS) {
        String insertQuery = "INSERT ignore into " + tableName +
                "(" +
                "base, " +
                "code, " +
                "code_norm, " +
                "kind, " +
                "seq, " +
                "score " +
                ") " +
                "values(?,?,?,?,?,?) ";

        try {
            PreparedStatement statement = null;

            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            int seq = 0;
            for (TESTCodeVO codeVO : codeVOS) {
                statement.setString(1, code.getCode_norm());
                statement.setString(2, codeVO.getCode());
                statement.setString(3, codeVO.getCode_norm());
                statement.setString(4, codeVO.getType().toLowerCase());
                statement.setInt(5, codeVO.getSeq());
                statement.setFloat(6, codeVO.getScore());
                statement.addBatch();
            }

            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static List<TESTCodeVO> esSearch(ESConfig esConfig, String index, String type, TESTCodeVO codeVO, String targetType) {
        if (codeVO.getDesc_extend() == null)
            return null;

        String code_str = codeVO.getTechCode();

        String[] includes = {

        };

        String[] excludes = {
                "desc_extend", "desc_en", "desc_kr"
        };

        BoolQueryBuilder bqb = QueryBuilders.boolQuery()
                .filter(QueryBuilders.matchQuery("type", targetType))
                .mustNot(QueryBuilders.termsQuery("code_norm", codeVO.allCodes()))
                .mustNot(QueryBuilders.boolQuery()
                        .filter(QueryBuilders.matchQuery("type", codeVO.getType()))
                        .filter(QueryBuilders.prefixQuery("code_norm", code_str))
                )
                .must(QueryBuilders.boolQuery()
                        .should(QueryBuilders.matchQuery("desc_extend", codeVO.getDesc_extend()==null?"":codeVO.getDesc_extend()))
                        .should(QueryBuilders.matchQuery("desc_en", codeVO.getDesc_en()==null?"":codeVO.getDesc_en() ))
                );

//        System.out.println(bqb);

        SearchResponse scrollResp = esConfig.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//                .setScroll(new TimeValue(200000))
                .setQuery(bqb)
                .setSize(300)
                .setFetchSource(includes, excludes)
                .execute()
                .actionGet();

//        System.out.println("Total Hit : " + scrollResp.getHits().getTotalHits() + "  --  "+ scrollResp.getTook());
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            Map<String, TESTCodeVO> codeVOMap = new HashMap<>();

            int seq = 0;
//            do {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                TESTCodeVO code = objectMapper.readValue(hit.getSourceAsString(), TESTCodeVO.class);
                if (code == null)
                    continue;

                String techCode = code.getTechCode();
//                System.out.println(techCode);
                if (techCode == null)
                    continue;

                if (codeVOMap.get(techCode) == null) {
                    code.setSeq(++seq);
                    code.setScore(hit.getScore());
                    codeVOMap.put(techCode, code);
                }

                if (codeVOMap.size() > 11)
                    return new ArrayList<>(codeVOMap.values());

            }
//                scrollResp = esConfig.client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(200000)).execute().actionGet();
//            } while (scrollResp.getHits().getHits().length != 0);


            if (!codeVOMap.isEmpty())
                return new ArrayList<>(codeVOMap.values());


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
