package es.model.code;

import es.module.utility.CodeUtil;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class TESTCodeVO {
    public String code;
    public String code_norm;
    public String code_origin;
    public String desc_en;
    public String desc_extend;
    public String desc_kr;
    public String kind;
    public String level;
    public String type;
    public String version;
    public Float score;

    public Integer seq;

    public String getTechCode() {
        if (this.code != null){
            String code = this.getCode();

            if (this.getType().matches("^(IPC|FI|CPC)$")) {
                if (code.length() < 5)
                    return code;

                return code.substring(0, 4);
            }else if (this.getType().matches("^(UPC)$")) {
                if (code.length() < 4)
                    return code;

                return code.substring(0, 3);
            } else if (this.getType().matches("^(FTERM)$")) {
                if (code.length() < 6)
                    return code;

                return code.substring(0, 5);
            }

        }

        return techCode;
    }

    public String techCode;

    public Set<String> allCodes() {
        Set<String> set = new HashSet<>();

        if (this.code != null) {
            set.add(this.getCode());
            String ncode = null;
            if (this.getType().matches("^(IPC|FI|CPC)$")) {
                ncode = CodeUtil.getIpc(this.code);
                if (ncode != null)
                    set.add(ncode);

                ncode = CodeUtil.getCpc(this.code);
                if (ncode != null)
                    set.add(ncode);
            }

//            if (ncode != null)
//                return ncode;
        }
//        return code;

        if (!set.isEmpty())
            return set;

        return null;
    }
}
