package es.model.code;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import es.model.bulk.ArrayDeserializer;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatentCodeVO {
    public String documentId;

    public String mainIpc;                     // IPC (Main) 최초

    public String getOriginMainIpc() {
        if (this.mainIpc != null) {
            return this.getMainIpc().replaceFirst("-[0]{1,2}"," ");
        }

        return originMainIpc;
    }

    public String originMainIpc;                     // IPC (Main) 원형
    public String cmainIpc;                     // IPC (Main) 현재
    public String originCmainIpc;                     // IPC (Main) 현재 원형

    public String mainUpc;                     // UPC (Main)
    public String cmainUpc;                     // UPC (Main)

    public String mainCpc;                     // CPC (Main)
    public String originMainCpc;                     // CPC (Main) 원형
    public String cmainCpc;                     // CPC (Main)
    public String originCmainCpc;                     // CPC (Main) 현재 원



    public List<String> subIpc;                // IPC (Sub)
    public List<String> subCpc;                // CPC (Sub)
    public List<String> subUpc;                // CPC (Sub)

    public String mainFi;                      // mainFI(JP)

    @JsonDeserialize(using = ArrayDeserializer.class)
    public List<String> fTerm;                 // fTerm(JP)
    public List<String> themeCode;             // 테마코드(JP)

    // ipc 정보 최초, , 현재
    public List<CodeVO> ipcInfo;
    public List<CodeVO> hipcInfo;
    public List<CodeVO> cipcInfo;

    // cpc 정보 최초, 중간, 현재
    public List<CodeVO> cpcInfo;
    public List<CodeVO> hcpcInfo;
    public List<CodeVO> ccpcInfo;

    public String ipcVersion;
    public String ipcVersionDate;
    public String techIpc;
    public String cpcVersionDate;

    public String mainIpcLevel;
    public String mainIpcTest;
    public String mainIpcValue;
    public String mainIpcVersion;

    public String patentDate;


    public void removeUnuseField() {
        this.setDocumentId(null);
        this.setPatentDate(null);
    }
}
