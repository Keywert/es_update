package es.model.code;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class USCpcVO {
    public String txt;

    public String getDocumentType() {
        if (txt != null)
            return txt.substring(0, 2).trim();

        return documentType;
    }

    public String documentType;

    public String getApplicationNumber() {
        if (txt != null)
            return txt.substring(2, 10);

        return applicationNumber;
    }

    public String applicationNumber;

    public String getPublicationNumber() {
        if (txt.length() == 48) {
            return txt.substring(10, 18).trim();
        } else if (txt.length() == 51) {
            return txt.substring(10, 14) + "-" + txt.substring(14, 21);
        }

        return publicationNumber;
    }

    public String publicationNumber;

    public String getCode() {
        String str = "";
        if (txt.length() == 48) {
            str = txt.substring(18, 32).trim().replace(" ", "0");
        } else if (txt.length() == 51) {
            str = txt.substring(21, 35).trim().replace(" ", "0");
        }
        if (!str.isEmpty())
            return str.substring(0, 4) + "-" + str.substring(4);

        return code;
    }

    public String code;

    public String getDate() {
        if (txt.length() == 48) {
            return txt.substring(33, 41);
        } else if (txt.length() == 51)
            return txt.substring(36, 44);
        return date;
    }

    public String date;

    public String getSymbolPositionCode() {
        if (txt.length() == 48) {
            return txt.substring(41, 42);
        } else if (txt.length() == 51)
            return txt.substring(44,45);

        return symbolPositionCode;
    }

    public String symbolPositionCode;

    public String getClassificationValueCode() {
        if (txt.length() == 48) {
            return txt.substring(42, 43);
        } else if (txt.length() == 51)
            return txt.substring(45,46);

        return classificationValueCode;
    }

    public String classificationValueCode;


}
