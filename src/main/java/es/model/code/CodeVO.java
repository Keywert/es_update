package es.model.code;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.module.utility.CodeUtil;
import lombok.Data;

import java.lang.reflect.Field;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CodeVO {
    public String type;   // ipc/cpc/fi/fterm 코드 종류
    public String rankNumber;
    public String actionDate;

    public String groupSeq;

    public String getVersion() {
        if (this.version != null) {
            String v = this.version;
            v = v.replaceAll("[^0-9]", "");

            if (v.matches("^(19|20)[0-9]{2}[0-1][0-9]01$"))
                return v;
            else if (v.matches("^(19|20)[0-9]{2}[0-1][0-9]$"))
                return v + "01";
        }

        return version;
    }

    public String version;  // code 버전
    public String office;   // code 관리국
    public String status;
    public String dataSource;
    public String symbolPosition;
    public String value;
    public String level;
    public String edition;
    public String qualifyingCharacter;

    public String techCode;
    public String getTechCode() {
        if (this.getCode() != null) {
            String code = this.getCode();
            if (code.matches("^[A-Z][0-9]{2}[A-Z].*"))
                return code.substring(0, 4);
        }
        return level3;
    }

    public String getCode() {
        if (this.code != null)
            return code;

        if (this.originCode != null) {
            String ncode = null;
            if (this.getType().matches("^(ipc)$")) {
                ncode = CodeUtil.getIpc(this.originCode);
            } else if (this.getType().matches("^(cpc)$"))
                ncode = CodeUtil.getCpc(this.originCode);

            if (ncode != null)
                return ncode;
        }
        return code;
    }

    public String getLevel1() {
        if (this.getCode() != null) {
            String code = this.getCode();
            if (code.matches("^[A-Z][0-9]{2}[A-Z].*"))
                return code.substring(0, 1);
        }
        return level1;
    }

    public String getLevel2() {
        if (this.getCode() != null) {
            String code = this.getCode();
            if (code.matches("^[A-Z][0-9]{2}[A-Z].*"))
                return code.substring(0, 3);
        }
        return level2;
    }

    public String getLevel3() {
        if (this.getCode() != null) {
            String code = this.getCode();
            if (code.matches("^[A-Z][0-9]{2}[A-Z].*"))
                return code.substring(0, 4);
        }
        return level3;
    }

    public String getLevel4() {
        if (this.getCode() != null) {
            String code = this.getCode();
            if (code.matches("^[A-Z][0-9]{2}[A-Z]-[0-9]{3,4}/.*")) {
                String[] arr = code.split("/");
                if (arr.length == 2)
                    return arr[0].replace("-", "");

            }
        }
        return level4;
    }

    public String getLevel5() {
        if (this.getCode() != null) {
            String code = this.getCode();
            if (code.matches("^[A-Z][0-9]{2}[A-Z]-[0-9]{3,4}/.*")) {
                String[] arr = code.split("/");
                if (arr.length == 2)
                    return arr[0].replace("-", "") + arr[1];

            }
        }
        return level5;
    }

    public String level1;
    public String level2;
    public String level3;
    public String level4;
    public String level5;

    public String code;  // 코드(정규화) normalize code

    public String getOriginCode() {
        if (this.originCode == null) {
            if (this.code != null)
                return this.code.replaceFirst("-[0]{1,3}", " ");
        }

        return originCode;
    }

    public String originCode; // 코드(원본) original code

    public String seq;   // code 순서
    public String text;  // original text

    public String getSection() {
        if (this.section == null) {
            if (this.getCode() != null) {
                if (this.getCode().matches("^[A-Z][0-9]{2}[A-Z]\\-.+$"))
                    return this.getCode().substring(0, 1);
            }
        }

        return this.section;
    }

    public String section;

    public String getMainClass() {
        if (this.mainClass == null) {
            if (this.getCode() != null) {
                if (this.getCode().matches("^[A-Z][0-9]{2}[A-Z]\\-.+$"))
                    return this.getCode().substring(1, 3);
            }
        }
        return mainClass;
    }

    public String mainClass;

    public String getSubClass() {
        if (this.subClass == null) {
            if (this.getCode() != null) {
                if (this.getCode().matches("^[A-Z][0-9]{2}[A-Z]\\-.+$"))
                    return this.getCode().substring(3, 4);
            }
        }
        return subClass;
    }

    public String subClass;

    public String getMainGroup() {
        if (this.mainGroup == null) {
            if (this.getCode() != null) {
                if (this.getCode().matches("^[A-z][0-9]{2}[A-z]\\-[0-9]+/.+$")) {
                    String group = this.getCode().substring(5);
                    String[] arr = group.split("/");
                    if (arr.length == 2)
                        return arr[0].replaceAll("^0+", "");
                } else if (this.getCode().matches("^[A-z][0-9]{2}[A-z]\\-[0-9]$"))
                    return this.getCode().substring(4).trim();
            }
        }

        return mainGroup;
    }

    public String mainGroup;

    public String getSubGroup() {
        if (this.subGroup == null) {
            if (this.getCode() != null) {
                if (this.getCode().matches("^[A-z][0-9]{2}[A-z]\\-[0-9]+/.+$")) {
                    String group = this.getCode().substring(5);
                    String[] arr = group.split("/");
                    if (arr.length == 2)
                        return arr[1];
                }
            }
        }
        return subGroup;
    }

    public String subGroup;

    public boolean codeCompare(CodeVO codeVO) {
        if (this.getCode() != null && codeVO.getCode() != null)
            if (this.getCode().equals(codeVO.getCode()))
                return true;

        return false;
    }


    public static <T> T mergeObjects(T first, T second) {
        Class<?> clas = first.getClass();
        Field[] fields = clas.getDeclaredFields();
        Object result = null;
        try {
            result = clas.getDeclaredConstructor().newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value1 = field.get(first);
                Object value2 = field.get(second);
                Object value = (value1 != null) ? value1 : value2;
                field.set(result, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) result;
    }

    public void merge(CodeVO second) {
        Class<?> clas = this.getClass();
        Field[] fields = clas.getDeclaredFields();
        try {
            for (Field field : fields) {
//                System.out.println(field);
                field.setAccessible(true);
                Object value1 = field.get(this);
                Object value2 = field.get(second);
                Object value = null;
                if (field.toString().contains("version"))
                    value = (value2 != null) ? value2 : value1;
                else
                    value = (value1 != null) ? value1 : value2;

                field.set(this, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;

    }
}
