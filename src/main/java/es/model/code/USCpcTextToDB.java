package es.model.code;

import es.config.DBHelper;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class USCpcTextToDB {
    public static void main(String[] args) {
        String tableName = "code.uspto_cpc_pub_txt";
        DBHelper dbHelper = new DBHelper();
        Connection conn = dbHelper.getConn_local();

        String filePath = args[0];

        ArrayList<String> filepathList = getFilepathList(filePath);

        if (filepathList == null) {
            System.out.println("No Input Files~");
            return;
        }

        for (String path : filepathList) {
            System.out.println(path);

            txtToDB(conn, tableName, path);

        }
    }

    private static void txtToDB(Connection conn, String tableName, String filepath) {
        String encoding = "UTF-8";

        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(filepath), encoding));
            String line;
            List<Map<String, String>> list = new ArrayList<>();
//            line = in.readLine();
//            System.out.println(line);

            int count = 0;

            List<USCpcVO> usCpcVOS = new ArrayList<>();

            while ((line = in.readLine()) != null) {
                if (++count % 10000 == 0)
                    System.out.println(count);

//                if (count > 100)
//                    break;

                if (line.length() != 48 && line.length() != 51) {
                    System.out.println(filepath + " : " + line + " ---- " + line.length());
                }

                USCpcVO usCpc = new USCpcVO();
                usCpc.setTxt(line);

                usCpcVOS.add(usCpc);

                if (usCpcVOS.size() > 1000) {
                    insertDB(conn, tableName, usCpcVOS);
                    usCpcVOS.clear();
                }

//                System.out.println(new ObjectMapper().writeValueAsString(usCpc));


            }

            if (!usCpcVOS.isEmpty())
                insertDB(conn, tableName, usCpcVOS);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void insertDB(Connection conn, String tableName, List<USCpcVO> usCpcVOS) {
        String insertQuery = "INSERT ignore into " + tableName +
                "(" +
                "applicationNumber,publicationNumber,code,documentType,date" +
                ", symbolPositionCode,classificationValueCode,txt " +
                ") " +
                "values(?,?,?,?,?,?,?,?) ";

        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(insertQuery);
            statement.clearParameters();

            for(USCpcVO usCpcVO : usCpcVOS) {
                statement.setString(1, usCpcVO.getApplicationNumber().trim());
                statement.setString(2, usCpcVO.getPublicationNumber());
                statement.setString(3, usCpcVO.getCode());
                statement.setString(4, usCpcVO.getDocumentType());
                statement.setString(5, usCpcVO.getDate());
                statement.setString(6, usCpcVO.getSymbolPositionCode());
                statement.setString(7, usCpcVO.getClassificationValueCode());
                statement.setString(8, usCpcVO.getTxt());

                statement.addBatch();
            }

            int[] updateCounts = statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public static ArrayList getFilepathList(String filepath) {
        ArrayList arrayList = new ArrayList();

        File dir = new File(filepath);

        if (dir.exists() == false) {
            System.out.println("경로가 존재하지 않음.");
            return null;
        }

        visitDir(arrayList, dir);

        return arrayList;
    }

    public static void visitDir(ArrayList arrayList, File file) {
        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();

            for (File f : childFiles) {
                visitDir(arrayList, f);
            }
        } else {

            if (file.getName().matches("[0-9|A-z|-]*\\.[Ttx][Xxm][Ttl]")) {
                arrayList.add(file.getAbsolutePath());
            } else if (file.getName().matches(".+\\.[Ttx][Xxm][Ttl]"))
                arrayList.add(file.getAbsolutePath());


        }
    }

}
