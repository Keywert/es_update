package es.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import es.module.dataproc.norm.StdNameNorm;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper=false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person extends StdNameNorm {
    protected String key;
    protected String name;
    protected String nameCN;

    public String getFirstName() {
        if (this.firstName != null){
            String t_name = this.firstName.replaceAll("\\s+"," ").trim();
            if (t_name != null && !"".equals(t_name))
                return t_name;
            else
                return null;
        }
        return firstName;
    }

    protected String firstName;

    public String getLastName() {
        if (this.lastName != null){
            String t_name = this.lastName.replaceAll("\\s+"," ").trim();
            if (t_name != null && !"".equals(t_name))
                return t_name;
            else
                return null;
        }
        return lastName;
    }

    protected String lastName;
    protected String orgName;
    protected String orgNameCN;
    protected String orgNameCN_original;
    protected String orgName_docdb;
    protected String orgName_docdba;
    protected String orgName_origin;
    protected String orgName_origin_original;
    protected String orgName_original;

    protected String country;
    protected String country_docdb;
    protected String country_docdba;
    protected String country_original;

    protected String address;
    protected String registeredNumber;
    protected String addressJP;
    protected String nameJP;
    protected String name_origin;
    protected String enName;
    protected String originalName;
    protected String originalAddress;
    protected String nameEN;
    protected String addressEN;
    protected String applicantType;
    protected String applicantType_docdb;
    protected String applicantType_docdba;
    protected String applicantType_original;

    protected String codeCN;
    protected String codeEN;

    public String getMiddleName() {
        if (this.middleName != null){
            String t_name = this.middleName.replaceAll("\\s+"," ").trim();
            if (t_name != null && !"".equals(t_name))
                return t_name;
            else
                return null;
        }
        return middleName;
    }

    protected String middleName;
    protected String applicantAuthorityCategory;
    protected String citizenship;
    protected String designation;
    protected String email;
    protected String fax;
    protected String phone;
    protected String postalcode;
    protected String postcode;
    protected String prefix;
    protected String residenceCountry;
    protected String rights;
    protected String state;
    protected String suffix;
    protected String telephone;
    public String businessNumber;
    public String corporateNumber;
    public String code;
    public String type;

    public String nameKR;
    public String applicantCodeKR;
    public String applicantCodeJP;

    public String keywertCodeKR;
    public String keywertCodeEN;
    public String keywertCodeTL;

    public String nameKROrigin;
    public String nameENOrigin;
    public String nameJPOrigin;

    @JsonProperty("registered-number")
    public String registeredNumber2;

    @JsonProperty("orgname-standardized")
    protected String orgnameStandardized;

    @JsonProperty("orgname-normalized")
    protected String orgnameNormalized;

    @JsonProperty("orgname-normalized_lookup")
    protected String orgnameNormalizedLookup;

    @JsonProperty("orgname-standardized_lookup")
    protected String orgnameStandardizedLookup;

    protected String name_docdb;
    protected String name_docdba;

    protected String address_docdb;
    protected String address_docdba;
    protected String address_original;
    protected String addressCN;

    public String stdOrgName;
    public String key_lookup;
    public String orgName_lookup;

    public String getStdName() {
        return null;
    }

    public String stdName;
    public String stdNameKo;

    public String getEnName() {
        if (this.enName != null &&  !"".equals(this.enName) && !".".equals(this.enName) && !"-".equals(this.enName) && !",".equals(this.enName) && !"--".equals(this.enName) && !"..".equals(this.enName) && !"---".equals(this.enName) && !"----".equals(this.enName) && !"...".equals(this.enName) && !"|".equals(this.enName) && !"||||".equals(this.enName))
            return enName;

        return null;
    }

    public String getoName() {
//        if (this.oName == null) {
        if (this.getNameJP() != null)
            return this.getNameJP();
        if (this.getOrgNameCN() != null)
            return this.getOrgNameCN();
        else if (this.getNameCN() != null)
            return this.getNameCN();
        else if (this.getOriginalName() != null)
            return this.getOriginalName();
        else if (this.getOrgNameCN_original() != null)
            return this.getOrgNameCN_original();
        else if (this.getOriginalName() != null)
            return this.getOriginalName();
        else if (this.getOrgName_origin_original() != null)
            return this.getOrgName_origin_original();
            ///
        else if (this.getrName() != null)
            return this.getrName();
//    }

        return this.oName;
    }

    public String oName;

    public String getrName() {
//        if (this.rName == null) {
            String name = this.getOrgnameStandardized();
            if (name != null && !"".equals(name))
                return name;

            name = this.getOrgName();
            if (name != null && !"".equals(name))
                return name;

            name = this.getName();
            if (name != null && !"".equals(name))
                return name;

//        }
        return rName;
    }



    public String rName;

    public String getSrName() {
//        if (this.srName == null){
            String name = this.getrName();
            if (name != null && !"".equals(name)) {
                String t_name = this.stdNameProc(name);
                if (t_name != null && !"".equals(t_name))
                    return t_name;
                else
                    return null;
            }
//        }
        return srName;
    }

    public String srName;

    public String geteName() {
//        if (this.eName == null) {
            String name = this.getEnName();
            if (name != null && !"".equals(name))
                return name;

            name = this.getNameEN();
            if (name != null && !"".equals(name))
                return name;

            name = this.getNameENOrigin();
            if (name != null && !"".equals(name))
                return name;

//        }
        return eName;
    }

    public String eName;

    public String getSeName() {
//        if (this.seName == null){
            String name = this.geteName();
            if (name != null && !"".equals(name)){
                String t_name = this.stdNameProc(name);
                if (t_name != null && !"".equals(t_name))
                    return t_name;
                else
                    return null;
            }

//        }
        return seName;
    }

    public String seName;

    public String getName () {
        if (name != null)
            return this.name.replaceAll("\\s+"," ").trim();

        if (orgName != null)
            return orgName.replaceAll("\\s+"," ").trim();

        if (nameKR != null)
            return nameKR;

        if (enName != null)
            return enName;

//        if (name_origin != null)
//            return name_origin;

        if (name_docdb != null)
            return name_docdb;

        if (name_docdba != null)
            return name_docdba;

//        if (nameJP != null)
//            return nameJP;
//
//        if (orgNameCN != null)
//            return orgNameCN;
//
//        if (nameCN != null)
//            return nameCN;
//
//        if (originalName != null)
//            return originalName;
//
//        if (name_origin != null)
//            return name_origin;

        return null;
    }

    public String getAddress () {
        if (address != null)
            return address;

        if (originalAddress != null)
            return originalAddress;

        if (address_docdb != null)
            return address_docdb;

        if (address_docdba != null)
            return address_docdba;

        if (address_original != null)
            return address_original;

        return null;
    }


}
