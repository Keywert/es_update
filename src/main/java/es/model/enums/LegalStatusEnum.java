package es.model.enums;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum LegalStatusEnum {
    AP("공개", true, "publication", Arrays.asList("AP","000001", "000000", "공개"
            //US
            ,"1" ,"100" ,"11" ,"12" ,"145" ,"148" ,"157" ,"158" ,"159" ,"16" ,"17" ,"18" ,"19" ,"195" ,"197","20" ,"200"
            ,"201" ,"218" ,"25" ,"251" ,"252" ,"254" ,"256" ,"258" ,"259" ,"260" ,"264" ,"265","266" ,"27" ,"271" ,"275"
            ,"28" ,"280" ,"29" ,"30" ,"31" ,"310" ,"311" ,"32" ,"33" ,"37" ,"38","39" ,"551" ,"552" ,"554" ,"555" ,"556"
            ,"558" ,"559" ,"560" ,"566" ,"568" ,"569" ,"65" ,"66" ,"97"
            )),
//    AN("공고", true, "Announcement", Arrays.asList("AN", "공고")),
    AG("등록", true, "grant", Arrays.asList("AG","000003"
            //JP
            , "C03501", "C0360A01", "C078000", "000002"
            //KR
            , "등록"
            //US
            ,"150","153","410","412","414","416","418","420","421","422","429","430","450","451","452","454","50"
            ,"51","604","606","610","612","614","650","660","670","680","77","804","806","808","809","810","812","814"
            ,"816","818","820","822","94","95","98","99"
    )),
    ID("거절", false, "Decline", Arrays.asList("ID"
            //JP
            ,"C03502", "C0360A02" ,"C0360A06"
            //KR
            , "거절"
            //US
            ,"116","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","135","136","137","139","140","143"
            ,"144","149","432","433","434","435","436","437","438","439","440","441","443","444","445","499","60","61")),

    II("무효", false, "invalidity", Arrays.asList("II","C0360A31", "C0360A32", "C0360A33", "C0360A34", "C078004", "C078006", "C078014", "C078015", "무효"

    )),
    IT("소멸", false, "Termination", Arrays.asList("IT"
            //JP
            ,"000004","C078001", "C078002", "C078007", "C078008", "C078012", "C078013"
            //KR
            , "소멸" ,"공고"
            //US
            ,"151", "152" ,"250"
    )),
    EM("심사중", true, "Exam", Arrays.asList("EM"
            //JP
            ,"C03500"
            //KR
            , "심사중"
            //US
            ,"172" ,"174" ,"180" ,"181" ,"182" ,"40" ,"404" ,"406" ,"407" ,"408" ,"41" ,"423" ,"424" ,"425" ,"426" ,"427" ,"428" ,"71" ,"80" ,"82"
            ,"823" ,"824" ,"825" ,"826" ,"827" ,"83" ,"830" ,"833" ,"834" ,"837" ,"840" ,"841" ,"842" ,"843" ,"844" ,"845" ,"846" ,"847" ,"848"
            ,"849" ,"850" ,"852" ,"854" ,"857" ,"859" ,"860" ,"861" ,"862" ,"864" ,"865" ,"870" ,"871" ,"872" ,"873" ,"874" ,"877" ,"878" ,"879"
            ,"880" ,"881" ,"882" ,"883" ,"884" ,"885" ,"886" ,"887" ,"888" ,"889" ,"89" ,"890" ,"891" ,"892" ,"899" ,"90" ,"92" ,"93"
    )),
    GI("등록예정", true, "Intened Grant", Arrays.asList("GI", "등록예정"

    )),
    RV("취소", false, "Revoked", Arrays.asList("RV","C078005", "취소"

    )),
    IW("취하", false, "Withdrawal", Arrays.asList("IW"
            //JP
            ,"C0360A04", "C0360A07", "C0360A09", "C0360A10", "C0360A11", "C0360A13"
            //KR
            , "취하"
            //US
            , "221" ,"91")),
    IA("포기", false, "Abandonment", Arrays.asList("IA","C0360A05", "C078003", "포기"
    ,"160" ,"161" ,"162" ,"163" ,"164" ,"165" ,"166" ,"167" ,"168" ,"169" ,"170"
    )),
    DM("각하", false, "Dismissed", Arrays.asList("DM","C0360A21", "C0360A41", "C0360A42", "C0360A43", "C0360A44", "C0360A45", "C0360A46", "C0360A47","C0360A08", "각하")),
    EMPTY("없음", false, "empty", Collections.EMPTY_LIST);


    @Getter
    @Setter
    public String descEn;

    @Getter
    @Setter
    public String descKo;

    @Getter
    @Setter
    public boolean active = true;

    private List<String> codeList;


    LegalStatusEnum(String descKo, boolean active, String descEn, List codeList) {
        this.descKo = descKo;
        this.active = active;
        this.descEn = descEn;
        this.codeList = codeList;
    }

    public static LegalStatusEnum getDescKoByCode(String descKo) {
        for (LegalStatusEnum legalStatusEnum : LegalStatusEnum.values())
            if (legalStatusEnum.getDescKo().equalsIgnoreCase(descKo))
                return legalStatusEnum;

        return null;
    }

    public static String getCodeByDescKo(String code) {
        for (LegalStatusEnum legalStatusEnum : LegalStatusEnum.values())
            if (legalStatusEnum.name().equalsIgnoreCase(code))
                return legalStatusEnum.getDescKo();

        return null;
    }

    public static LegalStatusEnum getDescByKoCode(String code) {
        for (LegalStatusEnum legalStatusEnum : LegalStatusEnum.values())
            if (legalStatusEnum.getDescKo().equalsIgnoreCase(code))
                return legalStatusEnum;

        return null;
    }

    public static String getCodeByDescEnCode(String code) {
        for (LegalStatusEnum legalStatusEnum : LegalStatusEnum.values())
            if (legalStatusEnum.name().equalsIgnoreCase(code))
                return legalStatusEnum.getDescEn();

        return null;
    }

    public static boolean getCodeByActiveCode(String code) {
        for (LegalStatusEnum legalStatusEnum : LegalStatusEnum.values())
            if (legalStatusEnum.name().equalsIgnoreCase(code))
                return legalStatusEnum.isActive();

        return true;
    }

    public static LegalStatusEnum findByLegalEnum (String name) {
        return Arrays.stream(LegalStatusEnum.values())
                .filter(out -> out.hasResult(name))
                .findAny()
                .orElse(EMPTY);

    }

        public boolean hasResult(String result) {
        return codeList.stream()
                .anyMatch(out -> out.equals(result));
    }
}
