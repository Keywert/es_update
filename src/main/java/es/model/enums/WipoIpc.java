package es.model.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
public enum WipoIpc {
    CBC("C", "화학", "화학2" , "기초재료화학" , "C","Chemistry", "화학","Basic materials chemistry", "기초재료화학", Arrays.asList("기초재료화학","A01N" ,"A01P" ,"C05B" ,"C05C" ,"C05D" ,"C05F" ,"C05G" ,"C06B" ,"C06C" ,"C06D" ,"C06F" ,"C09B" ,"C09C" ,"C09D" ,"C09F" ,"C09G" ,"C09H" ,"C09J" ,"C09K" ,"C10B" ,"C10C" ,"C10F" ,"C10G" ,"C10H" ,"C10J" ,"C10K" ,"C10L" ,"C10M" ,"C10N" ,"C11B" ,"C11C" ,"C11D" ,"C99Z")),
    CBI("C", "화학", "바이오 기술, 의약2" , "생물학적 제재 제외" , "C","Chemistry", "화학","Biotechnology", "바이오기술", Arrays.asList("생물학적 제재 제외","C07G", "C07K", "C12M", "C12N", "C12P", "C12Q", "C12R", "C12S")),
    CCE("C", "화학", "화학2" , "화학공학" , "C","Chemistry", "화학","Chemical engineering", "화학공학", Arrays.asList("화학공학","B01B","B01D-001","B01D-003","B01D-005","B01D-007","B01D-008","B01D-009","B01D-011","B01D-012","B01D-015","B01D-017","B01D-019","B01D-021","B01D-024","B01D-025","B01D-027","B01D-029","B01D-033","B01D-035","B01D-036","B01D-037","B01D-039","B01D-041","B01D-043","B01D-057","B01D-059","B01D-061","B01D-063","B01D-065","B01D-067","B01D-069","B01D-071","B01F","B01J","B01L","B02C","B03B","B03C","B03D","B04B","B04C","B05B","B06B","B07B","B07C","B08B","C14C","D06B","D06C","D06L","F25J","F26B","H05H")),
    CET("C", "화학", "화학2" , "환경기술" , "C","Chemistry", "화학","Environmental technology", "환경기술", Arrays.asList("환경기술","A62C","B01D-045","B01D-046","B01D-047","B01D-049","B01D-050","B01D-051","B01D-052","B01D-053","B09B","B09C","B65F","C02F","E01F-008","F01N","F23G","F23J","G01T")),
    CFC("C", "화학", "화학2" , "식료품" , "C","Chemistry", "화학","Food chemistry", "식료품", Arrays.asList("식료품","A01H","A21D","A23B","A23C","A23D","A23F","A23G","A23J","A23K","A23L","C12C","C12F","C12G","C12H","C12J","C13B-010","C13B-020","C13B-030","C13B-035","C13B-040","C13B-050","C13B-099","C13D","C13F","C13J","C13K")),
    CMC("C", "화학", "화학2" , "고분자화학/폴리머" , "C","Chemistry", "화학","Macromolecular chemistry, polymers", "고분자화학/폴리머", Arrays.asList("고분자화학/폴리머","C08B" ,"C08C" ,"C08F" ,"C08G" ,"C08H" ,"C08K" ,"C08L")),
    CMM("P", "물리/재료", "화학1" , "재료/금속학" , "C","Chemistry", "화학","Materials, metallurgy", "재료/금속학", Arrays.asList("재료/금속학","B22C" ,"B22D" ,"B22F" ,"C01B" ,"C01C" ,"C01D" ,"C01F" ,"C01G" ,"C03C" ,"C04B" ,"C21B" ,"C21C" ,"C21D" ,"C22B" ,"C22C" ,"C22F")),
    CMN("P", "물리/재료", "화학1" , "마이크로구조/나노기술" , "C","Chemistry", "화학","Micro-structural and nano-technology", "마이크로구조/나노기술", Arrays.asList("마이크로구조/나노기술","B81B","B81C","B82B","B82Y")),
    COF("C", "화학", "화학2" , "유기화학" , "C","Chemistry", "화학","Organic fine chemistry", "유기정밀화학", Arrays.asList("유기화학","A61K-008","A61Q","C07B","C07C","C07D","C07F","C07H","C07J","C40B")),
    CPH("B", "바이오", "바이오 기술, 의약2" , "생물학적 제제관련 기술" , "C","Chemistry", "화학","Pharmaceuticals", "의약", Arrays.asList("생물학적 제제관련 기술","A61K-006","A61K-009","A61K-031","A61K-033","A61K-035","A61K-036","A61K-038","A61K-039","A61K-041","A61K-045","A61K-047","A61K-048","A61K-049","A61K-050","A61K-051","A61K-101","A61K-103","A61K-125","A61K-127","A61K-129","A61K-131","A61K-133","A61K-135","A61P")),
    CST("P", "물리/재료", "화학1" , "표면기술/코팅" , "C","Chemistry", "화학","Surface technology, coating", "표면기술/코팅", Arrays.asList("표면기술/코팅","B05C","B05D","B32B","C23C","C23D","C23F","C23G","C25B","C25C","C25D","C25F","C30B")),
    EAT("E", "전기/전자/IT", "전기/전자/IT" , "오디오/영상기술" , "E","Electrical engineering", "전기","Audio-visual technology", "오디오/영상기술", Arrays.asList("오디오/영상기술","G09F","G09G","G11B","H04N-003","H04N-005","H04N-007","H04N-009","H04N-011","H04N-013","H04N-015","H04N-017","H04N-019","H04N-101","H04R","H04S","H05K")),
    ECP("E", "전기/전자/IT", "전기/전자/IT" , "기본통신프로세스" , "E","Electrical engineering", "전기","Basic communication processes", "기본통신프로세스", Arrays.asList("기본통신프로세스","H03B","H03C","H03D","H03F","H03G","H03H","H03J","H03K","H03L","H03M")),
    ECT("E", "전기/전자/IT", "전기/전자/IT" , "컴퓨터기술" , "E","Electrical engineering", "전기","Computer technology", "컴퓨터기술", Arrays.asList("컴퓨터기술","G06C","G06D","G06E","G06F","G06G","G06J","G06K","G06M","G06N","G06T","G10L","G11C")),
    EDC("E", "전기/전자/IT", "전기/전자/IT" , "디지털통신" , "E","Electrical engineering", "전기","Digital communication", "디지털통신", Arrays.asList("디지털통신","H04L","H04N-021","H04W")),
    EEM("E", "전기/전자/IT", "전기/전자/IT" , "전기기계/에너지" , "E","Electrical engineering", "전기","Electrical machinery, apparatus, energy", "전기기계/에너지", Arrays.asList("전기기계/에너지","F21H","F21K","F21L","F21S","F21V","F21W","F21Y","H01B","H01C","H01F","H01G","H01H","H01J","H01K","H01M","H01R","H01T","H02B","H02G","H02H","H02J","H02K","H02M","H02N","H02P","H02S","H05B","H05C","H05F","H99Z")),
    EIM("E", "전기/전자/IT", "전기/전자/IT" , "전자상거래" , "E","Electrical engineering", "전기","IT methods for management", "전자상거래", Arrays.asList("전자상거래","G06Q")),
    ESE("E", "전기/전자/IT", "전기/전자/IT" , "반도체" , "E","Electrical engineering", "전기","Semiconductors", "반도체", Arrays.asList("반도체","H01L")),
    ETE("E", "전기/전자/IT", "전기/전자/IT" , "원거리통신" , "E","Electrical engineering", "전기","Telecommunications", "원거리통신", Arrays.asList("원거리통신","G08C","H01P","H01Q","H04B","H04H","H04J","H04K","H04M","H04N-001","H04Q")),
    IAB("B", "바이오", "기구3" , "생물물질분석" , "I","Instruments", "기구","Analysis of biological materials", "생물물질분석", Arrays.asList("생물물질분석","G01N-033")),
    ICO("I", "전기/전자/IT", "기구1" , "기구제어" , "I","Instruments", "기구","Control", "제어", Arrays.asList("기구제어","G05B","G05D","G05F","G07B","G07C","G07D","G07F","G07G","G08B","G08G","G09B","G09C","G09D")),
    IME("I", "물리/재료", "기구2" , "측정" , "I","Instruments", "기구","Measurement", "측정", Arrays.asList("측정","G01B","G01C","G01D","G01F","G01G","G01H","G01J","G01K","G01L","G01M","G01N-001","G01N-003","G01N-005","G01N-007","G01N-009","G01N-011","G01N-013","G01N-015","G01N-017","G01N-019","G01N-021","G01N-022","G01N-023","G01N-024","G01N-025","G01N-027","G01N-029","G01N-030","G01N-031","G01N-035","G01N-037","G01P","G01Q","G01R","G01S","G01V","G01W","G04B","G04C","G04D","G04F","G04G","G04R","G12B","G99Z")),
    IMT("B", "바이오", "기구3" , "의료기술" , "I","Instruments", "기구","Medical technology", "의료기술", Arrays.asList("의료기술","A61B","A61C","A61D","A61F","A61G","A61H","A61J","A61L","A61M","A61N","H05G","G16H")),
    IOP("P", "물리/재료", "기구2" , "광학" , "I","Instruments", "기구","Optics", "광학", Arrays.asList("광학","G02B","G02C","G02F","G03B","G03C","G03D","G03F","G03G","G03H","H01S")),
    MEP("M", "기계", "기계" , "엔진/펌프/터빈" , "M","Mechanical engineering", "기계","Engines, pumps, turbines", "엔진/펌프/터빈", Arrays.asList("엔진/펌프/터빈","F01B","F01C","F01D","F01K","F01L","F01M","F01P","F02B","F02C","F02D","F02F","F02G","F02K","F02M","F02N","F02P","F03B","F03C","F03D","F03G","F03H","F04B","F04C","F04D","F04F","F23R","F99Z","G21B","G21C","G21D","G21F","G21G","G21H","G21J","G21K")),
    MHA("M", "기계", "기계" , "기계조작" , "M","Mechanical engineering", "기계","Handling", "기계조작", Arrays.asList("기계조작","B25J","B65B","B65C","B65D","B65G","B65H","B66B","B66C","B66D","B66F","B67B","B67C","B67D")),
    MAT("M", "기계", "기계" , "공작기계" , "M","Mechanical engineering", "기계","Machine tools", "공작기계", Arrays.asList("공작기계","A62D","B21B","B21C","B21D","B21F","B21G","B21H","B21J","B21K","B21L","B23B","B23C","B23D","B23F","B23G","B23H","B23K","B23P","B23Q","B24B","B24C","B24D","B25B","B25C","B25D","B25F","B25G","B25H","B26B","B26D","B26F","B27B","B27C","B27D","B27F","B27G","B27H","B27J","B27K","B27L","B27M","B27N","B30B")),
    MAE("M", "기계", "기계" , "기계요소" , "M","Mechanical engineering", "기계","Mechanical elements", "기계요소", Arrays.asList("기계요소","F15B","F15C","F15D","F16B","F16C","F16D","F16F","F16G","F16H","F16J","F16K","F16L","F16M","F16N","F16P","F16S","F16T","F17B","F17C","F17D","G05G")),
    MOS("M", "기계", "기계" , "기타특수기계" , "M","Mechanical engineering", "기계","Other special machines", "기타특수기계", Arrays.asList("기타특수기계","A01B","A01C","A01D","A01F","A01G","A01J","A01K","A01L","A01M","A21B","A21C","A22B","A22C","A23N","A23P","B02B","B28B","B28C","B28D","B29B","B29C","B29D","B29K","B29L","B33Y","B99Z","C03B","C08J","C12L","C13B-005","C13B-015","C13B-025","C13B-045","C13C","C13G","C13H","F41A","F41B","F41C","F41F","F41G","F41H","F41J","F42B","F42C","F42D","A41H")),
    MTE("M", "기계", "기계" , "섬유/제지기계" , "M","Mechanical engineering", "기계","Textile and paper machines", "섬유/제지기계", Arrays.asList("섬유/제지기계","A41H","A43D","A46D","B31B","B31C","B31D","B31F","B41B","B41C","B41D","B41F","B41G","B41J","B41K","B41L","B41M","B41N","C14B","D01B","D01C","D01D","D01F","D01G","D01H","D02G","D02H","D02J","D03C","D03D","D03J","D04B","D04C","D04G","D04H","D05B","D05C","D06G","D06H","D06J","D06M","D06P","D06Q","D21B","D21C","D21D","D21F","D21G","D21H","D21J","D99Z")),
    MTP("M", "기계", "기계" , "열처리/장치" , "M","Mechanical engineering", "기계","Thermal processes and apparatus", "열처리장치", Arrays.asList("열처리/장치","F22B","F22D","F22G","F23B","F23C","F23D","F23H","F23K","F23L","F23M","F23N","F23Q","F24B","F24C","F24D","F24F","F24H","F24J","F24S","F24T","F24V","F25B","F25C","F27B","F27D","F28B","F28C","F28D","F28F","F28G")),
    MTR("M", "기계", "기계" , "운송" , "M","Mechanical engineering", "기계","Transport", "운송", Arrays.asList("운송","B60B","B60C","B60D","B60F","B60G","B60H","B60J","B60K","B60L","B60M","B60N","B60P","B60Q","B60R","B60S","B60T","B60V","B60W","B61B","B61C","B61D","B61F","B61G","B61H","B61J","B61K","B61L","B62B","B62C","B62D","B62H","B62J","B62K","B62L","B62M","B63B","B63C","B63G","B63H","B63J","B64B","B64C","B64D","B64F","B64G")),
    OCE("M", "기계", "기타" , "토목공학" , "O","Other fields", "기타","Civil engineering", "토목공학", Arrays.asList("토목공학","E01B","E01C","E01D","E01F-001","E01F-003","E01F-005","E01F-007","E01F-009","E01F-011","E01F-013","E01F-015","E01H","E02B","E02C","E02D","E02F","E03B","E03C","E03D","E03F","E04B","E04C","E04D","E04F","E04G","E04H","E05B","E05C","E05D","E05F","E05G","E06B","E06C","E21B","E21C","E21D","E21F","E99Z")),
    OFG("M", "기계", "기타" , "가구/게임" , "O","Other fields", "기타","Furniture, games", "가구/게임", Arrays.asList("가구/게임","A47B","A47C","A47D","A47F","A47G","A47H","A47J","A47K","A47L","A63B","A63C","A63D","A63F","A63G","A63H","A63J","A63K")),
    OCG("M", "기계", "기타" , "기타소비재물품" , "O","Other fields", "기타","Other consumer goods", "기타소비재물품", Arrays.asList("기타소비재물품","A24B","A24C","A24D","A24F","A41B","A41C","A41D","A41F","A41G","A42B","A42C","A43B","A43C","A44B","A44C","A45B","A45C","A45D","A45F","A46B","A62B","A99Z","B42B","B42C","B42D","B42F","B43K","B43L","B43M","B44B","B44C","B44D","B44F","B68B","B68C","B68F","B68G","D04D","D06F","D06N","D07B","F25D","G10B","G10C","G10D","G10F","G10G","G10H","G10K")),
    ONC("M", "기계", "기타" , "기타(미분류)" , "O","Other fields", "기타","Other Non Classification", "기타(미분류)", Arrays.asList("기타(미분류)","A24B")),
    EMPTY(null,null,null,null,null,null,null,null,null,Collections.EMPTY_LIST);

    private String class1;
    private String class2;
    private String class1_name;
    private String class2_name;
    private String class3_name;

    private String sector_code;
    private String sector_en;
    private String sector_kr;
    private String field_en;
    private String field_kr;

    private List<String> ipcList;



    WipoIpc(String class1,String class1_name, String class2_name,String class3_name,
            String sector_code, String sector_en, String sector_kr, String field_en, String field_kr, List<String> ipcList) {
        this.class1 = class1;
        this.class2 = sector_code;
        this.class1_name = class1_name;
        this.class2_name = class2_name;
        this.class3_name = class3_name;
        this.sector_code = sector_code;
        this.sector_en = sector_en;
        this.sector_kr = sector_kr;
        this.field_en = field_en;
        this.field_kr = field_kr;

        this.ipcList =ipcList;
    }

    public static WipoIpc findByWipoIpc (String ipc) {
        return Arrays.stream(WipoIpc.values())
                .filter(out -> out.hasResult(ipc))
                .findAny()
                .orElse(ONC);
    }

    public static WipoIpc findByFieldName (String name) {
        return Arrays.stream(WipoIpc.values())
                .filter(out -> out.hasName(name))
                .findAny()
                .orElse(ONC);
    }

    private boolean hasName(String name) {
        String temp = name.replaceAll("\\(","\\(").replaceAll("\\)","\\)");
        return ipcList.stream()
                .anyMatch(out -> name.matches(out));
    }


    public boolean hasResult(String ipc) {
        return ipcList.stream()
                .anyMatch(out -> ipc.matches(out+".*"));
    }

}
