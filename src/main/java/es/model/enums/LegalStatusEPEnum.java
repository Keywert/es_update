package es.model.enums;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum LegalStatusEPEnum {
    AP("공개", true, "publication", Arrays.asList("17","16","0")),
//    AN("공고", true, "Announcement", Arrays.asList()),
    AG("등록", true, "grant", Arrays.asList("255","8","7","6","5","3","2")),
    ID("거절", false, "Decline", Arrays.asList("11")),
//    II("무효", false, "invalidity", Arrays.asList()),
//    IT("소멸", false, "Termination", Arrays.asList()),
    EM("심사중", true, "Exam", Arrays.asList("14","15")),
    GI("등록예정", true, "Intened Grant", Arrays.asList("12")),
    RV("취소", false, "Revoked", Arrays.asList("4","1")),
    IW("취하", false, "Withdrawal", Arrays.asList("13","10","9")),
//    IA("포기", false, "Abandonment", Arrays.asList()),
//    DM("각하", false, "Dismissed", Arrays.asList()),
    EMPTY("없음", false, "empty", Collections.EMPTY_LIST);


    @Getter
    @Setter
    public String descEn;

    @Getter
    @Setter
    public String descKo;

    @Getter
    @Setter
    public boolean active = true;

    private List<String> codeList;


    LegalStatusEPEnum(String descKo, boolean active, String descEn, List codeList) {
        this.descKo = descKo;
        this.active = active;
        this.descEn = descEn;
        this.codeList = codeList;
    }


    public static LegalStatusEPEnum findByLegalEnum (String code) {
        return Arrays.stream(LegalStatusEPEnum.values())
                .filter(out -> out.hasResult(code))
                .findAny()
                .orElse(EMPTY);

    }

    public boolean hasResult(String result) {
        return codeList.stream()
                .anyMatch(out -> out.equals(result));
    }
}
