package es.model.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
public enum  IndexEnum {
    kipo("한국", Arrays.asList("KR","한국")),
    uspto("미국", Arrays.asList("US","미국")),
    epo("유럽", Arrays.asList("EP","유럽")),
    jpo("일본", Arrays.asList("JP","일본")),
    sipo("중국", Arrays.asList("CN","중국")),
    dpma("독일", Arrays.asList("DE","독일")),
    pct("WO", Arrays.asList("WO","PCT")),
    EMPTY(null,Collections.EMPTY_LIST);

    private String countryName;

    private List<String> countryList;

    IndexEnum(String countryName, List<String> countryList) {
        this.countryName = countryName;

        this.countryList =countryList;
    }

    public static IndexEnum findByCountry (String country) {
        return Arrays.stream(IndexEnum.values())
                .filter(out -> out.hasResult(country))
                .findAny()
                .orElse(EMPTY);
    }

    public boolean hasResult(String country) {
        return countryList.stream()
                .anyMatch(out -> country.matches(out));
    }

}
