package es.model.enums;

import lombok.Getter;

public enum FirstActionTypesEnum {
    IC("침해 소송", "Infringement related actions cases"),
    TC("무효 소송", "Third party validity challenge related cases"),
    OC("기타 소송", "Other cases"),
    UN("미분류", "UNKNOWN"),
    NONE("없음","NONE")

    ;


    @Getter
    private String descKo;

    @Getter
    private String descEn;


    public static final String MISSING_KEY = "missing_";


    FirstActionTypesEnum(String descKo, String descEn) {
        this.descKo = descKo;
        this.descEn = descEn;
    }


    public static String getCodeForDescKo(String code) {
        for (FirstActionTypesEnum typesEnum : FirstActionTypesEnum.values()) {
            if (typesEnum.name().equals(code))
                return typesEnum.getDescKo();
        }

        return null;
    }

    public static String getCodeForDescEn(String code) {
        for (FirstActionTypesEnum typesEnum : FirstActionTypesEnum.values()) {
            if (typesEnum.name().equals(code))
                return typesEnum.getDescEn();
        }

        return null;
    }


}
